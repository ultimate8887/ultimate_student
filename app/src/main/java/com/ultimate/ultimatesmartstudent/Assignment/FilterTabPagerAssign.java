package com.ultimate.ultimatesmartstudent.Assignment;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class FilterTabPagerAssign extends FragmentStatePagerAdapter {
    int v;
    Context context;
    public FilterTabPagerAssign(FragmentManager fm, int v, Context context) {
        super(fm);
        this.context = context;
        this.v=v;
        //Log.e("Send_StdFragment",String.valueOf(v));
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CommonAssignFragment("a",context);
            case 1:
                return new CommonAssignFragment("s",context);
            case 2:
                return new CommonAssignFragment("e",context);
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Active";
            case 1:
                return "Submitted";
            case 2:
                return "Expire";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
