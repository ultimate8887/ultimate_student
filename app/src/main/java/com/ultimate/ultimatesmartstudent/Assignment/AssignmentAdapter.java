package com.ultimate.ultimatesmartstudent.Assignment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<AssignmentBean> assignmentList;
    Mycallback mAdaptercall;
    Animation animation;
    AssignmentAdapter(Context mContext, ArrayList<AssignmentBean> assignmentList, Mycallback mAdaptercall) {
        this.mContext = mContext;
        this.assignmentList = assignmentList;
        this.mAdaptercall = mAdaptercall;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.assignment_lyt, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        AssignmentBean data = assignmentList.get(position);


        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
//
//        if (data.getLast_date() != null){
//            holder.sub.setText(data.getLast_date());
//        }else{
//            holder.sub.setText("Not Mentioned");
//        }
        if (data.getSubject_name() != null) {
            String title = getColoredSpanned("Subject:-", "#000000");
            String Name="";
            Name = getColoredSpanned(data.getSubject_name(), "#5A5C59");
            holder.classess.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.classess.setVisibility(View.GONE);
        }

        if (data.getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("assign-"+data.getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        holder.homeTopic.setText(data.getComment());

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());
        //for Tomorrow
        DateFormat dateFormat1 = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, +1);
        String dateString2= dateFormat1.format(cal1.getTime());

        if(data.getStatus().equalsIgnoreCase("active")){

            String check= Utils.getDateFormated(data.getCreated_on());
            if (check.equalsIgnoreCase(dateString)){
                String title = getColoredSpanned("Created on ", "#000000");
                String l_Name = getColoredSpanned("<b>"+"Today"+"</b>", "#e31e25");
                holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
            }else if (check.equalsIgnoreCase(dateString1)){
                String title = getColoredSpanned("Created on ", "#000000");
                String l_Name = getColoredSpanned("<b>"+"Yesterday"+"</b>", "#1C8B3B");
                holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
            }else {
                String title = getColoredSpanned("Created on ", "#000000");
                String Name = getColoredSpanned("<b>"+Utils.getDateTimeFormatedWithAMPM(data.getCreated_on())+"</b>", "#5A5C59");
                holder.u_date.setText(Html.fromHtml(title + " " + Name));
            }
        } else if(data.getStatus().equalsIgnoreCase("expire")) {
            String title = getColoredSpanned("Status:- ", "#000000");
            String l_Name = getColoredSpanned("<b>"+"Expire"+"</b>", "#e31e25");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
        }else {
            String check = Utils.getDateFormated(data.getAs_submit_date());
            if (check.equalsIgnoreCase(dateString)) {
                String title = getColoredSpanned("Submitted on ", "#000000");
                String l_Name = getColoredSpanned("<b>" + "Today" + "</b>", "#e31e25");
                holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
            } else if (check.equalsIgnoreCase(dateString1)) {
                String title = getColoredSpanned("Submitted on ", "#000000");
                String l_Name = getColoredSpanned("<b>" + "Yesterday" + "</b>", "#1C8B3B");
                holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
            } else {
                String title = getColoredSpanned("Submitted on ", "#000000");
                String Name = getColoredSpanned("<b>" + Utils.getDateTimeFormatedWithAMPM(data.getAs_submit_date()) + "</b>", "#5A5C59");
                holder.u_date.setText(Html.fromHtml(title + " " + Name));

            }
        }

        String check1= Utils.getDateFormated(data.getLast_date());
        if (check1.equalsIgnoreCase(dateString)){

            String title = getColoredSpanned("Last Date: ", "#000000");
            String l_Name = getColoredSpanned("Today", "#e31e25");
            holder.sub.setText(Html.fromHtml(title + " " + l_Name));

        }else if (check1.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Last Date: ", "#000000");
            String l_Name = getColoredSpanned("Yesterday", "#1C8B3B");
            holder.sub.setText(Html.fromHtml(title + " " + l_Name));

        }else if (check1.equalsIgnoreCase(dateString2)){

            String title = getColoredSpanned("Last Date: ", "#000000");
            String l_Name = getColoredSpanned("Tomorrow", "#DE5C9D");
            holder.sub.setText(Html.fromHtml(title + " " + l_Name));

        }else {
            // holder.sub.setText(Utils.getDateFormated(data.getCreated_on()));
            String title = getColoredSpanned("Last Date: ", "#000000");
            String l_Name = getColoredSpanned(Utils.getDateFormated(data.getLast_date()), "#cc7722");
            holder.sub.setText(Html.fromHtml(title + " " + l_Name));
        }


        holder.nofile.setVisibility(View.VISIBLE);
        holder.audio_file.setVisibility(View.GONE);
        holder.pdf_file.setVisibility(View.GONE);

        if (data.getImage_tag() != null && data.getImage_tag().equalsIgnoreCase("images")){
            holder.empty_file.setVisibility(View.GONE);
            holder.image_file.setVisibility(View.VISIBLE);
            Picasso.get().load(data.getImage().get(0)).placeholder(mContext.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            holder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.image_file.startAnimation(animation);
                    if (mAdaptercall != null) {
                        mAdaptercall.viewPdf(assignmentList.get(position));
                    }
                }
            });
        }else if (data.getImage_tag().equalsIgnoreCase("pdf")){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.VISIBLE);
            holder.empty_file.setVisibility(View.GONE);
            holder.pdf_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.pdf_file.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethod_pdf_call_back(assignmentList.get(position));
                    }
                }
            });
        }else{
            //   Picasso.get().load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.drawable.classwork)).into(holder.image_file);
            Picasso.get().load(R.drawable.assignment).into(holder.empty_file);
            holder.empty_file.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
        }

        if(data.getStatus().equalsIgnoreCase("active")){
            holder.tap_rep_pdf.setVisibility(View.VISIBLE);
            holder.tap_exp_pdf.setVisibility(View.GONE);
            holder.tap_view_pdf.setVisibility(View.GONE);
            holder.tap_rep_pdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tap_rep_pdf.startAnimation(animation);
                    mAdaptercall.replyPdf(assignmentList.get(position));
                }
            });
        } else if(data.getStatus().equalsIgnoreCase("submit")){
            holder.tap_rep_pdf.setVisibility(View.GONE);
            holder.tap_exp_pdf.setVisibility(View.GONE);
            holder.tap_view_pdf.setVisibility(View.VISIBLE);
            holder.tap_view_pdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tap_view_pdf.startAnimation(animation);
                    mAdaptercall.view_rep_Pdf(assignmentList.get(position));
                }
            });
        }else {
            holder.tap_rep_pdf.setVisibility(View.GONE);
            holder.tap_exp_pdf.setVisibility(View.VISIBLE);
            holder.tap_view_pdf.setVisibility(View.GONE);
        }


    }

    public interface Mycallback {
        public void onMethod_pdf_call_back(AssignmentBean assignmentBean);
        public void viewPdf(AssignmentBean assignmentBean);
        public void replyPdf(AssignmentBean assignmentBean);
        public void view_rep_Pdf(AssignmentBean assignmentBean);

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return (assignmentList.size());
    }

    public void setAssignmentList(ArrayList<AssignmentBean> assignmentList) {
        this.assignmentList = assignmentList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tap_view_pdf)
        TextView tap_view_pdf;
        @BindView(R.id.tap_rep_pdf)
        TextView tap_rep_pdf;
        @BindView(R.id.tap_exp_pdf)
        TextView tap_exp_pdf;

        public TextView u_date,homeTopic,sub;
        public RelativeLayout withfile;
        @BindView(R.id.nofile)
        RelativeLayout nofile;
        @BindView(R.id.empty_file)
        ImageView empty_file;
        @BindView(R.id.classess)
        TextView classess;
        @BindView(R.id.id)
        TextView id;
        public ImageView image_file, audio_file, pdf_file;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
        }
    }
}