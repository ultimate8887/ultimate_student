package com.ultimate.ultimatesmartstudent.Assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartstudent.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainAssignActivity extends AppCompatActivity {


    @BindView(R.id.imgBackmsg)
    ImageView imgBackks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Animation animation;

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    String id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_assign);
        ButterKnife.bind(this);
        if (getIntent().getExtras() !=null){
            id = getIntent().getExtras().getString("id");
        }
        animation = AnimationUtils.loadAnimation(MainAssignActivity.this, R.anim.btn_blink_animation);

        setupTabPager();
    }
    private void setupTabPager() {


        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));

        tablayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewpager) {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                String value=String.valueOf(tab.getText());

                if (value.equalsIgnoreCase("Active")){
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.green)
                    );
                    // Selected Tab Indicator Color
                    //   tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.leave));
                    //  tablayout.setSelectedTabIndicatorHeight(5);
                }else if (value.equalsIgnoreCase("Submitted")) {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.leave)
                    );
                    // Selected Tab Indicator Color
                    //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.present));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }else {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.light_red)
                    );
                    // Selected Tab Indicator Color
                    //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.absent));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });


        FilterTabPagerAssign adapter;
//        if (id.equalsIgnoreCase("inbox")){
//            txtTitle.setText("Inbox");
//            adapter = new FilterTabPagerAssign(getSupportFragmentManager(),1,this);
//        }else {
            txtTitle.setText(getString(R.string.assignment));
            adapter = new FilterTabPagerAssign(getSupportFragmentManager(),2,this);
     //   }
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackks()  {
        imgBackks.startAnimation(animation);
        finish();
    }
}