package com.ultimate.ultimatesmartstudent.Assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.E_book.Ebook;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;
import com.ultimate.ultimatesmartstudent.Webview.FileDownloader;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Active_AssigmentActivity extends AppCompatActivity implements AssignmentAdapter.Mycallback {
    private String classid = "";
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)
    TextView imgNoData;
    private ArrayList<AssignmentBean> assignmentList;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerview;
    private AssignmentAdapter madapter;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    Spinner spinnerClass;
    CommonProgress commonProgress;
    @BindView(R.id.textView8)
    TextView textsubtitle;
    @BindView(R.id.spinnerGroups)
    Spinner SubjectSpinner;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    String  class_name, sub_id, sub_name;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Animation animation;
    String folder_main = "Assignment";
    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;

    AssignmentBean assignmentBean;
    BottomSheetDialog mBottomSheetDialog1;
    String path="";
    Uri uri;
    float  length;
    String size;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
      //  SubjectSpinner = (Spinner) findViewById(R.id.spinner);
        txtTitle.setText(getString(R.string.active));
        textsubtitle.setText(getString(R.string.f_subject));
        commonProgress=new CommonProgress(this);
        assignmentList = new ArrayList<>();
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        madapter = new AssignmentAdapter(this, assignmentList,this);
        recyclerview.setAdapter(madapter);
        getSubjectofClass();
    }
    private void getSubjectofClass() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            commonProgress.dismiss();
            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(Active_AssigmentActivity.this, subjectList);
                    SubjectSpinner.setAdapter(adaptersub);
                    SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                               // fetchAssignment(sub_id);

                            }
                            fetchAssignment(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @OnClick(R.id.imgBackmsg)
    public void onback() {
        imgBackmsg.startAnimation(animation);
        finish();
    }


    private void fetchAssignment(int i) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        if (i == 0){
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("key_type","active");
        } else {
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("sub_id", sub_id);
            params.put("key_type","active");
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ASSIGNMENT_URL, assignmentapiCallback, this, params);

    }

    ApiHandler.ApiCallback assignmentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (assignmentList != null)
                        assignmentList.clear();
                    assignmentList = AssignmentBean.parseAssignmentArray(jsonObject.getJSONArray(Constants.ASSIGNMENT_DATA));
                    madapter.setAssignmentList(assignmentList);
                    //setanimation on adapter...
                    recyclerview.getAdapter().notifyDataSetChanged();
                    recyclerview.scheduleLayoutAnimation();
                    //-----------end------------
                    imgNoData.setVisibility(View.GONE);
                    totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(assignmentList.size()));
                    if (assignmentList.size() <= 0) {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        imgNoData.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }  else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                assignmentList.clear();
                madapter.setAssignmentList(assignmentList);
                madapter.notifyDataSetChanged();
                imgNoData.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onMethod_pdf_call_back(AssignmentBean homeworkbean) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(Active_AssigmentActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(Active_AssigmentActivity.this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(homeworkbean.getSubject_name()+"("+homeworkbean.getComment()+")");
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.assignment).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        sub_wise.setText("View!");
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getBaseContext(), ViewPdfActivity.class);
                intent.putExtra("id", homeworkbean.getFile());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
        date_wise.setText("Save!");
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                // UltimateProgress.showProgressBar(SubWiseHW.this,"Please wait...");
                String mUrl=homeworkbean.getFile();
                // new DownloadFile().execute(mUrl);

                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("assign_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "assign_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getApplicationContext(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    @Override
    public void viewPdf(AssignmentBean assignmentBean) {

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(Active_AssigmentActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(Active_AssigmentActivity.this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(assignmentBean.getComment());
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.ebook).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setText(getString(R.string.view));
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getBaseContext(), ViewPdfActivity.class);
                intent.putExtra("id", assignmentBean.getFile());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setText(getString(R.string.save));
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                UltimateProgress.showProgressBar(Active_AssigmentActivity.this,"Please wait...");
                String mUrl=assignmentBean.getFile();
                new DownloadFile().execute(mUrl);
                String path_name ="/storage/Android/data/com.ultimate.ultimatesmartstudent/files/document/"+folder_main+"/Assign_file_saved.pdf";
                Toast.makeText(getApplicationContext(), "Downloaded to "+path_name, Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();

    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            File path = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), folder_main);
            path.mkdir();
            File file = new File(path, folder_main + System.currentTimeMillis() + "saved.pdf");
            try{
                file.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, file);
            UltimateProgress.cancelProgressBar();
            return null;
        }
    }

    @Override
    public void replyPdf(AssignmentBean assignmentBean1) {
        assignmentBean=assignmentBean1;
        pick_pdf();
    }

    private void pick_pdf() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Toast.makeText(this,"Please provide storage permission from app settings",Toast.LENGTH_LONG).show();
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else{
            callPicker();
            // showFileChooser();
        }

    }

    @Override
    public void view_rep_Pdf(AssignmentBean assignmentBean) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPicker();

                } else {
                    // permission denied, boo! Disable the
                    Toast.makeText(this, "Please provide storage permission from app settings, as it is mandatory to access your files!", Toast.LENGTH_LONG).show();
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
    public void callPicker(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //startActivityForResult(intent, SAVE_REQUEST_CODE);
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), LOAD_FILE_RESULTS);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case LOAD_FILE_RESULTS:
                    uri = data.getData();
                    // String path = Utils.getPath(this, uri);

                    if (uri != null) {
                        path = FileUtils.getRealPath(this, uri);
                        File file = new File(path);
                        length = file.length();
                        length = length / 1000000;
                        size = formatSize(file.length());
                        if ((float) length > 30.0f) {
                            Toast.makeText(getApplicationContext(), "Sorry, Your selected file size too large(" + size + "), Kindly compress your file first to upload!" +
                                    "(Max Size of file uploading should be less than 25MB)", Toast.LENGTH_LONG).show();
                        } else {
                            try {
                                fileBase64 = Utils.encodeFileToBase64Binary(path);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            showPdfFromUri(uri, path);

                            Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
//                    try {
//                        Uri uri = data.getData();
//                        // String path = Utils.getPath(this, uri);
//                        String path = FileUtils.getRealPath(this, uri);
//                        fileBase64 = Utils.encodeFileToBase64Binary(path);
//
//                        showPdfFromUri(uri,path);
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }

                        }
                    }  else {
                        Toast.makeText(this, "Selection Cancelled", Toast.LENGTH_SHORT).show();
                    }
            }
        }
    }

    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = " Bytes";
            size /= 1024;
            if (size >= 1024) {
                suffix = " MB";
                size /= 1024;
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }
        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    private void showPdfFromUri(Uri uri, String path) {
//        mBottomSheetDialog1 = new BottomSheetDialog(Active_AssigmentActivity.this);
//        final View sheetView = getLayoutInflater().inflate(R.layout.pdf_view_dialog, null);
//        mBottomSheetDialog1.setContentView(sheetView);
//
//        mBottomSheetDialog1.setCancelable(false);
//
//        Animation animation;
//        animation = AnimationUtils.loadAnimation(Active_AssigmentActivity.this, R.anim.btn_blink_animation);
//
//        TextView title= (TextView) sheetView.findViewById(R.id.title);
//        TextView path1= (TextView) sheetView.findViewById(R.id.path);
//        title.setText(assignmentBean.getComment());
//        path1.setText(path + "\nSize: " + size);
//        pdf = sheetView.findViewById(R.id.ViewPdf);
//        pdf.fromUri(uri).load();
//        Button retake= (Button) sheetView.findViewById(R.id.retake);
//        retake.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                retake.startAnimation(animation);
//                pick_pdf();
//                mBottomSheetDialog1.dismiss();
//            }
//        });
//        Button submitted= (Button) sheetView.findViewById(R.id.submitted);
//        submitted.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                submitted.startAnimation(animation);
//                submitAssignment();
//
//            }
//        });
//        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
//        btnNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnNo.startAnimation(animation);
//             //   fileBase64=null;
//                mBottomSheetDialog1.dismiss();
//            }
//        });
//        mBottomSheetDialog1.show();
    }

    private void submitAssignment() {
       UltimateProgress.showProgressBar(Active_AssigmentActivity.this,"Please Wait......!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("assignment_id", assignmentBean.getId());
        if (fileBase64 != null)
            params.put("rep_file", fileBase64);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.REPLYASSIGNMENT_URL, createassignmentapiCallback, this, params);
    }
    ApiHandler.ApiCallback createassignmentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
               // Utils.showSnackBar("Upload Succesfully", parent);
                Toast.makeText(getApplicationContext(),"Thanks for Submit!",Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                Log.e("error", error.getMessage() + "");
            }
        }
    };


}