package com.ultimate.ultimatesmartstudent.Assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.E_book.Ebook;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;
import com.ultimate.ultimatesmartstudent.Webview.FileDownloader;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class Replied_AssigmentActivity extends AppCompatActivity implements BottomSheetImagePicker.OnImagesSelectedListener {
    String add_img="empty";
    @BindView(R.id.imgBackmsg)
    ImageView imgBackks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    Animation animation;
    private ViewGroup imageContainer;
    ArrayList<String> image;
    List<? extends Uri> image2;
    String encoded;
    HorizontalScrollView scrollView;
    @BindView(R.id.textView9)
    TextView textView9;
    TextView retake,title1;
    private Bitmap hwbitmap;
    AssignmentBean data;
    @BindView(R.id.button3)
    Button button3;
    public TextView u_date,homeTopic,sub;
    @BindView(R.id.nofile)
    RelativeLayout nofile;
    @BindView(R.id.empty_file)
    ImageView empty_file;
    @BindView(R.id.classess)
    TextView classess;
    @BindView(R.id.id)
    TextView id;
    public ImageView image_file, audio_file, pdf_file;
    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    @BindView(R.id.txtSub)
    TextView txtSub;

    int PICK_IMAGE_MULTIPLE = 1;
    String imageEncoded;
    List<String> imagesEncodedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common_assign);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText(getString(R.string.submit)+" "+getString(R.string.assignment));
        sub = (TextView) findViewById(R.id.subject);
        u_date = (TextView) findViewById(R.id.u_date);
        homeTopic = (TextView) findViewById(R.id.homeTopic);
        image_file = (ImageView) findViewById(R.id.image_file);
        pdf_file = (ImageView) findViewById(R.id.pdf_file);
        audio_file = (ImageView) findViewById(R.id.audio_file);

        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("data")) {
            Gson gson = new Gson();
            data = gson.fromJson(intent_value.getString("data"), AssignmentBean.class);
            setData(data);
        } else {
            return;
        }

        retake= (TextView) findViewById(R.id.addimage);
        imageContainer = findViewById(R.id.imageContainer);
        scrollView = findViewById(R.id.scrollView);

        retake.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) {
                retake.startAnimation(animation);
                add_img="add";
                pick_image();
            }
        });

    }


    @SuppressLint("ResourceType")
    private void onMultiImageViewClick() {
        new BottomSheetImagePicker.Builder(getString(R.xml.provider_paths))
                .multiSelect(1, 6)
                .multiSelectTitles(
                        R.plurals.pick_multi,
                        R.plurals.pick_multi_more,
                        R.string.pick_multi_limit
                )
                .peekHeight(R.dimen.peekHeight)
                .columnSize(R.dimen.columnSize)
                .requestTag("multi")
                .show(getSupportFragmentManager(), null);

    }

    private void pick_image() {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {
                //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
                // initialising intent
                Intent intent = new Intent();
                // setting type to select to be image
                intent.setType("image/*");
                // allowing multiple image to be selected
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);
            }

        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);

                }
            } else {
                onMultiImageViewClick();
                // showFileChooser();
            }

        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // callPicker();
                  //  onMultiImageViewClick();
                } else {
                    // permission denied, boo! Disable the
                    Toast.makeText(this,"Please provide storage permission from app settings, as it is mandatory to access your files!",Toast.LENGTH_LONG).show();
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackks()  {
        imgBackks.startAnimation(animation);
        finish();
    }


    private void setData(AssignmentBean data) {
        txtSub.setVisibility(View.VISIBLE);
        txtSub.setText(User.getCurrentUser().getFirstname()+" "+User.getCurrentUser().getLastname()+"("+User.getCurrentUser().getId()+")");

        if (data.getSubject_name() != null) {
            String title = getColoredSpanned("Subject:-", "#000000");
            String Name="";
            Name = getColoredSpanned(data.getSubject_name(), "#5A5C59");
            classess.setText(Html.fromHtml(title + " " + Name));
        }else {
            classess.setVisibility(View.GONE);
        }

        if (data.getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("assign-"+data.getId(), "#5A5C59");
            id.setText(Html.fromHtml(title + " " + Name));
        }

        homeTopic.setText(data.getComment());

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(data.getCreated_on());
        if (check.equalsIgnoreCase(dateString)){
            String title = getColoredSpanned("Created on ", "#000000");
            String l_Name = getColoredSpanned("<b>"+"Today"+"</b>", "#e31e25");
            u_date.setText(Html.fromHtml(title + " " + l_Name));
        }else if (check.equalsIgnoreCase(dateString1)){
            String title = getColoredSpanned("Created on ", "#000000");
            String l_Name = getColoredSpanned("<b>"+"Yesterday"+"</b>", "#1C8B3B");
           u_date.setText(Html.fromHtml(title + " " + l_Name));
        }else {
            String title = getColoredSpanned("Created on ", "#000000");
            String Name = getColoredSpanned("<b>"+Utils.getDateTimeFormatedWithAMPM(data.getCreated_on())+"</b>", "#5A5C59");
            u_date.setText(Html.fromHtml(title + " " + Name));
        }

        String check1= Utils.getDateFormated(data.getLast_date());

        if (check1.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Last Date: ", "#000000");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Today", "#e31e25");
            sub.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check1.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Last Date: ", "#000000");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Yesterday", "#1C8B3B");
            sub.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
           // sub.setText(Utils.getDateFormated(data.getCreated_on()));

            String title = getColoredSpanned("Last Date: ", "#000000");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned(Utils.getDateFormated(data.getLast_date()), "#cc7722");
           sub.setText(Html.fromHtml(title + " " + l_Name));
        }

       nofile.setVisibility(View.VISIBLE);
       audio_file.setVisibility(View.GONE);
       pdf_file.setVisibility(View.GONE);

        if (data.getImage_tag().equalsIgnoreCase("images")){
            empty_file.setVisibility(View.GONE);
            image_file.setVisibility(View.VISIBLE);
            Picasso.get().load(data.getImage().get(0)).placeholder(this.getResources().getDrawable(R.color.transparent)).into(image_file);
            image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    image_file.startAnimation(animation);
                    openDialog(data);
                }
            });
        }else{
            //   Picasso.get().load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.drawable.classwork)).into(holder.image_file);
            Picasso.get().load(R.drawable.assignment).into(empty_file);
            empty_file.setVisibility(View.VISIBLE);
            image_file.setVisibility(View.GONE);
        }
    }

    private void openDialog(AssignmentBean data) {
        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(data.getComment());
        ViewPagerAdapter mAdapter = new  ViewPagerAdapter(this,data.getImage(),"assignment");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);
        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=data.getImage();
                Intent intent = new Intent(Replied_AssigmentActivity.this, IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "assignment");
                intent.putExtra("title", "Assignment");
                intent.putExtra("sub", "Assignment_id:- "+data.getId());
                startActivity(intent);
            }
        });

        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        commonCode(list);
    }

    private void commonCode(List<? extends Uri> list) {
        imageContainer.removeAllViews();
        // Toast.makeText(getApplicationContext(),"Thanks for Submit!",Toast.LENGTH_SHORT).show();
        image= new ArrayList<>();
        image2=list;
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.scrollitem_image, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            hwbitmap = Utils.getBitmapFromUri(this, uri, 2048);
            Log.e("hwbitmapup", String.valueOf(hwbitmap));
            encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);

            // encoded = String.format("data:image/jpeg;base64,%s", encoded);
            image.add(encoded);

        }
        Log.e("hwbitmapupdown", String.valueOf(hwbitmap));
        Log.e("image2", String.valueOf(image2));
        button3.setVisibility(View.VISIBLE);
        textView9.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                imagesEncodedList = new ArrayList<String>();
                if(data.getData()!=null){

                    Uri mImageUri=data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded  = cursor.getString(columnIndex);
                    cursor.close();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded  = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                        }
                        commonCode(mArrayUri);
                        Log.e("LOG_TAG", "Selected Images" + mArrayUri.size());
//                        Toast.makeText(this, "Selected Images" + imagesEncodedList.size(),
//                                Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
            Log.e("LOG_TAG", "Exception is " + e);
        }


    }

    @OnClick(R.id.button3)
    public void button3()  {
        button3.startAnimation(animation);
        if (add_img.equalsIgnoreCase("add")) {
            submitAssignment();
        }else {
            Toast.makeText(getApplicationContext(),"Kindly attach assignment images!",Toast.LENGTH_SHORT).show();
        }
    }

    private void submitAssignment() {
        UltimateProgress.showProgressBar(this,"Please Wait, file uploading takes few seconds......!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("assignment_id", data.getId());
        params.put("image", String.valueOf(image));
        params.put("add_img", add_img);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.REPLYASSIGNMENT_URL, createassignmentapiCallback, this, params);
    }
    ApiHandler.ApiCallback createassignmentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                // Utils.showSnackBar("Upload Succesfully", parent);
                Toast.makeText(getApplicationContext(),"Thanks for Submit!",Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                Log.e("error", error.getMessage() + "");
            }
        }
    };

}