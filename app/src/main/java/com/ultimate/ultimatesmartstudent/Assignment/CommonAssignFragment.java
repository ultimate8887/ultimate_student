package com.ultimate.ultimatesmartstudent.Assignment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;


public class CommonAssignFragment extends Fragment implements AssignmentAdapter.Mycallback {

    Context context;
    String value="";

    private String classid = "";
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)
    TextView imgNoData;
    private ArrayList<AssignmentBean> assignmentList;
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerview;
    private AssignmentAdapter madapter;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    CommonProgress commonProgress;
    @BindView(R.id.spinner)
    Spinner SubjectSpinner;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    String  class_name, sub_id, sub_name;
    Animation animation;
    String folder_main = "Assignment";
    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;
    AssignmentBean assignmentBean;
    Dialog mBottomSheetDialog1;

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;


    public CommonAssignFragment(String value, Context context) {
        this.context=context;
        this.value=value;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_common_assign, container, false);
        ButterKnife.bind(this,view);
        commonProgress=new CommonProgress(getActivity());
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        assignmentList = new ArrayList<>();
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        madapter = new AssignmentAdapter(getActivity(), assignmentList,this);
        recyclerview.setAdapter(madapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("2st","2st");
        getSubjectofClass();
    }

    private void getSubjectofClass() {
        if (value.equalsIgnoreCase("a")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            commonProgress.dismiss();
            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(getActivity(), subjectList);
                    SubjectSpinner.setAdapter(adaptersub);
                    SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                                // fetchAssignment(sub_id);

                            }
                            fetchAssignment(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    private void fetchAssignment(int i) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        if (i == 0){
            params.put("class_id", User.getCurrentUser().getClass_id());

            if (value.equalsIgnoreCase("a")){
                params.put("key_type","active");
            }else if (value.equalsIgnoreCase("e")){
                params.put("key_type","expire");
            }else {
                params.put("key_type","submit");
            }
        } else {
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("sub_id", sub_id);
            if (value.equalsIgnoreCase("a")){
                params.put("key_type","active");
            }else if (value.equalsIgnoreCase("e")){
                params.put("key_type","expire");
            }else {
                params.put("key_type","submit");
            }
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ASSIGNMENT_URL, assignmentapiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback assignmentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (assignmentList != null)
                        assignmentList.clear();
                    assignmentList = AssignmentBean.parseAssignmentArray(jsonObject.getJSONArray(Constants.ASSIGNMENT_DATA));
                    madapter.setAssignmentList(assignmentList);
                    //setanimation on adapter...
                    recyclerview.getAdapter().notifyDataSetChanged();
                    recyclerview.scheduleLayoutAnimation();
                    //-----------end------------
                    imgNoData.setVisibility(View.GONE);
                    totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(assignmentList.size()));
                    if (assignmentList.size() <= 0) {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        imgNoData.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }  else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                assignmentList.clear();
                madapter.setAssignmentList(assignmentList);
                madapter.notifyDataSetChanged();
                imgNoData.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onMethod_pdf_call_back(AssignmentBean homeworkbean) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(homeworkbean.getSubject_name()+"("+homeworkbean.getComment()+")");
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.assignment).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        sub_wise.setText("View!");
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getActivity(), ViewPdfActivity.class);
                intent.putExtra("id", homeworkbean.getFile());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
        date_wise.setText("Save!");
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                // UltimateProgress.showProgressBar(SubWiseHW.this,"Please wait...");
                String mUrl=homeworkbean.getFile();
                // new DownloadFile().execute(mUrl);

                DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("assign_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "assign_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getActivity(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    @Override
    public void viewPdf(AssignmentBean data) {
     openDialog(data,"a");
    }

    private void openDialog(AssignmentBean data,String value) {
        final Dialog warningDialog = new Dialog(getActivity());
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText("Assignment_id:- "+data.getId());

        ViewPagerAdapter mAdapter = null;
        if (value.equalsIgnoreCase("r")){
            mAdapter = new ViewPagerAdapter(getActivity(), data.getRep_image(), "assignment");
        }else {
            mAdapter = new ViewPagerAdapter(getActivity(), data.getImage(), "assignment");
        }
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);


        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);
        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);

        if (value.equalsIgnoreCase("r")){
            imgDownload.setVisibility(View.GONE);
        }else {
            imgDownload.setVisibility(View.VISIBLE);
        }

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=data.getImage();
                Intent intent = new Intent(getActivity(), IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "assignment");
                intent.putExtra("title", "Assignment");
                intent.putExtra("sub", "Assignment_id:- "+data.getId());
                startActivity(intent);
            }
        });



        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1="";
        if (value.equalsIgnoreCase("r")){
             l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getRep_image().size()), "#000000");
        }else {
             l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
        }
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
              //  String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getRep_image().size()), "#000000");

                String l_Name1="";
                if (value.equalsIgnoreCase("r")){
                    l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getRep_image().size()), "#000000");
                }else {
                    l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
                }

                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void replyPdf(AssignmentBean assignmentBean1) {
        assignmentBean=assignmentBean1;
        if (value.equalsIgnoreCase("a")){

            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
            builder1.setMessage("Do you want to attach replied Assignment? ");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Gson gson = new Gson();
                            String classworkdata = gson.toJson(assignmentBean1, AssignmentBean.class);
                            Intent intent = new Intent(getActivity(), Replied_AssigmentActivity.class);
                            intent.putExtra("data", classworkdata);
                            startActivity(intent);
                        }
                    });
            builder1.setNegativeButton(
                    "No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }else if (value.equalsIgnoreCase("e")){

        }else {

        }
    }


    @Override
    public void view_rep_Pdf(AssignmentBean assignmentBean) {
        openDialog(assignmentBean,"r");
    }


}