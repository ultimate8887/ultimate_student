package com.ultimate.ultimatesmartstudent.Assignment;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AssignmentBean  {

    private static String ID = "id";
    private static String COMMENT = "comment";
    private static String SUB_ID = "subject_id";
    private static String LAST_DATE = "last_date";
    private static String IMAGE = "file";
    private static String RE_FILE = "rep_file";
    private static String CREATED_ON = "created_on";
    private static String MARK = "mark";
    private static String STATUS = "status";
    private static String SUB_NAME = "subject_name";

    private static String AIMAGE="image";
    private static String RIMAGE="rep_image";

    private ArrayList<String> image;
    private ArrayList<String> rep_image;
    private String image_tag;

    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }

    public ArrayList<String> getRep_image() {
        return rep_image;
    }

    public void setRep_image(ArrayList<String> rep_image) {
        this.rep_image = rep_image;
    }

    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    public String getRep_image_tag() {
        return rep_image_tag;
    }

    public void setRep_image_tag(String rep_image_tag) {
        this.rep_image_tag = rep_image_tag;
    }

    private String rep_image_tag;


    /**
     * id : 1
     * comment : sfsdgdfsghsdggfgdf
     * subject_id : 1
     * last_date : 2018-03-30
     * image : appinfo.png
     * created_on : 2018-03-28
     * mark : 25
     */

    private String id;
    private String comment;
    private String subject_id;
    private String last_date;
    private String file;
    private String  class_id;
    private String class_name;

    public String getRep_file() {
        return rep_file;
    }

    public void setRep_file(String rep_file) {
        this.rep_file = rep_file;
    }

    private String status;


    private String rep_file;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAs_submit_date() {
        return as_submit_date;
    }

    public void setAs_submit_date(String as_submit_date) {
        this.as_submit_date = as_submit_date;
    }

    private String as_submit_date;

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    private String created_on;
    private String mark;
    /**
     * subject_name : ENGLISH
     */

    private String subject_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getLast_date() {
        return last_date;
    }

    public void setLast_date(String last_date) {
        this.last_date = last_date;
    }



    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public static ArrayList<AssignmentBean> parseAssignmentArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<AssignmentBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                AssignmentBean p = parseAssignmentObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static AssignmentBean parseAssignmentObject(JSONObject jsonObject) {
        AssignmentBean casteObj = new AssignmentBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(COMMENT) && !jsonObject.getString(COMMENT).isEmpty() && !jsonObject.getString(COMMENT).equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString(COMMENT));
            }
            if (jsonObject.has(SUB_ID) && !jsonObject.getString(SUB_ID).isEmpty() && !jsonObject.getString(SUB_ID).equalsIgnoreCase("null")) {
                casteObj.setSubject_id(jsonObject.getString(SUB_ID));
            }
            if (jsonObject.has(LAST_DATE) && !jsonObject.getString(LAST_DATE).isEmpty() && !jsonObject.getString(LAST_DATE).equalsIgnoreCase("null")) {
                casteObj.setLast_date(jsonObject.getString(LAST_DATE));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setFile(Constants.getImageBaseURL() +jsonObject.getString(IMAGE));
            }
            if (jsonObject.has(RE_FILE) && !jsonObject.getString(RE_FILE).isEmpty() && !jsonObject.getString(RE_FILE).equalsIgnoreCase("null")) {
                casteObj.setRep_file(Constants.getImageBaseURL() +jsonObject.getString(RE_FILE));
            }
            if (jsonObject.has(CREATED_ON) && !jsonObject.getString(CREATED_ON).isEmpty() && !jsonObject.getString(CREATED_ON).equalsIgnoreCase("null")) {
                casteObj.setCreated_on(jsonObject.getString(CREATED_ON));
            }
            if (jsonObject.has(MARK) && !jsonObject.isNull(MARK)) {
                String markValue = jsonObject.getString(MARK);
                // Check if markValue is not empty or "null" string
                if (markValue != null && !markValue.isEmpty() && !markValue.equalsIgnoreCase("null")) {
                    // Assign markValue to your bean or perform necessary operations
                    casteObj.setMark(markValue);
                } else {
                    // Handle case where markValue is empty or "null"
                    casteObj.setMark("0"); // or handle accordingly based on your logic
                }
            } else {
                // Handle case where "mark" field does not exist or is null
                casteObj.setMark("0"); // or handle accordingly based on your logic
            }

            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString(STATUS));
            }else{
                casteObj.setStatus("empty");
            }

            if (jsonObject.has(SUB_NAME) && !jsonObject.getString(SUB_NAME).isEmpty() && !jsonObject.getString(SUB_NAME).equalsIgnoreCase("null")) {
                casteObj.setSubject_name(jsonObject.getString(SUB_NAME));
            }

            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }


            if (jsonObject.has(AIMAGE) && jsonObject.get(AIMAGE) instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray(AIMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }

            if (jsonObject.has(RIMAGE) && jsonObject.get(RIMAGE) instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray(RIMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setRep_image(img_arrs);
            }


            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                casteObj.setImage_tag(jsonObject.getString("image_tag"));
            }else{
                casteObj.setImage_tag("empty");
            }

            if (jsonObject.has("rep_image_tag") && !jsonObject.getString("rep_image_tag").isEmpty() && !jsonObject.getString("rep_image_tag").equalsIgnoreCase("null")) {
                casteObj.setRep_image_tag(jsonObject.getString("rep_image_tag"));
            }
            if (jsonObject.has("as_submit_date") && !jsonObject.getString("as_submit_date").isEmpty() && !jsonObject.getString("as_submit_date").equalsIgnoreCase("null")) {
                casteObj.setAs_submit_date(jsonObject.getString("as_submit_date"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }
}
