package com.ultimate.ultimatesmartstudent.TestMod;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.SyllabusMod.SyllabusAdapterGDS;
import com.ultimate.ultimatesmartstudent.UltimateERP;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GDSTest extends AppCompatActivity implements TestListRecyclerAdapter.Adaptercall {


    @BindView(R.id.textView8)
    TextView textsubtitle;
    @BindView(R.id.spinnerGroups)
    Spinner testlist;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    ArrayList<TestList> subjectlist = new ArrayList<>();
    ArrayList<TestListBean> TestListBeanList = new ArrayList<>();
    ArrayList<Testresultbean> testresult = new ArrayList<>();
    RecyclerView recyclerView;
    ImageView back;
    TestListRecyclerAdapter testadapter;
    String sub_id="";
    String sub="";
    String folder_main = "Test";
    String test_id;
    String marks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    Animation animation;
    String textholder="",school="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText(getString(R.string.test));
        commonProgress=new CommonProgress(this);
        school=User.getCurrentUser().getSchoolData().getName();
        textsubtitle.setText(getString(R.string.f_subject));
       // testlist = (Spinner) findViewById(R.id.spinner);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView11);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerView.setLayoutManager(new LinearLayoutManager(GDSTest.this));
        testadapter = new TestListRecyclerAdapter(GDSTest.this, TestListBeanList, GDSTest.this);
        recyclerView.setAdapter(testadapter);
        back = (ImageView) findViewById(R.id.imgBackmsg);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        fetchsubjectlist();
    }

    private void fetchsubjectlist() {
       // UltimateProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, testapiCallback, this, params);
    }

    ApiHandler.ApiCallback testapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("sub_data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        TestList dm = new TestList();
                        dm.setName(jsonObject1.getString("name"));
                        dm.setId(jsonObject1.getString("id"));
                        subjectlist.add(dm);

                    }
                    TestSubjectListAdapter testlisttadapter = new TestSubjectListAdapter(GDSTest.this, subjectlist);
                    testlist.setAdapter(testlisttadapter);
                    testlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_id = subjectlist.get(i - 1).getId();
                              //  TestListBeanList.clear();
                            }
                            fetchTest(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(GDSTest.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchTest(int i) {
        commonProgress.show();
      //  Toast.makeText(getApplicationContext(),User.getCurrentUser().getSection_id(),Toast.LENGTH_SHORT).show();
        HashMap<String, String> params = new HashMap<String, String>();
        if (i == 0){
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("section_id", User.getCurrentUser().getSection_id());
        } else {
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("sub_id", sub_id);
            params.put("section_id", User.getCurrentUser().getSection_id());
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.TestListgds, test1apiCallback, this, params);
    }

    ApiHandler.ApiCallback test1apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (TestListBeanList != null) {
                        TestListBeanList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("test_data");
                    TestListBeanList = TestListBean.parsetestlistArray(jsonArray);
                    if (TestListBeanList.size() > 0) {
                        testadapter.settestList(TestListBeanList);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText(getString(R.string.t_entries)+ " "+String.valueOf(TestListBeanList.size()));
                    } else {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        testadapter.settestList(TestListBeanList);
                        testadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                TestListBeanList.clear();
                testadapter.settestList(TestListBeanList);
                testadapter.notifyDataSetChanged();
                txtNorecord.setVisibility(View.VISIBLE);

            }
        }
    };

    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "Test_img_saved" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "Test_img_saved" + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void methodcall(final TestListBean obj) {
        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imgShow = (ImageView) dialogLog.findViewById(R.id.img);

        Picasso.get().load(obj.getTest_file()).placeholder(getResources().getDrawable(R.drawable.logo)).into(imgShow);

        Button save = (Button) dialogLog.findViewById(R.id.submit);
        save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                save.startAnimation(animation);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);
            }
        });

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
    }

    @Override
    public void methodcallresult(TestListBean obj) {
        sub=obj.getSub_name();
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("sub_id", obj.getSub_id());
        params.put("test_id", obj.getId());
        params.put("student_id", User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.TestResultgds, testresultapiCallback, this, params);
    }

    ApiHandler.ApiCallback testresultapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("testresult_data");
                    testresult = Testresultbean.parseTstrsltArray(jsonArray);

                    opendialogsec(testresult.get(0).getMarks(),sub);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                opendialogsec("Wait For Your Result!",sub);
              //  Toast.makeText(GDSTest.this, error.getMessage(), Toast.LENGTH_SHORT).show();
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(GDSTest.this, LoginActivity.class));
//                    finish();
//                }
            }
        }
    };

    private void opendialogsec(String msg,String sub) {
        final Dialog Dialog1 = new Dialog(GDSTest.this);
        Dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Dialog1.setCancelable(true);
        Dialog1.setContentView(R.layout.testresult_lyt);
        Dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView score = (TextView) Dialog1.findViewById(R.id.txt2);
        TextView sub_name = (TextView) Dialog1.findViewById(R.id.txt1);
        CircularImageView img = (CircularImageView) Dialog1.findViewById(R.id.img);
        TextView waiting = (TextView) Dialog1.findViewById(R.id.txt3);
        TextView footer = (TextView) Dialog1.findViewById(R.id.txt4);


        sub_name.setText(sub+" "+getString(R.string.tresult));

        if (User.getCurrentUser().getProfile() != null) {
            Picasso.get().load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(img);
        } else {
            Picasso.get().load(R.drawable.boy).into(img);
        }

        if (msg== null){
            waiting.setVisibility(View.VISIBLE);
            footer.setVisibility(View.GONE);
            score.setVisibility(View.GONE);
            waiting.setText(getString(R.string.wresult));
        }else {

            String extract = msg.replaceAll("[^a-zA-Z]+", "");

            if (extract.equalsIgnoreCase("")) {
                footer.setVisibility(View.VISIBLE);
                score.setVisibility(View.VISIBLE);
                waiting.setVisibility(View.VISIBLE);
                waiting.setText(getString(R.string.congrats) + " " + User.getCurrentUser().getFirstname() + "! " + getString(R.string.t1_test));
                score.setText(msg);
            }else {
                waiting.setVisibility(View.GONE);
                footer.setVisibility(View.GONE);
                score.setVisibility(View.VISIBLE);
                score.setText(msg);
            }
        }
        Button exit = (Button) Dialog1.findViewById(R.id.OK);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog1.dismiss();
            }
        });
        Dialog1.show();
   }

}
