package com.ultimate.ultimatesmartstudent.TestMod;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TestListRecyclerAdapter extends RecyclerView.Adapter<TestListRecyclerAdapter.ViewHolder>

    {
        Context context;
        private Adaptercall mAdaptercall;
        private ArrayList<TestListBean> MainImageUploadInfoList;
        Animation animation;

    public TestListRecyclerAdapter(Context context, ArrayList < TestListBean > MainImageUploadInfoList, Adaptercall mAdaptercall)
        {
            this.MainImageUploadInfoList = MainImageUploadInfoList;
            this.context = context;
            this.mAdaptercall = mAdaptercall;
        }

        @Override
        public ViewHolder onCreateViewHolder (ViewGroup parent,int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_list_lyt, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


        @Override
        public void onBindViewHolder (final ViewHolder holder, @SuppressLint("RecyclerView") final int position){

            animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
            if(MainImageUploadInfoList.get(position).getStatus().equalsIgnoreCase("inactive")){

                holder.upldreslttext.setVisibility(View.INVISIBLE);
                holder.viewresult.setVisibility(View.VISIBLE);
            }else{

                holder.upldreslttext.setVisibility(View.VISIBLE);
                holder.viewresult.setVisibility(View.INVISIBLE);

            }

            holder.viewresult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAdaptercall.methodcallresult(MainImageUploadInfoList.get(position));
                }
            });

            if (MainImageUploadInfoList.get(position).getId() != null) {
                String title = getColoredSpanned("ID:- ", "#000000");
                String Name = getColoredSpanned("ts-"+MainImageUploadInfoList.get(position).getId(), "#5A5C59");
                holder.id.setText(Html.fromHtml(title + " " + Name));
            }

            holder.Topic.setText(MainImageUploadInfoList.get(position).getTopic());

            if (MainImageUploadInfoList.get(position).getMarks() != null) {
                String title = getColoredSpanned("<b>"+"Total marks:- "+"</b>", "#000000");
                String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getMarks(), "#5A5C59");
                holder.maxMarks.setText(Html.fromHtml(title + " " + Name));
            }

            if (MainImageUploadInfoList.get(position).getDur() != null) {
                String title = getColoredSpanned("<b>"+"Duration: "+"</b>", "#000000");
                String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getDur()+" min.", "#5A5C59");
                holder.Dur.setText(Html.fromHtml(title + " " + Name));
            }

            if (MainImageUploadInfoList.get(position).getTest_date() != null) {

            }

            //for Today
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            //for Yesterday
            DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            String dateString1= dateFormat.format(cal.getTime());

            String check= Utils.getDateFormated(MainImageUploadInfoList.get(position).getTest_date());
            if (check.equalsIgnoreCase(dateString)){
                //  holder.lytLine.setVisibility(View.VISIBLE);
                // holder.txtDT.setText("Today");
                String title = getColoredSpanned("TestDate:-", "#000000");
                //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
                String l_Name = getColoredSpanned("<b>"+"Today"+"</b>", "#e31e25");
                holder.Date.setText(Html.fromHtml(title + " " + l_Name));


                // holder.txtDT.setText("Today at "+time);
                //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
            }else if (check.equalsIgnoreCase(dateString1)){

                String title = getColoredSpanned("TestDate:-", "#000000");
                //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
                String l_Name = getColoredSpanned("<b>"+"Yesterday"+"</b>", "#1C8B3B");
                holder.Date.setText(Html.fromHtml(title + " " + l_Name));

                //  holder.lytLine.setVisibility(View.GONE);
                //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
            }else {
                String title = getColoredSpanned("TestDate:- ", "#000000");
                String Name = getColoredSpanned("<b>"+Utils.getDateFormated(MainImageUploadInfoList.get(position).getTest_date())+"</b>", "#5A5C59");
                holder.Date.setText(Html.fromHtml(title + " " + Name));
            }


            if (MainImageUploadInfoList.get(position).getSub_name() != null){
                holder.sub.setText(MainImageUploadInfoList.get(position).getSub_name());
            }else{
                holder.sub.setText("Not Mentioned");
            }

            if(MainImageUploadInfoList.get(position).getTest_file()!= null){
                Picasso.get().load(MainImageUploadInfoList.get(position).getTest_file()).placeholder(context.getResources().getDrawable(R.color.transparent)).into(holder.pdf);
                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdaptercall.methodcall(MainImageUploadInfoList.get(position));
                    }
                });
            }else{
                Picasso.get().load(R.drawable.test).into(holder.pdf);

            }

    }

        @Override
        public int getItemCount () {

        return MainImageUploadInfoList.size();
    }

        private String getColoredSpanned(String text, String color) {
            String input = "<font color=" + color + ">" + text + "</font>";
            return input;
        }

        public void settestList (ArrayList < TestListBean > MainImageUploadInfoList) {
        this.MainImageUploadInfoList = MainImageUploadInfoList;
    }

        static class ViewHolder extends RecyclerView.ViewHolder {

            public TextView Topic,maxMarks,Dur,Date,upldreslttext,upldsuccmsg,fupldsuccmsg,viewresult, sub;
            public ImageView line;
            ImageView pdf;
            public ConstraintLayout withfile;
            CardView card1;
            TextView id;

            public ViewHolder(View itemView) {

                super(itemView);

                sub = (TextView) itemView.findViewById(R.id.subject);
                id = (TextView) itemView.findViewById(R.id.id);
                card1 = (CardView) itemView.findViewById(R.id.card1);
                Topic = (TextView) itemView.findViewById(R.id.TestTopic);
                maxMarks= (TextView) itemView.findViewById(R.id.txtMarks);
                Dur = (TextView) itemView.findViewById(R.id.TestDuration);
                Date = (TextView) itemView.findViewById(R.id.TestDate);
                pdf=(ImageView)itemView.findViewById(R.id.imagefoto);
                line=(ImageView)itemView.findViewById(R.id.imageline);
                withfile=(ConstraintLayout)itemView.findViewById(R.id.withfile);
                upldreslttext=(TextView)itemView.findViewById(R.id.upldreslttext);
                viewresult=(TextView)itemView.findViewById(R.id.viewresult);

            }

        }

        public interface Adaptercall {
            public void methodcall(TestListBean obj);
            public void methodcallresult(TestListBean obj);

        }
    }

