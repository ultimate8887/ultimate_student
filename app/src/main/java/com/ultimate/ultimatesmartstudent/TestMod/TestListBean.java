package com.ultimate.ultimatesmartstudent.TestMod;

import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TestListBean {

    private static String ID="id";
    private static String SUB_ID="sub_id";
    private static String SUB_NAME="sub_name";
    private static String CLASS_ID="class_id";
    private static String TEST_DATE="test_date";
    private static String U_DATE="upload_date";
    private static String DURATION="dur";
    private static String MARKS="marks";
    private static String TOPIC="topic";
    private static String CLASS_NAME="classname";
    private static String TEST_FILE="test_file";
    private static String STATUS="status";
    /**
     * id : 2
     * sub_id : 1
     * sub_name : ENGLISH
     * class_id : 2
     * test_date : 2018-04-04
     * dur : 40
     * marks : 50
     * topic : fifty
     * classname : NURSERY1
     * test_file : office_admin/documents/test_04042018_100445.jpg
     */

    private static String URL="url";


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String url;

    private String id;
    private String sub_id;
    private String sub_name;
    private String class_id;
    private String test_date;
    private String dur;
    private String marks;
    private String topic;
    private String classname;
    private String test_file;
    private String status;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpload_date() {
        return upload_date;
    }

    public void setUpload_date(String upload_date) {
        this.upload_date = upload_date;
    }

    private String upload_date;


    public static ArrayList<TestListBean> parsetestlistArray(JSONArray arrayObj) {
        ArrayList<TestListBean> list = new ArrayList<TestListBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                TestListBean p = parsetestlistObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static TestListBean parsetestlistObject(JSONObject jsonObject) {
        TestListBean msgObj = new TestListBean();
        try {
            if (jsonObject.has(ID) && !jsonObject.getString(ID).isEmpty() && !jsonObject.getString(ID).equalsIgnoreCase("null")) {
                msgObj.setId(jsonObject.getString(ID));
            }


            if (jsonObject.has(URL) && !jsonObject.getString(URL).isEmpty() && !jsonObject.getString(URL).equalsIgnoreCase("null")) {
                msgObj.setUrl(jsonObject.getString(URL));
            }

            if (jsonObject.has(SUB_ID) && !jsonObject.getString(SUB_ID).isEmpty() && !jsonObject.getString(SUB_ID).equalsIgnoreCase("null")) {
                msgObj.setSub_id(jsonObject.getString(SUB_ID));
            }
            if (jsonObject.has(SUB_NAME) && !jsonObject.getString(SUB_NAME).isEmpty() && !jsonObject.getString(SUB_NAME).equalsIgnoreCase("null")) {
                msgObj.setSub_name(jsonObject.getString(SUB_NAME));
            }
            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                msgObj.setClass_id(jsonObject.getString(CLASS_ID));
            }
            if (jsonObject.has(TEST_DATE) && !jsonObject.getString(TEST_DATE).isEmpty() && !jsonObject.getString(TEST_DATE).equalsIgnoreCase("null")) {
                msgObj.setTest_date(jsonObject.getString(TEST_DATE));
            }
            if (jsonObject.has(U_DATE) && !jsonObject.getString(U_DATE).isEmpty() && !jsonObject.getString(U_DATE).equalsIgnoreCase("null")) {
                msgObj.setUpload_date(jsonObject.getString(U_DATE));
            }

            if (jsonObject.has(DURATION) && !jsonObject.getString(DURATION).isEmpty() && !jsonObject.getString(DURATION).equalsIgnoreCase("null")) {
                msgObj.setDur(jsonObject.getString(DURATION));
            }

            if (jsonObject.has(MARKS) && !jsonObject.getString(MARKS).isEmpty() && !jsonObject.getString(MARKS).equalsIgnoreCase("null")) {
                msgObj.setMarks(jsonObject.getString(MARKS));
            }

            if (jsonObject.has(TOPIC) && !jsonObject.getString(TOPIC).isEmpty() && !jsonObject.getString(TOPIC).equalsIgnoreCase("null")) {
                msgObj.setTopic(jsonObject.getString(TOPIC));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                msgObj.setClassname(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase("null")) {
                msgObj.setStatus(jsonObject.getString(STATUS));
            }

            if (jsonObject.has(TEST_FILE) && !jsonObject.getString(TEST_FILE).isEmpty() && !jsonObject.getString(TEST_FILE).equalsIgnoreCase("null")) {
                msgObj.setTest_file(Constants.getImageBaseURL()+jsonObject.getString(TEST_FILE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getTest_date() {
        return test_date;
    }

    public void setTest_date(String test_date) {
        this.test_date = test_date;
    }

    public String getDur() {
        return dur;
    }

    public void setDur(String dur) {
        this.dur = dur;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getTest_file() {
        return test_file;
    }

    public void setTest_file(String test_file) {
        this.test_file = test_file;
    }



}
