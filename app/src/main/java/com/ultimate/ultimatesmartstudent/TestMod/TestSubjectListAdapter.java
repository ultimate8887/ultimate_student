package com.ultimate.ultimatesmartstudent.TestMod;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

public class TestSubjectListAdapter extends BaseAdapter {
    private final ArrayList<TestList> subjectnames;
    Context context;
    LayoutInflater inflter;

    public TestSubjectListAdapter(Context applicationContext, ArrayList<TestList> subjectnames) {
        this.context = applicationContext;
        this.subjectnames = subjectnames;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return subjectnames.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("All");
        } else {
            TestList classObj = subjectnames.get(i - 1);
            label.setText(classObj.getName());
        }

        return view;
    }
}