package com.ultimate.ultimatesmartstudent.TestMod;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Testresultbean {


    private static String ID="id";
    private static String SUBID="sub_id";
    private static String CLASSID="class_id";
    private static String TEST_DATE="test_date";
    private static String STATUS="status";
    private static String MARKS="marks";
    /**
     * id : 96
     * sub_id : 1
     * class_id : 2
     * test_date : 2018-10-25
     * status : inactive
     * marks : 70
     */

    private String id;
    private String sub_id;
    private String class_id;
    private String test_date;
    private String status;
    private String marks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getTest_date() {
        return test_date;
    }

    public void setTest_date(String test_date) {
        this.test_date = test_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public static ArrayList<Testresultbean> parseTstrsltArray(JSONArray arrayObj) {
        ArrayList<Testresultbean> list = new ArrayList<Testresultbean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Testresultbean p = parseTstrsltObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Testresultbean parseTstrsltObject(JSONObject jsonObject) {
        Testresultbean casteObj = new Testresultbean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(SUBID) && !jsonObject.getString(SUBID).isEmpty() && !jsonObject.getString(SUBID).equalsIgnoreCase("null")) {
                casteObj.setSub_id(jsonObject.getString(SUBID));
            }
            if (jsonObject.has(CLASSID) && !jsonObject.getString(CLASSID).isEmpty() && !jsonObject.getString(CLASSID).equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString(CLASSID));
            }
            if (jsonObject.has(TEST_DATE) && !jsonObject.getString(TEST_DATE).isEmpty() && !jsonObject.getString(TEST_DATE).equalsIgnoreCase("null")) {
                casteObj.setTest_date(jsonObject.getString(TEST_DATE));
            }
            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString(STATUS));
            }
            if (jsonObject.has(MARKS) && !jsonObject.getString(MARKS).isEmpty() && !jsonObject.getString(MARKS).equalsIgnoreCase("null")) {
                casteObj.setMarks(jsonObject.getString(MARKS));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }


}
