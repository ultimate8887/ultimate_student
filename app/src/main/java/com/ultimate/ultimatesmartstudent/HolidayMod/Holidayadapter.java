package com.ultimate.ultimatesmartstudent.HolidayMod;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Holidayadapter extends RecyclerView.Adapter<Holidayadapter.Viewholder> {
    Context mContext;
    ArrayList<HolidayBean> holidayList;
    int value=0;
    public Holidayadapter(ArrayList<HolidayBean> holidayList, Context mContext,int value) {
        this.holidayList = holidayList;
        this.mContext = mContext;
        this.value = value;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_holiday_list, parent, false);
        Holidayadapter.Viewholder viewholder = new Holidayadapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holidayadapter.Viewholder holder, int position) {

        if (value==1){
            holder.holiday.setVisibility(View.VISIBLE);
            holder.home.setVisibility(View.GONE);
            if (holidayList.get(position).getId() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned("hd-"+holidayList.get(position).getId(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getTitle() != null) {
                String title = getColoredSpanned("Title: ", "#F4212C");
                String Name = getColoredSpanned(""+holidayList.get(position).getTitle(), "#5A5C59");
                holder.title.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getHoliday_date() != null) {
                String title = getColoredSpanned("Holiday Date: ", "#000000");
                String Name = getColoredSpanned(""+Utils.getDateFormated(holidayList.get(position).getHoliday_date()), "#5A5C59");
                holder.h_date.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getCreated_on() != null) {
                String title = getColoredSpanned("Created-On: ", "#000000");
                String Name = getColoredSpanned(""+ Utils.getDateFormated(holidayList.get(position).getCreated_on()), "#5A5C59");
                holder.createdon.setText(Html.fromHtml(title + " " + Name));
            }

            //  holder.albm_id.setText("Album Id: "+hq_list.get(position).getAlbum_id());
            // holder.title.setText(holidayList.get(position).getTitle());
            // holder.createdon.setText("Created-On: "+ Utils.getDateFormated(holidayList.get(position).getCreated_on()));
        }else{
            holder.holiday.setVisibility(View.GONE);
            holder.home.setVisibility(View.VISIBLE);
            holder.txtHoliday.setText(holidayList.get(position).getDate() + "\n" + holidayList.get(position).getMon());
        }


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return holidayList.size();
    }

    public void setHList(ArrayList<HolidayBean> holidayList) {
        this.holidayList = holidayList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.hol)
        RelativeLayout holiday;

        @BindView(R.id.home)
        RelativeLayout home;

        @BindView(R.id.txtHoliday)
        TextView txtHoliday;

        @BindView(R.id.albm_id)
        TextView albm_id;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.albm_name)
        TextView h_date;

        @BindView(R.id.status)
        TextView createdon;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}