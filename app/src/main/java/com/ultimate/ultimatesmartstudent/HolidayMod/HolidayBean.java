package com.ultimate.ultimatesmartstudent.HolidayMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HolidayBean {

    /**
     * id : 18
     * title : Guru Nanak Dev Ji Birthday
     * holiday_date : 2018-11-23
     * status : Active
     * created_on : 2018-08-14
     */

    private String id;
    private String title;
    private String holiday_date;
    private String status;
    private String created_on;
    private String mon;
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHoliday_date() {
        return holiday_date;
    }

    public void setHoliday_date(String holiday_date) {
        this.holiday_date = holiday_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    //    Now there is implementing all the ApiHandler working.............................

    public static ArrayList<HolidayBean> parseArray(JSONArray arrayObj) {

        ArrayList list = new ArrayList();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                HolidayBean bean = parseObject(arrayObj.getJSONObject(i));
                if (bean != null) {
                    list.add(bean);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static HolidayBean parseObject(JSONObject jsonObject) {
        HolidayBean casteObj = new HolidayBean();
        try {
            if (jsonObject.has("id")) {
                casteObj.setId(jsonObject.getString("id"));
            }

            if (jsonObject.has("holiday_date")) {
                String time = jsonObject.getString("holiday_date");
                casteObj.setHoliday_date(time);
                try {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date newDate = format.parse(time);

                    format = new SimpleDateFormat("MMM");
                    String mon = format.format(newDate);

                    format = new SimpleDateFormat("dd");
                    String date = format.format(newDate);

                    casteObj.setMon(mon);
                    casteObj.setDate(date);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (jsonObject.has("created_on")) {
                casteObj.setCreated_on(jsonObject.getString("created_on"));
            }

            if (jsonObject.has("title")) {
                casteObj.setTitle(jsonObject.getString("title"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return casteObj;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getMon() {
        return mon;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
