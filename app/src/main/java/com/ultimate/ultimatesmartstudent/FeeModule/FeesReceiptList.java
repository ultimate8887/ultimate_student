package com.ultimate.ultimatesmartstudent.FeeModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeesReceiptList extends AppCompatActivity implements FeeReceiptListAdapter.ProdMethodCallBack {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    ArrayList<Fee_Paid_Bean> paid_beanslist=new ArrayList<>();;
    private FeeReceiptListAdapter mAdapter;
    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    CommonProgress commonProgress;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.spinnerMonth)
    Spinner spinnerMonth;
    ArrayList<String> arrayList = new ArrayList<>();
    @BindView(R.id.textView8)
    TextView textView8;
    String tag = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_paid_list_new);
        ButterKnife.bind(this);
        textView8.setText(getString(R.string.f_rec));
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(getString(R.string.view_res));
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeeReceiptListAdapter(paid_beanslist,this, this);
        recyclerView.setAdapter(mAdapter);
        // fetchFeeDetail(selectMonth);
        fetchList();
    }

    private void fetchList() {
        arrayList.add("All");
        arrayList.add("Pending");
        arrayList.add("Complete");
        arrayList.add("Decline");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);
        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                if (arrayList.get(pos).equalsIgnoreCase("Pending")){
                    tag = "pending";
                } else if (arrayList.get(pos).equalsIgnoreCase("Complete")){
                    tag = "complete";
                }else if (arrayList.get(pos).equalsIgnoreCase("Decline")){
                    tag = "failure";
                }else {
                    tag = "";
                }
                fetchReceiptlist(tag);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        fetchReceiptlist(tag);

    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    private void fetchReceiptlist(String tag) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", User.getCurrentUser().getId());
        params.put("check", "receipt");
        if (tag.equalsIgnoreCase("")){
            params.put("month", "All");
        }else{
            params.put("month", tag);
        }

        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                if (paid_beanslist != null) {
                    paid_beanslist.clear();

                }
                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    paid_beanslist = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    if (paid_beanslist.size() > 0) {
                        mAdapter.setMerchantBeans(paid_beanslist);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(paid_beanslist.size()));
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                           totalRecord.setText(getString(R.string.t_entries)+" 0");
                        paid_beanslist.clear();
                        mAdapter.setMerchantBeans(paid_beanslist);
                        mAdapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Log.i("Fee_Paid_Bean", String.valueOf(list.get(0).getEs_particularamount())+ String.valueOf(list.get(0).getId()));
                // Log.i("Fee_Paid_Bean", String.valueOf(jsonObject));
            } else {
                  totalRecord.setText(getString(R.string.t_entries)+" 0");
                paid_beanslist.clear();
                mAdapter.setMerchantBeans(paid_beanslist);
                mAdapter.notifyDataSetChanged();
                 txtNorecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {
        if (fee_paid_bean.getScreenshot()!=null) {
            viewwwwReceipt(fee_paid_bean);
        }else {
            Gson gson = new Gson();
            String prod_data = gson.toJson(fee_paid_bean, Fee_Paid_Bean.class);
            Intent intent = new Intent(FeesReceiptList.this, FeeDetailsActivity.class);
            intent.putExtra("fee_data", prod_data);
            startActivity(intent);
            Animatoo.animateZoom(this);
        }
    }

    private void viewwwwReceipt(Fee_Paid_Bean fee_paid_bean) {

        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView img = (ImageView) dialogLog.findViewById(R.id.img);

        if (fee_paid_bean.getScreenshot()!=null) {
            Picasso.get().load(fee_paid_bean.getScreenshot()).placeholder(R.drawable.logo).into(img);
        }

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();

    }
}