package com.ultimate.ultimatesmartstudent.FeeModule.NewFees;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.FeeModule.Fee_Paid_Bean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeeDetailsActivity_New extends AppCompatActivity {

    @BindView(R.id.imgBack)
    ImageView imgBackmsg;
    String fee_cate_id = "", fromdate = "", todate = "", school="";
    @BindView(R.id.submit)
    TextView submit;
    Animation animation;
    int jan_m_fee = 0, feb_m_fee = 0, mar_m_fee = 0, april_m_fee = 0, may_m_fee = 0, june_m_fee = 0, july_m_fee = 0, aug_m_fee = 0, sep_m_fee = 0, oct_m_fee = 0,
            nov_m_fee = 0, dec_m_fee = 0;

    int jan_m_fee_fine = 0, feb_m_fee_fine = 0, mar_m_fee_fine = 0, april_m_fee_fine = 0, may_m_fee_fine = 0, june_m_fee_fine = 0, july_m_fee_fine = 0,
            aug_m_fee_fine = 0, sep_m_fee_fine = 0, oct_m_fee_fine = 0,
            nov_m_fee_fine = 0, dec_m_fee_fine = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @BindView(R.id.school_address)
    TextView school_address;
    @BindView(R.id.pay_date)
    TextView pay_date;
    @BindView(R.id.pay_rec)
    TextView pay_rec;
    @BindView(R.id.session)
    TextView session;
    @BindView(R.id.class_sec)
    TextView class_sec;
    @BindView(R.id.roll_no)
    TextView roll_no;
    @BindView(R.id.s_name)
    TextView s_name;
    @BindView(R.id.f_name)
    TextView f_name;
    @BindView(R.id.s_add)
    TextView s_add;
    @BindView(R.id.fee_month)
    TextView fee_month;
    @BindView(R.id.tuition)
    TextView tuition;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.discount)
    TextView discount;
    @BindView(R.id.net_amount_paid)
    TextView net_amount_paid;
    @BindView(R.id.amount_paid)
    TextView amount_paid;
    @BindView(R.id.gross_total)
    TextView gross_total;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.new_balance)
    TextView new_balance;

    @BindView(R.id.mode)
    TextView mode;
    @BindView(R.id.collector)
    TextView collector;
    @BindView(R.id.paidby)
    TextView paidby;
    @BindView(R.id.amount_in_word)
    TextView amount_in_word;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    Fee_Paid_Bean data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_details_new);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
         school= User.getCurrentUser().getSchoolData().getName();
        Bundle intent_value = getIntent().getExtras();
        if (intent_value != null) {
            if (intent_value.containsKey("fee_data")) {
                Gson gson = new Gson();
                data = gson.fromJson(intent_value.getString("fee_data"), Fee_Paid_Bean.class);
                fromdate = getIntent().getExtras().getString("fromdate");
                todate = getIntent().getExtras().getString("todate");
                setCustomerData();
            }

        }
    }



    @OnClick(R.id.submit)
    public void submit() {
// create bitmap screen capture
        submit.startAnimation(animation);
        Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
        saveImageToGallery(bitmap);
    }

    //create bitmap from the ScrollView
    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }

    private void saveImageToGallery(Bitmap bitmap){
        long time= System.currentTimeMillis();

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "Receipt_img_saved"+time+".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Receipt export to gallery!!", Toast.LENGTH_SHORT).show();

            }
            else{


                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "Receipt_img_saved"+time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Receipt export to gallery!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);


        }catch(Exception e){

            Toast.makeText(this, "Receipt not export to gallery!! \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }



    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void setCustomerData() {

        User user=User.getCurrentUser();


        txtTitle.setText("Fee Receipt");
        txtSub.setVisibility(View.VISIBLE);
        txtSub.setText(data.getName() + "(" + data.getS_id() + ")");
        school_address.setText(User.getCurrentUser().getSchoolData().getName());

        if (data.getPaymentdate() != null) {
            String title = getColoredSpanned("<b>" +"Date: "+"</b>", "#000000");
            String Name = getColoredSpanned("" + Utils.getDateFormated(data.getPaymentdate())+"", "#383737");
            pay_date.setText(Html.fromHtml(title + " " + Name));
        }else {
            pay_date.setText("Not Mentioned");
        }

        if (data.getId() != null) {
            String title = getColoredSpanned("<b>" +"Receipt No: "+"</b>", "#000000");
            String Name = getColoredSpanned(User.getCurrentUser().getSchoolData().getFi_school_id()+"-"+data.getId()+"", "#383737");
            pay_rec.setText(Html.fromHtml(title + " " + Name));
        }else {
            pay_rec.setText("Not Mentioned");
        }


            String title3 = getColoredSpanned("<b>" +"Session: "+"</b>", "#000000");
            String Name3= getColoredSpanned(fromdate+"-" + todate+"", "#383737");
            session.setText(Html.fromHtml(title3 + " " + Name3));


        if (user.getClass_name() != null) {
            String title = getColoredSpanned("<b>" +"Class & Sec: "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getClass_name()+"-" + user.getSection_name()+"", "#383737");
            class_sec.setText(Html.fromHtml(title + " " + Name));
        }else {
            class_sec.setText("Not Mentioned");
        }



        if (data.getS_id() != null) {
            String title = getColoredSpanned("<b>" +"Admission No: "+"</b>", "#000000");
            String Name = getColoredSpanned("" + data.getS_id()+"", "#383737");
            roll_no.setText(Html.fromHtml(title + " " + Name));
        }else {
            roll_no.setText("Not Mentioned");
        }


        if (data.getName() != null) {
            String title = getColoredSpanned("<b>" +"Student Name: "+"</b>", "#000000");
            String Name = getColoredSpanned("" + data.getName()+"", "#383737");
            s_name.setText(Html.fromHtml(title + " " + Name));
        }else {
            s_name.setText("Not Mentioned");
        }

        if (data.getFather_name() != null) {
            String title = getColoredSpanned("<b>" +"Father Name: "+"</b>", "#000000");
            String Name = getColoredSpanned("" + data.getFather_name()+"", "#383737");
            f_name.setText(Html.fromHtml(title + " " + Name));
        }else {
            f_name.setText("Not Mentioned");
        }

        if (user.getAddress() != null) {
            String title = getColoredSpanned("<b>" +"Address: "+"</b>", "#000000");
            String Name = getColoredSpanned("" + user.getAddress()+"", "#383737");
            s_add.setText(Html.fromHtml(title + " " + Name));
        }else {
            s_add.setText("Not Mentioned");
        }

        String Nameeee="";
        if (data.getEs_months() != null) {
            String str = data.getEs_months();
            int n = str.length();
            char last = str.charAt(n - 1);
            String value=String.valueOf(last);
            if (value.equalsIgnoreCase(",")){
                String substring = str.substring(0, str.length() - 1); // AB
                String replaced = substring + "";
                Nameeee = getColoredSpanned("" +replaced+".", "#383737");
            }else{
                Nameeee = getColoredSpanned("" +str+".", "#383737");
            }
            String title = getColoredSpanned("<b>" +"Fee of: "+"</b>", "#000000");

            fee_month.setText(Html.fromHtml(title + " " + Nameeee));
        }else {
            fee_month.setText("Not Mentioned");
        }


        if (data.getJan_month().equalsIgnoreCase("yes")){
            jan_m_fee = Integer.parseInt(data.getJan_paid());
            jan_m_fee_fine = Integer.parseInt(data.getJan_fine());

        }else{
            jan_m_fee = 0;
            jan_m_fee_fine = 0;
        }

        if (data.getFeb_month().equalsIgnoreCase("yes")){
            feb_m_fee = Integer.parseInt(data.getFeb_paid());
            feb_m_fee_fine = Integer.parseInt(data.getFeb_fine());
        }else{
            feb_m_fee = 0;
            feb_m_fee_fine = 0;
        }

        if (data.getMar_month().equalsIgnoreCase("yes")){
            mar_m_fee = Integer.parseInt(data.getMar_paid());
            mar_m_fee_fine = Integer.parseInt(data.getMar_fine());
        }else{
            mar_m_fee = 0;
            mar_m_fee_fine = 0;
        }

        if (data.getApr_month().equalsIgnoreCase("yes")){
            april_m_fee = Integer.parseInt(data.getApr_paid());
            april_m_fee_fine = Integer.parseInt(data.getApr_fine());
        }else{
            april_m_fee = 0;
            april_m_fee_fine = 0;
        }

        if (data.getMay_month().equalsIgnoreCase("yes")){
            may_m_fee = Integer.parseInt(data.getMay_paid());
            may_m_fee_fine = Integer.parseInt(data.getMay_fine());
        }else{
            may_m_fee = 0;
            may_m_fee_fine = 0;
        }

        if (data.getJune_month().equalsIgnoreCase("yes")){
            june_m_fee = Integer.parseInt(data.getJune_paid());
            june_m_fee_fine = Integer.parseInt(data.getJune_fine());
        }else{
            june_m_fee = 0;
            june_m_fee_fine = 0;
        }

        if (data.getJuly_month().equalsIgnoreCase("yes")){
            july_m_fee = Integer.parseInt(data.getJuly_paid());
            july_m_fee_fine = Integer.parseInt(data.getJuly_fine());
        }else{
            july_m_fee = 0;
            july_m_fee_fine = 0;
        }

        if (data.getAug_month().equalsIgnoreCase("yes")){
            aug_m_fee = Integer.parseInt(data.getAug_paid());
            aug_m_fee_fine = Integer.parseInt(data.getAug_fine());
        }else{
            aug_m_fee = 0;
            aug_m_fee_fine = 0;
        }

        if (data.getSep_month().equalsIgnoreCase("yes")){
            sep_m_fee = Integer.parseInt(data.getSep_paid());
            sep_m_fee_fine = Integer.parseInt(data.getSep_fine());
        }else{
            sep_m_fee = 0;
            sep_m_fee_fine = 0;
        }

        if (data.getOct_month().equalsIgnoreCase("yes")){
            oct_m_fee = Integer.parseInt(data.getOct_paid());
            oct_m_fee_fine = Integer.parseInt(data.getOct_fine());
        }else{
            oct_m_fee = 0;
            oct_m_fee_fine = 0;
        }

        if (data.getNov_month().equalsIgnoreCase("yes")){
            nov_m_fee = Integer.parseInt(data.getNov_paid());
            nov_m_fee_fine = Integer.parseInt(data.getNov_fine());
        }else{
            nov_m_fee = 0;
            nov_m_fee_fine = 0;
        }

        if (data.getDec_month().equalsIgnoreCase("yes")){
            dec_m_fee = Integer.parseInt(data.getDec_paid());
            dec_m_fee_fine = Integer.parseInt(data.getDec_fine());
        }else{
            dec_m_fee = 0;
            dec_m_fee_fine = 0;
        }


        int tuition_total=0,gross_totalsss=0,old_balance=0,net_paid=0;
        gross_totalsss= Integer.parseInt(data.getEs_totalamount());
        old_balance= Integer.parseInt(data.getLast_fee_bal());
        net_paid= Integer.parseInt(data.getEs_particularamount());
        tuition_total=gross_totalsss-old_balance;


        if (tuition_total != 0) {
            String title = getColoredSpanned("" +""+"", "#000000");
            String Name = getColoredSpanned("" + String.valueOf(tuition_total)+".00", "#000000");
            tuition.setText(Html.fromHtml(title + " " + Name));
        }else {
            tuition.setText("Not Mentioned");
        }

        if (data.getLast_fee_bal() != null) {
            String title = getColoredSpanned("" +""+"", "#000000");
            String Name = getColoredSpanned("" + data.getLast_fee_bal()+".00", "#000000");
            balance.setText(Html.fromHtml(title + " " + Name));
        }else {
            balance.setText("Not Mentioned");
        }


        if (data.getEs_totalamount() != null) {
            String title = getColoredSpanned("" +""+"", "#000000");
            String Name = getColoredSpanned("" + data.getEs_totalamount()+".00", "#000000");
            total.setText(Html.fromHtml(title + " " + Name));
            gross_total.setText(data.getEs_totalamount()+".00");
        }else {
            gross_total.setText("Not Mentioned");
        }


        if (data.getEs_concessionamount() != null) {
            String title = getColoredSpanned("" +""+"", "#000000");
            String Name = getColoredSpanned("" + data.getEs_concessionamount()+".00", "#000000");
            discount.setText(Html.fromHtml(title + " " + Name));
        }else {
            discount.setText("Not Mentioned");
        }

        if (data.getEs_particularamount() != null) {
            String title = getColoredSpanned("" +""+"", "#000000");
            String Name = getColoredSpanned("" + data.getEs_particularamount()+".00", "#000000");
            amount_paid.setText(data.getEs_particularamount()+".00");
            net_amount_paid.setText(Html.fromHtml(title + " " + Name));
        }else {
            net_amount_paid.setText("Not Mentioned");
            amount_paid.setText("Not Mentioned");
        }


        if (data.getFee_bal() != null) {
            String title = getColoredSpanned("" +""+"", "#000000");
            String Name = getColoredSpanned("" + data.getFee_bal()+".00", "#000000");
            new_balance.setText(Html.fromHtml(title + " " + Name));
        }else {
            new_balance.setText("Not Mentioned");
        }

        if (data.getPayment_mode() != null) {
            String title = getColoredSpanned("" +"Mode: "+"", "#000000");
            String Name = getColoredSpanned("" + data.getPayment_mode()+" Payment", "#383737");
            mode.setText(Html.fromHtml(title + " " + Name));
        }else {
            mode.setText("Not Mentioned");
        }

        if (data.getPayment_mode() != null) {
            String title = getColoredSpanned("" +"Paid By: "+"", "#000000");
            String Name = getColoredSpanned("" + "Parent"+"", "#383737");
            paidby.setText(Html.fromHtml(title + " " + Name));
        }else {
            paidby.setText("Not Mentioned");
        }

        if (data.getPayment_mode() != null) {
            String title = getColoredSpanned("" +"Collector: "+"", "#000000");
            String Name = getColoredSpanned("" + "Admin"+"", "#383737");
            collector.setText(Html.fromHtml(title + " " + Name));
        }else {
            collector.setText("Not Mentioned");
        }


        String return_val_in_english =   EnglishNumberToWords.convert(net_paid).toUpperCase();
        //return_val_in_english.toUpperCase();

      //  amount_in_word.setText(return_val_in_english);

        if (data.getEs_particularamount() != null) {
            String title = getColoredSpanned("" +"Amount in word: "+"", "#383737");
            String Name = getColoredSpanned("" + return_val_in_english +" RUPEES.", "#000000");
            amount_in_word.setText(Html.fromHtml(title + " " + Name));
        }else {
            amount_in_word.setText("Not Mentioned");
        }

    }


    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }
}