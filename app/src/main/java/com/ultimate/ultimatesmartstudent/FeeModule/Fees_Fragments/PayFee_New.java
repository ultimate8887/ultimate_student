package com.ultimate.ultimatesmartstudent.FeeModule.Fees_Fragments;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.chrisbanes.photoview.PhotoView;

import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartstudent.BeanModule.SessionBean;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.FeeModule.ExtraDetails;
import com.ultimate.ultimatesmartstudent.FeeModule.FeeBalanceBean;
import com.ultimate.ultimatesmartstudent.FeeModule.NewFees.CommonFeesListActivity;
import com.ultimate.ultimatesmartstudent.FeeModule.NewFees.Particular_Adapter_New;
import com.ultimate.ultimatesmartstudent.FeeModule.ParticularBean;
import com.ultimate.ultimatesmartstudent.ForCamera.IPickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartstudent.ForCamera.PickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickSetup;
import com.ultimate.ultimatesmartstudent.Leave_Mod.LeaveActivity;
import com.ultimate.ultimatesmartstudent.Messages.All_Activity.ComposeStaffMsg;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayFee_New extends AppCompatActivity implements IPickResult {

    ArrayList<ExtraDetails> extraDetails = new ArrayList<>();
    String fee_patricular = "",fee_patricular1 = "";
    CheckBox cb_april, cb_may, cb_june, cb_july, cb_aug, cb_sep, cb_oct, cb_nov, cb_dec, cb_jan, cb_feb, cb_march;
    TextView cb_april_p, cb_may_p, cb_june_p, cb_july_p, cb_aug_p, cb_sep_p, cb_oct_p, cb_nov_p, cb_dec_p, cb_jan_p, cb_feb_p, cb_march_p;
    TextView cb_april_y, cb_may_y, cb_june_y, cb_july_y, cb_aug_y, cb_sep_y, cb_oct_y, cb_nov_y, cb_dec_y, cb_jan_y, cb_feb_y, cb_march_y;
    Button btnCancel, btnSubmit;
    TextView h_currentday, h_day_name, h_currentMonth, text;
    private Calendar mCalendar;
    int day;
    int day_name;
    @BindView(R.id.fine_close)
    LinearLayout fine_close;

    @BindView(R.id.fine_balance)
    EditText fine_balance;
    @BindView(R.id.today_fine)
    EditText today_fine;
    @BindView(R.id.paid_fine)
    EditText paid_fine;

    @BindView(R.id.p_amount)
    EditText p_amount;
    @BindView(R.id.t_amount)
    EditText t_amount;
    @BindView(R.id.c_amount)
    EditText c_amount;
    @BindView(R.id.g_total)
    EditText g_total;


    @BindView(R.id.total_fine)
    EditText total_fine;
    @BindView(R.id.new_fine_balance)
    EditText new_fine_balance;
    @BindView(R.id.new_fee_balance)
    EditText new_fee_balance;

    @BindView(R.id.edtReason)
    EditText edtReason;


    @BindView(R.id.b_name)
    EditText b_name;
    @BindView(R.id.b_a_no)
    EditText b_a_no;
    @BindView(R.id.cheque)
    EditText cheque;

    @BindView(R.id.paying_amount)
    TextView paying_amount;
    @BindView(R.id.p)
    TextView p;
    // All RecyclerView
    @BindView(R.id.april_recycler_view)
    RecyclerView april_recycler_view;
    @BindView(R.id.may_recycler_view)
    RecyclerView may_recycler_view;
    @BindView(R.id.jun_recycler_view)
    RecyclerView jun_recycler_view;
    @BindView(R.id.july_recycler_view)
    RecyclerView july_recycler_view;
    @BindView(R.id.aug_recycler_view)
    RecyclerView aug_recycler_view;
    @BindView(R.id.sep_recycler_view)
    RecyclerView sep_recycler_view;
    @BindView(R.id.oct_recycler_view)
    RecyclerView oct_recycler_view;
    @BindView(R.id.nov_recycler_view)
    RecyclerView nov_recycler_view;
    @BindView(R.id.dec_recycler_view)
    RecyclerView dec_recycler_view;
    @BindView(R.id.jan_recycler_view)
    RecyclerView jan_recycler_view;
    @BindView(R.id.feb_recycler_view)
    RecyclerView feb_recycler_view;
    @BindView(R.id.mar_recycler_view)
    RecyclerView mar_recycler_view;
    LinearLayoutManager layoutManager;
    private Particular_Adapter_New newsAdapter, newsAdapter1,
            newsAdapter2, newsAdapter3, newsAdapter4, newsAdapter5,
            newsAdapter6, newsAdapter7, newsAdapter8, newsAdapter9,
            newsAdapter10, newsAdapter11;
    ArrayList<ParticularBean> classList1 = new ArrayList<>();
    ArrayList<ParticularBean> classList2 = new ArrayList<>();
    ArrayList<ParticularBean> classList3 = new ArrayList<>();
    ArrayList<ParticularBean> classList4 = new ArrayList<>();
    ArrayList<ParticularBean> classList5 = new ArrayList<>();
    ArrayList<ParticularBean> classList6 = new ArrayList<>();
    ArrayList<ParticularBean> classList7 = new ArrayList<>();
    ArrayList<ParticularBean> classList8 = new ArrayList<>();
    ArrayList<ParticularBean> classList9 = new ArrayList<>();
    ArrayList<ParticularBean> classList10 = new ArrayList<>();
    ArrayList<ParticularBean> classList11 = new ArrayList<>();
    ArrayList<ParticularBean> classList12 = new ArrayList<>();


    ArrayList<FeeBalanceBean> list = new ArrayList<>();
    // All EditText
    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtFather)
    EditText edtFather;
    @BindView(R.id.edtRegistration)
    EditText edtRegistration;

    // All ImageView
    @BindView(R.id.dp)
    ImageView dp;
    @BindView(R.id.textView7)
    TextView textView7;
    @BindView(R.id.textView8)
    TextView textView8;
    @BindView(R.id.classlayout3)
    LinearLayout classlayout3;
    @BindView(R.id.student_lyt)
    LinearLayout student_lyt;
    @BindView(R.id.top)
    LinearLayout top;
    @BindView(R.id.month_lyt)
    LinearLayout month_lyt;
    @BindView(R.id.bottom)
    LinearLayout bottom;
    @BindView(R.id.view_std)
    TextView view_std;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.totals)
    TextView totals;
    @BindView(R.id.fine_view)
    TextView fine_view;
    @BindView(R.id.pay_view)
    TextView pay_view;
    @BindView(R.id.pay_close)
    LinearLayout pay_close;
    @BindView(R.id.remarklyt)
    LinearLayout remarklyt;
    @BindView(R.id.a_sub)
    TextView a_sub;
    @BindView(R.id.f_sub)
    TextView f_sub;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @BindView(R.id.textNorecord)
    TextView textNorecord;
    @BindView(R.id.contact_support)
    ImageView contact_support;
    SharedPreferences sharedPreferences;
    ArrayList<String> monthList = new ArrayList<>();
    ArrayList<String> bankList = new ArrayList<>();
    String selectMonth = "";
    @BindView(R.id.bank_lyt)
    LinearLayout bank_lyt;
    @BindView(R.id.spinner)
    Spinner bank_spinner;

    @BindView(R.id.online_lyt)
    CardView online_lyt;

    @BindView(R.id.add)
    ImageButton add;

    String selectBankType = "", name = "", f_name = "", roll = "",
            image = "", gender = "", regist = "", classid = "",
            fee_cate_id = "", fromdate = "", todate = "", class_name = "", place_id = "", route_id = "", month_name = "", s_jan = "", s_feb = "", s_mar = "", s_april = "", s_may = "", s_jun = "", s_july = "", s_aug = "", s_sep = "", s_oct = "", s_nov = "", s_dec = "";

    String jan_m = "", feb_m = "", mar_m = "", april_m = "", may_m = "", june_m = "", july_m = "", aug_m = "", sep_m = "", oct_m = "", nov_m = "", dec_m = "";
    int jan_m_fee = 0, feb_m_fee = 0, mar_m_fee = 0, april_m_fee = 0, may_m_fee = 0, june_m_fee = 0, july_m_fee = 0, aug_m_fee = 0, sep_m_fee = 0, oct_m_fee = 0,
            nov_m_fee = 0, dec_m_fee = 0;

    int jan_m_fee_fine = 0, feb_m_fee_fine = 0, mar_m_fee_fine = 0, april_m_fee_fine = 0, may_m_fee_fine = 0, june_m_fee_fine = 0, july_m_fee_fine = 0,
            aug_m_fee_fine = 0, sep_m_fee_fine = 0, oct_m_fee_fine = 0,
            nov_m_fee_fine = 0, dec_m_fee_fine = 0;

    int jan_m_fee_fine1 = 0, feb_m_fee_fine1 = 0, mar_m_fee_fine1 = 0, april_m_fee_fine1 = 0, may_m_fee_fine1 = 0, june_m_fee_fine1 = 0, july_m_fee_fine1 = 0,
            aug_m_fee_fine1 = 0, sep_m_fee_fine1 = 0, oct_m_fee_fine1 = 0,
            nov_m_fee_fine1 = 0, dec_m_fee_fine1 = 0;

    Dialog dialog;
    int check = 0, click = 0;
    int totalPrice = 0, totalPrice1 = 0, trans_amount = 0, p_balance = 0, p_fine_balance = 0, new_totalPrice = 0, sub_totalPrice = 0, fine_totalPrice = 0, totalPrice_withoutTrans = 0, selection = 0;
    int p_value = 0, t_value = 0, c_value = 0, total_value = 0, paid_fine_value = 0, today_add_fine = 0, new_f_balance = 0, new_total_fine = 0, new_feess_balance = 0;
    CommonProgress commonProgress;
    private Bitmap bitmap;
    @BindView(R.id.imgShow)
    PhotoView imgShow;
    Animation animation;
    @BindView(R.id.speak_text)
    ImageView speak_text;
    private static final int REQUEST_CODE_SPEECH_INPUT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_fee_new);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        month_lyt.setVisibility(View.VISIBLE);

        speak_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent
                        = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                        Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak to text");

                try {
                    startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT);
                }
                catch (Exception e) {
                    Toast
                            .makeText(PayFee_New.this, " " + e.getMessage(),
                                    Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });

        textView7.setText("Fees Pending/Paid Month details");
        p.setText("Total Pending Fees");
        //CheckBox
        cb_jan = (CheckBox) findViewById(R.id.cb_jan);
        cb_feb = (CheckBox) findViewById(R.id.cb_feb);
        cb_march = (CheckBox) findViewById(R.id.cb_march);
        cb_april = (CheckBox) findViewById(R.id.cb_april);
        cb_may = (CheckBox) findViewById(R.id.cb_may);
        cb_june = (CheckBox) findViewById(R.id.cb_june);
        cb_july = (CheckBox) findViewById(R.id.cb_july);
        cb_aug = (CheckBox) findViewById(R.id.cb_aug);
        cb_sep = (CheckBox) findViewById(R.id.cb_sep);
        cb_oct = (CheckBox) findViewById(R.id.cb_oct);
        cb_nov = (CheckBox) findViewById(R.id.cb_nov);
        cb_dec = (CheckBox) findViewById(R.id.cb_dec);

        //TextView
        cb_jan_p = (TextView) findViewById(R.id.cb_jan_p);
        cb_feb_p = (TextView) findViewById(R.id.cb_feb_p);
        cb_march_p = (TextView) findViewById(R.id.cb_march_p);
        cb_april_p = (TextView) findViewById(R.id.cb_april_p);
        cb_may_p = (TextView) findViewById(R.id.cb_may_p);
        cb_june_p = (TextView) findViewById(R.id.cb_june_p);
        cb_july_p = (TextView) findViewById(R.id.cb_july_p);
        cb_aug_p = (TextView) findViewById(R.id.cb_aug_p);
        cb_sep_p = (TextView) findViewById(R.id.cb_sep_p);
        cb_oct_p = (TextView) findViewById(R.id.cb_oct_p);
        cb_nov_p = (TextView) findViewById(R.id.cb_nov_p);
        cb_dec_p = (TextView) findViewById(R.id.cb_dec_p);

        cb_jan_y = (TextView) findViewById(R.id.cb_jan_y);
        cb_feb_y = (TextView) findViewById(R.id.cb_feb_y);
        cb_march_y = (TextView) findViewById(R.id.cb_march_y);
        cb_april_y = (TextView) findViewById(R.id.cb_april_y);
        cb_may_y = (TextView) findViewById(R.id.cb_may_y);
        cb_june_y = (TextView) findViewById(R.id.cb_june_y);
        cb_july_y = (TextView) findViewById(R.id.cb_july_y);
        cb_aug_y = (TextView) findViewById(R.id.cb_aug_y);
        cb_sep_y = (TextView) findViewById(R.id.cb_sep_y);
        cb_oct_y = (TextView) findViewById(R.id.cb_oct_y);
        cb_nov_y = (TextView) findViewById(R.id.cb_nov_y);
        cb_dec_y = (TextView) findViewById(R.id.cb_dec_y);
        text = (TextView) findViewById(R.id.text);

        name = User.getCurrentUser().getFirstname()+" "+User.getCurrentUser().getLastname();
        f_name = User.getCurrentUser().getFather();
        image = User.getCurrentUser().getProfile();
        gender = User.getCurrentUser().getGender();
        roll = User.getCurrentUser().getId();
        regist = User.getCurrentUser().getId();
        classid = User.getCurrentUser().getClass_id();
        class_name = User.getCurrentUser().getClass_name();

        fetchSession();


        //ParticularBean
        april_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter = new Particular_Adapter_New(classList1, PayFee_New.this, getString(R.string.april));
        april_recycler_view.setAdapter(newsAdapter);
//
        may_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter1 = new Particular_Adapter_New(classList2, PayFee_New.this, getString(R.string.may));
        may_recycler_view.setAdapter(newsAdapter1);
//
        jun_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter2 = new Particular_Adapter_New(classList3, PayFee_New.this, getString(R.string.jun));
        jun_recycler_view.setAdapter(newsAdapter2);

        july_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter3 = new Particular_Adapter_New(classList4, PayFee_New.this, getString(R.string.jul));
        july_recycler_view.setAdapter(newsAdapter3);

        aug_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter4 = new Particular_Adapter_New(classList5, PayFee_New.this, getString(R.string.aug));
        aug_recycler_view.setAdapter(newsAdapter4);

        sep_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter5 = new Particular_Adapter_New(classList6, PayFee_New.this, getString(R.string.sep));
        sep_recycler_view.setAdapter(newsAdapter5);

        oct_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter6 = new Particular_Adapter_New(classList7, PayFee_New.this, getString(R.string.oct));
        oct_recycler_view.setAdapter(newsAdapter6);

        nov_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter7 = new Particular_Adapter_New(classList8, PayFee_New.this, getString(R.string.nov));
        nov_recycler_view.setAdapter(newsAdapter7);

        dec_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter8 = new Particular_Adapter_New(classList9, PayFee_New.this, getString(R.string.dec));
        dec_recycler_view.setAdapter(newsAdapter8);

        jan_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter9 = new Particular_Adapter_New(classList10, PayFee_New.this, getString(R.string.jan));
        jan_recycler_view.setAdapter(newsAdapter9);

        feb_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter10 = new Particular_Adapter_New(classList11, PayFee_New.this, getString(R.string.feb));
        feb_recycler_view.setAdapter(newsAdapter10);

        mar_recycler_view.setLayoutManager(new LinearLayoutManager(PayFee_New.this));
        newsAdapter11 = new Particular_Adapter_New(classList12, PayFee_New.this, getString(R.string.mar));
        mar_recycler_view.setAdapter(newsAdapter11);

        if (!s_april.equalsIgnoreCase("")) {
            cb_april.setChecked(true);
            cb_april.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setAprildata();
        }
        cb_april.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_april.isChecked()) {
                            cb_april.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_april_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_april.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_april_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });

        if (!s_may.equalsIgnoreCase("")) {
            cb_may.setChecked(true);
            cb_may.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setMaydata();
        }
        cb_may.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_may.isChecked()) {
                            cb_may.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_may_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_may.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_may_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_jun.equalsIgnoreCase("")) {
            cb_june.setChecked(true);
            cb_june.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setJunedata();
        }
        cb_june.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_june.isChecked()) {
                            cb_june.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_june_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_june.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_june_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_july.equalsIgnoreCase("")) {
            cb_july.setChecked(true);
            cb_july.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setjulydata();
        }

        cb_july.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_july.isChecked()) {

                            cb_july.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_july_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_july.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_july_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_aug.equalsIgnoreCase("")) {
            cb_aug.setChecked(true);
            cb_aug.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setAugdata();
        }

        cb_aug.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_aug.isChecked()) {

                            cb_aug.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_aug_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_aug.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_aug_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_sep.equalsIgnoreCase("")) {
            cb_sep.setChecked(true);
            cb_sep.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setSepdata();
        }
        cb_sep.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_sep.isChecked()) {

                            cb_sep.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_sep_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_sep.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_sep_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_oct.equalsIgnoreCase("")) {
            cb_oct.setChecked(true);
            cb_oct.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setOctdata();
        }

        cb_oct.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_oct.isChecked()) {

                            cb_oct.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_oct_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_oct.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_oct_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_nov.equalsIgnoreCase("")) {
            cb_nov.setChecked(true);
            cb_nov.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setNovdata();

        }
        cb_nov.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_nov.isChecked()) {

                            cb_nov.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_nov_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_nov.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_nov_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_dec.equalsIgnoreCase("")) {
            cb_dec.setChecked(true);
            cb_dec.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setDecdata();
        }
        cb_dec.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_dec.isChecked()) {

                            cb_dec.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_dec_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_dec.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_dec_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_jan.equalsIgnoreCase("")) {
            cb_jan.setChecked(true);
            cb_jan.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setJandata();
        }
        cb_jan.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_jan.isChecked()) {

                            cb_jan.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_jan_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_jan.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_jan_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_feb.equalsIgnoreCase("")) {
            cb_feb.setChecked(true);
            cb_feb.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            setFebdata();
        }
        cb_feb.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_feb.isChecked()) {

                            cb_feb.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_feb_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_feb.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_feb_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        if (!s_mar.equalsIgnoreCase("")) {
            cb_march.setChecked(true);
            cb_march.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_march_p.setVisibility(View.VISIBLE);
            setMardata();
        }
        cb_march.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        click++;
                        if (cb_march.isChecked()) {

                            cb_march.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                            cb_march_y.setTextColor(
                                    getResources().getColor(
                                            R.color.dark_leave));
                        } else {
                            cb_march.setTextColor(
                                    getResources().getColor(
                                            R.color.black));
                            cb_march_y.setTextColor(
                                    getResources().getColor(
                                            R.color.new_red));
                        }
                        setData();
                    }
                });


        p_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!p_amount.getText().toString().isEmpty()) {
                    p_value = Integer.parseInt(p_amount.getText().toString());
                } else {
                    p_value = 0;
                    p_amount.setHint("0");
                    if (check == 0) {
                        showOnce();
                    }
                }

                setTotalvalues("p_amount");
            }
        });

        t_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!t_amount.getText().toString().isEmpty()) {
                    t_value = Integer.parseInt(t_amount.getText().toString());
                } else {
                    t_value = 0;
                    t_amount.setHint("0");
                    if (check == 0) {
                        showOnce();
                    }

                }


                setTotalvalues("t_amount");
            }
        });

        c_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (!c_amount.getText().toString().isEmpty()) {
                    c_value = Integer.parseInt(c_amount.getText().toString());
                } else {
                    c_value = 0;
                    c_amount.setHint("0");
                    if (check == 0) {
                        showOnce();
                    }
                }
                setTotalvalues("c_amount");
            }
        });

        today_fine.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (!today_fine.getText().toString().isEmpty()) {
                    today_add_fine = Integer.parseInt(today_fine.getText().toString());
                } else {
                    today_add_fine = 0;
                    today_fine.setHint("0");
                    if (check == 0) {
                        showOnce();
                    }
                }
                setTotalfinevalues();
            }
        });

        paid_fine.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (!paid_fine.getText().toString().isEmpty()) {
                    paid_fine_value = Integer.parseInt(paid_fine.getText().toString());
                } else {
                    paid_fine_value = 0;
                    paid_fine.setHint("0");
                    if (check == 0) {
                        showOnce();
                    }

                }
                setTotalfinevalues();
            }
        });
        fetchBankType();

    }

    @OnClick(R.id.add)
    public void add() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            commonCode();
        }
    }

    private void commonCode() {
        add.startAnimation(animation);
        pick_img();
        //  assignData();
//        ImagePicker.Companion.with(PayFee_New.this)
//                .galleryOnly()
//                .crop()	    			//Crop image(Optional), Check Customization for more option
//                .compress(1024)			//Final image size will be less than 1 MB(Optional)
//                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
//                .start();
    }


    private void pick_img() {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        //  Log.e("imageView6", String.valueOf(r));
        if (r.getError() == null) {
            //If you want the Bitmap.
            imgShow.setImageBitmap(r.getBitmap());
            bitmap= r.getBitmap();
            imgShow.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                add.setImageDrawable(getResources().getDrawable(R.drawable.ic_done_gr, getApplicationContext().getTheme()));
            } else {
                add.setImageDrawable(getResources().getDrawable(R.drawable.ic_done_gr));
            }
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    @OnClick(R.id.online_lyt)
    public void online_lyt() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            commonCode();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(
                        RecognizerIntent.EXTRA_RESULTS);
                edtReason.setText(
                        Objects.requireNonNull(result).get(0));
            }
        }
    }


    @OnClick(R.id.submit)
    public void submit() {
        if (month_name.equalsIgnoreCase("") || month_name.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Kindly Select Fees Pay Month", Toast.LENGTH_LONG).show();
        } else {

            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            //builder1.setTitle("Confirmation Message!");
            builder1.setMessage("Are you sure, you want to proceed? ");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Toast.makeText(getApplicationContext(), month_name, Toast.LENGTH_LONG).show();
                            savefeesdata();
                            dialogInterface.dismiss();
                        }
                    });
            builder1.setNegativeButton(
                    "No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();


        }
    }

    private void savefeesdata() {

        ErpProgress.showProgressBar(this, "Please wait, fees submission takes few seconds.....");
        HashMap<String, String> params = new HashMap<>();

        if (p_amount.getText().toString().isEmpty()) {
            params.put("particular_amount", "0");
        } else {
            params.put("particular_amount", p_amount.getText().toString());
        }

        params.put("last_fee_bal", balance.getText().toString());
        params.put("total_amount", totals.getText().toString());
        params.put("fee_bal", new_fee_balance.getText().toString());


        if (c_amount.getText().toString().isEmpty()) {
            params.put("concession_amount", "0");
        } else {
            params.put("concession_amount", c_amount.getText().toString());
        }

        if (bitmap != null) {
            String encoded = Utils.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 50);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image", encoded);
        }
        params.put("classid", classid);

        params.put("payment_mode", selectBankType);

        params.put("bank_name", b_name.getText().toString());
        params.put("acc_no", b_a_no.getText().toString());
        params.put("dd_chk_no", cheque.getText().toString());


        if (edtReason.getText().toString().isEmpty()) {
            params.put("note", "Fees Paid Successfully");
        } else {
            params.put("note", edtReason.getText().toString());
        }

        params.put("todate", todate);
        params.put("fromdate", fromdate);

        params.put("es_months", month_name);
        params.put("student_id", regist);


        if (april_m.equalsIgnoreCase("yes")) {
            params.put("cb_april", "paid");
        }else{
            if (cb_april.isChecked()) {
                params.put("cb_april", "yes");
                params.put("fee_patricular", fee_patricular);
                params.put("cb_april_fees", String.valueOf(april_m_fee));
                params.put("cb_april_fees_fine", String.valueOf(april_m_fee_fine));
            } else {
                params.put("cb_april", "no");
                params.put("cb_april_fees", "0");
            }
        }


        if (may_m.equalsIgnoreCase("yes")) {
            params.put("cb_may", "paid");
        }else{
            if (cb_may.isChecked()) {
                params.put("cb_may", "yes");
                params.put("cb_may_fees", String.valueOf(may_m_fee));
                params.put("cb_may_fees_fine", String.valueOf(may_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_may", "no");
                params.put("cb_may_fees", "0");
            }
        }

        if (june_m.equalsIgnoreCase("yes")) {
            params.put("cb_june", "paid");
        }else{
            if (cb_june.isChecked()) {
                params.put("cb_june", "yes");
                params.put("cb_june_fees", String.valueOf(june_m_fee));
                params.put("cb_june_fees_fine", String.valueOf(june_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_june", "no");
                params.put("cb_june_fees", "0");
            }
        }

        if (july_m.equalsIgnoreCase("yes")) {
            params.put("cb_july", "paid");
        }else{
            if (cb_july.isChecked()) {
                params.put("cb_july", "yes");
                params.put("cb_july_fees", String.valueOf(july_m_fee));
                params.put("cb_july_fees_fine", String.valueOf(july_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_july", "no");
                params.put("cb_july_fees", "0");
            }
        }

        if (aug_m.equalsIgnoreCase("yes")) {
            params.put("cb_aug", "paid");
        }else{
            if (cb_aug.isChecked()) {
                params.put("cb_aug", "yes");
                params.put("cb_aug_fees", String.valueOf(aug_m_fee));
                params.put("cb_aug_fees_fine", String.valueOf(aug_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_aug", "no");
                params.put("cb_aug_fees", "0");
            }
        }

        if (sep_m.equalsIgnoreCase("yes")) {
            params.put("cb_sep", "paid");
        }else{
            if (cb_sep.isChecked()) {
                params.put("cb_sep", "yes");
                params.put("cb_sep_fees", String.valueOf(sep_m_fee));
                params.put("cb_sep_fees_fine", String.valueOf(sep_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_sep", "no");
                params.put("cb_sep_fees", "0");
            }
        }

        if (oct_m.equalsIgnoreCase("yes")) {
            params.put("cb_oct", "paid");
        }else{
            if (cb_oct.isChecked()) {
                params.put("cb_oct", "yes");
                params.put("cb_oct_fees", String.valueOf(oct_m_fee));
                params.put("cb_oct_fees_fine", String.valueOf(oct_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_oct", "no");
                params.put("cb_oct_fees", "0");
            }
        }

        if (nov_m.equalsIgnoreCase("yes")) {
            params.put("cb_nov", "paid");
        }else{
            if (cb_nov.isChecked()) {
                params.put("cb_nov", "yes");
                params.put("cb_nov_fees", String.valueOf(nov_m_fee));
                params.put("cb_nov_fees_fine", String.valueOf(nov_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_nov", "no");
                params.put("cb_nov_fees", "0");
            }
        }

        if (dec_m.equalsIgnoreCase("yes")) {
            params.put("cb_dec", "paid");
        }else{
            if (cb_dec.isChecked()) {
                params.put("cb_dec", "yes");
                params.put("cb_dec_fees", String.valueOf(dec_m_fee));
                params.put("cb_dec_fees_fine", String.valueOf(dec_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_dec", "no");
                params.put("cb_dec_fees", "0");
            }
        }

        if (jan_m.equalsIgnoreCase("yes")) {
            params.put("cb_jan", "paid");
        }else{
            if (cb_jan.isChecked()) {
                params.put("cb_jan", "yes");
                params.put("cb_jan_fees", String.valueOf(jan_m_fee));
                params.put("cb_jan_fees_fine", String.valueOf(jan_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_jan", "no");
                params.put("cb_jan_fees", "0");
            }
        }

        if (feb_m.equalsIgnoreCase("yes")) {
            params.put("cb_feb", "paid");
        }else{
            if (cb_feb.isChecked()) {
                params.put("cb_feb", "yes");
                params.put("cb_feb_fees", String.valueOf(feb_m_fee));
                params.put("cb_feb_fees_fine", String.valueOf(feb_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_feb", "no");
                params.put("cb_feb_fees", "0");
            }
        }

        if (mar_m.equalsIgnoreCase("yes")) {
            params.put("cb_march", "paid");
        }else{
            if (cb_march.isChecked()) {
                params.put("cb_march", "yes");
                params.put("cb_march_fees", String.valueOf(mar_m_fee));
                params.put("cb_march_fees_fine", String.valueOf(mar_m_fee_fine));
                params.put("fee_patricular1", fee_patricular1);
            } else {
                params.put("cb_march", "no");
                params.put("cb_march_fees", "0");
            }
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDFEESN, apisendmsgCallback, this, params);

    }


    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                Toast.makeText(getApplicationContext(), "Fees Paid Successfully \nThank You!", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Log.e("error", error.getMessage() + "");
                // Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }
    };

    private void fetchBankType() {
        bankList.add("Online");
        bankList.add("Cheque");
        bankList.add("DD");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, bankList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        bank_spinner.setAdapter(dataAdapter);
        //attach the listener to the spinner
        bank_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                selectBankType = bankList.get(pos);
                if (selectBankType.equalsIgnoreCase("Cash")) {
                    bank_lyt.setVisibility(View.GONE);
                    online_lyt.setVisibility(View.GONE);
                } else if (selectBankType.equalsIgnoreCase("Online")) {
                    bank_lyt.setVisibility(View.GONE);
                    online_lyt.setVisibility(View.VISIBLE);
                }else{
                    bank_lyt.setVisibility(View.VISIBLE);
                    online_lyt.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Kindly Add Bank Details \n", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setData() {
        String s = "";

        if (april_m.equalsIgnoreCase("no")) {
            if (cb_april.isChecked()) {
                s_april = getString(R.string.april);
                s +=  getString(R.string.aprilf) +"," ;
                setAprildata();
            } else {
                s_april = "";
                cb_april_p.setVisibility(View.INVISIBLE);
                april_recycler_view.setVisibility(View.GONE);
                april_m_fee = 0;
                april_m_fee_fine = 0;
                setTotals();
            }
        }

        if (may_m.equalsIgnoreCase("no")) {
            if (cb_may.isChecked()) {
                s_may = getString(R.string.may);
                s +=  getString(R.string.mayf)+",";
                setMaydata();
            } else {
                s_may = "";
                cb_may_p.setVisibility(View.INVISIBLE);
                may_recycler_view.setVisibility(View.GONE);
                may_m_fee = 0;
                may_m_fee_fine = 0;
                setTotals();
            }
        }


        if (june_m.equalsIgnoreCase("no")) {

            if (cb_june.isChecked()) {
                s_jun = getString(R.string.jun);
                s +=  getString(R.string.junf)+",";
                setJunedata();
            } else {
                s_jun = "";
                cb_june_p.setVisibility(View.INVISIBLE);
                jun_recycler_view.setVisibility(View.GONE);
                june_m_fee = 0;
                june_m_fee_fine = 0;
                setTotals();
            }
        }

        if (july_m.equalsIgnoreCase("no")) {

            if (cb_july.isChecked()) {
                s_july = getString(R.string.jul);
                s +=  getString(R.string.julf)+",";
                setjulydata();
            } else {
                s_july = "";
                cb_july_p.setVisibility(View.INVISIBLE);
                july_recycler_view.setVisibility(View.GONE);
                july_m_fee = 0;
                july_m_fee_fine = 0;
                setTotals();
            }
        }


        if (aug_m.equalsIgnoreCase("no")) {

            if (cb_aug.isChecked()) {
                s_aug = getString(R.string.aug);
                s +=  getString(R.string.augf)+",";
                setAugdata();
            } else {
                s_aug = "";
                cb_aug_p.setVisibility(View.INVISIBLE);
                aug_recycler_view.setVisibility(View.GONE);
                aug_m_fee = 0;
                aug_m_fee_fine = 0;
                setTotals();
            }
        }


        if (sep_m.equalsIgnoreCase("no")) {

            if (cb_sep.isChecked()) {
                s_sep = getString(R.string.sep);
                s +=  getString(R.string.sepf)+",";
                setSepdata();
            } else {
                s_sep = "";
                cb_sep_p.setVisibility(View.INVISIBLE);
                sep_recycler_view.setVisibility(View.GONE);
                sep_m_fee = 0;
                sep_m_fee_fine = 0;
                setTotals();
            }

        }

        if (oct_m.equalsIgnoreCase("no")) {
            if (cb_oct.isChecked()) {
                s_oct = getString(R.string.oct);
                s +=  getString(R.string.octf)+",";
                setOctdata();
            } else {
                s_oct = "";
                cb_oct_p.setVisibility(View.INVISIBLE);
                oct_recycler_view.setVisibility(View.GONE);
                oct_m_fee = 0;
                oct_m_fee_fine = 0;
                setTotals();
            }
        }

        if (nov_m.equalsIgnoreCase("no")) {
            if (cb_nov.isChecked()) {
                s_nov = getString(R.string.nov);
                s +=  getString(R.string.novf)+",";
                setNovdata();
            } else {
                s_nov = "";
                cb_nov_p.setVisibility(View.INVISIBLE);
                nov_recycler_view.setVisibility(View.GONE);
                nov_m_fee = 0;
                nov_m_fee_fine = 0;
                setTotals();
            }

        }

        if (dec_m.equalsIgnoreCase("no")) {
            if (cb_dec.isChecked()) {
                s_dec = getString(R.string.dec);
                s +=  getString(R.string.decf)+",";
                setDecdata();
            } else {
                s_dec = "";
                cb_dec_p.setVisibility(View.INVISIBLE);
                dec_recycler_view.setVisibility(View.GONE);
                dec_m_fee = 0;
                dec_m_fee_fine = 0;
                setTotals();
            }
        }

        if (jan_m.equalsIgnoreCase("no")) {

            if (cb_jan.isChecked()) {
                s_jan = getString(R.string.jan);
                s +=  getString(R.string.janf)+",";
                setJandata();
            } else {
                s_jan = "";
                cb_jan_p.setVisibility(View.INVISIBLE);
                jan_recycler_view.setVisibility(View.GONE);
                jan_m_fee = 0;
                jan_m_fee_fine = 0;
                setTotals();
            }
        }

        if (feb_m.equalsIgnoreCase("no")) {

            if (cb_feb.isChecked()) {
                s_feb = getString(R.string.feb);
                s += getString(R.string.febf)+",";
                setFebdata();
            } else {
                s_feb = "";
                cb_feb_p.setVisibility(View.INVISIBLE);
                feb_recycler_view.setVisibility(View.GONE);
                feb_m_fee = 0;
                feb_m_fee_fine = 0;
                setTotals();
            }
        }

        if (mar_m.equalsIgnoreCase("no")) {

            if (cb_march.isChecked()) {
                s_mar = getString(R.string.mar);
                s += getString(R.string.marf);
                setMardata();
            } else {
                s_mar = "";
                cb_march_p.setVisibility(View.INVISIBLE);
                mar_recycler_view.setVisibility(View.GONE);
                mar_m_fee = 0;
                mar_m_fee_fine = 0;
                setTotals();
            }
        }

        //  Toast.makeText(getApplicationContext(),"fee_cate_id"+month_name+"\nplace_id"+s+"\nroute_id"+route_id,Toast.LENGTH_LONG).show();
        month_name = s;
        setSelectmonthdata(month_name);

    }

    private void setAprildata() {
        cb_april_p.setVisibility(View.VISIBLE);
        cb_april_p.setText("(Selected)");
        cb_april_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        //selectMonth=getString(R.string.april);
        april_recycler_view.setVisibility(View.VISIBLE);
        april_m_fee = totalPrice1;
        april_m_fee_fine = april_m_fee_fine1;
        setTotals();
    }

    private void setMaydata() {
        cb_may_p.setVisibility(View.VISIBLE);
        cb_may_p.setText("(Selected)");
        cb_may_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        may_recycler_view.setVisibility(View.VISIBLE);
        may_m_fee = totalPrice;
        may_m_fee_fine = may_m_fee_fine1;
        setTotals();
    }

    private void setJunedata() {
        cb_june_p.setVisibility(View.VISIBLE);
        cb_june_p.setText("(Selected)");
        cb_june_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        jun_recycler_view.setVisibility(View.VISIBLE);
        june_m_fee = totalPrice;
        june_m_fee_fine = june_m_fee_fine1;
        setTotals();
    }

    private void setjulydata() {
        cb_july_p.setVisibility(View.VISIBLE);
        cb_july_p.setText("(Selected)");
        cb_july_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        july_recycler_view.setVisibility(View.VISIBLE);
        july_m_fee = totalPrice;
        july_m_fee_fine = july_m_fee_fine1;
        setTotals();
    }

    private void setAugdata() {
        cb_aug_p.setVisibility(View.VISIBLE);
        cb_aug_p.setText("(Selected)");
        cb_aug_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        aug_recycler_view.setVisibility(View.VISIBLE);
        aug_m_fee = totalPrice;
        aug_m_fee_fine = aug_m_fee_fine1;
        setTotals();
    }

    private void setSepdata() {
        cb_sep_p.setVisibility(View.VISIBLE);
        cb_sep_p.setText("(Selected)");
        cb_sep_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        sep_recycler_view.setVisibility(View.VISIBLE);
        sep_m_fee = totalPrice;
        sep_m_fee_fine = sep_m_fee_fine1;
        setTotals();
    }

    private void setOctdata() {
        cb_oct_p.setVisibility(View.VISIBLE);
        cb_oct_p.setText("(Selected)");
        cb_oct_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        oct_recycler_view.setVisibility(View.VISIBLE);
        oct_m_fee = totalPrice;
        oct_m_fee_fine = oct_m_fee_fine1;
        setTotals();
    }

    private void setNovdata() {
        cb_nov_p.setVisibility(View.VISIBLE);
        cb_nov_p.setText("(Selected)");
        cb_nov_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        nov_recycler_view.setVisibility(View.VISIBLE);
        nov_m_fee = totalPrice;
        nov_m_fee_fine = nov_m_fee_fine1;
        setTotals();
    }

    private void setDecdata() {
        cb_dec_p.setVisibility(View.VISIBLE);
        cb_dec_p.setText("(Selected)");
        cb_dec_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        dec_recycler_view.setVisibility(View.VISIBLE);
        dec_m_fee = totalPrice;
        dec_m_fee_fine = dec_m_fee_fine1;
        setTotals();
    }

    private void setJandata() {
        cb_jan_p.setVisibility(View.VISIBLE);
        cb_jan_p.setText("(Selected)");
        cb_jan_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        jan_recycler_view.setVisibility(View.VISIBLE);
        jan_m_fee = totalPrice;
        jan_m_fee_fine = jan_m_fee_fine1;
        setTotals();
    }

    private void setFebdata() {
        cb_feb_p.setVisibility(View.VISIBLE);
        cb_feb_p.setText("(Selected)");
        cb_feb_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        feb_recycler_view.setVisibility(View.VISIBLE);
        feb_m_fee = totalPrice;
        feb_m_fee_fine = feb_m_fee_fine1;
        setTotals();
    }

    private void setMardata() {
        cb_march_p.setVisibility(View.VISIBLE);
        cb_march_p.setText("(Selected)");
        cb_march_p.setTextColor(
                getResources().getColor(
                        R.color.dark_leave_n));
        mar_recycler_view.setVisibility(View.VISIBLE);
        mar_m_fee = totalPrice;
        mar_m_fee_fine = mar_m_fee_fine1;
        setTotals();
    }

    private void setSelectmonthdata(String s) {
        if (s.equalsIgnoreCase("") || s.isEmpty()) {
            text.setVisibility(View.GONE);
            textView7.setText("Kindly, Select Fee Paying Month");
        } else {
            //   text.setVisibility(View.VISIBLE);
            //  text.setText("Selected Month is " + s);
            String title = getColoredSpanned("<b>Selected month</b> ", "#000000");
            String Name = getColoredSpanned("" + s, "#5A5C59");
            text.setText(Html.fromHtml(title + " " + Name));
            String title1 = getColoredSpanned("Selected months are", "#f85164");
            //  textView7.setText(Html.fromHtml(title1 + " " + Name));
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    private void showOnce() {
        check++;
        Toast.makeText(getApplicationContext(), "Kindly enter valid amount\nThank You!", Toast.LENGTH_LONG).show();
    }

    private void setTotalfinevalues() {

        new_total_fine = p_fine_balance + today_add_fine;
        total_fine.setText(String.valueOf(new_total_fine));

        if (new_total_fine >= paid_fine_value) {
            new_f_balance = new_total_fine - paid_fine_value;
            new_fine_balance.setText(String.valueOf(new_f_balance));

        } else {
            Toast.makeText(getApplicationContext(), "Kindly enter valid amount, You are putting an amount\n more than the actual fine\nThank You!", Toast.LENGTH_LONG).show();
            paid_fine.setText("0");
            new_fine_balance.setText(String.valueOf(new_total_fine));
        }

        int fees = Integer.parseInt(g_total.getText().toString());
        int paying = paid_fine_value + fees;
        paying_amount.setText(String.valueOf(paying));

    }


    private void setTotalvalues(String tag) {


        if (tag.equalsIgnoreCase("p_amount")) {
            total_value = p_value + t_value - c_value;
            if (totalPrice_withoutTrans >= p_value) {
                g_total.setText(String.valueOf(total_value));
                new_feess_balance = new_totalPrice - p_value + t_value;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            } else {
                Toast.makeText(getApplicationContext(), "Kindly enter valid amount, You are putting an amount\n more than the actual Class fee\nThank You!", Toast.LENGTH_LONG).show();
                //this.p_amount.setText("0");
                p_amount.setText(String.valueOf(new_totalPrice));
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        } else if (tag.equalsIgnoreCase("t_amount")) {
            total_value = p_value + t_value - c_value;
            if (trans_amount >= t_value) {
                g_total.setText(String.valueOf(total_value));
                int check = p_value + t_value;
                new_feess_balance = new_totalPrice - check;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            } else {
                Toast.makeText(getApplicationContext(), "Kindly enter valid amount, You are putting an amount\n more than the actual Transport fee\nThank You!", Toast.LENGTH_LONG).show();
                this.t_amount.setText("0");
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        } else {
            int tot_value = p_value + t_value;
            total_value = p_value + t_value - c_value;
            g_total.setText(String.valueOf(total_value));
            if (tot_value >= c_value) {
                int check = p_value + t_value;
                new_feess_balance = new_totalPrice - check;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            } else {
                Toast.makeText(getApplicationContext(), "Kindly enter valid amount, You are putting an amount\n more than the actual total fee\nThank You!", Toast.LENGTH_LONG).show();
                this.c_amount.setText("0");
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        }


        int fees = Integer.parseInt(g_total.getText().toString());
        int paying = paid_fine_value + fees;
        paying_amount.setText(String.valueOf(paying));

    }


    private void fetchSession() {
        // ErpProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    fromdate=sessionlist.get(0).getStart_date();
                    todate=sessionlist.get(0).getEnd_date();
                    fetchExra(fromdate,todate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //  Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchExra(String fromdate, String todate) {
        HashMap<String, String> params = new HashMap<>();
        params.put("s_id", User.getCurrentUser().getId());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EXTRADETAILS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                if (error == null) {
                    try {

                        extraDetails = ExtraDetails.parseParticularBeanArray(jsonObject.getJSONArray("extra_data"));
                        fee_cate_id= extraDetails.get(0).getFee_cat_id();
                        route_id=extraDetails.get(0).getRoute_id();
                        place_id=extraDetails.get(0).getPlace_id();

                        jan_m = extraDetails.get(0).getJan_month();
                        feb_m = extraDetails.get(0).getFeb_month();
                        mar_m = extraDetails.get(0).getMar_month();
                        april_m = extraDetails.get(0).getApr_month();
                        may_m = extraDetails.get(0).getMay_month();
                        june_m = extraDetails.get(0).getJune_month();
                        july_m = extraDetails.get(0).getJuly_month();
                        aug_m = extraDetails.get(0).getAug_month();
                        sep_m = extraDetails.get(0).getSep_month();
                        oct_m = extraDetails.get(0).getOct_month();
                        nov_m = extraDetails.get(0).getNov_month();
                        dec_m = extraDetails.get(0).getDec_month();


                        fetchFeesAmount(fromdate,todate);
                        fetchPreviousbalance1(getString(R.string.aprilf), "04");
                        fetchPreviousbalance2(getString(R.string.mayf), "05");
                        fetchPreviousbalance3(getString(R.string.junf), "06");
                        fetchPreviousbalance4(getString(R.string.julf), "07");
                        fetchPreviousbalance5(getString(R.string.augf), "08");
                        fetchPreviousbalance6(getString(R.string.sepf), "09");
                        fetchPreviousbalance7(getString(R.string.octf), "10");
                        fetchPreviousbalance8(getString(R.string.novf), "11");
                        fetchPreviousbalance9(getString(R.string.decf), "12");
                        fetchPreviousbalance10(getString(R.string.janf), "03");
                        fetchPreviousbalance11(getString(R.string.febf), "02");
                        fetchPreviousbalance12(getString(R.string.marf), "01");
                        setDataNew();

                        //  Toast.makeText(AdddocumentActivity.this, "Documents list!", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }



    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    private void checkPaidMonth() {
        if (april_m.equalsIgnoreCase("yes")) {
            cb_april.setEnabled(false);
            cb_april_p.setVisibility(View.VISIBLE);
            cb_april.setChecked(true);
            cb_april.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_april.setButtonDrawable(R.drawable.cb_selector1);
            april_m_fee = 0;
        }else if (april_m.equalsIgnoreCase("pending")) {
            cb_april.setEnabled(false);
            cb_april_p.setVisibility(View.VISIBLE);
            cb_april_p.setText("(In Process)");
            cb_april_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
            cb_april.setChecked(true);
            cb_april.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_april.setButtonDrawable(R.drawable.cb_selector1);
            april_recycler_view.setVisibility(View.GONE);
            april_m_fee = 0;
            april_m_fee_fine = 0;
        }


        if (may_m.equalsIgnoreCase("yes")) {
            cb_may.setEnabled(false);
            cb_may.setChecked(true);
            cb_may_p.setVisibility(View.VISIBLE);
            cb_may.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_may.setButtonDrawable(R.drawable.cb_selector1);
            may_m_fee = 0;
        }else if (may_m.equalsIgnoreCase("pending")) {
            cb_may.setEnabled(false);
            cb_may.setChecked(true);
            cb_may_p.setVisibility(View.VISIBLE);
            cb_may.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_may.setButtonDrawable(R.drawable.cb_selector1);
            may_recycler_view.setVisibility(View.GONE);
            may_m_fee = 0;
            may_m_fee_fine = 0;
            cb_may_p.setText("(In Process)");
            cb_may_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }


        if (june_m.equalsIgnoreCase("yes")) {
            cb_june.setEnabled(false);
            cb_june.setChecked(true);
            cb_june_p.setVisibility(View.VISIBLE);
            cb_june.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_june.setButtonDrawable(R.drawable.cb_selector1);
            june_m_fee = 0;
        }else if (june_m.equalsIgnoreCase("pending")) {
            cb_june.setEnabled(false);
            cb_june.setChecked(true);
            cb_june_p.setVisibility(View.VISIBLE);
            cb_june.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_june.setButtonDrawable(R.drawable.cb_selector1);
            jun_recycler_view.setVisibility(View.GONE);
            june_m_fee = 0;
            june_m_fee_fine = 0;
            cb_june_p.setText("(In Process)");
            cb_june_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }

        if (july_m.equalsIgnoreCase("yes")) {
            cb_july.setEnabled(false);
            cb_july.setChecked(true);
            cb_july_p.setVisibility(View.VISIBLE);
            cb_july.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_july.setButtonDrawable(R.drawable.cb_selector1);
            july_m_fee = 0;
        }else if (july_m.equalsIgnoreCase("pending")) {
            cb_july.setEnabled(false);
            cb_july.setChecked(true);
            cb_july_p.setVisibility(View.VISIBLE);
            cb_july.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_july.setButtonDrawable(R.drawable.cb_selector1);
            july_recycler_view.setVisibility(View.GONE);
            july_m_fee = 0;
            july_m_fee_fine = 0;
            cb_july_p.setText("(In Process)");
            cb_july_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }


        if (aug_m.equalsIgnoreCase("yes")) {
            cb_aug.setEnabled(false);
            cb_aug.setChecked(true);
            cb_aug_p.setVisibility(View.VISIBLE);
            cb_aug.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_aug.setButtonDrawable(R.drawable.cb_selector1);
            aug_m_fee = 0;
        }else if (aug_m.equalsIgnoreCase("pending")) {
            cb_aug.setEnabled(false);
            cb_aug.setChecked(true);
            cb_aug_p.setVisibility(View.VISIBLE);
            cb_aug.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_aug.setButtonDrawable(R.drawable.cb_selector1);
            aug_recycler_view.setVisibility(View.GONE);
            aug_m_fee = 0;
            aug_m_fee_fine = 0;
            cb_aug_p.setText("(In Process)");
            cb_aug_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }


        if (sep_m.equalsIgnoreCase("yes")) {
            cb_sep.setEnabled(false);
            cb_sep.setChecked(true);
            cb_sep_p.setVisibility(View.VISIBLE);
            cb_sep.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_sep.setButtonDrawable(R.drawable.cb_selector1);
            sep_m_fee = 0;

        }else if (sep_m.equalsIgnoreCase("pending")) {
            cb_sep.setEnabled(false);
            cb_sep.setChecked(true);
            cb_sep_p.setVisibility(View.VISIBLE);
            cb_sep.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_sep.setButtonDrawable(R.drawable.cb_selector1);
            sep_recycler_view.setVisibility(View.GONE);
            sep_m_fee = 0;
            sep_m_fee_fine = 0;
            cb_sep_p.setText("(In Process)");
            cb_sep_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }

        if (oct_m.equalsIgnoreCase("yes")) {
            cb_oct.setEnabled(false);
            cb_oct.setChecked(true);
            cb_oct_p.setVisibility(View.VISIBLE);
            cb_oct.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_oct.setButtonDrawable(R.drawable.cb_selector1);
            oct_m_fee = 0;

        }else if (oct_m.equalsIgnoreCase("pending")) {
            cb_oct.setEnabled(false);
            cb_oct.setChecked(true);
            cb_oct_p.setVisibility(View.VISIBLE);
            cb_oct.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_oct.setButtonDrawable(R.drawable.cb_selector1);
            oct_recycler_view.setVisibility(View.GONE);
            oct_m_fee = 0;
            oct_m_fee_fine = 0;
            cb_oct_p.setText("(In Process)");
            cb_oct_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }


        if (nov_m.equalsIgnoreCase("yes")) {
            cb_nov.setEnabled(false);
            cb_nov.setChecked(true);
            cb_nov_p.setVisibility(View.VISIBLE);
            cb_nov.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_nov.setButtonDrawable(R.drawable.cb_selector1);
            nov_m_fee = 0;
        }else if (nov_m.equalsIgnoreCase("pending")) {
            cb_nov.setEnabled(false);
            cb_nov.setChecked(true);
            cb_nov_p.setVisibility(View.VISIBLE);
            cb_nov.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_nov.setButtonDrawable(R.drawable.cb_selector1);
            nov_recycler_view.setVisibility(View.GONE);
            nov_m_fee = 0;
            nov_m_fee_fine = 0;
            cb_nov_p.setText("(In Process)");
            cb_nov_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }


        if (dec_m.equalsIgnoreCase("yes")) {
            cb_dec.setEnabled(false);
            cb_dec.setChecked(true);
            cb_dec_p.setVisibility(View.VISIBLE);
            cb_dec.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_dec.setButtonDrawable(R.drawable.cb_selector1);
            dec_m_fee = 0;
        }else if (dec_m.equalsIgnoreCase("pending")) {
            cb_dec.setEnabled(false);
            cb_dec.setChecked(true);
            cb_dec_p.setVisibility(View.VISIBLE);
            cb_dec.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_dec.setButtonDrawable(R.drawable.cb_selector1);
            dec_recycler_view.setVisibility(View.GONE);
            dec_m_fee = 0;
            dec_m_fee_fine = 0;
            cb_dec_p.setText("(In Process)");
            cb_dec_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }


        if (jan_m.equalsIgnoreCase("yes")) {
            cb_jan.setEnabled(false);
            cb_jan.setChecked(true);
            cb_jan_p.setVisibility(View.VISIBLE);
            cb_jan.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_jan.setButtonDrawable(R.drawable.cb_selector1);
            jan_m_fee = 0;
        }else if (jan_m.equalsIgnoreCase("pending")) {
            cb_jan.setEnabled(false);
            cb_jan.setChecked(true);
            cb_jan_p.setVisibility(View.VISIBLE);
            cb_jan.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_jan.setButtonDrawable(R.drawable.cb_selector1);
            jan_recycler_view.setVisibility(View.GONE);
            jan_m_fee = 0;
            jan_m_fee_fine = 0;
            cb_jan_p.setText("(In Process)");
            cb_jan_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }


        if (feb_m.equalsIgnoreCase("yes")) {
            cb_feb.setEnabled(false);
            cb_feb.setChecked(true);
            cb_feb_p.setVisibility(View.VISIBLE);
            cb_feb.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_feb.setButtonDrawable(R.drawable.cb_selector1);
            feb_m_fee = 0;
        }else if (feb_m.equalsIgnoreCase("pending")) {
            cb_feb.setEnabled(false);
            cb_feb.setChecked(true);
            cb_feb_p.setVisibility(View.VISIBLE);
            cb_feb.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_feb.setButtonDrawable(R.drawable.cb_selector1);
            feb_recycler_view.setVisibility(View.GONE);
            feb_m_fee = 0;
            feb_m_fee_fine = 0;
            cb_feb_p.setText("(In Process)");
            cb_feb_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }

        if (mar_m.equalsIgnoreCase("yes")) {
            cb_march.setEnabled(false);
            cb_march.setChecked(true);
            cb_march_p.setVisibility(View.VISIBLE);
            cb_march.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_march.setButtonDrawable(R.drawable.cb_selector1);
            mar_m_fee = 0;
        }else if (mar_m.equalsIgnoreCase("pending")) {
            cb_march.setEnabled(false);
            cb_march.setChecked(true);
            cb_march_p.setVisibility(View.VISIBLE);
            cb_march.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_march.setButtonDrawable(R.drawable.cb_selector1);
            mar_recycler_view.setVisibility(View.GONE);
            mar_m_fee = 0;
            mar_m_fee_fine = 0;
            cb_march_p.setText("(In Process)");
            cb_march_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }

    }

//    private void checkPaidMonth() {
//
//        String c_year = "", n_year = "";
//        c_year = fromdate.substring(0, 4);
//        n_year = todate.substring(0, 4);
//
//        cb_april_y.setText("(" + c_year + ")");
//        cb_may_y.setText("(" + c_year + ")");
//        cb_june_y.setText("(" + c_year + ")");
//        cb_july_y.setText("(" + c_year + ")");
//        cb_aug_y.setText("(" + c_year + ")");
//        cb_sep_y.setText("(" + c_year + ")");
//        cb_oct_y.setText("(" + c_year + ")");
//        cb_nov_y.setText("(" + c_year + ")");
//        cb_dec_y.setText("(" + c_year + ")");
//        cb_jan_y.setText("(" + n_year + ")");
//        cb_feb_y.setText("(" + n_year + ")");
//        cb_march_y.setText("(" + n_year + ")");
//
//        if (april_m.equalsIgnoreCase("yes")) {
//            cb_april.setEnabled(false);
//            cb_april_p.setVisibility(View.VISIBLE);
//            cb_april.setChecked(true);
//            cb_april.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_april.setButtonDrawable(R.drawable.cb_selector1);
//            april_recycler_view.setVisibility(View.GONE);
//            april_m_fee = 0;
//            april_m_fee_fine = 0;
//
//        }
//        else{
//            cb_april.setEnabled(false);
//            cb_april_p.setVisibility(View.VISIBLE);
//            cb_april_p.setText("(Pending)");
//            cb_april_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_april.setChecked(true);
//            cb_april.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_april.setButtonDrawable(R.drawable.cb_selector2);
//            april_recycler_view.setVisibility(View.VISIBLE);
//            april_m_fee = totalPrice1;
//            april_m_fee_fine = april_m_fee_fine1;
//            cb_april_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//
//        if (may_m.equalsIgnoreCase("yes")) {
//            cb_may.setEnabled(false);
//            cb_may.setChecked(true);
//            cb_may_p.setVisibility(View.VISIBLE);
//            cb_may.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_may.setButtonDrawable(R.drawable.cb_selector1);
//            may_recycler_view.setVisibility(View.GONE);
//            may_m_fee = 0;
//            may_m_fee_fine = 0;
//        }else{
//            cb_may.setEnabled(false);
//            cb_may.setChecked(true);
//            cb_may_p.setVisibility(View.VISIBLE);
//            cb_may_p.setText("(Pending)");
//            cb_may_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_may.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_may.setButtonDrawable(R.drawable.cb_selector2);
//            may_recycler_view.setVisibility(View.VISIBLE);
//            may_m_fee = totalPrice;
//            may_m_fee_fine = may_m_fee_fine1;
//            cb_may_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//
//        if (june_m.equalsIgnoreCase("yes")) {
//            cb_june.setEnabled(false);
//            cb_june.setChecked(true);
//            cb_june_p.setVisibility(View.VISIBLE);
//            cb_june.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_june.setButtonDrawable(R.drawable.cb_selector1);
//            jun_recycler_view.setVisibility(View.GONE);
//            june_m_fee = 0;
//            june_m_fee_fine = 0;
//        }else{
//            cb_june.setEnabled(false);
//            cb_june.setChecked(true);
//            cb_june_p.setVisibility(View.VISIBLE);
//            cb_june_p.setText("(Pending)");
//            cb_june_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_june.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_june.setButtonDrawable(R.drawable.cb_selector2);
//            jun_recycler_view.setVisibility(View.VISIBLE);
//            june_m_fee = totalPrice;
//            june_m_fee_fine = june_m_fee_fine1;
//            cb_june_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//        if (july_m.equalsIgnoreCase("yes")) {
//            cb_july.setEnabled(false);
//            cb_july.setChecked(true);
//            cb_july_p.setVisibility(View.VISIBLE);
//            cb_july.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_july.setButtonDrawable(R.drawable.cb_selector1);
//            july_recycler_view.setVisibility(View.GONE);
//            july_m_fee = 0;
//            july_m_fee_fine = 0;
//        }else{
//            cb_july.setEnabled(false);
//            cb_july.setChecked(true);
//            cb_july_p.setVisibility(View.VISIBLE);
//            cb_july_p.setText("(Pending)");
//            cb_july_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_july.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_july.setButtonDrawable(R.drawable.cb_selector2);
//            july_recycler_view.setVisibility(View.VISIBLE);
//            july_m_fee = totalPrice;
//            july_m_fee_fine = july_m_fee_fine1;
//            cb_july_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//
//        if (aug_m.equalsIgnoreCase("yes")) {
//            cb_aug.setEnabled(false);
//            cb_aug.setChecked(true);
//            cb_aug_p.setVisibility(View.VISIBLE);
//            cb_aug.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_aug.setButtonDrawable(R.drawable.cb_selector1);
//            aug_recycler_view.setVisibility(View.GONE);
//            aug_m_fee = 0;
//            aug_m_fee_fine = 0;
//        }else{
//            cb_aug.setEnabled(false);
//            cb_aug.setChecked(true);
//            cb_aug_p.setVisibility(View.VISIBLE);
//            cb_aug_p.setText("(Pending)");
//            cb_aug_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_aug.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_aug.setButtonDrawable(R.drawable.cb_selector2);
//            aug_recycler_view.setVisibility(View.VISIBLE);
//            aug_m_fee = totalPrice;
//            aug_m_fee_fine = aug_m_fee_fine1;
//            cb_aug_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//
//        if (sep_m.equalsIgnoreCase("yes")) {
//            cb_sep.setEnabled(false);
//            cb_sep.setChecked(true);
//            cb_sep_p.setVisibility(View.VISIBLE);
//            cb_sep.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_sep.setButtonDrawable(R.drawable.cb_selector1);
//            sep_recycler_view.setVisibility(View.GONE);
//            sep_m_fee = 0;
//            sep_m_fee_fine = 0;
//
//        }else{
//            cb_sep.setEnabled(false);
//            cb_sep.setChecked(true);
//            cb_sep_p.setVisibility(View.VISIBLE);
//            cb_sep_p.setText("(Pending)");
//            cb_sep_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_sep.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_sep.setButtonDrawable(R.drawable.cb_selector2);
//            sep_recycler_view.setVisibility(View.VISIBLE);
//            sep_m_fee = totalPrice;
//            sep_m_fee_fine = sep_m_fee_fine1;
//            cb_sep_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//        if (oct_m.equalsIgnoreCase("yes")) {
//            cb_oct.setEnabled(false);
//            cb_oct.setChecked(true);
//            cb_oct_p.setVisibility(View.VISIBLE);
//            cb_oct.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_oct.setButtonDrawable(R.drawable.cb_selector1);
//            oct_recycler_view.setVisibility(View.GONE);
//            oct_m_fee = 0;
//            oct_m_fee_fine = 0;
//        }else{
//            cb_oct.setEnabled(false);
//            cb_oct.setChecked(true);
//            cb_oct_p.setVisibility(View.VISIBLE);
//            cb_oct_p.setText("(Pending)");
//            cb_oct_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_oct.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_oct.setButtonDrawable(R.drawable.cb_selector2);
//            oct_recycler_view.setVisibility(View.VISIBLE);
//            oct_m_fee = totalPrice;
//            oct_m_fee_fine = oct_m_fee_fine1;
//            cb_oct_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//
//        if (nov_m.equalsIgnoreCase("yes")) {
//            cb_nov.setEnabled(false);
//            cb_nov.setChecked(true);
//            cb_nov_p.setVisibility(View.VISIBLE);
//            cb_nov.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_nov.setButtonDrawable(R.drawable.cb_selector1);
//            nov_recycler_view.setVisibility(View.GONE);
//            nov_m_fee = 0;
//            nov_m_fee_fine = 0;
//        }else{
//            cb_nov.setEnabled(false);
//            cb_nov.setChecked(true);
//            cb_nov_p.setVisibility(View.VISIBLE);
//            cb_nov_p.setText("(Pending)");
//            cb_nov_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_nov.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_nov.setButtonDrawable(R.drawable.cb_selector2);
//            nov_recycler_view.setVisibility(View.VISIBLE);
//            nov_m_fee = totalPrice;
//            nov_m_fee_fine = nov_m_fee_fine1;
//            cb_nov_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//
//        if (dec_m.equalsIgnoreCase("yes")) {
//            cb_dec.setEnabled(false);
//            cb_dec.setChecked(true);
//            cb_dec_p.setVisibility(View.VISIBLE);
//            cb_dec.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_dec.setButtonDrawable(R.drawable.cb_selector1);
//            dec_recycler_view.setVisibility(View.GONE);
//            dec_m_fee = 0;
//            dec_m_fee_fine = 0;
//        }else{
//            cb_dec.setEnabled(false);
//            cb_dec.setChecked(true);
//            cb_dec_p.setVisibility(View.VISIBLE);
//            cb_dec_p.setText("(Pending)");
//            cb_dec_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_dec.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_dec.setButtonDrawable(R.drawable.cb_selector2);
//            dec_recycler_view.setVisibility(View.VISIBLE);
//            dec_m_fee = totalPrice;
//            dec_m_fee_fine = dec_m_fee_fine1;
//            cb_dec_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//
//        if (jan_m.equalsIgnoreCase("yes")) {
//            cb_jan.setEnabled(false);
//            cb_jan.setChecked(true);
//            cb_jan_p.setVisibility(View.VISIBLE);
//            cb_jan.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_jan.setButtonDrawable(R.drawable.cb_selector1);
//            jan_recycler_view.setVisibility(View.GONE);
//            jan_m_fee = 0;
//            jan_m_fee_fine = 0;
//        }else{
//            cb_jan.setEnabled(false);
//            cb_jan.setChecked(true);
//            cb_jan_p.setVisibility(View.VISIBLE);
//            cb_jan_p.setText("(Pending)");
//            cb_jan_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_jan.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_jan.setButtonDrawable(R.drawable.cb_selector2);
//            jan_recycler_view.setVisibility(View.VISIBLE);
//            jan_m_fee = totalPrice;
//            jan_m_fee_fine = jan_m_fee_fine1;
//            cb_jan_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//
//        if (feb_m.equalsIgnoreCase("yes")) {
//            cb_feb.setEnabled(false);
//            cb_feb.setChecked(true);
//            cb_feb_p.setVisibility(View.VISIBLE);
//            cb_feb.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_feb.setButtonDrawable(R.drawable.cb_selector1);
//            feb_recycler_view.setVisibility(View.GONE);
//            feb_m_fee = 0;
//            feb_m_fee_fine = 0;
//        }else{
//            cb_feb.setEnabled(false);
//            cb_feb.setChecked(true);
//            cb_feb_p.setVisibility(View.VISIBLE);
//            cb_feb_p.setText("(Pending)");
//            cb_feb_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_feb.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_feb.setButtonDrawable(R.drawable.cb_selector2);
//            feb_recycler_view.setVisibility(View.VISIBLE);
//            feb_m_fee = totalPrice;
//            feb_m_fee_fine = feb_m_fee_fine1;
//            cb_feb_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//        if (mar_m.equalsIgnoreCase("yes")) {
//            cb_march.setEnabled(false);
//            cb_march.setChecked(true);
//            cb_march_p.setVisibility(View.VISIBLE);
//            cb_march.setTextColor(
//                    getResources().getColor(
//                            R.color.light_grey1));
//            cb_march.setButtonDrawable(R.drawable.cb_selector1);
//            mar_recycler_view.setVisibility(View.GONE);
//            mar_m_fee = 0;
//            mar_m_fee_fine = 0;
//        }else{
//            cb_march.setEnabled(false);
//            cb_march.setChecked(true);
//            cb_march_p.setVisibility(View.VISIBLE);
//            cb_march_p.setText("(Pending)");
//            cb_march_p.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave_n));
//            cb_march.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//            cb_march.setButtonDrawable(R.drawable.cb_selector2);
//            mar_recycler_view.setVisibility(View.VISIBLE);
//            mar_m_fee = totalPrice;
//            mar_m_fee_fine = mar_m_fee_fine1;
//            cb_march_y.setTextColor(
//                    getResources().getColor(
//                            R.color.dark_leave));
//        }
//
//        setTotals();
//
//    }

    private void setTotals() {
        // totalPrice_withoutTrans = totalPrice + p_balance;
        // new_totalPrice = p_balance + totalPrice + trans_amount;


        fine_totalPrice = jan_m_fee_fine + feb_m_fee_fine + mar_m_fee_fine + april_m_fee_fine + may_m_fee_fine + june_m_fee_fine + july_m_fee_fine +
                aug_m_fee_fine + sep_m_fee_fine + oct_m_fee_fine + nov_m_fee_fine + dec_m_fee_fine;


        sub_totalPrice = jan_m_fee + feb_m_fee + mar_m_fee + april_m_fee + may_m_fee +
                june_m_fee + july_m_fee + aug_m_fee + sep_m_fee + oct_m_fee + nov_m_fee + dec_m_fee;

        new_totalPrice = fine_totalPrice + p_balance + sub_totalPrice;

        totalPrice_withoutTrans = new_totalPrice;


        a_sub.setText(String.valueOf(sub_totalPrice));
        f_sub.setText(String.valueOf(fine_totalPrice));

        p_amount.setText(String.valueOf(new_totalPrice));
        totals.setText(String.valueOf(new_totalPrice)+".00");

        if (new_totalPrice == p_balance) {
            top.setVisibility(View.GONE);
            bottom.setVisibility(View.GONE);
            textNorecord.setVisibility(View.VISIBLE);
        } else {
            textNorecord.setVisibility(View.GONE);
            top.setVisibility(View.VISIBLE);
            bottom.setVisibility(View.VISIBLE);
        }
    }

    private void fetchFeesAmount(String fromdate, String todate) {
        // ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", "all");
        params.put("c_id", classid);
        params.put("s_id", regist);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, apiCallback2, this, params);
    }

    ApiHandler.ApiCallback apiCallback2 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                //  Toast.makeText(getApplicationContext(),"select another Month",Toast.LENGTH_SHORT).show();

                try {
                    list = FeeBalanceBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                    p_balance = Integer.parseInt(list.get(0).getFee_bal());
                    p_fine_balance = Integer.parseInt(list.get(0).getBalance_fine());

                    // Toast.makeText(getApplicationContext(),p_balance+"\n"+p_fine_balance,Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setFeeData(p_balance, p_fine_balance);

            } else {
                ErpProgress.cancelProgressBar();

                if (p_fine_balance == 0) {
                    fine_close.setVisibility(View.GONE);
                    //  fine_view.setVisibility(View.VISIBLE);
                }
                //    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
//                fetchFeesAmount();
//                fetchPreviousbalance();
            }
        }
    };

    private void setFeeData(int p_balance, int p_fine_balance) {


        if (p_fine_balance == 0) {
            fine_close.setVisibility(View.GONE);
            // fine_view.setVisibility(View.VISIBLE);
        }

        fine_balance.setText(String.valueOf(p_fine_balance));
        total_fine.setText(String.valueOf(p_fine_balance));
        balance.setText(String.valueOf(p_balance)+".00");

        //   totals.setText(String.valueOf(new_totalPrice));
    }


    private void fetchPreviousbalance1(String m, String mc) {
        totalPrice1 = 0;
        // ErpProgress.showProgressBar(PayFee_New.this, "Please wait...");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

//                top.setVisibility(View.VISIBLE);
//                bottom.setVisibility(View.VISIBLE);

                    try {
                        if (classList1 != null) {
                            classList1.clear();
                        }

                        classList1 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter.setFeeList(classList1);
                        newsAdapter.notifyDataSetChanged();
                        for (int i = 0; i < classList1.size(); i++) {
                            totalPrice1 += Integer.parseInt(classList1.get(i).getFee_amount());
                            fee_patricular += String.valueOf(classList1.get(i).getFee_particular())+", ";
                            april_m_fee_fine1 += Integer.parseInt(classList1.get(i).getEs_fine());
                        }

                        checkPaidMonth();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    ErpProgress.cancelProgressBar();
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    classList1.clear();
                    newsAdapter.setFeeList(classList1);
                    newsAdapter.notifyDataSetChanged();
                    april_recycler_view.setVisibility(View.GONE);
                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance2(String m, String mc) {
        totalPrice = 0;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

                if (error == null) {

                    try {
                        if (classList2 != null) {
                            classList2.clear();
                        }

                        classList2 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter1.setFeeList(classList2);
                        newsAdapter1.notifyDataSetChanged();

                        for (int i = 0; i < classList2.size(); i++) {
                            totalPrice += Integer.parseInt(classList2.get(i).getFee_amount());
                            fee_patricular1 += String.valueOf(classList2.get(i).getFee_particular())+" ";
                            may_m_fee_fine1 += Integer.parseInt(classList2.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();

                        checkPaidMonth();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList2.clear();
                    newsAdapter1.setFeeList(classList2);
                    newsAdapter1.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance3(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {

                    try {
                        if (classList3 != null) {
                            classList3.clear();
                        }

                        classList3 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter2.setFeeList(classList3);
                        newsAdapter2.notifyDataSetChanged();

                        for (int i = 0; i < classList3.size(); i++) {
                            june_m_fee_fine1 += Integer.parseInt(classList3.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList3.clear();
                    newsAdapter2.setFeeList(classList3);
                    newsAdapter2.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance4(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

                if (error == null) {

                    try {
                        if (classList4 != null) {
                            classList4.clear();
                        }

                        classList4 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter3.setFeeList(classList4);
                        newsAdapter3.notifyDataSetChanged();

                        for (int i = 0; i < classList4.size(); i++) {
                            july_m_fee_fine1 += Integer.parseInt(classList4.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList4.clear();
                    newsAdapter3.setFeeList(classList4);
                    newsAdapter3.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance5(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        if (classList5 != null) {
                            classList5.clear();
                        }
                        classList5 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter4.setFeeList(classList5);
                        newsAdapter4.notifyDataSetChanged();

                        for (int i = 0; i < classList5.size(); i++) {
                            aug_m_fee_fine1 += Integer.parseInt(classList5.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList5.clear();
                    newsAdapter4.setFeeList(classList5);
                    newsAdapter4.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance6(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        if (classList6 != null) {
                            classList6.clear();
                        }

                        classList6 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter5.setFeeList(classList6);
                        newsAdapter5.notifyDataSetChanged();

                        for (int i = 0; i < classList6.size(); i++) {
                            sep_m_fee_fine1 += Integer.parseInt(classList6.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList6.clear();
                    newsAdapter5.setFeeList(classList6);
                    newsAdapter5.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance7(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        if (classList7 != null) {
                            classList7.clear();
                        }

                        classList7 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter6.setFeeList(classList7);
                        newsAdapter6.notifyDataSetChanged();

                        for (int i = 0; i < classList7.size(); i++) {
                            oct_m_fee_fine1 += Integer.parseInt(classList7.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList7.clear();
                    newsAdapter6.setFeeList(classList7);
                    newsAdapter6.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance8(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        if (classList8 != null) {
                            classList8.clear();
                        }

                        classList8 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter7.setFeeList(classList8);
                        newsAdapter7.notifyDataSetChanged();

                        for (int i = 0; i < classList8.size(); i++) {
                            nov_m_fee_fine1 += Integer.parseInt(classList8.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList8.clear();
                    newsAdapter7.setFeeList(classList8);
                    newsAdapter7.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance9(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                    try {
                        if (classList9 != null) {
                            classList9.clear();
                        }

                        classList9 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter8.setFeeList(classList9);
                        newsAdapter8.notifyDataSetChanged();

                        for (int i = 0; i < classList9.size(); i++) {
                            dec_m_fee_fine1 += Integer.parseInt(classList9.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList9.clear();
                    newsAdapter8.setFeeList(classList9);
                    newsAdapter8.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance10(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                    try {
                        if (classList10 != null) {
                            classList10.clear();
                        }

                        classList10 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter9.setFeeList(classList10);
                        newsAdapter9.notifyDataSetChanged();

                        for (int i = 0; i < classList10.size(); i++) {
                            jan_m_fee_fine1 += Integer.parseInt(classList10.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList10.clear();
                    newsAdapter9.setFeeList(classList10);
                    newsAdapter9.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance11(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                    try {
                        if (classList11 != null) {
                            classList11.clear();
                        }

                        classList11 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter10.setFeeList(classList11);
                        newsAdapter10.notifyDataSetChanged();

                        for (int i = 0; i < classList11.size(); i++) {
                            feb_m_fee_fine1 += Integer.parseInt(classList11.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList11.clear();
                    newsAdapter10.setFeeList(classList11);
                    newsAdapter10.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance12(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                    try {
                        if (classList12 != null) {
                            classList12.clear();
                        }

                        classList12 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter11.setFeeList(classList12);
                        newsAdapter11.notifyDataSetChanged();
                        for (int i = 0; i < classList12.size(); i++) {
                            mar_m_fee_fine1 += Integer.parseInt(classList12.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList12.clear();
                    newsAdapter11.setFeeList(classList12);
                    newsAdapter11.notifyDataSetChanged();
                }
            }
        }, this, params);
    }




    private void savePrefData() {
        sharedPreferences = PayFee_New.this.getSharedPreferences("boarding_pref_list", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_list", true);
        editor.apply();
    }

    private boolean restorePrefData() {
        sharedPreferences = PayFee_New.this.getSharedPreferences("boarding_pref_list", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_list", false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support, "View Student Fees History!", "Tap the View button to view Student Fees History.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        commonIntent();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();


    }

    @OnClick(R.id.contact_support)
    public void contact_support() {
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        commonIntent();
    }

    private void commonIntent() {
        Intent intent = new Intent(PayFee_New.this, CommonFeesListActivity.class);
        intent.putExtra("list_view", "add");
//        intent.putExtra("name", name);
//        intent.putExtra("f_name", f_name);
//        intent.putExtra("roll", roll);
//        intent.putExtra("dp", image);
//        intent.putExtra("regist", regist);
//        intent.putExtra("class", classid);
//        intent.putExtra("class_name", class_name);
//        intent.putExtra("gender", gender);
//        intent.putExtra("fee_cate_id", fee_cate_id);
//        intent.putExtra("route_id", route_id);
//        intent.putExtra("place_id", place_id);
//        intent.putExtra("fromdate", fromdate);
//        intent.putExtra("todate", todate);
//        intent.putExtra("month", selectMonth);
//        intent.putExtra("tag", "student_wise");
        startActivity(intent);
    }

    private void setDataNew() {

        if (!restorePrefData()) {
            setShowcaseView();
        }
        // txtTitle.setText(class_name+" Pay Fees");
        txtTitle.setText("Pending Fees");
        //  student_lyt.setVisibility(View.VISIBLE);

        if (gender.equalsIgnoreCase("Male")) {
            if (image != null) {
                Utils.progressImg(image, dp, PayFee_New.this);
                // Picasso.get().load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
            } else {
                Picasso.get().load(R.drawable.stud).into(dp);
            }
        } else {
            if (image != null) {
                Utils.progressImg(image, dp, PayFee_New.this);
                //  Picasso.get().load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
            } else {
                Picasso.get().load(R.drawable.stud).into(dp);
            }
        }

        txtSub.setText(name + "(" + regist + ")");
        edtName.setText(f_name);
        edtRegistration.setText(roll);

        //view_std.setVisibility(View.VISIBLE);
        student_lyt.setVisibility(View.GONE);
    }

}