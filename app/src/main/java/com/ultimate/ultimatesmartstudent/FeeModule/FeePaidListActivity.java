package com.ultimate.ultimatesmartstudent.FeeModule;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeePaidListActivity extends AppCompatActivity implements FeeListAdapter.ProdMethodCallBack {


    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    ArrayList<Fee_Paid_Bean> list;
    private FeeListAdapter mAdapter;
    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    CommonProgress commonProgress;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.textView8)
    TextView textView8;
    @BindView(R.id.spinnerMonth)
    Spinner spinnerMonth;
    ArrayList<String> monthList = new ArrayList<>();

    String selectMonth = "", name = "", f_name = "", roll = "",
            image = "", gender = "", regist = "", classid = "",
            fee_cate_id = "", fromdate = "", todate = "", class_name = "", place_id = "", route_id = "", tag = "", month_name = "";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_paid_list_new);
        list = new ArrayList<>();
        ButterKnife.bind(this);
        textView8.setText(getString(R.string.f_month));
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(getString(R.string.fee)+" "+getString(R.string.list));
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeeListAdapter(list,this, this);
        recyclerView.setAdapter(mAdapter);
       // fetchFeeDetail(selectMonth);
        fetchmonthList();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList() {

        monthList.add("All");
        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");
        monthList.add("January");
        monthList.add("February");
        monthList.add("March");


        //   Toast.makeText(getApplicationContext(),month_name,Toast.LENGTH_SHORT).show();


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, monthList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);

//        Calendar cal=Calendar.getInstance();
//        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
//        month_name = month_date.format(cal.getTime());
//
//        // setting here spinner item selected with position..
//        for(int i=0;i<monthList.size();i++) {
//            if (monthList.get(i).equalsIgnoreCase(month_name)) {
//                selectMonth = monthList.get(i);
//                spinnerMonth.setSelection(i);
//
//            }
//        }

        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                selectMonth = monthList.get(pos);
//                if (tag.equalsIgnoreCase("student_wise")) {
//                     fetchFeesDetails(selectMonth);
//                }else){
//                     fetchFeesDetails(selectMonth);
//                }
                fetchFeeDetail(selectMonth);



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }



    private void fetchFeeDetail(String selectMonth) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", User.getCurrentUser().getId());
        params.put("month", selectMonth);
        params.put("check", "fees");
        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                if (list != null) {
                    list.clear();

                }
                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    list = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    if (list.size() > 0) {
                        mAdapter.setMerchantBeans(list);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(list.size()));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        list.clear();
                        mAdapter.setMerchantBeans(list);
                        mAdapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
               // Log.i("Fee_Paid_Bean", String.valueOf(list.get(0).getEs_particularamount())+ String.valueOf(list.get(0).getId()));
               // Log.i("Fee_Paid_Bean", String.valueOf(jsonObject));
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                list.clear();
                mAdapter.setMerchantBeans(list);
                mAdapter.notifyDataSetChanged();
                txtNorecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(fee_paid_bean, Fee_Paid_Bean.class);
        Intent intent = new Intent(FeePaidListActivity.this, FeeDetailsActivity.class);
        intent.putExtra("fee_data", prod_data);
        startActivity(intent);
        Animatoo.animateZoom(this);
    }
}