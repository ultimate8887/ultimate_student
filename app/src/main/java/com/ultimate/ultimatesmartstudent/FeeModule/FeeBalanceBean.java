package com.ultimate.ultimatesmartstudent.FeeModule;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeeBalanceBean {
    private String id;
    private String fee_particular;

    public String getFee_particular() {
        return fee_particular;
    }

    public void setFee_particular(String fee_particular) {
        this.fee_particular = fee_particular;
    }



    public String getFee_bal() {
        return fee_bal;
    }

    public void setFee_bal(String fee_bal) {
        this.fee_bal = fee_bal;
    }

    public String getBalance_fine() {
        return balance_fine;
    }

    public void setBalance_fine(String balance_fine) {
        this.balance_fine = balance_fine;
    }

    private String fee_bal;
    private String balance_fine;
//    private String  prev_paid_bal;
//    private String prev_total_bal;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static ArrayList<FeeBalanceBean> parseParticularBeanArray(JSONArray arrayObj) {
        ArrayList<FeeBalanceBean> list = new ArrayList<FeeBalanceBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                FeeBalanceBean p = parseParticularBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp", e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static FeeBalanceBean parseParticularBeanObject(JSONObject jsonObject) {
        FeeBalanceBean casteObj = new FeeBalanceBean();
        try {

            if (jsonObject.has("id") && !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("fee_particular") && !jsonObject.getString("fee_particular").isEmpty() && !jsonObject.getString("fee_particular").equalsIgnoreCase("null")) {
                casteObj.setFee_particular(jsonObject.getString("fee_particular"));
            }
            if (jsonObject.has("fee_bal") && !jsonObject.getString("fee_bal").isEmpty() && !jsonObject.getString("fee_bal").equalsIgnoreCase("null")) {
                casteObj.setFee_bal(jsonObject.getString("fee_bal"));
            }
            if (jsonObject.has("balance_fine") && !jsonObject.getString("balance_fine").isEmpty() && !jsonObject.getString("balance_fine").equalsIgnoreCase("null")) {
                casteObj.setBalance_fine(jsonObject.getString("balance_fine"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
