package com.ultimate.ultimatesmartstudent.FeeModule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeePaidListAdapter extends RecyclerView.Adapter<FeePaidListAdapter.Viewholder> {
private final Context mContext;
private final List<String> part_value;
private final List<String> part_id;
private final List<String> part_name;
private final List<String> part_amonu;
private final List<String> con_amount;
private final List<String> con_id;
        FeePaidBean.FeeDataBean mlist;

        int last_fine_bal = 0;
        int last_fee_bal = 0;
        int fine = 0;
        int fine_pay = 0;
        int balance_pay = 0;
        int total_fee_pay = 0;//Doesn't include fine amount
        boolean shown = false;
        int listsize = 0;
        int t_pay=0;
        int t_amt =0;
        int t_con =0;

public FeePaidListAdapter(List<String> part_value,List<String> part_id, List<String> part_amonu, List<String> part_name, List<String> con_amount, List<String> con_id, FeePaidBean.FeeDataBean mlist, Context mContext) {
        this.part_id = part_id;
        this.part_amonu = part_amonu;
        this.part_name = part_name;
        this.con_amount = con_amount;
        this.con_id = con_id;
        this.mContext = mContext;
        this.part_value = part_value;
        this.mlist = mlist;




//this code added on 2662019
        int part_pay = 0;
        for (int i = 0; i < part_amonu.size(); i++) {
        if (con_id.size() > 0) {
        part_pay += Integer.parseInt(part_amonu.get(i));
        } else {
        for (int x = 0; x < con_id.size(); x++) {
        if (part_id.get(i) == con_id.get(x)) {
        part_pay += Integer.parseInt(part_amonu.get(i)) + Integer.parseInt(con_amount.get(x));
        }
        }
        }
        }
        if (!mlist.getEs_last_fine_bal().equalsIgnoreCase("0")) {
        last_fine_bal = Integer.parseInt(mlist.getEs_last_fine_bal());
        }
        if (!mlist.getEs_last_fee_bal().equalsIgnoreCase("0")) {
        last_fee_bal = Integer.parseInt(mlist.getEs_last_fee_bal());
        }
        if (!mlist.getEs_fine().equalsIgnoreCase("0")) {
        fine = Integer.parseInt(mlist.getEs_fine());
        }
        if (!mlist.getEs_fine_paid().equalsIgnoreCase("0")) {
        fine_pay = Integer.parseInt(mlist.getEs_fine_paid());
        }
//        if (!mlist.getEs_totalamount().equalsIgnoreCase("0")) {
//        total_fee_pay = Integer.parseInt(mlist.getEs_totalamount());
//        }
        balance_pay = total_fee_pay - part_pay;
        listsize = part_id.size();

        listsize++;
        if (last_fee_bal > 0) {
        listsize++;
        }
        if (last_fine_bal > 0 || fine > 0) {
        listsize++;
        }
        }

@Override
public FeePaidListAdapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fee_paid_amt_lyt, viewGroup, false);
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sch_fee_paid_amt_lyt, viewGroup, false);
        FeePaidListAdapter.Viewholder viewholder = new FeePaidListAdapter.Viewholder(view);
        return viewholder;
        }

//    @Override
//    public void onBindViewHolder(@NonNull FeePaidListAdapter.Viewholder viewholder, int i) {
//        viewholder.txtP.setText(part_name.get(i));
//        viewholder.txtA.setText(part_amonu.get(i));
//        if (con_id.size() > 0) {
//            for (int x = 0; x < con_id.size(); x++) {
//                if (part_id.get(i) == con_id.get(x)) {
//                    viewholder.txtC.setText(con_amount.get(x));
//                }
//            }
//        } else {
//            viewholder.txtC.setText("0");
//        }
//    }

@Override
public void onBindViewHolder(@NonNull FeePaidListAdapter.Viewholder viewholder, int i) {
        if ((i + 1) > part_id.size()) {
        if ((i + 1) == listsize) {
        //show total
        viewholder.txtP.setText("Total");

        viewholder.txtPay.setText(String.valueOf(t_pay));
        viewholder.txtA.setText(String.valueOf(t_amt));
        viewholder.txtC.setText(String.valueOf(t_con));

        } else if ((i + 1) == listsize - 1) {
        if (!shown) {

        if (last_fee_bal > 0) {

        t_pay += balance_pay;
        t_amt += last_fee_bal;

        viewholder.txtP.setText("Last Balance");
        viewholder.txtPay.setText(String.valueOf(balance_pay));
        viewholder.txtA.setText(String.valueOf(last_fee_bal));
        viewholder.txtC.setText("-");
        } else {
        t_pay += fine_pay;
        viewholder.txtP.setText("Fine");
        viewholder.txtPay.setText(String.valueOf(fine_pay));
        if (last_fine_bal > 0) {
        t_amt += fine+last_fine_bal;
        viewholder.txtA.setText(String.valueOf(fine) + " new\n" + last_fine_bal + " bal");
        } else {
        t_amt += fine;
        viewholder.txtA.setText(String.valueOf(fine));
        }
        viewholder.txtC.setText("-");
        }
        shown=true;
        } else {

        t_pay += fine_pay;
        viewholder.txtP.setText("Fine");
        viewholder.txtPay.setText(String.valueOf(fine_pay));
        if (last_fine_bal > 0) {
        t_amt += fine+last_fine_bal;
        viewholder.txtA.setText(String.valueOf(fine) + " new\n" + last_fine_bal + " bal");
        } else {
        t_amt += fine;
        viewholder.txtA.setText(String.valueOf(fine));
        }
        viewholder.txtC.setText("-");

        }
        } else {
        if (!shown) {

        t_pay += balance_pay;
        t_amt += last_fee_bal;

        viewholder.txtP.setText("Last Balance");
        viewholder.txtPay.setText(String.valueOf(balance_pay));
        viewholder.txtA.setText(String.valueOf(last_fee_bal));
        viewholder.txtC.setText("-");
        shown=true;
        } else {

        t_pay += fine_pay;

        viewholder.txtP.setText("Fine");
        viewholder.txtPay.setText(String.valueOf(fine_pay));
        if (last_fine_bal > 0) {
        t_amt += fine+last_fine_bal;
        viewholder.txtA.setText(String.valueOf(fine) + " new\n" + last_fine_bal + " bal");
        } else {
        t_amt += fine;
        viewholder.txtA.setText(String.valueOf(fine));
        }
        viewholder.txtC.setText("-");
        }
        }
        } else {
        t_pay += Integer.parseInt(part_amonu.get(i));
        t_amt += Integer.parseInt(part_value.get(i));

        viewholder.txtP.setText(part_name.get(i));
        viewholder.txtPay.setText(part_amonu.get(i));
        viewholder.txtA.setText(part_value.get(i));
        if (con_id.size() > 0) {
        for (int x = 0; x < con_id.size(); x++) {
        if (part_id.get(i).equalsIgnoreCase(con_id.get(x))) {
        t_con += Integer.parseInt(con_amount.get(x));
        viewholder.txtC.setText(con_amount.get(x));
        }
        }
        } else {
        viewholder.txtC.setText("0");
        }
        }
        }

@Override
public int getItemCount() {
        return listsize;
        }

public class Viewholder extends RecyclerView.ViewHolder {
    @BindView(R.id.txtP)
    TextView txtP;
    @BindView(R.id.txtA)
    TextView txtA;
    @BindView(R.id.txtC)
    TextView txtC;
    @BindView(R.id.txtPay)
    TextView txtPay;

    public Viewholder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

}
}
