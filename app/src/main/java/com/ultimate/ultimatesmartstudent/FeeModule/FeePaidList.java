package com.ultimate.ultimatesmartstudent.FeeModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.SessionBean;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeePaidList extends AppCompatActivity {

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.spinnerFinancialYear)
    Spinner spinnerFinancialYear;
//    @BindView(R.id.parent)
//    LinearLayout parent;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    ArrayList<FeePaidBean.FeeDataBean> list;
    private FeePaidAdapter mAdapter;
    @BindView(R.id.listLyt)
    LinearLayout listLyt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_paid_list);
        list = new ArrayList<>();
        ButterKnife.bind(this);
        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText("Fee Paid List");
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeePaidAdapter(list, this);
        recyclerView.setAdapter(mAdapter);
        fetchSession();
    }
    private void fetchSession() {
        UltimateProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    private SessionBean session;
    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(FeePaidList.this, sessionlist);
                    spinnerFinancialYear.setAdapter(adapter);
                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            session = sessionlist.get(i);
                            fetchFeeDetail(session, User.getCurrentUser().getId());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
               // Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();

            }
        }
    };

    private void fetchFeeDetail(SessionBean session, String id) {
        UltimateProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("todate", session.getEnd_date());
        params.put("fromdate", session.getStart_date());
        params.put("student_id", id);
        String url = Constants.getBaseURL() + Constants.FEE_PAID_BY_STUDENT;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                if (list != null)
                    list.clear();
                FeePaidBean data = FeePaidBean.parseFeeDetail(jsonObject);
                list = data.getFee_data();
                if (list != null) {
                    listLyt.setVisibility(View.VISIBLE);
                    mAdapter.setFeeList(list);
                    mAdapter.notifyDataSetChanged();
//                    int total = 0;
//                    for (int i = 0; i < list.size(); i++) {
//                        total += Integer.parseInt(list.get(i).getEs_totalamount())+Integer.parseInt(list.get(i).getEs_fine_paid());
//                    }
//                    txtTotal.setText(String.valueOf(total));
                } else {
                    listLyt.setVisibility(View.GONE);
                }
            } else {
                listLyt.setVisibility(View.GONE);
                if (list != null)
                    list.clear();
                mAdapter.setFeeList(list);
                mAdapter.notifyDataSetChanged();
               // Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @OnClick(R.id.imgBackmsg)
    public void backClick() {
        finish();
    }
}