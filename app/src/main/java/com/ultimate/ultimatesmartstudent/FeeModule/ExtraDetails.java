package com.ultimate.ultimatesmartstudent.FeeModule;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ExtraDetails {

    private String id;

    public String getFee_cat_id() {
        return fee_cat_id;
    }

    public void setFee_cat_id(String fee_cat_id) {
        this.fee_cat_id = fee_cat_id;
    }

    private String fee_cat_id;

    private String place_id;

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    private String route_id;

    private String jan_id;
    private String jan_month;
    private String jan_paid;

    public String getJan_id() {
        return jan_id;
    }

    public void setJan_id(String jan_id) {
        this.jan_id = jan_id;
    }

    public String getJan_month() {
        return jan_month;
    }

    public void setJan_month(String jan_month) {
        this.jan_month = jan_month;
    }

    public String getJan_paid() {
        return jan_paid;
    }

    public void setJan_paid(String jan_paid) {
        this.jan_paid = jan_paid;
    }

    public String getFeb_id() {
        return feb_id;
    }

    public void setFeb_id(String feb_id) {
        this.feb_id = feb_id;
    }

    public String getFeb_month() {
        return feb_month;
    }

    public void setFeb_month(String feb_month) {
        this.feb_month = feb_month;
    }

    public String getFeb_paid() {
        return feb_paid;
    }

    public void setFeb_paid(String feb_paid) {
        this.feb_paid = feb_paid;
    }

    public String getMar_id() {
        return mar_id;
    }

    public void setMar_id(String mar_id) {
        this.mar_id = mar_id;
    }

    public String getMar_month() {
        return mar_month;
    }

    public void setMar_month(String mar_month) {
        this.mar_month = mar_month;
    }

    public String getMar_paid() {
        return mar_paid;
    }

    public void setMar_paid(String mar_paid) {
        this.mar_paid = mar_paid;
    }

    public String getApr_id() {
        return apr_id;
    }

    public void setApr_id(String apr_id) {
        this.apr_id = apr_id;
    }

    public String getApr_month() {
        return apr_month;
    }

    public void setApr_month(String apr_month) {
        this.apr_month = apr_month;
    }

    public String getApr_paid() {
        return apr_paid;
    }

    public void setApr_paid(String apr_paid) {
        this.apr_paid = apr_paid;
    }

    public String getMay_id() {
        return may_id;
    }

    public void setMay_id(String may_id) {
        this.may_id = may_id;
    }

    public String getMay_month() {
        return may_month;
    }

    public void setMay_month(String may_month) {
        this.may_month = may_month;
    }

    public String getMay_paid() {
        return may_paid;
    }

    public void setMay_paid(String may_paid) {
        this.may_paid = may_paid;
    }

    public String getJune_id() {
        return june_id;
    }

    public void setJune_id(String june_id) {
        this.june_id = june_id;
    }

    public String getJune_month() {
        return june_month;
    }

    public void setJune_month(String june_month) {
        this.june_month = june_month;
    }

    public String getJune_paid() {
        return june_paid;
    }

    public void setJune_paid(String june_paid) {
        this.june_paid = june_paid;
    }

    public String getJuly_id() {
        return july_id;
    }

    public void setJuly_id(String july_id) {
        this.july_id = july_id;
    }

    public String getJuly_month() {
        return july_month;
    }

    public void setJuly_month(String july_month) {
        this.july_month = july_month;
    }

    public String getJuly_paid() {
        return july_paid;
    }

    public void setJuly_paid(String july_paid) {
        this.july_paid = july_paid;
    }

    public String getAug_id() {
        return aug_id;
    }

    public void setAug_id(String aug_id) {
        this.aug_id = aug_id;
    }

    public String getAug_month() {
        return aug_month;
    }

    public void setAug_month(String aug_month) {
        this.aug_month = aug_month;
    }

    public String getAug_paid() {
        return aug_paid;
    }

    public void setAug_paid(String aug_paid) {
        this.aug_paid = aug_paid;
    }

    public String getSep_id() {
        return sep_id;
    }

    public void setSep_id(String sep_id) {
        this.sep_id = sep_id;
    }

    public String getSep_month() {
        return sep_month;
    }

    public void setSep_month(String sep_month) {
        this.sep_month = sep_month;
    }

    public String getSep_paid() {
        return sep_paid;
    }

    public void setSep_paid(String sep_paid) {
        this.sep_paid = sep_paid;
    }

    public String getOct_id() {
        return oct_id;
    }

    public void setOct_id(String oct_id) {
        this.oct_id = oct_id;
    }

    public String getOct_month() {
        return oct_month;
    }

    public void setOct_month(String oct_month) {
        this.oct_month = oct_month;
    }

    public String getOct_paid() {
        return oct_paid;
    }

    public void setOct_paid(String oct_paid) {
        this.oct_paid = oct_paid;
    }

    public String getNov_id() {
        return nov_id;
    }

    public void setNov_id(String nov_id) {
        this.nov_id = nov_id;
    }

    public String getNov_month() {
        return nov_month;
    }

    public void setNov_month(String nov_month) {
        this.nov_month = nov_month;
    }

    public String getNov_paid() {
        return nov_paid;
    }

    public void setNov_paid(String nov_paid) {
        this.nov_paid = nov_paid;
    }

    public String getDec_id() {
        return dec_id;
    }

    public void setDec_id(String dec_id) {
        this.dec_id = dec_id;
    }

    public String getDec_month() {
        return dec_month;
    }

    public void setDec_month(String dec_month) {
        this.dec_month = dec_month;
    }

    public String getDec_paid() {
        return dec_paid;
    }

    public void setDec_paid(String dec_paid) {
        this.dec_paid = dec_paid;
    }

    private String feb_id;
    private String feb_month;
    private String feb_paid;

    private String mar_id;
    private String mar_month;
    private String mar_paid;

    private String apr_id;
    private String apr_month;
    private String apr_paid;



    private String may_id;
    private String may_month;
    private String may_paid;

    private String june_id;
    private String june_month;
    private String june_paid;

    private String july_id;
    private String july_month;
    private String july_paid;

    private String aug_id;
    private String aug_month;
    private String aug_paid;



    private String sep_id;
    private String sep_month;
    private String sep_paid;

    private String oct_id;
    private String oct_month;
    private String oct_paid;

    private String nov_id;
    private String nov_month;
    private String nov_paid;

    private String dec_id;
    private String dec_month;
    private String dec_paid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static ArrayList<ExtraDetails> parseParticularBeanArray(JSONArray arrayObj) {
        ArrayList<ExtraDetails> list = new ArrayList<ExtraDetails>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                ExtraDetails p = parseParticularBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp", e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static ExtraDetails parseParticularBeanObject(JSONObject jsonObject) {
        ExtraDetails user = new ExtraDetails();
        try {

            if (jsonObject.has("id") && !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                user.setId(jsonObject.getString("id"));
            }

            if (jsonObject.has("place_id") && !jsonObject.getString("place_id").isEmpty() && !jsonObject.getString("place_id").equalsIgnoreCase("null")) {
                user.setPlace_id(jsonObject.getString("place_id"));
            }
            if (jsonObject.has("route_id") && !jsonObject.getString("route_id").isEmpty() && !jsonObject.getString("route_id").equalsIgnoreCase("null")) {
                user.setRoute_id(jsonObject.getString("route_id"));
            }
            if (jsonObject.has("fee_cat_id") && !jsonObject.getString("fee_cat_id").isEmpty() && !jsonObject.getString("fee_cat_id").equalsIgnoreCase("null")) {
                user.setFee_cat_id(jsonObject.getString("fee_cat_id"));
            }


            if (jsonObject.has("jan_id") && !jsonObject.getString("jan_id").isEmpty() && !jsonObject.getString("jan_id").equalsIgnoreCase("null")) {
                user.setJan_id(jsonObject.getString("jan_id"));
            }
            if (jsonObject.has("jan_month") && !jsonObject.getString("jan_month").isEmpty() && !jsonObject.getString("jan_month").equalsIgnoreCase("null")) {
                user.setJan_month(jsonObject.getString("jan_month"));
            }
            if (jsonObject.has("jan_paid") && !jsonObject.getString("jan_paid").isEmpty() && !jsonObject.getString("jan_paid").equalsIgnoreCase("null")) {
                user.setJan_paid(jsonObject.getString("jan_paid"));
            }
            if (jsonObject.has("feb_id") && !jsonObject.getString("feb_id").isEmpty() && !jsonObject.getString("feb_id").equalsIgnoreCase("null")) {
                user.setFeb_id(jsonObject.getString("feb_id"));
            }
            if (jsonObject.has("feb_month") && !jsonObject.getString("feb_month").isEmpty() && !jsonObject.getString("feb_month").equalsIgnoreCase("null")) {
                user.setFeb_month(jsonObject.getString("feb_month"));
            }
            if (jsonObject.has("feb_paid") && !jsonObject.getString("feb_paid").isEmpty() && !jsonObject.getString("feb_paid").equalsIgnoreCase("null")) {
                user.setFeb_paid(jsonObject.getString("feb_paid"));
            }
            if (jsonObject.has("mar_id") && !jsonObject.getString("mar_id").isEmpty() && !jsonObject.getString("mar_id").equalsIgnoreCase("null")) {
                user.setMar_id(jsonObject.getString("mar_id"));
            }
            if (jsonObject.has("mar_month") && !jsonObject.getString("mar_month").isEmpty() && !jsonObject.getString("mar_month").equalsIgnoreCase("null")) {
                user.setMar_month(jsonObject.getString("mar_month"));
            }
            if (jsonObject.has("mar_paid") && !jsonObject.getString("mar_paid").isEmpty() && !jsonObject.getString("mar_paid").equalsIgnoreCase("null")) {
                user.setMar_paid(jsonObject.getString("mar_paid"));
            }


            if (jsonObject.has("apr_id") && !jsonObject.getString("apr_id").isEmpty() && !jsonObject.getString("apr_id").equalsIgnoreCase("null")) {
                user.setApr_id(jsonObject.getString("apr_id"));
            }
            if (jsonObject.has("apr_paid") && !jsonObject.getString("apr_paid").isEmpty() && !jsonObject.getString("apr_paid").equalsIgnoreCase("null")) {
                user.setApr_paid(jsonObject.getString("apr_paid"));
            }
            if (jsonObject.has("apr_month") && !jsonObject.getString("apr_month").isEmpty() && !jsonObject.getString("apr_month").equalsIgnoreCase("null")) {
                user.setApr_month(jsonObject.getString("apr_month"));
            }
            if (jsonObject.has("may_id") && !jsonObject.getString("may_id").isEmpty() && !jsonObject.getString("may_id").equalsIgnoreCase("null")) {
                user.setMay_id(jsonObject.getString("may_id"));
            }
            if (jsonObject.has("may_month") && !jsonObject.getString("may_month").isEmpty() && !jsonObject.getString("may_month").equalsIgnoreCase("null")) {
                user.setMay_month(jsonObject.getString("may_month"));
            }
            if (jsonObject.has("may_paid") && !jsonObject.getString("may_paid").isEmpty() && !jsonObject.getString("may_paid").equalsIgnoreCase("null")) {
                user.setMay_paid(jsonObject.getString("may_paid"));
            }
            if (jsonObject.has("june_id") && !jsonObject.getString("june_id").isEmpty() && !jsonObject.getString("june_id").equalsIgnoreCase("null")) {
                user.setJune_id(jsonObject.getString("june_id"));
            }
            if (jsonObject.has("june_month") && !jsonObject.getString("june_month").isEmpty() && !jsonObject.getString("june_month").equalsIgnoreCase("null")) {
                user.setJune_month(jsonObject.getString("june_month"));
            }
            if (jsonObject.has("june_paid") && !jsonObject.getString("june_paid").isEmpty() && !jsonObject.getString("june_paid").equalsIgnoreCase("null")) {
                user.setJune_paid(jsonObject.getString("june_paid"));
            }



            if (jsonObject.has("july_id") && !jsonObject.getString("july_id").isEmpty() && !jsonObject.getString("july_id").equalsIgnoreCase("null")) {
                user.setJuly_id(jsonObject.getString("july_id"));
            }
            if (jsonObject.has("july_month") && !jsonObject.getString("july_month").isEmpty() && !jsonObject.getString("july_month").equalsIgnoreCase("null")) {
                user.setJuly_month(jsonObject.getString("july_month"));
            }
            if (jsonObject.has("july_paid") && !jsonObject.getString("july_paid").isEmpty() && !jsonObject.getString("july_paid").equalsIgnoreCase("null")) {
                user.setJuly_paid(jsonObject.getString("july_paid"));
            }
            if (jsonObject.has("aug_id") && !jsonObject.getString("aug_id").isEmpty() && !jsonObject.getString("aug_id").equalsIgnoreCase("null")) {
                user.setAug_id(jsonObject.getString("aug_id"));
            }
            if (jsonObject.has("aug_month") && !jsonObject.getString("aug_month").isEmpty() && !jsonObject.getString("aug_month").equalsIgnoreCase("null")) {
                user.setAug_month(jsonObject.getString("aug_month"));
            }
            if (jsonObject.has("aug_paid") && !jsonObject.getString("aug_paid").isEmpty() && !jsonObject.getString("aug_paid").equalsIgnoreCase("null")) {
                user.setAug_paid(jsonObject.getString("aug_paid"));
            }
            if (jsonObject.has("sep_id") && !jsonObject.getString("sep_id").isEmpty() && !jsonObject.getString("sep_id").equalsIgnoreCase("null")) {
                user.setSep_id(jsonObject.getString("sep_id"));
            }
            if (jsonObject.has("sep_month") && !jsonObject.getString("sep_month").isEmpty() && !jsonObject.getString("sep_month").equalsIgnoreCase("null")) {
                user.setSep_month(jsonObject.getString("sep_month"));
            }
            if (jsonObject.has("sep_paid") && !jsonObject.getString("sep_paid").isEmpty() && !jsonObject.getString("sep_paid").equalsIgnoreCase("null")) {
                user.setSep_paid(jsonObject.getString("sep_paid"));
            }




            if (jsonObject.has("oct_id") && !jsonObject.getString("oct_id").isEmpty() && !jsonObject.getString("oct_id").equalsIgnoreCase("null")) {
                user.setOct_id(jsonObject.getString("oct_id"));
            }
            if (jsonObject.has("oct_month") && !jsonObject.getString("oct_month").isEmpty() && !jsonObject.getString("oct_month").equalsIgnoreCase("null")) {
                user.setOct_month(jsonObject.getString("oct_month"));
            }
            if (jsonObject.has("oct_paid") && !jsonObject.getString("oct_paid").isEmpty() && !jsonObject.getString("oct_paid").equalsIgnoreCase("null")) {
                user.setOct_paid(jsonObject.getString("oct_paid"));
            }
            if (jsonObject.has("nov_id") && !jsonObject.getString("nov_id").isEmpty() && !jsonObject.getString("nov_id").equalsIgnoreCase("null")) {
                user.setNov_id(jsonObject.getString("nov_id"));
            }
            if (jsonObject.has("nov_month") && !jsonObject.getString("nov_month").isEmpty() && !jsonObject.getString("nov_month").equalsIgnoreCase("null")) {
                user.setNov_month(jsonObject.getString("nov_month"));
            }
            if (jsonObject.has("nov_paid") && !jsonObject.getString("nov_paid").isEmpty() && !jsonObject.getString("nov_paid").equalsIgnoreCase("null")) {
                user.setNov_paid(jsonObject.getString("nov_paid"));
            }
            if (jsonObject.has("dec_id") && !jsonObject.getString("dec_id").isEmpty() && !jsonObject.getString("dec_id").equalsIgnoreCase("null")) {
                user.setDec_id(jsonObject.getString("dec_id"));
            }
            if (jsonObject.has("dec_month") && !jsonObject.getString("dec_month").isEmpty() && !jsonObject.getString("dec_month").equalsIgnoreCase("null")) {
                user.setDec_month(jsonObject.getString("dec_month"));
            }
            if (jsonObject.has("dec_paid") && !jsonObject.getString("dec_paid").isEmpty() && !jsonObject.getString("dec_paid").equalsIgnoreCase("null")) {
                user.setDec_paid(jsonObject.getString("dec_paid"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }
}
