package com.ultimate.ultimatesmartstudent.FeeModule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Particular_Adapter extends RecyclerView.Adapter<Particular_Adapter.Viewholder> {
    private final Context mContext;
    ArrayList<ParticularBean> list;

    public Particular_Adapter(ArrayList<ParticularBean> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;

    }

    @Override
    public Particular_Adapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pay_fee_adapter, viewGroup, false);
        Particular_Adapter.Viewholder viewholder = new Particular_Adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Particular_Adapter.Viewholder viewholder, int i) {

        if (list.get(i).getFee_particular() !=null) {
            viewholder.p.setText(list.get(i).getFee_particular());
        }
        if (list.get(i).getFee_amount() !=null) {
            viewholder.a.setText(list.get(i).getFee_amount());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFeeList(ArrayList<ParticularBean> list) {
        this.list = list;
    }

    public static class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.p)
        TextView p;
        @BindView(R.id.a)
        TextView a;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
