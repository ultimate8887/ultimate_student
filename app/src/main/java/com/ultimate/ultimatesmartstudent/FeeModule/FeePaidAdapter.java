package com.ultimate.ultimatesmartstudent.FeeModule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeePaidAdapter extends RecyclerView.Adapter<FeePaidAdapter.Viewholder> {
    private final Context mContext;
    ArrayList<FeePaidBean.FeeDataBean> list;

    public FeePaidAdapter(ArrayList<FeePaidBean.FeeDataBean> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;

    }

    @Override
    public FeePaidAdapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fee_paid_list_lyt, viewGroup, false);
        FeePaidAdapter.Viewholder viewholder = new FeePaidAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final FeePaidAdapter.Viewholder viewholder, int pos) {
        viewholder.txtDate.setText(list.get(pos).getEs_date());
        viewholder.pendingbal.setText("Balance: "+list.get(pos).getEs_fee_bal()+","+"Fine Bal: "+list.get(pos).getEs_fine_bal());
//        List<String> part_id = Arrays.asList(list.get(pos).getEs_particularid().split("\\s*,\\s*"));
//        List<String> part_amonu = Arrays.asList(list.get(pos).getEs_particularamount().split("\\s*,\\s*"));
//        List<String> part_name = Arrays.asList(list.get(pos).getEs_particularname().split("\\s*,\\s*"));
//        List<String> con_amount = Arrays.asList(list.get(pos).getEs_concessionamount().split("\\s*,\\s*"));
//        List<String> con_id = Arrays.asList(list.get(pos).getEs_concessiontoid().split("\\s*,\\s*"));


        List<String> part_value = new ArrayList<>();
        List<String> part_amonu = new ArrayList<>();
        List<String> part_id =new ArrayList<>();
        List<String> part_name =new ArrayList<>();
        List<String> con_amount =new ArrayList<>();
        List<String> con_id =new ArrayList<>();


        if (list.get(pos).getFee_value()!= null && !list.get(pos).getFee_value().isEmpty()) {
            part_value = Arrays.asList(list.get(pos).getFee_value().split("\\s*,\\s*"));
        }
        if (list.get(pos).getEs_particularamount() != null && !list.get(pos).getEs_particularamount().isEmpty()) {
            part_amonu = Arrays.asList(list.get(pos).getEs_particularamount().split("\\s*,\\s*"));
        }
        if (list.get(pos).getEs_particularid()!= null && !list.get(pos).getEs_particularid().isEmpty()) {
            part_id = Arrays.asList(list.get(pos).getEs_particularid().split("\\s*,\\s*"));
        }
        if (list.get(pos).getEs_particularname()!= null && !list.get(pos).getEs_particularname().isEmpty()) {
            part_name = Arrays.asList(list.get(pos).getEs_particularname().split("\\s*,\\s*"));
        }
        if (list.get(pos).getEs_concessionamount()!= null &&!list.get(pos).getEs_concessionamount().isEmpty()) {
            con_amount = Arrays.asList(list.get(pos).getEs_concessionamount().split("\\s*,\\s*"));
        }
        if (list.get(pos).getEs_concessiontoid() != null && !list.get(pos).getEs_concessiontoid().isEmpty()) {
            con_id = Arrays.asList(list.get(pos).getEs_concessiontoid().split("\\s*,\\s*"));
        }

        FeePaidListAdapter mAdapter = new FeePaidListAdapter(part_value,part_id,part_amonu,part_name,con_amount,con_id,list.get(pos), mContext);
        viewholder.recyclerview.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFeeList(ArrayList<FeePaidBean.FeeDataBean> list) {
        this.list = list;
    }

    public static class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.recyclerview)
        RecyclerView recyclerview;
        @BindView(R.id.pendingbal)TextView pendingbal;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
