package com.ultimate.ultimatesmartstudent.FeeModule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransAmountAdap extends RecyclerView.Adapter<TransAmountAdap.Viewholder> {
    private final Context mContext;
    ArrayList<TransAmount> list;

    public TransAmountAdap(ArrayList<TransAmount> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;

    }

    @Override
    public TransAmountAdap.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pay_fee_adapter, viewGroup, false);
        TransAmountAdap.Viewholder viewholder = new TransAmountAdap.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final TransAmountAdap.Viewholder viewholder, int i) {


        viewholder.p.setText("Transport Fees");

        if (list.get(i).getTrans_amount() != null) {
            viewholder.a.setText(list.get(i).getTrans_amount());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFeeList(ArrayList<TransAmount> list) {
        this.list = list;
    }

    public static class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.p)
        TextView p;
        @BindView(R.id.a)
        TextView a;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
