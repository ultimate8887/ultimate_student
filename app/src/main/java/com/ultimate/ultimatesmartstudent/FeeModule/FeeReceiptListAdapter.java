package com.ultimate.ultimatesmartstudent.FeeModule;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeeReceiptListAdapter extends RecyclerView.Adapter<FeeReceiptListAdapter.ViewHolder> {

    Context context;
    ArrayList<Fee_Paid_Bean> list;
    private FeeReceiptListAdapter.ProdMethodCallBack prodMethodCallBack;
    private Animation animation;

    public FeeReceiptListAdapter(ArrayList<Fee_Paid_Bean> list, Context context, FeeReceiptListAdapter.ProdMethodCallBack prodMethodCallBack) {
        this.list=list;
        this.context=context;
        this.prodMethodCallBack=prodMethodCallBack;
    }

    public void setMerchantBeans(ArrayList<Fee_Paid_Bean> list) {
        this.list = list;
    }

    public interface ProdMethodCallBack{
        void clickMethod(Fee_Paid_Bean fee_paid_bean);
    }


    @NonNull
    @Override
    public FeeReceiptListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_layout_new, parent, false);
        FeeReceiptListAdapter.ViewHolder viewHolder= new FeeReceiptListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FeeReceiptListAdapter.ViewHolder holder, int position) {

        Fee_Paid_Bean data= list.get(position);

        int add=0,sub=0,value=0;

        add =Integer.parseInt(data.getEs_particularamount()) + Integer.parseInt(data.getEs_prospect()) +
                Integer.parseInt(data.getEs_exam()) + Integer.parseInt(data.getEs_counsel()) +
                Integer.parseInt(data.getEs_srf()) + Integer.parseInt(data.getEs_student())
                +Integer.parseInt(data.getEs_other())+Integer.parseInt(data.getEs_transportfee())+
                Integer.parseInt(data.getPaid_fine());

        // add = Integer.parseInt(data.getEs_particularamount())+Integer.parseInt(data.getEs_transportfee());

        sub=Integer.parseInt(data.getEs_concessionamount());

        value=add-sub;

        Log.i("add", String.valueOf(add));
        Log.i("add", String.valueOf(sub));
        Log.i("add", String.valueOf(value));

        if (User.getCurrentUser().getSchoolData().getName() != null) {
            holder.name.setText(User.getCurrentUser().getSchoolData().getName());
        }else {
            holder.name.setText("Not Mentioned");
        }

//        if (data.getEs_totalamount() != null) {
        holder.price.setText(value+".00");
//        }else {
//            holder.price.setText("Not Mentioned");
//        }
        if (data.getScreenshot()!=null) {
            holder.product_img.setVisibility(View.GONE);
            Picasso.get().load(data.getScreenshot()).placeholder(context.getResources().getDrawable(R.drawable.logo)).into(holder.product_img_receipt);
            holder.bg.setVisibility(View.VISIBLE);
        }else {
            holder.bg.setVisibility(View.GONE);
            if (User.getCurrentUser().getSchoolData().getLogo() != null) {
                Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(context.getResources().getDrawable(R.drawable.logo)).into(holder.product_img);
            }
            holder.product_img.setVisibility(View.VISIBLE);
        }
        int bal=0;
        if (data.getBalance_fine().equalsIgnoreCase("0")) {
            bal = Integer.parseInt(data.getFee_bal()) + Integer.parseInt(data.getTotal_fine());
        }else {
            bal = Integer.parseInt(data.getFee_bal()) + Integer.parseInt(data.getBalance_fine());
        }
        holder.balance.setText(bal+".00");
        //  holder.tagline.setText("Payment Mode:- "+data.getPayment_mode());
        if (data.getPaymentdate()!=null) {
            String title = getColoredSpanned("<b>" +"Billing Date:- "+ "</b>", "#5A5C59");
            String Name = getColoredSpanned( Utils.getDateFormated(data.getPaymentdate()), "#5A5C59");
            holder.date.setText(Html.fromHtml(title + " " + Name));
        }

        String title5 = getColoredSpanned("Payment Status:- ", "#5A5C59");
        String Name5 = "";

        if (data.getPayment_status().equalsIgnoreCase("pending")){
            Name5 = getColoredSpanned("<b>" +"Pending"+"</b>", "#F4D00C");
        }else if (data.getPayment_status().equalsIgnoreCase("complete")){
            Name5 = getColoredSpanned("<b>" +"Complete"+"</b>", "#1C8B3B");
        }else {
            Name5 = getColoredSpanned("<b>" +"Decline"+"</b>", "#F4212C");
        }

        holder.status.setText(Html.fromHtml(title5 + " " + Name5));

        //   name2.setText(user.getFamily2name());
        String title = getColoredSpanned("Payment Mode:- ", "#5A5C59");
        String Name = getColoredSpanned("<b>" +data.getPayment_mode()+"</b>", "#f85164");
        holder.gift.setText(Html.fromHtml(title + " " + Name));




        // holder.gift.setTextColor(Color.parseColor("#f85164"));
        //holder.gift.setText("Payment Mode:-"+data.getPayment_mode());

        holder.linearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                if (data.getScreenshot()!=null) {
                    holder.product_img_receipt.startAnimation(animation);
                }else {
                    holder.linearlayout.startAnimation(animation);
                }
                prodMethodCallBack.clickMethod(list.get(position));
            }
        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_img)
        CircularImageView product_img;
        @BindView(R.id.product_img_receipt)
        ImageView product_img_receipt;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.gift)
        TextView gift;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.balance)
        TextView balance;
        @BindView(R.id.tagline)
        TextView tagline;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.linearlayout)
        RelativeLayout linearlayout;
        @BindView(R.id.waoo)
        RelativeLayout bg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
