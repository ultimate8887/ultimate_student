package com.ultimate.ultimatesmartstudent.FeeModule.NewFees;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.SessionBean;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.FeeModule.ExtraDetails;
import com.ultimate.ultimatesmartstudent.FeeModule.FeeBalanceBean;
import com.ultimate.ultimatesmartstudent.FeeModule.Fees_Fragments.AddBankDetails;
import com.ultimate.ultimatesmartstudent.FeeModule.MonthlyFees;
import com.ultimate.ultimatesmartstudent.FeeModule.ParticularBean;
import com.ultimate.ultimatesmartstudent.FeeModule.SessionAdapter;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommonSTDPendingFeesListActivity extends AppCompatActivity {
    ArrayList<ExtraDetails> extraDetails = new ArrayList<>();
    CheckBox cb_april, cb_may, cb_june, cb_july, cb_aug, cb_sep, cb_oct, cb_nov, cb_dec, cb_jan, cb_feb, cb_march;
    TextView cb_april_p, cb_may_p, cb_june_p, cb_july_p, cb_aug_p, cb_sep_p, cb_oct_p, cb_nov_p, cb_dec_p, cb_jan_p, cb_feb_p, cb_march_p;
    TextView cb_april_y, cb_may_y, cb_june_y, cb_july_y, cb_aug_y, cb_sep_y, cb_oct_y, cb_nov_y, cb_dec_y, cb_jan_y, cb_feb_y, cb_march_y;
    Button btnCancel, btnSubmit;
    TextView h_currentday, h_day_name, h_currentMonth, text;
    private Calendar mCalendar;
    int day;
    int day_name;
    @BindView(R.id.fine_close)
    LinearLayout fine_close;

    @BindView(R.id.fine_balance)
    EditText fine_balance;
    @BindView(R.id.today_fine)
    EditText today_fine;
    @BindView(R.id.paid_fine)
    EditText paid_fine;

    @BindView(R.id.p_amount)
    EditText p_amount;
    @BindView(R.id.t_amount)
    EditText t_amount;
    @BindView(R.id.c_amount)
    EditText c_amount;
    @BindView(R.id.g_total)
    EditText g_total;

    @BindView(R.id.dialog)
    ImageView dialog_click;


    @BindView(R.id.total_fine)
    EditText total_fine;
    @BindView(R.id.new_fine_balance)
    EditText new_fine_balance;
    @BindView(R.id.new_fee_balance)
    EditText new_fee_balance;

    @BindView(R.id.edtReason)
    EditText edtReason;


    @BindView(R.id.b_name)
    EditText b_name;
    @BindView(R.id.b_a_no)
    EditText b_a_no;
    @BindView(R.id.cheque)
    EditText cheque;

    @BindView(R.id.paying_amount)
    TextView paying_amount;
    @BindView(R.id.p)
    TextView p;
    // All RecyclerView
    @BindView(R.id.april_recycler_view)
    RecyclerView april_recycler_view;
    @BindView(R.id.may_recycler_view)
    RecyclerView may_recycler_view;
    @BindView(R.id.jun_recycler_view)
    RecyclerView jun_recycler_view;
    @BindView(R.id.july_recycler_view)
    RecyclerView july_recycler_view;
    @BindView(R.id.aug_recycler_view)
    RecyclerView aug_recycler_view;
    @BindView(R.id.sep_recycler_view)
    RecyclerView sep_recycler_view;
    @BindView(R.id.oct_recycler_view)
    RecyclerView oct_recycler_view;
    @BindView(R.id.nov_recycler_view)
    RecyclerView nov_recycler_view;
    @BindView(R.id.dec_recycler_view)
    RecyclerView dec_recycler_view;
    @BindView(R.id.jan_recycler_view)
    RecyclerView jan_recycler_view;
    @BindView(R.id.feb_recycler_view)
    RecyclerView feb_recycler_view;
    @BindView(R.id.mar_recycler_view)
    RecyclerView mar_recycler_view;
    LinearLayoutManager layoutManager;
    private Particular_Adapter_New newsAdapter, newsAdapter1,
            newsAdapter2, newsAdapter3, newsAdapter4, newsAdapter5,
            newsAdapter6, newsAdapter7, newsAdapter8, newsAdapter9,
            newsAdapter10, newsAdapter11;
    ArrayList<ParticularBean> classList1 = new ArrayList<>();
    ArrayList<ParticularBean> classList2 = new ArrayList<>();
    ArrayList<ParticularBean> classList3 = new ArrayList<>();
    ArrayList<ParticularBean> classList4 = new ArrayList<>();
    ArrayList<ParticularBean> classList5 = new ArrayList<>();
    ArrayList<ParticularBean> classList6 = new ArrayList<>();
    ArrayList<ParticularBean> classList7 = new ArrayList<>();
    ArrayList<ParticularBean> classList8 = new ArrayList<>();
    ArrayList<ParticularBean> classList9 = new ArrayList<>();
    ArrayList<ParticularBean> classList10 = new ArrayList<>();
    ArrayList<ParticularBean> classList11 = new ArrayList<>();
    ArrayList<ParticularBean> classList12 = new ArrayList<>();


    ArrayList<FeeBalanceBean> list = new ArrayList<>();
    // All EditText
    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtFather)
    EditText edtFather;
    @BindView(R.id.edtRegistration)
    EditText edtRegistration;

    // All ImageView
    @BindView(R.id.dp)
    ImageView dp;
    @BindView(R.id.textView7)
    TextView textView7;
    @BindView(R.id.textView8)
    TextView textView8;
    @BindView(R.id.classlayout3)
    LinearLayout classlayout3;
    @BindView(R.id.student_lyt)
    LinearLayout student_lyt;
    @BindView(R.id.top)
    LinearLayout top;
    @BindView(R.id.month_lyt)
    LinearLayout month_lyt;
    @BindView(R.id.bottom)
    LinearLayout bottom;
    @BindView(R.id.view_std)
    TextView view_std;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.totals)
    TextView totals;
    @BindView(R.id.fine_view)
    TextView fine_view;
    @BindView(R.id.pay_view)
    TextView pay_view;
    @BindView(R.id.pay_close)
    LinearLayout pay_close;
    @BindView(R.id.remarklyt)
    LinearLayout remarklyt;
    @BindView(R.id.a_sub)
    TextView a_sub;
    @BindView(R.id.f_sub)
    TextView f_sub;
    @BindView(R.id.note)
    TextView note;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @BindView(R.id.textNorecord)
    TextView textNorecord;
    @BindView(R.id.contact_support)
    ImageView contact_support;
    SharedPreferences sharedPreferences;
    ArrayList<String> monthList = new ArrayList<>();
    ArrayList<String> bankList = new ArrayList<>();
    String selectMonth = "";
    @BindView(R.id.bank_lyt)
    LinearLayout bank_lyt;
    @BindView(R.id.spinner)
    Spinner bank_spinner;

    @BindView(R.id.online_lyt)
    CardView online_lyt;

    @BindView(R.id.add)
    ImageButton add;

    String selectBankType = "", name = "", f_name = "", roll = "",
            image = "", gender = "", regist = "", classid = "",
            fee_cate_id = "", fromdate = "", todate = "", class_name = "", place_id = "", route_id = "", month_name = "", s_jan = "", s_feb = "", s_mar = "", s_april = "", s_may = "", s_jun = "", s_july = "", s_aug = "", s_sep = "", s_oct = "", s_nov = "", s_dec = "";

    String jan_m = "", feb_m = "", mar_m = "", april_m = "", may_m = "", june_m = "", july_m = "", aug_m = "", sep_m = "", oct_m = "", nov_m = "", dec_m = "";
    int jan_m_fee = 0, feb_m_fee = 0, mar_m_fee = 0, april_m_fee = 0, may_m_fee = 0, june_m_fee = 0, july_m_fee = 0, aug_m_fee = 0, sep_m_fee = 0, oct_m_fee = 0,
            nov_m_fee = 0, dec_m_fee = 0;

    int jan_m_fee_fine = 0, feb_m_fee_fine = 0, mar_m_fee_fine = 0, april_m_fee_fine = 0, may_m_fee_fine = 0, june_m_fee_fine = 0, july_m_fee_fine = 0,
            aug_m_fee_fine = 0, sep_m_fee_fine = 0, oct_m_fee_fine = 0,
            nov_m_fee_fine = 0, dec_m_fee_fine = 0;

    int jan_m_fee_fine1 = 0, feb_m_fee_fine1 = 0, mar_m_fee_fine1 = 0, april_m_fee_fine1 = 0, may_m_fee_fine1 = 0, june_m_fee_fine1 = 0, july_m_fee_fine1 = 0,
            aug_m_fee_fine1 = 0, sep_m_fee_fine1 = 0, oct_m_fee_fine1 = 0,
            nov_m_fee_fine1 = 0, dec_m_fee_fine1 = 0;

    Dialog dialog;
    String image_url="",note_url="";
    int check = 0, click = 0;
    int totalPrice = 0, totalPrice1 = 0, trans_amount = 0, p_balance = 0, p_fine_balance = 0, new_totalPrice = 0, sub_totalPrice = 0, fine_totalPrice = 0, totalPrice_withoutTrans = 0, selection = 0;
    int p_value = 0, t_value = 0, c_value = 0, total_value = 0, paid_fine_value = 0, today_add_fine = 0, new_f_balance = 0, new_total_fine = 0, new_feess_balance = 0;
    CommonProgress commonProgress;
    private Bitmap bitmap;
    @BindView(R.id.imgShow)
    PhotoView imgShow;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_stdpending_fees_list);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        month_lyt.setVisibility(View.VISIBLE);
        textView7.setText("Fees Pending/Paid Month details");
        p.setText("Total Pending Fees");
        //CheckBox
        cb_jan = (CheckBox) findViewById(R.id.cb_jan);
        cb_feb = (CheckBox) findViewById(R.id.cb_feb);
        cb_march = (CheckBox) findViewById(R.id.cb_march);
        cb_april = (CheckBox) findViewById(R.id.cb_april);
        cb_may = (CheckBox) findViewById(R.id.cb_may);
        cb_june = (CheckBox) findViewById(R.id.cb_june);
        cb_july = (CheckBox) findViewById(R.id.cb_july);
        cb_aug = (CheckBox) findViewById(R.id.cb_aug);
        cb_sep = (CheckBox) findViewById(R.id.cb_sep);
        cb_oct = (CheckBox) findViewById(R.id.cb_oct);
        cb_nov = (CheckBox) findViewById(R.id.cb_nov);
        cb_dec = (CheckBox) findViewById(R.id.cb_dec);

        //TextView
        cb_jan_p = (TextView) findViewById(R.id.cb_jan_p);
        cb_feb_p = (TextView) findViewById(R.id.cb_feb_p);
        cb_march_p = (TextView) findViewById(R.id.cb_march_p);
        cb_april_p = (TextView) findViewById(R.id.cb_april_p);
        cb_may_p = (TextView) findViewById(R.id.cb_may_p);
        cb_june_p = (TextView) findViewById(R.id.cb_june_p);
        cb_july_p = (TextView) findViewById(R.id.cb_july_p);
        cb_aug_p = (TextView) findViewById(R.id.cb_aug_p);
        cb_sep_p = (TextView) findViewById(R.id.cb_sep_p);
        cb_oct_p = (TextView) findViewById(R.id.cb_oct_p);
        cb_nov_p = (TextView) findViewById(R.id.cb_nov_p);
        cb_dec_p = (TextView) findViewById(R.id.cb_dec_p);

        cb_jan_y = (TextView) findViewById(R.id.cb_jan_y);
        cb_feb_y = (TextView) findViewById(R.id.cb_feb_y);
        cb_march_y = (TextView) findViewById(R.id.cb_march_y);
        cb_april_y = (TextView) findViewById(R.id.cb_april_y);
        cb_may_y = (TextView) findViewById(R.id.cb_may_y);
        cb_june_y = (TextView) findViewById(R.id.cb_june_y);
        cb_july_y = (TextView) findViewById(R.id.cb_july_y);
        cb_aug_y = (TextView) findViewById(R.id.cb_aug_y);
        cb_sep_y = (TextView) findViewById(R.id.cb_sep_y);
        cb_oct_y = (TextView) findViewById(R.id.cb_oct_y);
        cb_nov_y = (TextView) findViewById(R.id.cb_nov_y);
        cb_dec_y = (TextView) findViewById(R.id.cb_dec_y);
        text = (TextView) findViewById(R.id.text);

        name = User.getCurrentUser().getFirstname()+" "+User.getCurrentUser().getLastname();
        f_name = User.getCurrentUser().getFather();
        image = User.getCurrentUser().getProfile();
        gender = User.getCurrentUser().getGender();
        roll = User.getCurrentUser().getId();
        regist = User.getCurrentUser().getId();
        classid = User.getCurrentUser().getClass_id();
        class_name = User.getCurrentUser().getClass_name();

        fetchSession();


        //ParticularBean
        april_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter = new Particular_Adapter_New(classList1, CommonSTDPendingFeesListActivity.this, getString(R.string.april));
        april_recycler_view.setAdapter(newsAdapter);
//
        may_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter1 = new Particular_Adapter_New(classList2, CommonSTDPendingFeesListActivity.this, getString(R.string.may));
        may_recycler_view.setAdapter(newsAdapter1);
//
        jun_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter2 = new Particular_Adapter_New(classList3, CommonSTDPendingFeesListActivity.this, getString(R.string.jun));
        jun_recycler_view.setAdapter(newsAdapter2);

        july_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter3 = new Particular_Adapter_New(classList4, CommonSTDPendingFeesListActivity.this, getString(R.string.jul));
        july_recycler_view.setAdapter(newsAdapter3);

        aug_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter4 = new Particular_Adapter_New(classList5, CommonSTDPendingFeesListActivity.this, getString(R.string.aug));
        aug_recycler_view.setAdapter(newsAdapter4);

        sep_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter5 = new Particular_Adapter_New(classList6, CommonSTDPendingFeesListActivity.this, getString(R.string.sep));
        sep_recycler_view.setAdapter(newsAdapter5);

        oct_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter6 = new Particular_Adapter_New(classList7, CommonSTDPendingFeesListActivity.this, getString(R.string.oct));
        oct_recycler_view.setAdapter(newsAdapter6);

        nov_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter7 = new Particular_Adapter_New(classList8, CommonSTDPendingFeesListActivity.this, getString(R.string.nov));
        nov_recycler_view.setAdapter(newsAdapter7);

        dec_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter8 = new Particular_Adapter_New(classList9, CommonSTDPendingFeesListActivity.this, getString(R.string.dec));
        dec_recycler_view.setAdapter(newsAdapter8);

        jan_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter9 = new Particular_Adapter_New(classList10, CommonSTDPendingFeesListActivity.this, getString(R.string.jan));
        jan_recycler_view.setAdapter(newsAdapter9);

        feb_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter10 = new Particular_Adapter_New(classList11, CommonSTDPendingFeesListActivity.this, getString(R.string.feb));
        feb_recycler_view.setAdapter(newsAdapter10);

        mar_recycler_view.setLayoutManager(new LinearLayoutManager(CommonSTDPendingFeesListActivity.this));
        newsAdapter11 = new Particular_Adapter_New(classList12, CommonSTDPendingFeesListActivity.this, getString(R.string.mar));
        mar_recycler_view.setAdapter(newsAdapter11);

        userprofile();
    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("qr_img");
                        note_url = jsonObject.getJSONObject(Constants.USERDATA).getString("fees_note");

                        if (!note_url.equalsIgnoreCase("no")){
                            note.setText(note_url);

                            String title = getColoredSpanned("<b>" +"Note: "+"</b>", "#F4212C");
                            String Name = getColoredSpanned("" +note_url+"", "#5A5C59");
                            note.setText(Html.fromHtml(title + " " + Name));

                        }else{
                            note.setVisibility(View.GONE);
                        }

                        dialog_click.setVisibility(View.VISIBLE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    dialog_click.setVisibility(View.GONE);
                    //  Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    private void fetchSession() {
        // ErpProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    fromdate=sessionlist.get(0).getStart_date();
                    todate=sessionlist.get(0).getEnd_date();
                    fetchExra(fromdate,todate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //  Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchExra(String fromdate, String todate) {
        HashMap<String, String> params = new HashMap<>();
        params.put("s_id", User.getCurrentUser().getId());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EXTRADETAILS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                if (error == null) {
                    try {

                        extraDetails = ExtraDetails.parseParticularBeanArray(jsonObject.getJSONArray("extra_data"));
                        fee_cate_id= extraDetails.get(0).getFee_cat_id();
                        route_id=extraDetails.get(0).getRoute_id();
                        place_id=extraDetails.get(0).getPlace_id();

                        jan_m = extraDetails.get(0).getJan_month();
                        feb_m = extraDetails.get(0).getFeb_month();
                        mar_m = extraDetails.get(0).getMar_month();
                        april_m = extraDetails.get(0).getApr_month();
                        may_m = extraDetails.get(0).getMay_month();
                        june_m = extraDetails.get(0).getJune_month();
                        july_m = extraDetails.get(0).getJuly_month();
                        aug_m = extraDetails.get(0).getAug_month();
                        sep_m = extraDetails.get(0).getSep_month();
                        oct_m = extraDetails.get(0).getOct_month();
                        nov_m = extraDetails.get(0).getNov_month();
                        dec_m = extraDetails.get(0).getDec_month();


                        fetchFeesAmount(fromdate,todate);
                        fetchPreviousbalance1(getString(R.string.aprilf), "04");
                        fetchPreviousbalance2(getString(R.string.mayf), "05");
                        fetchPreviousbalance3(getString(R.string.junf), "06");
                        fetchPreviousbalance4(getString(R.string.julf), "07");
                        fetchPreviousbalance5(getString(R.string.augf), "08");
                        fetchPreviousbalance6(getString(R.string.sepf), "09");
                        fetchPreviousbalance7(getString(R.string.octf), "10");
                        fetchPreviousbalance8(getString(R.string.novf), "11");
                        fetchPreviousbalance9(getString(R.string.decf), "12");
                        fetchPreviousbalance10(getString(R.string.janf), "03");
                        fetchPreviousbalance11(getString(R.string.febf), "02");
                        fetchPreviousbalance12(getString(R.string.marf), "01");
                        setDataNew();

                        //  Toast.makeText(AdddocumentActivity.this, "Documents list!", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    private void checkPaidMonth() {

        String c_year = "", n_year = "";
        c_year = fromdate.substring(0, 4);
        n_year = todate.substring(0, 4);

        cb_april_y.setText("(" + c_year + ")");
        cb_may_y.setText("(" + c_year + ")");
        cb_june_y.setText("(" + c_year + ")");
        cb_july_y.setText("(" + c_year + ")");
        cb_aug_y.setText("(" + c_year + ")");
        cb_sep_y.setText("(" + c_year + ")");
        cb_oct_y.setText("(" + c_year + ")");
        cb_nov_y.setText("(" + c_year + ")");
        cb_dec_y.setText("(" + c_year + ")");
        cb_jan_y.setText("(" + n_year + ")");
        cb_feb_y.setText("(" + n_year + ")");
        cb_march_y.setText("(" + n_year + ")");

        if (april_m.equalsIgnoreCase("yes")) {
            cb_april.setEnabled(false);
            cb_april_p.setVisibility(View.VISIBLE);
            cb_april.setChecked(true);
            cb_april.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_april.setButtonDrawable(R.drawable.cb_selector1);
            april_recycler_view.setVisibility(View.GONE);
            april_m_fee = 0;
            april_m_fee_fine = 0;

        }else if (april_m.equalsIgnoreCase("pending")) {
            cb_april.setEnabled(false);
            cb_april_p.setVisibility(View.VISIBLE);
            cb_april_p.setText("(In Process)");
            cb_april_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
            cb_april.setChecked(true);
            cb_april.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_april.setButtonDrawable(R.drawable.cb_selector1);
            april_recycler_view.setVisibility(View.GONE);
            april_m_fee = 0;
            april_m_fee_fine = 0;
        }
        else{
            cb_april.setEnabled(false);
            cb_april_p.setVisibility(View.VISIBLE);
            cb_april_p.setText("(Pending)");
            cb_april_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_april.setChecked(true);
            cb_april.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_april.setButtonDrawable(R.drawable.cb_selector2);
            april_recycler_view.setVisibility(View.VISIBLE);
            april_m_fee = totalPrice1;
            april_m_fee_fine = april_m_fee_fine1;
            cb_april_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }


        if (may_m.equalsIgnoreCase("yes")) {
            cb_may.setEnabled(false);
            cb_may.setChecked(true);
            cb_may_p.setVisibility(View.VISIBLE);
            cb_may.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_may.setButtonDrawable(R.drawable.cb_selector1);
            may_recycler_view.setVisibility(View.GONE);
            may_m_fee = 0;
            may_m_fee_fine = 0;
        }else if (may_m.equalsIgnoreCase("pending")) {
            cb_may.setEnabled(false);
            cb_may.setChecked(true);
            cb_may_p.setVisibility(View.VISIBLE);
            cb_may.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_may.setButtonDrawable(R.drawable.cb_selector1);
            may_recycler_view.setVisibility(View.GONE);
            may_m_fee = 0;
            may_m_fee_fine = 0;
            cb_may_p.setText("(In Process)");
            cb_may_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_may.setEnabled(false);
            cb_may.setChecked(true);
            cb_may_p.setVisibility(View.VISIBLE);
            cb_may_p.setText("(Pending)");
            cb_may_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_may.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_may.setButtonDrawable(R.drawable.cb_selector2);
            may_recycler_view.setVisibility(View.VISIBLE);
            may_m_fee = totalPrice;
            may_m_fee_fine = may_m_fee_fine1;
            cb_may_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }


        if (june_m.equalsIgnoreCase("yes")) {
            cb_june.setEnabled(false);
            cb_june.setChecked(true);
            cb_june_p.setVisibility(View.VISIBLE);
            cb_june.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_june.setButtonDrawable(R.drawable.cb_selector1);
            jun_recycler_view.setVisibility(View.GONE);
            june_m_fee = 0;
            june_m_fee_fine = 0;
        }else if (june_m.equalsIgnoreCase("pending")) {
            cb_june.setEnabled(false);
            cb_june.setChecked(true);
            cb_june_p.setVisibility(View.VISIBLE);
            cb_june.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_june.setButtonDrawable(R.drawable.cb_selector1);
            jun_recycler_view.setVisibility(View.GONE);
            june_m_fee = 0;
            june_m_fee_fine = 0;
            cb_june_p.setText("(In Process)");
            cb_june_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_june.setEnabled(false);
            cb_june.setChecked(true);
            cb_june_p.setVisibility(View.VISIBLE);
            cb_june_p.setText("(Pending)");
            cb_june_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_june.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_june.setButtonDrawable(R.drawable.cb_selector2);
            jun_recycler_view.setVisibility(View.VISIBLE);
            june_m_fee = totalPrice;
            june_m_fee_fine = june_m_fee_fine1;
            cb_june_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }

        if (july_m.equalsIgnoreCase("yes")) {
            cb_july.setEnabled(false);
            cb_july.setChecked(true);
            cb_july_p.setVisibility(View.VISIBLE);
            cb_july.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_july.setButtonDrawable(R.drawable.cb_selector1);
            july_recycler_view.setVisibility(View.GONE);
            july_m_fee = 0;
            july_m_fee_fine = 0;
        }else if (july_m.equalsIgnoreCase("pending")) {
            cb_july.setEnabled(false);
            cb_july.setChecked(true);
            cb_july_p.setVisibility(View.VISIBLE);
            cb_july.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_july.setButtonDrawable(R.drawable.cb_selector1);
            july_recycler_view.setVisibility(View.GONE);
            july_m_fee = 0;
            july_m_fee_fine = 0;
            cb_july_p.setText("(In Process)");
            cb_july_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_july.setEnabled(false);
            cb_july.setChecked(true);
            cb_july_p.setVisibility(View.VISIBLE);
            cb_july_p.setText("(Pending)");
            cb_july_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_july.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_july.setButtonDrawable(R.drawable.cb_selector2);
            july_recycler_view.setVisibility(View.VISIBLE);
            july_m_fee = totalPrice;
            july_m_fee_fine = july_m_fee_fine1;
            cb_july_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }


        if (aug_m.equalsIgnoreCase("yes")) {
            cb_aug.setEnabled(false);
            cb_aug.setChecked(true);
            cb_aug_p.setVisibility(View.VISIBLE);
            cb_aug.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_aug.setButtonDrawable(R.drawable.cb_selector1);
            aug_recycler_view.setVisibility(View.GONE);
            aug_m_fee = 0;
            aug_m_fee_fine = 0;
        }else if (aug_m.equalsIgnoreCase("pending")) {
            cb_aug.setEnabled(false);
            cb_aug.setChecked(true);
            cb_aug_p.setVisibility(View.VISIBLE);
            cb_aug.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_aug.setButtonDrawable(R.drawable.cb_selector1);
            aug_recycler_view.setVisibility(View.GONE);
            aug_m_fee = 0;
            aug_m_fee_fine = 0;
            cb_aug_p.setText("(In Process)");
            cb_aug_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_aug.setEnabled(false);
            cb_aug.setChecked(true);
            cb_aug_p.setVisibility(View.VISIBLE);
            cb_aug_p.setText("(Pending)");
            cb_aug_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_aug.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_aug.setButtonDrawable(R.drawable.cb_selector2);
            aug_recycler_view.setVisibility(View.VISIBLE);
            aug_m_fee = totalPrice;
            aug_m_fee_fine = aug_m_fee_fine1;
            cb_aug_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }


        if (sep_m.equalsIgnoreCase("yes")) {
            cb_sep.setEnabled(false);
            cb_sep.setChecked(true);
            cb_sep_p.setVisibility(View.VISIBLE);
            cb_sep.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_sep.setButtonDrawable(R.drawable.cb_selector1);
            sep_recycler_view.setVisibility(View.GONE);
            sep_m_fee = 0;
            sep_m_fee_fine = 0;

        }else if (sep_m.equalsIgnoreCase("pending")) {
            cb_sep.setEnabled(false);
            cb_sep.setChecked(true);
            cb_sep_p.setVisibility(View.VISIBLE);
            cb_sep.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_sep.setButtonDrawable(R.drawable.cb_selector1);
            sep_recycler_view.setVisibility(View.GONE);
            sep_m_fee = 0;
            sep_m_fee_fine = 0;
            cb_sep_p.setText("(In Process)");
            cb_sep_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_sep.setEnabled(false);
            cb_sep.setChecked(true);
            cb_sep_p.setVisibility(View.VISIBLE);
            cb_sep_p.setText("(Pending)");
            cb_sep_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_sep.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_sep.setButtonDrawable(R.drawable.cb_selector2);
            sep_recycler_view.setVisibility(View.VISIBLE);
            sep_m_fee = totalPrice;
            sep_m_fee_fine = sep_m_fee_fine1;
            cb_sep_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }

        if (oct_m.equalsIgnoreCase("yes")) {
            cb_oct.setEnabled(false);
            cb_oct.setChecked(true);
            cb_oct_p.setVisibility(View.VISIBLE);
            cb_oct.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_oct.setButtonDrawable(R.drawable.cb_selector1);
            oct_recycler_view.setVisibility(View.GONE);
            oct_m_fee = 0;
            oct_m_fee_fine = 0;
        }else if (oct_m.equalsIgnoreCase("pending")) {
            cb_oct.setEnabled(false);
            cb_oct.setChecked(true);
            cb_oct_p.setVisibility(View.VISIBLE);
            cb_oct.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_oct.setButtonDrawable(R.drawable.cb_selector1);
            oct_recycler_view.setVisibility(View.GONE);
            oct_m_fee = 0;
            oct_m_fee_fine = 0;
            cb_oct_p.setText("(In Process)");
            cb_oct_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_oct.setEnabled(false);
            cb_oct.setChecked(true);
            cb_oct_p.setVisibility(View.VISIBLE);
            cb_oct_p.setText("(Pending)");
            cb_oct_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_oct.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_oct.setButtonDrawable(R.drawable.cb_selector2);
            oct_recycler_view.setVisibility(View.VISIBLE);
            oct_m_fee = totalPrice;
            oct_m_fee_fine = oct_m_fee_fine1;
            cb_oct_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }


        if (nov_m.equalsIgnoreCase("yes")) {
            cb_nov.setEnabled(false);
            cb_nov.setChecked(true);
            cb_nov_p.setVisibility(View.VISIBLE);
            cb_nov.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_nov.setButtonDrawable(R.drawable.cb_selector1);
            nov_recycler_view.setVisibility(View.GONE);
            nov_m_fee = 0;
            nov_m_fee_fine = 0;
        }else if (nov_m.equalsIgnoreCase("pending")) {
            cb_nov.setEnabled(false);
            cb_nov.setChecked(true);
            cb_nov_p.setVisibility(View.VISIBLE);
            cb_nov.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_nov.setButtonDrawable(R.drawable.cb_selector1);
            nov_recycler_view.setVisibility(View.GONE);
            nov_m_fee = 0;
            nov_m_fee_fine = 0;
            cb_nov_p.setText("(In Process)");
            cb_nov_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_nov.setEnabled(false);
            cb_nov.setChecked(true);
            cb_nov_p.setVisibility(View.VISIBLE);
            cb_nov_p.setText("(Pending)");
            cb_nov_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_nov.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_nov.setButtonDrawable(R.drawable.cb_selector2);
            nov_recycler_view.setVisibility(View.VISIBLE);
            nov_m_fee = totalPrice;
            nov_m_fee_fine = nov_m_fee_fine1;
            cb_nov_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }


        if (dec_m.equalsIgnoreCase("yes")) {
            cb_dec.setEnabled(false);
            cb_dec.setChecked(true);
            cb_dec_p.setVisibility(View.VISIBLE);
            cb_dec.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_dec.setButtonDrawable(R.drawable.cb_selector1);
            dec_recycler_view.setVisibility(View.GONE);
            dec_m_fee = 0;
            dec_m_fee_fine = 0;
        }else if (dec_m.equalsIgnoreCase("pending")) {
            cb_dec.setEnabled(false);
            cb_dec.setChecked(true);
            cb_dec_p.setVisibility(View.VISIBLE);
            cb_dec.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_dec.setButtonDrawable(R.drawable.cb_selector1);
            dec_recycler_view.setVisibility(View.GONE);
            dec_m_fee = 0;
            dec_m_fee_fine = 0;
            cb_dec_p.setText("(In Process)");
            cb_dec_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_dec.setEnabled(false);
            cb_dec.setChecked(true);
            cb_dec_p.setVisibility(View.VISIBLE);
            cb_dec_p.setText("(Pending)");
            cb_dec_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_dec.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_dec.setButtonDrawable(R.drawable.cb_selector2);
            dec_recycler_view.setVisibility(View.VISIBLE);
            dec_m_fee = totalPrice;
            dec_m_fee_fine = dec_m_fee_fine1;
            cb_dec_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }


        if (jan_m.equalsIgnoreCase("yes")) {
            cb_jan.setEnabled(false);
            cb_jan.setChecked(true);
            cb_jan_p.setVisibility(View.VISIBLE);
            cb_jan.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_jan.setButtonDrawable(R.drawable.cb_selector1);
            jan_recycler_view.setVisibility(View.GONE);
            jan_m_fee = 0;
            jan_m_fee_fine = 0;
        }else if (jan_m.equalsIgnoreCase("pending")) {
            cb_jan.setEnabled(false);
            cb_jan.setChecked(true);
            cb_jan_p.setVisibility(View.VISIBLE);
            cb_jan.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_jan.setButtonDrawable(R.drawable.cb_selector1);
            jan_recycler_view.setVisibility(View.GONE);
            jan_m_fee = 0;
            jan_m_fee_fine = 0;
            cb_jan_p.setText("(In Process)");
            cb_jan_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_jan.setEnabled(false);
            cb_jan.setChecked(true);
            cb_jan_p.setVisibility(View.VISIBLE);
            cb_jan_p.setText("(Pending)");
            cb_jan_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_jan.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_jan.setButtonDrawable(R.drawable.cb_selector2);
            jan_recycler_view.setVisibility(View.VISIBLE);
            jan_m_fee = totalPrice;
            jan_m_fee_fine = jan_m_fee_fine1;
            cb_jan_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }


        if (feb_m.equalsIgnoreCase("yes")) {
            cb_feb.setEnabled(false);
            cb_feb.setChecked(true);
            cb_feb_p.setVisibility(View.VISIBLE);
            cb_feb.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_feb.setButtonDrawable(R.drawable.cb_selector1);
            feb_recycler_view.setVisibility(View.GONE);
            feb_m_fee = 0;
            feb_m_fee_fine = 0;
        }else if (feb_m.equalsIgnoreCase("pending")) {
            cb_feb.setEnabled(false);
            cb_feb.setChecked(true);
            cb_feb_p.setVisibility(View.VISIBLE);
            cb_feb.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_feb.setButtonDrawable(R.drawable.cb_selector1);
            feb_recycler_view.setVisibility(View.GONE);
            feb_m_fee = 0;
            feb_m_fee_fine = 0;
            cb_feb_p.setText("(In Process)");
            cb_feb_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_feb.setEnabled(false);
            cb_feb.setChecked(true);
            cb_feb_p.setVisibility(View.VISIBLE);
            cb_feb_p.setText("(Pending)");
            cb_feb_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_feb.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_feb.setButtonDrawable(R.drawable.cb_selector2);
            feb_recycler_view.setVisibility(View.VISIBLE);
            feb_m_fee = totalPrice;
            feb_m_fee_fine = feb_m_fee_fine1;
            cb_feb_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }

        if (mar_m.equalsIgnoreCase("yes")) {
            cb_march.setEnabled(false);
            cb_march.setChecked(true);
            cb_march_p.setVisibility(View.VISIBLE);
            cb_march.setTextColor(
                    getResources().getColor(
                            R.color.light_grey));
            cb_march.setButtonDrawable(R.drawable.cb_selector1);
            mar_recycler_view.setVisibility(View.GONE);
            mar_m_fee = 0;
            mar_m_fee_fine = 0;
        }else if (mar_m.equalsIgnoreCase("pending")) {
            cb_march.setEnabled(false);
            cb_march.setChecked(true);
            cb_march_p.setVisibility(View.VISIBLE);
            cb_march.setTextColor(
                    getResources().getColor(
                            R.color.light_grey1));
            cb_march.setButtonDrawable(R.drawable.cb_selector1);
            mar_recycler_view.setVisibility(View.GONE);
            mar_m_fee = 0;
            mar_m_fee_fine = 0;
            cb_march_p.setText("(In Process)");
            cb_march_p.setTextColor(
                    getResources().getColor(
                            R.color.present));
        }
        else{
            cb_march.setEnabled(false);
            cb_march.setChecked(true);
            cb_march_p.setVisibility(View.VISIBLE);
            cb_march_p.setText("(Pending)");
            cb_march_p.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave_n));
            cb_march.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
            cb_march.setButtonDrawable(R.drawable.cb_selector2);
            mar_recycler_view.setVisibility(View.VISIBLE);
            mar_m_fee = totalPrice;
            mar_m_fee_fine = mar_m_fee_fine1;
            cb_march_y.setTextColor(
                    getResources().getColor(
                            R.color.dark_leave));
        }

        setTotals();

    }

    private void setTotals() {
        // totalPrice_withoutTrans = totalPrice + p_balance;
        // new_totalPrice = p_balance + totalPrice + trans_amount;


        fine_totalPrice = jan_m_fee_fine + feb_m_fee_fine + mar_m_fee_fine + april_m_fee_fine + may_m_fee_fine + june_m_fee_fine + july_m_fee_fine +
                aug_m_fee_fine + sep_m_fee_fine + oct_m_fee_fine + nov_m_fee_fine + dec_m_fee_fine;


        sub_totalPrice = jan_m_fee + feb_m_fee + mar_m_fee + april_m_fee + may_m_fee +
                june_m_fee + july_m_fee + aug_m_fee + sep_m_fee + oct_m_fee + nov_m_fee + dec_m_fee;

        new_totalPrice = fine_totalPrice + p_balance + sub_totalPrice;

        totalPrice_withoutTrans = new_totalPrice;


        a_sub.setText(String.valueOf(sub_totalPrice));
        f_sub.setText(String.valueOf(fine_totalPrice));

        p_amount.setText(String.valueOf(new_totalPrice));
        totals.setText(String.valueOf(new_totalPrice)+".00");

        if (new_totalPrice == p_balance) {
            top.setVisibility(View.GONE);
            bottom.setVisibility(View.GONE);
            textNorecord.setVisibility(View.VISIBLE);
            textNorecord.setText("No pending fees\n month found!");
            remarklyt.setVisibility(View.GONE);
            pay_close.setVisibility(View.GONE);
        } else {
            //  month_lyt.setVisibility(View.GONE);
            textNorecord.setVisibility(View.GONE);
            top.setVisibility(View.VISIBLE);
            bottom.setVisibility(View.GONE);
            remarklyt.setVisibility(View.GONE);
            pay_close.setVisibility(View.GONE);
        }
    }

    private void fetchFeesAmount(String fromdate, String todate) {
        // ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", "all");
        params.put("c_id", classid);
        params.put("s_id", regist);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, apiCallback2, this, params);
    }

    ApiHandler.ApiCallback apiCallback2 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                //  Toast.makeText(getApplicationContext(),"select another Month",Toast.LENGTH_SHORT).show();

                try {
                    list = FeeBalanceBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                    p_balance = Integer.parseInt(list.get(0).getFee_bal());
                    p_fine_balance = Integer.parseInt(list.get(0).getBalance_fine());

                    // Toast.makeText(getApplicationContext(),p_balance+"\n"+p_fine_balance,Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setFeeData(p_balance, p_fine_balance);

            } else {
                ErpProgress.cancelProgressBar();

                if (p_fine_balance == 0) {
                    fine_close.setVisibility(View.GONE);
                    //  fine_view.setVisibility(View.VISIBLE);
                }
                //    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
//                fetchFeesAmount();
//                fetchPreviousbalance();
            }
        }
    };

    private void setFeeData(int p_balance, int p_fine_balance) {


        if (p_fine_balance == 0) {
            fine_close.setVisibility(View.GONE);
            // fine_view.setVisibility(View.VISIBLE);
        }

        fine_balance.setText(String.valueOf(p_fine_balance));
        total_fine.setText(String.valueOf(p_fine_balance));
        balance.setText(String.valueOf(p_balance)+".00");

        //   totals.setText(String.valueOf(new_totalPrice));
    }


    private void fetchPreviousbalance1(String m, String mc) {
        totalPrice1 = 0;
        // ErpProgress.showProgressBar(PayFee_New.this, "Please wait...");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

//                top.setVisibility(View.VISIBLE);
//                bottom.setVisibility(View.VISIBLE);

                    try {
                        if (classList1 != null) {
                            classList1.clear();
                        }

                        classList1 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter.setFeeList(classList1);
                        newsAdapter.notifyDataSetChanged();
                        for (int i = 0; i < classList1.size(); i++) {
                            totalPrice1 += Integer.parseInt(classList1.get(i).getFee_amount());
                            april_m_fee_fine1 += Integer.parseInt(classList1.get(i).getEs_fine());
                        }

                        checkPaidMonth();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    ErpProgress.cancelProgressBar();
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    classList1.clear();
                    newsAdapter.setFeeList(classList1);
                    newsAdapter.notifyDataSetChanged();
                    april_recycler_view.setVisibility(View.GONE);
                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance2(String m, String mc) {
        totalPrice = 0;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

                if (error == null) {

                    try {
                        if (classList2 != null) {
                            classList2.clear();
                        }

                        classList2 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter1.setFeeList(classList2);
                        newsAdapter1.notifyDataSetChanged();

                        for (int i = 0; i < classList2.size(); i++) {
                            totalPrice += Integer.parseInt(classList2.get(i).getFee_amount());
                            may_m_fee_fine1 += Integer.parseInt(classList2.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();

                        checkPaidMonth();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList2.clear();
                    newsAdapter1.setFeeList(classList2);
                    newsAdapter1.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance3(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {

                    try {
                        if (classList3 != null) {
                            classList3.clear();
                        }

                        classList3 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter2.setFeeList(classList3);
                        newsAdapter2.notifyDataSetChanged();

                        for (int i = 0; i < classList3.size(); i++) {
                            june_m_fee_fine1 += Integer.parseInt(classList3.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList3.clear();
                    newsAdapter2.setFeeList(classList3);
                    newsAdapter2.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance4(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

                if (error == null) {

                    try {
                        if (classList4 != null) {
                            classList4.clear();
                        }

                        classList4 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter3.setFeeList(classList4);
                        newsAdapter3.notifyDataSetChanged();

                        for (int i = 0; i < classList4.size(); i++) {
                            july_m_fee_fine1 += Integer.parseInt(classList4.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList4.clear();
                    newsAdapter3.setFeeList(classList4);
                    newsAdapter3.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance5(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        if (classList5 != null) {
                            classList5.clear();
                        }
                        classList5 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter4.setFeeList(classList5);
                        newsAdapter4.notifyDataSetChanged();

                        for (int i = 0; i < classList5.size(); i++) {
                            aug_m_fee_fine1 += Integer.parseInt(classList5.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList5.clear();
                    newsAdapter4.setFeeList(classList5);
                    newsAdapter4.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance6(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        if (classList6 != null) {
                            classList6.clear();
                        }

                        classList6 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter5.setFeeList(classList6);
                        newsAdapter5.notifyDataSetChanged();

                        for (int i = 0; i < classList6.size(); i++) {
                            sep_m_fee_fine1 += Integer.parseInt(classList6.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList6.clear();
                    newsAdapter5.setFeeList(classList6);
                    newsAdapter5.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance7(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        if (classList7 != null) {
                            classList7.clear();
                        }

                        classList7 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter6.setFeeList(classList7);
                        newsAdapter6.notifyDataSetChanged();

                        for (int i = 0; i < classList7.size(); i++) {
                            oct_m_fee_fine1 += Integer.parseInt(classList7.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList7.clear();
                    newsAdapter6.setFeeList(classList7);
                    newsAdapter6.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance8(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        if (classList8 != null) {
                            classList8.clear();
                        }

                        classList8 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter7.setFeeList(classList8);
                        newsAdapter7.notifyDataSetChanged();

                        for (int i = 0; i < classList8.size(); i++) {
                            nov_m_fee_fine1 += Integer.parseInt(classList8.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList8.clear();
                    newsAdapter7.setFeeList(classList8);
                    newsAdapter7.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance9(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                    try {
                        if (classList9 != null) {
                            classList9.clear();
                        }

                        classList9 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter8.setFeeList(classList9);
                        newsAdapter8.notifyDataSetChanged();

                        for (int i = 0; i < classList9.size(); i++) {
                            dec_m_fee_fine1 += Integer.parseInt(classList9.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList9.clear();
                    newsAdapter8.setFeeList(classList9);
                    newsAdapter8.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance10(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                    try {
                        if (classList10 != null) {
                            classList10.clear();
                        }

                        classList10 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter9.setFeeList(classList10);
                        newsAdapter9.notifyDataSetChanged();

                        for (int i = 0; i < classList10.size(); i++) {
                            jan_m_fee_fine1 += Integer.parseInt(classList10.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList10.clear();
                    newsAdapter9.setFeeList(classList10);
                    newsAdapter9.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance11(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                    try {
                        if (classList11 != null) {
                            classList11.clear();
                        }

                        classList11 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter10.setFeeList(classList11);
                        newsAdapter10.notifyDataSetChanged();

                        for (int i = 0; i < classList11.size(); i++) {
                            feb_m_fee_fine1 += Integer.parseInt(classList11.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList11.clear();
                    newsAdapter10.setFeeList(classList11);
                    newsAdapter10.notifyDataSetChanged();

                }
            }
        }, this, params);
    }

    private void fetchPreviousbalance12(String m, String mc) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", m);
        params.put("month_code", mc);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    ErpProgress.cancelProgressBar();
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                    try {
                        if (classList12 != null) {
                            classList12.clear();
                        }

                        classList12 = ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                        newsAdapter11.setFeeList(classList12);
                        newsAdapter11.notifyDataSetChanged();
                        for (int i = 0; i < classList12.size(); i++) {
                            mar_m_fee_fine1 += Integer.parseInt(classList12.get(i).getEs_fine());
                        }
                        //  Toast.makeText(getApplicationContext(), "totals " + totalPrice, Toast.LENGTH_SHORT).show();
                        checkPaidMonth();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    classList12.clear();
                    newsAdapter11.setFeeList(classList12);
                    newsAdapter11.notifyDataSetChanged();
                }
            }
        }, this, params);
    }




    private void savePrefData() {
        sharedPreferences = CommonSTDPendingFeesListActivity.this.getSharedPreferences("boarding_pref_list", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_list", true);
        editor.apply();
    }

    private boolean restorePrefData() {
        sharedPreferences = CommonSTDPendingFeesListActivity.this.getSharedPreferences("boarding_pref_list", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_list", false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support, "View Student Fees History!", "Tap the View button to view Student Fees History.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40),TapTarget.forView(dialog_click,getString(R.string.list_title),getString(R.string.list_desc))
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        // commonIntent();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();


    }

    @OnClick(R.id.dialog)
    public void dialogclick() {
        dialog_click.startAnimation(animation);

        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")){


            Dialog dialogLog = new Dialog(this);
            dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogLog.setCancelable(true);
            dialogLog.setContentView(R.layout.enter_mobile_dialog);
            dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView img = (ImageView) dialogLog.findViewById(R.id.img);

            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.logo).into(img);
            } else {
                Picasso.get().load(R.drawable.logo).into(img);
            }


            Button save = (Button) dialogLog.findViewById(R.id.submit);
            save.setVisibility(View.VISIBLE);
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // btnNo.startAnimation(animation4);
                    save.startAnimation(animation);
                    BitmapDrawable bitmapDrawable = (BitmapDrawable) img.getDrawable();
                    Bitmap bitmap = bitmapDrawable.getBitmap();
                    saveImageToGallery(bitmap,"FeePay_qr_saved.jpg");
                }
            });

            RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // btnNo.startAnimation(animation4);
                    dialogLog.dismiss();
                }
            });
            dialogLog.show();

        }else {
            Toast.makeText(this, "Online Fees Payment QR Code Image Not Found!", Toast.LENGTH_SHORT).show();
        }

    }

    private void saveImageToGallery(Bitmap bitmap,String name){
        String school=User.getCurrentUser().getSchoolData().getName();
        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, name);
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        //  export.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.contact_support)
    public void contact_support() {
        contact_support.startAnimation(animation);
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        commonIntent();
    }

    private void commonIntent() {
        Intent intent = new Intent(CommonSTDPendingFeesListActivity.this, CommonFeesListActivity.class);
        intent.putExtra("list_view", "add");
//        intent.putExtra("f_name", f_name);
//        intent.putExtra("roll", roll);
//        intent.putExtra("dp", image);
//        intent.putExtra("regist", regist);
//        intent.putExtra("class", classid);
//        intent.putExtra("class_name", class_name);
//        intent.putExtra("gender", gender);
//        intent.putExtra("fee_cate_id", fee_cate_id);
//        intent.putExtra("route_id", route_id);
//        intent.putExtra("place_id", place_id);
//        intent.putExtra("fromdate", fromdate);
//        intent.putExtra("todate", todate);
//        intent.putExtra("month", selectMonth);
//        intent.putExtra("tag", "student_wise");
        startActivity(intent);
    }

    private void setDataNew() {

        if (!restorePrefData()) {
            setShowcaseView();
        }
        // txtTitle.setText(class_name+" Pay Fees");
        txtTitle.setText("Pending Fees");
        //  student_lyt.setVisibility(View.VISIBLE);

        if (gender.equalsIgnoreCase("Male")) {
            if (image != null) {
                Utils.progressImg(image, dp, CommonSTDPendingFeesListActivity.this);
                // Picasso.get().load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
            } else {
                Picasso.get().load(R.drawable.stud).into(dp);
            }
        } else {
            if (image != null) {
                Utils.progressImg(image, dp, CommonSTDPendingFeesListActivity.this);
                //  Picasso.get().load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
            } else {
                Picasso.get().load(R.drawable.stud).into(dp);
            }
        }

        txtSub.setText(name + "(" + regist + ")");
        edtName.setText(f_name);
        edtRegistration.setText(roll);

        //view_std.setVisibility(View.VISIBLE);
        student_lyt.setVisibility(View.GONE);
    }


}