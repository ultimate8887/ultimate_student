package com.ultimate.ultimatesmartstudent.FeeModule.Fees_Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AccountModule.SchoolInfo;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.FeeModule.MonthlyFees;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddBankDetails extends AppCompatActivity {

    @BindView(R.id.c_name)
    TextView txtTitle;
    @BindView(R.id.export)
    TextView export;

    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.ano)
    EditText ano;
    @BindView(R.id.upi)
    EditText upi;
    @BindView(R.id.aname)
    EditText aname;
    @BindView(R.id.bname)
    EditText bname;
    @BindView(R.id.visitaddress)
    EditText address;
    Animation animation;
    @BindView(R.id.code)
    EditText ifsc;
    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;
    CommonProgress commonProgress;
    String bank_n="",acc_n="",acc_no="",ifsc_code="",bank_add="",image_url="",upi_link="";
    @BindView(R.id.noNoticeData)
    TextView noNoticeData;
    @BindView(R.id.topppp)
    LinearLayout topppp;
    @BindView(R.id.upi_l)
    TextView upi_l;
    @BindView(R.id.upii)
    TextInputLayout upii;
    @BindView(R.id.circleimgrecepone)
    CircularImageView chooseimage;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank_details);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        if (!restorePrefData()){
            setShowcaseView();
        }
        userprofile();
    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();


                if (error == null) {
                    try {

                        image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("qr_img");
                        upi_link = jsonObject.getJSONObject(Constants.USERDATA).getString("upi");
                        bank_n=jsonObject.getJSONObject(Constants.USERDATA).getString("bank_name");
                        acc_n=jsonObject.getJSONObject(Constants.USERDATA).getString("acc_name");
                        acc_no=jsonObject.getJSONObject(Constants.USERDATA).getString("acc_no");
                        ifsc_code=jsonObject.getJSONObject(Constants.USERDATA).getString("ifsc_code");
                        bank_add=jsonObject.getJSONObject(Constants.USERDATA).getString("bank_address");

                        String image_url_new=jsonObject.getJSONObject(Constants.USERDATA).getString("logo");
                        if (!image_url_new.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
                            Picasso.get().load(image_url_new).placeholder(R.drawable.boy).into(chooseimage);
                        } else {
                            Picasso.get().load(R.drawable.logo).into(chooseimage);
                        }

                        if (!upi_link.equalsIgnoreCase("")){
                            upi_l.setVisibility(View.VISIBLE);
                            upii.setVisibility(View.VISIBLE);
                            upi.setText(upi_link);
                        }else{
                            upi_l.setVisibility(View.GONE);
                            upii.setVisibility(View.GONE);
                            upi.setText("");
                        }


                        txtTitle.setText(jsonObject.getJSONObject(Constants.USERDATA).getString("name"));
                        if (!bank_n.equalsIgnoreCase("")) {
                            bname.setText(bank_n);
                            aname.setText(acc_n);
                            ano.setText(acc_no);
                            address.setText(bank_add);
                            ifsc.setText(ifsc_code);
                            noNoticeData.setVisibility(View.GONE);
                            topppp.setVisibility(View.VISIBLE);
                        }else {
                            topppp.setVisibility(View.GONE);
                            noNoticeData.setVisibility(View.VISIBLE);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @OnClick(R.id.dialog)
    public void contact_support() {
        dialog.startAnimation(animation);
        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")){


            Dialog dialogLog = new Dialog(this);
            dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogLog.setCancelable(true);
            dialogLog.setContentView(R.layout.enter_mobile_dialog);
            dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView img = (ImageView) dialogLog.findViewById(R.id.img);

            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.logo).into(img);
            } else {
                Picasso.get().load(R.drawable.logo).into(img);
            }


            Button save = (Button) dialogLog.findViewById(R.id.submit);
            save.setVisibility(View.VISIBLE);
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // btnNo.startAnimation(animation4);
                    save.startAnimation(animation);
                    BitmapDrawable bitmapDrawable = (BitmapDrawable) img.getDrawable();
                    Bitmap bitmap = bitmapDrawable.getBitmap();
                    saveImageToGallery(bitmap,"FeePay_qr_saved.jpg");
                }
            });

            RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // btnNo.startAnimation(animation4);
                    dialogLog.dismiss();
                }
            });
            dialogLog.show();

        }else {
            Toast.makeText(this, "Online Fees Payment QR Code Image Not Found!", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.export)
    public void export() {
// create bitmap screen capture
        export.startAnimation(animation);
        export.setVisibility(View.GONE);
        Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
        // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        // v1.setDrawingCacheEnabled(false);
        saveImageToGallery(bitmap,"Bank_details.jpg");
    }

    //create bitmap from the ScrollView
    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }

    private void saveImageToGallery(Bitmap bitmap,String name){
        String school=User.getCurrentUser().getSchoolData().getName();
        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, name);
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        export.setVisibility(View.VISIBLE);
    }

    private void savePrefData(){
        sharedPreferences= AddBankDetails.this.getSharedPreferences("boarding_pref_list",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_list",true);
        editor.apply();
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        imgBackmsg.startAnimation(animation);
        finish();
    }



    private boolean restorePrefData(){
        sharedPreferences = AddBankDetails.this.getSharedPreferences("boarding_pref_list",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_list",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,getString(R.string.list_title),getString(R.string.list_desc))
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        // commonIntent();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();

    }

}