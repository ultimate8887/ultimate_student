package com.ultimate.ultimatesmartstudent.FeeModule.NewFees;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.SessionBean;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.FeeModule.Fee_Paid_Bean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommonFeesListActivity extends AppCompatActivity implements PaidFeeListAdapter.ProdMethodCallBack {


    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @BindView(R.id.imgBack)
    ImageView imgBackmsg;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.lyttt1)
    RelativeLayout lyttt;

    private LinearLayoutManager layoutManager;
    ArrayList<Fee_Paid_Bean> list;
    private PaidFeeListAdapter mAdapter;
    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.spinnerMonth)
    Spinner spinnerMonth;
    ArrayList<String> monthList = new ArrayList<>();
    CommonProgress commonProgress;
    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    String list_view="";
    int limit = 100, page_limit = 0, total_pages = 0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    String selectMonth = "", name = "", f_name = "", roll = "",
            image = "", gender = "", regist = "", classid = "",
            fee_cate_id = "", fromdate = "", todate = "", class_name = "", place_id = "", route_id = "", tag = "", month_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_fees_list);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);
        list = new ArrayList<>();

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);


        if (getIntent().getExtras() != null) {
            list_view = getIntent().getExtras().getString("list_view");

            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            if (list_view.equalsIgnoreCase("add")){
                txtTitle.setText(getString(R.string.fee));
                mAdapter = new PaidFeeListAdapter(list, this, this, 1);
            }else{
                txtTitle.setText(getString(R.string.view_res));
                mAdapter = new PaidFeeListAdapter(list, this, this, 2);
            }
            recyclerView.setAdapter(mAdapter);
        }


        //fetchFeeDetail(selectMonth);

        name = User.getCurrentUser().getFirstname()+" "+User.getCurrentUser().getLastname();
        f_name = User.getCurrentUser().getFather();
        image = User.getCurrentUser().getProfile();
        gender = User.getCurrentUser().getGender();
        roll = User.getCurrentUser().getId();
        regist = User.getCurrentUser().getId();
        classid = User.getCurrentUser().getClass_id();
        class_name = User.getCurrentUser().getClass_name();
        lyttt.setVisibility(View.GONE);
        txtSub.setVisibility(View.VISIBLE);
        txtSub.setText(name + "(" + regist + ")");

        fetchSession();

    }
    private void fetchSession() {
        // ErpProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    fromdate=sessionlist.get(0).getStart_date();
                    todate=sessionlist.get(0).getEnd_date();
                    fetchFeeDetail(0, selectMonth, limit, "yes",fromdate,todate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //  Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchFeeDetail(int i, String selectMonth, int limit, String progress,String fromdate, String todate) {


        if (progress.equalsIgnoreCase("yes")) {
            commonProgress.show();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", User.getCurrentUser().getId());
        params.put("tag", "student_wise");
        params.put("page_limit", String.valueOf(limit));
        params.put("month", selectMonth);
        params.put("classid", User.getCurrentUser().getClass_id());
        params.put("list_view", list_view);
        params.put("list_type", "new");
        params.put("todate", todate);
        params.put("fromdate", fromdate);
        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT_NEW;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                if (list != null)
                    list.clear();

                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    list = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    if (list.size() > 0) {
                        mAdapter.setMerchantBeans(list);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- " + String.valueOf(list.size()));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        mAdapter.setMerchantBeans(list);
                        mAdapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                mAdapter.setMerchantBeans(list);
                mAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {

        String start_year= Utils.getYearOnlyNEW(fromdate);
        String end_year= Utils.getYearOnlyNEW(todate);
        Gson gson = new Gson();
        String prod_data = gson.toJson(fee_paid_bean, Fee_Paid_Bean.class);
        Intent intent = new Intent(CommonFeesListActivity.this, FeeDetailsActivity_New.class);
        intent.putExtra("fee_data", prod_data);
        intent.putExtra("fromdate", start_year);
        intent.putExtra("todate", end_year);
        startActivity(intent);

    }

    @Override
    public void clickRECMethod(Fee_Paid_Bean fee_paid_bean) {
        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView img = (ImageView) dialogLog.findViewById(R.id.img);

        if (fee_paid_bean.getScreenshot()!=null) {
            Picasso.get().load(fee_paid_bean.getScreenshot()).placeholder(R.drawable.logo).into(img);
        }

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
    }
}