package com.ultimate.ultimatesmartstudent.FeeModule;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AccountModule.SchoolInfo;
import com.ultimate.ultimatesmartstudent.AddDocument.AdddocumentActivity;
import com.ultimate.ultimatesmartstudent.AttendMod.AttendMod;
import com.ultimate.ultimatesmartstudent.BeanModule.SessionBean;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MonthlyFees extends AppCompatActivity implements FeeListAdapter.ProdMethodCallBack {

    @BindView(R.id.spinnerMonth)
    Spinner spinnerMonth;
    @BindView(R.id.spinnerFinancialYear)
    Spinner spinnerFinancialYear;

    ArrayList<String> monthList = new ArrayList<>();
    ArrayList<ExtraDetails> extraDetails = new ArrayList<>();
    String selectMonth ="",month_name="",fee_cate_id="",place_id="",route_id="";
    CommonProgress commonProgress;
    int totalPrice = 0,trans_amount= 0, p_balance=0, p_fine_balance=0, new_totalPrice=0,check=0, totalPrice_withoutTrans=0,selection = 0;
    ArrayList<FeeBalanceBean> list = new ArrayList<>();

    @BindView(R.id.totalRecord)
    TextView totalRecord;
    //TransAmount
    @BindView(R.id.recycler_trans)
    RecyclerView recycler_trans;
    private TransAmountAdap transAmountAdap;
    ArrayList<TransAmount> transAmounts = new ArrayList<>();

    @BindView(R.id.fee_close)
    LinearLayout fee_close;


    //FeeListAdapter
    RecyclerView recyclerView;
    ArrayList<Fee_Paid_Bean> paid_beanslist= new ArrayList<>();
    private FeeListAdapter mAdapter;
    ImageView close;
    TextView txtSetup;
    TextView txt_list;

    //ParticularBean
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    RecyclerView.LayoutManager layoutManager;
    private Particular_Adapter newsAdapter;
    ArrayList<ParticularBean> classList = new ArrayList<>();
    ImageView imgShow;
    @BindView(R.id.contact_support)
    ImageView contact_support;
    SharedPreferences sharedPreferences;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.f_details)
    TextView f_details;
    @BindView(R.id.totals)
    TextView totals;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    Animation animation;
    String image_url="",school="";
    @BindView(R.id.noNoticeData)
    TextView noNoticeData;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_fees);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);


        school=User.getCurrentUser().getSchoolData().getName();

        //TransAmount
        layoutManager = new LinearLayoutManager(MonthlyFees.this);
        recycler_trans.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        transAmountAdap = new TransAmountAdap(transAmounts,MonthlyFees.this);
        recycler_trans.setAdapter(transAmountAdap);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        //ParticularBean
        layoutManager = new LinearLayoutManager(MonthlyFees.this);
        recycler_view.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        newsAdapter = new Particular_Adapter(classList,MonthlyFees.this);
        recycler_view.setAdapter(newsAdapter);

        if (!restorePrefData()){
            setShowcaseView();
        }

        fetchSession();
        userprofile();
       // fetchmonthList();
    }

    private void savePrefData(){
        sharedPreferences= MonthlyFees.this.getSharedPreferences("boarding_pref_list",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_list",true);
        editor.apply();
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }



    private boolean restorePrefData(){
        sharedPreferences = MonthlyFees.this.getSharedPreferences("boarding_pref_list",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_list",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,getString(R.string.list_title),getString(R.string.list_desc))
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
               // commonIntent();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();


    }

    @OnClick(R.id.contact_support)
    public void contact_support() {
        contact_support.startAnimation(animation);



        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")){


            Dialog dialogLog = new Dialog(this);
            dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogLog.setCancelable(true);
            dialogLog.setContentView(R.layout.enter_mobile_dialog);
            dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView img = (ImageView) dialogLog.findViewById(R.id.img);

            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
            Picasso.get().load(image_url).placeholder(R.drawable.logo).into(img);
             } else {
            Picasso.get().load(R.drawable.logo).into(img);
             }


            Button save = (Button) dialogLog.findViewById(R.id.submit);
            save.setVisibility(View.VISIBLE);
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // btnNo.startAnimation(animation4);
                    save.startAnimation(animation);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) img.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);
                }
            });

            RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // btnNo.startAnimation(animation4);
                    dialogLog.dismiss();
                }
            });
            dialogLog.show();

//        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
//        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
//        mBottomSheetDialog.setContentView(sheetView);
//        final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);
//        mBottomSheetDialog.setCancelable(true);
//
//        Animation animation;
//        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
//        imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);
//        TextView title = (TextView) sheetView.findViewById(R.id.title);
//        TextView path = (TextView) sheetView.findViewById(R.id.path);
//        path.setText("Internal Storage/Pictures/"+school+"/QRImage_.jpg");
//        TextView imgDownload = (TextView) sheetView.findViewById(R.id.imgDownload);
////        title.setText(obj.getTitle());
////        Picasso.get().load(obj.getImage()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);
//
//        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
//            Picasso.with(MonthlyFees.this).load(image_url).placeholder(R.drawable.logo).into(imgShow);
//        } else {
//            Picasso.with(MonthlyFees.this).load(R.drawable.logo).into(imgShow);
//        }
//
//
//        imgDownload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                imgDownload.startAnimation(animation);
//                //  UltimateProgress.showProgressBar(NoticeActivity.this, "downloading...");
//                //  Picasso.with(NoticeActivity.this).load(obj.getImage()).into(imgTraget);
//                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
//                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
//                Bitmap bitmap = bitmapDrawable.getBitmap();
//                saveImageToGallery(bitmap);
//
//            }
//        });
//        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
//        btnNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnNo.startAnimation(animation);
//                mBottomSheetDialog.dismiss();
//            }
//        });
//        mBottomSheetDialog.show();
    }else {
            Toast.makeText(this, "Online Fees Payment QR Code Image Not Found!", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "FeePay_qr_saved" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

               //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "FeePay_qr_saved" + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                //  ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {

                        image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("qr_img");

//                        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
//                            Picasso.with(MonthlyFees.this).load(image_url).placeholder(R.drawable.boy).into(imgShow);
//                        } else {
//                            Picasso.with(MonthlyFees.this).load(R.drawable.logo).into(imgShow);
//                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void fetchExra(SessionBean session) {
        HashMap<String, String> params = new HashMap<>();
        params.put("s_id", User.getCurrentUser().getId());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EXTRADETAILS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                if (error == null) {
                    try {

                        String fee_cate_id="",place_id="",route_id="";
                        extraDetails = ExtraDetails.parseParticularBeanArray(jsonObject.getJSONArray("extra_data"));

                          fee_cate_id= extraDetails.get(0).getFee_cat_id();

                            route_id=extraDetails.get(0).getRoute_id();

                            place_id=extraDetails.get(0).getPlace_id();

                        Log.e("USERDATA", fee_cate_id+"\n"+route_id+"\n"+place_id);

                        fetchFeesAmount(session);
                        fetchPreviousbalance(session,fee_cate_id);
                        fetTranAmount(session,place_id,route_id);

                        //  Toast.makeText(AdddocumentActivity.this, "Documents list!", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    fetchFeesAmount(session);
                    fetchPreviousbalance(session,fee_cate_id);
                    fee_close.setVisibility(View.VISIBLE);
                    noNoticeData.setVisibility(View.GONE);
                 //   Toast.makeText(MonthlyFees.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);

    }

    private void fetchSession() {
       // ErpProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
           // ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(MonthlyFees.this, sessionlist);
                    spinnerFinancialYear.setAdapter(adapter);
                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            SessionBean session = sessionlist.get(i);
                            fetchmonthList(session);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
              //  Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList(SessionBean session) {

        Calendar cal=Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        month_name = month_date.format(cal.getTime());

        if (month_name.equalsIgnoreCase("April")){
            monthList.add("April");
        }else if (month_name.equalsIgnoreCase("May")){
            monthList.add("April");
            monthList.add("May");
        }else if (month_name.equalsIgnoreCase("June")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
        }else if (month_name.equalsIgnoreCase("July")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
        }else if (month_name.equalsIgnoreCase("August")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
        }else if (month_name.equalsIgnoreCase("September")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
        }else if (month_name.equalsIgnoreCase("October")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
        }else if (month_name.equalsIgnoreCase("November")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
        }else if (month_name.equalsIgnoreCase("December")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
            monthList.add("December");
        }else if (month_name.equalsIgnoreCase("January")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
            monthList.add("December");
            monthList.add("January");
        }else if (month_name.equalsIgnoreCase("February")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
            monthList.add("December");
            monthList.add("January");
            monthList.add("February");
        }else {
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
            monthList.add("December");
            monthList.add("January");
            monthList.add("February");
            monthList.add("March");
        }


        //   Toast.makeText(getApplicationContext(),month_name,Toast.LENGTH_SHORT).show();


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, monthList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);

//        Calendar cal=Calendar.getInstance();
//        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
//        month_name = month_date.format(cal.getTime());

        // setting here spinner item selected with position..
        for(int i=0;i<monthList.size();i++) {
            if (monthList.get(i).equalsIgnoreCase(month_name)) {
                selectMonth = monthList.get(i);
                spinnerMonth.setSelection(i);

            }
        }

        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                selectMonth = monthList.get(pos);
                fetchFeesDetails(selectMonth,session);

                if (!selectMonth.equalsIgnoreCase("")) {
                    String title = getColoredSpanned(selectMonth, "#F55B53");
                    String Name = getColoredSpanned(" MONTH FEES DETAILS", "#000000");
                   // f_details.setText(Html.fromHtml(title + " " + Name));
                    f_details.setText(Html.fromHtml("'"+title+"'" + " " + Name));
                    // txt_list.setText(Html.fromHtml(title + " " + Name));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    private void fetchFeesDetails(String month,SessionBean session) {

        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", month);
        params.put("c_id", User.getCurrentUser().getClass_id());
        params.put("s_id", User.getCurrentUser().getId());
        params.put("fromdate", session.getStart_date());
        params.put("todate", session.getEnd_date());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    //  ErpProgress.cancelProgressBar();
                    commonProgress.dismiss();
                    openDialog("start");
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                    Toast.makeText(getApplicationContext(),"You have already paid "+selectMonth+" Month Fees \n kindly select another Month",Toast.LENGTH_LONG).show();
                    fee_close.setVisibility(View.GONE);
                    noNoticeData.setVisibility(View.VISIBLE);
                    totalRecord.setText(getString(R.string.t_entries)+" 0");
                } else {
                    //  ErpProgress.cancelProgressBar();
                    commonProgress.dismiss();
                    fetchExra(session);
                    //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    private void openDialog(String value) {

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.fee_success_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(false);

        recyclerView=(RecyclerView) sheetView.findViewById(R.id.recyclerview_list);
        close=(ImageView) sheetView.findViewById(R.id.close);
        txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
//        txtNorecord=(TextView) sheetView.findViewById(R.id.textNorecord);
        txt_list=(TextView) sheetView.findViewById(R.id.txt_list);

        if (value.equalsIgnoreCase("start")){
          //  txtSetup.setText("This Month Fees \nAlready Paid!");
            txtSetup.setText(getString(R.string.paid_fees));
        }else{
            //txtSetup.setText("Fees Paid Successfully \nThank You!");
        }

        //FeeListAdapter
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeeListAdapter(paid_beanslist,this, this);
        recyclerView.setAdapter(mAdapter);

        if (!selectMonth.equalsIgnoreCase("")) {
            String title = getColoredSpanned(selectMonth, "#F55B53");
            String Name = getColoredSpanned(" MONTH FEES DETAILS", "#000000");
            txt_list.setText(Html.fromHtml("'"+title+"'" + " " + Name));
        }

        fetchFeeDetail();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (value.equalsIgnoreCase("start")){
                    mBottomSheetDialog.dismiss();
                }else{
                    mBottomSheetDialog.dismiss();
                    finish();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (value.equalsIgnoreCase("start")){
                    mBottomSheetDialog.dismiss();
                }else{
                    mBottomSheetDialog.dismiss();
                    finish();
                }
                // finish();
            }
        });
        mBottomSheetDialog.show();

    }

    private void fetchFeeDetail() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", User.getCurrentUser().getId());
        params.put("month", selectMonth);
        params.put("check", "fees");
        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                if (paid_beanslist != null) {
                    paid_beanslist.clear();

                }
                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    paid_beanslist = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    if (paid_beanslist.size() > 0) {
                        mAdapter.setMerchantBeans(paid_beanslist);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();

                    } else {
                     //   totalRecord.setText(getString(R.string.t_entries)+" 0");
                        paid_beanslist.clear();
                        mAdapter.setMerchantBeans(paid_beanslist);
                        mAdapter.notifyDataSetChanged();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Log.i("Fee_Paid_Bean", String.valueOf(list.get(0).getEs_particularamount())+ String.valueOf(list.get(0).getId()));
                // Log.i("Fee_Paid_Bean", String.valueOf(jsonObject));
            } else {
              //  totalRecord.setText(getString(R.string.t_entries)+" 0");
                paid_beanslist.clear();
                mAdapter.setMerchantBeans(paid_beanslist);
                mAdapter.notifyDataSetChanged();
               // txtNorecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };


    private void fetTranAmount(SessionBean session, String place_id, String route_id) {
        trans_amount=0;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("s_id", User.getCurrentUser().getClass_id());
        params.put("place_id", place_id);
        params.put("route_id", route_id);
        params.put("month", "trans");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, apiCall, this, params);
    }

    ApiHandler.ApiCallback apiCall = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
           // ErpProgress.cancelProgressBar();
            if (error == null) {
              //  ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                try {
                    if (transAmounts != null) {
                        transAmounts.clear();
                    }

                    if (transAmounts != null){
                        transAmounts.clear();
                        transAmounts=TransAmount.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        transAmountAdap.setFeeList(transAmounts);
                        transAmountAdap.notifyDataSetChanged();

                        for (int i = 0; i<transAmounts.size(); i++)
                        {
                            trans_amount += Integer.parseInt(transAmounts.get(i).getTrans_amount());
                        }
                        setTotals();
                        // Toast.makeText(getApplicationContext(),"totals "+totalPrice,Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
              //  ErpProgress.cancelProgressBar();

                transAmounts.clear();
                transAmountAdap.setFeeList(transAmounts);
                transAmountAdap.notifyDataSetChanged();
            }
        }
    };

    private void fetchPreviousbalance(SessionBean session,String fee_cate_id) {
        totalPrice=0;
      //  ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", selectMonth);
        params.put("c_id", User.getCurrentUser().getClass_id());
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", session.getStart_date());
        params.put("todate", session.getEnd_date());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, apiCallback1, this, params);
    }

    ApiHandler.ApiCallback apiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
           // ErpProgress.cancelProgressBar();
            if (error == null) {
              //  ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                try {
                    if (classList != null) {
                        classList.clear();
                    }

                    if (classList != null){
                        classList.clear();
                        classList=ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter.setFeeList(classList);
                        newsAdapter.notifyDataSetChanged();
                        for (int i = 0; i<classList.size(); i++)
                        {
                            totalPrice += Integer.parseInt(classList.get(i).getFee_amount());
                        }
                        // Toast.makeText(getApplicationContext(),"totals "+totalPrice,Toast.LENGTH_SHORT).show();
                        setTotals();
//                        new_totalPrice=p_balance+totalPrice;
//                      totals.setText(String.valueOf(new_totalPrice));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
               // ErpProgress.cancelProgressBar();
                //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                classList.clear();
                newsAdapter.setFeeList(classList);
                newsAdapter.notifyDataSetChanged();
                fee_close.setVisibility(View.GONE);
                noNoticeData.setVisibility(View.VISIBLE);
            }
        }
    };

    private void setTotals() {
        fee_close.setVisibility(View.VISIBLE);
        noNoticeData.setVisibility(View.GONE);
        totalRecord.setText(getString(R.string.t_entries)+" 1");
        totalPrice_withoutTrans=totalPrice+p_balance;
        new_totalPrice=p_balance+totalPrice+trans_amount;
        totals.setText(String.valueOf(new_totalPrice));
    }

    private void fetchFeesAmount(SessionBean session) {

        // ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", "all");
        params.put("c_id", User.getCurrentUser().getClass_id());
        params.put("s_id", User.getCurrentUser().getId());
        params.put("fromdate", session.getStart_date());
        params.put("todate", session.getEnd_date());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, apiCallback2, this, params);
    }

    ApiHandler.ApiCallback apiCallback2 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                commonProgress.dismiss();
              //  Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                //  Toast.makeText(getApplicationContext(),"select another Month",Toast.LENGTH_SHORT).show();

                try {
                    list=FeeBalanceBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                    p_balance= Integer.parseInt(list.get(0).getFee_bal());
                    p_fine_balance= Integer.parseInt(list.get(0).getBalance_fine());

//                    Toast.makeText(getApplicationContext(),new_totalPrice+p_balance+p_fine_balance,Toast.LENGTH_LONG).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setFeeData(p_balance,p_fine_balance);
                setTotals();

            } else {
                commonProgress.dismiss();

                //    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
//                fetchFeesAmount();
//                fetchPreviousbalance();
            }
        }
    };
    private void setFeeData(int p_balance, int p_fine_balance) {



        Log.e("hahaha", String.valueOf(p_balance));
        Log.e("hahaha", String.valueOf(p_fine_balance));
        Log.e("hahaha", String.valueOf(totalPrice));
        Log.e("hahaha", String.valueOf(new_totalPrice));

//        fine_balance.setText(String.valueOf(p_fine_balance));
//        total_fine.setText(String.valueOf(p_fine_balance));
        balance.setText(String.valueOf(p_balance));


        //   totals.setText(String.valueOf(new_totalPrice));

    }


    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(fee_paid_bean, Fee_Paid_Bean.class);
        Intent intent = new Intent(MonthlyFees.this, FeeDetailsActivity.class);
        intent.putExtra("fee_data", prod_data);
        startActivity(intent);
        Animatoo.animateZoom(this);
    }
}