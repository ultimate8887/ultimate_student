package com.ultimate.ultimatesmartstudent.FeeModule.Fees_Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ultimate.ultimatesmartstudent.R;


public class Pending_Fees extends Fragment {


    public Pending_Fees() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_common__fees, container, false);

        return view;
    }
}