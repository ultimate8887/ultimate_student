package com.ultimate.ultimatesmartstudent.FeeModule;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeeDetailsActivity extends AppCompatActivity {

    //    @BindView(R.id.pros)
//    TextView pros;
//    @BindView(R.id.s_holder)
//    TextView s_holder;

    @BindView(R.id.imgBack)
    ImageView imgBackmsg;

    @BindView(R.id.amount)
    EditText amount;
    Animation animation;
    Fee_Paid_Bean data;

    Window window;
    @BindView(R.id.myimage)
    CircularImageView myimage;
    @BindView(R.id.m_image)
    CircularImageView m_image;

    @BindView(R.id.f_name)
    TextView f_name;
    @BindView(R.id.roll_no)
    TextView roll_no;

    @BindView(R.id.fine)
    TextView fine;
    @BindView(R.id.p_fine)
    TextView p_fine;
    @BindView(R.id.pay_g_total)
    TextView pay_g_total;
    @BindView(R.id.p_transport)
    TextView p_transport;
    @BindView(R.id.r_date)
    TextView r_date;
    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.concession)
    TextView concession;

//    @BindView(R.id.balance)
//    TextView balance;

    String total_balance,total_paid;

    @BindView(R.id.all_total)
    TextView particulr_total;
    @BindView(R.id.remarks)
    TextView remarks;
    @BindView(R.id.s_holder)
    TextView id;
    @BindView(R.id.g_total)
    TextView g_total;

    @BindView(R.id.p_total)
    TextView p_total;
    @BindView(R.id.month)
    TextView month;

    @BindView(R.id.due)
    TextView due;

    @BindView(R.id.s_name)
    TextView s_name;
    //    @BindView(R.id.s_name)
//    TextView s_name;
    @BindView(R.id.transport)
    TextView transport;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_details);

        ButterKnife.bind(this);

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        // holder.product_add.startAnimation(animation);

//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));

        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("fee_data")) {
            Gson gson = new Gson();
            data = gson.fromJson(intent_value.getString("fee_data"), Fee_Paid_Bean.class);
            setCustomerData();
        }

    }


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void setCustomerData() {

        if (data.getEs_months()!= null) {
            String title = getColoredSpanned(data.getEs_months(), "#F55B53");
            String Name = getColoredSpanned(" Month Fee Billing Details", "#000000");
            month.setText(Html.fromHtml(title + " " + Name));
            // txt_list.setText(Html.fromHtml(title + " " + Name));
        }else {
            month.setText("Fee Billing Details");
        }

        title.setText(User.getCurrentUser().getSchoolData().getName());
        s_name.setText(User.getCurrentUser().getFirstname()+""+User.getCurrentUser().getLastname()+"("+User.getCurrentUser().getId()+")");
        f_name.setText(User.getCurrentUser().getFather());
        roll_no.setText(User.getCurrentUser().getClass_name()+" Class");

        id.setText(User.getCurrentUser().getSchoolData().getFi_school_id()+"-"+data.getId());

        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
            //  Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(this.getResources().getDrawable(R.drawable.person_1)).into(m_image);
            Utils.progressImg(User.getCurrentUser().getSchoolData().getLogo(),m_image,FeeDetailsActivity.this);
        }


        if (User.getCurrentUser().getGender().equalsIgnoreCase("Male")) {
            if (User.getCurrentUser().getProfile() != null) {
                Utils.progressImg(User.getCurrentUser().getProfile(),myimage,FeeDetailsActivity.this);
                // Picasso.get().load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
            } else {
                Picasso.get().load(R.drawable.boy).into(myimage);
            }
        }else{
            if (User.getCurrentUser().getProfile() != null) {
                Utils.progressImg(User.getCurrentUser().getProfile(),myimage,FeeDetailsActivity.this);
                //  Picasso.get().load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
            } else {
                Picasso.get().load(R.drawable.girl).into(myimage);
            }
        }
        int add=0,sub=0,value=0;

        add =Integer.parseInt(data.getEs_particularamount()) + Integer.parseInt(data.getEs_prospect()) +
                Integer.parseInt(data.getEs_exam()) + Integer.parseInt(data.getEs_counsel()) +
                Integer.parseInt(data.getEs_srf()) + Integer.parseInt(data.getEs_student())
                +Integer.parseInt(data.getEs_other())+Integer.parseInt(data.getEs_transportfee())+
                Integer.parseInt(data.getPaid_fine());

        sub=Integer.parseInt(data.getEs_concessionamount());
        value=add-sub;

        concession.setText(sub+".00");
        p_total.setText(value+".00");
        amount.setText(value+".00");
        int bal=0;
        if (data.getBalance_fine().equalsIgnoreCase("0")) {
            bal = Integer.parseInt(data.getFee_bal()) + Integer.parseInt(data.getTotal_fine());
        }else {
            bal = Integer.parseInt(data.getFee_bal()) + Integer.parseInt(data.getBalance_fine());
        }
        due.setText(bal+".00");

        int t_particulr= Integer.parseInt(data.getEs_totalamount())-Integer.parseInt(data.getOrgnl_transfee());

        particulr_total.setText(String.valueOf(t_particulr)+".00");

        int grand_total=Integer.parseInt(data.getEs_totalamount())+Integer.parseInt(data.getTotal_fine());

        g_total.setText(String.valueOf(grand_total)+".00");

        pay_g_total.setText(data.getEs_particularamount()+".00");

        transport.setText(data.getOrgnl_transfee()+".00");

        p_transport.setText(data.getEs_transportfee()+".00");

        fine.setText(data.getTotal_fine()+".00");

        p_fine.setText(data.getPaid_fine()+".00");

        r_date.setText(Utils.getDateFormated(data.getPaymentdate()));

    }

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }
}