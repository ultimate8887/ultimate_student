package com.ultimate.ultimatesmartstudent.FeeModule;

import com.ultimate.ultimatesmartstudent.BeanModule.CommonBean;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Fee_Paid_Bean {

    public String getEs_particularamount() {
        return es_particularamount;
    }

    public void setEs_particularamount(String es_particularamount) {
        this.es_particularamount = es_particularamount;
    }

    public String getApr_paid() {
        return apr_paid;
    }

    public void setApr_paid(String apr_paid) {
        this.apr_paid = apr_paid;
    }

    public String getApr_month() {
        return apr_month;
    }

    public void setApr_month(String apr_month) {
        this.apr_month = apr_month;
    }

    public String getApr_particular() {
        return apr_particular;
    }

    public void setApr_particular(String apr_particular) {
        this.apr_particular = apr_particular;
    }

    public String getApr_fine() {
        return apr_fine;
    }

    public void setApr_fine(String apr_fine) {
        this.apr_fine = apr_fine;
    }

    public String getMay_month() {
        return may_month;
    }

    public void setMay_month(String may_month) {
        this.may_month = may_month;
    }

    public String getMay_particular() {
        return may_particular;
    }

    public void setMay_particular(String may_particular) {
        this.may_particular = may_particular;
    }

    public String getMay_paid() {
        return may_paid;
    }

    public void setMay_paid(String may_paid) {
        this.may_paid = may_paid;
    }

    public String getMay_fine() {
        return may_fine;
    }

    public void setMay_fine(String may_fine) {
        this.may_fine = may_fine;
    }

    public String getJune_month() {
        return june_month;
    }

    public void setJune_month(String june_month) {
        this.june_month = june_month;
    }

    public String getJune_particular() {
        return june_particular;
    }

    public void setJune_particular(String june_particular) {
        this.june_particular = june_particular;
    }

    public String getJune_paid() {
        return june_paid;
    }

    public void setJune_paid(String june_paid) {
        this.june_paid = june_paid;
    }

    public String getJune_fine() {
        return june_fine;
    }

    public void setJune_fine(String june_fine) {
        this.june_fine = june_fine;
    }

    public String getJuly_month() {
        return july_month;
    }

    public void setJuly_month(String july_month) {
        this.july_month = july_month;
    }

    public String getJuly_particular() {
        return july_particular;
    }

    public void setJuly_particular(String july_particular) {
        this.july_particular = july_particular;
    }

    public String getJuly_paid() {
        return july_paid;
    }

    public void setJuly_paid(String july_paid) {
        this.july_paid = july_paid;
    }

    public String getJuly_fine() {
        return july_fine;
    }

    public void setJuly_fine(String july_fine) {
        this.july_fine = july_fine;
    }

    public String getAug_month() {
        return aug_month;
    }

    public void setAug_month(String aug_month) {
        this.aug_month = aug_month;
    }

    public String getAug_particular() {
        return aug_particular;
    }

    public void setAug_particular(String aug_particular) {
        this.aug_particular = aug_particular;
    }

    public String getAug_paid() {
        return aug_paid;
    }

    public void setAug_paid(String aug_paid) {
        this.aug_paid = aug_paid;
    }

    public String getAug_fine() {
        return aug_fine;
    }

    public void setAug_fine(String aug_fine) {
        this.aug_fine = aug_fine;
    }

    public String getSep_month() {
        return sep_month;
    }

    public void setSep_month(String sep_month) {
        this.sep_month = sep_month;
    }

    public String getSep_particular() {
        return sep_particular;
    }

    public void setSep_particular(String sep_particular) {
        this.sep_particular = sep_particular;
    }

    public String getSep_paid() {
        return sep_paid;
    }

    public void setSep_paid(String sep_paid) {
        this.sep_paid = sep_paid;
    }

    public String getSep_fine() {
        return sep_fine;
    }

    public void setSep_fine(String sep_fine) {
        this.sep_fine = sep_fine;
    }

    public String getOct_month() {
        return oct_month;
    }

    public void setOct_month(String oct_month) {
        this.oct_month = oct_month;
    }

    public String getOct_particular() {
        return oct_particular;
    }

    public void setOct_particular(String oct_particular) {
        this.oct_particular = oct_particular;
    }

    public String getOct_paid() {
        return oct_paid;
    }

    public void setOct_paid(String oct_paid) {
        this.oct_paid = oct_paid;
    }

    public String getOct_fine() {
        return oct_fine;
    }

    public void setOct_fine(String oct_fine) {
        this.oct_fine = oct_fine;
    }

    public String getNov_month() {
        return nov_month;
    }

    public void setNov_month(String nov_month) {
        this.nov_month = nov_month;
    }

    public String getNov_particular() {
        return nov_particular;
    }

    public void setNov_particular(String nov_particular) {
        this.nov_particular = nov_particular;
    }

    public String getNov_paid() {
        return nov_paid;
    }

    public void setNov_paid(String nov_paid) {
        this.nov_paid = nov_paid;
    }

    public String getNov_fine() {
        return nov_fine;
    }

    public void setNov_fine(String nov_fine) {
        this.nov_fine = nov_fine;
    }

    public String getDec_month() {
        return dec_month;
    }

    public void setDec_month(String dec_month) {
        this.dec_month = dec_month;
    }

    public String getDec_particular() {
        return dec_particular;
    }

    public void setDec_particular(String dec_particular) {
        this.dec_particular = dec_particular;
    }

    public String getDec_paid() {
        return dec_paid;
    }

    public void setDec_paid(String dec_paid) {
        this.dec_paid = dec_paid;
    }

    public String getDec_fine() {
        return dec_fine;
    }

    public void setDec_fine(String dec_fine) {
        this.dec_fine = dec_fine;
    }

    public String getJan_month() {
        return jan_month;
    }

    public void setJan_month(String jan_month) {
        this.jan_month = jan_month;
    }

    public String getJan_particular() {
        return jan_particular;
    }

    public void setJan_particular(String jan_particular) {
        this.jan_particular = jan_particular;
    }

    public String getJan_paid() {
        return jan_paid;
    }

    public void setJan_paid(String jan_paid) {
        this.jan_paid = jan_paid;
    }

    public String getJan_fine() {
        return jan_fine;
    }

    public void setJan_fine(String jan_fine) {
        this.jan_fine = jan_fine;
    }

    public String getFeb_month() {
        return feb_month;
    }

    public void setFeb_month(String feb_month) {
        this.feb_month = feb_month;
    }

    public String getFeb_particular() {
        return feb_particular;
    }

    public void setFeb_particular(String feb_particular) {
        this.feb_particular = feb_particular;
    }

    public String getFeb_paid() {
        return feb_paid;
    }

    public void setFeb_paid(String feb_paid) {
        this.feb_paid = feb_paid;
    }

    public String getFeb_fine() {
        return feb_fine;
    }

    public void setFeb_fine(String feb_fine) {
        this.feb_fine = feb_fine;
    }

    public String getMar_month() {
        return mar_month;
    }

    public void setMar_month(String mar_month) {
        this.mar_month = mar_month;
    }

    public String getMar_particular() {
        return mar_particular;
    }

    public void setMar_particular(String mar_particular) {
        this.mar_particular = mar_particular;
    }

    public String getMar_paid() {
        return mar_paid;
    }

    public void setMar_paid(String mar_paid) {
        this.mar_paid = mar_paid;
    }

    public String getMar_fine() {
        return mar_fine;
    }

    public void setMar_fine(String mar_fine) {
        this.mar_fine = mar_fine;
    }

    private static String apr_PAID = "apr_paid";
    private static String apr_MONTH = "apr_month";
    private static String apr_PARTICULAR = "apr_particular";
    private static String apr_FINE = "apr_fine";


    private String apr_paid;
    private String apr_month;
    private String apr_particular;
    private String apr_fine;

    private static String may_PAID = "may_paid";
    private static String may_MONTH = "may_month";
    private static String may_PARTICULAR = "may_particular";
    private static String may_FINE = "may_fine";

    private String may_month;
    private String may_particular;
    private String may_paid;
    private String may_fine;

    private static String june_PAID = "june_paid";
    private static String june_MONTH = "june_month";
    private static String june_PARTICULAR = "june_particular";
    private static String june_FINE = "june_fine";

    private String june_month;
    private String june_particular;
    private String june_paid;
    private String june_fine;

    private static String july_PAID = "july_paid";
    private static String july_MONTH = "july_month";
    private static String july_PARTICULAR = "july_particular";
    private static String july_FINE = "july_fine";

    private String july_month;
    private String july_particular;
    private String july_paid;
    private String july_fine;

    private static String aug_PAID = "aug_paid";
    private static String aug_MONTH = "aug_month";
    private static String aug_PARTICULAR = "aug_particular";
    private static String aug_FINE = "aug_fine";

    private String aug_month;
    private String aug_particular;
    private String aug_paid;
    private String aug_fine;

    private static String sep_PAID = "sep_paid";
    private static String sep_MONTH = "sep_month";
    private static String sep_PARTICULAR = "sep_particular";
    private static String sep_FINE = "sep_fine";

    private String sep_month;
    private String sep_particular;
    private String sep_paid;
    private String sep_fine;

    private static String oct_PAID = "oct_paid";
    private static String oct_MONTH = "oct_month";
    private static String oct_PARTICULAR = "oct_particular";
    private static String oct_FINE = "oct_fine";


    private String oct_month;
    private String oct_particular;
    private String oct_paid;
    private String oct_fine;

    private static String nov_PAID = "nov_paid";
    private static String nov_MONTH = "nov_month";
    private static String nov_PARTICULAR = "nov_particular";
    private static String nov_FINE = "nov_fine";

    private String nov_month;
    private String nov_particular;
    private String nov_paid;
    private String nov_fine;

    private static String dec_PAID = "dec_paid";
    private static String dec_MONTH = "dec_month";
    private static String dec_PARTICULAR = "dec_particular";
    private static String dec_FINE = "dec_fine";

    private String dec_month;
    private String dec_particular;
    private String dec_paid;
    private String dec_fine;

    private static String jan_PAID = "jan_paid";
    private static String jan_MONTH = "jan_month";
    private static String jan_PARTICULAR = "jan_particular";
    private static String jan_FINE = "jan_fine";

    private String jan_month;
    private String jan_particular;
    private String jan_paid;
    private String jan_fine;

    private static String feb_PAID = "feb_paid";
    private static String feb_MONTH = "feb_month";
    private static String feb_PARTICULAR = "feb_particular";
    private static String feb_FINE = "feb_fine";

    private String feb_month;
    private String feb_particular;
    private String feb_paid;
    private String feb_fine;

    private static String mar_PAID = "mar_paid";
    private static String mar_MONTH = "mar_month";
    private static String mar_PARTICULAR = "mar_particular";
    private static String mar_FINE = "mar_fine";

    private String mar_month;
    private String mar_particular;
    private String mar_paid;
    private String mar_fine;

    public String getEs_prospect() {
        return es_prospect;
    }

    public void setEs_prospect(String es_prospect) {
        this.es_prospect = es_prospect;
    }

    public String getEs_counsel() {
        return es_counsel;
    }

    public void setEs_counsel(String es_counsel) {
        this.es_counsel = es_counsel;
    }

    public String getEs_exam() {
        return es_exam;
    }

    public void setEs_exam(String es_exam) {
        this.es_exam = es_exam;
    }

    public String getEs_srf() {
        return es_srf;
    }

    public void setEs_srf(String es_srf) {
        this.es_srf = es_srf;
    }

    public String getEs_student() {
        return es_student;
    }

    public void setEs_student(String es_student) {
        this.es_student = es_student;
    }

    public String getEs_other() {
        return es_other;
    }

    public void setEs_other(String es_other) {
        this.es_other = es_other;
    }

    public String getEs_annual() {
        return es_annual;
    }

    public void setEs_annual(String es_annual) {
        this.es_annual = es_annual;
    }

    public String getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(String paymentdate) {
        this.paymentdate = paymentdate;
    }

    public String getEs_bal_paid() {
        return es_bal_paid;
    }

    public void setEs_bal_paid(String es_bal_paid) {
        this.es_bal_paid = es_bal_paid;
    }

    public String getEs_transportfee() {
        return es_transportfee;
    }

    public void setEs_transportfee(String es_transportfee) {
        this.es_transportfee = es_transportfee;
    }

    public String getEs_totalamount() {
        return es_totalamount;
    }

    public void setEs_totalamount(String es_totalamount) {
        this.es_totalamount = es_totalamount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    private String id;
    private String es_particularamount;
    private String es_prospect;
    private String payment_mode;
    private String es_counsel;
    private String es_exam;
    private String es_srf;
    private String es_student;
    private String es_other;
    private String es_annual;
    private String paymentdate;
    private String es_bal_paid;
    private String es_transportfee;
    private String es_totalamount;
    private String remark;

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    private String payment_status;

    public String getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(String screenshot) {
        this.screenshot = screenshot;
    }

    private String screenshot;

    private String fee_bal;

    private String orgnl_transfee;

    public String getOrgnl_transfee() {
        return orgnl_transfee;
    }

    public void setOrgnl_transfee(String orgnl_transfee) {
        this.orgnl_transfee = orgnl_transfee;
    }

    public String getTotal_fine() {
        return total_fine;
    }

    public void setTotal_fine(String total_fine) {
        this.total_fine = total_fine;
    }

    private String total_fine;



    private String s_id;
    private String name;

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    private String class_id;
    private String class_name;
    private String father_name;
    private String gender;
    private String profile;
    private String phoneno;

    public String getLast_fee_bal() {
        return last_fee_bal;
    }

    public void setLast_fee_bal(String last_fee_bal) {
        this.last_fee_bal = last_fee_bal;
    }

    private String last_fee_bal;


    public String getFee_bal() {
        return fee_bal;
    }

    public void setFee_bal(String fee_bal) {
        this.fee_bal = fee_bal;
    }

    public String getBalance_fine() {
        return balance_fine;
    }

    public void setBalance_fine(String balance_fine) {
        this.balance_fine = balance_fine;
    }

    public String getPaid_fine() {
        return paid_fine;
    }

    public void setPaid_fine(String paid_fine) {
        this.paid_fine = paid_fine;
    }

    public String getEs_months() {
        return es_months;
    }

    public void setEs_months(String es_months) {
        this.es_months = es_months;
    }

    private String balance_fine;
    private String paid_fine;
    private String es_months;

    public String getEs_concessionamount() {
        return es_concessionamount;
    }

    public void setEs_concessionamount(String es_concessionamount) {
        this.es_concessionamount = es_concessionamount;
    }

    private String es_concessionamount;


    private static String ID = "id";
    private static String PARTICULAR = "es_particularamount";
    private static String PROSPECT = "es_prospect";
    private static String P_MODE = "payment_mode";
    private static String COUNSEL = "es_counsel";
    private static String EXAM = "es_exam";
    private static String SRF = "es_srf";
    private static String STUDENT = "es_student";
    private static String OTHER = "es_other";
    private static String ANNUAL = "es_annual";
    private static String DATE = "paymentdate";
    private static String BAL_PAID = "es_bal_paid";
    private static String TRANS_FEE = "es_transportfee";
    private static String TOTAL = "es_totalamount";
    private static String REMARK = "remark";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static ArrayList<Fee_Paid_Bean> parsFee_Paid_BeanArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<Fee_Paid_Bean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                Fee_Paid_Bean p = parseFee_Paid_BeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Fee_Paid_Bean parseFee_Paid_BeanObject(JSONObject jsonObject) {
        Fee_Paid_Bean casteObj = new Fee_Paid_Bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(PARTICULAR) && !jsonObject.getString(PARTICULAR).isEmpty() && !jsonObject.getString(PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setEs_particularamount(jsonObject.getString(PARTICULAR));
            }
            if (jsonObject.has(PROSPECT) && !jsonObject.getString(PROSPECT).isEmpty() && !jsonObject.getString(PROSPECT).equalsIgnoreCase("null")) {
                casteObj.setEs_prospect(jsonObject.getString(PROSPECT));
            }
            if (jsonObject.has(P_MODE) && !jsonObject.getString(P_MODE).isEmpty() && !jsonObject.getString(P_MODE).equalsIgnoreCase("null")) {
                casteObj.setPayment_mode(jsonObject.getString(P_MODE));
            }
            if (jsonObject.has(COUNSEL) && !jsonObject.getString(COUNSEL).isEmpty() && !jsonObject.getString(COUNSEL).equalsIgnoreCase("null")) {
                casteObj.setEs_counsel(jsonObject.getString(COUNSEL));
            }
            if (jsonObject.has(EXAM) && !jsonObject.getString(EXAM).isEmpty() && !jsonObject.getString(EXAM).equalsIgnoreCase("null")) {
                casteObj.setEs_exam(jsonObject.getString(EXAM));
            }
            if (jsonObject.has(SRF) && !jsonObject.getString(SRF).isEmpty() && !jsonObject.getString(SRF).equalsIgnoreCase("null")) {
                casteObj.setEs_srf(jsonObject.getString(SRF));
            }
            if (jsonObject.has(STUDENT) && !jsonObject.getString(STUDENT).isEmpty() && !jsonObject.getString(STUDENT).equalsIgnoreCase("null")) {
                casteObj.setEs_student(jsonObject.getString(STUDENT));
            }
            if (jsonObject.has(OTHER) && !jsonObject.getString(OTHER).isEmpty() && !jsonObject.getString(OTHER).equalsIgnoreCase("null")) {
                casteObj.setEs_other(jsonObject.getString(OTHER));
            }
            if (jsonObject.has(ANNUAL) && !jsonObject.getString(ANNUAL).isEmpty() && !jsonObject.getString(ANNUAL).equalsIgnoreCase("null")) {
                casteObj.setEs_annual(jsonObject.getString(ANNUAL));
            }
//            if (jsonObject.has("last_fee_bal") && !jsonObject.getString("last_fee_bal").isEmpty() && !jsonObject.getString("last_fee_bal").equalsIgnoreCase("null")) {
//                casteObj.setLast_fee_bal(jsonObject.getString("last_fee_bal"));
//            }

            if (jsonObject.has("last_fee_bal")) {
                Object hlValue = jsonObject.get("last_fee_bal");
                casteObj.setLast_fee_bal(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

            if (jsonObject.has(jan_FINE) && !jsonObject.getString(jan_FINE).isEmpty() && !jsonObject.getString(jan_FINE).equalsIgnoreCase("null")) {
                casteObj.setJan_fine(jsonObject.getString(jan_FINE));
            }
            if (jsonObject.has(jan_PAID) && !jsonObject.getString(jan_PAID).isEmpty() && !jsonObject.getString(jan_PAID).equalsIgnoreCase("null")) {
                casteObj.setJan_paid(jsonObject.getString(jan_PAID));
            }

            if (jsonObject.has(jan_MONTH) && !jsonObject.getString(jan_MONTH).isEmpty() && !jsonObject.getString(jan_MONTH).equalsIgnoreCase("null")) {
                casteObj.setJan_month(jsonObject.getString(jan_MONTH));
            }
            if (jsonObject.has(jan_PARTICULAR) && !jsonObject.getString(jan_PARTICULAR).isEmpty() && !jsonObject.getString(jan_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setJan_particular(jsonObject.getString(jan_PARTICULAR));
            }

            if (jsonObject.has(feb_FINE) && !jsonObject.getString(feb_FINE).isEmpty() && !jsonObject.getString(feb_FINE).equalsIgnoreCase("null")) {
                casteObj.setFeb_fine(jsonObject.getString(feb_FINE));
            }
            if (jsonObject.has(feb_PAID) && !jsonObject.getString(feb_PAID).isEmpty() && !jsonObject.getString(feb_PAID).equalsIgnoreCase("null")) {
                casteObj.setFeb_paid(jsonObject.getString(feb_PAID));
            }

            if (jsonObject.has(feb_MONTH) && !jsonObject.getString(feb_MONTH).isEmpty() && !jsonObject.getString(feb_MONTH).equalsIgnoreCase("null")) {
                casteObj.setFeb_month(jsonObject.getString(feb_MONTH));
            }
            if (jsonObject.has(feb_PARTICULAR) && !jsonObject.getString(feb_PARTICULAR).isEmpty() && !jsonObject.getString(feb_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setFeb_particular(jsonObject.getString(feb_PARTICULAR));
            }


            if (jsonObject.has(mar_FINE) && !jsonObject.getString(mar_FINE).isEmpty() && !jsonObject.getString(mar_FINE).equalsIgnoreCase("null")) {
                casteObj.setMar_fine(jsonObject.getString(mar_FINE));
            }
            if (jsonObject.has(mar_PAID) && !jsonObject.getString(mar_PAID).isEmpty() && !jsonObject.getString(mar_PAID).equalsIgnoreCase("null")) {
                casteObj.setMar_paid(jsonObject.getString(mar_PAID));
            }

            if (jsonObject.has(mar_MONTH) && !jsonObject.getString(mar_MONTH).isEmpty() && !jsonObject.getString(mar_MONTH).equalsIgnoreCase("null")) {
                casteObj.setMar_month(jsonObject.getString(mar_MONTH));
            }
            if (jsonObject.has(mar_PARTICULAR) && !jsonObject.getString(mar_PARTICULAR).isEmpty() && !jsonObject.getString(mar_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setMar_particular(jsonObject.getString(mar_PARTICULAR));
            }


            if (jsonObject.has(apr_FINE) && !jsonObject.getString(apr_FINE).isEmpty() && !jsonObject.getString(apr_FINE).equalsIgnoreCase("null")) {
                casteObj.setApr_fine(jsonObject.getString(apr_FINE));
            }
            if (jsonObject.has(apr_PAID) && !jsonObject.getString(apr_PAID).isEmpty() && !jsonObject.getString(apr_PAID).equalsIgnoreCase("null")) {
                casteObj.setApr_paid(jsonObject.getString(apr_PAID));
            }

            if (jsonObject.has(apr_MONTH) && !jsonObject.getString(apr_MONTH).isEmpty() && !jsonObject.getString(apr_MONTH).equalsIgnoreCase("null")) {
                casteObj.setApr_month(jsonObject.getString(apr_MONTH));
            }
            if (jsonObject.has(apr_PARTICULAR) && !jsonObject.getString(apr_PARTICULAR).isEmpty() && !jsonObject.getString(apr_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setApr_particular(jsonObject.getString(apr_PARTICULAR));
            }

            if (jsonObject.has(may_FINE) && !jsonObject.getString(may_FINE).isEmpty() && !jsonObject.getString(may_FINE).equalsIgnoreCase("null")) {
                casteObj.setMay_fine(jsonObject.getString(may_FINE));
            }
            if (jsonObject.has(may_PAID) && !jsonObject.getString(may_PAID).isEmpty() && !jsonObject.getString(may_PAID).equalsIgnoreCase("null")) {
                casteObj.setMay_paid(jsonObject.getString(may_PAID));
            }

            if (jsonObject.has(may_MONTH) && !jsonObject.getString(may_MONTH).isEmpty() && !jsonObject.getString(may_MONTH).equalsIgnoreCase("null")) {
                casteObj.setMay_month(jsonObject.getString(may_MONTH));
            }
            if (jsonObject.has(may_PARTICULAR) && !jsonObject.getString(may_PARTICULAR).isEmpty() && !jsonObject.getString(may_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setMay_particular(jsonObject.getString(may_PARTICULAR));
            }

            if (jsonObject.has(june_FINE) && !jsonObject.getString(june_FINE).isEmpty() && !jsonObject.getString(june_FINE).equalsIgnoreCase("null")) {
                casteObj.setJune_fine(jsonObject.getString(june_FINE));
            }
            if (jsonObject.has(june_PAID) && !jsonObject.getString(june_PAID).isEmpty() && !jsonObject.getString(june_PAID).equalsIgnoreCase("null")) {
                casteObj.setJune_paid(jsonObject.getString(june_PAID));
            }

            if (jsonObject.has(june_MONTH) && !jsonObject.getString(june_MONTH).isEmpty() && !jsonObject.getString(june_MONTH).equalsIgnoreCase("null")) {
                casteObj.setJune_month(jsonObject.getString(june_MONTH));
            }
            if (jsonObject.has(june_PARTICULAR) && !jsonObject.getString(june_PARTICULAR).isEmpty() && !jsonObject.getString(june_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setJune_particular(jsonObject.getString(june_PARTICULAR));
            }


            if (jsonObject.has(july_FINE) && !jsonObject.getString(july_FINE).isEmpty() && !jsonObject.getString(july_FINE).equalsIgnoreCase("null")) {
                casteObj.setJuly_fine(jsonObject.getString(july_FINE));
            }
            if (jsonObject.has(july_PAID) && !jsonObject.getString(july_PAID).isEmpty() && !jsonObject.getString(july_PAID).equalsIgnoreCase("null")) {
                casteObj.setJuly_paid(jsonObject.getString(july_PAID));
            }

            if (jsonObject.has(july_MONTH) && !jsonObject.getString(july_MONTH).isEmpty() && !jsonObject.getString(july_MONTH).equalsIgnoreCase("null")) {
                casteObj.setJuly_month(jsonObject.getString(july_MONTH));
            }
            if (jsonObject.has(july_PARTICULAR) && !jsonObject.getString(july_PARTICULAR).isEmpty() && !jsonObject.getString(july_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setJuly_particular(jsonObject.getString(july_PARTICULAR));
            }

            if (jsonObject.has(aug_FINE) && !jsonObject.getString(aug_FINE).isEmpty() && !jsonObject.getString(aug_FINE).equalsIgnoreCase("null")) {
                casteObj.setAug_fine(jsonObject.getString(aug_FINE));
            }
            if (jsonObject.has(aug_PAID) && !jsonObject.getString(aug_PAID).isEmpty() && !jsonObject.getString(aug_PAID).equalsIgnoreCase("null")) {
                casteObj.setAug_paid(jsonObject.getString(aug_PAID));
            }

            if (jsonObject.has(aug_MONTH) && !jsonObject.getString(aug_MONTH).isEmpty() && !jsonObject.getString(aug_MONTH).equalsIgnoreCase("null")) {
                casteObj.setAug_month(jsonObject.getString(aug_MONTH));
            }
            if (jsonObject.has(aug_PARTICULAR) && !jsonObject.getString(aug_PARTICULAR).isEmpty() && !jsonObject.getString(aug_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setAug_particular(jsonObject.getString(aug_PARTICULAR));
            }


            if (jsonObject.has(sep_FINE) && !jsonObject.getString(sep_FINE).isEmpty() && !jsonObject.getString(sep_FINE).equalsIgnoreCase("null")) {
                casteObj.setSep_fine(jsonObject.getString(sep_FINE));
            }
            if (jsonObject.has(sep_PAID) && !jsonObject.getString(sep_PAID).isEmpty() && !jsonObject.getString(sep_PAID).equalsIgnoreCase("null")) {
                casteObj.setSep_paid(jsonObject.getString(sep_PAID));
            }

            if (jsonObject.has(sep_MONTH) && !jsonObject.getString(sep_MONTH).isEmpty() && !jsonObject.getString(sep_MONTH).equalsIgnoreCase("null")) {
                casteObj.setSep_month(jsonObject.getString(sep_MONTH));
            }
            if (jsonObject.has(sep_PARTICULAR) && !jsonObject.getString(sep_PARTICULAR).isEmpty() && !jsonObject.getString(sep_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setSep_particular(jsonObject.getString(sep_PARTICULAR));
            }

            if (jsonObject.has(oct_FINE) && !jsonObject.getString(oct_FINE).isEmpty() && !jsonObject.getString(oct_FINE).equalsIgnoreCase("null")) {
                casteObj.setOct_fine(jsonObject.getString(oct_FINE));
            }
            if (jsonObject.has(oct_PAID) && !jsonObject.getString(oct_PAID).isEmpty() && !jsonObject.getString(oct_PAID).equalsIgnoreCase("null")) {
                casteObj.setOct_paid(jsonObject.getString(oct_PAID));
            }

            if (jsonObject.has(oct_MONTH) && !jsonObject.getString(oct_MONTH).isEmpty() && !jsonObject.getString(oct_MONTH).equalsIgnoreCase("null")) {
                casteObj.setOct_month(jsonObject.getString(oct_MONTH));
            }
            if (jsonObject.has(oct_PARTICULAR) && !jsonObject.getString(oct_PARTICULAR).isEmpty() && !jsonObject.getString(oct_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setOct_particular(jsonObject.getString(oct_PARTICULAR));
            }

            if (jsonObject.has(nov_FINE) && !jsonObject.getString(nov_FINE).isEmpty() && !jsonObject.getString(nov_FINE).equalsIgnoreCase("null")) {
                casteObj.setNov_fine(jsonObject.getString(nov_FINE));
            }
            if (jsonObject.has(nov_PAID) && !jsonObject.getString(nov_PAID).isEmpty() && !jsonObject.getString(nov_PAID).equalsIgnoreCase("null")) {
                casteObj.setNov_paid(jsonObject.getString(nov_PAID));
            }

            if (jsonObject.has(nov_MONTH) && !jsonObject.getString(nov_MONTH).isEmpty() && !jsonObject.getString(nov_MONTH).equalsIgnoreCase("null")) {
                casteObj.setNov_month(jsonObject.getString(nov_MONTH));
            }
            if (jsonObject.has(nov_PARTICULAR) && !jsonObject.getString(nov_PARTICULAR).isEmpty() && !jsonObject.getString(nov_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setNov_particular(jsonObject.getString(nov_PARTICULAR));
            }


            if (jsonObject.has(dec_FINE) && !jsonObject.getString(dec_FINE).isEmpty() && !jsonObject.getString(dec_FINE).equalsIgnoreCase("null")) {
                casteObj.setDec_fine(jsonObject.getString(dec_FINE));
            }
            if (jsonObject.has(dec_PAID) && !jsonObject.getString(dec_PAID).isEmpty() && !jsonObject.getString(dec_PAID).equalsIgnoreCase("null")) {
                casteObj.setDec_paid(jsonObject.getString(dec_PAID));
            }

            if (jsonObject.has(dec_MONTH) && !jsonObject.getString(dec_MONTH).isEmpty() && !jsonObject.getString(dec_MONTH).equalsIgnoreCase("null")) {
                casteObj.setDec_month(jsonObject.getString(dec_MONTH));
            }
            if (jsonObject.has(dec_PARTICULAR) && !jsonObject.getString(dec_PARTICULAR).isEmpty() && !jsonObject.getString(dec_PARTICULAR).equalsIgnoreCase("null")) {
                casteObj.setDec_particular(jsonObject.getString(dec_PARTICULAR));
            }


            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                casteObj.setPaymentdate(jsonObject.getString(DATE));
            }
//            if (jsonObject.has(BAL_PAID) && !jsonObject.getString(BAL_PAID).isEmpty() && !jsonObject.getString(BAL_PAID).equalsIgnoreCase("null")) {
//                casteObj.setEs_bal_paid(jsonObject.getString(BAL_PAID));
//            }

            if (jsonObject.has(BAL_PAID)) {
                Object hlValue = jsonObject.get(BAL_PAID);
                casteObj.setEs_bal_paid(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

            if (jsonObject.has(TRANS_FEE) && !jsonObject.getString(TRANS_FEE).isEmpty() && !jsonObject.getString(TRANS_FEE).equalsIgnoreCase("null")) {
                casteObj.setEs_transportfee(jsonObject.getString(TRANS_FEE));
            }

            if (jsonObject.has(TOTAL) && !jsonObject.getString(TOTAL).isEmpty() && !jsonObject.getString(TOTAL).equalsIgnoreCase("null")) {
                casteObj.setEs_totalamount(jsonObject.getString(TOTAL));
            }
            if (jsonObject.has(REMARK) && !jsonObject.getString(REMARK).isEmpty() && !jsonObject.getString(REMARK).equalsIgnoreCase("null")) {
                casteObj.setRemark(jsonObject.getString(REMARK));
            }


//            if (jsonObject.has("es_concessionamount") && !jsonObject.getString("es_concessionamount").isEmpty() && !jsonObject.getString("es_concessionamount").equalsIgnoreCase("null")) {
//                casteObj.setEs_concessionamount(jsonObject.getString("es_concessionamount"));
//            }

            if (jsonObject.has("es_concessionamount")) {
                Object hlValue = jsonObject.get("es_concessionamount");
                casteObj.setEs_concessionamount(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

//            if (jsonObject.has("total_fine") && !jsonObject.getString("total_fine").isEmpty() && !jsonObject.getString("total_fine").equalsIgnoreCase("null")) {
//                casteObj.setTotal_fine(jsonObject.getString("total_fine"));
//            }

            if (jsonObject.has("total_fine")) {
                Object hlValue = jsonObject.get("total_fine");
                casteObj.setTotal_fine(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

            if (jsonObject.has("payment_status") && !jsonObject.getString("payment_status").isEmpty() && !jsonObject.getString("payment_status").equalsIgnoreCase("null")) {
                casteObj.setPayment_status(jsonObject.getString("payment_status"));
            }

//            if (jsonObject.has("orgnl_transfee") && !jsonObject.getString("orgnl_transfee").isEmpty() && !jsonObject.getString("orgnl_transfee").equalsIgnoreCase("null")) {
//                casteObj.setOrgnl_transfee(jsonObject.getString("orgnl_transfee"));
//            }

            if (jsonObject.has("orgnl_transfee")) {
                Object hlValue = jsonObject.get("orgnl_transfee");
                casteObj.setOrgnl_transfee(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }



//            if (jsonObject.has("fee_bal") && !jsonObject.getString("fee_bal").isEmpty() && !jsonObject.getString("fee_bal").equalsIgnoreCase("null")) {
//                casteObj.setFee_bal(jsonObject.getString("fee_bal"));
//            }

            if (jsonObject.has("fee_bal")) {
                Object hlValue = jsonObject.get("fee_bal");
                casteObj.setFee_bal(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

//            if (jsonObject.has("balance_fine") && !jsonObject.getString("balance_fine").isEmpty() && !jsonObject.getString("balance_fine").equalsIgnoreCase("null")) {
//                casteObj.setBalance_fine(jsonObject.getString("balance_fine"));
//            }

            if (jsonObject.has("balance_fine")) {
                Object hlValue = jsonObject.get("balance_fine");
                casteObj.setBalance_fine(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

            if (jsonObject.has("es_months") && !jsonObject.getString("es_months").isEmpty() && !jsonObject.getString("es_months").equalsIgnoreCase("null")) {
                casteObj.setEs_months(jsonObject.getString("es_months"));
            }
//            if (jsonObject.has("paid_fine") && !jsonObject.getString("paid_fine").isEmpty() && !jsonObject.getString("paid_fine").equalsIgnoreCase("null")) {
//                casteObj.setPaid_fine(jsonObject.getString("paid_fine"));
//            }

            if (jsonObject.has("paid_fine")) {
                Object hlValue = jsonObject.get("paid_fine");
                casteObj.setPaid_fine(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

            if (jsonObject.has("s_id") && !jsonObject.getString("s_id").isEmpty() && !jsonObject.getString("s_id").equalsIgnoreCase("null")) {
                casteObj.setS_id(jsonObject.getString("s_id"));
            }


            if (jsonObject.has("name") && !jsonObject.getString("name").isEmpty() && !jsonObject.getString("name").equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString("name"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }

            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }
            if (jsonObject.has("father_name") && !jsonObject.getString("father_name").isEmpty() && !jsonObject.getString("father_name").equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString("father_name"));
            }

            if (jsonObject.has("profile") && !jsonObject.getString("profile").isEmpty() && !jsonObject.getString("profile").equalsIgnoreCase("null")) {
                casteObj.setProfile(jsonObject.getString("profile"));
            }
            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                casteObj.setGender(jsonObject.getString("gender"));
            }

            if (jsonObject.has("screenshot") && !jsonObject.getString("screenshot").isEmpty() && !jsonObject.getString("screenshot").equalsIgnoreCase("null")) {
                casteObj.setScreenshot(Constants.getImageBaseURL() + jsonObject.getString("screenshot"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
