package com.ultimate.ultimatesmartstudent.FeeModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeePaidBean {

    private int status_code;
    private StDataBean st_data;
    private ArrayList<FeeDataBean> fee_data;

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public StDataBean getSt_data() {
        return st_data;
    }

    public void setSt_data(StDataBean st_data) {
        this.st_data = st_data;
    }

    public ArrayList<FeeDataBean> getFee_data() {
        return fee_data;
    }

    public void setFee_data(ArrayList<FeeDataBean> fee_data) {
        this.fee_data = fee_data;
    }

    public static ArrayList<FeePaidBean> pareFeeDetail(JSONArray arrayObj) {
        ArrayList list = new ArrayList<FeePaidBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                FeePaidBean p = parseFeeDetail(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static FeePaidBean parseFeeDetail(JSONObject jsonObject) {
        FeePaidBean data = new FeePaidBean();
        try {
            if (jsonObject.has("class_data")) {
                data.setSt_data(StDataBean.parseStObject(jsonObject.getJSONObject("st_data")));
            }
            if (jsonObject.has("fee_data")) {
                data.setFee_data(FeeDataBean.parseFee(jsonObject.getJSONArray("fee_data")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static class StDataBean {
        /**
         * id : 1
         * firstname : Yadhvi
         * lastname : Rajput
         * image : st_02012018_080227_download.jpg
         * class_id : 2
         * class_name : NURSERY1
         * father_name : Sandeep Kumar
         * last_paid : 2018-06-15
         */

        private String id;
        private String firstname;
        private String lastname;
        private String image;
        private String class_id;
        private String class_name;
        private String father_name;
        private String last_paid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getClass_id() {
            return class_id;
        }

        public void setClass_id(String class_id) {
            this.class_id = class_id;
        }

        public String getClass_name() {
            return class_name;
        }

        public void setClass_name(String class_name) {
            this.class_name = class_name;
        }

        public String getFather_name() {
            return father_name;
        }

        public void setFather_name(String father_name) {
            this.father_name = father_name;
        }

        public String getLast_paid() {
            return last_paid;
        }

        public void setLast_paid(String last_paid) {
            this.last_paid = last_paid;
        }

        public static StDataBean parseStObject(JSONObject jsonObject) {
            StDataBean casteObj = new StDataBean();
            try {
                if (jsonObject.has("id")) {
                    casteObj.setId(jsonObject.getString("id"));
                }
                if (jsonObject.has("firstname") && !jsonObject.getString("firstname").isEmpty() && !jsonObject.getString("firstname").equalsIgnoreCase("null")) {
                    casteObj.setFirstname(jsonObject.getString("firstname"));
                }
                if (jsonObject.has("lastname") && !jsonObject.getString("lastname").isEmpty() && !jsonObject.getString("lastname").equalsIgnoreCase("null")) {
                    casteObj.setFirstname(jsonObject.getString("lastname"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return casteObj;
        }

    }

    public static class FeeDataBean {
        /**
         * es_feepayid : 6
         * es_studentid : 1
         * es_classid : 2
         * es_particularid : [202, 217]
         * es_particularname : [Jan add, newtest]
         * es_particularamount : [40, 60]
         * es_totalamount : 10
         * es_date : 2018-06-15
         * es_installment : 0
         * es_fromdate : 2018-04-01
         * es_todate : 2019-03-31
         * es_fine : 0
         * es_concessiontoid : []
         * es_concessionamount : []
         * es_payment_mode : Cash
         * es_bank_name :
         * es_acc_no :
         * es_dd_check_no :
         * es_teller_no : 0
         * es_note : sxvb
         */

        private String es_feepayid;
        private String es_studentid;
        private String es_classid;
        private String es_particularid;
        private String es_particularname;
        private String es_particularamount;
        private String es_totalamount;
        private String es_date;
        private String es_installment;
        private String es_fromdate;
        private String es_todate;
        private String es_fine="0";
        private String es_concessiontoid;
        private String es_concessionamount;
        private String es_payment_mode;
        private String es_bank_name;
        private String es_acc_no;
        private String es_dd_check_no;
        private String es_teller_no;
        private String es_note;


        private String es_installments;
        private String es_secondinstall;
        private String es_thirdinstall;
        private String es_fine_bal = "0";
        private String es_fee_bal = "0";
        private String es_last_fine_bal = "0";
        private String es_last_fee_bal = "0";
        private String es_fine_paid = "0";
        private String  es_months;
        private String fee_value;
        public String getEs_prev_bal() {
            return es_prev_bal;
        }

        public void setEs_prev_bal(String es_prev_bal) {
            this.es_prev_bal = es_prev_bal;
        }

        private String es_prev_bal;

        private String es_pendingbal;
        private ArrayList<String> partName;
        public String getEs_pendingbal() {
            return es_pendingbal;
        }

        public void setEs_pendingbal(String es_pendingbal) {
            this.es_pendingbal = es_pendingbal;
        }


        public String getEs_fine_paid() {
            return es_fine_paid;
        }

        public void setEs_fine_paid(String es_fine_paid) {
            this.es_fine_paid = es_fine_paid;
        }

        public String getEs_last_fee_bal() {
            return es_last_fee_bal;
        }

        public void setEs_last_fee_bal(String es_last_fee_bal) {
            this.es_last_fee_bal = es_last_fee_bal;
        }

        public String getEs_last_fine_bal() {
            return es_last_fine_bal;
        }

        public void setEs_last_fine_bal(String es_last_fine_bal) {
            this.es_last_fine_bal = es_last_fine_bal;
        }

        public String getEs_months() {
            return es_months;
        }

        public void setEs_months(String es_months) {
            this.es_months = es_months;
        }

        public String getEs_feepayid() {
            return es_feepayid;
        }

        public void setEs_feepayid(String es_feepayid) {
            this.es_feepayid = es_feepayid;
        }

        public String getEs_studentid() {
            return es_studentid;
        }

        public void setEs_studentid(String es_studentid) {
            this.es_studentid = es_studentid;
        }

        public String getEs_classid() {
            return es_classid;
        }

        public void setEs_classid(String es_classid) {
            this.es_classid = es_classid;
        }

        public String getEs_particularid() {
            return es_particularid;
        }

        public void setEs_particularid(String es_particularid) {
            this.es_particularid = es_particularid;
        }

        public String getEs_particularname() {
            return es_particularname;
        }

        public void setEs_particularname(String es_particularname) {
            this.es_particularname = es_particularname;
        }

        public String getEs_particularamount() {
            return es_particularamount;
        }

        public void setEs_particularamount(String es_particularamount) {
            this.es_particularamount = es_particularamount;
        }

        public String getEs_totalamount() {
            return es_totalamount;
        }

        public void setEs_totalamount(String es_totalamount) {
            this.es_totalamount = es_totalamount;
        }

        public String getEs_date() {
            return es_date;
        }

        public void setEs_date(String es_date) {
            this.es_date = es_date;
        }

        public String getEs_installment() {
            return es_installment;
        }

        public void setEs_installment(String es_installment) {
            this.es_installment = es_installment;
        }

        public String getEs_fromdate() {
            return es_fromdate;
        }

        public void setEs_fromdate(String es_fromdate) {
            this.es_fromdate = es_fromdate;
        }

        public String getEs_todate() {
            return es_todate;
        }

        public void setEs_todate(String es_todate) {
            this.es_todate = es_todate;
        }

        public String getEs_fine() {
            return es_fine;
        }

        public void setEs_fine(String es_fine) {
            this.es_fine = es_fine;
        }

        public String getEs_concessiontoid() {
            return es_concessiontoid;
        }

        public void setEs_concessiontoid(String es_concessiontoid) {
            this.es_concessiontoid = es_concessiontoid;
        }

        public String getEs_concessionamount() {
            return es_concessionamount;
        }

        public void setEs_concessionamount(String es_concessionamount) {
            this.es_concessionamount = es_concessionamount;
        }

        public String getEs_payment_mode() {
            return es_payment_mode;
        }

        public void setEs_payment_mode(String es_payment_mode) {
            this.es_payment_mode = es_payment_mode;
        }

        public String getEs_bank_name() {
            return es_bank_name;
        }

        public void setEs_bank_name(String es_bank_name) {
            this.es_bank_name = es_bank_name;
        }

        public String getEs_acc_no() {
            return es_acc_no;
        }

        public void setEs_acc_no(String es_acc_no) {
            this.es_acc_no = es_acc_no;
        }

        public String getEs_dd_check_no() {
            return es_dd_check_no;
        }

        public void setEs_dd_check_no(String es_dd_check_no) {
            this.es_dd_check_no = es_dd_check_no;
        }

        public String getEs_teller_no() {
            return es_teller_no;
        }

        public void setEs_teller_no(String es_teller_no) {
            this.es_teller_no = es_teller_no;
        }

        public String getEs_note() {
            return es_note;
        }

        public void setEs_note(String es_note) {
            this.es_note = es_note;
        }

        public String getEs_installments() {
            return es_installments;
        }

        public void setEs_installments(String es_installments) {
            this.es_installments = es_installments;
        }

        public String getEs_secondinstall() {
            return es_secondinstall;
        }

        public void setEs_secondinstall(String es_secondinstall) {
            this.es_secondinstall = es_secondinstall;
        }

        public String getEs_thirdinstall() {
            return es_thirdinstall;
        }

        public void setEs_thirdinstall(String es_thirdinstall) {
            this.es_thirdinstall = es_thirdinstall;
        }
        public String getEs_fee_bal() {
            return es_fee_bal;
        }

        public void setEs_fee_bal(String es_fee_bal) {
            this.es_fee_bal = es_fee_bal;
        }

        public String getEs_fine_bal() {
            return es_fine_bal;
        }

        public void setEs_fine_bal(String es_fine_bal) {
            this.es_fine_bal = es_fine_bal;
        }

        public String getFee_value() {
            return fee_value;
        }

        public void setFee_value(String fee_value) {
            this.fee_value = fee_value;
        }

        public static ArrayList<FeeDataBean> parseFee(JSONArray arrayObj) {
            ArrayList list = new ArrayList<FeeDataBean>();
            try {
                for (int i = 0; i < arrayObj.length(); i++) {
                    FeeDataBean p = parseFeeObj(arrayObj.getJSONObject(i));
                    if (p != null) {
                        list.add(p);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return list;
        }

        private static FeeDataBean parseFeeObj(JSONObject jsonObject) {
            FeeDataBean data = new FeeDataBean();
            try {
                if (jsonObject.has("fee_value") && !jsonObject.getString("fee_value").isEmpty() && !jsonObject.getString("fee_value").equalsIgnoreCase("null")) {
                    String part_id = jsonObject.getString("fee_value").replace("[", "").replace("]", "");
                    data.setFee_value(part_id);
                }
                if (jsonObject.has("es_feepayid") && !jsonObject.getString("es_feepayid").isEmpty() && !jsonObject.getString("es_feepayid").equalsIgnoreCase("null")) {
                    data.setEs_feepayid(jsonObject.getString("es_feepayid"));
                }
                if (jsonObject.has("es_particularid") && !jsonObject.getString("es_particularid").isEmpty() && !jsonObject.getString("es_particularid").equalsIgnoreCase("null")) {
                    String part_id = jsonObject.getString("es_particularid").replace("[","").replace("]","");
                    data.setEs_particularid(part_id);
                }
                if (jsonObject.has("es_particularname") && !jsonObject.getString("es_particularname").isEmpty() && !jsonObject.getString("es_particularname").equalsIgnoreCase("null")) {
                    data.setEs_particularname(jsonObject.getString("es_particularname").replace("[","").replace("]",""));
                }
                if (jsonObject.has("es_particularamount") && !jsonObject.getString("es_particularamount").isEmpty() && !jsonObject.getString("es_particularamount").equalsIgnoreCase("null")) {
                    data.setEs_particularamount(jsonObject.getString("es_particularamount").replace("[","").replace("]",""));
                }
                if (jsonObject.has("es_totalamount") && !jsonObject.getString("es_totalamount").isEmpty() && !jsonObject.getString("es_totalamount").equalsIgnoreCase("null")) {
                    data.setEs_totalamount(jsonObject.getString("es_totalamount"));
                }
                if (jsonObject.has("es_concessiontoid") && !jsonObject.getString("es_concessiontoid").isEmpty() && !jsonObject.getString("es_concessiontoid").equalsIgnoreCase("null")) {
                    data.setEs_concessiontoid(jsonObject.getString("es_concessiontoid").replace("[","").replace("]",""));
                }
                if (jsonObject.has("es_concessionamount") && !jsonObject.getString("es_concessionamount").isEmpty() && !jsonObject.getString("es_concessionamount").equalsIgnoreCase("null")) {
                    data.setEs_concessionamount(jsonObject.getString("es_concessionamount").replace("[","").replace("]",""));
                }
                if (jsonObject.has("es_date") && !jsonObject.getString("es_date").isEmpty() && !jsonObject.getString("es_date").equalsIgnoreCase("null")) {
                    data.setEs_date(jsonObject.getString("es_date"));
                }

                if (jsonObject.has("es_installments") && !jsonObject.getString("es_installments").isEmpty() && !jsonObject.getString("es_installments").equalsIgnoreCase("null")) {
                    data.setEs_installments(jsonObject.getString("es_installments"));
                }
                if (jsonObject.has("es_installment") && !jsonObject.getString("es_installment").isEmpty() && !jsonObject.getString("es_installment").equalsIgnoreCase("null")) {
                    data.setEs_installment(jsonObject.getString("es_installment"));
                }
                if (jsonObject.has("es_secondinstall") && !jsonObject.getString("es_secondinstall").isEmpty() && !jsonObject.getString("es_secondinstall").equalsIgnoreCase("null")) {
                    data.setEs_secondinstall(jsonObject.getString("es_secondinstall"));
                }
                if (jsonObject.has("es_thirdinstall") && !jsonObject.getString("es_thirdinstall").isEmpty() && !jsonObject.getString("es_thirdinstall").equalsIgnoreCase("null")) {
                    data.setEs_thirdinstall(jsonObject.getString("es_thirdinstall"));
                }
                if (jsonObject.has("es_months") && !jsonObject.getString("es_months").isEmpty() && !jsonObject.getString("es_months").equalsIgnoreCase("null")) {
                    data.setEs_months(jsonObject.getString("es_months").replace("[", "").replace("]", ""));
                }
                if (jsonObject.has("es_fee_bal") && !jsonObject.getString("es_fee_bal").isEmpty() && !jsonObject.getString("es_fee_bal").equalsIgnoreCase("null")) {
                    data.setEs_fee_bal(jsonObject.getString("es_fee_bal"));
                }
                if (jsonObject.has("es_fine_bal") && !jsonObject.getString("es_fine_bal").isEmpty() && !jsonObject.getString("es_fine_bal").equalsIgnoreCase("null")) {
                    data.setEs_fine_bal(jsonObject.getString("es_fine_bal"));
                }
                if (jsonObject.has("es_fine") && !jsonObject.getString("es_fine").isEmpty() && !jsonObject.getString("es_fine").equalsIgnoreCase("null")) {
                    data.setEs_fine(jsonObject.getString("es_fine"));
                }
                if (jsonObject.has("es_last_fine_bal") && !jsonObject.getString("es_last_fine_bal").isEmpty() && !jsonObject.getString("es_last_fine_bal").equalsIgnoreCase("null")) {
                    data.setEs_last_fine_bal(jsonObject.getString("es_last_fine_bal"));
                }
                if (jsonObject.has("es_last_fee_bal") && !jsonObject.getString("es_last_fee_bal").isEmpty() && !jsonObject.getString("es_last_fee_bal").equalsIgnoreCase("null")) {
                    data.setEs_last_fee_bal(jsonObject.getString("es_last_fee_bal"));
                }
                if (jsonObject.has("es_fine_paid") && !jsonObject.getString("es_fine_paid").isEmpty() && !jsonObject.getString("es_fine_paid").equalsIgnoreCase("null")) {
                    data.setEs_fine_paid(jsonObject.getString("es_fine_paid"));
                }
                if (jsonObject.has("es_pendingbal") && !jsonObject.getString("es_pendingbal").isEmpty() && !jsonObject.getString("es_pendingbal").equalsIgnoreCase("null")) {
                    data.setEs_pendingbal(jsonObject.getString("es_pendingbal").replace("[", "").replace("]", ""));
                }

                if (jsonObject.has("es_prev_bal") && !jsonObject.getString("es_prev_bal").isEmpty() && !jsonObject.getString("es_prev_bal").equalsIgnoreCase("null")) {
                    data.setEs_prev_bal(jsonObject.getString("es_prev_bal"));
                }

                if (jsonObject.has("part_name")) {
                    ArrayList<String> list = new ArrayList<>();
                    JSONArray subObj = jsonObject.getJSONArray("part_name");
                    for (int j = 0; j < subObj.length(); j++) {
                        list.add(subObj.getString(j));
                    }
                    data.setPartName(list);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return data;
        }


        public void setPartName(ArrayList<String> partName) {
            this.partName = partName;
        }

        public ArrayList<String> getPartName() {
            return partName;
        }


    }
}
