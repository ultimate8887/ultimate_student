package com.ultimate.ultimatesmartstudent.FeeModule;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.SessionBean;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.ForCamera.IPickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartstudent.ForCamera.PickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickSetup;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayFeeSActivity extends AppCompatActivity implements FeeListAdapter.ProdMethodCallBack, IPickResult {



    private Bitmap bitmap;
    @BindView(R.id.spinnerMonth)
    Spinner spinnerMonth;
    @BindView(R.id.spinnerFinancialYear)
    Spinner spinnerFinancialYear;

    ArrayList<String> monthList = new ArrayList<>();
    ArrayList<ExtraDetails> extraDetails = new ArrayList<>();
    String selectMonth ="",month_name="",fee_cate_id="",place_id="",route_id="";
    CommonProgress commonProgress;
    int totalPrice = 0,trans_amount= 0, p_balance=0, p_fine_balance=0, new_totalPrice=0,check=0, totalPrice_withoutTrans=0,selection = 0;
    ArrayList<FeeBalanceBean> list = new ArrayList<>();

    @BindView(R.id.totalRecord)
    TextView totalRecord;
    //TransAmount
    @BindView(R.id.recycler_trans)
    RecyclerView recycler_trans;
    private TransAmountAdap transAmountAdap;
    ArrayList<TransAmount> transAmounts = new ArrayList<>();

    @BindView(R.id.fee_close)
    LinearLayout fee_close;

    @BindView(R.id.add)
    ImageButton add;
    @BindView(R.id.imgShow)
    PhotoView imgShow;

    //FeeListAdapter
    RecyclerView recyclerView;
    ArrayList<Fee_Paid_Bean> paid_beanslist= new ArrayList<>();
    private FeeListAdapter mAdapter;
    ImageView close;
    TextView txtSetup;
    TextView txt_list;

    //ParticularBean
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    RecyclerView.LayoutManager layoutManager;
    private Particular_Adapter newsAdapter;
    ArrayList<ParticularBean> classList = new ArrayList<>();
    //    ImageView imgShow;
    @BindView(R.id.contact_support)
    ImageView contact_support;
    SharedPreferences sharedPreferences;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.f_details)
    TextView f_details;
    @BindView(R.id.totals)
    TextView totals;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    Animation animation;
    String image_url="",school="";
    @BindView(R.id.noNoticeData)
    TextView noNoticeData;


    int p_value=0,t_value=0,c_value=0,total_value=0,paid_fine_value=0,today_add_fine=0,new_f_balance=0,new_total_fine=0,new_feess_balance=0;

    // All EditText


    @BindView(R.id.p_amount)
    EditText p_amount;
    @BindView(R.id.t_amount)
    EditText t_amount;
    @BindView(R.id.c_amount)
    EditText c_amount;
    @BindView(R.id.g_total)
    EditText g_total;

    @BindView(R.id.new_fee_balance)
    EditText new_fee_balance;

    @BindView(R.id.edtReason)
    EditText edtReason;


    @BindView(R.id.b_name)
    EditText b_name;
    @BindView(R.id.b_a_no)
    EditText b_a_no;
    @BindView(R.id.cheque)
    EditText cheque;




    // All TextView


    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.submit)
    RelativeLayout submit;
    //    @BindView(R.id.textNorecord)
    TextView txtNorecord;

    @BindView(R.id.fee_view)
    TextView fee_view;

    @BindView(R.id.pay_view)
    TextView pay_view;

    @BindView(R.id.paying_amount)
    TextView paying_amount;

    //    @BindView(R.id.txt_list)


    // All LinearLayout
    @BindView(R.id.bank_lyt)
    LinearLayout bank_lyt;
    @BindView(R.id.online_lyt)
    CardView online_lyt;

    @BindView(R.id.top)
    LinearLayout top;
    @BindView(R.id.bottom)
    LinearLayout bottom;
//
//    @BindView(R.id.list)
//    RelativeLayout list_view;

    @BindView(R.id.pay_close)
    LinearLayout pay_close;

    @BindView(R.id.spinner)
    Spinner bank_spinner;


    ArrayList<String> bankList = new ArrayList<>();

    String selectBankType ="",name="",f_name="",roll="",
            image="",gender="",regist="",classid="",fromdate="",todate="",class_name="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_fee_s);

        ButterKnife.bind(this);

        commonProgress=new CommonProgress(this);


        school= User.getCurrentUser().getSchoolData().getName();

        //TransAmount
        layoutManager = new LinearLayoutManager(PayFeeSActivity.this);
        recycler_trans.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        transAmountAdap = new TransAmountAdap(transAmounts,PayFeeSActivity.this);
        recycler_trans.setAdapter(transAmountAdap);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        //ParticularBean
        layoutManager = new LinearLayoutManager(PayFeeSActivity.this);
        recycler_view.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        newsAdapter = new Particular_Adapter(classList,PayFeeSActivity.this);
        recycler_view.setAdapter(newsAdapter);

        if (!restorePrefData()){
            setShowcaseView();
        }
        setData();
        fetchSession();
//        userprofile();
        fetchBankType();

    }


    private void setData() {



        g_total.setEnabled(false);


        p_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!p_amount.getText().toString().isEmpty()) {
                    p_value= Integer.parseInt(p_amount.getText().toString());
                }else {
                    p_value=0;
                    p_amount.setHint("0");
                    if (check==0){
                        showOnce();
                    }
                }


                setTotalvalues("p_amount");
            }
        });

        t_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!t_amount.getText().toString().isEmpty()) {
                    t_value= Integer.parseInt(t_amount.getText().toString());
                }else {
                    t_value=0;
                    t_amount.setHint("0");
                    if (check==0){
                        showOnce();
                    }

                }


                setTotalvalues("t_amount");
            }
        });

        c_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (!c_amount.getText().toString().isEmpty()) {
                    c_value = Integer.parseInt(c_amount.getText().toString());
                }else {
                    c_value=0;
                    c_amount.setHint("0");
                    if (check==0){
                        showOnce();
                    }
                }
                setTotalvalues("c_amount");
            }
        });


    }

    private void showOnce() {
        check++;
        Toast.makeText(getApplicationContext(),"Kindly enter valid amount\nThank You!",Toast.LENGTH_LONG).show();
    }

    private void setTotalfinevalues() {

        new_total_fine=p_fine_balance+today_add_fine;

//        if (new_total_fine >= paid_fine_value) {
//            new_f_balance=new_total_fine-paid_fine_value;
//            new_fine_balance.setText(String.valueOf(new_f_balance));
//
//        }else {
//            Toast.makeText(getApplicationContext(),"Kindly enter valid amount, You are putting an amount\n more than the actual fine\nThank You!",Toast.LENGTH_LONG).show();
//            paid_fine.setText("0");
//            new_fine_balance.setText(String.valueOf(new_total_fine));
//        }

        int fees=Integer.parseInt(g_total.getText().toString());
        int paying=paid_fine_value+fees;
        paying_amount.setText(String.valueOf(paying));

    }

    private void setTotalvalues(String tag) {


        if (tag.equalsIgnoreCase("p_amount")){
            total_value=p_value+t_value-c_value;
            if (totalPrice_withoutTrans >= p_value) {
                g_total.setText(String.valueOf(total_value));
                new_feess_balance=new_totalPrice-p_value+t_value;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            }else {
                Toast.makeText(getApplicationContext(),"Kindly enter valid amount, You are putting an amount\n more than the actual Class fee\nThank You!",Toast.LENGTH_LONG).show();
                this.p_amount.setText("0");
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        }else if (tag.equalsIgnoreCase("t_amount")){
            total_value=p_value+t_value-c_value;
            if (trans_amount >= t_value) {
                g_total.setText(String.valueOf(total_value));
                int check=p_value+t_value;
                new_feess_balance=new_totalPrice-check;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            }else {
                Toast.makeText(getApplicationContext(),"Kindly enter valid amount, You are putting an amount\n more than the actual Transport fee\nThank You!",Toast.LENGTH_LONG).show();
                this.t_amount.setText("0");
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        }else {
            int tot_value=p_value+t_value;
            total_value=p_value+t_value-c_value;
            g_total.setText(String.valueOf(total_value));
            if (tot_value >= c_value) {
                int check=p_value+t_value;
                new_feess_balance=new_totalPrice-check;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            }else {
                Toast.makeText(getApplicationContext(),"Kindly enter valid amount, You are putting an amount\n more than the actual total fee\nThank You!",Toast.LENGTH_LONG).show();
                this.c_amount.setText("0");
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        }


        int fees=Integer.parseInt(g_total.getText().toString());
        int paying=paid_fine_value+fees;

        if (paying==0){
            bottom.setVisibility(View.GONE);
        }else {
            bottom.setVisibility(View.VISIBLE);
        }
        paying_amount.setText(String.valueOf(paying));


    }

    @OnClick(R.id.fee_close)
    public void fee_close() {
        //  Toast.makeText(getApplicationContext(),"Select view_std",Toast.LENGTH_SHORT).show();
        fee_close.setVisibility(View.GONE);
        fee_view.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.fee_view)
    public void fee_view() {
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        fee_close.setVisibility(View.VISIBLE);
        fee_view.setVisibility(View.GONE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.online_lyt)
    public void online_lyt() {
        add.startAnimation(animation);
        //  assignData();
//        ImagePicker.Companion.with(PayFeeSActivity.this)
//                .galleryOnly()
//                .crop()	    			//Crop image(Optional), Check Customization for more option
//                .compress(1024)			//Final image size will be less than 1 MB(Optional)
//                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
//                .start();
        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {

                onImageViewClick();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                onImageViewClick();

            }

        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        //  Log.e("imageView6", String.valueOf(r));
        if (r.getError() == null) {
            //If you want the Bitmap.
            imgShow.setImageBitmap(r.getBitmap());
            bitmap= r.getBitmap();
            imgShow.setVisibility(View.VISIBLE);
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @OnClick(R.id.contact_support)
    public void contact_support() {
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        //  commonIntent();
        contact_support.startAnimation(animation);
        startActivity(new Intent(PayFeeSActivity.this,FeesReceiptList.class));
        Animatoo.animateShrink(this);
    }


    @OnClick(R.id.pay_close)
    public void pay_close() {
        //  Toast.makeText(getApplicationContext(),"Select view_std",Toast.LENGTH_SHORT).show();
        pay_close.setVisibility(View.GONE);
        pay_view.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.pay_view)
    public void pay_view() {
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        pay_close.setVisibility(View.VISIBLE);
        pay_view.setVisibility(View.GONE);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        commonBack();
    }

    private void commonBack() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PayFeeSActivity.this);
        builder1.setMessage(getString(R.string.alert_body));
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void fetchBankType() {
        bankList.add("Online");
        bankList.add("Cheque");
        bankList.add("DD");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, bankList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        bank_spinner.setAdapter(dataAdapter);
        //attach the listener to the spinner
        bank_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                selectBankType = bankList.get(pos);
                if (selectBankType.equalsIgnoreCase("Online")){
                    bank_lyt.setVisibility(View.GONE);
                    online_lyt.setVisibility(View.VISIBLE);
                }else {
                    bank_lyt.setVisibility(View.VISIBLE);
                    online_lyt.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),"Kindly Add Bank Details \n",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void savePrefData(){
        sharedPreferences= PayFeeSActivity.this.getSharedPreferences("boarding_pref_list11",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_list11",true);
        editor.apply();
    }


    private boolean restorePrefData(){
        sharedPreferences = PayFeeSActivity.this.getSharedPreferences("boarding_pref_list11",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_list11",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,getString(R.string.list_title_1),getString(R.string.list_desc_1))
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        // commonIntent();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();


    }

    private void fetchExra(SessionBean session) {
        HashMap<String, String> params = new HashMap<>();
        params.put("s_id", User.getCurrentUser().getId());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EXTRADETAILS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                if (error == null) {
                    try {

                        String fee_cate_id="",place_id="",route_id="";
                        extraDetails = ExtraDetails.parseParticularBeanArray(jsonObject.getJSONArray("extra_data"));

                        fee_cate_id= extraDetails.get(0).getFee_cat_id();

                        route_id=extraDetails.get(0).getRoute_id();

                        place_id=extraDetails.get(0).getPlace_id();

                        Log.e("USERDATA", fee_cate_id+"\n"+route_id+"\n"+place_id);

                        fetchFeesAmount(session);
                        fetchPreviousbalance(session,fee_cate_id);
                        fetTranAmount(session,place_id,route_id);

                        //  Toast.makeText(AdddocumentActivity.this, "Documents list!", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    fetchFeesAmount(session);
                    fetchPreviousbalance(session,fee_cate_id);
                    fee_close.setVisibility(View.VISIBLE);
                    noNoticeData.setVisibility(View.GONE);
                    //   Toast.makeText(MonthlyFees.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);

    }

    private void fetchSession() {
        // ErpProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(PayFeeSActivity.this, sessionlist);
                    spinnerFinancialYear.setAdapter(adapter);
                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            SessionBean session = sessionlist.get(i);
                            fromdate=session.getStart_date();
                            todate=session.getEnd_date();
                            fetchmonthList(session);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //  Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList(SessionBean session) {

        Calendar cal=Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        month_name = month_date.format(cal.getTime());

        if (month_name.equalsIgnoreCase("April")){
            monthList.add("April");
        }else if (month_name.equalsIgnoreCase("May")){
            monthList.add("April");
            monthList.add("May");
        }else if (month_name.equalsIgnoreCase("June")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
        }else if (month_name.equalsIgnoreCase("July")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
        }else if (month_name.equalsIgnoreCase("August")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
        }else if (month_name.equalsIgnoreCase("September")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
        }else if (month_name.equalsIgnoreCase("October")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
        }else if (month_name.equalsIgnoreCase("November")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
        }else if (month_name.equalsIgnoreCase("December")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
            monthList.add("December");
        }else if (month_name.equalsIgnoreCase("January")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
            monthList.add("December");
            monthList.add("January");
        }else if (month_name.equalsIgnoreCase("February")){
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
            monthList.add("December");
            monthList.add("January");
            monthList.add("February");
        }else {
            monthList.add("April");
            monthList.add("May");
            monthList.add("June");
            monthList.add("July");
            monthList.add("August");
            monthList.add("September");
            monthList.add("October");
            monthList.add("November");
            monthList.add("December");
            monthList.add("January");
            monthList.add("February");
            monthList.add("March");
        }



        //   Toast.makeText(getApplicationContext(),month_name,Toast.LENGTH_SHORT).show();


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, monthList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);

//        Calendar cal=Calendar.getInstance();
//        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
//        month_name = month_date.format(cal.getTime());

        // setting here spinner item selected with position..
        for(int i=0;i<monthList.size();i++) {
            if (monthList.get(i).equalsIgnoreCase(month_name)) {
                selectMonth = monthList.get(i);
                spinnerMonth.setSelection(i);

            }
        }

        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                selectMonth = monthList.get(pos);
                fetchFeesDetails(selectMonth,session);

                if (!selectMonth.equalsIgnoreCase("")) {
                    String title = getColoredSpanned(selectMonth, "#F55B53");
                    String Name = getColoredSpanned(" MONTH FEES DETAILS", "#000000");
                    // f_details.setText(Html.fromHtml(title + " " + Name));
                    f_details.setText(Html.fromHtml("'"+title+"'" + " " + Name));
                    // txt_list.setText(Html.fromHtml(title + " " + Name));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    private void fetchFeesDetails(String month,SessionBean session) {

        balance.setText("0");
        totals.setText("0");
        p_amount.setText("0");
        t_amount.setText("0");
        c_amount.setText("0");
        g_total.setText("0");
        new_fee_balance.setText("0");

        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", month);
        params.put("check", "fees");
        params.put("c_id", User.getCurrentUser().getClass_id());
        params.put("s_id", User.getCurrentUser().getId());
        params.put("fromdate", session.getStart_date());
        params.put("todate", session.getEnd_date());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    //  ErpProgress.cancelProgressBar();
                    commonProgress.dismiss();
                    openDialog("start");
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                    Toast.makeText(getApplicationContext(),"You have already paid "+selectMonth+" Month Fees \n kindly select another Month",Toast.LENGTH_LONG).show();
                    top.setVisibility(View.GONE);
                    bottom.setVisibility(View.GONE);
                    noNoticeData.setVisibility(View.VISIBLE);
                    totalRecord.setText(getString(R.string.t_entries)+" 0");
                } else {
                    //  ErpProgress.cancelProgressBar();
                    // commonProgress.dismiss();
                    // fetchExra(session);
                    fetchFeesReceiptDetails(month,session);
                    //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    private void fetchFeesReceiptDetails(String month,SessionBean session) {

        // commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", month);
        params.put("check", "online");
        params.put("c_id", User.getCurrentUser().getClass_id());
        params.put("s_id", User.getCurrentUser().getId());
        params.put("fromdate", session.getStart_date());
        params.put("todate", session.getEnd_date());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    //  ErpProgress.cancelProgressBar();
                    commonProgress.dismiss();
                    openDialog("receipt");
                    Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                    Toast.makeText(getApplicationContext(),"You have already Add "+selectMonth+" Month Fees Receipt\n kindly select another Month",Toast.LENGTH_LONG).show();
                    top.setVisibility(View.GONE);
                    bottom.setVisibility(View.GONE);
                    noNoticeData.setVisibility(View.VISIBLE);
                    totalRecord.setText(getString(R.string.t_entries)+" 0");
                } else {
                    //  ErpProgress.cancelProgressBar();
                    commonProgress.dismiss();
                    fetchExra(session);
                    //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    private void openDialog(String value) {

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.fee_success_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(false);

        recyclerView=(RecyclerView) sheetView.findViewById(R.id.recyclerview_list);
        close=(ImageView) sheetView.findViewById(R.id.close);
        txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
//        txtNorecord=(TextView) sheetView.findViewById(R.id.textNorecord);
        txt_list=(TextView) sheetView.findViewById(R.id.txt_list);
        String titlesss="";
        if (value.equalsIgnoreCase("start")){
            txtSetup.setText(getString(R.string.paid_fees));
            titlesss=" MONTH FEES DETAILS";
        }else{
            txtSetup.setText(getString(R.string.paid_fees_res));
            titlesss=" MONTH FEES RECEIPT DETAILS";
        }

        //FeeListAdapter
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeeListAdapter(paid_beanslist,this, this);
        recyclerView.setAdapter(mAdapter);

        if (!selectMonth.equalsIgnoreCase("")) {
            String title = getColoredSpanned(selectMonth, "#F55B53");
            String Name = getColoredSpanned(titlesss, "#000000");
            txt_list.setText(Html.fromHtml("'"+title+"'" + " " + Name));
        }

        fetchFeeDetail(value);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (value.equalsIgnoreCase("start")){
                    mBottomSheetDialog.dismiss();
                }else{
                    mBottomSheetDialog.dismiss();
                    // finish();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (value.equalsIgnoreCase("start")){
                    mBottomSheetDialog.dismiss();
                }else{
                    mBottomSheetDialog.dismiss();
                    //  finish();
                }
                // finish();
            }
        });
        mBottomSheetDialog.show();

    }

    private void fetchFeeDetail(String value) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", User.getCurrentUser().getId());
        if (value.equalsIgnoreCase("start")){
            params.put("check", "fees");
        }else{
            params.put("check", "online");
        }
        params.put("month", selectMonth);
        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                if (paid_beanslist != null) {
                    paid_beanslist.clear();

                }
                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    paid_beanslist = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    if (paid_beanslist.size() > 0) {
                        mAdapter.setMerchantBeans(paid_beanslist);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();

                    } else {
                        //   totalRecord.setText(getString(R.string.t_entries)+" 0");
                        paid_beanslist.clear();
                        mAdapter.setMerchantBeans(paid_beanslist);
                        mAdapter.notifyDataSetChanged();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Log.i("Fee_Paid_Bean", String.valueOf(list.get(0).getEs_particularamount())+ String.valueOf(list.get(0).getId()));
                // Log.i("Fee_Paid_Bean", String.valueOf(jsonObject));
            } else {
                //  totalRecord.setText(getString(R.string.t_entries)+" 0");
                paid_beanslist.clear();
                mAdapter.setMerchantBeans(paid_beanslist);
                mAdapter.notifyDataSetChanged();
                // txtNorecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };


    private void fetTranAmount(SessionBean session, String place_id, String route_id) {
        trans_amount=0;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("s_id", User.getCurrentUser().getClass_id());
        params.put("place_id", place_id);
        params.put("route_id", route_id);
        params.put("month", "trans");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, apiCall, this, params);
    }

    ApiHandler.ApiCallback apiCall = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();
            if (error == null) {
                //  ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                try {
                    if (transAmounts != null) {
                        transAmounts.clear();
                    }

                    if (transAmounts != null){
                        transAmounts.clear();
                        transAmounts=TransAmount.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        transAmountAdap.setFeeList(transAmounts);
                        transAmountAdap.notifyDataSetChanged();

                        for (int i = 0; i<transAmounts.size(); i++)
                        {
                            trans_amount += Integer.parseInt(transAmounts.get(i).getTrans_amount());
                        }
                        setTotals();
                        // Toast.makeText(getApplicationContext(),"totals "+totalPrice,Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                //  ErpProgress.cancelProgressBar();

                transAmounts.clear();
                transAmountAdap.setFeeList(transAmounts);
                transAmountAdap.notifyDataSetChanged();
            }
        }
    };

    private void fetchPreviousbalance(SessionBean session,String fee_cate_id) {
        totalPrice=0;
        //  ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", selectMonth);
        params.put("c_id", User.getCurrentUser().getClass_id());
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", session.getStart_date());
        params.put("todate", session.getEnd_date());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, apiCallback1, this, params);
    }

    ApiHandler.ApiCallback apiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();
            if (error == null) {
                //  ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                try {
                    if (classList != null) {
                        classList.clear();
                    }

                    if (classList != null){
                        classList.clear();
                        classList=ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter.setFeeList(classList);
                        newsAdapter.notifyDataSetChanged();
                        for (int i = 0; i<classList.size(); i++)
                        {
                            totalPrice += Integer.parseInt(classList.get(i).getFee_amount());
                        }
                        // Toast.makeText(getApplicationContext(),"totals "+totalPrice,Toast.LENGTH_SHORT).show();
                        setTotals();
//                        new_totalPrice=p_balance+totalPrice;
//                      totals.setText(String.valueOf(new_totalPrice));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                // ErpProgress.cancelProgressBar();
                //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                classList.clear();
                newsAdapter.setFeeList(classList);
                newsAdapter.notifyDataSetChanged();
                fee_close.setVisibility(View.GONE);
                noNoticeData.setVisibility(View.VISIBLE);
            }
        }
    };

    private void setTotals() {
        top.setVisibility(View.VISIBLE);
        // bottom.setVisibility(View.VISIBLE);
        // fee_close.setVisibility(View.VISIBLE);
        noNoticeData.setVisibility(View.GONE);
        totalRecord.setText(getString(R.string.t_entries)+" 1");
        totalPrice_withoutTrans=totalPrice+p_balance;
        new_totalPrice=p_balance+totalPrice+trans_amount;
        totals.setText(String.valueOf(new_totalPrice));
    }

    private void fetchFeesAmount(SessionBean session) {

        // ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", "all");
        params.put("c_id", User.getCurrentUser().getClass_id());
        params.put("s_id", User.getCurrentUser().getId());
        params.put("fromdate", session.getStart_date());
        params.put("todate", session.getEnd_date());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, apiCallback2, this, params);
    }

    ApiHandler.ApiCallback apiCallback2 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                commonProgress.dismiss();
                //  Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                //  Toast.makeText(getApplicationContext(),"select another Month",Toast.LENGTH_SHORT).show();

                try {
                    list=FeeBalanceBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                    p_balance= Integer.parseInt(list.get(0).getFee_bal());
                    p_fine_balance= Integer.parseInt(list.get(0).getBalance_fine());

//                    Toast.makeText(getApplicationContext(),new_totalPrice+p_balance+p_fine_balance,Toast.LENGTH_LONG).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setFeeData(p_balance,p_fine_balance);
                setTotals();

            } else {
                commonProgress.dismiss();

                //    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
//                fetchFeesAmount();
//                fetchPreviousbalance();
            }
        }
    };
    private void setFeeData(int p_balance, int p_fine_balance) {



        Log.e("hahaha", String.valueOf(p_balance));
        Log.e("hahaha", String.valueOf(p_fine_balance));
        Log.e("hahaha", String.valueOf(totalPrice));
        Log.e("hahaha", String.valueOf(new_totalPrice));

//        fine_balance.setText(String.valueOf(p_fine_balance));
//        total_fine.setText(String.valueOf(p_fine_balance));
        balance.setText(String.valueOf(p_balance));


        //   totals.setText(String.valueOf(new_totalPrice));

    }

    @OnClick(R.id.submit)
    public void submit() {
        if (selectMonth.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),"Kindly Select Fees Pay Month",Toast.LENGTH_LONG).show();
        }else if (bitmap==null){
            Toast.makeText(getApplicationContext(),"Kindly Add Fees Paid Receipt",Toast.LENGTH_LONG).show();
        }
        else{
            commonProgress.show();
            HashMap<String, String> params = new HashMap<>();

            if (p_amount.getText().toString().isEmpty()) {
                params.put("particular_amount", "0");
            }else {
                params.put("particular_amount", p_amount.getText().toString());
            }

            params.put("last_fee_bal", balance.getText().toString());
            params.put("total_amount", totals.getText().toString());
            params.put("fee_bal", new_fee_balance.getText().toString());

            if (t_amount.getText().toString().isEmpty()) {
                params.put("es_transportfee", "0");
            }else {
                params.put("es_transportfee", t_amount.getText().toString());
            }



            params.put("es_orgnl_transfee", String.valueOf(trans_amount));

            if (c_amount.getText().toString().isEmpty()) {
                params.put("concession_amount", "0");
            }else {
                params.put("concession_amount", c_amount.getText().toString());
            }


            params.put("fine_pay", "0");


            params.put("fine", "0");
            params.put("fine_bal", "0");



            params.put("classid", User.getCurrentUser().getClass_id());

            params.put("payment_mode", selectBankType);

            params.put("bank_name", b_name.getText().toString());
            params.put("acc_no", b_a_no.getText().toString());
            params.put("dd_chk_no", cheque.getText().toString());


            if (edtReason.getText().toString().isEmpty()) {
                params.put("note", "Done");
            }else {
                params.put("note", edtReason.getText().toString());
            }

            params.put("todate", todate);
            params.put("fromdate", fromdate);

            params.put("es_months", selectMonth);
            params.put("student_id", User.getCurrentUser().getId());

            if (bitmap != null) {
                String encoded = Utils.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDFEES, apisendmsgCallback, this, params);
        }
    }

    private void commonToast(String s) {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }

    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    //openDialog("pay");
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                // Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }
    };



    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {

        if (fee_paid_bean.getScreenshot()!=null) {
            viewwwwReceipt(fee_paid_bean);
        }else {
            Gson gson = new Gson();
            String prod_data = gson.toJson(fee_paid_bean, Fee_Paid_Bean.class);
            Intent intent = new Intent(PayFeeSActivity.this, FeeDetailsActivity.class);
            intent.putExtra("fee_data", prod_data);
            startActivity(intent);
            Animatoo.animateZoom(this);
        }
    }

    private void viewwwwReceipt(Fee_Paid_Bean fee_paid_bean) {

        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView img = (ImageView) dialogLog.findViewById(R.id.img);

        if (fee_paid_bean.getScreenshot()!=null) {
            Picasso.get().load(fee_paid_bean.getScreenshot()).placeholder(R.drawable.logo).into(img);
        }

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();

    }
}