package com.ultimate.ultimatesmartstudent.ForCamera;

import static android.graphics.BitmapFactory.decodeStream;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ImageHandler {

    private Context context;
    private Uri uri;
    private EPickType provider;
    private PickSetup setup;

    public ImageHandler(Context context) {
        this.context = context;
    }

    public static ImageHandler with(Context context) {
        return new ImageHandler(context);
    }

    public ImageHandler uri(Uri uri) {
        this.uri = uri;
        return this;
    }

    public ImageHandler provider(EPickType provider) {
        this.provider = provider;
        return this;
    }

    public ImageHandler setup(PickSetup setup) {
        this.setup = setup;
        return this;
    }

    private Bitmap rotateIfNeeded(Bitmap bitmap) {
        int rotation = getRotation();

        if (rotation != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }

        return bitmap;
    }

    private int getRotation() {
        try {
            ExifInterface exif = new ExifInterface(uri.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90;
                default:
                    return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public Bitmap decode() throws FileNotFoundException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        decodeStream(context.getContentResolver().openInputStream(uri), null, options);

        int imageWidth = options.outWidth;
        int imageHeight = options.outHeight;

        int scaleFactor = calculateScaleFactor(imageWidth, imageHeight);

        options.inJustDecodeBounds = false;
        options.inSampleSize = scaleFactor;

        Bitmap bitmap = decodeStream(context.getContentResolver().openInputStream(uri), null, options);

        if (bitmap != null) {
            bitmap = rotateIfNeeded(bitmap);
        }

        return bitmap;
    }

    private int calculateScaleFactor(int imageWidth, int imageHeight) {
        int scaleFactor = 1;

        int targetWidth = setup.getWidth();
        int targetHeight = setup.getHeight();

        if (targetWidth > 0 && targetHeight > 0 && (imageWidth > targetWidth || imageHeight > targetHeight)) {
            final int halfWidth = imageWidth / 2;
            final int halfHeight = imageHeight / 2;

            while ((halfWidth / scaleFactor) >= targetWidth && (halfHeight / scaleFactor) >= targetHeight) {
                scaleFactor *= 2;
            }
        }

        return scaleFactor;
    }

    public Uri getUri() {
        return uri;
    }

    public String getUriPath() {
        if (provider.equals(EPickType.CAMERA)) {
            return uri.getPath();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return getGalleryNewPath(uri);
            } else {
                return getGalleryPath();
            }
        }
    }

    private String getGalleryNewPath(Uri uri) {
        File folder = new File(context.getFilesDir() + "/Pictures");
        if (!folder.exists()) {
            folder.mkdir();
        }

        File galleryFile = new File(folder.getAbsolutePath(), getGalleryFileName(uri));
        try {
            FileOutputStream fos = new FileOutputStream(galleryFile);
            InputStream fis = context.getContentResolver().openInputStream(uri);
            copy(fis, fos);
            fos.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return galleryFile.getPath();
    }

    private void copy(InputStream source, OutputStream target) throws IOException {
        byte[] buf = new byte[8192];
        int length;
        while ((length = source.read(buf)) > 0) {
            target.write(buf, 0, length);
        }
    }

    private String getGalleryFileName(Uri uri) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, null, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            return "FILE_" + getTimestampString() + "." + getExtension(uri);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private String getExtension(Uri uri) {
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        return (extension != null) ? extension : "";
    }

    private String getTimestampString() {
        Calendar date = Calendar.getInstance();
        return new SimpleDateFormat("yyyy MM dd hh mm ss", Locale.US).format(date.getTime()).replace(" ", "");
    }

    private String getGalleryPath() {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            return uri.getPath();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
