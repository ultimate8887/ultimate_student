package com.ultimate.ultimatesmartstudent.ForCamera;

/**
 * Created by jrvansuita build 02/12/16.
 */

public interface IPickClick {
    void onGalleryClick();

    void onCameraClick();
}
