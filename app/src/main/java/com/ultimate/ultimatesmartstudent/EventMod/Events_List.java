package com.ultimate.ultimatesmartstudent.EventMod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.Classwork.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.Homework.HomeWorkByDate;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeAdapter;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class Events_List extends AppCompatActivity implements Eventlist_adapter.Mycallback {

    CommonProgress commonProgress;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    TextToSpeech textToSpeech;
    @BindView(R.id.recyclerView)
    RecyclerView nbRV;

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;

    @BindView(R.id.textNorecord)
    TextView noNoticeData;
    Animation animation;
    String folder_main = "NoticeBoard";
    String textholder="",school="";
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.txtSub)
    TextView txtSub;


    private Eventlist_adapter adapter;
    ArrayList<Eventlist_bean> eventList = new ArrayList<>();
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_new);
        ButterKnife.bind(this);
        nbRV.setNestedScrollingEnabled(false);
        txtTitle.setText(getString(R.string.event)+" "+getString(R.string.list));
       // txtTitle.setText(" Event-List");
        txtSub.setText(Utils.setNaviHeaderClassData());
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        layoutManager = new LinearLayoutManager(Events_List.this);
        nbRV.setLayoutManager(layoutManager);
        adapter = new Eventlist_adapter(eventList, Events_List.this,this);
        nbRV.setAdapter(adapter);
        fetchEventlist();
    }
    private void fetchEventlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EVENTLIST_URL, apiCallback, this, params);

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (eventList != null) {
                        eventList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("event_data");
                    eventList = Eventlist_bean.parseEVENTArray(jsonArray);
                    if (eventList.size() > 0) {
                        adapter.setEventList(eventList);
                        adapter.notifyDataSetChanged();
                        nbRV.setVisibility(View.VISIBLE);
                        noNoticeData.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                    } else {
                        adapter.setEventList(eventList);
                        adapter.notifyDataSetChanged();
                        noNoticeData.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- 0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                noNoticeData.setVisibility(View.VISIBLE);
                eventList.clear();
                adapter.setEventList(eventList);
                adapter.notifyDataSetChanged();
            }
        }
    };
    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    @Override
    public void onMethod_Image_callback(Eventlist_bean data) {
        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(data.getTitle());
        ViewPagerAdapter mAdapter = new  ViewPagerAdapter(this,data.getImage(),"event");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });


        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);

        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.VISIBLE);

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=data.getImage();
                Intent intent = new Intent(Events_List.this, IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "event");
                intent.putExtra("title", getString(R.string.event));
                intent.putExtra("sub", "Event_id:- "+data.getEvent_id());
                startActivity(intent);
            }
        });

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

}