package com.ultimate.ultimatesmartstudent.EventMod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AchievementList extends AppCompatActivity implements Achievementlist_adapter.Mycallback {
    CommonProgress commonProgress;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    TextToSpeech textToSpeech;
    @BindView(R.id.recyclerView)
    RecyclerView nbRV;

    @BindView(R.id.textNorecord)
    TextView noNoticeData;
    Animation animation;
    String folder_main = "NoticeBoard";
    String textholder="",school="";
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.txtSub)
    TextView txtSub;
    String a_sign="";

    private Achievementlist_adapter adapter;
    ArrayList<Acievement_bean> eventList = new ArrayList<>();
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_new);
        ButterKnife.bind(this);
        nbRV.setNestedScrollingEnabled(false);
        txtTitle.setText(getString(R.string.achive));
        // txtTitle.setText(" Event-List");
        txtSub.setText(Utils.setNaviHeaderClassData());
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        layoutManager = new LinearLayoutManager(AchievementList.this);
        nbRV.setLayoutManager(layoutManager);
        adapter = new Achievementlist_adapter(eventList, AchievementList.this,this);
        nbRV.setAdapter(adapter);
        fetchEventlist();
        userprofile();
    }
    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        //commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                //  commonProgress.dismiss();
                //  ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        a_sign = jsonObject.getJSONObject(Constants.USERDATA).getString("a_signature");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }
    private void fetchEventlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GETACHIVMNT_URL, apiCallback, this, params);

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (eventList != null) {
                        eventList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("acieve_data");
                    eventList = Acievement_bean.parseavhievarray(jsonArray);
                    if (eventList.size() > 0) {
                        adapter.setachivList(eventList);
                        adapter.notifyDataSetChanged();
                        nbRV.setVisibility(View.VISIBLE);
                        noNoticeData.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                    } else {
                        adapter.setachivList(eventList);
                        adapter.notifyDataSetChanged();
                        noNoticeData.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- 0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                noNoticeData.setVisibility(View.VISIBLE);
                eventList.clear();
                adapter.setachivList(eventList);
                adapter.notifyDataSetChanged();
            }
        }
    };
    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }
    FloatingActionButton export;
    NestedScrollView scrollView;
    @Override
    public void onMethodCallback(Acievement_bean obj) {
        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.certificate_dialog);
        // dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //ImageView imgShow = (ImageView) dialogLog.findViewById(R.id.img);
        ImageView p_signature = (ImageView) dialogLog.findViewById(R.id.signature);
        if (!a_sign.equalsIgnoreCase("")) {
            Utils.setImageUri(AchievementList.this,a_sign,p_signature);
            // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
        } else {
            Picasso.get().load(R.color.transparent).into(p_signature);
        }
        TextView txtSchool = (TextView) dialogLog.findViewById(R.id.txtSchool);
        TextView txtAdd = (TextView) dialogLog.findViewById(R.id.txtAffillated);
        TextView txtTitle = (TextView) dialogLog.findViewById(R.id.txtAdd);
        TextView txtName = (TextView) dialogLog.findViewById(R.id.txtName);
        TextView txtDetails = (TextView) dialogLog.findViewById(R.id.txtDetails);
        TextView txtDetails1 = (TextView) dialogLog.findViewById(R.id.txtDetails1);
        txtTitle.setText("Certificate of Achievement");
        txtSchool.setText(User.getCurrentUser().getSchoolData().getName());
        txtAdd.setText(User.getCurrentUser().getSchoolData().getAddress());
        txtName.setText(obj.getStudent_name());
        txtDetails.setText("for outstanding performance in the "+obj.getEvent_name()+" Event, from "
                +Utils.getDateFormated(obj.getEvent_date())+
                ", awarded on "+ Utils.getDateFormated(obj.getParticipationdate()));


        String title5 = getColoredSpanned("Your hard work, dedication, and achievement will be cherished. Congratulations on securing the ", "#000800");
        String Name5 = "",Name6 = "";;

            Name5 = getColoredSpanned(obj.getPosition(), "#060b70");
            Name6 = getColoredSpanned("" +" position."+"", "#000800");

        txtDetails1.setText(Html.fromHtml(title5 + " " + Name5+ " " + Name6));


//        txtDetails1.setText("Your hard work, dedication, and achievement will be cherished. Congratulations on securing the "
//                +obj.getPosition()+" position.");

        export = (FloatingActionButton) dialogLog.findViewById(R.id.export);
        scrollView = (NestedScrollView) dialogLog.findViewById(R.id.scrollView);
        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                export.startAnimation(animation);
                int captureWidth = scrollView.getWidth();
                int captureHeight = scrollView.getHeight();

                // Get the visible portion of the ScrollView
                int visibleWidth = scrollView.getChildAt(0).getWidth();
                int visibleHeight = scrollView.getChildAt(0).getHeight();

                // Adjust the capture width and height if necessary
                if (visibleWidth < captureWidth) {
                    captureWidth = visibleWidth;
                }

                if (visibleHeight < captureHeight) {
                    captureHeight = visibleHeight;
                }

                // Capture the bitmap with adjusted parameters
                Bitmap bitmap = getBitmapFromView(scrollView, captureHeight, captureWidth);

                saveImageToGallery(bitmap);
            }
        });

        Button save = (Button) dialogLog.findViewById(R.id.submit);
       // save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                save.startAnimation(animation);

            }
        });

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
        Window window = dialogLog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }


    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;
        String school=User.getCurrentUser().getSchoolData().getName();
        long time= System.currentTimeMillis();
        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "achievement_certificate" +time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();

            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "achievement_certificate" +time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);


        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
//        layt.setVisibility(View.VISIBLE);
        // note.setVisibility(View.VISIBLE);
    }

}