package com.ultimate.ultimatesmartstudent.EventMod;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Admin on 1/15/2019.
 */

public class Acievement_bean {
    private static String ACHIEV_ID="achiv_id";
    private static String CLASS_ID="class_id";
    private static String CLASS_NAME="class_name";
    private static String STUDENT_ID="student_id";
    private static String STUDENT_NAME="student_name";
    private static String FATHER_NAME="father_name";
    private static String GENDER="gender";
    private static String MOBILENO="mobileno";
    private static String EVENT_ID="event_id";
    private static String EVENTDATE="event_date";

    private static String PARTICIPATION_DATE="participationdate";
    private static String EVENTNAME="event_name";
    private static String POSITION="position";
    /**
     * achiv_id : 5
     * class_id : 2
     * class_name : NURSERY1
     * student_id : 15
     * student_name : fname flname
     * father_name : fname
     * gender : male
     * mobileno : 3543451657
     * event_id : 11
     * event_date : 2019-01-14 to 2019-01-16
     * participationdate : 2019-01-15
     */

    private String achiv_id;
    private String class_id;
    private String class_name;
    private String student_id;
    private String student_name;
    private String father_name;
    private String gender;
    private String mobileno;
    private String event_id;
    private String event_date;
    private String participationdate;
    /**
     * event_name : Makar sankranti
     */

    private String event_name;
    /**
     * position : First
     */

    private String position;

    public String getAchiv_id() {
        return achiv_id;
    }

    public void setAchiv_id(String achiv_id) {
        this.achiv_id = achiv_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getParticipationdate() {
        return participationdate;
    }

    public void setParticipationdate(String participationdate) {
        this.participationdate = participationdate;
    }


    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public static ArrayList<Acievement_bean> parseavhievarray(JSONArray arrayObj) {
        ArrayList<Acievement_bean> list = new ArrayList<Acievement_bean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Acievement_bean p = parseavhievObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Acievement_bean parseavhievObject(JSONObject jsonObject) {
        Acievement_bean casteObj = new Acievement_bean();
        try {
            if (jsonObject.has(ACHIEV_ID)) {
                casteObj.setAchiv_id(jsonObject.getString(ACHIEV_ID));
            }

            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString(CLASS_ID));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(STUDENT_ID) && !jsonObject.getString(STUDENT_ID).isEmpty() && !jsonObject.getString(STUDENT_ID).equalsIgnoreCase("null")) {
                casteObj.setStudent_id(jsonObject.getString(STUDENT_ID));
            }
            if (jsonObject.has(STUDENT_NAME) && !jsonObject.getString(STUDENT_NAME).isEmpty() && !jsonObject.getString(STUDENT_NAME).equalsIgnoreCase("null")) {
                casteObj.setStudent_name(jsonObject.getString(STUDENT_NAME));
            } if (jsonObject.has(FATHER_NAME) && !jsonObject.getString(FATHER_NAME).isEmpty() && !jsonObject.getString(FATHER_NAME).equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString(FATHER_NAME));
            }
            if (jsonObject.has(GENDER) && !jsonObject.getString(GENDER).isEmpty() && !jsonObject.getString(GENDER).equalsIgnoreCase("null")) {
                casteObj.setGender(jsonObject.getString(GENDER));
            }
            if (jsonObject.has(MOBILENO) && !jsonObject.getString(MOBILENO).isEmpty() && !jsonObject.getString(MOBILENO).equalsIgnoreCase("null")) {
                casteObj.setMobileno(jsonObject.getString(MOBILENO));
            } if (jsonObject.has(EVENT_ID) && !jsonObject.getString(EVENT_ID).isEmpty() && !jsonObject.getString(EVENT_ID).equalsIgnoreCase("null")) {
                casteObj.setEvent_id(jsonObject.getString(EVENT_ID));
            }
            if (jsonObject.has(EVENTDATE) && !jsonObject.getString(EVENTDATE).isEmpty() && !jsonObject.getString(EVENTDATE).equalsIgnoreCase("null")) {
                casteObj.setEvent_date(jsonObject.getString(EVENTDATE));
            }
            if (jsonObject.has(EVENTNAME) && !jsonObject.getString(EVENTNAME).isEmpty() && !jsonObject.getString(EVENTNAME).equalsIgnoreCase("null")) {
                casteObj.setEvent_name(jsonObject.getString(EVENTNAME));
            }
            if (jsonObject.has(PARTICIPATION_DATE) && !jsonObject.getString(PARTICIPATION_DATE).isEmpty() && !jsonObject.getString(PARTICIPATION_DATE).equalsIgnoreCase("null")) {
                casteObj.setParticipationdate(jsonObject.getString(PARTICIPATION_DATE));
            }
            if (jsonObject.has(POSITION) && !jsonObject.getString(POSITION).isEmpty() && !jsonObject.getString(POSITION).equalsIgnoreCase("null")) {
                casteObj.setPosition(jsonObject.getString(POSITION));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
