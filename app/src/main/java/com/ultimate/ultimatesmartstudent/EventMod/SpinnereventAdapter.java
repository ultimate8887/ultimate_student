package com.ultimate.ultimatesmartstudent.EventMod;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartstudent.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;



/**
 * Created by Admin on 1/8/2019.
 */

public class SpinnereventAdapter extends BaseAdapter {
    Context context;
    ArrayList<Eventlist_bean> eventList;
    LayoutInflater inflter;


    public SpinnereventAdapter(Context context, ArrayList<Eventlist_bean> eventList) {

        this.context=context;
        this.eventList=eventList;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return eventList.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = convertView.findViewById(R.id.txtText);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Select Event");
        } else {
            Eventlist_bean classObj = eventList.get(position - 1);
            label.setText(classObj.getTitle());
            Log.e("eventtttle",classObj.getTitle());
        }
        return convertView;
    }

}
