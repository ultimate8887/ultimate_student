package com.ultimate.ultimatesmartstudent.EventMod;

import android.annotation.SuppressLint;
import android.content.Context;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.Gallery.ViewPagerAdapters;
import com.ultimate.ultimatesmartstudent.Homework.Homeworkadapter;
import com.ultimate.ultimatesmartstudent.Homework.Homeworkbean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Admin on 1/8/2019.
 */

public class Eventlist_adapter extends RecyclerView.Adapter<Eventlist_adapter.Viewholder>{
    private final Context context;
    ArrayList<Eventlist_bean> eventList;
    private final Mycallback mAdaptercall;

    public Eventlist_adapter(ArrayList<Eventlist_bean> eventList, Context context, Mycallback mAdaptercall) {
        this.context=context;
        this.eventList=eventList;
        this.mAdaptercall=mAdaptercall;

    }
    public interface Mycallback{
        public void onMethod_Image_callback(Eventlist_bean homeworkbean);
    }

    @NonNull
    @Override
    public Eventlist_adapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_adapt_lay_new, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull final Eventlist_adapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        holder.reason.setEnabled(false);
        if(eventList.get(position).getTitle()!=null) {
            holder.name.setText(eventList.get(position).getTitle() + "" +"" + "");
        }else{
            holder.name.setText(eventList.get(position).getTitle());
        }

        if (eventList.get(position).getDescription() != null) {
//            String title = getColoredSpanned("", "#000000");
//            String Name = getColoredSpanned(eventList.get(position).getDescription(), "#5A5C59");
//            holder.reason.setText(Html.fromHtml(title + " " + Name));
            holder.reason.setText(Html.fromHtml(eventList.get(position).getDescription()));
        }


        if (eventList.get(position).getEvent_id() != null) {
            String title = getColoredSpanned("Event ID: ", "#000000");
            String Name = getColoredSpanned(eventList.get(position).getEvent_id(), "#5A5C59");
            holder.txtRollNo.setText(Html.fromHtml(title + " " + Name));
        }

        if(eventList.get(position).getTodate().equalsIgnoreCase("0000-00-00")){

            String title = getColoredSpanned("", "#ff0099cc");
            String Name="";
                Name = getColoredSpanned(Utils.getDateFormated(eventList.get(position).getFromdate()), "#1C8B3B");
            holder.date.setText(Html.fromHtml(title + " " + Name));
            //  holder.date.setText(Utility.getDateFormated(leavelist.get(position).getFrom_date()));
        }else {
            String title = getColoredSpanned("", "#ff0099cc");
            String Name="";
                Name = getColoredSpanned(Utils.getDateFormated(eventList.get(position).getFromdate())+ " " + "to" + " " +
                        Utils.getDateFormated(eventList.get(position).getTodate()), "#1C8B3B");
            holder.date.setText(Html.fromHtml(title + " " + Name));
            //  holder.date.setText(Utility.getDateFormated(leavelist.get(position).getFrom_date()) + " " + "to" + " " + Utility.getDateFormated(leavelist.get(position).getTo_date()));
        }
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
        if (eventList.get(position).getImage()!=null) {
            Picasso.get().load(eventList.get(position).getImage().get(0)).placeholder(context.getResources().getDrawable(R.color.transparent)).into(holder.visitimage);
           // Log.i("Image_url",eventList.get(position).getImage().get(0));

            holder.visitimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.visitimage.startAnimation(animation);
                    if (mAdaptercall != null) {
                        mAdaptercall.onMethod_Image_callback(eventList.get(position));
                    }
                }
            });
        }

//        if (eventList.get(position).getGender().equalsIgnoreCase("Male")) {
//            if (eventList.get(position).getProfile()!=null) {
//                Picasso.get().load(eventList.get(position).getProfile()).placeholder(R.drawable.stud).into(holder.visitimage);
//            } else {
//                Picasso.get().load(R.drawable.stud).into(holder.visitimage);
//            }
//        }else{
//            if (eventList.get(position).getProfile()!=null) {
//                Picasso.get().load(eventList.get(position).getProfile()).placeholder(R.drawable.f_student).into(holder.visitimage);
//
//                //  Log.i("USERDATA",User.getCurrentUser().getProfile());
//                Log.i("USERDATA-1",eventList.get(position).getProfile());
//
//            } else {
//                Picasso.get().load(R.drawable.f_student).into(holder.visitimage);
//            }
//        }
        //  holder.txtRollNo.setText("Reg no. :"+leavelist.get(position).getS_id());

        if(eventList.get(position).getEs_coordinator()!=null) {
            holder.classname.setText(eventList.get(position).getEs_coordinator());
        }else{
            holder.classname.setText(eventList.get(position).getEs_coordinator());
        }

        holder.apply.setText(""+ Utils.getDateFormated(eventList.get(position).getLastdate()));

//        ViewPagerAdapters mAdapter = new  ViewPagerAdapters(context,eventList.get(position).getImage(),eventList.get(position).getMsg());
//        holder.intro_images.setAdapter(mAdapter);
//        holder.intro_images.setCurrentItem(0);
//        holder.indicator.setViewPager(holder.intro_images);


    }


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public void setEventList(ArrayList<Eventlist_bean> eventList) {
        this.eventList = eventList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName)
        TextView name;
        @BindView(R.id.apply)
        TextView apply;
        @BindView(R.id.txtDate)TextView date;
        @BindView(R.id.txtReason)
        EditText reason;
        @BindView(R.id.approve)
        ImageView approve;
        @BindView(R.id.wait)
        LinearLayout wait;
        @BindView(R.id.visitimage)
        ImageView visitimage;
        @BindView(R.id.unaprove)ImageView unapprove;
        @BindView(R.id.txtRollNo)TextView txtRollNo;
        @BindView(R.id.aprvimg)ImageView aproveimage;
        @BindView(R.id.rejetedimg)ImageView rejecteed;
        @BindView(R.id.txtClass)TextView classname;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
