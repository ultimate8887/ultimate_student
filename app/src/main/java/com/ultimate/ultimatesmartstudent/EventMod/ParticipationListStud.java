package com.ultimate.ultimatesmartstudent.EventMod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParticipationListStud extends AppCompatActivity implements Particpatlist_adapter.Mycallback {
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinnerGroups)
    Spinner eventnam;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private Particpatlist_adapter adapter;
    ArrayList<Particpatlist_bean> particpatList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    ArrayList<Eventlist_bean> eventList = new ArrayList<>();
    String event_id;
    int loaded=0;
    String a_sign="";
    @BindView(R.id.textView8)
    TextView textsubtitle;
    CommonProgress commonProgress;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        txtNorecord.setVisibility(View.VISIBLE);
        txtTitle.setText(getString(R.string.part_list));
        textsubtitle.setText(getString(R.string.f_leave));
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(this);
        layoutManager = new LinearLayoutManager(ParticipationListStud.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new Particpatlist_adapter(particpatList, ParticipationListStud.this,this);
        recyclerView.setAdapter(adapter);
        fetchEventlist();
        userprofile();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back.startAnimation(animation);
                finish();
            }
        });
    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        //commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
              //  commonProgress.dismiss();
                //  ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        a_sign = jsonObject.getJSONObject(Constants.USERDATA).getString("a_signature");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void fetchEventlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EVNTLISTLASTDATEPARTICIPATE_URL, eventapiCallback, this, params);

    }
    ApiHandler.ApiCallback eventapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    eventList = Eventlist_bean.parseEVENTArray(jsonObject.getJSONArray("event_data"));
                    SpinnereventAdapter adapterstu = new SpinnereventAdapter(ParticipationListStud.this, eventList);
                    eventnam.setAdapter(adapterstu);
                    eventnam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                event_id = eventList.get(i-1).getEvent_id();
                                fetchParticipationlist(event_id);

//                                if(eventList.get(i-1).getTodate().equalsIgnoreCase("0000-00-00")){
//                                    eventdate.setText("EventDate: "+ Utils.getDateFormated(eventList.get(i-1).getFromdate()));
//                                    eventdates=eventList.get(i-1).getFromdate();
//                                    participatedate.setText("ParticipationDate: "+ Utils.getDateFormated(eventList.get(i-1).getFromdate()));
//                                    participateDate=eventList.get(i-1).getFromdate();
//                                }else{
//                                    eventdate.setText("EventDate: "+Utils.getDateFormated(eventList.get(i-1).getFromdate())+" to "+Utils.getDateFormated(eventList.get(i-1).getTodate()));
//                                    participatedate.setText("ParticipationDate");
//                                    eventdates=eventList.get(i-1).getFromdate()+" to "+eventList.get(i-1).getTodate();
//                                }
//                                fromdate=eventList.get(i-1).getFromdate();
                            }else{
                                event_id="";
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
               // Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    public void fetchParticipationlist(String event_id){

        if(loaded==0){
            commonProgress.show();
        }loaded++;
        HashMap<String, String> params = new HashMap<String, String>();

        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.ADDEVNTPARTICIPATE_URL+ Constants.event_id+event_id+Constants.studid+ User.getCurrentUser().getId(),apicallback,this,params);
    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (particpatList != null) {
                        particpatList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("participationlist_data");
                    particpatList = Particpatlist_bean.parseparticipatearray(jsonArray);
                    if (particpatList.size() > 0) {
                        adapter.setparticipatList(particpatList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(particpatList.size()));
                    } else {
                        adapter.setparticipatList(particpatList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                txtNorecord.setVisibility(View.VISIBLE);
                particpatList.clear();
                adapter.setparticipatList(particpatList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    FloatingActionButton export;
    NestedScrollView scrollView;
    @Override
    public void onMethodCallback(Particpatlist_bean obj) {
        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.certificate_dialog);
        // dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView p_signature = (ImageView) dialogLog.findViewById(R.id.signature);
        if (!a_sign.equalsIgnoreCase("")) {
            Utils.setImageUri(ParticipationListStud.this,a_sign,p_signature);
            // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
        } else {
            Picasso.get().load(R.color.transparent).into(p_signature);
        }

        TextView txtSchool = (TextView) dialogLog.findViewById(R.id.txtSchool);
        TextView txtAdd = (TextView) dialogLog.findViewById(R.id.txtAffillated);
        TextView txtTitle = (TextView) dialogLog.findViewById(R.id.txtAdd);
        TextView txtName = (TextView) dialogLog.findViewById(R.id.txtName);
        TextView txtDetails = (TextView) dialogLog.findViewById(R.id.txtDetails);
        TextView txtDetails1 = (TextView) dialogLog.findViewById(R.id.txtDetails1);
        txtTitle.setText("Certificate of Participation");
        txtSchool.setText(User.getCurrentUser().getSchoolData().getName());
        txtAdd.setText(User.getCurrentUser().getSchoolData().getAddress());
        txtName.setText(obj.getStudent_name());
        txtDetails.setText("for outstanding participation in the "+obj.getEvent_name()+" Event, from "
                +Utils.getDateFormated(obj.getEvent_date())+
                ", awarded by "+ User.getCurrentUser().getSchoolData().getName());

        txtDetails1.setText("Your hard work, dedication, and participation will be cherished.");

        export = (FloatingActionButton) dialogLog.findViewById(R.id.export);
        scrollView = (NestedScrollView) dialogLog.findViewById(R.id.scrollView);
        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                export.startAnimation(animation);
                int captureWidth = scrollView.getWidth();
                int captureHeight = scrollView.getHeight();

                // Get the visible portion of the ScrollView
                int visibleWidth = scrollView.getChildAt(0).getWidth();
                int visibleHeight = scrollView.getChildAt(0).getHeight();

                // Adjust the capture width and height if necessary
                if (visibleWidth < captureWidth) {
                    captureWidth = visibleWidth;
                }

                if (visibleHeight < captureHeight) {
                    captureHeight = visibleHeight;
                }

                // Capture the bitmap with adjusted parameters
                Bitmap bitmap = getBitmapFromView(scrollView, captureHeight, captureWidth);

                saveImageToGallery(bitmap);
            }
        });

        Button save = (Button) dialogLog.findViewById(R.id.submit);
      //  save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                save.startAnimation(animation);

            }
        });

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
        Window window = dialogLog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }


    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;
        String school=User.getCurrentUser().getSchoolData().getName();
        long time= System.currentTimeMillis();
        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "participation_certificate" +time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();

            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "participation_certificate" +time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);


        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
//        layt.setVisibility(View.VISIBLE);
        // note.setVisibility(View.VISIBLE);
    }
}