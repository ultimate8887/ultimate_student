package com.ultimate.ultimatesmartstudent.EventMod;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Admin on 1/15/2019.
 */

public class Achievementlist_adapter extends RecyclerView.Adapter<Achievementlist_adapter.Viewholder>{
    ArrayList<Acievement_bean> particpatList;
    Context context;
    private final Mycallback mMycallback;

    public Achievementlist_adapter(ArrayList<Acievement_bean> particpatList, Context context, Mycallback mMycallback) {
        this.context=context;
        this.particpatList=particpatList;
        this.mMycallback = mMycallback;
    }

    public interface Mycallback {
        public void onMethodCallback(Acievement_bean obj);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.event_adapt_lay_new,parent,false);
        Viewholder viewholder=new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull final Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        holder.reason.setEnabled(false);
        holder.inpUsername.setHint("Achievement(Position)");
        holder.inpUsername.setVisibility(View.VISIBLE);
        holder.app_txt.setText("Participation Date: ");
        if(particpatList.get(position).getEvent_name()!=null) {
            holder.name.setText(particpatList.get(position).getEvent_name() + "" +"" + "");
        }else{
            holder.name.setText(particpatList.get(position).getEvent_name());
        }

        if (particpatList.get(position).getPosition() != null) {
            String title = getColoredSpanned("", "#000000");
            String Name = getColoredSpanned(particpatList.get(position).getPosition()+" Position", "#5A5C59");
            holder.reason.setText(Html.fromHtml(title + " " + Name));
        }
        //  holder.reason.setText(eventList.get(position).getDescription());

        if (particpatList.get(position).getEvent_id() != null) {
            String title = getColoredSpanned("Participation ID: ", "#000000");
            String Name = getColoredSpanned(particpatList.get(position).getEvent_id(), "#5A5C59");
            holder.txtRollNo.setText(Html.fromHtml(title + " " + Name));
        }

        holder.date.setText(""+particpatList.get(position).getEvent_date());

        if(particpatList.get(position).getStudent_name()!=null) {
            holder.classname.setText(particpatList.get(position).getStudent_name()+"("+particpatList.get(position).getClass_name()+")");
        }else{
            holder.classname.setText(particpatList.get(position).getStudent_id());
        }

        holder.apply.setText(""+ Utils.getDateFormated(particpatList.get(position).getParticipationdate()));

        holder.r2.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
        holder.r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.r2.startAnimation(animation);
                if (mMycallback != null) {
                    mMycallback.onMethodCallback(particpatList.get(position));
                }
            }
        });

//        holder.regid.setText("Reg.Id: "+particpatList.get(position).getStudent_id());
//        holder.classess.setText("Class: "+particpatList.get(position).getClass_name());
//        holder.eventdate.setText("Event-Date: "+particpatList.get(position).getEvent_date());
//        holder.eventname.setText("Event-Name: "+particpatList.get(position).getEvent_name());
//        holder.fathername.setText("Father's Name: "+particpatList.get(position).getFather_name());
//        holder.gender.setText("Gender: "+particpatList.get(position).getGender());
//        holder.mobileno.setText("Mobile No.: "+particpatList.get(position).getMobileno());
//        holder.name.setText("Student-Name: "+particpatList.get(position).getStudent_name());
//        holder.Participationdate.setText("Participation-Date: "+particpatList.get(position).getParticipationdate());
//        holder.achivment.setText("Achievement(Position): "+particpatList.get(position).getPosition());

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public int getItemCount() {
        return particpatList.size();
    }

    public void setachivList(ArrayList<Acievement_bean> particpatList) {
        this.particpatList = particpatList;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName)
        TextView name;
        @BindView(R.id.apply)
        TextView apply;
        @BindView(R.id.app)
        TextView app_txt;
        @BindView(R.id.txtDate)TextView date;
        @BindView(R.id.txtReason)
        EditText reason;
        @BindView(R.id.approve)
        ImageView approve;
        @BindView(R.id.wait)
        LinearLayout wait;
        @BindView(R.id.inpUsername)
        TextInputLayout inpUsername;
        @BindView(R.id.visitimage)
        ImageView visitimage;
        @BindView(R.id.unaprove)ImageView unapprove;
        @BindView(R.id.txtRollNo)TextView txtRollNo;
        @BindView(R.id.aprvimg)ImageView aproveimage;
        @BindView(R.id.rejetedimg)ImageView rejecteed;
        @BindView(R.id.txtClass)TextView classname;
        @BindView(R.id.r2)
        RelativeLayout r2;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
