package com.ultimate.ultimatesmartstudent.EventMod;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Admin on 1/8/2019.
 */

public class Particpatlist_bean {

    private static String ID="participent_id";
    private static String CLASS_ID="class_id";
    private static String CLASS_NAME="class_name";
    private static String STUDENT_ID="student_id";
    private static String STUDENT_NAME="student_name";
    private static String FATHER_NAME="father_name";
    private static String GENDER="gender";
    private static String MOBILENO="mobileno";
    private static String EVENT_ID="event_id";
    private static String EVENTDATE="event_date";

    private static String PARTICIPATION_DATE="participationdate";
    private static String EVENTNAME="event_name";

    /**
     * participent_id : 5
     * class_id : 2
     * class_name : NURSERY1
     * student_id : 1
     * student_name : Yadhvi Rajput
     * father_name : Sandeep Kumar
     * gender : female
     * mobileno : 7889267816
     * event_id : 5
     * event_date : 2019-01-05
     * participationlist : 2019-01-05
     * event_name : New year Event
     */

    private String participent_id;
    private String class_id;
    private String class_name;
    private String student_id;
    private String student_name;
    private String father_name;
    private String gender;
    private String mobileno;
    private String event_id;
    private String event_date;

    private String event_name;
    /**
     * participationdate : 2019-01-05
     */

    private String participationdate;

    public String getParticipent_id() {
        return participent_id;
    }

    public void setParticipent_id(String participent_id) {
        this.participent_id = participent_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }



    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getParticipationdate() {
        return participationdate;
    }

    public void setParticipationdate(String participationdate) {
        this.participationdate = participationdate;
    }


    public static ArrayList<Particpatlist_bean> parseparticipatearray(JSONArray arrayObj) {
        ArrayList<Particpatlist_bean> list = new ArrayList<Particpatlist_bean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Particpatlist_bean p = parseparticipateObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Particpatlist_bean parseparticipateObject(JSONObject jsonObject) {
        Particpatlist_bean casteObj = new Particpatlist_bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setParticipent_id(jsonObject.getString(ID));
            }

            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString(CLASS_ID));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(STUDENT_ID) && !jsonObject.getString(STUDENT_ID).isEmpty() && !jsonObject.getString(STUDENT_ID).equalsIgnoreCase("null")) {
                casteObj.setStudent_id(jsonObject.getString(STUDENT_ID));
            }
            if (jsonObject.has(STUDENT_NAME) && !jsonObject.getString(STUDENT_NAME).isEmpty() && !jsonObject.getString(STUDENT_NAME).equalsIgnoreCase("null")) {
                casteObj.setStudent_name(jsonObject.getString(STUDENT_NAME));
            } if (jsonObject.has(FATHER_NAME) && !jsonObject.getString(FATHER_NAME).isEmpty() && !jsonObject.getString(FATHER_NAME).equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString(FATHER_NAME));
            }
            if (jsonObject.has(GENDER) && !jsonObject.getString(GENDER).isEmpty() && !jsonObject.getString(GENDER).equalsIgnoreCase("null")) {
                casteObj.setGender(jsonObject.getString(GENDER));
            }
            if (jsonObject.has(MOBILENO) && !jsonObject.getString(MOBILENO).isEmpty() && !jsonObject.getString(MOBILENO).equalsIgnoreCase("null")) {
                casteObj.setMobileno(jsonObject.getString(MOBILENO));
            } if (jsonObject.has(EVENT_ID) && !jsonObject.getString(EVENT_ID).isEmpty() && !jsonObject.getString(EVENT_ID).equalsIgnoreCase("null")) {
                casteObj.setEvent_id(jsonObject.getString(EVENT_ID));
            }
            if (jsonObject.has(EVENTDATE) && !jsonObject.getString(EVENTDATE).isEmpty() && !jsonObject.getString(EVENTDATE).equalsIgnoreCase("null")) {
                casteObj.setEvent_date(jsonObject.getString(EVENTDATE));
            }
            if (jsonObject.has(EVENTNAME) && !jsonObject.getString(EVENTNAME).isEmpty() && !jsonObject.getString(EVENTNAME).equalsIgnoreCase("null")) {
                casteObj.setEvent_name(jsonObject.getString(EVENTNAME));
            }
            if (jsonObject.has(PARTICIPATION_DATE) && !jsonObject.getString(PARTICIPATION_DATE).isEmpty() && !jsonObject.getString(PARTICIPATION_DATE).equalsIgnoreCase("null")) {
                casteObj.setParticipationdate(jsonObject.getString(PARTICIPATION_DATE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
