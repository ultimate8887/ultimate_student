package com.ultimate.ultimatesmartstudent.Datesheet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DatesheetAdapter extends RecyclerView.Adapter<DatesheetAdapter.Viewholder> {
    private final Mycallback mAdaptercall;
    ArrayList<DatesheetBean> hq_list;
    Context listner;
    Animation animation;

    public DatesheetAdapter(ArrayList<DatesheetBean> hq_list, Context listener, Mycallback mAdaptercall) {
        this.hq_list = hq_list;
        this.listner = listener;
        this.mAdaptercall = mAdaptercall;

    }

    @Override
    public DatesheetAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.datesheet_adpt_lyt, parent, false);
        Viewholder viewholder = new DatesheetAdapter.Viewholder(view);
        return viewholder;

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public void onBindViewHolder(final DatesheetAdapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        String title1 = getColoredSpanned("<b>Class:- </b>", "#000000");
        String Name1 = "";
        Name1 = getColoredSpanned(hq_list.get(position).getEs_class(), "#5A5C59");
        holder.homeTopic.setText(Html.fromHtml(title1 + " " + Name1));

        if (hq_list.get(position).getEs_id() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("ds-" + hq_list.get(position).getEs_id(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        holder.classess.setText(hq_list.get(position).getEs_title());

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1 = dateFormat.format(cal.getTime());

        String check = Utils.getDateFormated(hq_list.get(position).getEs_date());
        if (check.equalsIgnoreCase(dateString)) {
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        } else if (check.equalsIgnoreCase(dateString1)) {

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        } else {
            holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getEs_date()));
        }

        //  holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getEs_date()));

        holder.nofile.setVisibility(View.VISIBLE);
        holder.audio_file.setVisibility(View.GONE);
        holder.pdf_file.setVisibility(View.GONE);

        holder.empty_file.setVisibility(View.GONE);
        holder.image_file.setVisibility(View.VISIBLE);
        Picasso.get().load(hq_list.get(position).getEs_image().get(0)).placeholder(listner.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
        holder.image_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdaptercall != null) {
                    mAdaptercall.onMethodCallback(hq_list.get(position));
                }
            }
        });

    }


    public interface Mycallback {
        public void onMethodCallback(DatesheetBean homeworkbean);
    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<DatesheetBean> hq_list) {
        this.hq_list = hq_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView u_date, homeTopic, sub;
        public ImageView image_file, audio_file, pdf_file;
        public RelativeLayout withfile;
        @BindView(R.id.nofile)
        RelativeLayout nofile;
        @BindView(R.id.empty_file)
        ImageView empty_file;
        @BindView(R.id.classess)
        TextView classess;
        @BindView(R.id.id)
        TextView id;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);
            sub.setVisibility(View.GONE);
        }
    }
}
