package com.ultimate.ultimatesmartstudent.Datesheet;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DatesheetBean {

    /**
     * es_id : 1
     * es_title : t
     * es_image : office_admin/images/datesheet_11262018_101109t.jpg
     * es_date : 2018-11-26
     * es_class : 0
     */

    private String es_id;
    private String es_title;

    public ArrayList<String> getEs_image() {
        return es_image;
    }

    public void setEs_image(ArrayList<String> es_image) {
        this.es_image = es_image;
    }

    private ArrayList<String> es_image;

    private String es_date;
    private String es_class;

    public String getEs_id() {
        return es_id;
    }

    public void setEs_id(String es_id) {
        this.es_id = es_id;
    }

    public String getEs_title() {
        return es_title;
    }

    public void setEs_title(String es_title) {
        this.es_title = es_title;
    }


    public String getEs_date() {
        return es_date;
    }

    public void setEs_date(String es_date) {
        this.es_date = es_date;
    }

    public String getEs_class() {
        return es_class;
    }

    public void setEs_class(String es_class) {
        this.es_class = es_class;
    }

    public static ArrayList<DatesheetBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<DatesheetBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                DatesheetBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static DatesheetBean parseClassObject(JSONObject jsonObject) {
        DatesheetBean casteObj = new DatesheetBean();
        try {
            if (jsonObject.has("es_id")) {
                casteObj.setEs_id(jsonObject.getString("es_id"));
            }
            if (jsonObject.has("es_title") && !jsonObject.getString("es_title").isEmpty() && !jsonObject.getString("es_title").equalsIgnoreCase("null")) {
                casteObj.setEs_title(jsonObject.getString("es_title"));
            }
            if (jsonObject.has("es_date") && !jsonObject.getString("es_date").isEmpty() && !jsonObject.getString("es_date").equalsIgnoreCase("null")) {
                casteObj.setEs_date(jsonObject.getString("es_date"));
            }
            if (jsonObject.has("es_class") && !jsonObject.getString("es_class").isEmpty() && !jsonObject.getString("es_class").equalsIgnoreCase("null")) {
                casteObj.setEs_class(jsonObject.getString("es_class"));
            }

            if (jsonObject.has("es_image") && jsonObject.get("es_image") instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray("es_image");
                ArrayList<String> img_arrs = new ArrayList<>();
                for (int i = 0; i < img_arr.length(); i++) {
                    img_arrs.add(Constants.getImageBaseURL() + img_arr.getString(i));
                }
                casteObj.setEs_image(img_arrs);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
