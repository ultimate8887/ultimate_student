package com.ultimate.ultimatesmartstudent.Leave_Mod;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class LeaveActivity extends AppCompatActivity {

    @BindView(R.id.leaveradio)
    RadioGroup radioGroup;
    @BindView(R.id.radioone)
    RadioButton radioone;
    @BindView(R.id.radiomulti)
    RadioButton radiomulti;
    @BindView(R.id.todate)
    TextView todate;
    @BindView(R.id.date)TextView fromdate;
    @BindView(R.id.applyleave)
    Button applyleave;
    @BindView(R.id.edtAddreason)
    EditText edittextreason;
    @BindView(R.id.parentsss)
    RelativeLayout parentss;
    public String to_date;
    public String from_date;
    private String s_id;
    String reason;
    int abcd;
    CommonProgress commonProgress;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.textView11)
    TextView textView11;

    @BindView(R.id.multi)
    RelativeLayout lyt2;
    @BindView(R.id.text1)
    TextView text1;
    @BindView(R.id.text2)
    TextView text2;

    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;

    @BindView(R.id.text_s)
    TextView text_s;
    @BindView(R.id.text1_s)
    TextView text1_s;
    @BindView(R.id.text2_s)
    TextView text2_s;
    SharedPreferences sharedPreferences;
    private static final int REQUEST_CODE_SPEECH_INPUT = 1;
    @BindView(R.id.cal_img1)
    ImageView cal_img1;
    @BindView(R.id.cal_img2)
    ImageView cal_img2;

    @BindView(R.id.hDay)
    RelativeLayout hDay;
    @BindView(R.id.oneDay)
    RelativeLayout oneDay;
    @BindView(R.id.multiDay)
    RelativeLayout multiDay;
    @BindView(R.id.speak_text)
    ImageView speak_text;
    String apply="full";
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText(getString(R.string.apply));
        if (!restorePrefData()){
            setShowcaseView();
        }
        radioone.setChecked(true);
        s_id= User.getCurrentUser().getId();
        abcd=radioGroup.getCheckedRadioButtonId();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioone:
                        todate.setVisibility(View.GONE);
                        lyt2.setVisibility(View.GONE);
                        // img1.setVisibility(View.GONE);
                        textView11.setText(getString(R.string.leavedate));
                        edittextreason.setText(" ");
                        //to_date="00-00-00";
                        break;
                    case R.id.radiomulti:
                        todate.setVisibility(View.VISIBLE);
                        lyt2.setVisibility(View.VISIBLE);
                        textView11.setText(getString(R.string.fromdate));
                        edittextreason.setText(" ");
                        break;
                }
            }
        });

        speak_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent
                        = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                        Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak to text");

                try {
                    startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT);
                }
                catch (Exception e) {
                    Toast
                            .makeText(LeaveActivity.this, " " + e.getMessage(),
                                    Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });


    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("main_view_new_text_speech",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("main_view_new_text_speech",true);
        editor.apply();
        // dataPasser.onTargetViewPass("save");
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("main_view_new_text_speech",MODE_PRIVATE);
        return sharedPreferences.getBoolean("main_view_new_text_speech",false);
    }


    private void setShowcaseView() {


        new GuideView.Builder(this)
                .setTitle("Speech to text")
                .setContentText("Tap the Speech to \ntext Icon")
                .setTargetView(speak_text)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(DismissType.outside) //optional - default dismissible by TargetView
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        savePrefData();
                    }
                })
                .build()
                .show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(
                        RecognizerIntent.EXTRA_RESULTS);
                edittextreason.setText(
                        Objects.requireNonNull(result).get(0));
            }
        }
    }

    @OnClick(R.id.hDay)
    public void hDay() {
        apply="half";
        radioone.setChecked(true);
        todate.setVisibility(View.GONE);
        lyt2.setVisibility(View.GONE);
        img1.setVisibility(View.GONE);
        img.setVisibility(View.VISIBLE);
        text1_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.GONE);
        text_s.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        textView11.setText(getString(R.string.leavedate));
        edittextreason.setText(" ");
        oneDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#00000000"));
        hDay.setBackgroundColor(Color.parseColor("#66000000"));
    }


    @OnClick(R.id.oneDay)
    public void oneDay() {
        apply="full";
        radioone.setChecked(true);
        todate.setVisibility(View.GONE);
        lyt2.setVisibility(View.GONE);
        img.setVisibility(View.GONE);
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        text_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.GONE);
        text1_s.setVisibility(View.VISIBLE);
        textView11.setText(getString(R.string.leavedate));
        edittextreason.setText(" ");
        hDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#00000000"));
        oneDay.setBackgroundColor(Color.parseColor("#66000000"));
    }

    @OnClick(R.id.multiDay)
    public void multiDay() {
        apply="multi";
        radiomulti.setChecked(true);
        todate.setVisibility(View.VISIBLE);
        lyt2.setVisibility(View.VISIBLE);
        img.setVisibility(View.GONE);
        img2.setVisibility(View.VISIBLE);
        img1.setVisibility(View.GONE);
        text_s.setVisibility(View.GONE);
        text1_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.VISIBLE);
        textView11.setText(getString(R.string.fromdate));
        edittextreason.setText(" ");
        hDay.setBackgroundColor(Color.parseColor("#00000000"));
        oneDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#66000000"));
    }

    @OnClick(R.id.imgBackmsg)
    public void onback() {
        imgBackmsg.startAnimation(animation);
        finish();
    }
    @OnClick(R.id.one)
    public void dateset(){
        cal_img1.startAnimation(animation);
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            fromdate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
        }
    };


    @OnClick(R.id.multi)
    public void todateset(){
        cal_img2.startAnimation(animation);
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, datefrom,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener datefrom = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            todate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            to_date = dateFrmOut.format(setdate);
        }
    };


    @OnClick(R.id.applyleave)
    public void applyleave(){


//        if(!todate.getText().toString().equalsIgnoreCase(""))
//        {
//            if (checkValidsec()) {
//                //Toast.makeText(getApplicationContext(), "Apply sucess1", Toast.LENGTH_LONG).show();
//                commonProgress.show();
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("s_id", s_id);
//                params.put("msg",edittextreason.getText().toString());
//                params.put("from_date", from_date);
//                params.put("to_date",to_date);
//                ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.APPLYLEAVE, apiCallback, this, params);
//            }
//        } else{
//            if (checkValid()) {
//                commonProgress.show();
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("s_id", s_id);
//                params.put("msg", edittextreason.getText().toString());
//                params.put("from_date", from_date);
//                //params.put("to_date",to_date);
//                ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.APPLYLEAVE, apiCallback, this, params);
//            }
//        }


        if(radioone.isChecked()) {
            if (checkValid()) {
                commonProgress.show();
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("s_id", s_id);
                params.put("msg", edittextreason.getText().toString());
                params.put("from_date", from_date);
                params.put("apply",apply);
                ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.APPLYLEAVE, apiCallback, this, params);
            }

        }
        else
        if(radiomulti.isChecked())
        {
            if (checkValidsec()) {
                //Toast.makeText(getApplicationContext(), "Apply sucess1", Toast.LENGTH_LONG).show();
                commonProgress.show();
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("s_id", s_id);
                params.put("msg",edittextreason.getText().toString());
                params.put("from_date", from_date);
                params.put("to_date",to_date);
                ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.APPLYLEAVE, apiCallback, this, params);
            }
        }
    }
    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;

        if (from_date == null) {
            valid = false;
            errorMsg = getString(R.string.e_fromdate);
        }
        else if (edittextreason.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = getString(R.string.leave_reason);
        }

        if (!valid) {
            //   Utils.showSnackBar(errorMsg, parentss);
            showToast(errorMsg);
        }
        return valid;
    }

    private void showToast(String errorMsg) {
        Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_LONG).show();
    }

    private boolean checkValidsec() {
        boolean valid = true;
        String errorMsg = null;
//        if (radioGroup.getCheckedRadioButtonId()==-1) {
//            valid = false;
//            errorMsg = "Please select day!";
//        }else
        if (to_date == null) {
            valid = false;
            errorMsg = getString(R.string.e_todate);
        }else if (from_date == null) {
            valid = false;
            errorMsg = getString(R.string.e_fromdate);
        }
        else if (edittextreason.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = getString(R.string.leave_reason);
        }

        if (!valid) {
            //  Utils.showSnackBar(errorMsg, parentss);
            showToast(errorMsg);
        }
        return valid;
    }


    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();

            if (error == null) {

                try {

                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    //  Utils.showSnackBar(jsonObject.getString("msg"), parentss);
                    //  Log.e("checkerroer", jsonObject.getString("msg") + "");
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                //  Utils.showSnackBar(error.getMessage(), parentss);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(LeaveActivity.this, LoginActivity.class));
                    finish();
                }

            }

        }

    };


}