package com.ultimate.ultimatesmartstudent.Leave_Mod;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

public class OptionAdapter  extends BaseAdapter {
    LayoutInflater inflter;
    Context context;
    private ArrayList<OptionBean> vehicleList;

    public OptionAdapter(Context context, ArrayList<OptionBean> vehicleList) {
        this.context = context;
        this.vehicleList = vehicleList;
        inflter = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return vehicleList.size()+ 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("All");
        } else {
            OptionBean classObj = vehicleList.get(i - 1);
            label.setText(classObj.getName());
        }
        return view;
    }
}