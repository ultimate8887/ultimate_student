package com.ultimate.ultimatesmartstudent.Leave_Mod;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Leavelistbean {

    private static String ID="id";
    private static String STUDENTID="s_id";
    private static String MESSAGE="msg";
    private static String FROMDATE="from_date";
    private static String TODATE="to_date";
    private static String NAME="name";
    private static String APPROVE_STATUS="approve_status";

    /**
     * id : 2
     * s_id : 15
     * msg :  hhello
     * from_date : 2018-07-16
     * to_date : 2018-07-16
     * status : active
     */

    private String id;
    private String s_id;
    private String msg;
    private String from_date;
    private String to_date;
    private String status;

    public String getL_type() {
        return l_type;
    }

    public void setL_type(String l_type) {
        this.l_type = l_type;
    }

    private String l_type;
    /**
     * name : fname flname
     */

    private String name;
    /**
     * approve_status : decline
     */

    private String approve_status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public static ArrayList<Leavelistbean> parseleavelistArray(JSONArray jsonArray) {
        ArrayList<Leavelistbean> list = new ArrayList<Leavelistbean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Leavelistbean p = parseleavelistObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static Leavelistbean parseleavelistObject(JSONObject jsonObject) {
        Leavelistbean msgObj = new Leavelistbean();
        try {
            if (jsonObject.has(ID)) {
                msgObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(FROMDATE) && !jsonObject.getString(FROMDATE).isEmpty() && !jsonObject.getString(FROMDATE).equalsIgnoreCase("null")) {
                msgObj.setFrom_date(jsonObject.getString(FROMDATE));
            }
            if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                msgObj.setMsg(jsonObject.getString(MESSAGE));
            }
            if (jsonObject.has(TODATE) && !jsonObject.getString(TODATE).isEmpty() && !jsonObject.getString(TODATE).equalsIgnoreCase("null")) {
                msgObj.setTo_date(jsonObject.getString(TODATE));
            }

            if (jsonObject.has("l_type") && !jsonObject.getString("l_type").isEmpty() && !jsonObject.getString("l_type").equalsIgnoreCase("null")) {
                msgObj.setL_type(jsonObject.getString("l_type"));
            }

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                msgObj.setName(jsonObject.getString(NAME));
            }

            if (jsonObject.has(APPROVE_STATUS) && !jsonObject.getString(APPROVE_STATUS).isEmpty() && !jsonObject.getString(APPROVE_STATUS).equalsIgnoreCase("null")) {
                msgObj.setApprove_status(jsonObject.getString(APPROVE_STATUS));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApprove_status() {
        return approve_status;
    }

    public void setApprove_status(String approve_status) {
        this.approve_status = approve_status;
    }
}
