package com.ultimate.ultimatesmartstudent.Leave_Mod;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Leave_list_Adapter extends RecyclerView.Adapter<Leave_list_Adapter.Viewholder> {
    ArrayList<Leavelistbean> leavelist;
    Context context;

    public Leave_list_Adapter(ArrayList<Leavelistbean> leavelist, Context context) {
        this.context = context;
        this.leavelist = leavelist;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leavelst_adapt_lay_latest, parent, false);
        Leave_list_Adapter.Viewholder viewholder = new Leave_list_Adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        if(leavelist.get(position).getS_id()!=null) {
            holder.name.setText(User.getCurrentUser().getFirstname()+" "+User.getCurrentUser().getLastname() + "(" + User.getCurrentUser().getId() + ")");
        }else{
            holder.name.setText(User.getCurrentUser().getFirstname());
        }
        holder.reason.setText(leavelist.get(position).getMsg());

        holder.reason.setEnabled(false);
        if (leavelist.get(position).getId() != null) {
            String title = getColoredSpanned("Leave ID: ", "#000000");
            String Name = getColoredSpanned(leavelist.get(position).getId(), "#5A5C59");
            holder.txtRollNo.setText(Html.fromHtml(title + " " + Name));
        }

        //  holder.txtRollNo.setText("Reg no. :"+leavelist.get(position).getS_id());

        if(User.getCurrentUser().getSection_name()!=null) {
            holder.classname.setText(User.getCurrentUser().getClass_name()+"("+User.getCurrentUser().getSection_name()+")");
        }else{
            holder.classname.setText(User.getCurrentUser().getClass_name());
        }

        if(leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")){
            holder.aproveimage.setVisibility(View.VISIBLE);
            holder.reason.setTextColor(ContextCompat.getColor(context, R.color.present));
            holder.approve.setVisibility(View.INVISIBLE);
            holder.unapprove.setVisibility(View.INVISIBLE);
            holder.wait.setVisibility(View.INVISIBLE);

        }else{
            holder.aproveimage.setVisibility(View.GONE);
            holder.approve.setVisibility(View.INVISIBLE);
            holder.unapprove.setVisibility(View.INVISIBLE);
            //  holder.wait.setVisibility(View.VISIBLE);
        }
        if(leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")){
            holder.rejecteed.setVisibility(View.VISIBLE);
            holder.reason.setTextColor(ContextCompat.getColor(context, R.color.orange));
            holder.approve.setVisibility(View.INVISIBLE);
            holder.unapprove.setVisibility(View.INVISIBLE);
            holder.wait.setVisibility(View.INVISIBLE);
        }else {
            holder.rejecteed.setVisibility(View.GONE);
            holder.approve.setVisibility(View.INVISIBLE);
            holder.unapprove.setVisibility(View.INVISIBLE);
            // holder.wait.setVisibility(View.VISIBLE);
        }

        if(leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")){
            holder.approve.setVisibility(View.INVISIBLE);
            holder.unapprove.setVisibility(View.INVISIBLE);
            holder.wait.setVisibility(View.VISIBLE);
        }else{
            holder.approve.setVisibility(View.INVISIBLE);
            holder.unapprove.setVisibility(View.INVISIBLE);
        }


        if(leavelist.get(position).getTo_date().equalsIgnoreCase("0000-00-00")){

            String title = getColoredSpanned("", "#ff0099cc");
            String Name="";
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date()), "#1C8B3B");

            }
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date()), "#F4212C");

            }
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date()), "#000000");
            }

            holder.date.setText(Html.fromHtml(title + " " + Name));

            if (leavelist.get(position).getL_type()!=null) {
                holder.apply.setText("Half day");
            }else {
                holder.apply.setText("Full day");
            }

            //  holder.date.setText(Utility.getDateFormated(leavelist.get(position).getFrom_date()));

        }else {
            String title = getColoredSpanned("", "#ff0099cc");
            String Name="";
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date())+ " " + "to" + " " + Utils.getDateFormated(leavelist.get(position).getTo_date()), "#1C8B3B");
            }
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date())+ " " + "to" + " " + Utils.getDateFormated(leavelist.get(position).getTo_date()), "#F4212C");
            }
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date())+ " " + "to" + " " + Utils.getDateFormated(leavelist.get(position).getTo_date()), "#000000");
            }

            holder.date.setText(Html.fromHtml(title + " " + Name));

            holder.apply.setText("Multiple day");

            //  holder.date.setText(Utility.getDateFormated(leavelist.get(position).getFrom_date()) + " " + "to" + " " + Utility.getDateFormated(leavelist.get(position).getTo_date()));
        }


//        if (leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")) {
//            holder.approve.setVisibility(View.VISIBLE);
//            holder.unapprove.setVisibility(View.GONE);
//            holder.waitapprove.setVisibility(View.GONE);
//        }
//        if (leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")) {
//            holder.unapprove.setVisibility(View.VISIBLE);
//            holder.approve.setVisibility(View.GONE);
//            holder.waitapprove.setVisibility(View.GONE);
//        }
//        if (leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")) {
//            holder.waitapprove.setVisibility(View.VISIBLE);
//            holder.approve.setVisibility(View.GONE);
//            holder.unapprove.setVisibility(View.GONE);
//        }
//
//        if (leavelist.get(position).getId() != null) {
//            String title = getColoredSpanned("Leave ID: ", "#ff0099cc");
//            String Name = getColoredSpanned(leavelist.get(position).getId(), "#7D7D7D");
//            holder.leaveid.setText(Html.fromHtml(title + " " + Name));
//        }
//
//        if (leavelist.get(position).getL_type() != null) {
//            String title = getColoredSpanned("Apply For: ", "#ff0099cc");
//            String Name = getColoredSpanned(leavelist.get(position).getL_type(), "#7D7D7D");
//            holder.name.setText(Html.fromHtml(title + " " + Name));
//        }
//
//
//        if (leavelist.get(position).getMsg() != null) {
//            String title = getColoredSpanned("Reason: ", "#ff0099cc");
//            String Name = getColoredSpanned(leavelist.get(position).getMsg(), "#7D7D7D");
//            holder.reason.setText(Html.fromHtml(title + " " + Name));
//        }
//
//        if (leavelist.get(position).getTo_date().equalsIgnoreCase("0000-00-00")) {
//            String title = getColoredSpanned("Leave Date: ", "#ff0099cc");
//            String Name = "";
//            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")) {
//                Name = getColoredSpanned(Utils.dateFormat(leavelist.get(position).getFrom_date()), "#1C8B3B");
//
//            }
//            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")) {
//                Name = getColoredSpanned(Utils.dateFormat(leavelist.get(position).getFrom_date()), "#F4212C");
//
//            }
//            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")) {
//                Name = getColoredSpanned(Utils.dateFormat(leavelist.get(position).getFrom_date()), "#673AB7");
//            }
//
//            holder.date.setText(Html.fromHtml(title + " " + Name));
//
//            if (leavelist.get(position).getL_type() != null) {
//                String title1 = getColoredSpanned("Apply for:- ", "#000000");
//                String Name1 = getColoredSpanned("<b>" + "Half day" + "</b>", "#5A5C59");
//                holder.u_date.setText(Html.fromHtml(title1 + " " + Name1));
//            } else {
//                String title1 = getColoredSpanned("Apply for:- ", "#000000");
//                String Name1 = getColoredSpanned("<b>" + "Full day" + "</b>", "#5A5C59");
//                holder.u_date.setText(Html.fromHtml(title1 + " " + Name1));
//            }
//
//        } else {
//            String title = getColoredSpanned("Leave Date: ", "#ff0099cc");
//            String Name = "";
//            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")) {
//                Name = getColoredSpanned(Utils.dateFormat(leavelist.get(position).getFrom_date()) + " " + "to" + " " + Utils.dateFormat(leavelist.get(position).getTo_date()), "#1C8B3B");
//            }
//            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")) {
//                Name = getColoredSpanned(Utils.dateFormat(leavelist.get(position).getFrom_date()) + " " + "to" + " " + Utils.dateFormat(leavelist.get(position).getTo_date()), "#F4212C");
//            }
//            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")) {
//                Name = getColoredSpanned(Utils.dateFormat(leavelist.get(position).getFrom_date()) + " " + "to" + " " + Utils.dateFormat(leavelist.get(position).getTo_date()), "#673AB7");
//            }
//
//            holder.date.setText(Html.fromHtml(title + " " + Name));
//
//            String title1 = getColoredSpanned("Apply for:- ", "#000000");
//            String Name1 = getColoredSpanned("<b>" + "Multiple day" + "</b>", "#5A5C59");
//            holder.u_date.setText(Html.fromHtml(title1 + " " + Name1));
//
//        }

        // holder.leaveid.setText("LeaveID:"+leavelist.get(position).getId());
        //   holder.name.setText("Name:"+leavelist.get(position).getName());
        //  holder.reason.setText("Reason:"+leavelist.get(position).getMsg());
//        if(leavelist.get(position).getTo_date().equalsIgnoreCase("0000-00-00")){
//        holder.date.setText("LeaveDate:"+leavelist.get(position).getFrom_date());
//
//        }else {
//        holder.date.setText("LeaveDate:" + leavelist.get(position).getFrom_date() + " " + "to" + " " + leavelist.get(position).getTo_date());
//        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return leavelist.size();
    }

    public void setleavelstList(ArrayList<Leavelistbean> leavelist) {
        this.leavelist = leavelist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName)
        TextView name;
        @BindView(R.id.apply)
        TextView apply;
        @BindView(R.id.txtDate)TextView date;
        @BindView(R.id.txtReason)
        EditText reason;
        @BindView(R.id.approve)
        ImageView approve;
        @BindView(R.id.pendingimg)
        ImageView pendingimg;
        @BindView(R.id.visitimage)
        CircularImageView visitimage;
        @BindView(R.id.unaprove)ImageView unapprove;
        @BindView(R.id.txtRollNo)TextView txtRollNo;
        @BindView(R.id.aprvimg)ImageView aproveimage;
        @BindView(R.id.rejetedimg)ImageView rejecteed;
        @BindView(R.id.txtClass)TextView classname;
        @BindView(R.id.wait)
        LinearLayout wait;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
