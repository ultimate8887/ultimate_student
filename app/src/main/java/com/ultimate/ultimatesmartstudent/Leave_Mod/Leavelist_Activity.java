package com.ultimate.ultimatesmartstudent.Leave_Mod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Leavelist_Activity extends AppCompatActivity {
    @BindView(R.id.textView8)
    TextView textsubtitle;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;
//    @BindView(R.id.parent)
//    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    Leave_list_Adapter adapter;
    ArrayList<Leavelistbean> leavelist = new ArrayList<>();
    int loaded = 0;
    String id;
    CommonProgress commonProgress;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Animation animation;
    ArrayList<OptionBean> vehicleList = new ArrayList<>();
    @BindView(R.id.spinnerGroups)
    Spinner vehicleType;
    String tag="";
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.leave)+" "+getString(R.string.list));
        textsubtitle.setText(getString(R.string.f_leave));
        id = User.getCurrentUser().getId();
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new Leave_list_Adapter(leavelist, this);
        recyclerView.setAdapter(adapter);


        //vehicleList.add(new OptionBean("All"));
        vehicleList.add(new OptionBean("Pending"));
        vehicleList.add(new OptionBean("Approved"));
        vehicleList.add(new OptionBean("Decline"));

        OptionAdapter dataAdapter = new OptionAdapter(Leavelist_Activity.this, vehicleList);
        vehicleType.setAdapter(dataAdapter);
        //attach the listener to the spinner
        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    String tag1 = vehicleList.get(i - 1).getName();
                    if (tag1.equalsIgnoreCase("Pending")) {
                        tag = "unapproved";
                    }else {
                        tag = vehicleList.get(i - 1).getName();
                    }
                }else {
                    tag="";
                }
                fetchleavelist(tag);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @OnClick(R.id.imgBackmsg)
    public void onback() {
      imgBackmsg.startAnimation(animation);
        finish();
    }

    private void fetchleavelist(String tag) {
//        if (loaded == 0) {
            commonProgress.show();
       // }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
        params.put("id",User.getCurrentUser().getId());
        params.put("tag",tag);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.LEAVELIST, apicallback, this, params);



//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.LEAVELIST + Constants.LEAVELISTID + id + Constants.TAGGG + tag, apicallback, this, params);
    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();

            if (error == null) {
                try {
                    if (leavelist != null) {
                        leavelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("leave_data");
                    leavelist = Leavelistbean.parseleavelistArray(jsonArray);
                    if (leavelist.size() > 0) {
                        adapter.setleavelstList(leavelist);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(leavelist.size()));
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        txtNorecord.setVisibility(View.VISIBLE);
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        adapter.setleavelstList(leavelist);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                leavelist.clear();
                adapter.setleavelstList(leavelist);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
}