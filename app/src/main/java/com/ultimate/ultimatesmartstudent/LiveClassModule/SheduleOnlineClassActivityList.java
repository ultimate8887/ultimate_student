package com.ultimate.ultimatesmartstudent.LiveClassModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.TestMod.GDSTest;
import com.ultimate.ultimatesmartstudent.TestMod.TestList;
import com.ultimate.ultimatesmartstudent.TestMod.TestListBean;
import com.ultimate.ultimatesmartstudent.TestMod.TestListRecyclerAdapter;
import com.ultimate.ultimatesmartstudent.TestMod.TestSubjectListAdapter;
import com.ultimate.ultimatesmartstudent.TestMod.Testresultbean;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SheduleOnlineClassActivityList extends AppCompatActivity implements ScheduleTestAdapter.Adaptercall {

    @BindView(R.id.textView8)
    TextView textsubtitle;
    @BindView(R.id.spinnerGroups)
    Spinner testlist;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    ArrayList<TestList> subjectlist = new ArrayList<>();
    ArrayList<TestListBean> TestListBeanList = new ArrayList<>();
    ArrayList<Testresultbean> testresult = new ArrayList<>();
    RecyclerView recyclerView;
    ImageView back;
    ScheduleTestAdapter testadapter;
    String sub_id="";
    String sub="";
    String folder_main = "Test";
    String test_id;
    String marks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    Animation animation;
    String textholder="",school="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText(getString(R.string.liveclass));
        commonProgress=new CommonProgress(this);
        school= User.getCurrentUser().getSchoolData().getName();
        textsubtitle.setText(getString(R.string.f_subject));
        // testlist = (Spinner) findViewById(R.id.spinner);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView11);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerView.setLayoutManager(new LinearLayoutManager(SheduleOnlineClassActivityList.this));
        testadapter = new ScheduleTestAdapter(SheduleOnlineClassActivityList.this, TestListBeanList, SheduleOnlineClassActivityList.this);
        recyclerView.setAdapter(testadapter);
        back = (ImageView) findViewById(R.id.imgBackmsg);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        fetchsubjectlist();
    }

    private void fetchsubjectlist() {
        // UltimateProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, testapiCallback, this, params);
    }

    ApiHandler.ApiCallback testapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("sub_data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        TestList dm = new TestList();
                        dm.setName(jsonObject1.getString("name"));
                        dm.setId(jsonObject1.getString("id"));
                        subjectlist.add(dm);

                    }
                    TestSubjectListAdapter testlisttadapter = new TestSubjectListAdapter(SheduleOnlineClassActivityList.this, subjectlist);
                    testlist.setAdapter(testlisttadapter);
                    testlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_id = subjectlist.get(i - 1).getId();
                                //  TestListBeanList.clear();
                            }
                            fetchTest(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(SheduleOnlineClassActivityList.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchTest(int i) {
        commonProgress.show();
        //  Toast.makeText(getApplicationContext(),User.getCurrentUser().getSection_id(),Toast.LENGTH_SHORT).show();
        HashMap<String, String> params = new HashMap<String, String>();
        if (i == 0){
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("section_id", User.getCurrentUser().getSection_id());
        } else {
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("sub_id", sub_id);
            params.put("section_id", User.getCurrentUser().getSection_id());
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ScheduleClassListgds, test1apiCallback, this, params);
    }

    ApiHandler.ApiCallback test1apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (TestListBeanList != null) {
                        TestListBeanList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("test_data");
                    TestListBeanList = TestListBean.parsetestlistArray(jsonArray);
                    if (TestListBeanList.size() > 0) {
                        testadapter.settestList(TestListBeanList);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText(getString(R.string.t_entries)+ " "+String.valueOf(TestListBeanList.size()));
                    } else {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        testadapter.settestList(TestListBeanList);
                        testadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                TestListBeanList.clear();
                testadapter.settestList(TestListBeanList);
                testadapter.notifyDataSetChanged();
                txtNorecord.setVisibility(View.VISIBLE);

            }
        }
    };


    @Override
    public void methodcall(TestListBean obj) {
       String str = obj.getUrl();
        String msg = "";
        String regex
                = "\\b((?:https?|ftp|file):"
                + "//[-a-zA-Z0-9+&@#/%?="
                + "~_|!:, .;]*[-a-zA-Z0-9+"
                + "&@#/%=~_|])";

        // Compile the Regular Expression
        Pattern p = Pattern.compile(
                regex,
                Pattern.CASE_INSENSITIVE);
        // Find the match between string
        // and the regular expression
        Matcher m = p.matcher(str);
        // Find the next subsequence of
        // the input subsequence that
        // find the pattern
        while (m.find()) {
            // Find the substring from the
            // first index of match result
            // to the last index of match
            // result and add in the list
          msg= str.substring(
                    m.start(0), m.end(0));
        }
        if (!msg.equalsIgnoreCase("")) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(msg)));
          //  Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
            }else{
                 Toast.makeText(getApplicationContext(),"Something went wrong!",Toast.LENGTH_SHORT).show();
            }

    }
}