package com.ultimate.ultimatesmartstudent.LiveClassModule;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;


import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.TestMod.TestListBean;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleTestAdapter extends RecyclerView.Adapter<ScheduleTestAdapter.ViewHolder> {
    Context context;
    private Adaptercall mAdaptercall;
    private ArrayList<TestListBean> MainImageUploadInfoList;

    public ScheduleTestAdapter(Context context, ArrayList<TestListBean> MainImageUploadInfoList, Adaptercall mAdaptercall) {

        this.MainImageUploadInfoList = MainImageUploadInfoList;
        this.context = context;
        this.mAdaptercall=mAdaptercall;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_list_lyt, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        holder.upldreslttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdaptercall.methodcall(MainImageUploadInfoList.get(position));
            }
        });

        if (MainImageUploadInfoList.get(position).getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("oc-"+MainImageUploadInfoList.get(position).getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        holder.Topic.setText(MainImageUploadInfoList.get(position).getTopic());



        if (MainImageUploadInfoList.get(position).getDur() != null) {
            String title = getColoredSpanned("<b>"+"Timing: "+"</b>", "#000000");
            String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getDur()+"", "#000000");
            holder.Dur.setText(Html.fromHtml(title + " " + Name));
        }


        //for Today
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());
        //for Tommowow
        DateFormat dateFormat1 = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, +1);
        String dateString2= dateFormat1.format(cal1.getTime());

        String check= Utils.getDateFormated(MainImageUploadInfoList.get(position).getTest_date());

        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("<b>"+"Class Date: "+"</b>", "#000000");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("<b>"+"Today", "#e31e25");
            holder.maxMarks.setText(Html.fromHtml(title + " " + l_Name));
            holder.upldreslttext.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){
            holder.upldreslttext.setVisibility(View.GONE);
            String title = getColoredSpanned("<b>"+"Class Date: "+"</b>", "#000000");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Yesterday", "#F9A602");
            holder.maxMarks.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else if (check.equalsIgnoreCase(dateString2)){
            holder.upldreslttext.setVisibility(View.GONE);
            String title = getColoredSpanned("<b>"+"Class Date: "+"</b>", "#000000");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Tomorrow", "#1C8B3B");
            holder.maxMarks.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.upldreslttext.setVisibility(View.GONE);
            String title = getColoredSpanned("<b>"+"Class Date: "+"</b>", "#000000");
            String Name = getColoredSpanned(Utils.getDateFormated(MainImageUploadInfoList.get(position).getTest_date()), "#000000");
            holder.maxMarks.setText(Html.fromHtml(title + " " + Name));
        }


        if (MainImageUploadInfoList.get(position).getSub_name() != null){
            holder.sub.setText(MainImageUploadInfoList.get(position).getSub_name());
        }else{
            holder.sub.setText("Not Mentioned");
        }




    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {

        return MainImageUploadInfoList.size();
    }



    public void settestList(ArrayList<TestListBean> MainImageUploadInfoList) {
        this.MainImageUploadInfoList = MainImageUploadInfoList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


        public TextView Topic,maxMarks,Dur,Date,upldreslttext,upldsuccmsg,fupldsuccmsg,viewresult, sub;
        public ImageView line;

        public ConstraintLayout withfile;
        CardView card1;
        TextView id;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            id = (TextView) itemView.findViewById(R.id.id);
            card1 = (CardView) itemView.findViewById(R.id.card1);
            Topic = (TextView) itemView.findViewById(R.id.TestTopic);
            maxMarks= (TextView) itemView.findViewById(R.id.txtMarks);
            Dur = (TextView) itemView.findViewById(R.id.TestDuration);
            Date = (TextView) itemView.findViewById(R.id.TestDate);
            line=(ImageView)itemView.findViewById(R.id.imageline);
            withfile=(ConstraintLayout)itemView.findViewById(R.id.withfile);
            upldreslttext=(TextView)itemView.findViewById(R.id.upldreslttext);
            viewresult=(TextView)itemView.findViewById(R.id.viewresult);

        }


    }
    public interface Adaptercall{
        public void methodcall(TestListBean obj);

    }
}
