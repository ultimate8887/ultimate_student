package com.ultimate.ultimatesmartstudent.Messages.BeanClasses;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Stafflist_bean {
    private static String ID="id";
    private static String NAME="name";
    /**
     * id : 3
     * name : Staff Gupta
     * dateofbirth : 1995-08-17
     * dateofjoining : 2018-03-10
     * salary : 15000
     */

    private String id;
    private String name;
    private String dateofbirth;
    private String dateofjoining;
    private String salary;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getDateofjoining() {
        return dateofjoining;
    }

    public void setDateofjoining(String dateofjoining) {
        this.dateofjoining = dateofjoining;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public static ArrayList<Stafflist_bean> parsestaffArray(JSONArray jsonArray) {
        ArrayList<Stafflist_bean> list=new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Stafflist_bean p = parsestaffObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static Stafflist_bean parsestaffObject(JSONObject jsonObject) {
        Stafflist_bean casteObj = new Stafflist_bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
