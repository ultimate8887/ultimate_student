package com.ultimate.ultimatesmartstudent.Messages.All_Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.Datesheet.Datesheet;
import com.ultimate.ultimatesmartstudent.Datesheet.DatesheetAdapter;
import com.ultimate.ultimatesmartstudent.Datesheet.DatesheetBean;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.Homework.HomeWorkByDate;
import com.ultimate.ultimatesmartstudent.Leave_Mod.OptionAdapter;
import com.ultimate.ultimatesmartstudent.Leave_Mod.OptionBean;
import com.ultimate.ultimatesmartstudent.Messages.AdapterClasses.Message_Adapter;
import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Message_Bean;
import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Sent_message_bean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class MessageActivity extends AppCompatActivity implements Message_Adapter.Mycallback {
    CommonProgress commonProgress;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerview;
    ImageView imageView6;
    EditText subject;
    EditText message;
    TextView sendername,txtdate;
    ArrayList<Message_Bean> messagelist = new ArrayList<>();
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    String folder_main = "Received Message";
    private Message_Adapter adapter;
    String id;
    String tag="";
    String type="1",name="",date="";
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    Animation animation;
    @BindView(R.id.spinnerGroups)
    Spinner spinnerMonth;
    ArrayList<OptionBean> vehicleList = new ArrayList<>();
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    String image_url="",school="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        id= User.getCurrentUser().getId();
        school=User.getCurrentUser().getSchoolData().getName();
        txtTitle.setText(getString(R.string.received));
        commonProgress=new CommonProgress(this);

        if (getIntent().getExtras()!=null){
            name = getIntent().getExtras().getString("Today");
        }

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerview.setLayoutManager(new LinearLayoutManager(MessageActivity.this));
        adapter = new Message_Adapter(messagelist, MessageActivity.this,this);
        recyclerview.setAdapter(adapter);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onResume() {
        super.onResume();
        fetnewmsg();
        fetchmonthList();
        getTodayDate();
    }


    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList() {

        vehicleList.add(new OptionBean("Today"));
        vehicleList.add(new OptionBean("History"));
        OptionAdapter dataAdapter = new OptionAdapter(MessageActivity.this, vehicleList);
        spinnerMonth.setAdapter(dataAdapter);
        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    tag = vehicleList.get(i - 1).getName();
                }else {
                    tag="";
                }
                fetchmessagelist(tag);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }




    private void fetnewmsg() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        params.put("check","");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG, apicallback5, this, params);
    }

    ApiHandler.ApiCallback apicallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();

            if (error == null) {

                try {
                    Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {

              //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    public void fetchmessagelist(String name){
        commonProgress.show();
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE+Constants.MSGID+id+Constants.MSGTYPE+type
                +Constants.NAME+name+Constants.DATE+date,apicallback,this,params);
    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (messagelist != null) {
                        messagelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("msg_data");
                    messagelist = Message_Bean.parseMessageArray(jsonArray);
                    if (messagelist.size() > 0) {
                        adapter.setmessageList(messagelist);
                        //setanimation on adapter...
                        recyclerview.getAdapter().notifyDataSetChanged();
                        recyclerview.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(messagelist.size()));
                    } else {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        adapter.setmessageList(messagelist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                messagelist.clear();
                txtNorecord.setVisibility(View.VISIBLE);
                adapter.setmessageList(messagelist);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }
    };

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    @Override
    public void onMethod_Image_callback(Message_Bean message_bean) {

        Dialog mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.view_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
         imageView6 = (ImageView) sheetView.findViewById(R.id.imageView6);
         subject = (EditText) sheetView.findViewById(R.id.txtsubject);
         message = (EditText) sheetView.findViewById(R.id.message_body);
         sendername = (TextView) sheetView.findViewById(R.id.txtsendername);
        txtdate = (TextView) sheetView.findViewById(R.id.txtdate);
        setData(message_bean);

        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 downloadfile(message_bean);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    private void setData(Message_Bean sentmsglist) {
        subject.setEnabled(false);
        message.setEnabled(false);
        subject.setText(sentmsglist.getSubject());


        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS")
                || User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("NMSNEW")){
            message.setText(Html.fromHtml(sentmsglist.getMessage()));
        }else{
            message.setText(sentmsglist.getMessage());
        }

     //   message.setText(Html.fromHtml(sentmsglist.getMessage()));



        txtdate.setText(Utils.getTimeHr(sentmsglist.getCreated_on()));
        if (sentmsglist.getFrom_name() != null) {
            String title = getColoredSpanned("From: ", "#e31e25");
            String Name = getColoredSpanned(sentmsglist.getFrom_name(), "#7D7D7D");
            String l_Name = getColoredSpanned("["+ sentmsglist.getFrom_type()+"]", "#5A5C59");
            sendername.setText(Html.fromHtml(title + " " + Name + " " + l_Name));
        }
        if (sentmsglist.getImage() != null) {
            imageView6.setVisibility(View.VISIBLE);
            Picasso.get().load(sentmsglist.getImage()).placeholder(this.getResources().getDrawable(R.drawable.logo)).into(imageView6);
           }  else {
            imageView6.setVisibility(View.GONE);
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void downloadfile(Message_Bean message_bean) {

        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imgShow = (ImageView) dialogLog.findViewById(R.id.img);

        Picasso.get().load(message_bean.getImage()).placeholder(getResources().getDrawable(R.drawable.logo)).into(imgShow);

        Button save = (Button) dialogLog.findViewById(R.id.submit);
        save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                save.startAnimation(animation);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);
            }
        });

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
    }

//        Dialog mBottomSheetDialog = new Dialog(this);
//        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
//        mBottomSheetDialog.setContentView(sheetView);
//        final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);
//        mBottomSheetDialog.setCancelable(true);
//
//        Animation animation;
//        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
//        ImageView imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);
//        TextView title = (TextView) sheetView.findViewById(R.id.title);
//        TextView path = (TextView) sheetView.findViewById(R.id.path);
//        path.setText("/storage/Android/data/com.ultimate.ultimatesmartstudent/files/Pictures/"+folder_main+"/ReceiveMsg_file_saved.jpg");
//        TextView imgDownload = (TextView) sheetView.findViewById(R.id.imgDownload);
//        title.setText(folder_main);
//        Picasso.get().load(message_bean.getImage()).placeholder(getResources().getDrawable(R.drawable.logo)).into(imgShow);
//        imgDownload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                imgDownload.startAnimation(animation);
//                UltimateProgress.showProgressBar(MessageActivity.this, "downloading...");
//                Picasso.with(MessageActivity.this).load(message_bean.getImage()).into(imgTraget);
//                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
//            }
//        });
//        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
//        btnNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnNo.startAnimation(animation);
//                mBottomSheetDialog.dismiss();
//            }
//        });
//
//        mBottomSheetDialog.show();
//
//    }
//
//    Target imgTraget = new Target() {
//        @RequiresApi(api = Build.VERSION_CODES.O)
//        @Override
//        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
////            Toast.makeText(GDSTest.this, "Downloaded Successfully", Toast.LENGTH_LONG).show();
////            File path = new File(Environment.getExternalStorageDirectory().getPath(), folder_main);
//            File path = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), folder_main);
//            path.mkdir();
//            File file = new File(path, folder_main + System.currentTimeMillis() + "saved.jpg");
//            if (!file.exists())
//                try {
//                    file.createNewFile();
//                    FileOutputStream ostream = new FileOutputStream(file);
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
//                    ostream.close();
//                    Toast.makeText(MessageActivity.this, "Downloaded Successfully!", Toast.LENGTH_LONG).show();
//                    UltimateProgress.cancelProgressBar();
//                } catch (IOException e) {
//                    UltimateProgress.cancelProgressBar();
//                    e.printStackTrace();
//                }
//        }
//
//        @Override
//        public void onBitmapFailed(Drawable errorDrawable) {
//            ErpProgress.cancelProgressBar();
//        }
//
//        @Override
//        public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//        }
//    };

    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "msg_image" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "msg_image" + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onMethod_Image_callback_new(Message_Bean message_bean) {
        openCommonMultiImage(message_bean.getSubject(),message_bean.getMulti_image());
    }

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    String viewwwww = "";

    private void openCommonMultiImage(String subject, ArrayList<String> multiImage) {
        final Dialog warningDialog = new Dialog(MessageActivity.this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView = (TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images = (ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(subject);
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(MessageActivity.this, multiImage, "Message");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        TextView tap_count = (TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>" + "1" + "</b>" + "", "#F4212C");
        String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));

        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);
        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.VISIBLE);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=multiImage;
                Intent intent = new Intent(MessageActivity.this, IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "Message");
                intent.putExtra("title", subject);
                intent.putExtra("sub", "Message details ");
                startActivity(intent);
            }
        });

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c = arg0 + 1;
                String title1 = getColoredSpanned("<b>" + String.valueOf(c) + "</b>" + "", "#F4212C");
                String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    @Override
    public void onMethod_recording_callback(Message_Bean message_bean) {
        openCommonVoice(message_bean.getRecordingsfile());
    }

    private MediaPlayer mediaPlayer;
    private Button playButton, pauseButton;
    ImageView play_img, pause_img;
    RelativeLayout play_lyt;
    private SeekBar seekBar;
    private Handler handler = new Handler();
    private TextView currentDurationTextView, totalDurationTextView;
    LottieAnimationView loti_record_play;
    private Button btnRetake, btnPick,btnSave;
    BottomSheetDialog mBottomSheetDialog_play;
    private void openCommonVoice(String recordingsfile) {

        mBottomSheetDialog_play = new BottomSheetDialog(MessageActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.activity_audio, null);
        mBottomSheetDialog_play.setContentView(sheetView);
        mBottomSheetDialog_play.setCancelable(false);

        btnRetake = sheetView.findViewById(R.id.btnRetake);
        btnPick = sheetView.findViewById(R.id.btnPick);
        btnSave = sheetView.findViewById(R.id.btnSave);
        play_lyt = sheetView.findViewById(R.id.play_lyt);
        play_img = sheetView.findViewById(R.id.play_img);
        pause_img = sheetView.findViewById(R.id.pause_img);
        playButton = sheetView.findViewById(R.id.playButton);
        pauseButton = sheetView.findViewById(R.id.pauseButton);
        seekBar = sheetView.findViewById(R.id.seekBar);
        loti_record_play = sheetView.findViewById(R.id.loti_play);
        currentDurationTextView = sheetView.findViewById(R.id.currentDurationTextView);
        totalDurationTextView = sheetView.findViewById(R.id.totalDurationTextView);


        btnSave.setVisibility(View.GONE);
        btnPick.setVisibility(View.GONE);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.seekTo(0);
                seekBar.setProgress(0);
                loti_record_play.pauseAnimation();
            }
        });
        String url=recordingsfile;
        //  Log.d("selectedImageUri",url);
        Uri selectedImageUri = Uri.parse(url);

        try {
            mediaPlayer.setDataSource(MessageActivity.this, selectedImageUri);

            mediaPlayer.prepare();
            mediaPlayer.start();
            updateSeekBar();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        play_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.pause();
                loti_record_play.pauseAnimation();
            }
        });

        pause_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.VISIBLE);
                pause_img.setVisibility(View.GONE);
                mediaPlayer.start();
                updateSeekBar();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                    updateDurationTextView(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        totalDurationTextView.setText(formatDuration(mediaPlayer.getDuration()));

        btnRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonCodeForDssmiss();
            }
        });

        // Dismiss the dialog and reset the MediaPlayer when the dialog is closed
        mBottomSheetDialog_play.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //  Toast.makeText(getApplicationContext(), "Dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog_play.show();
    }

    int starts = 0;

    private void commonCodeForDssmiss() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mBottomSheetDialog_play.dismiss();
    }

    private void updateSeekBar() {
        loti_record_play.playAnimation();
        seekBar.setMax(mediaPlayer.getDuration());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int currentPosition = mediaPlayer.getCurrentPosition();
                    seekBar.setProgress(currentPosition);
                    updateDurationTextView(currentPosition);
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    private void updateDurationTextView(int duration) {
        currentDurationTextView.setText(formatDuration(duration));
    }

    private String formatDuration(int milliseconds) {
        int seconds = (milliseconds / 1000) % 60;
        int minutes = (milliseconds / (1000 * 60)) % 60;
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }


    @Override
    public void onMethod_pdf_call_back(Message_Bean homeworkbean) {
        //   startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(homeworkbean.getPdf_file())));
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(MessageActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(MessageActivity.this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(homeworkbean.getSubject());
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.ebook).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        sub_wise.setText("View!");
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getBaseContext(), ViewPdfActivity.class);
                intent.putExtra("id", homeworkbean.getPdf_file());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
        date_wise.setText("Save!");
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                // UltimateProgress.showProgressBar(SubWiseHW.this,"Please wait...");
                String mUrl=homeworkbean.getPdf_file();
                // new DownloadFile().execute(mUrl);
                String path_name ="/storage/Android/data/com.ultimate.tbs_student/files/document/"+folder_main+"/Msg_file_saved.pdf";

                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("Msg_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Msg_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getApplicationContext(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

}