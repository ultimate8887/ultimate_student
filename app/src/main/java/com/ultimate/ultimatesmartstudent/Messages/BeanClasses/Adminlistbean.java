package com.ultimate.ultimatesmartstudent.Messages.BeanClasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Adminlistbean {
    private static String S_no = "s_no";
    private static String NAME = "name";
    private static String USER_PASS = "username_password";
    private static String EMAIL = "email";
    private static String PHONE="contact";
    /**
     * s_no : 1
     * name : Admin Admin
     * username_password : admin - admin
     * email : admin@gmail.com
     */

    private String s_no;
    private String name;
    private String username_password;
    private String email;
    /**
     * contact : 12345556
     */

    private String contact;

    public String getS_no() {
        return s_no;
    }

    public void setS_no(String s_no) {
        this.s_no = s_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername_password() {
        return username_password;
    }

    public void setUsername_password(String username_password) {
        this.username_password = username_password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static ArrayList<Adminlistbean> parseadminlistarray(JSONArray jsonArray) {
        ArrayList<Adminlistbean> list = new ArrayList<Adminlistbean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Adminlistbean p =  parseadminlistobject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    private static Adminlistbean parseadminlistobject(JSONObject jsonObject) {
        Adminlistbean adminlistbean=new Adminlistbean();

        try {
            if (jsonObject.has(S_no)) {
                adminlistbean.setS_no(jsonObject.getString(S_no));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                adminlistbean.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(USER_PASS) && !jsonObject.getString(USER_PASS).isEmpty() && !jsonObject.getString(USER_PASS).equalsIgnoreCase("null")) {
                adminlistbean.setUsername_password(jsonObject.getString(USER_PASS));
            }
            if (jsonObject.has(EMAIL) && !jsonObject.getString(EMAIL).isEmpty() && !jsonObject.getString(EMAIL).equalsIgnoreCase("null")) {
                adminlistbean.setEmail(jsonObject.getString(EMAIL));
            }
            if (jsonObject.has(PHONE) && !jsonObject.getString(PHONE).isEmpty() && !jsonObject.getString(PHONE).equalsIgnoreCase("null")) {
                adminlistbean.setContact(jsonObject.getString(PHONE));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return adminlistbean;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
