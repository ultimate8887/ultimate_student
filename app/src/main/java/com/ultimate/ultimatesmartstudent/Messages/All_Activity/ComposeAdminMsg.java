package com.ultimate.ultimatesmartstudent.Messages.All_Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.ForCamera.IPickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartstudent.ForCamera.PickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickSetup;
import com.ultimate.ultimatesmartstudent.Messages.AdapterClasses.SpinAdminlistadapter;
import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Adminlistbean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComposeAdminMsg extends AppCompatActivity implements IPickResult {
    @BindView(R.id.spinnerselectadmin)
    Spinner spinnneradmin;
    CommonProgress commonProgress;
    @BindView(R.id.staff_view)
    LinearLayout staff_view;
    @BindView(R.id.admin_view)
    RelativeLayout admin_view;
    ArrayList<Adminlistbean> adminlist = new ArrayList<>();
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    String admini_toid;
    String fromid;
    String to_type="admin";
    @BindView(R.id.send_messageadmin)
    Button send;
    @BindView(R.id.edtsubadmn)
    EditText subedittext;
    @BindView(R.id.edtAddmessageadmn)
    EditText editmsg;
    private Bitmap hwbitmap;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    @BindView(R.id.textView6)
    TextView textView6;
    private int type;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_admin_msg);
        ButterKnife.bind(this);
        staff_view.setVisibility(View.GONE);
        admin_view.setVisibility(View.VISIBLE);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText(getString(R.string.compose)+" "+getString(R.string.toadmin));
        fromid= User.getCurrentUser().getId();
        fetchadminlist();
    }

    private void fetchadminlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.ADMINLIST, adminapiCallback, this, params);
    }
    ApiHandler.ApiCallback adminapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    adminlist = Adminlistbean.parseadminlistarray(jsonObject.getJSONArray("admin_data"));
                    SpinAdminlistadapter adapter = new SpinAdminlistadapter(ComposeAdminMsg.this, adminlist);
                    spinnneradmin.setAdapter(adapter);
                    spinnneradmin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                admini_toid = adminlist.get(i-1).getS_no();
                                Log.e("classsid",admini_toid);

                            }else{
                                admini_toid="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
               // Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.send_messageadmin)
    public void send_message(){
        send.startAnimation(animation);
        if(checkValid()){
            commonProgress.show();
            HashMap<String,String> params=new HashMap<>();
            params.put("from_id",fromid);
            params.put("to_id",admini_toid);
            params.put("to_type",to_type);
            params.put("sub",subedittext.getText().toString());
            params.put("msg",editmsg.getText().toString());
            if (hwbitmap != null) {
                String encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.MESSAGE, apisendmsgCallback, this, params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ComposeAdminMsg.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;
        if (admini_toid.isEmpty() || admini_toid == "") {
            valid = false;
            errorMsg = "Please select admin";
        }
        else if (subedittext.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter subject";
        }else if(editmsg.getText().toString().trim().length()<=0){
            valid=false;
            errorMsg="Please enter the Messages";
        }

        if (!valid) {
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_LONG).show();
        }
        return valid;
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.textView6)
    public void click() {
        textView6.startAnimation(animation);
        type = 10;
        pick_img();
//        ImagePicker.Companion.with(this)
//                .galleryOnly()
//                .crop()	    			//Crop image(Optional), Check Customization for more option
//                .compress(1024)			//Final image size will be less than 1 MB(Optional)
//                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
//                .start();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);
        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            imageView6.setImageBitmap(r.getBitmap());
            hwbitmap= r.getBitmap();
            imageView6.setVisibility(View.VISIBLE);

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }


    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }


}