package com.ultimate.ultimatesmartstudent.Messages.BeanClasses;

import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Message_Bean {


    private static String SUBJECT="subject";
    private static String MESSAGE="message";
    private static String DATE="created_on";
    private static String MESSAGE_FROM="from_type";
    private static String MESSAGE_NAME="from_name";
    private static String IMAGE="image";
    private static String FIMAGE="from_image";
    /**
     * es_messagesid : 7
     * from_id : 1
     * from_type : admin
     * to_id : 1
     * to_type : admin
     * subject : ljkllkj
     * message : <p>&nbsp;hlulkjl</p>
     * created_on : 2018-07-13 04:16:30
     * status : active
     * from_status : active
     * to_status : active
     * replay_status : notreplied
     */
    private String image;
    private String es_messagesid;
    private String from_id;
    private String from_type;
    private String to_id;
    private String to_type;
    private String subject;
    private String message;
    private String created_on;
    private String status;
    private String from_status;
    private String to_status;
    private String replay_status;

    private String pdf_file;

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getRecordingsfile() {
        return recordingsfile;
    }

    public void setRecordingsfile(String recordingsfile) {
        this.recordingsfile = recordingsfile;
    }

    private String recordingsfile;


    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    private String image_tag;

    public ArrayList<String> getMulti_image() {
        return multi_image;
    }

    public void setMulti_image(ArrayList<String> multi_image) {
        this.multi_image = multi_image;
    }

    private ArrayList<String> multi_image;

    public String getFrom_image() {
        return from_image;
    }

    public void setFrom_image(String from_image) {
        this.from_image = from_image;
    }

    private String from_image;

    /**
     * from_name : Monika Gupta
     */

    private String from_name;

    public String getEs_messagesid() {
        return es_messagesid;
    }

    public void setEs_messagesid(String es_messagesid) {
        this.es_messagesid = es_messagesid;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getFrom_type() {
        return from_type;
    }

    public void setFrom_type(String from_type) {
        this.from_type = from_type;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getTo_type() {
        return to_type;
    }

    public void setTo_type(String to_type) {
        this.to_type = to_type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFrom_status() {
        return from_status;
    }

    public void setFrom_status(String from_status) {
        this.from_status = from_status;
    }

    public String getTo_status() {
        return to_status;
    }

    public void setTo_status(String to_status) {
        this.to_status = to_status;
    }

    public String getReplay_status() {
        return replay_status;
    }

    public void setReplay_status(String replay_status) {
        this.replay_status = replay_status;
    }

    public static ArrayList<Message_Bean> parseMessageArray(JSONArray jsonArray) {
        ArrayList<Message_Bean> list = new ArrayList<Message_Bean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Message_Bean p = parsemessageObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static Message_Bean parsemessageObject(JSONObject jsonObject) {

        Message_Bean msgObj = new Message_Bean();
        try {


            if (jsonObject.has(SUBJECT) && !jsonObject.getString(SUBJECT).isEmpty() && !jsonObject.getString(SUBJECT).equalsIgnoreCase("null")) {
                msgObj.setSubject(jsonObject.getString(SUBJECT));
            }
            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                msgObj.setCreated_on(jsonObject.getString(DATE));
            }
            if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                msgObj.setMessage(jsonObject.getString(MESSAGE));
            }
            if (jsonObject.has(MESSAGE_FROM) && !jsonObject.getString(MESSAGE_FROM).isEmpty() && !jsonObject.getString(MESSAGE_FROM).equalsIgnoreCase("null")) {
                msgObj.setFrom_type(jsonObject.getString(MESSAGE_FROM));
            }

            if (jsonObject.has(MESSAGE_NAME) && !jsonObject.getString(MESSAGE_NAME).isEmpty() && !jsonObject.getString(MESSAGE_NAME).equalsIgnoreCase("null")) {
                msgObj.setFrom_name(jsonObject.getString(MESSAGE_NAME));
            }

            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                msgObj.setImage(Constants.getImageBaseURL()+jsonObject.getString(IMAGE));
            }

            if (jsonObject.has(FIMAGE) && !jsonObject.getString(FIMAGE).isEmpty() && !jsonObject.getString(FIMAGE).equalsIgnoreCase("null")) {
                msgObj.setFrom_image(Constants.getImageBaseURL()+jsonObject.getString(FIMAGE));
            }

            if (jsonObject.has("pdf_file") && !jsonObject.getString("pdf_file").isEmpty() && !jsonObject.getString("pdf_file").equalsIgnoreCase("null")) {
                msgObj.setPdf_file(Constants.getImageBaseURL()+jsonObject.getString("pdf_file"));
            }
            if (jsonObject.has("recordingsfile") && !jsonObject.getString("recordingsfile").isEmpty() && !jsonObject.getString("recordingsfile").equalsIgnoreCase("null")) {
                msgObj.setRecordingsfile(Constants.getImageBaseURL()+jsonObject.getString("recordingsfile"));
            }

            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                msgObj.setImage_tag(jsonObject.getString("image_tag"));
            }
            if (jsonObject.has("multi_image") && jsonObject.get("multi_image") instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray("multi_image");
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                msgObj.setMulti_image(img_arrs);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;


    }

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
