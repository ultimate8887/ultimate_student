package com.ultimate.ultimatesmartstudent.Messages.All_Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.Assignment.FileUtils;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.ForCamera.IPickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartstudent.ForCamera.PickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickSetup;
import com.ultimate.ultimatesmartstudent.Messages.AdapterClasses.Adapteraddstaf_post;
import com.ultimate.ultimatesmartstudent.Messages.AdapterClasses.Adapteraddstff_departmnt;
import com.ultimate.ultimatesmartstudent.Messages.AdapterClasses.Spinner_staff_adapter;
import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Deparment_bean;
import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Postname_Bean;
import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Stafflist_bean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComposeStaffMsg extends AppCompatActivity implements IPickResult {
    @BindView(R.id.spinnerselectdepart)
    Spinner spinnerdepart;
    CommonProgress commonProgress;
    @BindView(R.id.spinnerselectpost)
    Spinner spinnerpost;
    @BindView(R.id.spinnerselectstff)
    Spinner spinnerstaff;
    @BindView(R.id.edtsubadmn)
    EditText edtsubadmn;
    @BindView(R.id.edtAddmessageadmn)
    EditText editmessage;
    @BindView(R.id.send_messageadmin)
    Button send;
    @BindView(R.id.staff_view)
    LinearLayout staff_view;
    @BindView(R.id.admin_view)
    RelativeLayout admin_view;
    String departmentid="";
    String post_id="";
    String staff_id="";
    String from_id="";
    String to_type="staff";
    ArrayList<Deparment_bean> department_list=new ArrayList<>();
    ArrayList<Postname_Bean> postlist=new ArrayList<>();
    ArrayList<Stafflist_bean> stafflist=new ArrayList<>();
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    Adapteraddstaf_post spinerpost;
    Spinner_staff_adapter adapter;
    private Bitmap hwbitmap;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    @BindView(R.id.textView6)
    TextView textView6;
    private int type;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_admin_msg);
        ButterKnife.bind(this);
        staff_view.setVisibility(View.VISIBLE);
        commonProgress=new CommonProgress(this);
        admin_view.setVisibility(View.GONE);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText(getString(R.string.compose)+" "+getString(R.string.tostaff));
        from_id = User.getCurrentUser().getId();
        fetchstaffname();

    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    private void fetchdepartment() {
        commonProgress.show();
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.deparmrnt, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (department_list != null) {
                        department_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("department_data");
                    department_list = Deparment_bean.parseDepartmentArray(jsonArray);
                    Adapteraddstff_departmnt spinneradapt= new Adapteraddstff_departmnt(ComposeStaffMsg.this,department_list);
                    spinnerdepart.setAdapter(spinneradapt);
                    spinnerdepart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                if (spinerpost != null) {
                                    postlist.clear();
                                    spinerpost.notifyDataSetChanged();
                                }

                                if (adapter != null) {
                                    stafflist.clear();
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            if (i > 0) {
                                departmentid = department_list.get(i-1 ).getId();
                                //fetchSubject(classid);
                                post_id="";
                                staff_id="";
                                fetchPostname(departmentid);

                            }else{
                                departmentid="";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ComposeStaffMsg.this, LoginActivity.class));
                    ComposeStaffMsg.this.finish();
                }
            }
        }
    };

    private void fetchPostname(String departmentid) {
        commonProgress.show();
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.Post+Constants.dpartment_id+departmentid, apiCallbackpost, this, params);

    }
    ApiHandler.ApiCallback apiCallbackpost = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (postlist != null) {
                        postlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("post_data");
                    postlist = Postname_Bean.parsePostnsmeArray(jsonArray);
                    spinerpost = new Adapteraddstaf_post(ComposeStaffMsg.this,postlist);
                    spinnerpost.setAdapter(spinerpost);
                    spinnerpost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (adapter != null) {
                                stafflist.clear();
                                adapter.notifyDataSetChanged();
                            }

                            if (i > 0) {
                                post_id = postlist.get(i-1 ).getId();
                                staff_id="";
                               // fetchstaffname(post_id);
                            }else{
                                post_id="";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    };
//    public void fetchstaffname(String post_id){
//        commonProgress.show();
//        HashMap<String,String> params=new HashMap<>();
//        params.put("d_id",departmentid);
//        params.put("p_id",post_id);
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, apiCallbackstaffdetail, this, params);
//    }


    public void fetchstaffname(){
         commonProgress.show();
         HashMap<String,String> params=new HashMap<>();
         params.put("d_id",departmentid);
         params.put("p_id",post_id);
         params.put("c_id",User.getCurrentUser().getClass_id());
         ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, apiCallbackstaffdetail, this, params);
      }
    ApiHandler.ApiCallback apiCallbackstaffdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    stafflist = Stafflist_bean.parsestaffArray(jsonObject.getJSONArray("staff_data"));
                    adapter = new Spinner_staff_adapter(ComposeStaffMsg.this, stafflist);
                    spinnerstaff.setAdapter(adapter);
                    spinnerstaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                staff_id = stafflist.get(i-1).getId();
                                Log.e("studentss1",staff_id);
                            }else{
                                staff_id="";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    };
    @OnClick(R.id.send_messageadmin)
    public void send_message(){
        if(checkValid()){
            send.startAnimation(animation);
            commonProgress.show();
            HashMap<String,String> params=new HashMap<>();
            params.put("from_id",from_id);
            params.put("to_id",staff_id);
            params.put("to_type",to_type);
            if (hwbitmap != null) {
                String encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }
            params.put("sub",edtsubadmn.getText().toString());
            params.put("msg",editmessage.getText().toString());
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.MESSAGE, apisendmsgCallback, this, params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                  //  Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
//                Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ComposeStaffMsg.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;
//        if (departmentid.isEmpty() || departmentid == "") {
//            valid = false;
//            errorMsg = "Please select department";
//        }else if (post_id.isEmpty() || post_id == "") {
//            valid = false;
//            errorMsg = "Please select post";
//        }else
       if (staff_id.isEmpty() || staff_id == "") {
            valid = false;
            errorMsg = "Please select staff";
        }
        else if (edtsubadmn.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter subject";
        }else if(editmessage.getText().toString().trim().length()<=0){
            valid=false;
            errorMsg="Please enter the Messages";
        }

        if (!valid) {
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_LONG).show();
        }
        return valid;
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.textView6)
    public void click() {
        textView6.startAnimation(animation);
//        type = 10;
        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
      //  Log.e("imageView6", String.valueOf(r));
        if (r.getError() == null) {
            //If you want the Bitmap.
            imageView6.setImageBitmap(r.getBitmap());
            hwbitmap= r.getBitmap();
            imageView6.setVisibility(View.VISIBLE);
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }

}