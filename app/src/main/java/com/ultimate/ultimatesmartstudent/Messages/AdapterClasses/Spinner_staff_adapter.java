package com.ultimate.ultimatesmartstudent.Messages.AdapterClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Stafflist_bean;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

public class Spinner_staff_adapter extends BaseAdapter {
    Context context;
    ArrayList<Stafflist_bean> stafflist;
    LayoutInflater inflter;

    public Spinner_staff_adapter(Context context, ArrayList<Stafflist_bean> stafflist) {
        this.context=context;
        this.stafflist=stafflist;
        inflter = (LayoutInflater.from(context));
    }


    @Override
    public int getCount() {
        return stafflist.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Select Staff");
        } else {
            Stafflist_bean classObj = stafflist.get(position - 1);
            label.setText(classObj.getName());
        }

        return convertView;

    }
}
