package com.ultimate.ultimatesmartstudent.Messages.AdapterClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Deparment_bean;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

public class Adapteraddstff_departmnt extends BaseAdapter {
    Context context;
    ArrayList<Deparment_bean> department_list;
    LayoutInflater inflter;

    public Adapteraddstff_departmnt(Context context, ArrayList<Deparment_bean> department_list) {
        this.context=context;
        this.department_list=department_list;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return department_list.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Select Department");
        } else {
            Deparment_bean classObj = department_list.get(position - 1);
            label.setText(classObj.getName());
        }

        return convertView;

    }
}
