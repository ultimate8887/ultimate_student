package com.ultimate.ultimatesmartstudent.Messages.AdapterClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Adminlistbean;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

public class SpinAdminlistadapter extends BaseAdapter {
    Context context;
    ArrayList<Adminlistbean> adminlist;
    LayoutInflater inflter;
    public SpinAdminlistadapter(Context context, ArrayList<Adminlistbean> adminlist) {
        this.context=context;
        this.adminlist=adminlist;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return adminlist.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Select Admin");
        } else {
            Adminlistbean classObj = adminlist.get(position - 1);
            label.setText(classObj.getName());
        }
        return convertView;
    }
}
