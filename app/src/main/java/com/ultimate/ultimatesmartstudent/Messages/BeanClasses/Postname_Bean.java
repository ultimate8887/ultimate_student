package com.ultimate.ultimatesmartstudent.Messages.BeanClasses;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Postname_Bean {

    private static String ID="id";
    private static String NAME="name";
    /**
     * id : 1
     * name : Android Dev
     */

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<Postname_Bean> parsePostnsmeArray(JSONArray jsonArray) {
        ArrayList<Postname_Bean> list=new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Postname_Bean p = parsepostObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static Postname_Bean parsepostObject(JSONObject jsonObject) {
        Postname_Bean casteObj = new Postname_Bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
