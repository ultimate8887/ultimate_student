package com.ultimate.ultimatesmartstudent.BeanModule;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SubjectBeancls {

    private static String ID = "id";
    private static String NAME = "name";
    /**
     * id : 2
     * name : NURSERY
     * school_id : 0
     * group_id : 9
     * order_by : 1
     */

    private String id;
    private String name;
    private String school_id;
    private String order_by;

    /**
     * group_data : {"id":"1","name":"Pre-Primary(Nry-Prep2)"}
     */

    public CommonBean getGroup_data() {
        return group_data;
    }

    public void setGroup_data(CommonBean group_data) {
        this.group_data = group_data;
    }

    private CommonBean group_data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }


    public String getOrder_by() {
        return order_by;
    }

    public void setOrder_by(String order_by) {
        this.order_by = order_by;
    }

    public static ArrayList<SubjectBeancls> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<SubjectBeancls>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                SubjectBeancls p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static SubjectBeancls parseClassObject(JSONObject jsonObject) {
        SubjectBeancls casteObj = new SubjectBeancls();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(Constants.GROUP_DATA)) {
                casteObj.setGroup_data(CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.GROUP_DATA)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
