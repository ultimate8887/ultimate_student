package com.ultimate.ultimatesmartstudent.BeanModule;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ultimate.ultimatesmartstudent.UltimateERP;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class User {

    private static User sInstance;
    private static ArrayList<User> sInstance_list;
    private static String PREFRENCES_USER = "user";
    private static String PREFRENCES_USER_LIST = "userlist";
    public static String ID = "id";
    private static String EMAIL = "email";
    private static String NAME = "username";
    private static String PHONENO = "phoneno";
    private static String TYPE = "type";
    private static String FIRSTNAME = "firstname";
    private static String LASTNAME = "lastname";
    private static String FATHER = "father";
    private static String BOARD = "board";
    private static String CLASS_ID = "class_id";
    private static String R_ID = "r_id";
    private static String SCHOOLDATA = "school_data";
    private static String LAT = "lat";
    private static String LNG = "lng";
    private static String CLASSNAME = "class_name";
    private static String PROFILE = "profile";

    private static String ADDHAR = "aadhar_image";
    private static String TENTH = "tenmarksheet_image";
    private static String TWELTH = "twelvemarksheet_image";
    private static String SC = "schol_certif_image";
    private static String STC = "schol_trans_certif_image";
    private static String GRAD = "graduation_image";
    private static String MOB_NAME = "mobile_name";
    private static String MOB_ID = "mobile_id";
    /**
     * bg : B+
     * gender : female
     * dob : 1998-01-15
     * aadhaar : 123456789
     * address : laleana, Bathinda\n Pincode: 12345
     * device_token : dMgxx1pOQ5A:APA91bF0LLkQpB-qW1FvVNm5Z0TokZVOyU0IHeM_sbDn0Zwx5E17FuC6SNBM-sdH4XU1PitV_S9oX1qN7EVzl185aWfHUmHmbE3sjI8sdOfLhqEl9xeGQffu-IMnxyekpVR79DqUJjog
     * lat : 30.196637500000016
     * lng : 74.95832421875001
     */

    private String bg = "";
    private String gender = "";
    private String dob = "";
    private String aadhaar = "";
    private String address = "";
    private String roll_no = "";
    private String rollserialno;

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    private String father;

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getMobile_name() {
        return mobile_name;
    }

    public void setMobile_name(String mobile_name) {
        this.mobile_name = mobile_name;
    }


    public String getMobile_id() {
        return mobile_id;
    }

    public void setMobile_id(String mobile_id) {
        this.mobile_id = mobile_id;
    }

    private String mobile_id;
    private String mobile_name;
    private String board;
    private String   morningVId;
    private String eveningVId;
    private String  section_name;

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    private String section_id;

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getMorningVId() {
        return morningVId;
    }

    public void setMorningVId(String morningVId) {
        this.morningVId = morningVId;
    }

    public String getEveningVId() {
        return eveningVId;
    }

    public void setEveningVId(String eveningVId) {
        this.eveningVId = eveningVId;
    }

    public String getRollserialno() {
        return rollserialno;
    }

    public void setRollserialno(String rollserialno) {
        this.rollserialno = rollserialno;
    }
    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    /**
     * id : 1
     * type : super
     * firstname : Admin
     * lastname : Admin
     * username : admin
     * email : admin@gmail.com
     * phoneno : 12345556
     */

    private String profile;
    private String id;
    private String type;
    private String firstname = "";
    private String lastname = "";
    private String username = "";
    private String email = "";
    private String phoneno = "";
    private SchoolBean schoolData;
    private String class_id;

    /**
     * r_id : 1
     */

    private String r_id;
    private double lat = 0.0;
    private double lng = 0.0;



    private String aadhar_image;
    private String tenmarksheet_image;
    private String twelvemarksheet_image;

    public String getAadhar_image() {
        return aadhar_image;
    }

    public void setAadhar_image(String aadhar_image) {
        this.aadhar_image = aadhar_image;
    }

    public String getTenmarksheet_image() {
        return tenmarksheet_image;
    }

    public void setTenmarksheet_image(String tenmarksheet_image) {
        this.tenmarksheet_image = tenmarksheet_image;
    }

    public String getTwelvemarksheet_image() {
        return twelvemarksheet_image;
    }

    public void setTwelvemarksheet_image(String twelvemarksheet_image) {
        this.twelvemarksheet_image = twelvemarksheet_image;
    }

    public String getSchol_certif_image() {
        return schol_certif_image;
    }

    public void setSchol_certif_image(String schol_certif_image) {
        this.schol_certif_image = schol_certif_image;
    }

    public String getSchol_trans_certif_image() {
        return schol_trans_certif_image;
    }

    public void setSchol_trans_certif_image(String schol_trans_certif_image) {
        this.schol_trans_certif_image = schol_trans_certif_image;
    }

    public String getGraduation_image() {
        return graduation_image;
    }

    public void setGraduation_image(String graduation_image) {
        this.graduation_image = graduation_image;
    }

    private String schol_certif_image;
    private String schol_trans_certif_image;
    private String graduation_image;


    /**
     * device_token :
     * class_name : NURSERY1
     * lat : 0
     * lng : 0
     * school_data : {"id":"5","address":"SCF-60,2nd Floor,Phase-1,Model Town,Bathinda,151001","logo":"office_admin/images/school_logo/school_06192018_110643.jpg","phoneno":"9855475456","email":"sukhwinder@smartedgeindia.com","website":"www.smartedgeindia.com","name":"Digital School1","key":"SE2","fi_ac_startdate":"2018-04-01","fi_ac_enddate":"2019-03-31","txt_from":"1","short_name":"SEDS","online_payment":"1"}
     */

    private String class_name;

    public SchoolBean getSchoolData() {
        return schoolData;
    }

    public void setSchoolData(SchoolBean schoolData) {
        this.schoolData = schoolData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public static void setCurrentUser(JSONObject jsonObject) {
        User user = new User();
//       User user = getCurrentUser();
        try {
//
//            if (user == null) {
//                user = new User();
//            }
            if (jsonObject.has(ID)) {
                user.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(EMAIL) && !jsonObject.getString(EMAIL).isEmpty() && !jsonObject.getString(EMAIL).equalsIgnoreCase("null")) {
                user.setEmail(jsonObject.getString(User.EMAIL));
            }
            if (jsonObject.has(MOB_NAME) && !jsonObject.getString(MOB_NAME).isEmpty() && !jsonObject.getString(MOB_NAME).equalsIgnoreCase("null")) {
                user.setMobile_name(jsonObject.getString(User.MOB_NAME));
            }

            if (jsonObject.has(MOB_ID) && !jsonObject.getString(MOB_ID).isEmpty() && !jsonObject.getString(MOB_ID).equalsIgnoreCase("null")) {
                user.setMobile_id(jsonObject.getString(User.MOB_ID));
            }


            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                user.setUsername(jsonObject.getString(User.NAME));
            }
            if (jsonObject.has(PHONENO) && !jsonObject.getString(PHONENO).isEmpty() && !jsonObject.getString(PHONENO).equalsIgnoreCase("null")) {
                user.setPhoneno(jsonObject.getString(PHONENO));
            }
            if (jsonObject.has(TYPE) && !jsonObject.getString(TYPE).isEmpty() && !jsonObject.getString(TYPE).equalsIgnoreCase("null")) {
                user.setType(jsonObject.getString(TYPE));
            }
            if (jsonObject.has(FIRSTNAME) && !jsonObject.getString(FIRSTNAME).isEmpty() && !jsonObject.getString(FIRSTNAME).equalsIgnoreCase("null")) {
                user.setFirstname(jsonObject.getString(FIRSTNAME));
            }
            if (jsonObject.has(LASTNAME) && !jsonObject.getString(LASTNAME).isEmpty() && !jsonObject.getString(LASTNAME).equalsIgnoreCase("null")) {
                user.setLastname(jsonObject.getString(LASTNAME));
            }
            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                user.setClass_id(jsonObject.getString(CLASS_ID));
            }
            if (jsonObject.has(R_ID) && !jsonObject.getString(R_ID).isEmpty() && !jsonObject.getString(R_ID).equalsIgnoreCase("null")) {
                user.setR_id(jsonObject.getString(R_ID));
            }
            if (jsonObject.has(LAT) && !jsonObject.getString(LAT).isEmpty() && !jsonObject.getString(LAT).equalsIgnoreCase("null")) {
                user.setLat(jsonObject.getDouble(LAT));
            }
            if (jsonObject.has(LNG) && !jsonObject.getString(LNG).isEmpty() && !jsonObject.getString(LNG).equalsIgnoreCase("null")) {
                user.setLng(jsonObject.getDouble(LNG));
            }
            if (jsonObject.has(FATHER) && !jsonObject.getString(FATHER).isEmpty() && !jsonObject.getString(FATHER).equalsIgnoreCase("null")) {
                user.setFather(jsonObject.getString(User.FATHER));
            }
            if (jsonObject.has(BOARD) && !jsonObject.getString(BOARD).isEmpty() && !jsonObject.getString(BOARD).equalsIgnoreCase("null")) {
                user.setBoard(jsonObject.getString(User.BOARD));
            }
            if (jsonObject.has(CLASSNAME) && !jsonObject.getString(CLASSNAME).isEmpty() && !jsonObject.getString(CLASSNAME).equalsIgnoreCase("null")) {
                user.setClass_name(jsonObject.getString(CLASSNAME));
            }
            if (jsonObject.has("bg") && !jsonObject.getString("bg").isEmpty() && !jsonObject.getString("bg").equalsIgnoreCase("null")) {
                user.setBg(jsonObject.getString("bg"));
            }
            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                user.setGender(jsonObject.getString("gender"));
            }
            if (jsonObject.has("dob") && !jsonObject.getString("dob").isEmpty() && !jsonObject.getString("dob").equalsIgnoreCase("null")) {
                user.setDob(jsonObject.getString("dob"));
            }
            if (jsonObject.has("aadhaar") && !jsonObject.getString("aadhaar").isEmpty() && !jsonObject.getString("aadhaar").equalsIgnoreCase("null")) {
                user.setAadhaar(jsonObject.getString("aadhaar"));
            }
            if (jsonObject.has("address") && !jsonObject.getString("address").isEmpty() && !jsonObject.getString("address").equalsIgnoreCase("null")) {
                user.setAddress(jsonObject.getString("address"));
            }
            if (jsonObject.has("roll_no") && !jsonObject.getString("roll_no").isEmpty() && !jsonObject.getString("roll_no").equalsIgnoreCase("null")) {
                user.setRoll_no(jsonObject.getString("roll_no"));
            }
            if (jsonObject.has("rollserialno") && !jsonObject.getString("rollserialno").isEmpty() && !jsonObject.getString("rollserialno").equalsIgnoreCase("null")) {
                user.setRollserialno(jsonObject.getString("rollserialno"));
            }
            if (jsonObject.has("morningVId") && !jsonObject.getString("morningVId").isEmpty() && !jsonObject.getString("morningVId").equalsIgnoreCase("null")) {
                user.setMorningVId(jsonObject.getString("morningVId"));
            }
            if (jsonObject.has("eveningVId") && !jsonObject.getString("eveningVId").isEmpty() && !jsonObject.getString("eveningVId").equalsIgnoreCase("null")) {
                user.setEveningVId(jsonObject.getString("eveningVId"));
            }
            if (jsonObject.has("section_id") && !jsonObject.getString("section_id").isEmpty() && !jsonObject.getString("section_id").equalsIgnoreCase("null")) {
                user.setSection_id(jsonObject.getString("section_id"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                user.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
                user.setProfile("https://ultimatesolutiongroup.com/" + jsonObject.getString(PROFILE));
            }
            if (jsonObject.has(ADDHAR) && !jsonObject.getString(ADDHAR).isEmpty() && !jsonObject.getString(ADDHAR).equalsIgnoreCase("null")) {
                user.setAadhar_image(Constants.getImageBaseURL() + jsonObject.getString(ADDHAR));
            }
            if (jsonObject.has(TENTH) && !jsonObject.getString(TENTH).isEmpty() && !jsonObject.getString(TENTH).equalsIgnoreCase("null")) {
                user.setTenmarksheet_image(Constants.getImageBaseURL() + jsonObject.getString(TENTH));
            }
            if (jsonObject.has(TWELTH) && !jsonObject.getString(TWELTH).isEmpty() && !jsonObject.getString(TWELTH).equalsIgnoreCase("null")) {
                user.setTwelvemarksheet_image(Constants.getImageBaseURL() + jsonObject.getString(TWELTH));
            }
            if (jsonObject.has(SC) && !jsonObject.getString(SC).isEmpty() && !jsonObject.getString(SC).equalsIgnoreCase("null")) {
                user.setSchol_certif_image(Constants.getImageBaseURL() + jsonObject.getString(SC));
            }
            if (jsonObject.has(STC) && !jsonObject.getString(STC).isEmpty() && !jsonObject.getString(STC).equalsIgnoreCase("null")) {
                user.setSchol_trans_certif_image(Constants.getImageBaseURL() + jsonObject.getString(STC));
            }
            if (jsonObject.has(GRAD) && !jsonObject.getString(GRAD).isEmpty() && !jsonObject.getString(GRAD).equalsIgnoreCase("null")) {
                user.setGraduation_image(Constants.getImageBaseURL() + jsonObject.getString(GRAD));
            }
            if (jsonObject.has(SCHOOLDATA)) {
                user.setSchoolData(SchoolBean.parseSchoolObject(jsonObject.getJSONObject(SCHOOLDATA)));
            }
            User.setUserlist(user);
            logout();
            Gson gson = new Gson();
            String userString = gson.toJson(user, User.class);
            SharedPreferences preferences = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            preferences.edit().putString(PREFRENCES_USER, userString).commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static void setCurrentUserUL(User user) {
        logout();
        Gson gson = new Gson();
        String userString = gson.toJson(user, User.class);
        SharedPreferences preferences = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(PREFRENCES_USER, userString).commit();
    }

    public static User getCurrentUser() {
        if (sInstance == null) {
            SharedPreferences preferences = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            String userString = preferences.getString(PREFRENCES_USER, null);
            if (userString != null) {
                Gson gson = new Gson();
                sInstance = gson.fromJson(userString, User.class);
                return sInstance;
            } else {
                return null;
            }
        } else {
            return sInstance;
        }
    }

    public static void logout() {
        sInstance = null;
        SharedPreferences pref = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(PREFRENCES_USER);
        editor.commit();

    }

    public static void clearUserList() {
        // Clear the user list
        ArrayList<User> userlist = getUserList();
        userlist.clear();

        // Update the shared preferences
        sInstance_list = null;

        SharedPreferences pref = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(PREFRENCES_USER_LIST);
        editor.commit(); // Use commit() to immediately save changes; you can use apply() for asynchronous saving
    }

    public static void removeUserFromList() {
        String current_id = getCurrentUser().getId();
        ArrayList<User> userlist = getUserList();
        for (int i = 0; i < userlist.size(); i++) {
            if (userlist.get(i).getId().equalsIgnoreCase(current_id)) {
                userlist.remove(i);
                break;
            }

        }

        if (userlist.size() == 0) {
            sInstance_list = null;
            SharedPreferences pref = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.remove(PREFRENCES_USER_LIST);
            editor.commit();
        } else {
            sInstance_list = userlist;
            Gson gson = new Gson();
            String userString = gson.toJson(userlist, new TypeToken<ArrayList<User>>() {
            }.getType());
            SharedPreferences preferences = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            preferences.edit().putString(PREFRENCES_USER_LIST, userString).commit();
        }


    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getR_id() {
        return r_id;
    }

    public void setR_id(String r_id) {
        this.r_id = r_id;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLat() {
        return lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLng() {
        return lng;
    }


    public static ArrayList<User> getUserList() {

        if (sInstance_list == null) {
            SharedPreferences preferences = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            String userString = preferences.getString(PREFRENCES_USER_LIST, null);
            if (userString != null) {
                Gson gson = new Gson();
                sInstance_list = gson.fromJson(userString, new TypeToken<ArrayList<User>>() {
                }.getType());

                return sInstance_list;
            } else {
                return null;
            }
        } else {
            return sInstance_list;
        }

    }


    public static void setUserlist(User userObj) {
        ArrayList<User> userlist = getUserList();
        if (userlist == null) {
            userlist = new ArrayList<>();
        }
        userlist.add(userObj);
        Gson gson = new Gson();
        String userString = gson.toJson(userlist, new TypeToken<ArrayList<User>>() {
        }.getType());
        SharedPreferences preferences = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(PREFRENCES_USER_LIST, userString).commit();
    }


    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }
}

