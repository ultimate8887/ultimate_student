package com.ultimate.ultimatesmartstudent.BeanModule;

import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONException;
import org.json.JSONObject;

public class SchoolBean {
    private static String ID = "id";
    private static String NAME = "name";
    private static String ADDRESS = "address";
    private static String LOGO = "logo";
    private static String PHONENO = "phoneno";
    private static String EMAIL = "email";
    private static String WEBSITE = "website";
    private static String BASE_URL = "url";
    private static String FEE_ONLINE = "online_payment";
    private static String DB_HOST = "dbhost";
    private static String DB_USER = "dbuser";
    private static String DB_PASS = "dbpass";
    private static String DB_NAME = "dbname";
    private static String SCHOOL_ID_KEY="fi_school_id";
    private static String NO_OF_SUBJECT = "fi_no_subjects";
    /**
     * id : 1
     * address : Bathinda
     * logo : st_12272017_041245_logo (2).png
     * phoneno : 8264883507
     * email : ravinder@smartedgeindia.com
     * website :
     * name : Demo
     */
    private String fi_no_subjects;
    private String id;
    private String address;
    private String logo;
    private String phoneno;
    private String email;
    private String website;
    private String name;
    private String baseUrl;
    private String dbhost;
    private String dbuser;
    private String dbpass;
    private String dbname;
    /**
     * key : SE2
     * fi_ac_startdate : 2018-04-01
     * fi_ac_enddate : 2019-03-31
     * txt_from : 1
     * short_name : SEDS
     */
    private String fi_school_id;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private String key;

    public String getFi_school_id() {
        return fi_school_id;
    }

    public void setFi_school_id(String fi_school_id) {
        this.fi_school_id = fi_school_id;
    }
    private String txt_from="0";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDbhost() {
        return dbhost;
    }

    public void setDbhost(String dbhost) {
        this.dbhost = dbhost;
    }

    public String getDbuser() {
        return dbuser;
    }

    public void setDbuser(String dbuser) {
        this.dbuser = dbuser;
    }

    public String getDbpass() {
        return dbpass;
    }

    public void setDbpass(String dbpass) {
        this.dbpass = dbpass;
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }
    public String getFi_no_subjects() {
        return fi_no_subjects;
    }

    public void setFi_no_subjects(String fi_no_subjects) {
        this.fi_no_subjects = fi_no_subjects;
    }

    public static SchoolBean parseSchoolObject(JSONObject jsonObject) {
        DebugLog.printLog("User_details_jsonObject ","\njsonObject"+ jsonObject);
        SchoolBean schoolObj = new SchoolBean();
        try {
            if (jsonObject.has(ID)) {
                schoolObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(EMAIL) && !jsonObject.getString(EMAIL).isEmpty() && !jsonObject.getString(EMAIL).equalsIgnoreCase("null")) {
                schoolObj.setEmail(jsonObject.getString(EMAIL));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                schoolObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(PHONENO) && !jsonObject.getString(PHONENO).isEmpty() && !jsonObject.getString(PHONENO).equalsIgnoreCase("null")) {
                schoolObj.setPhoneno(jsonObject.getString(PHONENO));
            }
            if (jsonObject.has(ADDRESS) && !jsonObject.getString(ADDRESS).isEmpty() && !jsonObject.getString(ADDRESS).equalsIgnoreCase("null")) {
                schoolObj.setAddress(jsonObject.getString(ADDRESS));
            }
            if (jsonObject.has(LOGO) && !jsonObject.getString(LOGO).isEmpty() && !jsonObject.getString(LOGO).equalsIgnoreCase("null")) {
                schoolObj.setLogo(Constants.getImageBaseURL() + jsonObject.getString(LOGO));
            }
            if (jsonObject.has(WEBSITE) && !jsonObject.getString(WEBSITE).isEmpty() && !jsonObject.getString(WEBSITE).equalsIgnoreCase("null")) {
                schoolObj.setWebsite(jsonObject.getString(WEBSITE));
            }
            if (jsonObject.has(BASE_URL) && !jsonObject.getString(BASE_URL).isEmpty() && !jsonObject.getString(BASE_URL).equalsIgnoreCase("null")) {
                schoolObj.setBaseUrl(jsonObject.getString(BASE_URL));
            }
            if (jsonObject.has(FEE_ONLINE) && !jsonObject.getString(FEE_ONLINE).isEmpty() && !jsonObject.getString(FEE_ONLINE).equalsIgnoreCase("null")) {
                schoolObj.setFeeOnline(jsonObject.getString(FEE_ONLINE));
            }
            if (jsonObject.has(DB_NAME) && !jsonObject.getString(DB_NAME).isEmpty() && !jsonObject.getString(DB_NAME).equalsIgnoreCase("null")) {
                schoolObj.setDbname(jsonObject.getString(DB_NAME));
            }
            if (jsonObject.has(DB_HOST) && !jsonObject.getString(DB_HOST).isEmpty() && !jsonObject.getString(DB_HOST).equalsIgnoreCase("null")) {
                schoolObj.setDbhost(jsonObject.getString(DB_HOST));
            }
            if (jsonObject.has(DB_PASS) && !jsonObject.getString(DB_PASS).isEmpty() && !jsonObject.getString(DB_PASS).equalsIgnoreCase("null")) {
                schoolObj.setDbpass(jsonObject.getString(DB_PASS));
            }
            if (jsonObject.has(DB_USER) && !jsonObject.getString(DB_USER).isEmpty() && !jsonObject.getString(DB_USER).equalsIgnoreCase("null")) {
                schoolObj.setDbuser(jsonObject.getString(DB_USER));
            }

            // Check for 'key' field
//            if (jsonObject.has("key")) {
//                String keyValue = jsonObject.getString("key");
//                DebugLog.printLog("User_details_key_value", "Key value from JSON: " + keyValue);
//                if (keyValue != null && !keyValue.isEmpty() && !keyValue.equalsIgnoreCase("null")) {
//                    schoolObj.setKey(keyValue);
//                }
//            }else{
//                DebugLog.printLog("User_details_key_value", "Key value from JSON: " + "null");
//            }

            // Check for 'fi_school_id' field
            if (jsonObject.has(SCHOOL_ID_KEY) && !jsonObject.getString(SCHOOL_ID_KEY).isEmpty() && !jsonObject.getString(SCHOOL_ID_KEY).equalsIgnoreCase("null")) {
                String schoolIdValue = jsonObject.getString(SCHOOL_ID_KEY);
               // DebugLog.printLog("User_details_school_id_value", "School ID value from JSON: " + schoolIdValue);
                if (schoolIdValue != null && !schoolIdValue.isEmpty() && !schoolIdValue.equalsIgnoreCase("null")) {
                    schoolObj.setFi_school_id(schoolIdValue);
                }
            }else{
                DebugLog.printLog("User_details_school_id_value", "School ID value from JSON: " + "null");
            }


//            if (jsonObject.has(NO_OF_SUBJECT) && !jsonObject.getString(NO_OF_SUBJECT).isEmpty() && !jsonObject.getString(NO_OF_SUBJECT).equalsIgnoreCase("null")) {
//                schoolObj.setFi_no_subjects(jsonObject.getString(NO_OF_SUBJECT));
//            }

            if (jsonObject.has(NO_OF_SUBJECT) && !jsonObject.isNull(NO_OF_SUBJECT)) {
                String markValue = jsonObject.getString(NO_OF_SUBJECT);
                // Check if markValue is not empty or "null" string
                if (markValue != null && !markValue.isEmpty() && !markValue.equalsIgnoreCase("null")) {
                    // Assign markValue to your bean or perform necessary operations
                    schoolObj.setFi_no_subjects(markValue);
                } else {
                    // Handle case where markValue is empty or "null"
                    schoolObj.setFi_no_subjects("0"); // or handle accordingly based on your logic
                }
            } else {
                // Handle case where "mark" field does not exist or is null
                schoolObj.setFi_no_subjects("0"); // or handle accordingly based on your logic
            }

        } catch (JSONException e) {
            e.printStackTrace();
           // DebugLog.printLog("User_details_SCHOOL_ID_KEY_all ",e.getMessage());
        }


        //DebugLog.printLog("User_details_schoolObj ","\nStd_Name"+ schoolObj.toString());
        return schoolObj;
    }

    @Override
    public String toString() {
        return "SchoolBean{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", phoneno='" + phoneno + '\'' +
                ", address='" + address + '\'' +
                ", logo='" + logo + '\'' +
                ", website='" + website + '\'' +
                ", baseUrl='" + baseUrl + '\'' +
                ", dbname='" + dbname + '\'' +
                ", dbhost='" + dbhost + '\'' +
                ", dbpass='" + dbpass + '\'' +
                ", dbuser='" + dbuser + '\'' +
                ", fi_no_subjects='" + fi_no_subjects + '\'' +
                ", key='" + key + '\'' +
                ", fi_school_id='" + fi_school_id + '\'' +
                // Add other fields...
                '}';
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getFeeOnline() {
        return txt_from;
    }

    public void setFeeOnline(String txt_from) {
        this.txt_from = txt_from;
    }
}
