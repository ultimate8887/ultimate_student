package com.ultimate.ultimatesmartstudent.BeanModule;

public class BannerBean {
    /**
     * id : 1
     * title : image1
     * description : image1 description
     * image : hero2.jpg
     */

    private String id;
    private String title;
    private String description;
    /**
     * baner_image : hero2.jpg
     */

    private String baner_image;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBaner_image() {
        return baner_image;
    }

    public void setBaner_image(String baner_image) {
        this.baner_image = baner_image;
    }
}
