package com.ultimate.ultimatesmartstudent.BeanModule;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StudentBean {
    private static String ID = "id";
    /**
     * id : 8
     * firstname : asdsd
     * lastname : asdsd
     * image :
     * class_id : 2
     * father_name : asdsd
     */

    private String id;
    private String firstname;
    private String lastname;
    private String image;
    private String class_name;
    private String father_name;
    private String rollserialno;

    public String getRollserialno() {
        return rollserialno;
    }

    public void setRollserialno(String rollserialno) {
        this.rollserialno = rollserialno;
    }

    /**

     * last_paid : 2018-02-19
     */

    private String last_paid;
    /**
     * class_id : 2
     */

    private String class_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public static ArrayList<StudentBean> parseStudentArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<StudentBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                StudentBean p = parseStudentObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static StudentBean parseStudentObject(JSONObject jsonObject) {
        StudentBean casteObj = new StudentBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(Constants.FIRST_NAME) && !jsonObject.getString(Constants.FIRST_NAME).isEmpty() && !jsonObject.getString(Constants.FIRST_NAME).equalsIgnoreCase("null")) {
                casteObj.setFirstname(jsonObject.getString(Constants.FIRST_NAME));
            }
            if (jsonObject.has(Constants.LAST_NAME) && !jsonObject.getString(Constants.LAST_NAME).isEmpty() && !jsonObject.getString(Constants.LAST_NAME).equalsIgnoreCase("null")) {
                casteObj.setLastname(jsonObject.getString(Constants.LAST_NAME));
            }

            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }
            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }

            if (jsonObject.has("father_name") && !jsonObject.getString("father_name").isEmpty() && !jsonObject.getString("father_name").equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString("father_name"));
            }

            if (jsonObject.has("image") && !jsonObject.getString("image").isEmpty() && !jsonObject.getString("image").equalsIgnoreCase("null")) {
                casteObj.setImage(jsonObject.getString("image"));
            }
            if (jsonObject.has("last_paid") && !jsonObject.getString("last_paid").isEmpty() && !jsonObject.getString("last_paid").equalsIgnoreCase("null")) {
                casteObj.setLast_paid(jsonObject.getString("last_paid"));
            }

            if (jsonObject.has("rollserialno") && !jsonObject.getString("rollserialno").isEmpty() && !jsonObject.getString("rollserialno").equalsIgnoreCase("null")) {
                casteObj.setRollserialno(jsonObject.getString("rollserialno"));
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return casteObj;
    }

    public String getLast_paid() {
        return last_paid;
    }

    public void setLast_paid(String last_paid) {
        this.last_paid = last_paid;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }
}
