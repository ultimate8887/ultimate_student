package com.ultimate.ultimatesmartstudent.BeanModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SessionBean {

    private static String ID = "id";
    private static String START_DATE = "start_date";
    private static String END_DATE = "end_date";
    /**
     * start_date : 2015-04-01
     * id : 1
     * end_date : 2016-03-31
     */

    private String start_date;
    private String id;
    private String end_date;

    public static ArrayList<SessionBean> parseSessionArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<SessionBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                SessionBean p = parseSessionObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static SessionBean parseSessionObject(JSONObject jsonObject) {
        SessionBean casteObj = new SessionBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(START_DATE) && !jsonObject.getString(START_DATE).isEmpty() && !jsonObject.getString(START_DATE).equalsIgnoreCase("null")) {
                casteObj.setStart_date(jsonObject.getString(START_DATE));
            }
            if (jsonObject.has(END_DATE) && !jsonObject.getString(END_DATE).isEmpty() && !jsonObject.getString(END_DATE).equalsIgnoreCase("null")) {
                casteObj.setEnd_date(jsonObject.getString(END_DATE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}
