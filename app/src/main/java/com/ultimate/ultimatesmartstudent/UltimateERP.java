package com.ultimate.ultimatesmartstudent;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.multidex.MultiDexApplication;

//import com.crashlytics.android.Crashlytics;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

//import io.fabric.sdk.android.Fabric;

public class UltimateERP extends MultiDexApplication {

    private static UltimateERP mInstance;
    private static final String TAG = UltimateERP.class.getName();
    private SharedPreferences preferences;
    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        mInstance = this;
        preferences = this.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

    }

    public static synchronized UltimateERP getInstance() {
        return mInstance;
    }

    @Override
    public void onTerminate() {
//        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(getApplicationContext(), AlarmBroadCastReceiver.class);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        if (Build.VERSION.SDK_INT >= 23)
//        {
//            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 2, pendingIntent);
//        }
//        else if (Build.VERSION.SDK_INT >= 19)
//        {
//            alarmManager.setExact(AlarmManager.RTC_WAKEUP, 2, pendingIntent);
//        }
//        else
//        {
//            alarmManager.set(AlarmManager.RTC_WAKEUP, 2, pendingIntent);
//        }
        mInstance = null;
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        Utils.deleteCache(this);
        super.onLowMemory();
    }
    public void setNotificationStatus(boolean isNotification){
        preferences.edit().putBoolean(Constants.NOTIFICATION_STATUS,isNotification).commit();
    }
    public boolean getNotificationStatus(){
        return preferences.getBoolean(Constants.NOTIFICATION_STATUS,true);
    }

}
