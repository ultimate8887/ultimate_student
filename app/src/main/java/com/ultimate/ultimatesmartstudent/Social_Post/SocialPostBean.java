package com.ultimate.ultimatesmartstudent.Social_Post;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SocialPostBean {

    private String id;
    private String title;
    private String post;
    private String post_type;
    private String date;

//    Now there is implementing all the ApiHandler working.............................

    public static ArrayList<SocialPostBean> parseNoticetArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<SocialPostBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                SocialPostBean p = parseNoticeObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static SocialPostBean parseNoticeObject(JSONObject jsonObject) {
        SocialPostBean casteObj = new SocialPostBean();
        try {

            if (jsonObject.has("id") && !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }

            if (jsonObject.has("title") && !jsonObject.getString("title").isEmpty() && !jsonObject.getString("title").equalsIgnoreCase("null")) {
                casteObj.setTitle(jsonObject.getString("title"));
            }

            if (jsonObject.has("post") && !jsonObject.getString("post").isEmpty() && !jsonObject.getString("post").equalsIgnoreCase("null")) {
                casteObj.setPost(jsonObject.getString("post"));
            }
            if (jsonObject.has("post_type") && !jsonObject.getString("post_type").isEmpty() && !jsonObject.getString("post_type").equalsIgnoreCase("null")) {
                casteObj.setPost_type(jsonObject.getString("post_type"));
            }

            if (jsonObject.has("date") && !jsonObject.getString("date").isEmpty() && !jsonObject.getString("date").equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString("date"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}