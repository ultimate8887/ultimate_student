package com.ultimate.ultimatesmartstudent.Social_Post;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Gallery.GDSGalleryAct;
import com.ultimate.ultimatesmartstudent.Leave_Mod.OptionAdapter;
import com.ultimate.ultimatesmartstudent.Leave_Mod.OptionBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SocialLinkList extends AppCompatActivity implements SocialPostAdapter.StudentPro {

    ArrayList<SocialPostBean> datalist = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.textNorecord)
    TextView txtNoData;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    SharedPreferences sharedPreferences;

    private RequestQueue queue;
    private String url;
    private SocialPostAdapter adapter1;
    int loaded = 0;
    Dialog mBottomSheetDialog;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.today_date)
    TextView cal_text;

    String from_date = "";

    CommonProgress commonProgress;

    @BindView(R.id.textView8)
    TextView textView8;

    ArrayList<OptionBean> vehicleList = new ArrayList<>();

    @BindView(R.id.spinnerGroups)
    Spinner vehicleType;
    String tag="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_link_list);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.social)+" "+getString(R.string.list));
        //txtSub.setText(Utility.setNaviHeaderClassData());
        // adepter set here
        commonProgress = new CommonProgress(this);
        layoutManager = new LinearLayoutManager(SocialLinkList.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter1 = new SocialPostAdapter(datalist, SocialLinkList.this, this);
        recyclerView.setAdapter(adapter1);
        textView8.setText(getString(R.string.f_leave));
        //vehicleList.add(new OptionBean("All"));
        vehicleList.add(new OptionBean("Class"));

        OptionAdapter dataAdapter = new OptionAdapter(SocialLinkList.this, vehicleList);
        vehicleType.setAdapter(dataAdapter);
        //attach the listener to the spinner
        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    tag = vehicleList.get(i - 1).getName();
                }else {
                    tag="";
                }
                from_date = "";
                cal_text.setText("Select date");
                fetchNoticeBoard(tag);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @OnClick(R.id.cal_lyttttt)
    public void cal_lyttttt() {

        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchNoticeBoard(tag);
        }
    };


    private void fetchNoticeBoard(String tag) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("today", from_date);
        params.put("view_type", "post");
        params.put("tag",tag);
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("section_id",User.getCurrentUser().getSection_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.POST, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {

                    try {
                        JSONArray noticeArray = jsonObject.getJSONArray("hw_data");
                        datalist = SocialPostBean.parseNoticetArray(noticeArray);

                        if (datalist.size() > 0) {
                            adapter1.setHList(datalist);
                            adapter1.notifyDataSetChanged();
                            txtNoData.setVisibility(View.GONE);

                            totalRecord.setVisibility(View.VISIBLE);
                            totalRecord.setText("Total Entries:- " + String.valueOf(datalist.size()));

                        } else {

                            totalRecord.setText("Total Entries:- 0");
                            txtNoData.setVisibility(View.VISIBLE);
                            adapter1.setHList(datalist);
                            adapter1.notifyDataSetChanged();
                        }
                        //   main_progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    totalRecord.setText("Total Entries:- 0");
                    txtNoData.setVisibility(View.VISIBLE);
                    datalist.clear();
                    adapter1.setHList(datalist);
                    adapter1.notifyDataSetChanged();
                }


            }
        }, this, params);

    }


    @OnClick(R.id.imgBack)
    public void imgBack() {
        // Animatoo.animateZoom(ViewNoticeInfo.this);
        finish();
    }



    @Override
    public void onDelecallback(SocialPostBean std) {

    }
}