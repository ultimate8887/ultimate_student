package com.ultimate.ultimatesmartstudent.Health_Module;

import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HealthBean {
    private static String ID="id";
    private static String COMMENT="es_remarks";
    private static String H_DATE="date";
    /**
     * id : 2
     * comment : asfgsdfgdgegdfgdfgfg
     * image : office_admin/images/homework/hw_03272018_080342.jpg
     * date : 2018-03-27
     * class : 10
     */

    private String id;
    private String comment;
    private String date;
    private String section;
    private String section_name;

    public String getEs_regid() {
        return es_regid;
    }

    public void setEs_regid(String es_regid) {
        this.es_regid = es_regid;
    }

    public String getEs_sname() {
        return es_sname;
    }

    public void setEs_sname(String es_sname) {
        this.es_sname = es_sname;
    }

    public String getEs_fname() {
        return es_fname;
    }

    public void setEs_fname(String es_fname) {
        this.es_fname = es_fname;
    }

    public String getEs_allergy() {
        return es_allergy;
    }

    public void setEs_allergy(String es_allergy) {
        this.es_allergy = es_allergy;
    }

    public String getEs_disease() {
        return es_disease;
    }

    public void setEs_disease(String es_disease) {
        this.es_disease = es_disease;
    }

    public String getEs_medicine() {
        return es_medicine;
    }

    public void setEs_medicine(String es_medicine) {
        this.es_medicine = es_medicine;
    }

    public String getEs_medicine_time() {
        return es_medicine_time;
    }

    public void setEs_medicine_time(String es_medicine_time) {
        this.es_medicine_time = es_medicine_time;
    }

    public String getEs_expirydate() {
        return es_expirydate;
    }

    public void setEs_expirydate(String es_expirydate) {
        this.es_expirydate = es_expirydate;
    }

    public String getEs_medical_issue() {
        return es_medical_issue;
    }

    public void setEs_medical_issue(String es_medical_issue) {
        this.es_medical_issue = es_medical_issue;
    }

    private String es_regid;
    private String es_sname;
    private String es_fname;
    private String es_allergy;
    private String es_disease;
    private String es_medicine;

    private String es_medicine_time;
    private String es_expirydate;
    private String es_medical_issue;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    private String profile;

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getStaff_image() {
        return staff_image;
    }

    public void setStaff_image(String staff_image) {
        this.staff_image = staff_image;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    private String staff_name;
    private String staff_image;
    private String post_name;


    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }



    /**
     * class_id : 10
     * class_name : 6TH
     */

    private String class_id;
    private String class_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public static ArrayList<HealthBean> parseHWArray(JSONArray arrayObj) {
        ArrayList<HealthBean> list = new ArrayList<HealthBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                HealthBean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static HealthBean parseHWObject(JSONObject jsonObject) {
        HealthBean casteObj = new HealthBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(COMMENT) && !jsonObject.getString(COMMENT).isEmpty() && !jsonObject.getString(COMMENT).equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString(COMMENT));
            }
            if (jsonObject.has(H_DATE) && !jsonObject.getString(H_DATE).isEmpty() && !jsonObject.getString(H_DATE).equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString(H_DATE));
            }

            if (jsonObject.has("es_regid") && !jsonObject.getString("es_regid").isEmpty() && !jsonObject.getString("es_regid").equalsIgnoreCase("null")) {
                casteObj.setEs_regid(jsonObject.getString("es_regid"));
            }
            if (jsonObject.has("es_sname") && !jsonObject.getString("es_sname").isEmpty() && !jsonObject.getString("es_sname").equalsIgnoreCase("null")) {
                casteObj.setEs_sname(jsonObject.getString("es_sname"));
            }
            if (jsonObject.has("es_fname") && !jsonObject.getString("es_fname").isEmpty() && !jsonObject.getString("es_fname").equalsIgnoreCase("null")) {
                casteObj.setEs_fname(jsonObject.getString("es_fname"));
            }
            if (jsonObject.has("rowcount") && !jsonObject.getString("rowcount").isEmpty() && !jsonObject.getString("rowcount").equalsIgnoreCase("null")) {
                casteObj.setRowcount(jsonObject.getString("rowcount"));
            }

            if (jsonObject.has("section") && !jsonObject.getString("section").isEmpty() && !jsonObject.getString("section").equalsIgnoreCase("null")) {
                casteObj.setSection(jsonObject.getString("section"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }

            if (jsonObject.has("profile") && !jsonObject.getString("profile").isEmpty() && !jsonObject.getString("profile").equalsIgnoreCase("null")) {
                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString("profile"));
            }
            if (jsonObject.has("es_allergy") && !jsonObject.getString("es_allergy").isEmpty() && !jsonObject.getString("es_allergy").equalsIgnoreCase("null")) {
                casteObj.setEs_allergy(jsonObject.getString("es_allergy"));
            }

            if (jsonObject.has("es_disease") && !jsonObject.getString("es_disease").isEmpty() && !jsonObject.getString("es_disease").equalsIgnoreCase("null")) {
                casteObj.setEs_disease(jsonObject.getString("es_disease"));
            }

            if (jsonObject.has("es_medicine") && !jsonObject.getString("es_medicine").isEmpty() && !jsonObject.getString("es_medicine").equalsIgnoreCase("null")) {
                casteObj.setEs_medicine(jsonObject.getString("es_medicine"));
            }
            if (jsonObject.has("es_medicine_time") && !jsonObject.getString("es_medicine_time").isEmpty() && !jsonObject.getString("es_medicine_time").equalsIgnoreCase("null")) {
                casteObj.setEs_medicine_time(jsonObject.getString("es_medicine_time"));
            }
            if (jsonObject.has("es_expirydate") && !jsonObject.getString("es_expirydate").isEmpty() && !jsonObject.getString("es_expirydate").equalsIgnoreCase("null")) {
                casteObj.setEs_expirydate(jsonObject.getString("es_expirydate"));
            }
            if (jsonObject.has("es_medical_issue") && !jsonObject.getString("es_medical_issue").isEmpty() && !jsonObject.getString("es_medical_issue").equalsIgnoreCase("null")) {
                casteObj.setEs_medical_issue(jsonObject.getString("es_medical_issue"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
