package com.ultimate.ultimatesmartstudent.Health_Module;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class HealthAdapter extends RecyclerView.Adapter<HealthAdapter.Viewholder> {
    public ArrayList<HealthBean> stulist;
    Context mContext;
    int value;

    public HealthAdapter(ArrayList<HealthBean> stulist, Context mContext) {
        this.mContext = mContext;
        this.stulist = stulist;
        this.value = value;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_health, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Viewholder viewHolder, @SuppressLint("RecyclerView") final int position) {

//        viewHolder.circleimg.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_transition_animation));
//        viewHolder.parent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));
        HealthBean mData=stulist.get(position);

        if (stulist.get(position).getProfile() != null) {
            Picasso.get().load(stulist.get(position).getProfile()).placeholder(R.color.transparent).into(viewHolder.circleimg);
        } else {
            Picasso.get().load(R.color.transparent).into(viewHolder.circleimg);
        }

        //for Today
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());


//        if (mData.getDate() != null) {
//            String title = getColoredSpanned("", "#000000");
//            String Name = getColoredSpanned(Utils.getDateTimeFormatedWithAMPM(mData.getDate()), "#5A5C59");
//            viewHolder.txtid1.setText(Html.fromHtml(title + " " + Name));
//        }

        String check= Utils.getDateFormated(stulist.get(position).getDate());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            viewHolder.txtid1.setText(Html.fromHtml(title + " " + l_Name));
            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){
            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            viewHolder.txtid1.setText(Html.fromHtml(title + " " + l_Name));
            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            viewHolder.txtid1.setText(Utils.getDateTimeFormatedWithAMPM(stulist.get(position).getDate()));
        }


        if (mData.getClass_name() != null) {
            String title = getColoredSpanned("Class & Sec: ", "#000000");
            String Name = getColoredSpanned(mData.getClass_name()+"-"+mData.getSection_name(), "#5A5C59");
            viewHolder.txtid.setText(Html.fromHtml(title + " " + Name));
        }

        // holder.employee_name.setText("Name: " + " " + viewstafflist.get(position).getName());
        if (mData.getEs_sname() != null) {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_sname()+"("+mData.getEs_regid()+")", "#5A5C59");
            viewHolder.textViewData.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.textViewData.setText("Name: Not Mentioned");
        }

        //  holder.post.setText("Post:" + " " + viewstafflist.get(position).getPost());
        if (mData.getEs_fname() != null) {
            String title = getColoredSpanned("Father Name: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_fname(), "#5A5C59");
            viewHolder.txtFather.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.txtFather.setText("Not Mentioned");
        }

        // holder.employee_doj.setText("Date of Joining: " + " " + viewstafflist.get(position).getDateofjoining());
        if (mData.getEs_allergy() != null) {
            String title = getColoredSpanned("Type of allergy: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_allergy(), "#5A5C59");
            viewHolder.txtClass.setText(Html.fromHtml(title + " " + Name));
        } else {
            viewHolder.txtClass.setText("Not Mentioned");
        }

        if (mData.getEs_disease() != null) {
            String title = getColoredSpanned("Type of disease: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_disease(), "#5A5C59");
            viewHolder.txtMobile.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.txtMobile.setText("Not Mentioned");
        }

        if (mData.getEs_medicine() != null) {
            String title = getColoredSpanned("Medicine submitted in school: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_medicine(), "#5A5C59");
            viewHolder.mor_img.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.mor_img.setText("Not Mentioned");
        }

        if (mData.getEs_medicine_time() != null) {
            String title = getColoredSpanned("How many time take medicine: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_medicine_time(), "#5A5C59");
            viewHolder.txtMobile1.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.txtMobile1.setText("Not Mentioned");
        }

//        if (mData.getEs_expirydate() != null) {
//            String title = getColoredSpanned("Expire date of medicine: ", "#000000");
//            String Name = getColoredSpanned(Utils.getDateFormated(mData.getEs_expirydate()), "#5A5C59");
//            viewHolder.evn_img.setText(Html.fromHtml(title + " " + Name));
//        }else {
//            viewHolder.evn_img.setText("Not Mentioned");
//        }

        String check1= Utils.getDateFormated(stulist.get(position).getEs_expirydate());
        if (check1.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Expire date of medicine: ", "#OOOOOO");
            viewHolder.evn_img.setText(Html.fromHtml(l_Name + " " + title));
            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check1.equalsIgnoreCase(dateString1)){
            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Expire date of medicine: ", "#OOOOOO");
            viewHolder.evn_img.setText(Html.fromHtml(l_Name + " " + title));
            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            String title = getColoredSpanned("Expire date of medicine: ", "#000000");
            String Name = getColoredSpanned(Utils.getDateFormated(mData.getEs_expirydate()), "#5A5C59");
            viewHolder.evn_img.setText(Html.fromHtml(title + " " + Name));
        }


        if (mData.getEs_medical_issue() != null) {
            String title = getColoredSpanned("Is student take part in games: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_medical_issue(), "#5A5C59");
            viewHolder.evn_img1.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.evn_img1.setText("Not Mentioned");
        }

        if (mData.getComment() != null) {
            String title = getColoredSpanned("Remarks: ", "#000000");
            String Name = getColoredSpanned(mData.getComment(), "#5A5C59");
            viewHolder.evn_img2.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.evn_img2.setText("Not Mentioned");
        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface StudentPro{
        public void StudentProfile(HealthBean std);
        public void onUpdateCallback(HealthBean std);

        public void onDelecallback(HealthBean std);

    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }



    public void setHQList(ArrayList<HealthBean> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        //        SwipeLayout swipeLayout;
        TextView textViewData, txtMobile,evn_img,mor_img,txtMobile1, txtFather, txtClass,txtid,txtid1,status,evn_img1,evn_img2;
        ImageView trash,imgOfDialog,update;
        // CardView update;
        ImageView arrow;
        RelativeLayout parent,root;
        CircularImageView circleimg;


        public Viewholder(View itemView) {
            super(itemView);
            root =(RelativeLayout)itemView.findViewById(R.id.root);
            parent =(RelativeLayout)itemView.findViewById(R.id.parent);
            circleimg=(CircularImageView)itemView.findViewById(R.id.circleimg);
            textViewData = (TextView) itemView.findViewById(R.id.name);
            txtFather = (TextView) itemView.findViewById(R.id.fathername);
            txtMobile = (TextView) itemView.findViewById(R.id.mobileno);
            txtMobile1 = (TextView) itemView.findViewById(R.id.mobileno1);
            mor_img = (TextView) itemView.findViewById(R.id.mor_img);
            evn_img = (TextView) itemView.findViewById(R.id.evn_img);
            evn_img1 = (TextView) itemView.findViewById(R.id.evn_img1);
            evn_img2 = (TextView) itemView.findViewById(R.id.evn_img2);
            txtClass = (TextView) itemView.findViewById(R.id.classess);
            status = (TextView) itemView.findViewById(R.id.status);

            imgOfDialog = (ImageView) itemView.findViewById(R.id.imgOfDialog);
            txtid=(TextView)itemView.findViewById(R.id.textViewid);
            txtid1=(TextView)itemView.findViewById(R.id.textViewid1);
            arrow = (ImageView) itemView.findViewById(R.id.arrow);
            arrow.setVisibility(View.GONE);
        }
    }
}
