package com.ultimate.ultimatesmartstudent.Health_Module;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Datesheet.DatesheetAdapter;
import com.ultimate.ultimatesmartstudent.Datesheet.DatesheetBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class View_Health extends AppCompatActivity {
    public TextView textView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    ArrayList<HealthBean> leavelist = new ArrayList<>();
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    private HealthAdapter adapter;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    Animation animation;
    String textholder="",school="";
    CommonProgress commonProgress;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.txtSub)
    TextView txtSub;
    RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_new);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        txtSub.setText(Utils.setNaviHeaderClassData());
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new HealthAdapter(leavelist, this);
        recyclerView.setAdapter(adapter);
        txtTitle.setText(getString(R.string.health)+" "+getString(R.string.list));
        fetchleavelist();
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBack() {
        // Animatoo.animateZoom(ViewNoticeInfo.this);
        finish();
    }

    private void fetchleavelist() {
        HashMap<String, String> params = new HashMap<>();
        params.put("class_id", User.getCurrentUser().getId());
        params.put("view_type", "class");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.HEALTH,apicallback, this, params);

    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (leavelist != null) {
                        leavelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hw_data");
                    leavelist = HealthBean.parseHWArray(jsonArray);
                    if (leavelist.size() > 0) {
                        adapter.setHQList(leavelist);
                        totalRecord.setText("Total Entries:- "+String.valueOf(leavelist.size()));
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setHQList(leavelist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                leavelist.clear();
                adapter.setHQList(leavelist);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
               // Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

}