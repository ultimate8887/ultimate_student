package com.ultimate.ultimatesmartstudent.Holiday;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.HolidayMod.HolidayBean;
import com.ultimate.ultimatesmartstudent.HolidayMod.Holidayadapter;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HolidayActivity extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.textNorecord)
    TextView textNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtSub)
    TextView txtSub;
    private Holidayadapter adapter;
    ArrayList<HolidayBean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    CommonProgress commonProgress;
    String h_date="";
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_new);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(this);
        //txtTitle.setText("Holiday List");
        txtSub.setText(Utils.setNaviHeaderClassData());
        txtTitle.setText(getString(R.string.holiday_h)+" "+getString(R.string.list));
        layoutManager=new LinearLayoutManager(HolidayActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter=new Holidayadapter(hwList, HolidayActivity.this,1);
        recyclerView.setAdapter(adapter);
        fetchalbumlist();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    public void fetchalbumlist(){
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "full");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.HOLIDAY_URL, albmapiCallback, this, params);

    }

    ApiHandler.ApiCallback albmapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray hArray = jsonObject.getJSONArray("holiday_data");
                    hwList = HolidayBean.parseArray(hArray);
                    if (hwList.size() > 0) {
                        adapter.setHList(hwList);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    } else {
                        adapter.setHList(hwList);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- 0");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                textNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHList(hwList);
                totalRecord.setText("Total Entries:- 0");
                adapter.notifyDataSetChanged();
            }
        }
    };



    @OnClick(R.id.imgBackmsg)
    public void onback() {
        finish();
    }
}