package com.ultimate.ultimatesmartstudent.E_book;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Webview.FileDownloader;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Ebook extends AppCompatActivity implements EbookAdapter.Mycallback {


    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;

    @BindView(R.id.textView8)
    TextView textsubtitle;
    @BindView(R.id.spinnerGroups)
    Spinner SubjectSpinner;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;

    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    String  sub_id, sub_name;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;
    String folder_main = "Ebook";
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<EbookBean> ebooklist = new ArrayList<>();
    EbookAdapter adapter;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.ebook));
        commonProgress=new CommonProgress(this);
        textsubtitle.setText(getString(R.string.f_subject));
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerView.setLayoutManager(new LinearLayoutManager(Ebook.this));
        adapter = new EbookAdapter(ebooklist, Ebook.this, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //if (class_id != null && !class_id.isEmpty())
        getSubjectofClass();
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }


    private void getSubjectofClass() {
        //  UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);
    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));
                    AdapterOfSubjectList adaptersub = new AdapterOfSubjectList(Ebook.this, subjectList);
                    SubjectSpinner.setAdapter(adaptersub);
                    SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }
                            fetchSubWise(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void fetchSubWise(int i) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        if (i == 0){
            params.put("class_id", User.getCurrentUser().getClass_id());
        } else {
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("sub_id",sub_id);
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.get_EBOOK, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        if (ebooklist != null) {
                            ebooklist.clear();
                        }
                        JSONArray jsonArray = jsonObject.getJSONArray("syllabus_data");
                        ebooklist = EbookBean.parseSyllabusArray(jsonArray);
                        if (ebooklist.size() > 0) {
                            adapter.setSyllabusList(ebooklist);
                            //setanimation on adapter...
                            recyclerView.getAdapter().notifyDataSetChanged();
                            recyclerView.scheduleLayoutAnimation();
                            //-----------end------------
                            txtNorecord.setVisibility(View.GONE);
                            totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(ebooklist.size()));
                        } else {
                            totalRecord.setText(getString(R.string.t_entries)+" 0");
                            adapter.setSyllabusList(ebooklist);
                            adapter.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText(getString(R.string.t_entries)+" 0");
                    txtNorecord.setVisibility(View.VISIBLE);
                    ebooklist.clear();
                    adapter.setSyllabusList(ebooklist);
                    adapter.notifyDataSetChanged();
                }
            }
        }, this, params);

    }

    @Override
    public void onMethodCallback(EbookBean syllabusBean) {
        //  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(syllabusBean.getS_file())));


        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(Ebook.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(Ebook.this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(syllabusBean.getSub_name()+"("+syllabusBean.getEs_title()+")");
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.ebook).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        sub_wise.setText(getString(R.string.view));
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getBaseContext(), ViewPdfActivity.class);
                intent.putExtra("id", syllabusBean.getS_file());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setText(getString(R.string.save));
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
              //  UltimateProgress.showProgressBar(Ebook.this,"Please wait...");
                String mUrl=syllabusBean.getS_file();
                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("Eb_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Eb_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getApplicationContext(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();

    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            File path = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), folder_main);
            path.mkdir();
            File file = new File(path, folder_main + System.currentTimeMillis() + "saved.pdf");
            try{
                file.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, file);
            UltimateProgress.cancelProgressBar();
            return null;
        }
    }

    @Override
    public void viewPdf(EbookBean syllabusBean) {

    }
}
