package com.ultimate.ultimatesmartstudent.E_book;

import android.util.Log;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EbookBean {
    private String es_subid;
    private String es_title;
    private String s_file;
    private String id;
    private String es_writer;
    private String es_class;
    private String classname;
    private String sub_name;

    public String getEs_date() {
        return es_date;
    }

    public void setEs_date(String es_date) {
        this.es_date = es_date;
    }

    private String es_date;


    public String getEs_subid() {
        return es_subid;
    }

    public void setEs_subid(String es_subid) {
        this.es_subid = es_subid;
    }


    public String getEs_title() {
        return es_title;
    }

    public void setEs_title(String es_title) {
        this.es_title = es_title;
    }

    public String getS_file() {
        return s_file;
    }

    public void setS_file(String s_file) {
        this.s_file = s_file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEs_writer() {
        return es_writer;
    }

    public void setEs_writer(String es_writer) {
        this.es_writer = es_writer;
    }

    public String getEs_class() {
        return es_class;
    }

    public void setEs_class(String es_class) {
        this.es_class = es_class;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public static ArrayList<EbookBean> parseSyllabusArray(JSONArray arrayObj) {
        ArrayList<EbookBean> list = new ArrayList<EbookBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                EbookBean p = parseSyllabusObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static EbookBean parseSyllabusObject(JSONObject jsonObject) {
        EbookBean casteObj = new EbookBean();
        try {
            if (jsonObject.has("id")&& !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("es_subid") && !jsonObject.getString("es_subid").isEmpty() && !jsonObject.getString("es_subid").equalsIgnoreCase("null")) {
                casteObj.setEs_subid(jsonObject.getString("es_subid"));
            }
            if (jsonObject.has("s_file") && !jsonObject.getString("s_file").isEmpty() && !jsonObject.getString("s_file").equalsIgnoreCase("null")) {
                casteObj.setS_file(Constants.getImageBaseURL()+jsonObject.getString("s_file"));
            }

            if (jsonObject.has("es_title") && !jsonObject.getString("es_title").isEmpty() && !jsonObject.getString("es_title").equalsIgnoreCase("null")) {
                casteObj.setEs_title(jsonObject.getString("es_title"));
            }
            if (jsonObject.has("es_class") && !jsonObject.getString("es_class").isEmpty() && !jsonObject.getString("es_class").equalsIgnoreCase("null")) {
                casteObj.setEs_class(jsonObject.getString("es_class"));
            }
            if (jsonObject.has("classname") && !jsonObject.getString("classname").isEmpty() && !jsonObject.getString("classname").equalsIgnoreCase("null")) {
                casteObj.setClassname(jsonObject.getString("classname"));
            }
            // Handle sub_name field
            if (jsonObject.has("sub_name")) {
                Object subNameObj = jsonObject.get("sub_name");
                if (subNameObj instanceof String) {
                    casteObj.setSub_name((String) subNameObj);
                } else {
                    Log.e("parseNoticeObject", "sub_name is not a string, it is: " + subNameObj.getClass().getSimpleName());
                }
            }
            if (jsonObject.has("es_date") && !jsonObject.getString("es_date").isEmpty() && !jsonObject.getString("es_date").equalsIgnoreCase("null")) {
                casteObj.setEs_date(jsonObject.getString("es_date"));
            }
            if (jsonObject.has("es_writer") && !jsonObject.getString("es_writer").isEmpty() && !jsonObject.getString("es_writer").equalsIgnoreCase("null")) {
                casteObj.setEs_writer(jsonObject.getString("es_writer"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
