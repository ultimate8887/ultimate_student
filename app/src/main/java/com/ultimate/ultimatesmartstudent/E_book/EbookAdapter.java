package com.ultimate.ultimatesmartstudent.E_book;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EbookAdapter  extends RecyclerView.Adapter<EbookAdapter.Viewholder> {
    ArrayList<EbookBean> sy_list;
    Mycallback mAdaptercall;
    Context mContext;
    Animation animation;

    public EbookAdapter(ArrayList<EbookBean> sy_list, Context mContext, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public EbookAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ebooka_adpt_lyt, parent, false);
        EbookAdapter.Viewholder viewholder = new EbookAdapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(EbookAdapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
        if (sy_list.get(position).getSub_name() != null){
            holder.classess.setText(sy_list.get(position).getSub_name()+"-"+sy_list.get(position).getClassname());
        }else{
            holder.classess.setText("Not Mentioned");
        }

        if (sy_list.get(position).getEs_writer() != null){
            String title = getColoredSpanned("Writer-", "#000000");
            String Name="";
            Name = getColoredSpanned(sy_list.get(position).getEs_writer(), "#5A5C59");
            holder.homeTopic.setText(Html.fromHtml(title + " " + Name));
        }else{
            holder.homeTopic.setText("Not Mentioned");
        }

        if (sy_list.get(position).getEs_title() != null) {
            String title = getColoredSpanned("Title-", "#000000");
            String Name="";
            Name = getColoredSpanned(sy_list.get(position).getEs_title(), "#5A5C59");
            holder.sub.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.sub.setVisibility(View.GONE);
        }

        if (sy_list.get(position).getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("eb-"+sy_list.get(position).getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());



        String check= Utils.getDateFormated(sy_list.get(position).getEs_date());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.u_date.setText(Utils.getDateFormated(sy_list.get(position).getEs_date()));
        }
        if (sy_list.get(position).getS_file() != null){
            holder.withfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tap_view_pdf.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethodCallback(sy_list.get(position));
                    }
                }
            });
        }else {
            holder.pdf_file.setVisibility(View.GONE);
        }

    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface Mycallback {
        public void onMethodCallback(EbookBean syllabusBean);

        public void viewPdf(EbookBean syllabusBean);
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<EbookBean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView u_date,homeTopic,sub,tap_view_pdf;
        public ImageView  pdf_file;
        public RelativeLayout withfile;
        @BindView(R.id.classess)
        TextView classess;
        @BindView(R.id.id)
        TextView id;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tap_view_pdf = (TextView) itemView.findViewById(R.id.tap_view_pdf);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);
        }
    }
}
