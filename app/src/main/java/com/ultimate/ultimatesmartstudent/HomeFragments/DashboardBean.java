package com.ultimate.ultimatesmartstudent.HomeFragments;

import android.graphics.drawable.Drawable;

public class DashboardBean {

    private String title;
    private Drawable mIcon;
    private String mColor;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getMIcon() {
        return mIcon;
    }

    public void setMIcon(Drawable mIcon) {
        this.mIcon = mIcon;
    }

    public String getTxtColor() {
        return mColor;
    }

    public void setTxtColor(String mColor) {
        this.mColor = mColor;
    }
}
