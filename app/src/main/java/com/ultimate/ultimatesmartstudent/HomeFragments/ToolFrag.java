package com.ultimate.ultimatesmartstudent.HomeFragments;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.floatingnavigationview.FloatingNavigationView;
import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartstudent.AccountModule.AccountActivity;
import com.ultimate.ultimatesmartstudent.AccountModule.ImgPdfConvert.ImageToPdfConvert;
import com.ultimate.ultimatesmartstudent.AccountModule.SchoolInfo;
import com.ultimate.ultimatesmartstudent.AddDocument.AdddocumentActivity;
import com.ultimate.ultimatesmartstudent.Assignment.MainAssignActivity;
import com.ultimate.ultimatesmartstudent.AttendMod.StudentGateAttendance;
import com.ultimate.ultimatesmartstudent.E_LearningMod.Live_Class_Bean;
import com.ultimate.ultimatesmartstudent.E_LearningMod.VideoActivity;
import com.ultimate.ultimatesmartstudent.AttendMod.AttendanceActivity;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.GDSClasswork;
import com.ultimate.ultimatesmartstudent.Datesheet.Datesheet;
import com.ultimate.ultimatesmartstudent.DirtChart.DietChartActivity;
import com.ultimate.ultimatesmartstudent.E_book.Ebook;
import com.ultimate.ultimatesmartstudent.EventMod.AchievementList;
import com.ultimate.ultimatesmartstudent.EventMod.Events_List;
import com.ultimate.ultimatesmartstudent.EventMod.ParticipationListStud;
import com.ultimate.ultimatesmartstudent.Examination.Activity.ExamWebviewActivity;
import com.ultimate.ultimatesmartstudent.Examination.Activity.Exam_Detail_Result_Activity;
import com.ultimate.ultimatesmartstudent.Examination.Activity.ReportCard;
import com.ultimate.ultimatesmartstudent.Examination.Activity.ReportCardNew;
import com.ultimate.ultimatesmartstudent.FeeModule.Fees_Fragments.AddBankDetails;
import com.ultimate.ultimatesmartstudent.FeeModule.Fees_Fragments.PayFee_New;
import com.ultimate.ultimatesmartstudent.FeeModule.Fees_Fragments.PayFee_Online;
import com.ultimate.ultimatesmartstudent.FeeModule.NewFees.CommonFeesListActivity;
import com.ultimate.ultimatesmartstudent.FeeModule.NewFees.CommonSTDPendingFeesListActivity;
import com.ultimate.ultimatesmartstudent.Gallery.GDSGalleryAct;
import com.ultimate.ultimatesmartstudent.Gallery.VideoGalleryActivity;
import com.ultimate.ultimatesmartstudent.GatePass.AddGatePass;
import com.ultimate.ultimatesmartstudent.GatePass.AddGatePersonDetails;
import com.ultimate.ultimatesmartstudent.GatePass.NewGatePass.GatePass;
import com.ultimate.ultimatesmartstudent.GatePass.NewGatePass.NewGatePassActivity;
import com.ultimate.ultimatesmartstudent.Health_Module.View_Health;
import com.ultimate.ultimatesmartstudent.Holiday.HolidayActivity;
import com.ultimate.ultimatesmartstudent.Homework.HomeWorkByDate;
import com.ultimate.ultimatesmartstudent.Homework.SubWiseHW;
import com.ultimate.ultimatesmartstudent.Leave_Mod.LeaveActivity;
import com.ultimate.ultimatesmartstudent.Leave_Mod.Leavelist_Activity;
import com.ultimate.ultimatesmartstudent.LiveClassModule.SheduleOnlineClassActivityList;
import com.ultimate.ultimatesmartstudent.Messages.All_Activity.ComposeAdminMsg;
import com.ultimate.ultimatesmartstudent.Messages.All_Activity.ComposeStaffMsg;
import com.ultimate.ultimatesmartstudent.Messages.All_Activity.MessageActivity;
import com.ultimate.ultimatesmartstudent.Messages.All_Activity.Sent_MessageActivity;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeActivity;
import com.ultimate.ultimatesmartstudent.E_LearningMod.ViewOnlineClassLink;
import com.ultimate.ultimatesmartstudent.Notification.NotificationBean;
import com.ultimate.ultimatesmartstudent.Notification.NotificationHomeAdapter;
import com.ultimate.ultimatesmartstudent.OCRMod.OcrCaptureActivity;
import com.ultimate.ultimatesmartstudent.Performance_Report.CleanlinessList;
import com.ultimate.ultimatesmartstudent.Performance_Report.CommunicationList;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SchoolActivity.ViewStudents;
import com.ultimate.ultimatesmartstudent.Social_Post.SocialLinkList;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.SyllabusMod.SyllabusActivity;
import com.ultimate.ultimatesmartstudent.SyllabusMod.SyllabusAdapterGDS;
import com.ultimate.ultimatesmartstudent.SyllabusMod.SyllabusBean;
import com.ultimate.ultimatesmartstudent.TestMod.GDSTest;
import com.ultimate.ultimatesmartstudent.TimetableMod.TimeTableAdapter;
import com.ultimate.ultimatesmartstudent.TimetableMod.TimeTableBean_IMG;
import com.ultimate.ultimatesmartstudent.TimetableMod.TimeTableSectionWise.SecWiseTimeTable;
import com.ultimate.ultimatesmartstudent.TimetableMod.ViewTimeTable;
import com.ultimate.ultimatesmartstudent.TransportModule.DriverLocActivity;
import com.ultimate.ultimatesmartstudent.TransportModule.NewTransport.MorningDetails;
import com.ultimate.ultimatesmartstudent.TransportModule.NewTransport.TransportAttendanceActivity;
import com.ultimate.ultimatesmartstudent.TransportModule.TranTodayAttActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Webview.FileDownloader;
import com.ultimate.ultimatesmartstudent.Webview.QuizWebviewActivity;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class ToolFrag extends Fragment implements DashBoardAdapter.OpenDahsboardActivity, SyllabusAdapterGDS.Mycallback,TimeTableAdapter.TimtableCallbacks
        , TextToSpeech.OnInitListener, NotificationHomeAdapter.Mycallback {

    private FloatingNavigationView mFloatingNavigationView;
    SharedPreferences sharedPreferences;
    TextToSpeech textToSpeech;
    String textholder="",title="",date="",school="";
    int count=0,speech=0;

    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ShimmerFrameLayout noti_shimmer;
    RecyclerView recyclerView;
    private DashBoardAdapter adapter;
    private ArrayList<DashboardBean> dataList;
    Animation animation1,animation2;

    @BindView(R.id.msg_count)
    TextView msg_count;
    @BindView(R.id.lytttt)
    RelativeLayout lytttt;

    @BindView(R.id.fees_count)
    TextView fees_count;
    @BindView(R.id.lytttt_fees)
    RelativeLayout lytttt_fees;

    @BindView(R.id.lytttt_school)
    RelativeLayout lytttt_school;


    @BindView(R.id.schName)
    TextView schName;
    @BindView(R.id.imgLogo)
    CircularImageView imgLogo;

    String s_key,student_id,class_id;

    TextView stu_name,class_namessss;
    ImageView imgMoreUser;
    CircularImageView navi_profile;
    ArrayList<Live_Class_Bean> live_class_beans = new ArrayList<>();
    String link="";
    BottomSheetDialog mBottomSheetDialog;

    RecyclerView recyclerview1;
    private LinearLayoutManager layoutManager;
    ArrayList<SyllabusBean> syllabusList = new ArrayList<>();
    private SyllabusAdapterGDS s_adapter;
    @BindView(R.id.searchView)
    SearchView searchView;
    TextView textNorecord;

    RecyclerView notiRV;

    private TimeTableAdapter t_adapter;
    ArrayList<TimeTableBean_IMG> time_list = new ArrayList<>();
    String folder_main = "TimeTable";
    String folder_main1 = "Syllabus";

    Animation animation;
    ArrayList<NotificationBean> notiList = new ArrayList<>();
    private NotificationHomeAdapter notiAdapter;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    int loaded=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_tool, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        setSchoolData();
        fetchClassUrl();
        textToSpeech = new TextToSpeech(getActivity(), this);

        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        dataList = new ArrayList<>();
        dataList = addList();
        loaded=dataList.size();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter = new DashBoardAdapter(getActivity(), dataList, this);
        recyclerView.setAdapter(adapter);
//        animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left_animation);
//        recyclerView.startAnimation(animation1);
        mFloatingNavigationView = (FloatingNavigationView) view.findViewById(R.id.floating_navigation_view);
        /* Side Drawer and Hrader files set data here*/
        View headerView = mFloatingNavigationView.getHeaderView(0);
        stu_name = (TextView) headerView.findViewById(R.id.stu_name);
        class_namessss = (TextView) headerView.findViewById(R.id.class_namessss);
        navi_profile = (CircularImageView) headerView.findViewById(R.id.navi_profile);
        imgMoreUser = (ImageView) headerView.findViewById(R.id.imgMoreUser);
        // Utils.setNaviHeaderData(stu_name,class_namessss,navi_profile,getActivity());


        imgMoreUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgMoreUser.startAnimation(animation2);
                startActivity(new Intent(getActivity(), AccountActivity.class));
            }
        });

        lytttt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msg_count.startAnimation(animation);
                Intent intent= new Intent(getActivity(), MessageActivity.class);
                intent.putExtra("Today","Today");
                startActivity(intent);
            }
        });

        lytttt_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lytttt_school.startAnimation(animation);
                // viewSchoolInfo();
                Intent intent= new Intent(getActivity(), SchoolInfo.class);
                startActivity(intent);
            }
        });

        lytttt_fees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fees_count.startAnimation(animation);

                if (!fees_count.getText().toString().equalsIgnoreCase("0")) {
                    openDialog();
                }else {
                    Toast.makeText(getContext(), getString(R.string.no_noti), Toast.LENGTH_SHORT).show();
                }
                // startActivity(new Intent(getActivity(), FeePaidListActivity.class));
            }

        });


        mFloatingNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingNavigationView.open();
            }
        });
        mFloatingNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                if (item.getTitle().equals("Change Password")){
                    Toast.makeText(getActivity(),"Change Password",Toast.LENGTH_SHORT).show();
                }
                else if (item.getTitle().equals("Feedback")){
                    Toast.makeText(getActivity(),"Feedback",Toast.LENGTH_SHORT).show();
                } else if (item.getTitle().equals("Rate Us")){
                    Toast.makeText(getActivity(),"Rate Us",Toast.LENGTH_SHORT).show();
                } else if (item.getTitle().equals("Share Us")){
                    Toast.makeText(getActivity(),"Share Us",Toast.LENGTH_SHORT).show();
                } else if (item.getTitle().equals("Privacy Policy")){
                    Toast.makeText(getActivity(),"Privacy Policy",Toast.LENGTH_SHORT).show();
                }
                else if (item.getTitle().equals("App Info")){
                    Toast.makeText(getActivity(),"App Info",Toast.LENGTH_SHORT).show();
                } else{
                    // Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                    Toast.makeText(getActivity(),"Logout!",Toast.LENGTH_SHORT).show();
                }
                //  Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();
                return true;
            }
        });

        totalRecord.setText(getString(R.string.t_record)+""+String.valueOf(loaded));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE
                if (query.length()==0){
                    totalRecord.setText(getString(R.string.t_record)+""+String.valueOf(loaded));
                    checkLoaded(loaded);
                }else {
                    totalRecord.setText(getString(R.string.s_record)+"" +String.valueOf(adapter.dataList.size()));
                    checkLoaded(adapter.dataList.size());
                }
                adapter.getFilter().filter(query);

                return false;
            }
        });
    }

    private void checkLoaded(int loaded) {
        if (loaded==0){
            txtNorecord.setVisibility(View.VISIBLE);
        }else {
            txtNorecord.setVisibility(View.GONE);
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(getView() != null && isVisibleToUser){
            Log.i("1st","1st");
            if (!restorePrefData()){
                setShowcaseView();
            }
        }
    }

    private void setShowcaseView() {
        new TapTargetSequence(getActivity())
                .targets(
                        TapTarget.forView(lytttt_school,getString(R.string.s_icon),getString(R.string.s_body))
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt,getString(R.string.m_icon),getString(R.string.m_body))
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt_fees,getString(R.string.n_icon),getString(R.string.n_body))
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData(){
        sharedPreferences=getActivity().getSharedPreferences("boarding_pref_tool",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_tool",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_tool",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_tool",false);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void openDialog() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.noti_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);
        notiRV=(RecyclerView) sheetView.findViewById(R.id.recyclerView);

        TextView tittle = (TextView) sheetView.findViewById(R.id.tittle);
        String count=fees_count.getText().toString();

        if (!count.equalsIgnoreCase("0")) {
            String title = getColoredSpanned(count, "#e31e25");
            String Name = getColoredSpanned(" New Notification", "#000000");
            tittle.setText(Html.fromHtml(title + " " + Name));
        }else {
            tittle.setText("Today Notification");
        }

        notiList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
        notiRV.setLayoutManager(layoutManager1);
        notiAdapter = new NotificationHomeAdapter(notiList, getActivity(),this);
        notiRV.setAdapter(notiAdapter);
        fetchNotification();

        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    public void fetchNotification() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", User.getCurrentUser().getId());
        params.put("type", "10");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTIFICATION_URL, notiapiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback notiapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    notiList = NotificationBean.parseNotificationArray(noticeArray);
                    if (notiList.size() > 0) {
                        notiAdapter.setNList(notiList);
                        //setanimation on adapter...
                        notiRV.getAdapter().notifyDataSetChanged();
                        notiRV.scheduleLayoutAnimation();
                        //-----------end------------
                    } else {
                        notiList.clear();
                        notiAdapter.setNList(notiList);
                        notiAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                notiList.clear();
                notiAdapter.setNList(notiList);
                notiAdapter.notifyDataSetChanged();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }
        }
    };


    private void viewSchoolInfo() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.school_info_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);


        TextView txtHead = (TextView) sheetView.findViewById(R.id.txtHead);
        TextView c_name = (TextView) sheetView.findViewById(R.id.c_name);
        TextView txtAdd = (TextView) sheetView.findViewById(R.id.txtAdd);


        CircularImageView circleimgrecepone = (CircularImageView) sheetView.findViewById(R.id.circleimgrecepone);

        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
        String number = User.getCurrentUser().getSchoolData().getPhoneno();
        String mail = User.getCurrentUser().getSchoolData().getEmail();

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
            txtHead.setText("College Details");
        }else{
            txtHead.setText("School Details");
        }


        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(circleimgrecepone);
        } else {
            Picasso.get().load(R.drawable.logo).into(imgLogo);
        }
        if (User.getCurrentUser().getSchoolData().getName() != null){
            c_name.setText(User.getCurrentUser().getSchoolData().getName());
        }

        if (User.getCurrentUser().getSchoolData().getName() != null){
            txtAdd.setText(User.getCurrentUser().getSchoolData().getAddress());
        }

        if (mail!=null || mail.equalsIgnoreCase("")) {
            email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  mBottomSheetDialog.dismiss();
                    //  UltimateProgress.showProgressBar(getActivity(), "Please Wait.....!");
                    Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                    intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                    //simplydirect email for setting email,,,
                    intent.setData(Uri.parse("mailto:" + mail)); // or just "mailto:" for blank
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                    startActivity(intent);
                    UltimateProgress.cancelProgressBar();

                }
            });
        }else{
            Toast.makeText(getActivity(),"Email Not Found!",Toast.LENGTH_LONG).show();
        }


        if (number!=null || number.equalsIgnoreCase("")) {
            phone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // mBottomSheetDialog.dismiss();
                    // for permission granted....
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},1);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + number));
                        startActivity(intent);
                        UltimateProgress.cancelProgressBar();
                    }
                }
            });
        }else{
            Toast.makeText(getActivity(),"Number Not Found!",Toast.LENGTH_LONG).show();
        }


        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        fetnewmsg();
        fetnewmsgNoti();
    }

    private void fetnewmsgNoti() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG_NOTI, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                        // fees_count.setText(jsonObject.getJSONObject("doc_data").getString("noti_count"));
                        JSONObject docData = jsonObject.getJSONObject("doc_data");
                        int notiCount = 0; // Default value
                        if (docData.has("noti_count") && !docData.isNull("noti_count")) {
                            notiCount = docData.getInt("noti_count");
                        }
                        fees_count.setText(String.valueOf(notiCount));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    fees_count.setText("0");
                    // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void fetnewmsg() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        params.put("check","list");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG, apicallback5, getActivity(), params);
    }

    ApiHandler.ApiCallback apicallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();

            if (error == null) {

                try {
                    Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                    JSONObject docData = jsonObject.getJSONObject("doc_data");
                    int notiCount = 0; // Default value
                    if (docData.has("count") && !docData.isNull("count")) {
                        notiCount = docData.getInt("count");
                    }
                    msg_count.setText(String.valueOf(notiCount));

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                msg_count.setText("0");
                // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchClassUrl() {

        HashMap<String, String> params = new HashMap<>();
        params.put("class_id",User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.LIVECLSS, apicallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();

            if (error == null) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("class_data");
                    live_class_beans = Live_Class_Bean.parseLive_Class_BeanArray(jsonArray);
                    if (!live_class_beans.get(0).getLink().equalsIgnoreCase("")) {
                        link = live_class_beans.get(0).getLink();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            } else {

                //   Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };



    private void setSchoolData() {
        school=User.getCurrentUser().getSchoolData().getName();
        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(imgLogo);
        } else {
            Picasso.get().load(R.drawable.logo).into(imgLogo);
        }
        if (User.getCurrentUser().getSchoolData().getName() != null){
            schName.setText(User.getCurrentUser().getSchoolData().getName());
        }

    }

    public ArrayList<DashboardBean> addList() {
        ArrayList<DashboardBean> list = new ArrayList<>();

        DashboardBean sattendanceObj = new DashboardBean();
        sattendanceObj.setTitle(getString(R.string.attendance));
        sattendanceObj.setMIcon(getResources().getDrawable(R.drawable.attendance));
        list.add(sattendanceObj);

        DashboardBean staffObj17 = new DashboardBean();
        staffObj17.setTitle(getString(R.string.holiday_h));
        staffObj17.setMIcon(getResources().getDrawable(R.drawable.holiday_calendar));
        list.add(staffObj17);

        DashboardBean timetableObj = new DashboardBean();
        timetableObj.setTitle(getString(R.string.timetable));
        timetableObj.setMIcon(getResources().getDrawable(R.drawable.timetable));
        list.add(timetableObj);

        DashboardBean syllabusObj = new DashboardBean();
        syllabusObj.setTitle(getString(R.string.syllabus));
        syllabusObj.setMIcon(getResources().getDrawable(R.drawable.syllabus));
        list.add(syllabusObj);

        DashboardBean syllabusObj1 = new DashboardBean();
        syllabusObj1.setTitle(getString(R.string.month_plan));
        syllabusObj1.setMIcon(getResources().getDrawable(R.drawable.planner));
        list.add(syllabusObj1);

        DashboardBean homeworkObj = new DashboardBean();
        homeworkObj.setTitle(getString(R.string.homework));
        homeworkObj.setMIcon(getResources().getDrawable(R.drawable.homework));
        list.add(homeworkObj);


        DashboardBean classworkobj = new DashboardBean();
        classworkobj.setTitle(getString(R.string.classwork));
        classworkobj.setMIcon(getResources().getDrawable(R.drawable.classwork));
        list.add(classworkobj);




        DashboardBean trainingobj = new DashboardBean();
        trainingobj.setTitle(getString(R.string.elearning));
        trainingobj.setMIcon(getResources().getDrawable(R.drawable.elearning));
        list.add(trainingobj);


        DashboardBean ebookObj = new DashboardBean();
        ebookObj.setTitle(getString(R.string.ebook));
        ebookObj.setMIcon(getResources().getDrawable(R.drawable.ebook));
        list.add(ebookObj);


        DashboardBean dsObj = new DashboardBean();
        dsObj.setTitle(getString(R.string.datesheet));
        dsObj.setMIcon(getResources().getDrawable(R.drawable.datesheet));
        list.add(dsObj);



        DashboardBean feeObj = new DashboardBean();
        feeObj.setTitle(getString(R.string.assignment));
        feeObj.setMIcon(getResources().getDrawable(R.drawable.assignment));
        list.add(feeObj);

        DashboardBean QuerryObj = new DashboardBean();
        QuerryObj.setTitle(getString(R.string.exam));
        QuerryObj.setMIcon(getResources().getDrawable(R.drawable.exam));
        list.add(QuerryObj);


        DashboardBean testObj = new DashboardBean();
        testObj.setTitle(getString(R.string.test));
        testObj.setMIcon(getResources().getDrawable(R.drawable.test));
        list.add(testObj);


        DashboardBean leaveObj = new DashboardBean();
        leaveObj.setTitle(getString(R.string.leave));
        leaveObj.setMIcon(getResources().getDrawable(R.drawable.leave));
//        leaveObj.setTxtColor("1");
        list.add(leaveObj);

        DashboardBean hostlObj = new DashboardBean();
        hostlObj.setTitle(getString(R.string.school_act));
        hostlObj.setMIcon(getResources().getDrawable(R.drawable.school_activty));
        list.add(hostlObj);


        DashboardBean feeO = new DashboardBean();
        feeO.setTitle(getString(R.string.fee));
        feeO.setMIcon(getResources().getDrawable(R.drawable.fee_management));
        list.add(feeO);

        DashboardBean feebank = new DashboardBean();
        feebank.setTitle(getString(R.string.bank));
        feebank.setMIcon(getResources().getDrawable(R.drawable.transfer));
        list.add(feebank);


        DashboardBean transObj = new DashboardBean();
        transObj.setTitle(getString(R.string.transport));
        transObj.setMIcon(getResources().getDrawable(R.drawable.transpotssss));
        list.add(transObj);


        DashboardBean gateObj = new DashboardBean();
        gateObj.setTitle(getString(R.string.gatepass));
        gateObj.setMIcon(getResources().getDrawable(R.drawable.gatepass));
        list.add(gateObj);


        DashboardBean staffObj31 = new DashboardBean();
        staffObj31.setTitle(getString(R.string.social));
        staffObj31.setMIcon(getResources().getDrawable(R.drawable.social_media));
        list.add(staffObj31);


        DashboardBean QuerryObj3 = new DashboardBean();
        QuerryObj3.setTitle(getString(R.string.health));
        QuerryObj3.setMIcon(getResources().getDrawable(R.drawable.health));
        list.add(QuerryObj3);

        DashboardBean QuerryObj1 = new DashboardBean();
        QuerryObj1.setTitle(getString(R.string.performance));
        QuerryObj1.setMIcon(getResources().getDrawable(R.drawable.performance));
        list.add(QuerryObj1);




        DashboardBean studentobj = new DashboardBean();
        studentobj.setTitle(getString(R.string.msg));
        studentobj.setMIcon(getResources().getDrawable(R.drawable.msges));
        list.add(studentobj);


//        DashboardBean feeobj = new DashboardBean();
//        feeobj.setTitle(Constants.fee);
//        feeobj.setMIcon(getResources().getDrawable(R.drawable.fee_management));
//        list.add(feeobj);


        DashboardBean galleryObj = new DashboardBean();
        galleryObj.setTitle(getString(R.string.gallery));
        galleryObj.setMIcon(getResources().getDrawable(R.drawable.picture));
        list.add(galleryObj);


        DashboardBean eventObj = new DashboardBean();
        eventObj.setTitle(getString(R.string.event));
        eventObj.setMIcon(getResources().getDrawable(R.drawable.event));
        list.add(eventObj);



        DashboardBean noticeObj = new DashboardBean();
        noticeObj.setTitle(getString(R.string.notice_board));
        noticeObj.setMIcon(getResources().getDrawable(R.drawable.notice_board));
        list.add(noticeObj);



        DashboardBean onlineobj = new DashboardBean();
        onlineobj.setTitle(getString(R.string.liveclass));
        onlineobj.setMIcon(getResources().getDrawable(R.drawable.education));
        list.add(onlineobj);

        DashboardBean quizObj = new DashboardBean();
        quizObj.setTitle(getString(R.string.onlinequiztest));
        quizObj.setMIcon(getResources().getDrawable(R.drawable.onlinetest));
        list.add(quizObj);


        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
            //  schName.setText(User.getCurrentUser().getSchoolData().getName());
            DashboardBean addDocObj = new DashboardBean();
            addDocObj.setTitle(getString(R.string.add_doc));
            addDocObj.setMIcon(getResources().getDrawable(R.drawable.add_doc));
            list.add(addDocObj);
        }else{

//            DashboardBean DiestObj = new DashboardBean();
//            DiestObj.setTitle(getString(R.string.dietchart));
//            DiestObj.setMIcon(getResources().getDrawable(R.drawable.dietchart));
//            list.add(DiestObj);
        }

//        DashboardBean imgtopdfObj = new DashboardBean();
//        imgtopdfObj.setTitle(getString(R.string.imgtopdf));
//        imgtopdfObj.setMIcon(getResources().getDrawable(R.drawable.jpgtopdfpng));
//        list.add(imgtopdfObj);

        DashboardBean ocrobj = new DashboardBean();
        ocrobj.setTitle(getString(R.string.ocr));
        ocrobj.setMIcon(getResources().getDrawable(R.drawable.scanner));
        list.add(ocrobj);


        return list;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void openDahsboardActivity(DashboardBean dashboard) {

        if (dashboard.getTitle().equals(getString(R.string.imgtopdf))) {
            startActivity(new Intent(getContext(), ImageToPdfConvert.class));
        }

        if (dashboard.getTitle().equals(getString(R.string.holiday_h))) {
            startActivity(new Intent(getContext(), HolidayActivity.class));
        }

        if (dashboard.getTitle().equals(getString(R.string.event))) {
            open_classwork_dialog(dashboard.getTitle());
           // startActivity(new Intent(getContext(), AchievementList.class));

        }



        if (dashboard.getTitle().equals(getString(R.string.performance))) {
            openAdmissionformdialog(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(getString(R.string.health))) {
            startActivity(new Intent(getContext(), View_Health.class));

        }

        if (dashboard.getTitle().equals(getString(R.string.school_act))) {
            startActivity(new Intent(getContext(), ViewStudents.class));
        }

        if (dashboard.getTitle().equals(getString(R.string.social))) {
            startActivity(new Intent(getContext(), SocialLinkList.class));

        }

        if (dashboard.getTitle().equals(getString(R.string.transport))) {
            // startActivity( new Intent(getActivity(), DriverLocActivity.class));
            open_leave_dialog(dashboard.getTitle());
            startanimation();

        }

        if (dashboard.getTitle().equals(getString(R.string.ocr))) {
            startActivity( new Intent(getActivity(), OcrCaptureActivity.class));
            startanimation();
        }

        if (dashboard.getTitle().equals(getString(R.string.fee))) {
//            startActivity( new Intent(getActivity(), FeePaidListActivity.class));
            // startActivity( new Intent(getActivity(), PayFeeSActivity.class));
            open_assignment_dialog(dashboard.getTitle());
            startanimation();
        }

        if (dashboard.getTitle().equals(getString(R.string.add_doc))) {
            startActivity( new Intent(getActivity(), AdddocumentActivity.class));
            startanimation();
        }

        if (dashboard.getTitle().equals(getString(R.string.syllabus))) {
            Intent i = new Intent(getActivity(), SyllabusActivity.class);
            i.putExtra("check", "syllabus");
            startActivity(i);

            // open_syllabus_dialog(dashboard.getTitle());
        }


        if (dashboard.getTitle().equals(getString(R.string.month_plan))) {
            Intent i = new Intent(getActivity(), SyllabusActivity.class);
            i.putExtra("check", "month");
            startActivity(i);

            // open_syllabus_dialog(dashboard.getTitle());
        }


        if (dashboard.getTitle().equals(getString(R.string.datesheet))) {
            startActivity( new Intent(getActivity(), Datesheet.class));
            startanimation();
        }

        if (dashboard.getTitle().equals(getString(R.string.dietchart))) {
            startActivity( new Intent(getActivity(), DietChartActivity.class));
            startanimation();
        }

        if (dashboard.getTitle().equals(getString(R.string.timetable))) {
//            open_syllabus_dialog(dashboard.getTitle());
           // startActivity( new Intent(getActivity(), ViewTimeTable.class));
            open_homework_dialog(dashboard.getTitle());

            //startanimation();
        }

        if (dashboard.getTitle().equals(getString(R.string.classwork))) {
            open_classwork_dialog(dashboard.getTitle());
        }
        if (dashboard.getTitle().equals(getString(R.string.exam))) {
            open_assignment_dialog(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(getString(R.string.assignment))) {

            startActivity( new Intent(getActivity(), MainAssignActivity.class));
            startanimation();
            //   open_assignment_dialog(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(getString(R.string.gatepass))) {
            // startActivity( new Intent(getActivity(), GatePass.class));
            //startActivity( new Intent(getActivity(), NewGatePassActivity.class));
            open_homework_dialog(dashboard.getTitle());
           // startanimation();
        }


        if (dashboard.getTitle().equals(getString(R.string.msg))) {
            open_assignment_dialog(dashboard.getTitle());
            //  startActivity( new Intent(getActivity(), ViewPdfActivity.class));
        }

        if (dashboard.getTitle().equals(getString(R.string.homework))) {
            open_homework_dialog(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(getString(R.string.onlinequiztest))) {
            //open_homework_dialog(dashboard.getTitle());
            startActivity(new Intent(getActivity(), QuizWebviewActivity.class));
        }

        if (dashboard.getTitle().equals(getString(R.string.attendance))) {

            if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT"))
            {
                open_attend_dialog();
                //startActivity( new Intent(getActivity(), StudentGateAttendance.class));
            }else{
                startActivity( new Intent(getActivity(), AttendanceActivity.class));

            }
            startanimation();
        }

        if (dashboard.getTitle().equals(getString(R.string.leave))) {
            open_leave_dialog(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(getString(R.string.bank))) {
            startActivity( new Intent(getActivity(), AddBankDetails.class));
        }

        if (dashboard.getTitle().equals(getString(R.string.ebook))) {
            startActivity( new Intent(getActivity(), Ebook.class));
            startanimation();

        }

        if (dashboard.getTitle().equals(getString(R.string.notice_board))) {
            startActivity( new Intent(getActivity(), NoticeActivity.class));
            startanimation();
        }

        if (dashboard.getTitle().equals(getString(R.string.gallery))) {
            //startActivity( new Intent(getActivity(), GDSGalleryAct.class));
            //startanimation();
            gallerydialog(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(getString(R.string.test))) {
            startActivity( new Intent(getActivity(), GDSTest.class));
            startanimation();

        }
        if (dashboard.getTitle().equals(getString(R.string.elearning))) {
            // startActivity( new Intent(getActivity(), ViewOnlineClassLink.class));
            // startanimation();
            onlineClassdialog();
        }

        if (dashboard.getTitle().equals(getString(R.string.liveclass))) {
//            if (!link.equalsIgnoreCase("")) {
//                startActivity(new Intent(Intent.ACTION_VIEW,
//                        Uri.parse(link)));
//            }else{
//                 Toast.makeText(getActivity(),"Online Classes Not Available!",Toast.LENGTH_SHORT).show();
//            }
            startActivity( new Intent(getActivity(), SheduleOnlineClassActivityList.class));
            startanimation();
        }

        // Toast.makeText(getActivity(),"Click On "+ dashboard.getTitle(),Toast.LENGTH_SHORT).show();
    }

    private void openAdmissionformdialog(String title1) {


            BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
            final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
            mBottomSheetDialog.setContentView(sheetView);

            mBottomSheetDialog.setCancelable(true);

            Animation animation;
            animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

            TextView title= (TextView) sheetView.findViewById(R.id.title);
            ImageView image= (ImageView) sheetView.findViewById(R.id.image);
            // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
            Button sub_wise = (Button) sheetView.findViewById(R.id.sub_wise);
            Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);


                title.setText(getString(R.string.performance));
                sub_wise.setText(getString(R.string.communication));
                date_wise.setText(getString(R.string.cleaniless));
                //image.getResources().setDrawable();
                Picasso.get().load(R.drawable.performance).into(image);

            sub_wise.setBackground(getResources().getDrawable(R.drawable.orange_bg));
            date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));


            sub_wise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sub_wise.startAnimation(animation);


                    if (title1.equalsIgnoreCase(getString(R.string.fee))){
                        // startActivity( new Intent(getActivity(), PayFeeSActivity.class));
                        startActivity( new Intent(getActivity(), PayFee_New.class));



                    }else {
                        startActivity( new Intent(getActivity(), CommunicationList.class));
                    }

                    startanimation();
                }
            });

            date_wise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    date_wise.startAnimation(animation);

                    if (title1.equalsIgnoreCase(getString(R.string.fee))){
                        //startActivity( new Intent(getActivity(), FeesReceiptList.class));
                        Intent intent = new Intent(getActivity(), CommonFeesListActivity.class);
                        intent.putExtra("list_view", "pending");
                        startActivity(intent);

                    }else {
                        startActivity( new Intent(getActivity(), CleanlinessList.class));
                    }


                    startanimation();
                }
            });
            RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnNo.startAnimation(animation);
                    mBottomSheetDialog.dismiss();
                }
            });
            mBottomSheetDialog.show();


    }


    private void open_attend_dialog() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText("Attendance");
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.attendance).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setText("Gate!");
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getActivity(), StudentGateAttendance.class);
                startActivity(intent);
                startanimation();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        date_wise.setText("Class!");
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                Intent intent = new Intent(getActivity(), AttendanceActivity.class);
                startActivity(intent);
                startanimation();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void gallerydialog(String title1) {

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        Button sub_wise = (Button) sheetView.findViewById(R.id.sub_wise);
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);


        if (title1.equalsIgnoreCase(getString(R.string.fee))){
            title.setText(getString(R.string.add_fees));
            sub_wise.setText(getString(R.string.add_res));
            date_wise.setText(getString(R.string.view_res));
            //image.getResources().setDrawable();
            Picasso.get().load(R.drawable.fee_management).into(image);
        }else {
            title.setText(getString(R.string.gallery));
            sub_wise.setText(getString(R.string.image));
            date_wise.setText(getString(R.string.video));
            //image.getResources().setDrawable();
            Picasso.get().load(R.drawable.picture).into(image);
        }


        sub_wise.setBackground(getResources().getDrawable(R.drawable.orange_bg));
        date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));


        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);


                if (title1.equalsIgnoreCase(getString(R.string.fee))){
                    // startActivity( new Intent(getActivity(), PayFeeSActivity.class));
                    startActivity( new Intent(getActivity(), PayFee_New.class));



                }else {
                    startActivity( new Intent(getActivity(), GDSGalleryAct.class));
                }

                startanimation();
            }
        });

        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);

                if (title1.equalsIgnoreCase(getString(R.string.fee))){
                    //startActivity( new Intent(getActivity(), FeesReceiptList.class));
                    Intent intent = new Intent(getActivity(), CommonFeesListActivity.class);
                    intent.putExtra("list_view", "pending");
                    startActivity(intent);

                }else {
                    startActivity( new Intent(getActivity(), VideoGalleryActivity.class));
                }


                startanimation();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    private void onlineClassdialog() {

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        Button sub_wise = (Button) sheetView.findViewById(R.id.sub_wise);
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);

        title.setText(getString(R.string.elearning));
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.elearning).into(image);
        sub_wise.setText(getString(R.string.video));
        date_wise.setText(getString(R.string.youtube));

        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                startActivity( new Intent(getActivity(), VideoActivity.class));
                startanimation();
            }
        });

        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                startActivity( new Intent(getActivity(), ViewOnlineClassLink.class));
                startanimation();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();

    }

    private void startanimation() {
        Animatoo.animateShrink(getActivity());
    }

    private void open_assignment_dialog(String title1) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);

        Button submitted= (Button) sheetView.findViewById(R.id.submitted);
        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);

        LinearLayout li1= (LinearLayout) sheetView.findViewById(R.id.li1);
        Button sub_wise1= (Button) sheetView.findViewById(R.id.sub_wise1);
        Button date_wise1= (Button) sheetView.findViewById(R.id.date_wise1);

        if (title1.equals(getString(R.string.assignment))){
            title.setText(title1);
            sub_wise.setText(getString(R.string.active));
            date_wise.setText(getString(R.string.expire));
            submitted.setText(getString(R.string.submitted));
            submitted.setVisibility(View.VISIBLE);
            //image.getResources().setDrawable();
            submitted.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            date_wise.setBackground(getResources().getDrawable(R.drawable.orange_bg));
            Picasso.get().load(R.drawable.homework).into(image);
        } else if (title1.equals(getString(R.string.msg))){
            title.setText(title1);
            sub_wise.setText(getString(R.string.received));
            date_wise.setText(getString(R.string.send));
            submitted.setText(getString(R.string.compose));
            submitted.setVisibility(View.VISIBLE);
            //image.getResources().setDrawable();
            sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            submitted.setBackground(getResources().getDrawable(R.drawable.orange_bg));
            date_wise.setBackground(getResources().getDrawable(R.drawable.leave_bg));
            Picasso.get().load(R.drawable.msges).into(image);
        }else if (title1.equals(getString(R.string.fee))){

            if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZ") ||
                    User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("SXYZ")){
                title.setText(title1);
                sub_wise.setText(getString(R.string.status));
                date_wise.setText(getString(R.string.pay_online));

                sub_wise1.setText(getString(R.string.receipt));
                date_wise1.setText(getString(R.string.f_paid_list));

                submitted.setText(getString(R.string.f_paid_list));
                li1.setVisibility(View.VISIBLE);
                //image.getResources().setDrawable();
                sub_wise.setBackground(getResources().getDrawable(R.drawable.orange_bg));
                submitted.setBackground(getResources().getDrawable(R.drawable.absent_bg));
                date_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
                sub_wise1.setBackground(getResources().getDrawable(R.drawable.leave_bg));
                date_wise1.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            }else{
                title.setText(title1);
                sub_wise.setText(getString(R.string.status));
                date_wise.setText(getString(R.string.receipt));
                submitted.setText(getString(R.string.f_paid_list));
                submitted.setVisibility(View.VISIBLE);
                //image.getResources().setDrawable();
                sub_wise.setBackground(getResources().getDrawable(R.drawable.orange_bg));
                submitted.setBackground(getResources().getDrawable(R.drawable.absent_bg));
                date_wise.setBackground(getResources().getDrawable(R.drawable.leave_bg));
                Picasso.get().load(R.drawable.fee_management).into(image);
            }


        }else {
            title.setText(title1);
            sub_wise.setText(getString(R.string.admitcard));
            date_wise.setText(getString(R.string.reportcard));
            submitted.setVisibility(View.GONE);
            //  date_wise.setText(getString(R.string.result));
            submitted.setText("Annual Result!");
            submitted.setVisibility(View.GONE);
            //image.getResources().setDrawable();
            sub_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
            submitted.setBackground(getResources().getDrawable(R.drawable.orange_bg));
            date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            Picasso.get().load(R.drawable.exam).into(image);
        }


        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZ") ||
                User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("SXYZ")){
            sub_wise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sub_wise.startAnimation(animation);
                    if (title1.equals(getString(R.string.assignment))) {
                        Intent intent = new Intent(getActivity(), MainAssignActivity.class);
                        startActivity(intent);

                        startanimation();
                    } else if (title1.equals(getString(R.string.msg))){
                        Intent intent = new Intent(getActivity(), MessageActivity.class);
                        startActivity(intent);

                        startanimation();
                    }
                    else if (title1.equals(getString(R.string.fee))){
//                    Intent intent = new Intent(getActivity(), MonthlyFees.class);
                        Intent intent = new Intent(getActivity(), CommonSTDPendingFeesListActivity.class);
                        startActivity(intent);

                        startanimation();
                    }else {
                        Intent intent = new Intent(getActivity(), Exam_Detail_Result_Activity.class);
                        intent.putExtra("view", getString(R.string.details));
                        startActivity(intent);

                        startanimation();
                    }
                }
            });
            date_wise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    date_wise.startAnimation(animation);

                    if (title1.equals(getString(R.string.assignment))) {
                        Intent intent = new Intent(getActivity(), MainAssignActivity.class);
                        startActivity(intent);

                        startanimation();
                    } else if (title1.equals(getString(R.string.msg))){
                        Intent intent = new Intent(getActivity(), Sent_MessageActivity.class);
                        startActivity(intent);

                        startanimation();
                    }else if (title1.equals(getString(R.string.fee))){
//

                        Intent intent = new Intent(getActivity(), PayFee_Online.class);
                        startActivity(intent);

                    }else {
                        //    Toast.makeText(getActivity(), "Result is not available!", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(getActivity(), Exam_Detail_Result_Activity.class);

                        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                        final View sheetView = getLayoutInflater().inflate(R.layout.message_dialog3, null);
                        mBottomSheetDialog.setContentView(sheetView);


                        Button add = (Button) sheetView.findViewById(R.id.add);
                        Button view11 = (Button) sheetView.findViewById(R.id.view);

                        TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);

                        //txtTitle.setText("Report Card");

//                    add.setText("Exam-Wise");
//                    view11.setText("Term-1");


                        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getActivity(), ReportCard.class);
                                intent.putExtra("view", getString(R.string.result));
                                startActivity(intent);
                                startanimation();
                            }
                        });

                        sheetView.findViewById(R.id.staff).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getContext(), ExamWebviewActivity.class);
                                intent.putExtra("name", "full");
                                startActivity(intent);
                                startanimation();
                            }
                        });

                        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getContext(), ExamWebviewActivity.class);
                                intent.putExtra("name", "half");
                                startActivity(intent);
                                startanimation();
                            }
                        });


                        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mBottomSheetDialog.dismiss();
                            }
                        });

                        mBottomSheetDialog.show();

//
//                    startanimation();
                    }

                }
            });

            sub_wise1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sub_wise1.startAnimation(animation);
                    gallerydialog(title1);

                    // startanimation();

                }
            });

            date_wise1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    date_wise1.startAnimation(animation);

                    Intent intent = new Intent(getActivity(), CommonFeesListActivity.class);
                    intent.putExtra("list_view", "add");
                    startActivity(intent);

                    startanimation();

                }
            });

            submitted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    submitted.startAnimation(animation);

                    if (title1.equals(getString(R.string.assignment))) {
                        Intent intent = new Intent(getActivity(), MainAssignActivity.class);
                        startActivity(intent);

                        startanimation();
                    } else if (title1.equals(getString(R.string.msg))){
                        open_compose_msg();
                    } else if (title1.equals(getString(R.string.fee))){
//                    Intent intent = new Intent(getActivity(), CommonFeesListActivity.class);
//                    startActivity(intent);

                        Intent intent = new Intent(getActivity(), CommonFeesListActivity.class);
                        intent.putExtra("list_view", "add");
                        startActivity(intent);

                        startanimation();
                    }else {

                        Intent intent = new Intent(getActivity(), ReportCardNew.class);
                        intent.putExtra("view", getString(R.string.result));
                        startActivity(intent);
                        startanimation();

                        //Toast.makeText(getActivity(), "Result is not available!", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }else {
            sub_wise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sub_wise.startAnimation(animation);
                    if (title1.equals(getString(R.string.assignment))) {
                        Intent intent = new Intent(getActivity(), MainAssignActivity.class);
                        startActivity(intent);

                        startanimation();
                    } else if (title1.equals(getString(R.string.msg))){
                        Intent intent = new Intent(getActivity(), MessageActivity.class);
                        startActivity(intent);

                        startanimation();
                    }
                    else if (title1.equals(getString(R.string.fee))){
//                    Intent intent = new Intent(getActivity(), MonthlyFees.class);
                        Intent intent = new Intent(getActivity(), CommonSTDPendingFeesListActivity.class);
                        startActivity(intent);

                        startanimation();
                    }else {
                        Intent intent = new Intent(getActivity(), Exam_Detail_Result_Activity.class);
                        intent.putExtra("view", getString(R.string.details));
                        startActivity(intent);

                        startanimation();
                    }
                }
            });
            date_wise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    date_wise.startAnimation(animation);

                    if (title1.equals(getString(R.string.assignment))) {
                        Intent intent = new Intent(getActivity(), MainAssignActivity.class);
                        startActivity(intent);

                        startanimation();
                    } else if (title1.equals(getString(R.string.msg))){
                        Intent intent = new Intent(getActivity(), Sent_MessageActivity.class);
                        startActivity(intent);

                        startanimation();
                    }else if (title1.equals(getString(R.string.fee))){
//
                        gallerydialog(title1);

                    }else {
                        //    Toast.makeText(getActivity(), "Result is not available!", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(getActivity(), Exam_Detail_Result_Activity.class);

                        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                        final View sheetView = getLayoutInflater().inflate(R.layout.message_dialog3, null);
                        mBottomSheetDialog.setContentView(sheetView);


                        Button add = (Button) sheetView.findViewById(R.id.add);
                        Button view11 = (Button) sheetView.findViewById(R.id.view);

                        TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);

                        //txtTitle.setText("Report Card");

//                    add.setText("Exam-Wise");
//                    view11.setText("Term-1");


                        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getActivity(), ReportCard.class);
                                intent.putExtra("view", getString(R.string.result));
                                startActivity(intent);
                                startanimation();
                            }
                        });

                        sheetView.findViewById(R.id.staff).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //  Toast.makeText(getActivity(),"Annually result not available",Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getContext(), ExamWebviewActivity.class);
                                intent.putExtra("name", "full");
                                startActivity(intent);
                                startanimation();
                            }
                        });

                        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //  startActivity(new Intent(getContext(), ExamWebviewActivity.class));
                                Intent intent = new Intent(getContext(), ExamWebviewActivity.class);
                                intent.putExtra("name", "half");
                                startActivity(intent);
                                startanimation();
                            }
                        });


                        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mBottomSheetDialog.dismiss();
                            }
                        });

                        mBottomSheetDialog.show();

//
//                    startanimation();
                    }

                }
            });

            submitted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    submitted.startAnimation(animation);

                    if (title1.equals(getString(R.string.assignment))) {
                        Intent intent = new Intent(getActivity(), MainAssignActivity.class);
                        startActivity(intent);

                        startanimation();
                    } else if (title1.equals(getString(R.string.msg))){
                        open_compose_msg();
                    } else if (title1.equals(getString(R.string.fee))){
//                    Intent intent = new Intent(getActivity(), CommonFeesListActivity.class);
//                    startActivity(intent);

                        Intent intent = new Intent(getActivity(), CommonFeesListActivity.class);
                        intent.putExtra("list_view", "add");
                        startActivity(intent);

                        startanimation();
                    }else {

                        Intent intent = new Intent(getActivity(), ReportCardNew.class);
                        intent.putExtra("view", getString(R.string.result));
                        startActivity(intent);
                        startanimation();

                        //Toast.makeText(getActivity(), "Result is not available!", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }

        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void open_compose_msg() {

        final Dialog logoutDialog = new Dialog(getActivity());
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setCancelable(true);
        logoutDialog.setContentView(R.layout.compose_msg_dialog);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Button btnNo = (Button) logoutDialog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                Intent intent = new Intent(getActivity(), ComposeStaffMsg.class);
                startActivity(intent);
                logoutDialog.dismiss();
                startanimation();
            }
        });
        Button btnYes = (Button) logoutDialog.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnYes.startAnimation(animation);
                Intent intent = new Intent(getActivity(), ComposeAdminMsg.class);
                startActivity(intent);
                logoutDialog.dismiss();
                startanimation();
            }
        });
        logoutDialog.show();
    }

    private void open_classwork_dialog(String value) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);

      //  title.setText(getString(R.string.classwork));
        //image.getResources().setDrawable();
        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        Button submitted= (Button) sheetView.findViewById(R.id.submitted);

        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);

                if (value.equalsIgnoreCase(getString(R.string.event))){
                    Intent intent = new Intent(getActivity(), Events_List.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), GDSClasswork.class);
                    intent.putExtra("type", "today");
                    startActivity(intent);
                }
                startanimation();
            }
        });

        date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));


        if (value.equalsIgnoreCase(getString(R.string.event))){
            submitted.setVisibility(View.VISIBLE);
            title.setText(getString(R.string.event));
            sub_wise.setText(getString(R.string.event)+" "+getString(R.string.list));
            date_wise.setText(getString(R.string.part_list));
            submitted.setText(getString(R.string.achive));
            Picasso.get().load(R.drawable.event).into(image);
        }else{
            title.setText(getString(R.string.classwork));
            sub_wise.setText(getString(R.string.today));
            date_wise.setText(getString(R.string.history));
            Picasso.get().load(R.drawable.classwork).into(image);
        }

        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);

                if (value.equalsIgnoreCase(getString(R.string.event))){
                    Intent intent = new Intent(getActivity(), AchievementList.class);
                    startActivity(intent);
                }else{

                }
                startanimation();
            }
        });


        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);


                if (value.equalsIgnoreCase(getString(R.string.event))){
                    Intent intent = new Intent(getActivity(), ParticipationListStud.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), GDSClasswork.class);
                    intent.putExtra("type", "history");
                    startActivity(intent);
                }



                startanimation();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void open_leave_dialog(String title1) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        Button sub_wise = (Button) sheetView.findViewById(R.id.sub_wise);
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);


        if (title1.equalsIgnoreCase(getString(R.string.leave))) {

            title.setText(title1);
            //image.getResources().setDrawable();
            Picasso.get().load(R.drawable.leave).into(image);
            sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            sub_wise.setText(getString(R.string.apply));
            date_wise.setText(getString(R.string.view_list));

        } else{
            title.setText(getString(R.string.att_details));
            //image.getResources().setDrawable();
            Picasso.get().load(R.drawable.transpotssss).into(image);
            sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            sub_wise.setText(getString(R.string.morning));
            date_wise.setText(getString(R.string.evening));

        }
//        else{
//            title.setText(Constants.onlineclass);
//            //image.getResources().setDrawable();
//            Picasso.get().load(R.drawable.leave).into(image);
//            sub_wise.setText("Video!");
//            date_wise.setText("Youtube!");
//        }
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                if (title1.equalsIgnoreCase(getString(R.string.leave))) {
                    startActivity(new Intent(getActivity(), LeaveActivity.class));
                }else{
                    String typeeee="morn";
                   // open_tranDetails_dialog(title1,typeeee);

                    Intent intent = new Intent(getActivity(), TransportAttendanceActivity.class);
                    intent.putExtra("view", "morning");
                    startActivity(intent);

                }
                startanimation();
            }
        });

        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                if (title1.equalsIgnoreCase(getString(R.string.leave))) {
                    startActivity( new Intent(getActivity(), Leavelist_Activity.class));
                }else{
                    String typeeee="even";
                   // open_tranDetails_dialog(title1,typeeee);

                    Intent intent = new Intent(getActivity(), TransportAttendanceActivity.class);
                    intent.putExtra("view", "evening");
                    startActivity(intent);

                }


                startanimation();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    private void open_tranDetails_dialog(String title1, String typeeee) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);

        Button submitted= (Button) sheetView.findViewById(R.id.submitted);
        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);

        if (typeeee.equals("morn")){
            title.setText(getString(R.string.m_att));
            sub_wise.setText(getString(R.string.today));
            date_wise.setText(getString(R.string.history));
            submitted.setText(getString(R.string.trackvan));
            submitted.setVisibility(View.VISIBLE);
            //image.getResources().setDrawable();
            submitted.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            date_wise.setBackground(getResources().getDrawable(R.drawable.orange_bg));
            Picasso.get().load(R.drawable.transpotssss).into(image);
        } else {
            title.setText(getString(R.string.e_att));
            sub_wise.setText(getString(R.string.today));
            date_wise.setText(getString(R.string.history));
            submitted.setText(getString(R.string.trackvan));
            submitted.setVisibility(View.VISIBLE);
            //image.getResources().setDrawable();
            sub_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
            submitted.setBackground(getResources().getDrawable(R.drawable.leave_bg));
            date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            Picasso.get().load(R.drawable.transpotssss).into(image);
        }

        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                if (typeeee.equals("morn")) {
                    Intent intent = new Intent(getActivity(), TranTodayAttActivity.class);
                    intent.putExtra("view", "morning");
                    startActivity(intent);

                    startanimation();
                } else {
                    Intent intent = new Intent(getActivity(), TranTodayAttActivity.class);
                    intent.putExtra("view", "evening");
                    startActivity(intent);

                    startanimation();
                }
            }
        });
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);

                if (typeeee.equals("morn")) {
                    Intent intent = new Intent(getActivity(), TranTodayAttActivity.class);
                    intent.putExtra("view", "morning_date");
                    startActivity(intent);

                    startanimation();
                } else {
                    Intent intent = new Intent(getActivity(), TranTodayAttActivity.class);
                    intent.putExtra("view", "evening_date");
                    startActivity(intent);

                    startanimation();
                }

            }
        });

        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);

                if (typeeee.equals("morn")) {
                    Intent intent = new Intent(getActivity(), DriverLocActivity.class);
                    intent.putExtra("view", "morning");
                    startActivity(intent);

                    startanimation();
                } else {
                    Intent intent = new Intent(getActivity(), DriverLocActivity.class);
                    intent.putExtra("view", "evening");
                    startActivity(intent);

                    startanimation();
                }
            }
        });

        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();

    }

    private void open_homework_dialog(String title1) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        Button submit= (Button) sheetView.findViewById(R.id.submitted);

        if (title1.equals(getString(R.string.homework))){
            title.setText(title1);
            submit.setVisibility(View.VISIBLE);
            Picasso.get().load(R.drawable.homework).into(image);
            sub_wise.setText(getString(R.string.today));
            date_wise.setText(getString(R.string.history));
            submit.setText(getString(R.string.holiday_hw));
            submit.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        } else if(title1.equals(getString(R.string.gatepass))){
            submit.setVisibility(View.VISIBLE);
            title.setText(title1);
            sub_wise.setText(getString(R.string.add_person));
            date_wise.setText("Apply");
            submit.setText(getString(R.string.list));
            sub_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
            submit.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            Picasso.get().load(R.drawable.gatepass).into(image);
        }else if(title1.equals(getString(R.string.timetable))){
            submit.setVisibility(View.GONE);
            title.setText(title1);
            sub_wise.setText(getString(R.string.class_view));
            date_wise.setText(getString(R.string.sec_view));
            submit.setText(getString(R.string.list));
            sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            submit.setBackground(getResources().getDrawable(R.drawable.leave_bg));
            Picasso.get().load(R.drawable.timetable).into(image);
        }else {
            title.setText(title1);
            sub_wise.setText(getString(R.string.check));
            date_wise.setText(getString(R.string.result));
            sub_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
            date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
            Picasso.get().load(R.drawable.onlinetest).into(image);
        }

        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);

                s_key= User.getCurrentUser().getSchoolData().getFi_school_id();
                student_id=User.getCurrentUser().getId();
                class_id=User.getCurrentUser().getClass_id();



                if (title1.equals(getString(R.string.homework))) {
                    startActivity(new Intent(getActivity(), HomeWorkByDate.class));
                }else if(title1.equals(getString(R.string.gatepass))){
                    startActivity( new Intent(getActivity(), AddGatePersonDetails.class));
                }else if(title1.equals(getString(R.string.timetable))){
                    startActivity( new Intent(getActivity(), ViewTimeTable.class));
                }else{
                    startActivity(new Intent(getActivity(), QuizWebviewActivity.class));

//                    String link="https://ultimatesolutiongroup.com/onlinetest/studentHome.php?s_key="+s_key+"&student_id="+student_id+"&class_id="+class_id+"";
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
                }

                startanimation();
            }
        });

        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                if (title1.equals(getString(R.string.homework))) {

                    Intent intent = new Intent(getActivity(), SubWiseHW.class);
                    intent.putExtra("view", "normal");
                    startActivity(intent);

                }else if(title1.equals(getString(R.string.gatepass))){
                    startActivity( new Intent(getActivity(), AddGatePass.class));
                }else if(title1.equals(getString(R.string.timetable))){
                    startActivity( new Intent(getActivity(), SecWiseTimeTable.class));
                }else{
                    // startActivity(new Intent(getActivity(), ViewQuizResult.class));
                    Toast.makeText(getActivity(),"Currently Result Not Available!",Toast.LENGTH_LONG).show();
                }

                startanimation();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit.startAnimation(animation);
                if(title1.equals(getString(R.string.gatepass))){
                    startActivity( new Intent(getActivity(), GatePass.class));
                }else{
                    Intent intent = new Intent(getActivity(), SubWiseHW.class);
                    intent.putExtra("view", "holiday");
                    startActivity(intent);
                    // startActivity(new Intent(getActivity(), ViewQuizResult.class));
                    //  Toast.makeText(getActivity(),"Currently Result Not Available!",Toast.LENGTH_LONG).show();
                }
                startanimation();
            }
        });


        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();

    }

    private void open_syllabus_dialog(String adapter) {

        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.syllabus_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(false);

        recyclerview1  = (RecyclerView) sheetView.findViewById(R.id.recyclerView);
        noti_shimmer  = (ShimmerFrameLayout) sheetView.findViewById(R.id.noti_shimmer);
        textNorecord  = (TextView) sheetView.findViewById(R.id.textNorecord);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        recyclerview1.setLayoutAnimation(controller);
        //----------------end--------------------
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview1.setLayoutManager(layoutManager);
        TextView title = (TextView) sheetView.findViewById(R.id.title);
        ImageView image = (ImageView) sheetView.findViewById(R.id.image);

        if (adapter.equals(getString(R.string.syllabus))) {
            Picasso.get().load(R.drawable.syllabus).into(image);
            title.setText(getString(R.string.syllabus));
            s_adapter = new SyllabusAdapterGDS(syllabusList, getActivity(), this);
            recyclerview1.setAdapter(s_adapter);
            fetchSyllabus();
        } else {
            Picasso.get().load(R.drawable.timetable).into(image);
            title.setText(getString(R.string.timetable));
            t_adapter = new TimeTableAdapter(time_list, getActivity(), this);
            recyclerview1.setAdapter(t_adapter);
            fetchTimeTable();
        }
        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);



        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void fetchTimeTable() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("section_id", User.getCurrentUser().getSection_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEWTIMETABLE, apiCallback1, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (time_list != null) {
                        time_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("timetable_data");
                    time_list = TimeTableBean_IMG.parseTimeTableArray(jsonArray);
                    if (time_list.size() > 0) {
                        t_adapter.setTime_list(time_list);
                        //setanimation on adapter...
                        recyclerview1.getAdapter().notifyDataSetChanged();
                        recyclerview1.scheduleLayoutAnimation();
                        //-----------end------------
                        recyclerview1.setVisibility(View.VISIBLE);
                        noti_shimmer.stopShimmer();
                        noti_shimmer.setVisibility(View.GONE);
                        textNorecord.setVisibility(View.GONE);

                    } else {
                        noti_shimmer.stopShimmer();
                        noti_shimmer.setVisibility(View.GONE);
                        t_adapter.setTime_list(time_list);
                        t_adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                noti_shimmer.stopShimmer();
                noti_shimmer.setVisibility(View.GONE);
                textNorecord.setVisibility(View.VISIBLE);
                time_list.clear();
                t_adapter.setTime_list(time_list);
                t_adapter.notifyDataSetChanged();
            }
        }
    };

    private void fetchSyllabus() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SYLLABUSGDS, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (syllabusList != null) {
                        syllabusList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("syllabus_data");
                    syllabusList = SyllabusBean.parseSyllabusArray(jsonArray);
                    if (syllabusList.size() > 0) {
                        s_adapter.setSyllabusList(syllabusList);
                        //setanimation on adapter...
                        recyclerview1.getAdapter().notifyDataSetChanged();
                        recyclerview1.scheduleLayoutAnimation();
                        //-----------end------------
                        recyclerview1.setVisibility(View.VISIBLE);
                        noti_shimmer.stopShimmer();
                        noti_shimmer.setVisibility(View.GONE);
                        textNorecord.setVisibility(View.GONE);

                    } else {
                        s_adapter.setSyllabusList(syllabusList);
                        s_adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.VISIBLE);
                        noti_shimmer.stopShimmer();
                        noti_shimmer.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                noti_shimmer.stopShimmer();
                noti_shimmer.setVisibility(View.GONE);
                textNorecord.setVisibility(View.VISIBLE);
                syllabusList.clear();
                s_adapter.setSyllabusList(syllabusList);
                s_adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onMethodCallback(SyllabusBean syllabusBean) {

    }

    @Override
    public void viewPdf(SyllabusBean syllabusBean) {
        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(syllabusBean.getS_file())));

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(getString(R.string.syllabus));
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.ebook).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setText(getString(R.string.view));
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getActivity(), ViewPdfActivity.class);
                intent.putExtra("id", syllabusBean.getS_file());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);

            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        date_wise.setText(getString(R.string.save));
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                UltimateProgress.showProgressBar(getActivity(),"Please wait...");
                String mUrl=syllabusBean.getS_file();
                new DownloadFile().execute(mUrl);
                String path_name ="/storage/Android/data/com.ultimate.ultimatesmartstudent/files/document/"+folder_main1+"/Syllabus_file_saved.pdf";
                Toast.makeText(getActivity(), "Downloaded to "+path_name, Toast.LENGTH_LONG).show();

            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            if (speech==0) {
                TextToSpeechFunction();
            }
        }
    }

    @Override
    public void onMethodCallback(NotificationBean notiList) {
        speech=1;
        textholder = notiList.getN_title()+" ! "+ notiList.getN_msg();
        TextToSpeechFunction();
    }

    @Override
    public void onMethodCallbackNavigation(NotificationBean notiList) {
        if (notiList.getN_title().equalsIgnoreCase("Online Class")||notiList.getN_title().equalsIgnoreCase("Online Class!")){
            startActivity(new Intent(getActivity(), SheduleOnlineClassActivityList.class));

        }else if (notiList.getN_title().equalsIgnoreCase("Photo")||notiList.getN_title().equalsIgnoreCase("Gallery!")){
            startActivity(new Intent(getActivity(), GDSGalleryAct.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Holidays Homework")||notiList.getN_title().equalsIgnoreCase("Holidays Homework!")){
            Intent intent = new Intent(getActivity(), SubWiseHW.class);
            intent.putExtra("view", "holiday");
            startActivity(intent);
        }else if (notiList.getN_title().equalsIgnoreCase("Participant")||notiList.getN_title().equalsIgnoreCase("Participant!")){
            startActivity(new Intent(getActivity(), ParticipationListStud.class));
        }else if (notiList.getN_title().equalsIgnoreCase("E-Learning")||notiList.getN_title().equalsIgnoreCase("E-Learning!")){
            startActivity(new Intent(getActivity(), ViewOnlineClassLink.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Ebook")||notiList.getN_title().equalsIgnoreCase("Ebook Added!")){
            startActivity(new Intent(getActivity(), Ebook.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Syllabus") || notiList.getN_title().equalsIgnoreCase("Syllabus Updated!")
                ||notiList.getN_title().equalsIgnoreCase("Syllabus!")){
            Intent i = new Intent(getActivity(), SyllabusActivity.class);
            i.putExtra("check", "syllabus");
            startActivity(i);
        }else if (notiList.getN_title().equalsIgnoreCase("Datesheet")||notiList.getN_title().equalsIgnoreCase("Datesheet!")){
            startActivity(new Intent(getActivity(), Datesheet.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Test")||notiList.getN_title().equalsIgnoreCase("Daily Test!")
                ||notiList.getN_title().equalsIgnoreCase("Test Result!")){
            startActivity(new Intent(getActivity(), GDSTest.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Home Work")||notiList.getN_title().equalsIgnoreCase("Homework")){
            Intent intent = new Intent(getActivity(), SubWiseHW.class);
            intent.putExtra("view", "normal");
            startActivity(intent);
        }else if (notiList.getN_title().equalsIgnoreCase("Activity")|| notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), ViewStudents.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Leave")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), Leavelist_Activity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("School Activity Attendance!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), AttendanceActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Monthly Planner!")||notiList.getN_title().equalsIgnoreCase("")){
            Intent i = new Intent(getActivity(), SyllabusActivity.class);
            i.putExtra("check", "month");
            startActivity(i);
        }else if (notiList.getN_title().equalsIgnoreCase("Instagram Post")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), SocialLinkList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Communication Report!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), CommunicationList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Cleanliness Report!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), CleanlinessList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Social Post!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), SocialLinkList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Health Details!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), View_Health.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Video link Added!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), ViewOnlineClassLink.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Event Participation Acheivement")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), AchievementList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Monthly Fees!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), CommonFeesListActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Assignment!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), MainAssignActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Gate-Pass!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), GatePass.class));
        }else if (notiList.getN_title().equalsIgnoreCase("E-Learning Video Added!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), VideoActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("online class Added!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), SheduleOnlineClassActivityList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Notice!")||notiList.getN_title().equalsIgnoreCase("Notice")){
            startActivity(new Intent(getActivity(), NoticeActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Timetable")||notiList.getN_title().equalsIgnoreCase("Timetable!")){
            startActivity(new Intent(getActivity(), ViewTimeTable.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Message!")||notiList.getN_title().equalsIgnoreCase("Message")){
            Intent intent = new Intent(getActivity(), MessageActivity.class);
            startActivity(intent);
        }else if (notiList.getN_title().equalsIgnoreCase("Classwork")||notiList.getN_title().equalsIgnoreCase("Classwork!")){
            Intent intent = new Intent(getActivity(), GDSClasswork.class);
            intent.putExtra("type", "history");
            startActivity(intent);
        }else if (notiList.getN_title().equalsIgnoreCase("Attendance Notification")||notiList.getN_title().equalsIgnoreCase("Attendance Notification!")){
            startActivity(new Intent(getActivity(), AttendanceActivity.class));

        }else {
            Toast.makeText(getActivity(),"Module not found!",Toast.LENGTH_LONG).show();
        }
    }

    //text to speech programmatically...
    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onDestroy() {
        textToSpeech.shutdown();
        super.onDestroy();
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            File path = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), folder_main1);
            path.mkdir();
            File file = new File(path, folder_main1 + System.currentTimeMillis() + "saved.pdf");
            try{
                file.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, file);
            UltimateProgress.cancelProgressBar();
            return null;
        }
    }



    @Override
    public void time_viewImg(TimeTableBean_IMG timeTableBean) {
        Dialog dialogLog = new Dialog(getActivity());
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imgShow = (ImageView) dialogLog.findViewById(R.id.img);

        Picasso.get().load(timeTableBean.getImage().get(0)).placeholder(getResources().getDrawable(R.drawable.logo)).into(imgShow);

        Button save = (Button) dialogLog.findViewById(R.id.submit);
        save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                save.startAnimation(animation);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);
            }
        });

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
    }

    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getActivity().getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "Timetb_file_saved" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(getActivity(), "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "Timetb_file_saved" + ".jpg");
                fos = new FileOutputStream(Objects.requireNonNull(image));

                Toast.makeText(getActivity(), "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(getActivity(), "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void time_viewPdf(TimeTableBean_IMG timeTableBean) {
        // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(timeTableBean.getPdf_file())));
        Intent intent = new Intent(getActivity(), ViewPdfActivity.class);
        intent.putExtra("id", timeTableBean.getPdf_file());
        intent.putExtra("title", "Timetable PDF");
        startActivity(intent);

    }
}
