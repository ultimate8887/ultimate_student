package com.ultimate.ultimatesmartstudent.HomeFragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.andremion.floatingnavigationview.FloatingNavigationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
//import com.google.android.play.core.review.ReviewInfo;
//import com.google.android.play.core.review.ReviewManager;
//import com.google.android.play.core.review.ReviewManagerFactory;
import com.skyfishjy.library.RippleBackground;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AccountModule.ChangeLanguageActivity;
import com.ultimate.ultimatesmartstudent.AccountModule.UpdateProfileActivity;
import com.ultimate.ultimatesmartstudent.AdapterClasses.CustomVolleyRequest;
import com.ultimate.ultimatesmartstudent.AdapterClasses.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.AttendMod.AttendAdapter;
import com.ultimate.ultimatesmartstudent.AttendMod.AttendMod;
import com.ultimate.ultimatesmartstudent.BeanModule.BannerBean;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Holiday.HolidayActivity;
import com.ultimate.ultimatesmartstudent.HolidayMod.HolidayBean;
import com.ultimate.ultimatesmartstudent.HolidayMod.Holidayadapter;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeActivity;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeBean;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeBoardAdapter;
import com.ultimate.ultimatesmartstudent.NoticeMod.Thought_bean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.AccountModule.AccountActivity;
import com.ultimate.ultimatesmartstudent.SliderModule.B_SliderAdapter;
import com.ultimate.ultimatesmartstudent.SliderModule.NewsData;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.HomeActivity;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.firebase.ui.auth.AuthUI.getApplicationContext;


public class HomeFrag extends Fragment implements TextToSpeech.OnInitListener {
    Dialog mBottomSheetDialog1;
    private FloatingNavigationView mFloatingNavigationView;
    private Bitmap bitmap;
    String get_email = "", get_number = "";
    private int VERIFY_NUMBER = 1000;
    @BindView(R.id.imgStud)
    CircularImageView imgStud;
    Animation animation1, animation2, animation3, animation4;
    @BindView(R.id.txtStudName)
    TextView txtStudName;
    @BindView(R.id.class_name)
    TextView class_name;
    Animation animation;
    @BindView(R.id.roll_no)
    TextView roll_no;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.attendlayoutliner)
    LinearLayout attendlayoutliner;
    @BindView(R.id.view_noticeBoard)
    ImageView view_noticeBoard;
    @BindView(R.id.view_holidayList)
    ImageView view_holidayList;
    TextView stu_name, class_namessss;
    ImageView imgMoreUser;
    ImageView edit_profile;
    CircularImageView navi_profile;
//    private ReviewManager reviewManager;
//    ReviewInfo reviewInfo = null;
    String password = "", c_password = "", image_url = "", image_url_birth = "", house_name = "";
    ImageButton export;
    ImageView close;
    TextView bdayMsg;
    RelativeLayout layout0, layoutW;
    ViewSwitcher logo0, logo1, logo2, logo3, logow, logox, logoy, logoz;
    /* Attendance section*/
    @BindView(R.id.attendRV)
    RecyclerView attendRV;
    ArrayList<AttendMod> attendList = new ArrayList<>();
    ;
    private AttendAdapter attendAdapter;
//    @BindView(R.id.lytAttend)
//    CardView lytAttend;

    /*Notice board section*/
    @BindView(R.id.nbRV)
    RecyclerView nbRV;
    ArrayList<NoticeBean> noticeList;
    private NoticeBoardAdapter nbAdapter;
    @BindView(R.id.noNoticeData)
    TextView noNoticeData;

    JSONObject jsonObject;

    /* holiday section*/
    @BindView(R.id.holiRV)
    RecyclerView holiRV;
    ArrayList<HolidayBean> holidayList;
    private Holidayadapter hAdapter;
    @BindView(R.id.noHolidayData)
    TextView noHolidayData;
    @BindView(R.id.headtext)
    TextView headtext;
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private int current_pos = 0;
    int count = 0, speech = 0;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    RequestQueue rq;
    List<BannerBean> sliderImg;
    ViewPagerAdapter viewPagerAdapter;

    String request_url = "http://ultimatesolutiongroup.com/includes/studentapi/sliderBanner.php";
    String request_url1 = "http://ultimatesolutiongroup.com/office_admin/images/banner/";

    Handler handler = new Handler();
    Runnable runnable;
    Timer timer = new Timer();
    TextToSpeech textToSpeech;
    Dialog dialogLog;
    BottomSheetDialog mBottomSheetDialog;
    TextView txtName;
    CircularImageView imgPro;
    TextView txtClass;
    TextView txtRegNo;
    TextView txtDOB;
    TextView txtAadhar;
    TextView txtGender;
    TextView txtAddress;
    TextView txtContact;
    TextView txtEmail;
    TextView txtbg;
    TextView txtBoard;
    private RippleBackground rippleBg;
    SharedPreferences sharedPreferences;
    ArrayList<Thought_bean> eventList = new ArrayList<>();
    @BindView(R.id.thought)
    TextView thought;

    @BindView(R.id.thought_shimmer)
    ShimmerFrameLayout thought_shimmer;
    @BindView(R.id.holiday_shimmer)
    ShimmerFrameLayout holiday_shimmer;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.noti_shimmer)
    ShimmerFrameLayout noti_shimmer;

    @BindView(R.id.f_view1)
    LinearLayout f_view1;
    int load = 0;
    @BindView(R.id.lyt1)
    RelativeLayout lyt1;
    ArrayList<NewsData> new_sliderDataArrayList = new ArrayList<>();
    B_SliderAdapter b_adapter;
    CommonProgress commonProgress;
    public String date;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.new_fragment_home, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        attendlayoutliner.setVisibility(View.VISIBLE);

//        // below method is used to set auto cycle direction in left to
        commonProgress = new CommonProgress(getActivity());
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        rq = CustomVolleyRequest.getInstance(getActivity()).getRequestQueue();
        sliderImg = new ArrayList<>();
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        sliderDotspanel = (LinearLayout) view.findViewById(R.id.SliderDots);
        //Toast.makeText(getActivity(),"Count "+count,Toast.LENGTH_LONG).show();
        handler1 = new Handler();
        textToSpeech = new TextToSpeech(getActivity(), this);

        if (load == 0) {
            load++;
            commonProgress.show();
            // UltimateProgress.showProgressBar(getActivity(), "Please Wait........!");
        }

        /* holiday section*/
        // set animation on recyclerview
        int resId1 = R.anim.layout_animation_left_to_right;
        final LayoutAnimationController controller1 = AnimationUtils.loadLayoutAnimation(getActivity(), resId1);
        holiRV.setLayoutAnimation(controller1);
        //----------------end--------------------
        holidayList = new ArrayList<>();
        hAdapter = new Holidayadapter(holidayList, getActivity(), 5);
        holiRV.setAdapter(hAdapter);


        /* Notice board section*/
        noticeList = new ArrayList<>();
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        nbRV.setLayoutAnimation(controller);
        //----------------end--------------------
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        nbRV.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(nbRV.getContext(), layoutManager.getOrientation());
        nbRV.addItemDecoration(dividerItemDecoration);
        nbAdapter = new NoticeBoardAdapter(noticeList, getActivity());
        nbRV.setAdapter(nbAdapter);


        /* Attendance section*/
        attendRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
//        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
//        attendRV.setLayoutManager(layoutManager1);
        attendAdapter = new AttendAdapter(attendList, getActivity());
        attendRV.setAdapter(attendAdapter);


        mFloatingNavigationView = (FloatingNavigationView) view.findViewById(R.id.floating_navigation_view);

        /* Side Drawer and Hrader files set data here*/
        View headerView = mFloatingNavigationView.getHeaderView(0);
        stu_name = (TextView) headerView.findViewById(R.id.stu_name);
        class_namessss = (TextView) headerView.findViewById(R.id.class_namessss);
        navi_profile = (CircularImageView) headerView.findViewById(R.id.navi_profile);
        imgMoreUser = (ImageView) headerView.findViewById(R.id.imgMoreUser);
        edit_profile = (ImageView) headerView.findViewById(R.id.edit_profile);

        if (!restorePrefData()) {
            setShowcaseView();
        }

        startAnimation();
        // sendRequest();
        //checkView();
        setUserData();
        userprofile();

        imgMoreUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgMoreUser.startAnimation(animation4);
                startActivity(new Intent(getActivity(), AccountActivity.class));
                Animatoo.animateShrink(getActivity());
            }
        });


        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_profile.startAnimation(animation4);
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                Animatoo.animateShrink(getActivity());
            }
        });


        mFloatingNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingNavigationView.open();
            }
        });
        mFloatingNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                switch (id) {

                    case R.id.txtRate:
                        Toast.makeText(getActivity(), "Rate Us", Toast.LENGTH_SHORT).show();

                        Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        myAppLinkToMarket.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        try {
                            startActivity(myAppLinkToMarket);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), " Unable to find market app!", Toast.LENGTH_LONG).show();
                        }
                        //init();
                        break;

                    case R.id.txtShare:
                        Toast.makeText(getActivity(), "Share Us", Toast.LENGTH_SHORT).show();
                        try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, "XYZ Ultimate Student");
                            String sAux = "\nLet me recommend you this application\n\n";
                            sAux = sAux + "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName() + "\n\n";
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "Choose one!"));
                        } catch (Exception e) {
                            //e.toString();
                        }
                        break;

                    case R.id.txtPass:
                        Toast.makeText(getActivity(), "Reset Password", Toast.LENGTH_SHORT).show();
                        // reset_password();
                        open_changePassword();
                        break;

                    case R.id.txtFeedback:
                        Toast.makeText(getActivity(), "Change Language", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), ChangeLanguageActivity.class));
                        break;

                    case R.id.txtPP:
                        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
                        final View sheetView = getLayoutInflater().inflate(R.layout.privacy_policy_dialog, null);
                        mBottomSheetDialog.setContentView(sheetView);
                        mBottomSheetDialog.setCancelable(false);

                        final boolean[] loadingFinished = {true};
                        final boolean[] redirect = {false};
                        WebView webView = (WebView) sheetView.findViewById(R.id.privacy);

                        UltimateProgress.showProgressBar(getActivity(), "Please Wait.......!");
                        if (!loadingFinished[0]) {
                            redirect[0] = true;
                        }
                        loadingFinished[0] = false;
                        webView.getSettings().setJavaScriptEnabled(true);
                        String url = "http://ultimatesolutiongroup.com/includes/privacy_policy.html";
                        webView.loadUrl(url);
                        webView.setWebViewClient(new WebViewClient() {
                            @Override
                            public void onPageFinished(WebView view, String url) {
                                if (!redirect[0]) {
                                    loadingFinished[0] = true;
                                }
                                if (loadingFinished[0] && !redirect[0]) {
                                    UltimateProgress.cancelProgressBar();
                                } else {
                                    redirect[0] = false;
                                }
                            }
                        });

                        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                btnNo.startAnimation(animation4);
                                mBottomSheetDialog.dismiss();
                            }
                        });
                        mBottomSheetDialog.show();
                        break;
                    case R.id.txtInfo:
                        Toast.makeText(getActivity(), "App Info", Toast.LENGTH_SHORT).show();
                        oenInfoDialog();
                        break;

                    case R.id.txtLogout:
                        final Dialog logoutDialog = new Dialog(getActivity());
                        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        logoutDialog.setCancelable(true);
                        logoutDialog.setContentView(R.layout.logout_dialog);
                        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);

                        String logout_user = User.getCurrentUser().getFirstname();
                        //  txt2.setText(logout_user + "!" + " " + getString(R.string.log1));

                        String title1 = getColoredSpanned("<b>" + " " + logout_user + "!  " + "</b>", "#000000");
                        String Name = getColoredSpanned(getString(R.string.log1), "#5A5C59");
                        txt2.setText(Html.fromHtml(" " + title1 + " " + Name));

                        CircularImageView img = (CircularImageView) logoutDialog.findViewById(R.id.img);


                        if (User.getCurrentUser().getGender().equalsIgnoreCase("Male")) {
                            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                                Picasso.get().load(image_url).placeholder(R.drawable.boy).into(img);
                            } else {
                                Picasso.get().load(R.drawable.boy).into(img);
                            }

                        } else {
                            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                                Picasso.get().load(image_url).placeholder(R.drawable.girl).into(img);
                            } else {
                                Picasso.get().load(R.drawable.girl).into(img);
                            }
                        }

                        Button btnNo1 = (Button) logoutDialog.findViewById(R.id.btnNo);
                        btnNo1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                logoutDialog.dismiss();
                            }
                        });
                        Button btnYes = (Button) logoutDialog.findViewById(R.id.btnYes);
                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                logoutDialog.dismiss();
                                User.removeUserFromList();
                                User.logout();
                                Toast.makeText(getActivity(), logout_user + " Logout Successfully!", Toast.LENGTH_LONG).show();
                                if (User.getUserList() == null) {
                                    Intent loginpage = new Intent(getActivity(), LoginActivity.class);
                                    loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(loginpage);
                                    getActivity().finish();


                                } else {
                                    User.setCurrentUserUL(User.getUserList().get(0));
                                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    getActivity().finish();
                                }

                            }
                        });
                        logoutDialog.show();
                        Window window = logoutDialog.getWindow();
                        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        break;


                }

                //  Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();
                return true;
            }
        });

    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        // adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
        fetchbirth(date);
    }

    private void fetchbirth(String date) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userid", User.getCurrentUser().getId());
        params.put("date", date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.birthdaylist, apiCallbackstaffdetail, getActivity(), params);

    }

    ApiHandler.ApiCallback apiCallbackstaffdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            //txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                speech = 1;
                try {
                    image_url_birth = jsonObject.getJSONObject("birthday_data").getString("birthday_card");
                    openBirthDialog(image_url_birth);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                if (speech == 0) {
                    TextToSpeechFunction();
                }
            }
        }
    };

    private void openBirthDialog(String image_url_birth) {
        Dialog openBirthDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        final View sheetView = getLayoutInflater().inflate(R.layout.birthday_dialog_new, null);
        openBirthDialog.setContentView(sheetView);
//                mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        openBirthDialog.setCancelable(false);


        RelativeLayout scrollView = (RelativeLayout) sheetView.findViewById(R.id.parent);

        CircularImageView profile = (CircularImageView) sheetView.findViewById(R.id.profile);
        CircularImageView school_profile = (CircularImageView) sheetView.findViewById(R.id.school_profile);

        TextView birth_name = (TextView) sheetView.findViewById(R.id.birth_name);
        TextView school_name = (TextView) sheetView.findViewById(R.id.school_name);
        TextView std_name = (TextView) sheetView.findViewById(R.id.std_name);
        export = (ImageButton) sheetView.findViewById(R.id.export);
        close = (ImageView) sheetView.findViewById(R.id.close);

        // Define an array of strings
        String[] strings = {String.valueOf(R.string.bday1), String.valueOf(R.string.bday2), String.valueOf(R.string.bday3), String.valueOf(R.string.bday4), String.valueOf(R.string.bday5), String.valueOf(R.string.bday6), String.valueOf(R.string.bday7), String.valueOf(R.string.bday8), String.valueOf(R.string.bday9)};
        // Generate a random index
        Random random = new Random();
        int randomIndex = random.nextInt(strings.length);
        // Select the string at the random index
        String selectedString = strings[randomIndex];
        // Do something with the selected string (e.g., print it)
        // System.out.println(selectedString);
        birth_name.setText(getResources().getText(Integer.parseInt(selectedString)));

        std_name.setText(User.getCurrentUser().getFirstname() + " " + User.getCurrentUser().getLastname());
        school_name.setText(User.getCurrentUser().getSchoolData().getName());
        //school_name.setText(User.getCurrentUser().getSchoolData().getName()+"\n"+User.getCurrentUser().getSchoolData().getName());

        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.color.transparent).into(school_profile);
        } else {
            Picasso.get().load(R.color.transparent).into(school_profile);
        }

        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
            Picasso.get().load(image_url).placeholder(R.color.transparent).into(profile);
        } else {
            Picasso.get().load(R.color.transparent).into(profile);
        }


        final MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.bdaytone);
        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBirthDialog.dismiss();
                close.startAnimation(animation);
                mp.release();
                saveData(openBirthDialog);


            }
        });

        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                export.startAnimation(animation);
                export.setVisibility(View.INVISIBLE);
                close.setVisibility(View.INVISIBLE);
//                viewed.setVisibility(View.VISIBLE);
                //  mBottomSheetDialog1.dismiss();
                Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
                // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                // v1.setDrawingCacheEnabled(false);
                saveImageToGallery(bitmap, User.getCurrentUser().getFirstname() + " " + User.getCurrentUser().getLastname());

            }
        });

        openBirthDialog.show();
        Window window = openBirthDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void saveImageToGallery(Bitmap bitmap, String name) {

        OutputStream fos;
        String school = User.getCurrentUser().getSchoolData().getName();
        long time = System.currentTimeMillis();
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                ContentResolver resolver = getActivity().getContentResolver();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name + "_BirthdayCard" + time + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));

                Toast.makeText(getActivity(), "Image Saved", Toast.LENGTH_SHORT).show();

            } else {
                String imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, name + "_BirthdayCard" + time + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(getActivity(), "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);


        } catch (Exception e) {

            Toast.makeText(getActivity(), "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        export.setVisibility(View.VISIBLE);
        close.setVisibility(View.VISIBLE);
    }

    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) bgDrawable.draw(canvas);
        else canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }

    private void saveData(Dialog openBirthDialog) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userid", User.getCurrentUser().getId());
        params.put("check", "insert");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.birthdaylist, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                openBirthDialog.dismiss();
                if (error == null) {
                    Toast.makeText(getActivity(), "Close", Toast.LENGTH_SHORT).show();
                } else {
                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        }, getActivity(), params);


    }

    private void userprofile() {

        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("image", "");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                UltimateProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        JSONObject userData = jsonObject.getJSONObject(Constants.USERDATA);
                        image_url = userData.getString("profile");

                        if (!userData.isNull("es_housename")) {
                            house_name = userData.getString("es_housename");
                            Log.i("USERDATA-Home", house_name);
                        }
                        Utils.setNaviHeaderData(txtStudName, class_name, imgStud, image_url, getActivity());

                        if (house_name != null && !house_name.isEmpty()) {
                            String text = "House: " + house_name;
                            Utils.setNaviHeaderDataHouse(stu_name, class_namessss, navi_profile, image_url, getActivity(), text);
                        } else {
                            Utils.setNaviHeaderData(stu_name, class_namessss, navi_profile, image_url, getActivity());

                        }
                        getTodayDate();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, getActivity(), params);
    }


    private void setShowcaseView() {
        new TapTargetSequence(getActivity()).targets(TapTarget.forView(mFloatingNavigationView, getString(R.string.o_icon), getString(R.string.o_body)).outerCircleColor(R.color.light).outerCircleAlpha(0.96f).targetCircleColor(R.color.white).titleTextSize(20).titleTextColor(R.color.white).descriptionTextSize(14).descriptionTextColor(R.color.black).textColor(R.color.black).textTypeface(Typeface.SANS_SERIF).dimColor(R.color.black).drawShadow(true).cancelable(false).tintTarget(true).transparentTarget(true).targetRadius(60), TapTarget.forView(imgProfile, getString(R.string.p_icon), getString(R.string.p_body)).outerCircleColor(R.color.light).outerCircleAlpha(0.96f).targetCircleColor(R.color.white).titleTextSize(20).titleTextColor(R.color.white).descriptionTextSize(14).descriptionTextColor(R.color.black).textColor(R.color.black).textTypeface(Typeface.SANS_SERIF).dimColor(R.color.black).drawShadow(true).cancelable(false).tintTarget(true).transparentTarget(true).targetRadius(60), TapTarget.forView(view_noticeBoard, getString(R.string.notice_icon), getString(R.string.notice_body)).outerCircleColor(R.color.light).outerCircleAlpha(0.96f).targetCircleColor(R.color.white).titleTextSize(20).titleTextColor(R.color.white).descriptionTextSize(14).descriptionTextColor(R.color.black).textColor(R.color.black).textTypeface(Typeface.SANS_SERIF).dimColor(R.color.black).drawShadow(true).cancelable(false).tintTarget(true).transparentTarget(true).targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData() {
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit", true);
        editor.apply();
    }

    private boolean restorePrefData() {
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit", false);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchNoticeBoardList();
        fetchHolidayList();
        fetchthoughtlist();
        newslist();
        fetchWeekAttend();
        userprofile();
        // setShowcaseView();
    }


    private void newslist() {
        // i++;
        // cancelProgressBar.show();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEWS_URL, apiCallback5, getActivity(), null);

    }

    ApiHandler.ApiCallback apiCallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            //  cancelProgressBar.dismiss();

            if (error == null) {
                try {
                    if (new_sliderDataArrayList != null) {
                        new_sliderDataArrayList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("news_data");
                    Log.e("jsonArray", String.valueOf(jsonArray));
                    new_sliderDataArrayList = NewsData.parseNewsDataArrayyy(jsonArray);

                    Log.e("image2", String.valueOf(new_sliderDataArrayList.get(0).getImage()));
                    if (new_sliderDataArrayList.get(0).getDate() != null) {
                        Log.e("image", new_sliderDataArrayList.get(0).getDate());
                    } else {
                        Log.e("image", "empty");
                    }
                    //Top SliderView
                    setUpSliderShow(new_sliderDataArrayList);
                    shimmer.stopShimmer();
                    shimmer.setVisibility(View.GONE);
                    f_view1.setVisibility(View.GONE);
                    lyt1.setVisibility(View.VISIBLE);
                    //setanimation on adapter...
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                f_view1.setVisibility(View.GONE);
                lyt1.setVisibility(View.VISIBLE);
                //      Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    Timer swipeTimer;
    Handler handler1;

    private void setUpSliderShow(ArrayList<NewsData> newSliderDataArrayList) {

        NUM_PAGES =newSliderDataArrayList.size();
        b_adapter = new B_SliderAdapter(getContext(), new_sliderDataArrayList);
        viewPager.setAdapter(b_adapter);
        indicator.setViewPager(viewPager);
        // Auto start of viewpager
        // Ensure handler1 and Update Runnable are not null
        final Runnable Update = new Runnable() {
            public void run() {
                if (viewPager == null || b_adapter == null) {
                    return; // Exit if viewPager or b_adapter is null
                }
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        // Stop the previous timer if it's running
        if (swipeTimer != null) {
            swipeTimer.cancel();
            swipeTimer.purge();
        }

        // Create a new timer
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (handler1 != null) {
                    handler1.post(Update);
                }
            }
        }, 2000, 2000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }
            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }
            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }


    //text to speech programmatically...
    public void TextToSpeechFunction() {
        speech = 1;

        // String textholder = "Hi "+ User.getCurrentUser().getFirstname() +"!"+" Welcome to Ultimate solution";
        String textholder = "Hi " + User.getCurrentUser().getFirstname() + "! Welcome to " + User.getCurrentUser().getSchoolData().getName();

        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);
        //  Toast.makeText(getActivity() , textholder+speech, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onDestroy() {

        textToSpeech.shutdown();

        super.onDestroy();
    }

//    @Override
//    public void onInit(int status) {
//
//    }

    @Override
    public void onInit(int Text2SpeechCurrentStatus) {

        if (Text2SpeechCurrentStatus == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
//            if (speech == 0){
//                TextToSpeechFunction();
//             }
        }

    }

    @SuppressLint({"RestrictedApi", "SetTextI18n"})
    private void oenInfoDialog() {

        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.app_info_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);


        TextView txtVersion = (TextView) sheetView.findViewById(R.id.txtVersion);
        FloatingActionButton whatsapp = (FloatingActionButton) sheetView.findViewById(R.id.whatsapp);
        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
        String number = "7009091606";
        String versionName = "";
        int code = 0;
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            versionName = pInfo.versionName;
            code = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
//        try {
//            versionName = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }

        txtVersion.setText("App Version Code: " + code + "\n" + "App Version Name: " + String.valueOf(versionName));

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                UltimateProgress.showProgressBar(getActivity(), "Please Wait.....!");
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                //simplydirect email for setting email,,,
                intent.setData(Uri.parse("mailto:xyz.ultimatesolution@gmail.com")); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
                UltimateProgress.cancelProgressBar();

            }
        });
        String wnumber = "7009091606";
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                Uri uri = Uri.parse("smsto:" + wnumber);
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, "Chat Now!"));
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mBottomSheetDialog.dismiss();
                // for permission granted....
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                    UltimateProgress.cancelProgressBar();
                }
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();

//
//        TextView txtVersion = (TextView) sheetView.findViewById(R.id.txtVersion);
//        FloatingActionButton whatsapp = (FloatingActionButton) sheetView.findViewById(R.id.whatsapp);
//        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
//        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
//        String number = "7009091606";
//        String versionName = "";
//        int code = 0;
//        try {
//            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
//            versionName = pInfo.versionName;
//            code = pInfo.versionCode;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
////        try {
////            versionName = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
////        } catch (PackageManager.NameNotFoundException e) {
////            e.printStackTrace();
////        }
//
//        txtVersion.setText("App Version Code: " + code + "\n" + "App Version Name: " + String.valueOf(versionName));
//
//        email.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //  mBottomSheetDialog.dismiss();
//                UltimateProgress.showProgressBar(getActivity(), "Please Wait.....!");
//                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
//                intent.setType("text/plain");
//                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
//                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
//                //simplydirect email for setting email,,,
//                intent.setData(Uri.parse("mailto:xyz.ultimatesolution@gmail.com")); // or just "mailto:" for blank
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
//                startActivity(intent);
//                UltimateProgress.cancelProgressBar();
//
//            }
//        });
//        whatsapp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //  mBottomSheetDialog.dismiss();
//                Uri uri = Uri.parse("smsto:" + number);
//                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
//                i.setPackage("com.whatsapp");
//                startActivity(Intent.createChooser(i, "Chat Now!"));
//            }
//        });
//        phone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // mBottomSheetDialog.dismiss();
//                // for permission granted....
//                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
//                } else {
//                    Intent intent = new Intent(Intent.ACTION_CALL);
//                    intent.setData(Uri.parse("tel:" + number));
//                    startActivity(intent);
//                    UltimateProgress.cancelProgressBar();
//                }
//            }
//        });
//
//
//        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
//        btnNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnNo.startAnimation(animation4);
//                mBottomSheetDialog.dismiss();
//            }
//        });
//        mBottomSheetDialog.show();

    }

    private void reset_password() {


        dialogLog = new Dialog(getActivity());
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.logout_dialog);
        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView txt1 = (TextView) dialogLog.findViewById(R.id.txt1);
        TextView txt2 = (TextView) dialogLog.findViewById(R.id.txt2);
        txt1.setText(" " + getString(R.string.password));
        String logout_user = User.getCurrentUser().getFirstname();
        //txt2.setText(logout_user + "!" + " " + getString(R.string.change_pass));


        String title1 = getColoredSpanned("<b>" + " " + logout_user + "!  " + "</b>", "#000000");
        String Name = getColoredSpanned(getString(R.string.change_pass), "#5A5C59");
        txt2.setText(Html.fromHtml(" " + title1 + " " + Name));

        CircularImageView img = (CircularImageView) dialogLog.findViewById(R.id.img);


        if (User.getCurrentUser().getGender().equalsIgnoreCase("Male")) {
            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.boy).into(img);
            } else {
                Picasso.get().load(R.drawable.boy).into(img);
            }

        } else {
            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.girl).into(img);
            } else {
                Picasso.get().load(R.drawable.girl).into(img);
            }
        }

        Button btnNo1 = (Button) dialogLog.findViewById(R.id.btnNo);
        btnNo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLog.dismiss();
            }
        });
        Button btnYes = (Button) dialogLog.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_layout();

            }
        });
        dialogLog.show();
        Window window = dialogLog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


//        dialogLog = new Dialog(getActivity());
//        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialogLog.setCancelable(false);
//        dialogLog.setContentView(R.layout.enter_mobile_dialog);
//        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        Button btnYes = (Button) dialogLog.findViewById(R.id.btnYes);
//        TextView prod_name = (TextView) dialogLog.findViewById(R.id.txt2);
//        CircularImageView img = (CircularImageView) dialogLog.findViewById(R.id.img);
//
//
//        if (User.getCurrentUser().getGender().equalsIgnoreCase("Male")) {
//            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
//                Picasso.get().load(image_url).placeholder(R.drawable.boy).into(img);
//            } else {
//                Picasso.get().load(R.drawable.boy).into(img);
//            }
//
//        }else{
//            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
//                Picasso.get().load(image_url).placeholder(R.drawable.girl).into(img);
//            } else {
//                Picasso.get().load(R.drawable.girl).into(img);
//            }
//        }
//
//
//        btnYes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //   SmartKidsClubProgress.showProgressBar(ProductDiscActivity.this,"Please wait...");
//                btnYes.startAnimation(animation4);
//               // Toast.makeText(getActivity(), "Done", Toast.LENGTH_SHORT).show();
//                open_layout();
//            }
//        });
//        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
//        btnNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnNo.startAnimation(animation4);
//                dialogLog.dismiss();
//            }
//        });
//        dialogLog.show();

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void open_layout() {

        List<AuthUI.IdpConfig> providers = Arrays.asList(new AuthUI.IdpConfig.PhoneBuilder().build());

// Create and launch sign-in intent
        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setTheme(R.style.phoneTheme).setLogo(R.drawable.logo).setAvailableProviders(providers).setIsSmartLockEnabled(false, true).build(), VERIFY_NUMBER);

        AuthUI.getInstance().signOut(getActivity()).addOnCompleteListener(new OnCompleteListener<Void>() {
            public void onComplete(@NonNull Task<Void> task) {
                //              Toast.makeText(LoginActivity.this,"SignOut",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == VERIFY_NUMBER) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                get_number = response.getPhoneNumber();
                //  Toast.makeText(LoginActivity.this,"Number "+get_number,Toast.LENGTH_SHORT).show();
                Log.e("number", get_number);
                numberCheck(get_number);
            } else {
                Toast.makeText(getActivity(), "Sign in failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void numberCheck(String get_number) {

        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("std_id", User.getCurrentUser().getId());
        params.put("number", get_number);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.VERIFY_MOBILE, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                UltimateProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    dialogLog.dismiss();
                    Toast.makeText(getActivity(), "Registered Number Matched Successfully!", Toast.LENGTH_SHORT).show();
                    open_changePassword();
                } else {
                    if (error.getStatusCode() == 401) {
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                        //  Toast.makeText(getActivity(),"error"+get_number, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, getActivity(), params);

    }

    private void open_changePassword() {

        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.reset_password_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(false);
        CircularImageView imgStud1 = (CircularImageView) sheetView.findViewById(R.id.imgStud);
        TextView txtStudName1 = (TextView) sheetView.findViewById(R.id.txtStudName);
        TextView class_name1 = (TextView) sheetView.findViewById(R.id.class_name);
        TextView roll_no1 = (TextView) sheetView.findViewById(R.id.roll_no);

        Utils.setNaviHeaderData(txtStudName1, class_name1, imgStud1, image_url, getActivity());

        if (User.getCurrentUser().getId() != null) {
            roll_no1.setText("Admission No: " + User.getCurrentUser().getId());
        } else {
            roll_no1.setText("");
        }

        final EditText edtPassword1 = (EditText) sheetView.findViewById(R.id.edtPassword1);
        final EditText edtPassword2 = (EditText) sheetView.findViewById(R.id.edtPassword2);
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });
        Button btnYes = (Button) sheetView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnYes.startAnimation(animation4);
                if (edtPassword1.getText().toString().trim().length() <= 0) {
                    //    edtPassword1.setError("Enter Password");
                    Toast.makeText(getActivity(), "Enter Password!", Toast.LENGTH_SHORT).show();
                } else if (edtPassword2.getText().toString().trim().length() <= 0) {
                    // edtPassword2.setError("Re-Enter Password");
                    Toast.makeText(getActivity(), "Re-Enter Password!", Toast.LENGTH_SHORT).show();
                } else {
                    password = edtPassword1.getText().toString();
                    c_password = edtPassword2.getText().toString();
                    if (password.equalsIgnoreCase(c_password)) {
                        change_password_api();
                    } else {
                        //  edtPassword2.setError("Re-Enter Password Not Matched!");
                        Toast.makeText(getActivity(), "Re-Enter Password Not Matched!", Toast.LENGTH_SHORT).show();
                    }
                }
                // Toast.makeText(getActivity(),"Reset Password",Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog.show();
    }

    private void change_password_api() {
        Utils.hideKeyboard(getActivity());
        HashMap<String, String> params = new HashMap<String, String>();
        //   params.put("user_id", User.getCurrentUser().getId());
        params.put("pass", c_password);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.PASS_RESET_URL, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                Toast.makeText(getActivity(), "Password Reset Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            } else {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(ResetPass.this, LoginActivity.class));
//                    finish();
//                }
            }
        }

    };


    private void fetchHolidayList() {
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.HOLIDAY_URL, apiCallback1, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    if (holidayList != null) holidayList.clear();
                    JSONArray hArray = jsonObject.getJSONArray("holiday_data");
                    holidayList = HolidayBean.parseArray(hArray);
                    hAdapter.setHList(holidayList);
                    //setanimation on adapter...
                    holiRV.getAdapter().notifyDataSetChanged();
                    holiRV.scheduleLayoutAnimation();
                    //-----------end------------
                    holiday_shimmer.stopShimmer();
                    holiday_shimmer.setVisibility(View.GONE);
                    holiRV.setVisibility(View.VISIBLE);
                    noHolidayData.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                holiday_shimmer.stopShimmer();
                holiday_shimmer.setVisibility(View.GONE);
                holidayList.clear();
                hAdapter.setHList(holidayList);
                hAdapter.notifyDataSetChanged();
                holiRV.setVisibility(View.GONE);
                noHolidayData.setVisibility(View.VISIBLE);
                if (error.getStatusCode() == 405) {
                    User.removeUserFromList();
                    User.logout();
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    if (User.getUserList() == null) {
                        Intent loginpage = new Intent(getActivity(), LoginActivity.class);
                        loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(loginpage);
                        getActivity().finish();


                    } else {
                        User.setCurrentUserUL(User.getUserList().get(0));
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    Animatoo.animateShrink(getActivity());

                }
            }
        }
    };

    private void fetchNoticeBoardList() {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "1");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTICE_URL, napiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback napiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    if (noticeList != null) noticeList.clear();
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    noticeList = NoticeBean.parseNoticetArray(noticeArray);
                    nbAdapter.setNList(noticeList);
                    //setanimation on adapter...
                    nbRV.getAdapter().notifyDataSetChanged();
                    nbRV.scheduleLayoutAnimation();
                    //-----------end------------
                    nbRV.setVisibility(View.VISIBLE);
                    noti_shimmer.stopShimmer();
                    noti_shimmer.setVisibility(View.GONE);
                    noNoticeData.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                noticeList.clear();
                noti_shimmer.stopShimmer();
                noti_shimmer.setVisibility(View.GONE);
                nbAdapter.setNList(noticeList);
                nbAdapter.notifyDataSetChanged();
                nbRV.setVisibility(View.GONE);
                noNoticeData.setVisibility(View.VISIBLE);
                if (error.getStatusCode() == 405) {
                    User.removeUserFromList();
                    User.logout();
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    if (User.getUserList() == null) {
                        Intent loginpage = new Intent(getActivity(), LoginActivity.class);
                        loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(loginpage);
                        getActivity().finish();


                    } else {
                        User.setCurrentUserUL(User.getUserList().get(0));
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    Animatoo.animateShrink(getActivity());
                }

            }
        }
    };

    private void startAnimation() {
        animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.logo_animation);
        animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left_animation);
        animation3 = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_animation);
        animation4 = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        noHolidayData.startAnimation(animation1);
        noNoticeData.startAnimation(animation1);
        imgStud.startAnimation(animation2);
        txtStudName.startAnimation(animation3);
        class_name.startAnimation(animation3);
        roll_no.startAnimation(animation3);
        viewPager.startAnimation(animation3);
    }


    private void fetchWeekAttend() {
        /*Calculate start end date of week*/
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.DAY_OF_WEEK, 2);  //monday
        SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd"); //this format changeable
        String start_date = dateFormatter1.format(c1.getTime());

        c1.set(Calendar.DAY_OF_WEEK, 7);  //saturday
        String end_date = dateFormatter1.format(c1.getTime());

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, User.getCurrentUser().getId());
        params.put("start_date", start_date);
        params.put("end_date", end_date);
        Log.e("start_date", start_date);
        Log.e("end_date", end_date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ATTENDANCE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        Log.e("hiiii", "hiii");
                        attendList.clear();
                        attendList = AttendMod.parseAttendArray(jsonObject.getJSONArray("attend_data"));
                        attendAdapter.setAList(attendList);
                        attendAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("hiiii2", "hiii2");
                    attendList.clear();
                    attendAdapter.setAList(attendList);
                    attendAdapter.notifyDataSetChanged();
                    if (error.getStatusCode() == 405) {
                        User.removeUserFromList();
                        User.logout();
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                        if (User.getUserList() == null) {
                            Intent loginpage = new Intent(getActivity(), LoginActivity.class);
                            loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(loginpage);
                            getActivity().finish();


                        } else {
                            User.setCurrentUserUL(User.getUserList().get(0));
                            Intent intent = new Intent(getActivity(), HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                        Animatoo.animateShrink(getActivity());
                    }

                }
            }
        }, getActivity(), params);
    }


    private void checkView() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.unactive_dots));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dots));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

//        if (dotscount == 0){
//            Toast.makeText(getActivity(),"Next "+dotscount,Toast.LENGTH_SHORT).show();
//            createSlideShow();
//        } else {
//            Toast.makeText(getActivity(),"Stop ",Toast.LENGTH_SHORT).show();
//        }

    }

    private void fetchthoughtlist() {
        // ErpProgress.showProgressBar(getActivity(),"Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.VIEWTHOUGHT, apiCallbackgggg, getActivity(), params);

    }

    ApiHandler.ApiCallback apiCallbackgggg = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();

            if (error == null) {
                try {
                    if (eventList != null) {
                        eventList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("thought_data");
                    eventList = Thought_bean.parseALBMlistarray(jsonArray);
                    thought_shimmer.stopShimmer();
                    thought_shimmer.setVisibility(View.GONE);
                    thought.setVisibility(View.VISIBLE);
                    thought.setText(eventList.get(0).getMessage());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                thought_shimmer.stopShimmer();
                thought_shimmer.setVisibility(View.GONE);
                thought.setVisibility(View.GONE);
                eventList.clear();

            }
        }
    };

    private void createSlideShow() {
        //  handler= new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (current_pos == dotscount) {
                    current_pos = 0;
                }
                viewPager.setCurrentItem(current_pos++, true);
            }
        };

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }

        }, 2500, 2500);

    }

    public void sendRequest() {
        UltimateProgress.cancelProgressBar();
        commonProgress.dismiss();
        count++;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, request_url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                UltimateProgress.cancelProgressBar();
                Log.e("Response", String.valueOf(response));
                Log.e("Length", String.valueOf(response.length()));
                for (int i = 0; i < response.length(); i++) {

                    BannerBean sliderUtils = new BannerBean();

                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        // comment is original code...
                        //sliderUtils.setCateImage(request_url1+jsonObject.getString("CateImage"));
                        sliderUtils.setBaner_image(request_url1 + jsonObject.getString("baner_image"));
                        sliderUtils.setTitle(jsonObject.getString("title"));
                        Log.e("Image_url", sliderUtils.getBaner_image());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    sliderImg.add(sliderUtils);

                }

                viewPagerAdapter = new ViewPagerAdapter(sliderImg, getActivity());

                viewPager.setAdapter(viewPagerAdapter);

                dotscount = viewPagerAdapter.getCount();
                // new code
                if (sliderDotspanel.getChildCount() > 0) {
                    sliderDotspanel.removeAllViews();
                }
                // new code
                dots = new ImageView[dotscount];

                for (int i = 0; i < dotscount; i++) {

                    dots[i] = new ImageView(getActivity());
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.unactive_dots));

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    params.setMargins(8, 0, 8, 0);

                    sliderDotspanel.addView(dots[i], params);

                }

                dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dots));

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        CustomVolleyRequest.getInstance(getActivity()).addToRequestQueue(jsonArrayRequest);

    }

    private void setUserData() {

        if (User.getCurrentUser().getId() != null) {
            roll_no.setText("Admission No: " + User.getCurrentUser().getId());
        } else {
            roll_no.setText("");
        }
        if (User.getCurrentUser().getSchoolData().getName() != null) {
            headtext.setText(User.getCurrentUser().getSchoolData().getName());
        }
    }

    @OnClick(R.id.imgProfile)
    public void imgProfilesss() {
        imgProfile.startAnimation(animation4);
        //     Toast.makeText(getActivity(),"View Profile Details",Toast.LENGTH_SHORT).show();
        //    startActivity(new Intent(getActivity(), SliderActivity.class));
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.profile_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);
        imgPro = (CircularImageView) sheetView.findViewById(R.id.circleimgrecepone);
        ImageView edit_profile1 = (ImageView) sheetView.findViewById(R.id.edit_profile);
        ImageView img = (ImageView) sheetView.findViewById(R.id.myimage);
        txtClass = (TextView) sheetView.findViewById(R.id.classtxtone);
        txtRegNo = (TextView) sheetView.findViewById(R.id.txtRegNo);
        txtDOB = (TextView) sheetView.findViewById(R.id.txtDOB);
        txtAadhar = (TextView) sheetView.findViewById(R.id.txtAadhar);
        txtGender = (TextView) sheetView.findViewById(R.id.txtGender);
        txtAddress = (TextView) sheetView.findViewById(R.id.txtAddress);
        txtContact = (TextView) sheetView.findViewById(R.id.txtContact);
        txtEmail = (TextView) sheetView.findViewById(R.id.txtEmail);
        txtbg = (TextView) sheetView.findViewById(R.id.txtbg);
        txtBoard = (TextView) sheetView.findViewById(R.id.txtBoard);
        txtName = (TextView) sheetView.findViewById(R.id.nametxtone);
        createQR(img);
        setProfileData();

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        edit_profile1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edit_profile1.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                Animatoo.animateShrink(getActivity());

            }
        });

        mBottomSheetDialog.show();
    }

    private void createQR(ImageView img) {
        String inputValue = "";
        if (User.getCurrentUser() != null) {
            inputValue = User.getCurrentUser().getId();
        }
        if (!inputValue.equalsIgnoreCase("")) {
            //Toast.makeText(getApplicationContext(),"hiiii "+inputValue,Toast.LENGTH_LONG).show();
            QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 800);
//            qrgEncoder.setColorBlack(R.color.dark_black);
//            qrgEncoder.setColorWhite(R.color.white);
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.getBitmap();
            // Setting Bitmap to ImageView
            img.setImageBitmap(bitmap);

        }
    }

    private void setProfileData() {
        User user = User.getCurrentUser();
        if (User.getCurrentUser().getGender().equalsIgnoreCase("male")) {
            txtName.setText(user.getFirstname() + " " + user.getLastname() + " S/O " + user.getFather());
        } else {
            txtName.setText(user.getFirstname() + " " + user.getLastname() + " D/O " + user.getFather());
        }
        if (User.getCurrentUser().getSection_name() != null) {
            txtClass.setText(user.getClass_name() + "(" + user.getSection_name() + ")");
        } else {
            txtClass.setText(user.getClass_name());
        }

        if (user.getEmail().length() > 0) {
            txtEmail.setText(user.getEmail());
        } else {
            txtEmail.setText("Not Mentioned");
        }
        if (user.getPhoneno().length() > 0) {
            txtContact.setText(user.getPhoneno());
        } else {
            txtContact.setText("Not Mentioned");
        }
        if (user.getGender().length() > 0) txtGender.setText(user.getGender());
        if (user.getAadhaar().length() > 0) txtAadhar.setText(user.getAadhaar());
        if (user.getAddress().length() > 0) txtAddress.setText(user.getAddress());
        if (user.getBg().length() > 0) {
            txtbg.setText(user.getBg());
        } else {
            txtbg.setText("Not Mentioned");
        }
        if (user.getId().length() > 0) {
            txtRegNo.setText(user.getId());
        }

        if (user.getBoard() != null) {
            txtBoard.setText(user.getBoard());
        } else {
            txtBoard.setText("Not Mentioned");
        }


        if (user.getDob().length() > 0) txtDOB.setText(Utils.dateFormat(user.getDob()));


        if (User.getCurrentUser().getGender().equalsIgnoreCase("Male")) {
            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.boy).into(imgPro);
            } else {
                Picasso.get().load(R.drawable.boy).into(imgPro);
            }
        } else {
            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.girl).into(imgPro);

            } else {
                Picasso.get().load(R.drawable.girl).into(imgPro);
            }
        }


    }

    @OnClick(R.id.view_noticeBoard)
    public void view_noticeBoardss() {
        view_noticeBoard.startAnimation(animation4);
        Toast.makeText(getActivity(), "View Notice Board List", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), NoticeActivity.class));
    }

    @OnClick(R.id.view_holidayList)
    public void view_holidayListss() {
        view_holidayList.startAnimation(animation4);
        Toast.makeText(getActivity(), "View Holiday List", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), HolidayActivity.class));
    }


}
