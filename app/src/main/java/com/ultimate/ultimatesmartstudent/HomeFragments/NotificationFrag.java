package com.ultimate.ultimatesmartstudent.HomeFragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.floatingnavigationview.FloatingNavigationView;
import com.android.volley.Request;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AccountModule.AccountActivity;
import com.ultimate.ultimatesmartstudent.AccountModule.SchoolInfo;
import com.ultimate.ultimatesmartstudent.Assignment.MainAssignActivity;
import com.ultimate.ultimatesmartstudent.AttendMod.AttendanceActivity;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.GDSClasswork;
import com.ultimate.ultimatesmartstudent.Datesheet.Datesheet;
import com.ultimate.ultimatesmartstudent.E_LearningMod.VideoActivity;
import com.ultimate.ultimatesmartstudent.E_LearningMod.ViewOnlineClassLink;
import com.ultimate.ultimatesmartstudent.E_book.Ebook;
import com.ultimate.ultimatesmartstudent.EventMod.AchievementList;
import com.ultimate.ultimatesmartstudent.EventMod.ParticipationListStud;
import com.ultimate.ultimatesmartstudent.FeeModule.NewFees.CommonFeesListActivity;
import com.ultimate.ultimatesmartstudent.Gallery.GDSGalleryAct;
import com.ultimate.ultimatesmartstudent.GatePass.NewGatePass.GatePass;
import com.ultimate.ultimatesmartstudent.Health_Module.View_Health;
import com.ultimate.ultimatesmartstudent.Homework.SubWiseHW;
import com.ultimate.ultimatesmartstudent.Leave_Mod.Leavelist_Activity;
import com.ultimate.ultimatesmartstudent.LiveClassModule.SheduleOnlineClassActivityList;
import com.ultimate.ultimatesmartstudent.Messages.All_Activity.MessageActivity;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeActivity;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeAdapter;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeBean;
import com.ultimate.ultimatesmartstudent.Notification.NotiSpinner;
import com.ultimate.ultimatesmartstudent.Notification.NotificationBean;
import com.ultimate.ultimatesmartstudent.Notification.NotificationHomeAdapter;
import com.ultimate.ultimatesmartstudent.Performance_Report.CleanlinessList;
import com.ultimate.ultimatesmartstudent.Performance_Report.CommunicationList;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SchoolActivity.ViewStudents;
import com.ultimate.ultimatesmartstudent.Social_Post.SocialLinkList;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.SyllabusMod.SyllabusActivity;
import com.ultimate.ultimatesmartstudent.TestMod.GDSTest;
import com.ultimate.ultimatesmartstudent.TimetableMod.ViewTimeTable;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;


public class NotificationFrag extends Fragment implements TextToSpeech.OnInitListener, NotificationHomeAdapter.Mycallback {


    @BindView(R.id.noti_shimmer)
    ShimmerFrameLayout noti_shimmer;
    private FloatingNavigationView mFloatingNavigationView;
    Animation animation1,animation2;
    @BindView(R.id.noNotification)
    TextView noNotification;
    @BindView(R.id.recyclerView)
    RecyclerView notiRV;
    TextView stu_name,class_namessss;
    ImageView imgMoreUser;
    CircularImageView navi_profile;
    TextToSpeech textToSpeech;
    ArrayList<NotificationBean> notiList = new ArrayList<>();;
    private NotificationHomeAdapter notiAdapter;
    @BindView(R.id.schName)
    TextView schName;
    @BindView(R.id.imgLogo)
    CircularImageView imgLogo;
    String textholder="",title="",date="";
    int count=0,speech=0;
    ArrayList<NotificationBean> classLists = new ArrayList<>();
    @BindView(R.id.spinnerGroups)
    Spinner spinnerGroups;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    SharedPreferences sharedPreferences;

    @BindView(R.id.msg_count)
    TextView msg_count;
    @BindView(R.id.lytttt)
    RelativeLayout lytttt;

    @BindView(R.id.fees_count)
    TextView fees_count;
    @BindView(R.id.lytttt_fees)
    RelativeLayout lytttt_fees;

    @BindView(R.id.lytttt_school)
    RelativeLayout lytttt_school;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_notification, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.logo_animation);
        animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        noNotification.startAnimation(animation1);
        setSchoolData();
        textToSpeech = new TextToSpeech(getActivity(), this);
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId);

        notiRV.setLayoutAnimation(controller);

        lytttt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msg_count.startAnimation(animation1);
                Intent intent= new Intent(getActivity(), MessageActivity.class);
                intent.putExtra("Today","Today");
                startActivity(intent);
            }
        });

        lytttt_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lytttt_school.startAnimation(animation1);
                // viewSchoolInfo();
                Intent intent= new Intent(getActivity(), SchoolInfo.class);
                startActivity(intent);
            }
        });

        getTodayDate();
        /* Notification section*/
        notiList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
        notiRV.setLayoutManager(layoutManager1);
        notiAdapter = new NotificationHomeAdapter(notiList, getActivity(),this);
        notiRV.setAdapter(notiAdapter);
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    private void viewSchoolInfo() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.school_info_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);


        TextView txtHead = (TextView) sheetView.findViewById(R.id.txtHead);
        TextView c_name = (TextView) sheetView.findViewById(R.id.c_name);
        TextView txtAdd = (TextView) sheetView.findViewById(R.id.txtAdd);


        CircularImageView circleimgrecepone = (CircularImageView) sheetView.findViewById(R.id.circleimgrecepone);

        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
        String number = User.getCurrentUser().getSchoolData().getPhoneno();
        String mail = User.getCurrentUser().getSchoolData().getEmail();

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
            txtHead.setText("College Details");
        }else{
            txtHead.setText("School Details");
        }


        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(circleimgrecepone);
        } else {
            Picasso.get().load(R.drawable.logo).into(imgLogo);
        }
        if (User.getCurrentUser().getSchoolData().getName() != null){
            c_name.setText(User.getCurrentUser().getSchoolData().getName());
        }

        if (User.getCurrentUser().getSchoolData().getName() != null){
            txtAdd.setText(User.getCurrentUser().getSchoolData().getAddress());
        }

        if (mail!=null || mail.equalsIgnoreCase("")) {
            email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  mBottomSheetDialog.dismiss();
                 //   UltimateProgress.showProgressBar(getActivity(), "Please Wait.....!");
                    Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                    intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                    //simplydirect email for setting email,,,
                    intent.setData(Uri.parse("mailto:" + mail)); // or just "mailto:" for blank
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                    startActivity(intent);
                    UltimateProgress.cancelProgressBar();

                }
            });
        }else{
            Toast.makeText(getActivity(),"Email Not Found!",Toast.LENGTH_LONG).show();
        }


        if (number!=null || number.equalsIgnoreCase("")) {
            phone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // mBottomSheetDialog.dismiss();
                    // for permission granted....
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},1);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + number));
                        startActivity(intent);
                        UltimateProgress.cancelProgressBar();
                    }
                }
            });
        }else{
            Toast.makeText(getActivity(),"Number Not Found!",Toast.LENGTH_LONG).show();
        }


        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(getView() != null && isVisibleToUser){
            Log.i("1st","1st");
            if (!restorePrefData()){
                setShowcaseView();
            }
        }
    }

    private void setShowcaseView() {
        new TapTargetSequence(getActivity())
                .targets(
                        TapTarget.forView(lytttt_school,getString(R.string.s_icon),getString(R.string.s_body))
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt,getString(R.string.m_icon),getString(R.string.m_body))
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData(){
        sharedPreferences=getActivity().getSharedPreferences("boarding_pref_tool",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_tool",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_tool",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_tool",false);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchNotificationSpinner();
        fetnewmsg();
    }
    private void fetnewmsg() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        params.put("check","list");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG, apicallback5, getActivity(), params);
    }

    ApiHandler.ApiCallback apicallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();

            if (error == null) {

                try {
                    Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                    JSONObject docData = jsonObject.getJSONObject("doc_data");
                    int notiCount = 0; // Default value
                    if (docData.has("count") && !docData.isNull("count")) {
                        notiCount = docData.getInt("count");
                    }
                    msg_count.setText(String.valueOf(notiCount));

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                msg_count.setText("0");
                // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchNotificationSpinner() {
       // UltimateProgress.showProgressBar(getActivity(),"Please Wait.....!");
        HashMap<String, String> params = new HashMap<>();
        params.put("id", User.getCurrentUser().getId());
         params.put("type", "5");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTIFICATION_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                UltimateProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        classLists.clear();
                        JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                        classLists = NotificationBean.parseNotificationArray(noticeArray);
                        NotiSpinner adapter = new NotiSpinner(getActivity(), classLists);
                        spinnerGroups.setAdapter(adapter);
                        spinnerGroups.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i != 0) {
//                                    dataList.clear();
//                                    madapter.notifyDataSetChanged();
                                    title = classLists.get(i - 1).getN_title();

                                }
                                fetchNotification(i,title);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    noNotification.setVisibility(View.VISIBLE);
                    notiRV.setVisibility(View.VISIBLE);
                    noti_shimmer.stopShimmer();
                    noti_shimmer.setVisibility(View.GONE);
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);


    }

    public void fetchNotification(int i, String title) {
      //  UltimateProgress.showProgressBar(getActivity(),"Please Wait.....!");

        HashMap<String, String> params = new HashMap<String, String>();
        if (i == 0){
            params.put("title", "");
            Log.i("title",title);
        } else {
            params.put("title", title);
            Log.i("title",title);
        }
        params.put("today",date);
        params.put("id", User.getCurrentUser().getId());
//         params.put("type", "1");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTIFICATION_URL, notiapiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback notiapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            notiRV.setVisibility(View.VISIBLE);
            noti_shimmer.stopShimmer();
            noti_shimmer.setVisibility(View.GONE);
            if (error == null) {
                try {
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    notiList = NotificationBean.parseNotificationArray(noticeArray);
                    if (notiList.size() > 0) {
                        notiAdapter.setNList(notiList);
                        //setanimation on adapter...
                        notiRV.getAdapter().notifyDataSetChanged();
                        notiRV.scheduleLayoutAnimation();
                        //-----------end------------

                        noNotification.setVisibility(View.GONE);
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(notiList.size()));
                    } else {
                        notiList.clear();
                        notiAdapter.setNList(notiList);
                        notiAdapter.notifyDataSetChanged();
                        noNotification.setVisibility(View.VISIBLE);
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                notiList.clear();
                notiAdapter.setNList(notiList);
                notiAdapter.notifyDataSetChanged();
                noNotification.setVisibility(View.VISIBLE);
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }
        }
    };

    private void setSchoolData() {

        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(imgLogo);
        } else {
            Picasso.get().load(R.drawable.logo).into(imgLogo);
        }
        if (User.getCurrentUser().getSchoolData().getName() != null){
            schName.setText(User.getCurrentUser().getSchoolData().getName());
        }
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            if (speech==0) {
                TextToSpeechFunction();
            }
        }
    }

    @Override
    public void onMethodCallback(NotificationBean notiList) {
        speech=1;
        textholder = notiList.getN_title()+" ! "+ notiList.getN_msg();
        TextToSpeechFunction();
    }

    @Override
    public void onMethodCallbackNavigation(NotificationBean notiList) {
        if (notiList.getN_title().equalsIgnoreCase("Online Class")||notiList.getN_title().equalsIgnoreCase("Online Class!")){
            startActivity(new Intent(getActivity(), SheduleOnlineClassActivityList.class));

        }else if (notiList.getN_title().equalsIgnoreCase("Photo")||notiList.getN_title().equalsIgnoreCase("Gallery!")){
            startActivity(new Intent(getActivity(), GDSGalleryAct.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Holidays Homework")||notiList.getN_title().equalsIgnoreCase("Holidays Homework!")){
            Intent intent = new Intent(getActivity(), SubWiseHW.class);
            intent.putExtra("view", "holiday");
            startActivity(intent);
        }else if (notiList.getN_title().equalsIgnoreCase("Participant")||notiList.getN_title().equalsIgnoreCase("Participant!")){
            startActivity(new Intent(getActivity(), ParticipationListStud.class));
        }else if (notiList.getN_title().equalsIgnoreCase("E-Learning")||notiList.getN_title().equalsIgnoreCase("E-Learning!")){
            startActivity(new Intent(getActivity(), ViewOnlineClassLink.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Ebook")||notiList.getN_title().equalsIgnoreCase("Ebook Added!")){
            startActivity(new Intent(getActivity(), Ebook.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Syllabus") || notiList.getN_title().equalsIgnoreCase("Syllabus Updated!")
        ||notiList.getN_title().equalsIgnoreCase("Syllabus!")){
            Intent i = new Intent(getActivity(), SyllabusActivity.class);
            i.putExtra("check", "syllabus");
            startActivity(i);
        }else if (notiList.getN_title().equalsIgnoreCase("Datesheet")||notiList.getN_title().equalsIgnoreCase("Datesheet!")){
            startActivity(new Intent(getActivity(), Datesheet.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Test")||notiList.getN_title().equalsIgnoreCase("Daily Test!")
                ||notiList.getN_title().equalsIgnoreCase("Test Result!")){
            startActivity(new Intent(getActivity(), GDSTest.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Home Work")||notiList.getN_title().equalsIgnoreCase("Homework")){
            Intent intent = new Intent(getActivity(), SubWiseHW.class);
            intent.putExtra("view", "normal");
            startActivity(intent);
        }else if (notiList.getN_title().equalsIgnoreCase("Activity")|| notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), ViewStudents.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Leave")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), Leavelist_Activity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("School Activity Attendance!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), AttendanceActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Monthly Planner!")||notiList.getN_title().equalsIgnoreCase("")){
            Intent i = new Intent(getActivity(), SyllabusActivity.class);
            i.putExtra("check", "month");
            startActivity(i);
        }else if (notiList.getN_title().equalsIgnoreCase("Instagram Post")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), SocialLinkList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Communication Report!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), CommunicationList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Cleanliness Report!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), CleanlinessList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Social Post!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), SocialLinkList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Health Details!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), View_Health.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Video link Added!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), ViewOnlineClassLink.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Event Participation Acheivement")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), AchievementList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Monthly Fees!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), CommonFeesListActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Assignment!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), MainAssignActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Gate-Pass!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), GatePass.class));
        }else if (notiList.getN_title().equalsIgnoreCase("E-Learning Video Added!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), VideoActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("online class Added!")||notiList.getN_title().equalsIgnoreCase("")){
            startActivity(new Intent(getActivity(), SheduleOnlineClassActivityList.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Notice!")||notiList.getN_title().equalsIgnoreCase("Notice")){
            startActivity(new Intent(getActivity(), NoticeActivity.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Timetable")||notiList.getN_title().equalsIgnoreCase("Timetable!")){
            startActivity(new Intent(getActivity(), ViewTimeTable.class));
        }else if (notiList.getN_title().equalsIgnoreCase("Message!")||notiList.getN_title().equalsIgnoreCase("Message")){
            Intent intent = new Intent(getActivity(), MessageActivity.class);
            startActivity(intent);
        }else if (notiList.getN_title().equalsIgnoreCase("Classwork")||notiList.getN_title().equalsIgnoreCase("Classwork!")){
            Intent intent = new Intent(getActivity(), GDSClasswork.class);
            intent.putExtra("type", "history");
            startActivity(intent);
        }else if (notiList.getN_title().equalsIgnoreCase("Attendance Notification")||notiList.getN_title().equalsIgnoreCase("Attendance Notification!")){
            startActivity(new Intent(getActivity(), AttendanceActivity.class));
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){
//
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){
//
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){
//
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){
//
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){
//
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){
//
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){
//
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){
//
//        }else if (notiList.getN_title().equalsIgnoreCase("")||notiList.getN_title().equalsIgnoreCase("")){

        }else {
            Toast.makeText(getActivity(),"Module not found!",Toast.LENGTH_LONG).show();
        }
    }

    //text to speech programmatically...
    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onDestroy() {
        textToSpeech.shutdown();
        super.onDestroy();
    }
}
