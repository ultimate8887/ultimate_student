package com.ultimate.ultimatesmartstudent.Notification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class NotificationBean {
    private static String N_TITLE="n_title";
    private static String N_ID="n_id";
    private static String N_MSG="n_msg";
    private static String N_TIME="n_time";


    /**
     * n_id : 19
     * n_sender : 3
     * n_reciver : 1
     * n_msg : aniket has been marked as Present for 2018-03-29
     * n_title : Attendance Notification
     * n_time : 1521882379
     * n_type : 1
     * n_sucess : 0
     */

    private String n_id;
    private String n_sender;
    private String n_reciver;
    private String n_msg;
    private String n_title;
    private String n_time;
    private String n_type;
    private String n_sucess;
    private String n_Date;

    public String getN_id() {
        return n_id;
    }

    public void setN_id(String n_id) {
        this.n_id = n_id;
    }

    public String getN_sender() {
        return n_sender;
    }

    public void setN_sender(String n_sender) {
        this.n_sender = n_sender;
    }

    public String getN_reciver() {
        return n_reciver;
    }

    public void setN_reciver(String n_reciver) {
        this.n_reciver = n_reciver;
    }

    public String getN_msg() {
        return n_msg;
    }

    public void setN_msg(String n_msg) {
        this.n_msg = n_msg;
    }

    public String getN_title() {
        return n_title;
    }

    public void setN_title(String n_title) {
        this.n_title = n_title;
    }

    public String getN_time() {
        return n_time;
    }

    public void setN_time(String n_time) {
        this.n_time = n_time;
    }

    public String getN_type() {
        return n_type;
    }

    public void setN_type(String n_type) {
        this.n_type = n_type;
    }

    public String getN_sucess() {
        return n_sucess;
    }

    public void setN_sucess(String n_sucess) {
        this.n_sucess = n_sucess;
    }


    //    Now there is implementing all the ApiHandler working.............................

    public static ArrayList<NotificationBean> parseNotificationArray(JSONArray arrayObj) {

        ArrayList list = new ArrayList();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                NotificationBean bean = parseNotificationObject(arrayObj.getJSONObject(i));
                if (bean != null) {
                    list.add(bean);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static NotificationBean parseNotificationObject(JSONObject jsonObject) {

        NotificationBean casteObj = new NotificationBean();
        try {


            if (jsonObject.has(N_TITLE) && !jsonObject.getString(N_TITLE).isEmpty() && !jsonObject.getString(N_TITLE).equalsIgnoreCase("null")) {
                casteObj.setN_title(jsonObject.getString(N_TITLE));
            }

            if (jsonObject.has(N_TIME) && !jsonObject.getString(N_TIME).isEmpty() && !jsonObject.getString(N_TIME).equalsIgnoreCase("null")) {

                String time = jsonObject.getString("n_time");
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(Long.parseLong(time) * 1000);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm aa"); //this format changeable
                String OurTime = dateFormatter.format(cal.getTime());
                SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd MMM, yyyy"); //this format changeable
                String OurDate = dateFormatter1.format(cal.getTime());
                casteObj.setN_time(OurTime);
                casteObj.setN_Date(OurDate);

            }
            if (jsonObject.has(N_ID) && !jsonObject.getString(N_ID).isEmpty() && !jsonObject.getString(N_ID).equalsIgnoreCase("null")) {
                casteObj.setN_id(jsonObject.getString(N_ID));
            }

            if (jsonObject.has(N_MSG) && !jsonObject.getString(N_MSG).isEmpty() && !jsonObject.getString(N_MSG).equalsIgnoreCase("null")) {
                casteObj.setN_msg(jsonObject.getString(N_MSG));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return casteObj;
    }

    public void setN_Date(String n_Date) {
        this.n_Date = n_Date;
    }

    public String getN_Date() {
        return n_Date;
    }
}
