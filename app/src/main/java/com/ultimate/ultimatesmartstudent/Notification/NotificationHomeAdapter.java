package com.ultimate.ultimatesmartstudent.Notification;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeAdapter;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeBean;
import com.ultimate.ultimatesmartstudent.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationHomeAdapter extends RecyclerView.Adapter<NotificationHomeAdapter.Viewholder> {
    Context mContext;
    ArrayList<NotificationBean> notiList;
    private final Mycallback mMycallback;
    Animation animation;

    public NotificationHomeAdapter(ArrayList<NotificationBean> notiList, Context mContext,Mycallback mMycallback) {
        this.notiList = notiList;
        this.mContext = mContext;
        this.mMycallback = mMycallback;
    }

    public interface Mycallback {
        public void onMethodCallback(NotificationBean notiList);
        public void onMethodCallbackNavigation(NotificationBean notiList);
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_notice_list, parent, false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_notification_adapt, parent, false);
        NotificationHomeAdapter.Viewholder viewholder = new NotificationHomeAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHomeAdapter.Viewholder holder, @SuppressLint("RecyclerView") int position) {
        holder.parent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.up_to_down));
        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
      //  holder.imgSpeech.setVisibility(View.VISIBLE);
      //  holder.viewwwwww.setVisibility(View.VISIBLE);

        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);

        if (notiList.get(position).getN_Date().equalsIgnoreCase(dateString)){
            holder.newwwww.setText("Today");
            holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else {
            holder.newwwww.setText("");
            holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }


        holder.txtNTitle.setText(notiList.get(position).getN_title());
        holder.txtMsg.setText(notiList.get(position).getN_msg());
        holder.txtDate.setText(notiList.get(position).getN_Date());
        holder.txtTime.setText(notiList.get(position).getN_time());
//        if((position % 2) == 0){ //Even section
//            Picasso.get().load(R.drawable.s_bell).into(holder.imgNInfo);
//        }else{
//            Picasso.get().load(R.drawable.s_bell).into(holder.imgNInfo);
//        }

        holder.vieww.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.vieww.startAnimation(animation);
                if (mMycallback != null) {
                    mMycallback.onMethodCallbackNavigation(notiList.get(position));
                }
            }
        });

        holder.imgSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgSpeech.startAnimation(animation);
                if (mMycallback != null) {
                    mMycallback.onMethodCallback(notiList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notiList.size();
    }

    public void setNList(ArrayList<NotificationBean> notiList) {
        this.notiList = notiList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView)
        TextView txtNTitle;
        @BindView(R.id.textView3)
        TextView txtMsg;
        @BindView(R.id.textView2)
        TextView txtTime;
        @BindView(R.id.newwwww)
        TextView newwwww;
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.parent)
        RelativeLayout parent;
//        @BindView(R.id.imgNInfo)
//        ImageView imgNInfo;
        @BindView(R.id.imgSpeech)
        ImageView imgSpeech;
        @BindView(R.id.vieww)
        RelativeLayout vieww;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
