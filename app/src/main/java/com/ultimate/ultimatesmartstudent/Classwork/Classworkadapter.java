package com.ultimate.ultimatesmartstudent.Classwork;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.ultimate.ultimatesmartstudent.AttendMod.Util;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class Classworkadapter extends RecyclerView.Adapter<Classworkadapter.Viewholder>{
    private final Context mContext;
    ArrayList<Classworkbean> cwList;
    public Classworkadapter(ArrayList<Classworkbean> cwList, Context mContext) {
        this.cwList=cwList;
        this.mContext=mContext;

    }

    @Override
    public Classworkadapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.clsswrkada_lay, viewGroup, false);
        Classworkadapter.Viewholder viewholder = new Classworkadapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Classworkadapter.Viewholder viewholder, final int i) {
        viewholder.textView.setText(cwList.get(i).getInformation()+" ("+ Utils.dateFormat(cwList.get(i).getDate())+")");
        ViewPagerAdapter mAdapter = new  ViewPagerAdapter(mContext,cwList.get(i).getImage(),cwList.get(i).getMsg());
        viewholder.intro_images.setAdapter(mAdapter);
        viewholder.intro_images.setCurrentItem(0);
        viewholder.indicator.setViewPager(viewholder.intro_images);
    }

    @Override
    public int getItemCount() {
        return cwList.size();
    }

    public void setHQList(ArrayList<Classworkbean> cwList) {
        this.cwList = cwList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ViewPager intro_images;
        LinearLayout pager_indicator;
        public CircleIndicator indicator;
        public ImageButton button_next;
        public ImageButton button_finish;
        public Viewholder(View itemView) {
            super(itemView);
            indicator = (CircleIndicator) itemView.findViewById(R.id.indicator);
            textView=(TextView)itemView.findViewById(R.id.clsswrktexttt);
            intro_images=(ViewPager)itemView.findViewById(R.id.pager_introduction);

        }
    }
}
