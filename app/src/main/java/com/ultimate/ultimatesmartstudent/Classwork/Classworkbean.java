package com.ultimate.ultimatesmartstudent.Classwork;

import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Classworkbean {

    private static String ID="id";
    private static String IMAGE="image";
    private static String INFORMATION="information";
    private static String C_DATE="date";
    private static String CLASS_ID="class_id";
    private static String CLASS_NAME="class_name";
    private static String MSG="msg";

    /**
     * id : 1
     * information : This is a dummy data for your testing.
     * image : office_admin/images/classwork/cw_03272018_080305.jpg,cw_03272018_080306.jpg,cw_03272018_080307.jpg,cw_03272018_080308.jpg,cw_03272018_080309.jpg,cw_03272018_080310.jpg
     * date : 2018-04-21
     * class_id : 2
     * class_name : NURSERY1
     */

    private String id;
    private String information;
    private String date;
    private String class_id;
    private String class_name;
    private ArrayList<String> image;
    private String msg;

    private String pdf_file;

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    /**
     * msg : fdgdfg
     */

    private String image_tag;

    private String subject_name;
    private String section_name;

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public static ArrayList<Classworkbean> parseHWArray(JSONArray arrayObj) {
        ArrayList<Classworkbean> list = new ArrayList<Classworkbean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Classworkbean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Classworkbean parseHWObject(JSONObject jsonObject) {
        Classworkbean casteObj = new Classworkbean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(IMAGE) && jsonObject.get(IMAGE) instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray(IMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }
            if (jsonObject.has(INFORMATION) && !jsonObject.getString(INFORMATION).isEmpty() && !jsonObject.getString(INFORMATION).equalsIgnoreCase("null")) {
                casteObj.setInformation(jsonObject.getString(INFORMATION));
            }
            if (jsonObject.has("pdf_file") && !jsonObject.getString("pdf_file").isEmpty() && !jsonObject.getString("pdf_file").equalsIgnoreCase("null")) {
                casteObj.setPdf_file(Constants.getImageBaseURL()+jsonObject.getString("pdf_file"));
            }

            if (jsonObject.has(C_DATE) && !jsonObject.getString(C_DATE).isEmpty() && !jsonObject.getString(C_DATE).equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString(C_DATE));
            }
            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString(CLASS_ID));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(MSG) && !jsonObject.getString(MSG).isEmpty() && !jsonObject.getString(MSG).equalsIgnoreCase("null")) {
                casteObj.setMsg(jsonObject.getString(MSG));
            }

            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }

            if (jsonObject.has("subject_name") && !jsonObject.getString("subject_name").isEmpty() && !jsonObject.getString("subject_name").equalsIgnoreCase("null")) {
                casteObj.setSubject_name(jsonObject.getString("subject_name"));
            }

            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                casteObj.setImage_tag(jsonObject.getString("image_tag"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }


    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
