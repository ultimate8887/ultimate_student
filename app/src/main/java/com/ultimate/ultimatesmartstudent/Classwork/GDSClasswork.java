package com.ultimate.ultimatesmartstudent.Classwork;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.Homework.SubWiseHW;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeActivity;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SyllabusMod.SyllabusActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class GDSClasswork extends AppCompatActivity implements ClassworkadapterNew.Mycallback,TextToSpeech.OnInitListener {

    String textholder="";
    TextToSpeech textToSpeech;
    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;

    private ClassworkadapterNew adapter;
    ArrayList<Classworkbean> cwList = new ArrayList<>();
    ArrayList<Classworkbean> a_cwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerview;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private int loaded=0;
    private static final int PERMISSION_REQUEST_CODE = 1;
    boolean today = true;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    String type="";
    Animation animation;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String  sub_id, sub_name;
    @BindView(R.id.textView8)
    TextView textsubtitle;

    @BindView(R.id.spinnerGroups)
    Spinner SubjectSpinner;
    @BindView(R.id.totalRecord)
    TextView totalRecord;


    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        textsubtitle.setText(getString(R.string.f_subject));
        commonProgress=new CommonProgress(this);
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkPermissionone())
            {
                // Code for above or equal 23 API Oriented Device
                // Your Permission granted already .Do next code
            } else {
                requestPermission(); // Code for permission
            }
        }
        else
        {
            // Code for Below 23 API Oriented Device
            // Do next code
        }
        textToSpeech = new TextToSpeech(GDSClasswork.this, this);
        type = getIntent().getStringExtra("type");
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
//        // set animation on recyclerview
//        int resId = R.anim.layout_animation_right_to_left;
//        final LayoutAnimationController controller =
//                AnimationUtils.loadLayoutAnimation(this, resId);
//        recyclerview.setLayoutAnimation(controller);
//        //----------------end--------------------
        layoutManager = new LinearLayoutManager(GDSClasswork.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new ClassworkadapterNew(cwList, GDSClasswork.this, this);
        recyclerview.setAdapter(adapter);
//        fetchClasswork();
        getSubjectofClass();
    }

    private void getSubjectofClass() {
        //UltimateProgress.showProgressBar(this, "Please wait...");

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            UltimateProgress.cancelProgressBar();
            //   commonProgress.dismiss();
            if (error == null) {

                try {
                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));
                    adaptersub = new AdapterOfSubjectList(GDSClasswork.this, subjectList);
                    SubjectSpinner.setAdapter(adaptersub);
                    SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }
                            fetchClasswork(i);
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    @OnClick(R.id.imgBackmsg)
    public void backCall(){
        imgBackmsg.startAnimation(animation);
        finish();
    }
    private boolean checkPermissionone() {
        int result = ContextCompat.checkSelfPermission(GDSClasswork.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(GDSClasswork.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(GDSClasswork.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(GDSClasswork.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    public void setCWList() {
        if (cwList != null) {
            cwList.clear();
        }
        if (a_cwList.size() > 0) {
            if (type.equalsIgnoreCase("today")) {
                txtTitle.setText(getString(R.string.today)+" "+getString(R.string.classwork));
                Calendar c1 = Calendar.getInstance();
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd"); //this format changeable
                String todaydate = dateFormatter.format(c1.getTime());
//                String date= Utils.getDateFormated(a_cwList.get(0).getDate());
//                Toast.makeText(getApplicationContext(), "date"+date+"\n\n"+todaydate, Toast.LENGTH_SHORT).show();
                for (int i = 0; i < a_cwList.size(); i++) {
                    String date= Utils.getDateFormated_date(a_cwList.get(i).getDate());
                    if (date.equalsIgnoreCase(todaydate)) {
                        cwList.add(a_cwList.get(i));

                    }
                }
            } else {
                txtTitle.setText(getString(R.string.classwork));
                cwList.addAll(a_cwList);
            }
            if (cwList.size() > 0) {
                adapter.setHQList(cwList);
                //setanimation on adapter...
                recyclerview.getAdapter().notifyDataSetChanged();
                recyclerview.scheduleLayoutAnimation();
                //-----------end------------
                txtNorecord.setVisibility(View.GONE);
                totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(cwList.size()));
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                adapter.setHQList(cwList);
                adapter.notifyDataSetChanged();
                txtNorecord.setVisibility(View.VISIBLE);
            }
        } else {
            adapter.setHQList(cwList);
            //setanimation on adapter...
            recyclerview.getAdapter().notifyDataSetChanged();
            recyclerview.scheduleLayoutAnimation();
            //-----------end------------
            txtNorecord.setVisibility(View.VISIBLE);
            totalRecord.setText(getString(R.string.t_entries)+" 0");
        }
    }

    public void fetchClasswork(int i) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();

        if (i == 0){
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("sectionid", User.getCurrentUser().getSection_id());
        } else {
            params.put("sub_id", sub_id);
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("sectionid", User.getCurrentUser().getSection_id());
        }


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSCLASSWORK_URL, apiCallback, this, params);
    }


    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (a_cwList != null) {
                        a_cwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("cw_data");
                    a_cwList = Classworkbean.parseHWArray(jsonArray);
                    setCWList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                cwList.clear();
                adapter.setHQList(cwList);
                txtNorecord.setVisibility(View.VISIBLE);
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                Toast.makeText(GDSClasswork.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    public void onMethod_Image_callback(Classworkbean data) {
        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(data.getInformation());
        ViewPagerAdapter mAdapter = new  ViewPagerAdapter(this,data.getImage(),"classwork");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });


        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);

        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.VISIBLE);

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=data.getImage();
                Intent intent = new Intent(GDSClasswork.this, IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "classwork");
                intent.putExtra("title", getString(R.string.classwork));
                intent.putExtra("sub", "Classwork_id:- "+data.getId());
                startActivity(intent);
            }
        });

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onMethod_pdf_call_back(Classworkbean homeworkbean) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(GDSClasswork.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(GDSClasswork.this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(homeworkbean.getSubject_name()+"("+homeworkbean.getInformation()+")");
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.ebook).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        sub_wise.setText("View!");
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getBaseContext(), ViewPdfActivity.class);
                intent.putExtra("id", homeworkbean.getPdf_file());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
        date_wise.setText("Save!");
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                // UltimateProgress.showProgressBar(SubWiseHW.this,"Please wait...");
                String mUrl=homeworkbean.getPdf_file();
                // new DownloadFile().execute(mUrl);

                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("Cw_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Cw_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getApplicationContext(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    @Override
    public void onImgSpeechCallback(Classworkbean obj) {
        textholder = obj.getInformation();
        TextToSpeechFunction();
    }

    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);

    }

    @Override
    public void onDestroy() {

        textToSpeech.shutdown();

        super.onDestroy();
    }


    @Override
    public void onInit(int Text2SpeechCurrentStatus) {

        if (Text2SpeechCurrentStatus == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            TextToSpeechFunction();
        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onDelecallback(Classworkbean classworkbean) {

    }


}