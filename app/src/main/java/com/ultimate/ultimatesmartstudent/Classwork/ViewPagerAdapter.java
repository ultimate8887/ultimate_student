package com.ultimate.ultimatesmartstudent.Classwork;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.PagerAdapter;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Homework.SubWiseHW;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Objects;

public class ViewPagerAdapter extends PagerAdapter {


    private final Context mContext;
    private final ArrayList<String> cwList;
    String folder_main = "ClassWork";
    String msg,school="";

    public ViewPagerAdapter(Context mContext, ArrayList<String> cwList, String msg) {
        this.mContext = mContext;
        this.cwList = cwList;
        this.msg = msg;
    }

    @Override
    public int getCount() {
        int size = 0;
        if(cwList!= null){
            size = cwList.size();
        }
//        if(msg!= null){
//            size= size+1;
//        }
        return size;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == ((RelativeLayout) o);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);
        school= User.getCurrentUser().getSchoolData().getName();
        final ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);
        TextView txtText= (TextView)itemView.findViewById(R.id.clsswrktexttt);
        if(msg!= null){
            //old code to show msg content on first slide.....
//            if(position==0){
//                txtText.setVisibility(View.VISIBLE);
//                txtText.setText(msg);
//                txtText.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        openDialog(msg);
//                    }
//                });
//            }else{
//                Picasso.get().load(cwList.get(position-1)).into(imageView);
//            }
            Picasso.get().load(cwList.get(position)).into(imageView);
        }else{
            Picasso.get().load(cwList.get(position)).into(imageView);
        }

        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                UltimateProgress.showProgressBar(mContext, "Waiting For Downloading......!");
//                Picasso.get().load(cwList.get(position)).into(imgTraget);
            //    Toast.makeText(mContext,"Downloaded successfully",Toast.LENGTH_LONG).show();
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
               // saveImageToGallery(bitmap);
                fullViewImage(bitmap,cwList.get(position));
                return false;
            }
        });
        container.addView(itemView);

        return itemView;
    }

    private void fullViewImage(Bitmap bitmap, String s) {
        Dialog mBottomSheetDialog = new Dialog(mContext);
        //  final View sheetView = mContext.getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(R.layout.image_lyt_new);
        ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);

        ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.VISIBLE);
        // Picasso.get().load(obj.getImage()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

        imgShow.setImageBitmap(bitmap);

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
                imgDownload.startAnimation(animation);
                //  ErpProgress.showProgressBar(mContext, "downloading...");
                saveImageToGallery(bitmap);
                // Picasso.with(ViewNoticeInfo.this).load(obj.getImage()).into(imgTraget);
                // Toast.makeText(ViewNoticeInfo.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });
        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void saveImageToGallery(Bitmap bitmap){
        long time= System.currentTimeMillis();
        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = mContext.getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, msg+"_img_saved" +time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(mContext, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, msg+"_img_saved" +time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(mContext, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(mContext, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private void openDialog(String msg) {
        final Dialog logoutDialog = new Dialog(mContext);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setCancelable(true);
        logoutDialog.setContentView(R.layout.msg_lyt);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);
        txt2.setText(msg);
        logoutDialog.show();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
