package com.ultimate.ultimatesmartstudent.DirtChart.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.DirtChart.DailytipsBean;
import com.ultimate.ultimatesmartstudent.DirtChart.DietchartBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class Satufrag extends Fragment {

    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.tipss)TextView tips;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recipiname)TextView recipetiitle;
    @BindView(R.id.imageView6)
    ImageView recipepic;
    @BindView(R.id.txtfullprocedure)TextView description;
  //  @BindView(R.id.designlay)RelativeLayout dishpiclayput;
    @BindView(R.id.tiplay)RelativeLayout tipslayout;
    int loaded=0;
    @BindView(R.id.daytext)TextView daytext;
    String day_id="5";
    String abcd;
    EditText edretips;
    ArrayList<DailytipsBean> tipsList = new ArrayList<>();
    ArrayList<DietchartBean> dietchrtList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_common, container, false);
        ButterKnife.bind(this,view);
        daytext.setText("Saturday");
        fetchdietchart();
        return view;
    }


    private void fetchdailytips() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("day_id", day_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DAILYTIPSURL, apiCallback,getContext(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            //txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (tipsList != null) {
                        tipsList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("dailytips_data");
                    tipsList = DailytipsBean.parseDTArray(jsonArray);
                    abcd=tipsList.get(0).getTips();
                    tips.setText(abcd);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    };
    @Override
    public void onResume() {
        super.onResume();
        fetchdailytips();
        fetchdietchart();
    }

    private void fetchdietchart() {
//        if (loaded == 0) {
//            ErpProgress.showProgressBar(getContext(), "Please wait...");
//        }
//        loaded++;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("day_id", day_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DIETCHARTURL, dietchartapiCallback,getContext(), params);

    }

    ApiHandler.ApiCallback dietchartapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            //txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (dietchrtList != null) {
                        dietchrtList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("dietchart_data");
                    dietchrtList = DietchartBean.parseDCArray(jsonArray);

                    if (dietchrtList.size() > 0) {
                        recipetiitle.setText(dietchrtList.get(0).getTittle());
                        description.setText(dietchrtList.get(0).getDescription());
                        if (dietchrtList.get(0).getImage() != null) {
                            Picasso.get().load(dietchrtList.get(0).getImage()).into(recipepic);
                        }else{
                            Picasso.get().load(String.valueOf(getActivity().getResources())).into(recipepic);

                        }
                        txtNorecord.setVisibility(View.GONE);


                    } else {
                  //      dishpiclayput.setVisibility(View.GONE);
                        tipslayout.setVisibility(View.GONE);
                        txtNorecord.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
              //  dishpiclayput.setVisibility(View.GONE);
                tipslayout.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                dietchrtList.clear();
            }
        }
    };



}