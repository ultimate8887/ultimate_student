package com.ultimate.ultimatesmartstudent.DirtChart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartstudent.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DietChartActivity extends AppCompatActivity {
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;

    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet_chart);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText(getString(R.string.dietchart));
        tablayout.setupWithViewPager(viewpager);
        final PagerAdapter adapter = new Pager
                (getSupportFragmentManager());
        viewpager.setAdapter(adapter);
    }
    @OnClick(R.id.imgBackmsg)
    public void backCall(){
        imgBackmsg.startAnimation(animation);
        finish();
    }
}