package com.ultimate.ultimatesmartstudent.DirtChart;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ultimate.ultimatesmartstudent.DirtChart.Fragments.Fridfrag;
import com.ultimate.ultimatesmartstudent.DirtChart.Fragments.Monfrag;
import com.ultimate.ultimatesmartstudent.DirtChart.Fragments.Satufrag;
import com.ultimate.ultimatesmartstudent.DirtChart.Fragments.Thufrag;
import com.ultimate.ultimatesmartstudent.DirtChart.Fragments.Tuefrag;
import com.ultimate.ultimatesmartstudent.DirtChart.Fragments.Wedfrag;

public class Pager extends FragmentPagerAdapter {

    int tabCount;

    private String[] tabTitles = new String[]{"Mon", "Tue", "Wed","Thu","Fri","Sat"};

    public Pager(FragmentManager fm) {
        super(fm);
        this.tabCount = 6;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Monfrag tab1 = new Monfrag();
                return tab1;

            case 1:
                Tuefrag tab2 = new Tuefrag();
                return tab2;

            case 2:
                Wedfrag tab3 = new Wedfrag();
                return tab3;

            case 3:
                Thufrag tab4 = new Thufrag();
                return tab4;

            case 4:
                Fridfrag tab5 = new Fridfrag();
                return tab5;

            case 5:
                Satufrag tab6 = new Satufrag();
                return tab6;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
