package com.ultimate.ultimatesmartstudent.NoticeMod;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.Homework.HomeWorkByDate;
import com.ultimate.ultimatesmartstudent.Homework.SubWiseHW;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class NoticeActivity extends AppCompatActivity implements TextToSpeech.OnInitListener, NoticeAdapter.Mycallback{
    CommonProgress commonProgress;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    TextToSpeech textToSpeech;
    @BindView(R.id.recyclerView)
    RecyclerView nbRV;
    ArrayList<NoticeBean> noticeList;
    private NoticeAdapter nbAdapter;
    @BindView(R.id.textNorecord)
    TextView noNoticeData;
    Animation animation;
    String folder_main = "NoticeBoard";
    String textholder="",school="";
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.txtSub)
    TextView txtSub;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_new);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.notice_board)+" "+getString(R.string.list));
        school=User.getCurrentUser().getSchoolData().getName();
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        textToSpeech = new TextToSpeech(NoticeActivity.this, this);
        txtSub.setText(Utils.setNaviHeaderClassData());
        commonProgress=new CommonProgress(this);
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);

        nbRV.setLayoutAnimation(controller);


        /* Notice board section*/
        noticeList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(NoticeActivity.this);
        nbRV.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(nbRV.getContext(),
                layoutManager.getOrientation());
        nbRV.addItemDecoration(dividerItemDecoration);
        nbAdapter = new NoticeAdapter(noticeList, NoticeActivity.this,this);
        nbRV.setAdapter(nbAdapter);
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchNoticeBoardList();
    }

    private void fetchNoticeBoardList() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "2");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTICE_URL, napiCallback, this, params);

    }

    ApiHandler.ApiCallback napiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (noticeList != null)
                        noticeList.clear();
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    noticeList = NoticeBean.parseNoticetArray(noticeArray);
                    nbAdapter.setNoticeList(noticeList);
                   // nbAdapter.notifyDataSetChanged();
                    //setanimation on adapter...
                    Log.e("noticeList", String.valueOf(noticeList));
                    nbRV.getAdapter().notifyDataSetChanged();
                    nbRV.scheduleLayoutAnimation();
                    //-----------end------------
                    nbRV.setVisibility(View.VISIBLE);
                    noNoticeData.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- "+String.valueOf(noticeList.size()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                noticeList.clear();
                nbAdapter.setNoticeList(noticeList);
                nbAdapter.notifyDataSetChanged();
                nbRV.setVisibility(View.GONE);
                noNoticeData.setVisibility(View.VISIBLE);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(NoticeActivity.this, LoginActivity.class));
                    finish();
                }

            }
        }
    };

    @Override
    public void onMethodCallback(NoticeBean obj) {
        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
        // dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imgShow = (ImageView) dialogLog.findViewById(R.id.img);

        Picasso.get().load(obj.getImage()).into(imgShow);
        Button save = (Button) dialogLog.findViewById(R.id.submit);
        save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                save.startAnimation(animation);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);
            }
        });

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
        Window window = dialogLog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;
        long time= System.currentTimeMillis();
        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "noticeBoard_file_saved"+time+".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "noticeBoard_file_saved"+time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onImgSpeechCallback(NoticeBean obj) {
        textholder = obj.getTitle()+" !  "+obj.getMsg();
        TextToSpeechFunction();
    }

    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);

   }

    @Override
    public void onDestroy() {

        textToSpeech.shutdown();

        super.onDestroy();
    }


    @Override
    public void onInit(int Text2SpeechCurrentStatus) {

        if (Text2SpeechCurrentStatus == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
                TextToSpeechFunction();
        }

    }

    @Override
    public void onMethod_Image_callback_new(NoticeBean message_bean) {
        openCommonMultiImage(message_bean.getTitle(),message_bean.getMulti_image(),message_bean.getEs_noticeid());
    }

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    String viewwwww = "";

    private void openCommonMultiImage(String subject, ArrayList<String> multiImage,String id) {
        final Dialog warningDialog = new Dialog(NoticeActivity.this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView = (TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images = (ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(subject);
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(NoticeActivity.this, multiImage, "Notice");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        TextView tap_count = (TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>" + "1" + "</b>" + "", "#F4212C");
        String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));

        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);
        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.VISIBLE);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=multiImage;
                Intent intent = new Intent(NoticeActivity.this, IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "Noticeboard");
                intent.putExtra("title", getString(R.string.notice_board));
                intent.putExtra("sub", "Notice_id:- "+id);
                startActivity(intent);
            }
        });

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c = arg0 + 1;
                String title1 = getColoredSpanned("<b>" + String.valueOf(c) + "</b>" + "", "#F4212C");
                String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public void onMethod_recording_callback(NoticeBean message_bean) {
        openCommonVoice(message_bean.getRecordingsfile());
    }

    private MediaPlayer mediaPlayer;
    private Button playButton, pauseButton;
    ImageView play_img, pause_img;
    RelativeLayout play_lyt;
    private SeekBar seekBar;
    private Handler handler = new Handler();
    private TextView currentDurationTextView, totalDurationTextView;
    LottieAnimationView loti_record_play;
    private Button btnRetake, btnPick,btnSave;
    BottomSheetDialog mBottomSheetDialog_play;
    private void openCommonVoice(String recordingsfile) {

        mBottomSheetDialog_play = new BottomSheetDialog(NoticeActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.activity_audio, null);
        mBottomSheetDialog_play.setContentView(sheetView);
        mBottomSheetDialog_play.setCancelable(false);

        btnRetake = sheetView.findViewById(R.id.btnRetake);
        btnPick = sheetView.findViewById(R.id.btnPick);
        btnSave = sheetView.findViewById(R.id.btnSave);
        play_lyt = sheetView.findViewById(R.id.play_lyt);
        play_img = sheetView.findViewById(R.id.play_img);
        pause_img = sheetView.findViewById(R.id.pause_img);
        playButton = sheetView.findViewById(R.id.playButton);
        pauseButton = sheetView.findViewById(R.id.pauseButton);
        seekBar = sheetView.findViewById(R.id.seekBar);
        loti_record_play = sheetView.findViewById(R.id.loti_play);
        currentDurationTextView = sheetView.findViewById(R.id.currentDurationTextView);
        totalDurationTextView = sheetView.findViewById(R.id.totalDurationTextView);


        btnSave.setVisibility(View.GONE);
        btnPick.setVisibility(View.GONE);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.seekTo(0);
                seekBar.setProgress(0);
                loti_record_play.pauseAnimation();
            }
        });
        String url=recordingsfile;
        //  Log.d("selectedImageUri",url);
        Uri selectedImageUri = Uri.parse(url);

        try {
            mediaPlayer.setDataSource(NoticeActivity.this, selectedImageUri);

            mediaPlayer.prepare();
            mediaPlayer.start();
            updateSeekBar();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        play_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.pause();
                loti_record_play.pauseAnimation();
            }
        });

        pause_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.VISIBLE);
                pause_img.setVisibility(View.GONE);
                mediaPlayer.start();
                updateSeekBar();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                    updateDurationTextView(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        totalDurationTextView.setText(formatDuration(mediaPlayer.getDuration()));

        btnRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonCodeForDssmiss();
            }
        });

        // Dismiss the dialog and reset the MediaPlayer when the dialog is closed
        mBottomSheetDialog_play.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //  Toast.makeText(getApplicationContext(), "Dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog_play.show();
    }

    int starts = 0;

    private void commonCodeForDssmiss() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mBottomSheetDialog_play.dismiss();
    }

    private void updateSeekBar() {
        loti_record_play.playAnimation();
        seekBar.setMax(mediaPlayer.getDuration());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int currentPosition = mediaPlayer.getCurrentPosition();
                    seekBar.setProgress(currentPosition);
                    updateDurationTextView(currentPosition);
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    private void updateDurationTextView(int duration) {
        currentDurationTextView.setText(formatDuration(duration));
    }

    private String formatDuration(int milliseconds) {
        int seconds = (milliseconds / 1000) % 60;
        int minutes = (milliseconds / (1000 * 60)) % 60;
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }


    @Override
    public void onMethod_pdf_call_back(NoticeBean homeworkbean) {
        //   startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(homeworkbean.getPdf_file())));
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(NoticeActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(NoticeActivity.this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(homeworkbean.getTitle());
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.ebook).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        sub_wise.setText("View!");
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getBaseContext(), ViewPdfActivity.class);
                intent.putExtra("id", homeworkbean.getPdf_file());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
        date_wise.setText("Save!");
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                // UltimateProgress.showProgressBar(SubWiseHW.this,"Please wait...");
                String mUrl=homeworkbean.getPdf_file();
                // new DownloadFile().execute(mUrl);
                String path_name ="/storage/Android/data/com.ultimate.tbs_student/files/document/"+folder_main+"/Notice_file_saved.pdf";

                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("Notice_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Notice_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getApplicationContext(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

}
