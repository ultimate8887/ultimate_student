package com.ultimate.ultimatesmartstudent.NoticeMod;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.ViewHolder> {

    private final Context mContext;
    private final Mycallback mMycallback;
    private ArrayList<NoticeBean> noticeList = new ArrayList<>();
    private Animation animation;

    public NoticeAdapter(ArrayList<NoticeBean> noticeList, Context context, Mycallback mMycallback) {
        this.noticeList = noticeList != null ? noticeList : new ArrayList<>();
        this.mMycallback = mMycallback;
        this.mContext = context;
    }

    public void setNoticeList(ArrayList<NoticeBean> noticeList) {
        this.noticeList = noticeList != null ? noticeList : new ArrayList<>();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle;
        public TextView txtDate;
        public TextView txtMsg;
        ImageView imgNotice;
        @BindView(R.id.copy)
        ImageView copy;
        @BindView(R.id.albm_id)
        TextView albm_id;
        CardView parent;
        @BindView(R.id.nofile)
        RelativeLayout nofile;

        public ImageView image_file, audio_file, pdf_file, empty_file;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            parent = itemView.findViewById(R.id.card1);
            txtTitle = itemView.findViewById(R.id.textView);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtMsg = itemView.findViewById(R.id.txtMsg);
            imgNotice = itemView.findViewById(R.id.imgNotice);
            empty_file = itemView.findViewById(R.id.empty_file);
            image_file = itemView.findViewById(R.id.image_file);
            pdf_file = itemView.findViewById(R.id.pdf_file);
            audio_file = itemView.findViewById(R.id.audio_file);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_notice_adap, parent, false);
        return new ViewHolder(v);
    }

    private String getColoredSpanned(String text, String color) {
        return "<font color=" + color + ">" + text + "</font>";
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {
        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);

        NoticeBean notice = noticeList.get(position);

        viewHolder.txtTitle.setText(notice.getTitle());

        //for Today
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(calendar.getTime());

        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        calendar.add(Calendar.DATE, -1);
        String dateString1 = dateFormat.format(calendar.getTime());

        String check = notice.getDate();

        if (check.equalsIgnoreCase(dateString)) {
            String title = getColoredSpanned("Today", "#e31e25");
            viewHolder.txtDate.setText(Html.fromHtml(title + " "));
        } else if (check.equalsIgnoreCase(dateString1)) {
            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            viewHolder.txtDate.setText(Html.fromHtml(title + " "));
        } else {
            viewHolder.txtDate.setText(notice.getDate());
        }

        viewHolder.txtMsg.setText(Html.fromHtml(notice.getMsg()));

        if (notice.getEs_noticeid() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String name = getColoredSpanned("nb-" + notice.getEs_noticeid(), "#5A5C59");
            viewHolder.albm_id.setText(Html.fromHtml(title + " " + name));
        }

        viewHolder.copy.setOnClickListener(v -> {
            viewHolder.copy.startAnimation(animation);
            if (mMycallback != null) {
                mMycallback.onImgSpeechCallback(notice);
            }
        });

        setupFileIcons(viewHolder, notice);

    }

    private void setupFileIcons(ViewHolder viewHolder, NoticeBean notice) {
        if (notice.getImage_tag().equalsIgnoreCase("images")) {
            viewHolder.nofile.setVisibility(View.VISIBLE);
            viewHolder.image_file.setVisibility(View.VISIBLE);
            viewHolder.audio_file.setVisibility(View.GONE);
            viewHolder.pdf_file.setVisibility(View.GONE);
            viewHolder.empty_file.setVisibility(View.GONE);

            Picasso.get().load(notice.getMulti_image().get(0))
                    .placeholder(mContext.getResources().getDrawable(R.color.transparent))
                    .into(viewHolder.image_file);

            viewHolder.image_file.setOnClickListener(v -> {
                viewHolder.image_file.startAnimation(animation);
                if (mMycallback != null) {
                    mMycallback.onMethod_Image_callback_new(notice);
                }
            });

        } else if (notice.getRecordingsfile() != null) {
            viewHolder.nofile.setVisibility(View.VISIBLE);
            viewHolder.image_file.setVisibility(View.GONE);
            viewHolder.audio_file.setVisibility(View.VISIBLE);
            viewHolder.pdf_file.setVisibility(View.GONE);
            viewHolder.empty_file.setVisibility(View.GONE);

            viewHolder.audio_file.setOnClickListener(v -> {
                viewHolder.audio_file.startAnimation(animation);
                if (mMycallback != null) {
                    mMycallback.onMethod_recording_callback(notice);
                }
            });

        } else if (notice.getPdf_file() != null) {
            viewHolder.nofile.setVisibility(View.VISIBLE);
            viewHolder.image_file.setVisibility(View.GONE);
            viewHolder.audio_file.setVisibility(View.GONE);
            viewHolder.pdf_file.setVisibility(View.VISIBLE);
            viewHolder.empty_file.setVisibility(View.GONE);

            viewHolder.pdf_file.setOnClickListener(v -> {
                viewHolder.pdf_file.startAnimation(animation);
                if (mMycallback != null) {
                    mMycallback.onMethod_pdf_call_back(notice);
                }
            });

        } else {
            viewHolder.nofile.setVisibility(View.GONE);
            viewHolder.image_file.setVisibility(View.GONE);
            viewHolder.audio_file.setVisibility(View.GONE);
            viewHolder.pdf_file.setVisibility(View.GONE);
            viewHolder.empty_file.setVisibility(View.VISIBLE);
        }
    }

    public interface Mycallback {
        void onMethodCallback(NoticeBean obj);

        void onImgSpeechCallback(NoticeBean obj);

        void onInit(int Text2SpeechCurrentStatus);

        void onMethod_Image_callback_new(NoticeBean message_bean);

        void onMethod_recording_callback(NoticeBean message_bean);

        void onMethod_pdf_call_back(NoticeBean message_bean);
    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

}
