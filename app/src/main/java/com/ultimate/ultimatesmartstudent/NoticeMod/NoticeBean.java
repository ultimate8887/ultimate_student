package com.ultimate.ultimatesmartstudent.NoticeMod;

import android.util.Log;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NoticeBean {
    private static String TITLE="title";
    private static String MSG="msg";
    private static String DATE="date";

    public String getNew_date() {
        return new_date;
    }

    public void setNew_date(String new_date) {
        this.new_date = new_date;
    }

    private String new_date;

    public String getEs_noticeid() {
        return es_noticeid;
    }

    public void setEs_noticeid(String es_noticeid) {
        this.es_noticeid = es_noticeid;
    }

    private String es_noticeid;

    private String pdf_file;

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getRecordingsfile() {
        return recordingsfile;
    }

    public void setRecordingsfile(String recordingsfile) {
        this.recordingsfile = recordingsfile;
    }

    private String recordingsfile;


    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    private String image_tag;

    public ArrayList<String> getMulti_image() {
        return multi_image;
    }

    public void setMulti_image(ArrayList<String> multi_image) {
        this.multi_image = multi_image;
    }

    private ArrayList<String> multi_image;
    /**
     * title : admission open 2018-19
     * msg : <p><span style="font-size: small;"><strong>Module 1:</strong></span></p>
     <p><strong>Alebracvs:</strong> dsfkhfoudshfohsdfh dsfjksdfhjkd fdsjfbvdjsfds fjkdsfdslfmnkdsf sdfsdkfndsfd sfjdsbfjdsfds fbdsfbdfdsnfhndsf dsfndsnfd. dfjhdsjfsdf dsbfdsjfbdsufdsm fjdsfdsf.</p>
     <p><strong>dskjhdj: </strong>jffkjgdff dsfhndsf dskfbkdsf dsfhsudfhusd fuksfudsgf fdusi fudf.df dshfuds fudsgfukd sfjd gsiuf dsfdf dfo.fd sfsdj fiousdofidsfoi sd.</p>
     <p>&Acirc;&nbsp;</p>
     * date : 2018-03-06
     */

    private String title;
    private String msg;
    private String date;
    private String image;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }


//    Now there is implementing all the ApiHandler working.............................

    public static ArrayList<NoticeBean> parseNoticetArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<NoticeBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                NoticeBean p = parseNoticeObject(arrayObj.getJSONObject(i));
                if (p != null && p.getMsg()!= null && p.getTitle()!= null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static NoticeBean parseNoticeObject(JSONObject jsonObject) {
        NoticeBean casteObj = new NoticeBean();
        try {

            if (jsonObject.has(TITLE) && !jsonObject.getString(TITLE).isEmpty() && !jsonObject.getString(TITLE).equalsIgnoreCase("null")) {
                casteObj.setTitle(jsonObject.getString(TITLE));
            }

            if (jsonObject.has("es_noticeid") && !jsonObject.getString("es_noticeid").isEmpty() && !jsonObject.getString("es_noticeid").equalsIgnoreCase("null")) {
                casteObj.setEs_noticeid(jsonObject.getString("es_noticeid"));
            }

            if (jsonObject.has(MSG) && !jsonObject.getString(MSG).isEmpty() && !jsonObject.getString(MSG).equalsIgnoreCase("null")) {
                casteObj.setMsg(jsonObject.getString(MSG));
            }

            if (jsonObject.has("image") && !jsonObject.getString("image").isEmpty() && !jsonObject.getString("image").equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString("image"));
            }
            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                casteObj.setNew_date(jsonObject.getString(DATE));
            }

            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                Date dte = new SimpleDateFormat("yyyy-MM-dd").parse(jsonObject.getString("date"));
                String stringgg = new SimpleDateFormat("dd MMM, yyyy").format(dte);
                casteObj.setDate(stringgg);
            }

            if (jsonObject.has("pdf_file") && !jsonObject.getString("pdf_file").isEmpty() && !jsonObject.getString("pdf_file").equalsIgnoreCase("null")) {
                casteObj.setPdf_file(Constants.getImageBaseURL()+jsonObject.getString("pdf_file"));
            }
            if (jsonObject.has("recordingsfile") && !jsonObject.getString("recordingsfile").isEmpty() && !jsonObject.getString("recordingsfile").equalsIgnoreCase("null")) {
                casteObj.setRecordingsfile(Constants.getImageBaseURL()+jsonObject.getString("recordingsfile"));
            }

            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                casteObj.setImage_tag(jsonObject.getString("image_tag"));
            }

            if (jsonObject.has("multi_image") && jsonObject.get("multi_image") instanceof JSONArray) {
                JSONArray imgArr = jsonObject.getJSONArray("multi_image");
                ArrayList<String> imgList = new ArrayList<>();
                for (int i = 0; i < imgArr.length(); i++) {
                    imgList.add(Constants.getImageBaseURL() + imgArr.getString(i));
                }
                casteObj.setMulti_image(imgList);
                Log.d("parseNoticeObject", "Multi Image List: " + imgList.toString());
            }


        } catch (JSONException e) {
           // Log.e("parseNoticeObject", "JSONException: " + e.getMessage(), e);
            e.printStackTrace();
        } catch (ParseException e) {
          //  Log.e("parseNoticeObject", "ParseException: " + e.getMessage(), e);
            e.printStackTrace();
        }
        return casteObj;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }
}
