package com.ultimate.ultimatesmartstudent.NoticeMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Thought_bean {

    private String id;
    private String message;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public static ArrayList<Thought_bean> parseALBMlistarray(JSONArray jsonArray) {
        ArrayList<Thought_bean> list = new ArrayList<Thought_bean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Thought_bean p =  parseALBMlistobject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    private static Thought_bean parseALBMlistobject(JSONObject jsonObject) {
        Thought_bean adminlistbean=new Thought_bean();

        try {
            if (jsonObject.has("id")) {
                adminlistbean.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("message") && !jsonObject.getString("message").isEmpty() && !jsonObject.getString("message").equalsIgnoreCase("null")) {
                adminlistbean.setMessage(jsonObject.getString("message"));
            }
            if (jsonObject.has("date") && !jsonObject.getString("date").isEmpty() && !jsonObject.getString("date").equalsIgnoreCase("null")) {
                adminlistbean.setDate(jsonObject.getString("date"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return adminlistbean;
    }
}
