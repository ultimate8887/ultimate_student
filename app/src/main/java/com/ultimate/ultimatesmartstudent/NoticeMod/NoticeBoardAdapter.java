package com.ultimate.ultimatesmartstudent.NoticeMod;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoticeBoardAdapter extends RecyclerView.Adapter<NoticeBoardAdapter.Viewholder> {
    Context mContext;
    ArrayList<NoticeBean> noticeList;

    public NoticeBoardAdapter(ArrayList<NoticeBean> noticeList, Context mContext) {
        this.noticeList = noticeList;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_notice_list, parent, false);
        NoticeBoardAdapter.Viewholder viewholder = new NoticeBoardAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull NoticeBoardAdapter.Viewholder holder, int position) {
        holder.txtNTitle.setText(noticeList.get(position).getTitle());
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS")){
            holder.txtMsg.setText(Html.fromHtml(noticeList.get(position).getMsg()));
        }else{
            holder.txtMsg.setText(noticeList.get(position).getMsg());
        }
        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(noticeList.get(position).getNew_date());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.txtTime.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.txtTime.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.txtTime.setText(Utils.getDateFormated(noticeList.get(position).getNew_date()));
            //  Toast.makeText(mContext, "checkView"+Utility.getDateFormated(noticeList.get(position).getNew_date()), Toast.LENGTH_SHORT).show();
        }

        if((position % 2) == 0){ //Even section
            Picasso.get().load(R.drawable.notice_icon_list).into(holder.imgNInfo);
        }else{
            Picasso.get().load(R.drawable.notice_icon_list).into(holder.imgNInfo);
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    public void setNList(ArrayList<NoticeBean> noticeList) {
        this.noticeList = noticeList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtNTitle)
        TextView txtNTitle;
        @BindView(R.id.txtMsg)
        TextView txtMsg;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.imgNInfo)
        ImageView imgNInfo;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}