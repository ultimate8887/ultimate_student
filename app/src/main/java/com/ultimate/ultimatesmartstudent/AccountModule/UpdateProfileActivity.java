package com.ultimate.ultimatesmartstudent.AccountModule;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.firebase.ui.auth.IdpResponse;
import com.github.siyamed.shapeimageview.CircularImageView;

import com.ultimate.ultimatesmartstudent.AddDocument.AdddocumentActivity;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateProfileActivity extends AppCompatActivity {

    private int selection;
    private Bitmap userBitmap=null, userBitmapProf=null;
    private int VERIFY_NUMBER = 1000;
    Animation animation;
    @BindView(R.id.circleimgrecepone)
    CircularImageView image;
    @BindView(R.id.nametxtone)
    TextView name;
    @BindView(R.id.classtxtone)
    TextView classname;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.edit_profile)
    ImageView edit_profile;
    @BindView(R.id.submit)
    TextView submit;
    String image_url="";
    CommonProgress commonProgress;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
        userprofile();
        selectImg();
    }

    private void userprofile() {
      //  commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        if (userBitmapProf != null) {
            String encoded = Utils.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image", encoded);
        }else{
            params.put("image", "");
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                UltimateProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        Log.i("USERDATA", jsonObject.getJSONObject(Constants.USERDATA).getString("profile"));
                        image_url=jsonObject.getJSONObject(Constants.USERDATA).getString("profile");
                        Utils.setNaviHeaderData(name,classname,image,image_url,UpdateProfileActivity.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(UpdateProfileActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_profile)
    public void edit_profile() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_profile.startAnimation(animation);

        selectImg();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void selectImg() {
        selection=1;


    }


    @OnClick(R.id.submit)
    public void submitt() {
        submit.startAnimation(animation);
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        if (userBitmapProf != null) {
            String encoded = Utils.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image", encoded);
        }else{
            params.put("image", "");
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                UltimateProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        Log.i("USERDATA", jsonObject.getJSONObject(Constants.USERDATA).getString("profile"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(UpdateProfileActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                    //   Log.i("USERDATA", User.getCurrentUser().getAadhar_image());
                    finish();
                } else {
                    Toast.makeText(UpdateProfileActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


}