package com.ultimate.ultimatesmartstudent.AccountModule.ImgPdfConvert;

import androidx.annotation.NonNull;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.ultimate.ultimatesmartstudent.R;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageToPdfConvert extends AppCompatActivity implements Picker.PickListener {

    SharedPreferences sharedPreferences;
    private static final String TAG = "StdQrCheckout";
    private static final int PICK_IMAGE_REQUEST_CODE = 1;
    private static final int TAKE_PICTURE_REQUEST_CODE = 2;
    private File myPDF;
    private static LinkedList<Uri> imageList;
    private ArrayList<ImageEntry> mSelectedImages;// = new ArrayList<ImageEntry>();
    private RecyclerView mImageSampleRecycler;
    private RecyclerView.Adapter myAdapter;
    private ViewSwitcher switcher;
    File pdfFolder;
    private MenuItem mPortraitMenuItem;
    private MenuItem mLandscapeMenuItem;
    private boolean isPortrait = true;
    ImageButton galleryButton;
    @BindView(R.id.add)
    ImageButton add;
    @BindView(R.id.convertButton)
    ImageButton convertButton;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    Animation animation;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    Document document;
    String one="",second="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_to_pdf_convert);
        ButterKnife.bind(this);
        isStoragepermissiongranted(ImageToPdfConvert.this);
        switcher = (ViewSwitcher) findViewById(R.id.profileSwitcher);
        mImageSampleRecycler = (RecyclerView) findViewById(R.id.my_recycler_view);
        galleryButton=(ImageButton)findViewById(R.id.galleryButton);
        setupRecycler();
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText(getString(R.string.imgtopdf));
        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add.startAnimation(animation);
                pickImages();
            }
        });

        if (!restorePrefDataP()){
            setShowcaseViewP();
        }

        // recycle view must start with an adaptor or else layout will become onresponsive
        myAdapter = new ImageSamplesAdapter(mSelectedImages, ImageToPdfConvert.this);
        mImageSampleRecycler.setAdapter(myAdapter);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("ImageToPdfConverter");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean isStoragepermissiongranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }

        } else {
            return true;
        }


    }

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(galleryButton,getString(R.string.g_button),getString(R.string.g_button_desc))
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_add_img",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_add_img",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_add_img",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_add_img",false);
    }





    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(convertButton,getString(R.string.s_button),getString(R.string.s_button_desc))
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_pdf_img",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_pdf_img",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_pdf_img",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_pdf_img",false);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT).show();

        }else if(grantResults[0]==PackageManager.PERMISSION_DENIED){
            Toast.makeText(this,"Write external storagepermission required",Toast.LENGTH_SHORT).show();
        }
    }
    @OnClick(R.id.imgBackmsg)
    public void onback() {
        back.startAnimation(animation);
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menusec, menu);
        mPortraitMenuItem = menu.findItem(R.id.action_portrait);
        mLandscapeMenuItem = menu.findItem(R.id.action_landscape);

        mPortraitMenuItem.setChecked(true);

        return true;
    }


    // listener which executes code for when items in the option menu are selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // opens up gallery image picker to add additional images to recycle view
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle(getString(R.string.alert));
                builder1.setMessage(getString(R.string.alert_body));
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                return true;

            case R.id.action_gallery:
                // opens up gallery image picker to add additional images to recycle view
                pickImages();
                return true;

            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;
            case R.id.action_clear:
                // Removes all the images from the recycle view
                mSelectedImages = null;
                myAdapter = new ImageSamplesAdapter(mSelectedImages, ImageToPdfConvert.this);
                mImageSampleRecycler.setAdapter(myAdapter);
                return true;

            case R.id.action_portrait:
                // check marks portrait and disables landscape
                // changes flag
                if (mLandscapeMenuItem.isChecked()) {
                    item.setChecked(true);
                    mLandscapeMenuItem.setChecked(false);
                    isPortrait = true;
                }


                return true;

            case R.id.action_landscape:
                // check marks landscape and disables portrait
                // changes flag
                if (mPortraitMenuItem.isChecked()) {
                    item.setChecked(true);
                    mPortraitMenuItem.setChecked(false);
                    isPortrait = false;
                }



                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    // initializes the recycle view with a grid layout with vertical orientation
    private void setupRecycler() {
//
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, getResources().getInteger(R.integer.num_columns_image_samples));
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mImageSampleRecycler.setLayoutManager(gridLayoutManager);

    }

    //Button Listener for opening gallery page
    @OnClick(R.id.galleryButton)
    public void btnGallHandler() {

        pickImages();

    }

    // Starts the multi image picker gallery
    private void pickImages(){

        //You can change many settings in builder like limit , Pick mode and colors
        new Picker.Builder(this, this ,R.style.AppTheme)
                .build()
                .startActivity();

    }

    // listeners for multi image picker
    // When the selected pictures are returned from picker gallery...
    @Override
    public void onPickedSuccessfully(ArrayList<ImageEntry> images) {
        // call adaptor here for listview

        // if our global image array is empty, initialized it with the images returned from the multi-image picker gallery
        if (mSelectedImages == null)
            mSelectedImages = images;

            // else, add additional images to the bottom of the list
        else
            mSelectedImages.addAll(images);

        //setupImageSamples();
        Log.d(TAG, "Picked images  " + images.toString());

        // refresh adapter to display changes
        myAdapter = new ImageSamplesAdapter(mSelectedImages, ImageToPdfConvert.this);
        mImageSampleRecycler.setAdapter(myAdapter);

        // if we aren't on the second layout, switch. else do nothing
        if (switcher.getNextView() ==  findViewById(R.id.myRelativeLayout1) )
            switcher.showNext();

        myAdapter.notifyDataSetChanged();


        if (!restorePrefData()){
            setShowcaseView();
        }

    }

    // When there are no picture selected from the picker gallery...
    @Override
    public void onCancel() {
        //Log.i(TAG, "User canceled picker activity");
        Toast.makeText(this, "Image picker cancelled", Toast.LENGTH_SHORT).show();

    }

    // button listener for converting images to pdf file

    @OnClick(R.id.convertButton)
    public void onConvertPdfClick()
    {
        convertButton.startAnimation(animation);
        try {
            createPdf();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
       // Toast.makeText(this, "Pdf file created", Toast.LENGTH_SHORT).show();
    }



    // function that converts image data into a pdf file
    public void createPdf() throws  DocumentException, java.io.IOException
    {



        try {

            // creates folder with a pathname including the android storage directory
            if (Build.VERSION.SDK_INT > 19) {
                pdfFolder = new File(Environment.getExternalStorageDirectory(), "Download");
            }else{
                pdfFolder = new File(Environment.getExternalStorageDirectory(), "Downloads");
            }// check this warning, may be important for diff API levels

            //ProgressBar progress = (ProgressBar) findViewById(R.id.progressBar);

            // if the directory doesn't already exist, create it
            if (!pdfFolder.exists()) {
                pdfFolder.mkdirs();
                Log.i(TAG, "Folder successfully created");
            }

            // as long as we have images in the recycle view...
            if (mSelectedImages != null) {

                // progress.setVisibility(View.VISIBLE);

                // name the pdf with the current timestamp by default
                Date date = new Date();
                final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);
                myPDF = new File(pdfFolder + "/" + timeStamp + ".pdf");



                // point an output stream to our created document
                OutputStream output = new FileOutputStream(myPDF);


                // create a document with difference page sizes depending on orientation
                if (isPortrait)
                    document = new Document(PageSize.A4, 50, 50, 50, 50);
                else
                    document = new Document(PageSize.A4.rotate(), 50, 50, 50, 50);

                PdfWriter.getInstance(document, output);

                long startTime, estimatedTime;

                document.open();
                //document.add(new Paragraph("~~~~Hello World!!~~~~"));


                // loop through all the images in the array
                for (int i = 0; i < mSelectedImages.size(); i++) {

                    // create bitmap from URI in our list
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(mSelectedImages.get(i).path)));

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();

                    startTime = System.currentTimeMillis();

                    // changed from png to jpeg, lowered processing time greatly
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);

                    estimatedTime = System.currentTimeMillis() - startTime;

                    Log.e(TAG, "compressed image into stream: " + estimatedTime);

                    byte[] byteArray = stream.toByteArray();

                    // instantiate itext image
                    com.itextpdf.text.Image img = com.itextpdf.text.Image.getInstance(byteArray);

                    //img.scalePercent(40, 40);
                    //img.setAlignment(Element.ALIGN_CENTER);

                    //img.scaleAbsolute(PageSize.A4.getWidth(), PageSize.A4.getHeight());


                    // scale the image and set it to center
                    if (isPortrait) {
                        img.scaleToFit(PageSize.A4);
                        img.setAbsolutePosition(
                                (PageSize.A4.getWidth() - img.getScaledWidth()) / 2,
                                (PageSize.A4.getHeight() - img.getScaledHeight()) / 2
                        );
                    }
                    else
                    {
                        img.scaleToFit(PageSize.A4.rotate());
                        img.setAbsolutePosition(
                                (PageSize.A4.rotate().getWidth() - img.getScaledWidth()) / 2,
                                (PageSize.A4.rotate().getHeight() - img.getScaledHeight()) / 2
                        );
                    }
                    document.add(img);

                    // add a new page to the document to maintain 1 image per page
                    document.newPage();

                    float fractionalProgress = (i + 1) / mSelectedImages.size() * 100;




                }

                //progress.cancel();

                //start renaming
                LayoutInflater li = LayoutInflater.from(ImageToPdfConvert.this);
                View promptsView = li.inflate(R.layout.layout, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        ImageToPdfConvert.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // get user input and set it to result
                                        // edit text
                                        String fileName = userInput.getText().toString();

                                        if (!fileName.isEmpty() || !fileName.equalsIgnoreCase("")) {

                                            //myPDF = new File(pdfFolder + "/" + fileName + ".pdf");
                                            File newFile = new File(pdfFolder + "/" + fileName + ".pdf");
                                            boolean result = myPDF.renameTo(newFile);

                                            myPDF = newFile;

                                            Log.w(TAG, "myPDF renamed to: " + myPDF.toString());
                                            promptForNextAction();

                                            mSelectedImages = null;
                                            document.close();

                                            Toast.makeText(getApplicationContext(), "Save Successfully!", Toast.LENGTH_SHORT).show();
                                        }else{
                                            Toast.makeText(getApplicationContext(), "Kindly add Pdf name, Pdf name not be empty!", Toast.LENGTH_LONG).show();
                                        }

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                        //promptForNextAction();


                                    }
                                });

                // create alert dialog
                //AlertDialog alertDialog = alertDialogBuilder.create();

                // show it

                Log.e(TAG, "Before alertdialogue.show");


                alertDialogBuilder.show();



                // promptForNextAction();


                Log.e(TAG, "After alertdialogue.show, and before promptfornextaction");

                Log.e(TAG, "prompt for next action has completed");

                myAdapter = new ImageSamplesAdapter(mSelectedImages, ImageToPdfConvert.this);
                mImageSampleRecycler.setAdapter(myAdapter);

                //progress.setVisibility(View.GONE);

            }
        }catch (DocumentException e){
            e.printStackTrace();
            myPDF.delete();

            // check if pdf file exists
            // if so, remove pdf file

        }
        catch (IOException e){
            e.printStackTrace();
            myPDF.delete();
            // check if pdf file exists
            // if so, remove pdf file
        }
    }

    private void viewPdf(){
        PackageManager packageManager = this.getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        // testIntent.setType("application/pdf");


        Uri uri = FileProvider.getUriForFile(this, "com.ultimate.ultimatesmartstudent.fileprovider", myPDF);
        testIntent.setDataAndType(uri, "application/pdf");
        testIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        this.startActivity(testIntent);
      //  finish();

    }

    private void emailNote()
    {
        Intent email = new Intent(Intent.ACTION_SEND);
        //email.putExtra(Intent.EXTRA_SUBJECT,"hello world");
        //email.putExtra(Intent.EXTRA_TEXT, "hello world");
        Uri uri = Uri.parse(myPDF.getAbsolutePath());
        email.putExtra(Intent.EXTRA_STREAM, uri);
        email.setType("message/rfc822");
        startActivity(email);
       // finish();
    }

    public void promptForNextAction()
    {

        final Dialog logoutDialog = new Dialog(ImageToPdfConvert.this);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setCancelable(false);
        logoutDialog.setContentView(R.layout.pdfoption_dialog);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);
        Button email = (Button) logoutDialog.findViewById(R.id.email);
        Button preview = (Button) logoutDialog.findViewById(R.id.preview);
        Button cancel = (Button) logoutDialog.findViewById(R.id.cancel);
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailNote();

            }
        });
        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPdf();
               // logoutDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                logoutDialog.dismiss();

            }
        });

        logoutDialog.show();
    }
}