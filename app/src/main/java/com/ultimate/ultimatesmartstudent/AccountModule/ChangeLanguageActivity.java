package com.ultimate.ultimatesmartstudent.AccountModule;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.util.ui.BucketedTextChangeListener;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.HomeActivity;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangeLanguageActivity extends AppCompatActivity {

    @BindView(R.id.cardView_en)
    CardView english;
    @BindView(R.id.cardView_hi)
    CardView hindi;
    @BindView(R.id.cardView_pa)
    CardView punjabi;

    @BindView(R.id.r_en)
    RelativeLayout r_en;
    @BindView(R.id.r_hi)
    RelativeLayout r_hi;
    @BindView(R.id.r_pa)
    RelativeLayout r_pa;

    @BindView(R.id.en)
    TextView en;
    @BindView(R.id.hi)
    TextView hi;
    @BindView(R.id.pa)
    TextView pa;

    @BindView(R.id.en_img)
    ImageView en_img;
    @BindView(R.id.hi_img)
    ImageView hi_img;
    @BindView(R.id.pa_img)
    ImageView pa_img;

    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;

    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Animation animation,blink;
    String enn="",hii="",paa="",check="";

    SharedPreferences sharedPreferences;
    Window window;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        ButterKnife.bind(this);
        blink = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.btn_blink_animation);
        window=this.getWindow();
        window.setStatusBarColor(this.getResources().getColor(R.color.transparent));

        if (getIntent().getExtras() != null) {
            check = getIntent().getExtras().getString("tag");
        }

        if (!check.equalsIgnoreCase("")){
            imgBackmsg.setVisibility(View.GONE);
            submit.setText(getString(R.string.conti));
            submit.setVisibility(View.VISIBLE);
            txtTitle.setText("Setup App Language");
        }else {
            txtTitle.setText(getString(R.string.lag_title));
            imgBackmsg.setVisibility(View.VISIBLE);
            submit.setText(getString(R.string.update));
            submit.setVisibility(View.GONE);
        }

        checkLanguage();
    }

    private void checkLanguage() {
        sharedPreferences = getApplicationContext().getSharedPreferences("setting",MODE_PRIVATE);
        String lang =sharedPreferences.getString("app_lang","");
        if (lang != null && lang.equalsIgnoreCase("pa")){
           setPunjabi();
        }else if (lang != null &&lang.equalsIgnoreCase("hi")){
           setHindi();
        }else {
           setEnglish();
        }
        // Toast.makeText(getApplicationContext(),"Language "+check,Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.imgBackmsg)
    public void callBackF() {
        imgBackmsg.startAnimation(blink);
        finish();
    }

    @OnClick(R.id.cardView_en)
    public void english() {
        setEnglish();
        submit.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(),"Selected English",Toast.LENGTH_SHORT).show();

    }

    private void setEnglish() {
        // setLocale("en");
        en.setTextColor(Color.parseColor("#ffffff"));
        hi.setTextColor(Color.parseColor("#000000"));
        pa.setTextColor(Color.parseColor("#000000"));
        r_en.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_c_bg) );
        r_hi.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_bg) );
        r_pa.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_bg) );
        enn="en";
        hii="";
        paa="";
    }

    private void setHindi() {
        // setLocale("hi");
        hi.setTextColor(Color.parseColor("#ffffff"));
        en.setTextColor(Color.parseColor("#000000"));
        pa.setTextColor(Color.parseColor("#000000"));
        r_hi.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_c_bg) );
        r_en.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_bg) );
        r_pa.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_bg) );
        enn="";
        hii="hi";
        paa="";

    }


    private void setPunjabi() {
        //  setLocale("pa");
        pa.setTextColor(Color.parseColor("#ffffff"));
        en.setTextColor(Color.parseColor("#000000"));
        hi.setTextColor(Color.parseColor("#000000"));
        r_pa.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_c_bg) );
        r_en.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_bg) );
        r_hi.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.w_bg) );
        enn="";
        hii="";
        paa="pa";
    }


    private void commonCode() {
        Intent loginpage = new Intent(ChangeLanguageActivity.this, HomeActivity.class);
        loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginpage);
    }


    @OnClick(R.id.cardView_hi)
    public void hindi() {
      setHindi();
        submit.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(),"Selected Hindi",Toast.LENGTH_SHORT).show();

    }

    @OnClick(R.id.cardView_pa)
    public void punjabi() {
     setPunjabi();
        submit.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(),"Selected Punjabi",Toast.LENGTH_SHORT).show();

    }

    private void setLocale(String language) {

        Locale locale=new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration=new Configuration();
        configuration.locale=locale;
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        sharedPreferences=getApplicationContext().getSharedPreferences("setting",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("app_lang",language);
        editor.apply();

    }

    @OnClick(R.id.submit)
    public void submitt() {
        if (paa.equalsIgnoreCase("pa")){
            setLocale("pa");
        }else if (hii.equalsIgnoreCase("hi")){
            setLocale("hi");
        }else {
            setLocale("en");
        }
     commonCode();
    }

}