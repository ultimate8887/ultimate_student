package com.ultimate.ultimatesmartstudent.AccountModule;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.HomeActivity;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountActivity extends AppCompatActivity implements AccountAdapter.AdapterCallback {
    SharedPreferences sharedPreferences;
    @BindView(R.id.circleimgrecepone)
    CircularImageView image;
    @BindView(R.id.useridtxtone)
    TextView userid;
    @BindView(R.id.nametxtone)
    TextView name;
    @BindView(R.id.classtxtone)
    TextView classname;
    @BindView(R.id.usernametxtone)
    TextView username;
    @BindView(R.id.addaccount)
    CardView addaccount;
    @BindView(R.id.textsignout)
    CardView logout;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txttitle;
    String image_url="";
    @BindView(R.id.recyclerViewAccount)
    RecyclerView recyclerView;
    ArrayList<User> userlist = new ArrayList<>();
    ArrayList<User> ul = new ArrayList<>();
    RecyclerView.LayoutManager layoutManagersec;
    RecyclerView.LayoutManager layoutManager;
    private AccountAdapter adapter;
    CommonProgress commonProgress;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        txttitle.setText(getString(R.string.login_user));
        Log.e("sioful", String.valueOf(ul.size()));
        ul = User.getUserList();
        userprofile();
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });

        if (!restorePrefDataP()){
            setShowcaseViewP();
        }

        userid.setText(User.getCurrentUser().getId());
        username.setText(User.getCurrentUser().getUsername());

        for (int i = 0; i < ul.size(); i++) {
            if (!User.getCurrentUser().getId().equals(ul.get(i).getId())) {

                userlist.add(ul.get(i));
//                layoutManager=new LinearLayoutManager(this);
//                recyclerView.setLayoutManager(layoutManager);
//                //adapter=new AccountAdapter(User.getUserList(),this,this);
//                adapter=new AccountAdapter(userlist,this,this);
//                recyclerView.setAdapter(adapter);
//                Log.e("usergetlist", String.valueOf(userlist));
            }
//            else{
//                Toast.makeText(getApplicationContext(),"only 1 user",Toast.LENGTH_SHORT).show();
//
//            }


        }


//        //TODO change
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //adapter=new AccountAdapter(User.getUserList(),this,this);
        adapter = new AccountAdapter(userlist, this, this);
        recyclerView.setAdapter(adapter);


        addaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addaccount.startAnimation(animation);
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout.startAnimation(animation);
                final Dialog logoutDialog = new Dialog(AccountActivity.this);
                logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                logoutDialog.setCancelable(true);
                logoutDialog.setContentView(R.layout.logout_dialog);
                logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);
                String logout_user=User.getCurrentUser().getFirstname();
                String title1 = getColoredSpanned("<b>"+" " + logout_user + "!  "+"</b>", "#000000");
                String Name = getColoredSpanned(getString(R.string.log1), "#5A5C59");
                txt2.setText(Html.fromHtml( " " + title1+ " " + Name));

                CircularImageView img = (CircularImageView) logoutDialog.findViewById(R.id.img);
                if (User.getCurrentUser().getProfile() != null) {
                    Picasso.get().load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(img);
                } else {
                    Picasso.get().load(R.drawable.boy).into(img);
                }

                Button btnNo = (Button) logoutDialog.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        logoutDialog.dismiss();
                    }
                });
                Button btnYes = (Button) logoutDialog.findViewById(R.id.btnYes);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        logoutDialog.dismiss();
                        User.removeUserFromList();
                        User.logout();
                        Toast.makeText(getApplicationContext(),logout_user+"Logout Successfully!",Toast.LENGTH_SHORT).show();
                        if (User.getUserList() == null) {
                            Intent loginpage = new Intent(AccountActivity.this, LoginActivity.class);
                            loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(loginpage);
                            finish();


                        } else {
                            User.setCurrentUserUL(User.getUserList().get(0));
                            Intent intent = new Intent(AccountActivity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                });
                logoutDialog.show();
                Window window = logoutDialog.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });
    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(addaccount,"Add Button!",getString(R.string.add_btn_desc))
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40),TapTarget.forView(logout,"Logout Button!",getString(R.string.logout_btn_desc))
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_attend_staffP",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attend_staffP",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attend_staffP",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attend_staffP",false);
    }

    private void userprofile() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("image", "");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                UltimateProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        Log.i("USERDATA", jsonObject.getJSONObject(Constants.USERDATA).getString("profile"));
                        image_url=jsonObject.getJSONObject(Constants.USERDATA).getString("profile");
                        Utils.setNaviHeaderData(name,classname,image,image_url,AccountActivity.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(AccountActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @Override
    public void click(User user) {
        User.setCurrentUserUL(user);
        Toast.makeText(getApplicationContext(),user.getFirstname()+" Login Successfully!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(AccountActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
      //  startActivity(loginpage);
        startActivity(intent);
        finish();
    }
}
