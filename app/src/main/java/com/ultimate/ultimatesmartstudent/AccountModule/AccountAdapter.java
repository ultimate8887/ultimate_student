package com.ultimate.ultimatesmartstudent.AccountModule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.Viewholder> {
    private AdapterCallback mAdapterCallback;
    ArrayList<User> userList;
    // ArrayList<UserAccountBean> useraccountlist;
    Context context;
    String active="1";
    Boolean abc=true;
    Animation animation;
    public AccountAdapter(ArrayList<User> userList, Context context, AdapterCallback mAdapterCallback) {

        this.context=context;
        this.userList=userList;
        this.mAdapterCallback=mAdapterCallback;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.account_adpt_layout,parent,false);
        AccountAdapter.Viewholder viewholder=new AccountAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, final int position) {
        if(User.getCurrentUser()!= null) {

            if (userList.get(position).getLastname() == null) {
                holder.name.setText(userList.get(position).getFirstname());
            } else {
                holder.name.setText(userList.get(position).getFirstname() + " " + userList.get(position).getLastname());
            }
            if (userList.get(position).getSection_name() != null && userList.get(position).getSection_name() != null){
                holder.classname.setText(userList.get(position).getClass_name() + " (" + userList.get(position).getSection_name() + ")");
            } else{
            holder.classname.setText(userList.get(position).getClass_name());
             }
            holder.userid.setText("Roll No: " +userList.get(position).getId());
           // holder.username.setText(userList.get(position).getUsername());

            if (userList.get(position).getGender().equalsIgnoreCase("Male")) {
                if (userList.get(position).getProfile() != null) {
                    Picasso.get().load(userList.get(position).getProfile()).placeholder(R.drawable.boy).into(holder.imageView);
                } else {
                    Picasso.get().load(R.drawable.boy).into(holder.imageView);
                }
            }else{
                if (userList.get(position).getProfile() != null) {
                    Picasso.get().load(userList.get(position).getProfile()).placeholder(R.drawable.girl).into(holder.imageView);
                } else {
                    Picasso.get().load(R.drawable.girl).into(holder.imageView);
                }
            }

        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                holder.imgProfile.startAnimation(animation);
                mAdapterCallback.click(userList.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public interface AdapterCallback {
        public void click(User user);
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgStud)
        CircularImageView imageView;
        @BindView(R.id.imgProfile)
        ImageView imgProfile;

        @BindView(R.id.roll_no)
        TextView userid;
        @BindView(R.id.txtStudName)TextView name;
        @BindView(R.id.class_name)TextView classname;
      //  @BindView(R.id.usernametxt)TextView username;
        @BindView(R.id.layout)
        RelativeLayout layout;
        // @BindView(R.id.approve)ImageView active;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
