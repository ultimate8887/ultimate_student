package com.ultimate.ultimatesmartstudent.AttendMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AttendMod {
    private static String DATE = "date";
    private static String ATTEND = "attend";
    /**
     * date : 2018-03-02
     * attend : P
     */
    private String date;
    private String attend;
    private String day;

    private String ab;
    private String lv;
    private String pr;
    private String hl;

    private String hlv;

    public String getHlv() {
        return hlv;
    }

    public void setHlv(String hlv) {
        this.hlv = hlv;
    }

    public String getL_remarks() {
        return l_remarks;
    }

    public void setL_remarks(String l_remarks) {
        this.l_remarks = l_remarks;
    }

    private String l_remarks;

    public String getAb() {
        return ab;
    }

    public void setAb(String ab) {
        this.ab = ab;
    }

    public String getLv() {
        return lv;
    }

    public String getSal() {
        return sal;
    }

    public void setSal(String sal) {
        this.sal = sal;
    }

    private String sal;

    public void setLv(String lv) {
        this.lv = lv;
    }

    public String getPr() {
        return pr;
    }

    public void setPr(String pr) {
        this.pr = pr;
    }

    public String getHl() {
        return hl;
    }

    public void setHl(String hl) {
        this.hl = hl;
    }

    public String getTwcount() {
        return twcount;
    }

    public void setTwcount(String twcount) {
        this.twcount = twcount;
    }

    public String getTprcount() {
        return tprcount;
    }

    public void setTprcount(String tprcount) {
        this.tprcount = tprcount;
    }

    public String getPrtcont() {
        return prtcont;
    }

    public void setPrtcont(String prtcont) {
        this.prtcont = prtcont;
    }

    private String twcount;
    private String tprcount;
    private String prtcont;
    private String totalworkcountwoh;
    public String getTotalworkcountwoh() {
        return totalworkcountwoh;
    }

    public void setTotalworkcountwoh(String totalworkcountwoh) {
        this.totalworkcountwoh = totalworkcountwoh;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getAttend() {
        return attend;
    }
    public void setAttend(String attend) {
        this.attend = attend;
    }

    public static AttendMod parseAttendObject(JSONObject jsonObject) {
        AttendMod attendObj = new AttendMod();
        try {
            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                attendObj.setDate(jsonObject.getString(DATE));
                try {
                    String input_date = jsonObject.getString(DATE);
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-mm-dd");
                    Date dt1 = format1.parse(input_date);
                    DateFormat format2 = new SimpleDateFormat("dd");
                    String finalDay = format2.format(dt1);
                    attendObj.setDay(finalDay);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (jsonObject.has(ATTEND) && !jsonObject.getString(ATTEND).isEmpty() && !jsonObject.getString(ATTEND).equalsIgnoreCase("null")) {
                attendObj.setAttend(jsonObject.getString(ATTEND));
            }

            if (jsonObject.has("ab")) {
                Object abValue = jsonObject.get("ab");
                attendObj.setAb(abValue instanceof Integer ? String.valueOf(abValue) : (String) abValue);
            }

            if (jsonObject.has("lv")) {
                Object lvValue = jsonObject.get("lv");
                attendObj.setLv(lvValue instanceof Integer ? String.valueOf(lvValue) : (String) lvValue);
            }

            if (jsonObject.has("hlv")) {
                Object hlvValue = jsonObject.get("hlv");
                attendObj.setHlv(hlvValue instanceof Integer ? String.valueOf(hlvValue) : (String) hlvValue);
            }

            if (jsonObject.has("pr")) {
                Object prValue = jsonObject.get("pr");
                attendObj.setPr(prValue instanceof Integer ? String.valueOf(prValue) : (String) prValue);
            }

            if (jsonObject.has("sal")) {
                Object salValue = jsonObject.get("sal");
                attendObj.setSal(salValue instanceof Integer ? String.valueOf(salValue) : (String) salValue);
            }

            if (jsonObject.has("hl")) {
                Object hlValue = jsonObject.get("hl");
                attendObj.setHl(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

            if (jsonObject.has("l_remarks") && !jsonObject.getString("l_remarks").isEmpty() && !jsonObject.getString("l_remarks").equalsIgnoreCase("null")) {
                attendObj.setL_remarks(jsonObject.getString("l_remarks"));
            }
//            if (jsonObject.has("hlv") && !jsonObject.getString("hlv").isEmpty() && !jsonObject.getString("hlv").equalsIgnoreCase("null")) {
//                attendObj.setHlv(jsonObject.getString("hlv"));
//            }
//
//            if (jsonObject.has("sal")) {
//                attendObj.setSal(jsonObject.getString("sal"));
//            }

            if (jsonObject.has("twcount")) {
                Object hlValue = jsonObject.get("twcount");
                attendObj.setTwcount(hlValue instanceof Integer ? String.valueOf(hlValue) : (String) hlValue);
            }

//            if (jsonObject.has("twcount")) {
//                attendObj.setTwcount(String.valueOf(jsonObject.getInt("twcount")));
//            }

            if (jsonObject.has("tprcount") && !jsonObject.getString("tprcount").isEmpty() && !jsonObject.getString("tprcount").equalsIgnoreCase("null")) {
                attendObj.setTprcount(jsonObject.getString("tprcount"));
            }

            if (jsonObject.has("prtcont") && !jsonObject.getString("prtcont").isEmpty() && !jsonObject.getString("prtcont").equalsIgnoreCase("null")) {
                attendObj.setPrtcont(jsonObject.getString("prtcont"));
            }
            if (jsonObject.has("totalworkcountwoh") && !jsonObject.getString("totalworkcountwoh").isEmpty() && !jsonObject.getString("totalworkcountwoh").equalsIgnoreCase("null")) {
                attendObj.setTotalworkcountwoh(jsonObject.getString("totalworkcountwoh"));
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return attendObj;
    }
    public static ArrayList<AttendMod> parseAttendArray(JSONArray jsonArray) {
        try {

            ArrayList<AttendMod> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                AttendMod attendObj = parseAttendObject(jsonArray.getJSONObject(i));
                arrayList.add(attendObj);
            }
            return arrayList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
