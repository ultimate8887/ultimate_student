package com.ultimate.ultimatesmartstudent.AttendMod;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AttendAdapter extends RecyclerView.Adapter<AttendAdapter.Viewholder> {
    Context mContext;
    ArrayList<AttendMod> attendList;

    public AttendAdapter(ArrayList<AttendMod> attendList, Context mContext) {
        this.attendList = attendList;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weekly_att_list, parent, false);
        AttendAdapter.Viewholder viewholder = new AttendAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull AttendAdapter.Viewholder holder, int position) {
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.DAY_OF_WEEK, (position + 2));
        SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd"); //this format changeable
        String start_date = dateFormatter1.format(c1.getTime());
        holder.txtHoliday.setText(start_date);

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd"); //this format changeable
        String comparedate = dateFormatter.format(c1.getTime());
        for (int i = 0; i < attendList.size(); i++) {
            Log.e("date: ",comparedate+" , "+attendList.get(i).getDate()+"  "+attendList.get(i).getDate().equalsIgnoreCase(comparedate));
            if (attendList.get(i).getDate().equalsIgnoreCase(comparedate)) {
                if (attendList.get(i).getAttend().equalsIgnoreCase("p")) {
                    Picasso.get().load(R.drawable.c_g).into(holder.imgCircle);
                } else if (attendList.get(i).getAttend().equalsIgnoreCase("a")) {
                    Picasso.get().load(R.drawable.c_r).into(holder.imgCircle);
                } else if (attendList.get(i).getAttend().equalsIgnoreCase("l")) {
                    if (attendList.get(i).getL_remarks()!= null){
                        if(attendList.get(i).getL_remarks().equalsIgnoreCase("SAL")||attendList.get(i).getL_remarks().equalsIgnoreCase("sal")) {
                            Picasso.get().load(R.drawable.sal_c).into(holder.imgCircle);
                        } else if(attendList.get(i).getL_remarks().equalsIgnoreCase("HL")|| attendList.get(i).getL_remarks().equalsIgnoreCase("hl")) {
                            Picasso.get().load(R.drawable.c_hl).into(holder.imgCircle);
                        }else{
                            Picasso.get().load(R.drawable.c_y).into(holder.imgCircle);
                        }
                    }else {
                        Picasso.get().load(R.drawable.c_y).into(holder.imgCircle);
                    }

                }else if (attendList.get(i).getAttend().equalsIgnoreCase("holiday")) {
                    Picasso.get().load(R.drawable.hol_c).into(holder.imgCircle);
                }else{
                    Picasso.get().load(R.drawable.c_gg).into(holder.imgCircle);
                }
                break;
            }else{
                Picasso.get().load(R.drawable.c_gg).into(holder.imgCircle);
            }
        }
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public void setAList(ArrayList<AttendMod> attendList) {
        this.attendList = attendList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtHoliday)
        TextView txtHoliday;
        @BindView(R.id.imgCircle)
        ImageView imgCircle;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
