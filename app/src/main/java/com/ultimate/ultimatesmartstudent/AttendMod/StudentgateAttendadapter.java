package com.ultimate.ultimatesmartstudent.AttendMod;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentgateAttendadapter extends RecyclerView.Adapter<StudentgateAttendadapter.Viewholder> {
    public ArrayList<Studentbean> stulist;
    private ArrayList<Studentbean> filterModelClass;
    Context mContext;

    public StudentgateAttendadapter(ArrayList<Studentbean> stulist, Context mContext) {
        this.mContext = mContext;
        this.stulist = stulist;
        this.filterModelClass = stulist;
    }

    @Override
    public StudentgateAttendadapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.staff_item_news, parent, false);
        StudentgateAttendadapter.Viewholder viewholder = new StudentgateAttendadapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final StudentgateAttendadapter.Viewholder holder, final int position) {


        final Studentbean data = stulist.get(position);
        holder.spinnerAttend.setVisibility(View.GONE);

            if (User.getCurrentUser().getSchoolData().getLogo() != null) {
                Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(holder.circleimg);
            } else {
                Picasso.get().load(R.drawable.logo).into(holder.circleimg);
            }

//        holder.txtName.setText(data.getStudent_name());
//        holder.txtRegNo.setText("Father: "+data.getStudent_id());
//        holder.txtFname.setText("Father: "+data.getFather_name());

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getName() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(data.getName(), "#383737");
            holder.txtName.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtName.setText("Name: Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getId() != null) {
            String title = getColoredSpanned("Reg.No: ", "#5A5C59");
            String Name = getColoredSpanned(data.getId(), "#383737");
            holder.txtRegNo.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtRegNo.setText("ID: Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getFather_name() != null) {
            String title = getColoredSpanned("Father: ", "#5A5C59");
            String Name = getColoredSpanned(data.getFather_name(), "#383737");
            holder.txtFname.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtFname.setText("Father: Not Mentioned");
        }

        if (data.getClass_name()!=null){
            holder.txtDT.setText(Utils.getDateFormated(stulist.get(position).getAt_attendance_date()));
        }else {
            holder.txtDT.setText("Not Mentioned");
        }

        holder.check_in.setText(Utils.getDateTimeFormatedWithAMPM(stulist.get(position).getAt_attendance_date()));


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }



    public interface StudentPro{
        void StudentProfile(Studentbean std);
        void onUpdateCallback(Studentbean std);
        void stdCallBack(Studentbean std);
        void stdEmailCallBack(Studentbean std);
    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }


    public void setHQList(ArrayList<Studentbean> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtRegNo)
        TextView txtRegNo;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.spinnerAttend)
        Spinner spinnerAttend;
        @BindView(R.id.txtFname)
        TextView txtFname;
        @BindView(R.id.circleimg)
        CircularImageView circleimg;
        @BindView(R.id.txtReason)
        EditText check_in;
        @BindView(R.id.txtDT)
        TextView txtDT;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


    }
