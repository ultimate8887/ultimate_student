package com.ultimate.ultimatesmartstudent.AttendMod;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.E_book.Ebook;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.LoginActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AttendanceActivity extends AppCompatActivity {

    @BindView(R.id.gridview)
    RecyclerView recyclerView;
    @BindView(R.id.currentMonth)
    TextView currentMonth;
    @BindView(R.id.h_currentday)
    TextView h_currentday;
    @BindView(R.id.h_day_name)
    TextView h_day_name;
    @BindView(R.id.h_currentMonth)
    TextView h_currentMonth;
//    @BindView(R.id.parent)
//    LinearLayout parent;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    ArrayList<AttendMod> attendList = new ArrayList<>();
    int month;
    int year;
    int day;
    int day_name;
    private Calendar mCalendar;
    private MonthAdapter madapter;
    private String start_date;
    private String end_date;
    private List<String> mItems;
    private final String[] mDays = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    private final int[] mDaysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private int mDaysShown = 0;
    private int mDaysLastMonth=0;
    private int mDaysNextMonth=0;
    @BindView(R.id.noofpresent)TextView noofpresent;
    @BindView(R.id.noofabsent)TextView noofabsent;
    @BindView(R.id.noofleave)TextView noofleave;
    @BindView(R.id.noofholydy)TextView noofholydy;
    @BindView(R.id.sal_noofleave)TextView sal_noofleave;
    @BindView(R.id.totalwork)TextView totalwork;
    @BindView(R.id.totalpresent)TextView totalpresent;
    @BindView(R.id.annualpercent)TextView annualpercent;
    @BindView(R.id.attenddetailpercentlay)
    CardView attenddetailpercentlay;
//    @BindView(R.id.holidylayout)
//    RelativeLayout holidylayout;
    @BindView(R.id.totalannualworkd)TextView totalannualworkd;

    Animation animation,animation1,animation2,animation3,top_curve_anim;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.h_calender)
    ImageView h_calender;
    @BindView(R.id.btnBefore)
    TextView btnBefore;
    @BindView(R.id.btnNext)
    TextView btnNext;
    @BindView(R.id.lytCard)
    LinearLayout lytCard;
    @BindView(R.id.lytCard1)
    LinearLayout lytCard1;
    @BindView(R.id.spinner2)
    Spinner SubjectSpinner;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    String  sub_id, sub_name;
    @BindView(R.id.stdlayout)
    RelativeLayout stdlayout;
    SharedPreferences sharedPreferences;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        ButterKnife.bind(this);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.logo_animation);
        animation2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.enter_from_left_animation);
        animation3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.enter_from_right_animation);
        top_curve_anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_animation);
        h_day_name.startAnimation(animation3);
        h_currentMonth.startAnimation(animation3);
        lytCard.startAnimation(animation2);
        lytCard1.startAnimation(animation3);
        h_currentday.startAnimation(top_curve_anim);
        currentMonth.startAnimation(top_curve_anim);
        h_currentMonth.startAnimation(top_curve_anim);
        h_day_name.startAnimation(top_curve_anim);
        h_calender.startAnimation(animation1);
        txtTitle.setText(getString(R.string.attendance));
        txtSub.setText(User.getCurrentUser().getFirstname()+""+User.getCurrentUser().getLastname()+" ("+User.getCurrentUser().getId()+")");
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
            stdlayout.setVisibility(View.VISIBLE);
            if (!restorePrefData1()){
                setShowcaseView1();
            }
            getSubjectofClass();
        }else {
            stdlayout.setVisibility(View.GONE);
            setCalendar();
        }

    }

    private void getSubjectofClass() {
        UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);
    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));
                    AdapterOfSubjectList adaptersub = new AdapterOfSubjectList(AttendanceActivity.this, subjectList);
                    SubjectSpinner.setAdapter(adaptersub);
                    SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }
                            setCalendar();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void savePrefData1(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_attend1",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attend1",true);
        editor.apply();
    }

    private boolean restorePrefData1(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attend1",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attend1",false);
    }

    private void setShowcaseView1() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(stdlayout,"Spinner Button!","Tap the Spinner Button and select Subject to view Subject-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(true)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData1();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void fetchAttedance() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, User.getCurrentUser().getId());
        params.put("start_date", start_date);
        params.put("end_date", end_date);

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
            params.put("sub_id", sub_id);
            params.put("tag", "college");
        }else {
            params.put("sub_id", "");
            params.put("tag", "school");
        }

        Log.e("start_date",start_date);
        Log.e("end_date",end_date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ATTENDANCE_URL, apiCallback, this, params);

    }

    @OnClick(R.id.imgBackmsg)
    public void onBackclick() {
        back.startAnimation(animation);
        finish();
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    attendList.clear();
                    attendList = AttendMod.parseAttendArray(jsonObject.getJSONArray("attend_data"));
                    if (attendList.get(0).getPr() != null) {
                        noofpresent.setText(attendList.get(0).getPr()+"d");
                    } else {
                        noofpresent.setText("0d");
                    }
                    if (attendList.get(0).getAb() != null) {
                        noofabsent.setText(attendList.get(0).getAb()+"d");
                    } else {
                        noofabsent.setText("0d");
                    }
                    if (attendList.get(0).getLv() != null) {
                        noofleave.setText(attendList.get(0).getLv()+"d");
                    } else {
                        noofleave.setText("0d");
                    }
                    if(attendList.get(0).getSal()!=null ) {
                        sal_noofleave.setText(attendList.get(0).getSal()+"d");
                        //  Log.e("no of holyday",attendList.get(0).getHl());
                    }else{
                        sal_noofleave.setText("0"+"d");
                    }

                    if (attendList.get(0).getHlv() != null) {
                        noofholydy.setText(attendList.get(0).getHlv()+"d");
                    } else {
                        noofholydy.setText("0d");
                    }
//                    if (User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Global Discovery School")) {
//                        holidylayout.setVisibility(View.VISIBLE);
//                        if (attendList.get(0).getHl() != null) {
//                            noofholydy.setText(attendList.get(0).getHl());
//                        } else {
//                            noofholydy.setText("0");
//                        }
//                    }else{
//                        holidylayout.setVisibility(View.GONE);
//                    }

     //               if (User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Global Discovery School")){
                        attenddetailpercentlay.setVisibility(View.GONE);
                        if (attendList.get(0).getTwcount() != null) {
                            totalwork.setText("Total Working Day: " + attendList.get(0).getTwcount());
                        } else {
                            totalwork.setText("0");
                        }
                        if (attendList.get(0).getTprcount() != null) {
                            totalpresent.setText("Total Annual Present Day: " + attendList.get(0).getTprcount());

                        } else {
                            totalpresent.setText("0");

                        }
                        if(attendList.get(0).getTotalworkcountwoh()!=null){
                            totalannualworkd.setText("Total Annual Working Day: "+attendList.get(0).getTotalworkcountwoh());
                        }else{
                            totalannualworkd.setText("0");
                        }

                        if (attendList.get(0).getPrtcont() != null) {
                            annualpercent.setText("Annual(%) Till Current Date: " + attendList.get(0).getPrtcont());
                        } else {
                            annualpercent.setText("0");
                        }
                 //   }else{
//                        attenddetailpercentlay.setVisibility(View.GONE);
//                    }
                    madapter.setAttendList(attendList);
//                    madapter.notifyDataSetChanged();
                    //setanimation on adapter...
                    recyclerView.getAdapter().notifyDataSetChanged();
                    recyclerView.scheduleLayoutAnimation();
                    lytCard.startAnimation(animation2);
                    lytCard1.startAnimation(animation3);
                    //-----------end------------
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                attendList.clear();
                madapter.setAttendList(attendList);
               // madapter.notifyDataSetChanged();
                //setanimation on adapter...
                recyclerView.getAdapter().notifyDataSetChanged();
                recyclerView.scheduleLayoutAnimation();
                //-----------end------------
                lytCard.startAnimation(animation2);
                lytCard1.startAnimation(animation3);
                setZero(error);
             //   noofholydy.setText("0");
                if (error.getStatusCode() != 401) {
                  //  Utils.showSnackBar(error.getMessage(), parent);
                    setZero(error);
                }
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AttendanceActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void setZero(ApiHandlerError error) {
        noofholydy.setText("0d");
        noofleave.setText("0d");
        noofabsent.setText("0d");
        noofpresent.setText("0d");
        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
    }

    public void setCalendar() {
        mCalendar = Calendar.getInstance();

        //new code add by saurabh
        day = mCalendar.get(Calendar.DATE);
        day_name = mCalendar.get(Calendar.DAY_OF_WEEK);
        String weekday = new DateFormatSymbols().getWeekdays()[day_name];
        h_currentday.setText(String.valueOf(day));
        h_day_name.setText(weekday);
        // end..........

        month = mCalendar.get(Calendar.MONTH); // zero based
        year = mCalendar.get(Calendar.YEAR);

//        Toast.makeText(getApplicationContext(),day,Toast.LENGTH_SHORT).show();
        recyclerView.setLayoutManager(new GridLayoutManager(this, 7));
        recyclerView.setHasFixedSize(false);


        getDateList(month, year);


        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));

        h_currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));



        String setmonth =""+(month + 1);
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        //end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        fetchAttedance();
    }

    @OnClick(R.id.btnNext)
    public void onClickNext() {
        btnNext.startAnimation(animation);

        month = month + 1;

        if (month > 11) {
            month = 0;
            year = year + 1;
        }

        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));
        mCalendar.set(Calendar.MONTH, month);
        getDateList(month, year);

        String setmonth = ""+(month + 1);
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        // end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        fetchAttedance();
    }

    @OnClick(R.id.btnBefore)
    public void onClickBefore() {
        btnBefore.startAnimation(animation);
        month = month - 1;

        if (month < 0) {
            month = 11;
            year = year - 1;
        }
        mCalendar.set(Calendar.MONTH, month);


        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));
        getDateList(month, year);


        String setmonth = ""+(month + 1);
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        // end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        //end_date = year + "-" + setmonth + "-" + mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        fetchAttedance();
    }

    public void getDateList(int mMonth, int mYear) {
        mDaysLastMonth=0;
        mDaysNextMonth=0;
        mDaysShown =0;
        GregorianCalendar gCal = new GregorianCalendar(mYear, mMonth, 1);
        ArrayList<String> mItems = new ArrayList<String>();
        for (String day : mDays) {
            mItems.add(day);
            mDaysShown++;
        }

        int firstDay = getDay(gCal.get(Calendar.DAY_OF_WEEK));
        int prevDay;
        if (mMonth == 0) {
            mMonth = 11;
        }
        prevDay = daysInMonth(mMonth - 1, gCal, mYear) - firstDay + 1;



//        if (mMonth == 0)
//            prevDay = daysInMonth(11, gCal, mYear) - firstDay + 1;
//        else
//            prevDay = daysInMonth(mMonth - 1, gCal, mYear) - firstDay + 1;

        for (int i = 0; i < firstDay; i++) {
            mItems.add(String.valueOf(prevDay + i));
            mDaysLastMonth++;
            mDaysShown++;
        }

        int daysInMonth = daysInMonth(mMonth, gCal, mYear);
        for (int i = 1; i <= daysInMonth; i++) {
            mItems.add(String.valueOf(i));
            mDaysShown++;
        }

        mDaysNextMonth = 1;
        while (mDaysShown % 7 != 0) {
            mItems.add(String.valueOf(mDaysNextMonth));
            mDaysShown++;
            mDaysNextMonth++;
        }
        if (madapter == null) {
            madapter = new MonthAdapter(this, attendList, mItems, month, year, mDaysLastMonth, mDaysNextMonth,mDaysShown);
            recyclerView.setAdapter(madapter);
        }else{
            madapter.setDateList( mItems, month, year, mDaysLastMonth, mDaysNextMonth,mDaysShown);
            madapter.notifyDataSetChanged();
        }
    }

    private int daysInMonth(int month, GregorianCalendar gCal, int mYear) {
        int daysInMonth = mDaysInMonth[month];
        if (month == 1 && gCal.isLeapYear(mYear))
            daysInMonth++;
        return daysInMonth;
    }


    private int getDay(int day) {
        switch (day) {
            case Calendar.MONDAY:
                return 0;
            case Calendar.TUESDAY:
                return 1;
            case Calendar.WEDNESDAY:
                return 2;
            case Calendar.THURSDAY:
                return 3;
            case Calendar.FRIDAY:
                return 4;
            case Calendar.SATURDAY:
                return 5;
            case Calendar.SUNDAY:
                return 6;
            default:
                return 0;
        }
    }
}
