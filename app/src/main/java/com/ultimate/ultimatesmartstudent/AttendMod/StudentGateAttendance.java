package com.ultimate.ultimatesmartstudent.AttendMod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentGateAttendance extends AppCompatActivity {

    RecyclerView NewsRecyclerview;
    //NewsAdapter newsAdapter;
    //List<NewsItem> mData;
    SharedPreferences sharedPreferences;
    FloatingActionButton fabSwitcher;
    boolean isDark = false;
    RelativeLayout rootLayout;
    EditText searchInput;
    CharSequence search = "";
    ArrayList<Studentbean> mData = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    //    @BindView(R.id.contact_support)
//    ImageView contact_support;
    RecyclerView.LayoutManager layoutManager;
    private StudentgateAttendadapter newsAdapter;
    public String date = "";

    @BindView(R.id.cal_lyt)
    LinearLayout cal_lyt;
    @BindView(R.id.cal_text)
    TextView cal_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_gate_attendance);
        ButterKnife.bind(this);

        rootLayout = findViewById(R.id.root_layout);
        NewsRecyclerview = findViewById(R.id.news_rv);
        //  searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
        rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

        // fetchStudent();

        layoutManager = new LinearLayoutManager(this);
        NewsRecyclerview.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        newsAdapter = new StudentgateAttendadapter(mData, this);
        NewsRecyclerview.setAdapter(newsAdapter);
//        int i=NewsRecyclerview.getAdapter().getItemCount();
//        txtTitle.setText(i);


    }


    private void fetchgateStudent(String date) {
         ErpProgress.showProgressBar(this,"please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("date", date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.gatestudentlistclass, apiCallback, this, params);
    }

    @OnClick(R.id.cal_lyt)
    public void cal_lyttttt(){

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            //  SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchgateStudent(date);
        }
    };


    @OnClick(R.id.imgBackmsg)
    public void onback() {
        finish();
    }

//    public void fetchStudent() {
//        // ErpProgress.showProgressBar(this,"please wait...");
//        HashMap<String, String> params = new HashMap<String, String>();
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.gatestudentlistclass, apiCallback, this, params);
//
//    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                try {
                    if (mData != null) {
                        mData.clear();
                    }

                    if (mData != null) {
                        mData.clear();
                        mData = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                        Log.e("sizeeeeeeeeeeeeeeeeee", String.valueOf(mData.size()));
                        newsAdapter.setHQList(mData);
                        newsAdapter.notifyDataSetChanged();
                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- " + String.valueOf(mData.size()));
                        txtNorecord.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ErpProgress.cancelProgressBar();
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                mData.clear();
                newsAdapter.setHQList(mData);
                newsAdapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        //  SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM, yyyy");
        String dateString = fmtOut.format(setdate);
        cal_text.setText(dateString);
        //adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);

        fetchgateStudent(date);
        txtTitle.setText("Gate Attendance");

    }
}