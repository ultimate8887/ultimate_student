package com.ultimate.ultimatesmartstudent.AttendMod;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MonthAdapter extends RecyclerView.Adapter<MonthAdapter.ViewHolder> {
    private ArrayList<AttendMod> attendList;
    private GregorianCalendar mCalendar;
    private Context mContext;
    private List<String> mItems;
    private int mDaysShown = 0;
    int mMonth, mYear;
    private int mDaysLastMonth;
    private int mDaysNextMonth;


    public MonthAdapter(Context context, ArrayList<AttendMod> attendList, List<String> mItems, int mMonth, int mYear, int mDaysLastMonth, int mDaysNextMonth, int mDaysShown) {
        mContext = context;
        this.mDaysShown = mDaysShown;
        this.mItems = mItems;
        this.attendList = attendList;
        this.mMonth = mMonth;
        this.mYear = mYear;
        this.mDaysNextMonth = mDaysNextMonth;
        this.mDaysLastMonth = mDaysLastMonth;
    }


    public void setAttendList(ArrayList<AttendMod> attendList) {
        this.attendList = attendList;
    }

    public void setDateList(List<String> mItems, int mMonth, int mYear, int mDaysLastMonth, int mDaysNextMonth, int mDaysShown) {
        this.mItems = mItems;
        this.mMonth = mMonth;
        this.mYear = mYear;
        this.mDaysShown = mDaysShown;
        this.mDaysNextMonth = mDaysNextMonth;
        this.mDaysLastMonth = mDaysLastMonth;

        Log.e("mDaysLastMonth",mDaysLastMonth+","+mDaysNextMonth);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.day)
        TextView day;
        @BindView(R.id.lytBg)
        LinearLayout lytBg;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.item_calendar, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.day.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        holder.day.setText(mItems.get(position));
        holder.day.setTextColor(Color.BLACK);
        int[] date = getDate(position);
        if (date != null) {
            holder.day.setTextColor(Util.resolveDate(date[1], mMonth) ? Color.BLACK : Color.GRAY);
            if (Util.resolveDate(date[1], mMonth)) {
                for (int i = 0; i < attendList.size(); i++) {
                    int pos_value = Integer.parseInt(mItems.get(position));

                    int attend_value = Integer.parseInt(attendList.get(i).getDay());

                    if (pos_value == attend_value) {
                        if (attendList.get(i).getAttend().equalsIgnoreCase("p")) {
                            holder.day.setTextColor(mContext.getResources().getColor(R.color.white));
                            holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.present_bg));
                        } else if (attendList.get(i).getAttend().equalsIgnoreCase("a")) {
                            holder.day.setTextColor(mContext.getResources().getColor(R.color.white));
                            holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.absent_bg));
                        } else if (attendList.get(i).getAttend().equalsIgnoreCase("l")) {
                            if (attendList.get(i).getL_remarks()!= null){
                                if(attendList.get(i).getL_remarks().equalsIgnoreCase("SAL")||attendList.get(i).getL_remarks().equalsIgnoreCase("sal")) {
                                    holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.sal_leave_bg));
                                } else if(attendList.get(i).getL_remarks().equalsIgnoreCase("HL")|| attendList.get(i).getL_remarks().equalsIgnoreCase("hl")) {
                                    holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.leave_bg_hl));
                                }else{
                                    holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.leave_bg));
                                }
                            }else {
                                holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.leave_bg));
                            }
                            holder.day.setTextColor(mContext.getResources().getColor(R.color.white));
                        }
                        else if (attendList.get(i).getAttend().equalsIgnoreCase("holiday")) {
                            holder.day.setTextColor(mContext.getResources().getColor(R.color.white));
                            holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                        }else if (attendList.get(i).getAttend().equalsIgnoreCase("sunday")) {
                            holder.day.setTextColor(mContext.getResources().getColor(R.color.white));
                            holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.sunday_bg));
                        }
                        else {
                            holder.day.setTextColor(mContext.getResources().getColor(R.color.dark_black));
                            holder.lytBg.setBackground(mContext.getResources().getDrawable(R.drawable.white_bg));
                        }
                    }




                }
            }
        } else {
            holder.day.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.lytBg.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }


    private int[] getDate(int position) {
        int date[] = new int[3];
        if (position <= 6) {
            return null; // day names
        } else if (position <= mDaysLastMonth + 6) {
            // previous month
            date[0] = Integer.parseInt(mItems.get(position));
            if (mMonth == 0) {
                date[1] = 11;
                date[2] = mYear - 1;
            } else {
                date[1] = mMonth - 1;
                date[2] = mYear;
            }
        } else if (position <= mDaysShown - mDaysNextMonth) {
            // current month
            date[0] = position - (mDaysLastMonth + 6);
            date[1] = mMonth;
            date[2] = mYear;
        } else {
            // next month
            date[0] = Integer.parseInt(mItems.get(position));
            if (mMonth == 11) {
                date[1] = 0;
                date[2] = mYear + 1;
            } else {
                date[1] = mMonth + 1;
                date[2] = mYear;
            }
        }
        return date;
    }
}
