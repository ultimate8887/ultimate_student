package com.ultimate.ultimatesmartstudent.AttendMod;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Studentbean {


    /**
     * id : 1
     * name : aniket garg
     */
    private static String ID = "id";
    private static String NAME = "name";
    private static String F_NAME = "father_name";
    private static String CLASS_NAME = "class_name";
    private static String PHONE = "mobile";
    private static String PROFILE = "profile";
    private static String CLASS_ID = "class_id";
    private static String R_ID = "r_id";
    private static String SCHOOLDATA = "school_data";
    private static String LAT = "lat";
    private static String LNG = "lng";
    private static String CLASSNAME = "class_name";

    private static String EMAIL = "email";
    private static String PHONENO = "phoneno";


    private String id;
    private String name;
    /**
     * class_name : 2
     * father_name : Sandeep Kumar
     * mobile : 7889267816
     * profile : office_admin/images/st_02012018_080227_download.jpg
     */

    private String class_name;
    private String father_name;
    private String mobile;
    private String profile;
    private String course_name;
    private String section_name;

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    /**

     * bg : abc
     * gender : female
     * dob : 2013-02-08
     * aadhaar : 1234567899
     * address : 27 B Green View Colony Rajbaha Road , Patiala , Pincode: 147001
     * device_token :
     * class_id : 2
     * r_id : 0
     * lat : 30.342423
     * lng : 72.243232
     * roll_no : 1
     */

    private String bg;
    private String gender;
    private String dob;
    private String aadhaar;
    private String address;
    private String class_id;
    private String r_id;
    private double lat;
    private double lng;
    private String roll_no;

    public String getMobile_name() {
        return mobile_name;
    }

    public void setMobile_name(String mobile_name) {
        this.mobile_name = mobile_name;
    }

    private String mobile_name;

    /**
     * email : aman21081993@gmail.com
     * phoneno : 7889267816
     * device_token :
     * lat : 30.342423
     * lng : 72.243232
     */

    private String email;
    private String phoneno;

    public String getFee_cat_id() {
        return fee_cat_id;
    }

    public void setFee_cat_id(String fee_cat_id) {
        this.fee_cat_id = fee_cat_id;
    }

    private String fee_cat_id;

    /**
     * device_token : dDr3bszSK7k:APA91bF3gDKdOD7ID1WNPGlgxk-iyuD_cvuqdCT2DzKJnOTdUsWI0QGmm4t8seGxYg-yrfBuxr68nsvstLNZSVnI
     * lat : 30.342423
     * lng : 72.243232
     * croll_no : Miss
     */

    private String croll_no;

    public String getAt_attendance_date() {
        return at_attendance_date;
    }

    public void setAt_attendance_date(String at_attendance_date) {
        this.at_attendance_date = at_attendance_date;
    }

    private String at_attendance_date;

    /**
     * device_token : d58Ci0HjTWI:APA91bE_sEFTs6Q3O4rx7Y5jSsFDxcsS75cwgyRGvVhPcQCHKkgMDn8HZBSUwWw1QHYkgNUHoqlRfZaxYmreJyj5
     * lat : 0
     * lng : 0
     * collegeroll_no : Miss
     */
    private String username;
    private String password;
    private String  hostelstatus;
    private String admissiondate;
    private String  admissionquota;
    private String religion;
    private String  nationality;
    private String  universityregno;

    private String pr_address;
    private String pr_city;
    private String  pr_state;
    private String pr_pincode;
    private String  pr_email;
    private String pr_mobile;
    private String  pmt_address;
    private String  pmt_city;
    private String  pmt_state;
    private String pmt_pincode;
    private String  pmt_email;
    private String  pmt_mobile;
    private String neet_roll;
    private String  neet_mrkobt;
    private String  neet_totmrks;
    private String  neet_prcntile;
    private String neet_statrank;
    private String  neet_air;

    private String  mtric_session;
    private String  mtric_school;
    private String  mtric_board;
    private String  mtric_statscol;
    private String  mtric_stboard;
    private String  elev_session;
    private String  elev_school;
    private String  elev_board;
    private String  elev_statscol;
    private String  elev_stboard;
    private String  twlv_session;
    private String  twlv_school;
    private String  twlv_board;
    private String  twlv_statscol;
    private String  twlv_stboard;

    private String twlv_roll;
    private String maxphy;
    private String obtphy;
    private String maxchem;
    private String obtchem;
    private String maxbio;
    private String obtbio;
    private String maxeng;
    private String obteng;
    private String maxpcb;
    private String obtpcb;
    private String maxmarks;
    private String minmarks;

    private String bdssession;
    private String bdsschoolname;
    private String bdsboard_univ;
    private String bdsmarksobt;
    private String bdstotalmarks;
    private String bdsstatcolg;
    private String bdsstatuniv;


    private String bdsfstyrmrks;
    private String bdsfstyrtotmrks;
    private String bdssecyrmrks;
    private String bdssecyrtotmrks;
    private String bdsthrdyrmrks;
    private String bdsthrdyrtotmrks;
    private String  bdsfinlyrmrks;
    private String  bdsfinlyrtotmrks;

    private String   mother_name;
    private String   gndfathermobile;
    private String  gndmothermobile;

    private String  fatheroccu;
    private String  motheroccu;
    private String fatherage;
    private String motherage;
    private String fathereduquali;
    private String mothereduquali;

    private String castetype;

    private String es_morning_vid;
    private String es_evening_vid;
    private String village;

    private String place_id;

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    private String route_id;



    private String   aadhar_image;
    private String   tenmarksheet_image;
    private String  twelvemarksheet_image;

    private String  schol_certif_image;
    private String  schol_trans_certif_image;
    private String graduation_image;

    public String getAadhar_image() {
        return aadhar_image;
    }

    public void setAadhar_image(String aadhar_image) {
        this.aadhar_image = aadhar_image;
    }

    public String getTenmarksheet_image() {
        return tenmarksheet_image;
    }

    public void setTenmarksheet_image(String tenmarksheet_image) {
        this.tenmarksheet_image = tenmarksheet_image;
    }


    public String getMobile_brand() {
        return mobile_brand;
    }

    public void setMobile_brand(String mobile_brand) {
        this.mobile_brand = mobile_brand;
    }

    private String mobile_brand;

    public String getTwelvemarksheet_image() {
        return twelvemarksheet_image;
    }

    public void setTwelvemarksheet_image(String twelvemarksheet_image) {
        this.twelvemarksheet_image = twelvemarksheet_image;
    }

    public String getSchol_certif_image() {
        return schol_certif_image;
    }

    public void setSchol_certif_image(String schol_certif_image) {
        this.schol_certif_image = schol_certif_image;
    }

    public String getSchol_trans_certif_image() {
        return schol_trans_certif_image;
    }

    public void setSchol_trans_certif_image(String schol_trans_certif_image) {
        this.schol_trans_certif_image = schol_trans_certif_image;
    }

    public String getGraduation_image() {
        return graduation_image;
    }

    public void setGraduation_image(String graduation_image) {
        this.graduation_image = graduation_image;
    }

    public String getAadhar_pdf() {
        return aadhar_pdf;
    }

    public void setAadhar_pdf(String aadhar_pdf) {
        this.aadhar_pdf = aadhar_pdf;
    }

    public String getTenmarksheet_pdf() {
        return tenmarksheet_pdf;
    }

    public void setTenmarksheet_pdf(String tenmarksheet_pdf) {
        this.tenmarksheet_pdf = tenmarksheet_pdf;
    }

    public String getTwelvemarksheet_pdf() {
        return twelvemarksheet_pdf;
    }

    public void setTwelvemarksheet_pdf(String twelvemarksheet_pdf) {
        this.twelvemarksheet_pdf = twelvemarksheet_pdf;
    }

    public String getSchol_certif_pdf() {
        return schol_certif_pdf;
    }

    public void setSchol_certif_pdf(String schol_certif_pdf) {
        this.schol_certif_pdf = schol_certif_pdf;
    }

    public String getSchol_trans_certif_pdf() {
        return schol_trans_certif_pdf;
    }

    public void setSchol_trans_certif_pdf(String schol_trans_certif_pdf) {
        this.schol_trans_certif_pdf = schol_trans_certif_pdf;
    }

    public String getGraduation_pdf() {
        return graduation_pdf;
    }

    public void setGraduation_pdf(String graduation_pdf) {
        this.graduation_pdf = graduation_pdf;
    }

    private String aadhar_pdf;
    private String tenmarksheet_pdf;
    private String twelvemarksheet_pdf;

    private String schol_certif_pdf;

    private String schol_trans_certif_pdf;
    private String graduation_pdf;

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getEs_morning_vid() {
        return es_morning_vid;
    }

    public void setEs_morning_vid(String es_morning_vid) {
        this.es_morning_vid = es_morning_vid;
    }

    public String getEs_evening_vid() {
        return es_evening_vid;
    }

    public void setEs_evening_vid(String es_evening_vid) {
        this.es_evening_vid = es_evening_vid;
    }

    public String getCastetype() {
        return castetype;
    }

    public void setCastetype(String castetype) {
        this.castetype = castetype;
    }

    public String getFatheroccu() {
        return fatheroccu;
    }

    public void setFatheroccu(String fatheroccu) {
        this.fatheroccu = fatheroccu;
    }

    public String getMotheroccu() {
        return motheroccu;
    }

    public void setMotheroccu(String motheroccu) {
        this.motheroccu = motheroccu;
    }

    public String getFatherage() {
        return fatherage;
    }

    public void setFatherage(String fatherage) {
        this.fatherage = fatherage;
    }

    public String getMotherage() {
        return motherage;
    }

    public void setMotherage(String motherage) {
        this.motherage = motherage;
    }

    public String getFathereduquali() {
        return fathereduquali;
    }

    public void setFathereduquali(String fathereduquali) {
        this.fathereduquali = fathereduquali;
    }

    public String getMothereduquali() {
        return mothereduquali;
    }

    public void setMothereduquali(String mothereduquali) {
        this.mothereduquali = mothereduquali;
    }

    public String getGndfathermobile() {
        return gndfathermobile;
    }

    public void setGndfathermobile(String gndfathermobile) {
        this.gndfathermobile = gndfathermobile;
    }

    public String getGndmothermobile() {
        return gndmothermobile;
    }

    public void setGndmothermobile(String gndmothermobile) {
        this.gndmothermobile = gndmothermobile;
    }

    public String getMother_name() {
        return mother_name;
    }

    public void setMother_name(String mother_name) {
        this.mother_name = mother_name;
    }

    public String getBdsfinlyrmrks() {
        return bdsfinlyrmrks;
    }

    public void setBdsfinlyrmrks(String bdsfinlyrmrks) {
        this.bdsfinlyrmrks = bdsfinlyrmrks;
    }

    public String getBdsfinlyrtotmrks() {
        return bdsfinlyrtotmrks;
    }

    public void setBdsfinlyrtotmrks(String bdsfinlyrtotmrks) {
        this.bdsfinlyrtotmrks = bdsfinlyrtotmrks;
    }

    public String getBdsfstyrmrks() {
        return bdsfstyrmrks;
    }

    public void setBdsfstyrmrks(String bdsfstyrmrks) {
        this.bdsfstyrmrks = bdsfstyrmrks;
    }

    public String getBdsfstyrtotmrks() {
        return bdsfstyrtotmrks;
    }

    public void setBdsfstyrtotmrks(String bdsfstyrtotmrks) {
        this.bdsfstyrtotmrks = bdsfstyrtotmrks;
    }

    public String getBdssecyrmrks() {
        return bdssecyrmrks;
    }

    public void setBdssecyrmrks(String bdssecyrmrks) {
        this.bdssecyrmrks = bdssecyrmrks;
    }

    public String getBdssecyrtotmrks() {
        return bdssecyrtotmrks;
    }

    public void setBdssecyrtotmrks(String bdssecyrtotmrks) {
        this.bdssecyrtotmrks = bdssecyrtotmrks;
    }

    public String getBdsthrdyrmrks() {
        return bdsthrdyrmrks;
    }

    public void setBdsthrdyrmrks(String bdsthrdyrmrks) {
        this.bdsthrdyrmrks = bdsthrdyrmrks;
    }

    public String getBdsthrdyrtotmrks() {
        return bdsthrdyrtotmrks;
    }

    public void setBdsthrdyrtotmrks(String bdsthrdyrtotmrks) {
        this.bdsthrdyrtotmrks = bdsthrdyrtotmrks;
    }

    public String getBdssession() {
        return bdssession;
    }

    public void setBdssession(String bdssession) {
        this.bdssession = bdssession;
    }

    public String getBdsschoolname() {
        return bdsschoolname;
    }

    public void setBdsschoolname(String bdsschoolname) {
        this.bdsschoolname = bdsschoolname;
    }

    public String getBdsboard_univ() {
        return bdsboard_univ;
    }

    public void setBdsboard_univ(String bdsboard_univ) {
        this.bdsboard_univ = bdsboard_univ;
    }

    public String getBdsmarksobt() {
        return bdsmarksobt;
    }

    public void setBdsmarksobt(String bdsmarksobt) {
        this.bdsmarksobt = bdsmarksobt;
    }

    public String getBdstotalmarks() {
        return bdstotalmarks;
    }

    public void setBdstotalmarks(String bdstotalmarks) {
        this.bdstotalmarks = bdstotalmarks;
    }

    public String getBdsstatcolg() {
        return bdsstatcolg;
    }

    public void setBdsstatcolg(String bdsstatcolg) {
        this.bdsstatcolg = bdsstatcolg;
    }

    public String getBdsstatuniv() {
        return bdsstatuniv;
    }

    public void setBdsstatuniv(String bdsstatuniv) {
        this.bdsstatuniv = bdsstatuniv;
    }

    public String getTwlv_roll() {
        return twlv_roll;
    }

    public void setTwlv_roll(String twlv_roll) {
        this.twlv_roll = twlv_roll;
    }

    public String getMaxphy() {
        return maxphy;
    }

    public void setMaxphy(String maxphy) {
        this.maxphy = maxphy;
    }

    public String getObtphy() {
        return obtphy;
    }

    public void setObtphy(String obtphy) {
        this.obtphy = obtphy;
    }

    public String getMaxchem() {
        return maxchem;
    }

    public void setMaxchem(String maxchem) {
        this.maxchem = maxchem;
    }

    public String getObtchem() {
        return obtchem;
    }

    public void setObtchem(String obtchem) {
        this.obtchem = obtchem;
    }

    public String getMaxbio() {
        return maxbio;
    }

    public void setMaxbio(String maxbio) {
        this.maxbio = maxbio;
    }

    public String getObtbio() {
        return obtbio;
    }

    public void setObtbio(String obtbio) {
        this.obtbio = obtbio;
    }

    public String getMaxeng() {
        return maxeng;
    }

    public void setMaxeng(String maxeng) {
        this.maxeng = maxeng;
    }

    public String getObteng() {
        return obteng;
    }

    public void setObteng(String obteng) {
        this.obteng = obteng;
    }

    public String getMaxpcb() {
        return maxpcb;
    }

    public void setMaxpcb(String maxpcb) {
        this.maxpcb = maxpcb;
    }

    public String getObtpcb() {
        return obtpcb;
    }

    public void setObtpcb(String obtpcb) {
        this.obtpcb = obtpcb;
    }

    public String getMaxmarks() {
        return maxmarks;
    }

    public void setMaxmarks(String maxmarks) {
        this.maxmarks = maxmarks;
    }

    public String getMinmarks() {
        return minmarks;
    }

    public void setMinmarks(String minmarks) {
        this.minmarks = minmarks;
    }

    public String getMtric_session() {
        return mtric_session;
    }

    public void setMtric_session(String mtric_session) {
        this.mtric_session = mtric_session;
    }

    public String getMtric_school() {
        return mtric_school;
    }

    public void setMtric_school(String mtric_school) {
        this.mtric_school = mtric_school;
    }

    public String getMtric_board() {
        return mtric_board;
    }

    public void setMtric_board(String mtric_board) {
        this.mtric_board = mtric_board;
    }

    public String getMtric_statscol() {
        return mtric_statscol;
    }

    public void setMtric_statscol(String mtric_statscol) {
        this.mtric_statscol = mtric_statscol;
    }

    public String getMtric_stboard() {
        return mtric_stboard;
    }

    public void setMtric_stboard(String mtric_stboard) {
        this.mtric_stboard = mtric_stboard;
    }

    public String getElev_session() {
        return elev_session;
    }

    public void setElev_session(String elev_session) {
        this.elev_session = elev_session;
    }

    public String getElev_school() {
        return elev_school;
    }

    public void setElev_school(String elev_school) {
        this.elev_school = elev_school;
    }

    public String getElev_board() {
        return elev_board;
    }

    public void setElev_board(String elev_board) {
        this.elev_board = elev_board;
    }

    public String getElev_statscol() {
        return elev_statscol;
    }

    public void setElev_statscol(String elev_statscol) {
        this.elev_statscol = elev_statscol;
    }

    public String getElev_stboard() {
        return elev_stboard;
    }

    public void setElev_stboard(String elev_stboard) {
        this.elev_stboard = elev_stboard;
    }

    public String getTwlv_session() {
        return twlv_session;
    }

    public void setTwlv_session(String twlv_session) {
        this.twlv_session = twlv_session;
    }

    public String getTwlv_school() {
        return twlv_school;
    }

    public void setTwlv_school(String twlv_school) {
        this.twlv_school = twlv_school;
    }

    public String getTwlv_board() {
        return twlv_board;
    }

    public void setTwlv_board(String twlv_board) {
        this.twlv_board = twlv_board;
    }

    public String getTwlv_statscol() {
        return twlv_statscol;
    }

    public void setTwlv_statscol(String twlv_statscol) {
        this.twlv_statscol = twlv_statscol;
    }

    public String getTwlv_stboard() {
        return twlv_stboard;
    }

    public void setTwlv_stboard(String twlv_stboard) {
        this.twlv_stboard = twlv_stboard;
    }

    public String getPr_address() {
        return pr_address;
    }

    public void setPr_address(String pr_address) {
        this.pr_address = pr_address;
    }

    public String getPr_city() {
        return pr_city;
    }

    public void setPr_city(String pr_city) {
        this.pr_city = pr_city;
    }

    public String getPr_state() {
        return pr_state;
    }

    public void setPr_state(String pr_state) {
        this.pr_state = pr_state;
    }

    public String getPr_pincode() {
        return pr_pincode;
    }

    public void setPr_pincode(String pr_pincode) {
        this.pr_pincode = pr_pincode;
    }

    public String getPr_email() {
        return pr_email;
    }

    public void setPr_email(String pr_email) {
        this.pr_email = pr_email;
    }

    public String getPr_mobile() {
        return pr_mobile;
    }

    public void setPr_mobile(String pr_mobile) {
        this.pr_mobile = pr_mobile;
    }

    public String getPmt_address() {
        return pmt_address;
    }

    public void setPmt_address(String pmt_address) {
        this.pmt_address = pmt_address;
    }

    public String getPmt_city() {
        return pmt_city;
    }

    public void setPmt_city(String pmt_city) {
        this.pmt_city = pmt_city;
    }

    public String getPmt_state() {
        return pmt_state;
    }

    public void setPmt_state(String pmt_state) {
        this.pmt_state = pmt_state;
    }

    public String getPmt_pincode() {
        return pmt_pincode;
    }

    public void setPmt_pincode(String pmt_pincode) {
        this.pmt_pincode = pmt_pincode;
    }

    public String getPmt_email() {
        return pmt_email;
    }

    public void setPmt_email(String pmt_email) {
        this.pmt_email = pmt_email;
    }

    public String getPmt_mobile() {
        return pmt_mobile;
    }

    public void setPmt_mobile(String pmt_mobile) {
        this.pmt_mobile = pmt_mobile;
    }

    public String getNeet_roll() {
        return neet_roll;
    }

    public void setNeet_roll(String neet_roll) {
        this.neet_roll = neet_roll;
    }

    public String getNeet_mrkobt() {
        return neet_mrkobt;
    }

    public void setNeet_mrkobt(String neet_mrkobt) {
        this.neet_mrkobt = neet_mrkobt;
    }

    public String getNeet_totmrks() {
        return neet_totmrks;
    }

    public void setNeet_totmrks(String neet_totmrks) {
        this.neet_totmrks = neet_totmrks;
    }

    public String getNeet_prcntile() {
        return neet_prcntile;
    }

    public void setNeet_prcntile(String neet_prcntile) {
        this.neet_prcntile = neet_prcntile;
    }

    public String getNeet_statrank() {
        return neet_statrank;
    }

    public void setNeet_statrank(String neet_statrank) {
        this.neet_statrank = neet_statrank;
    }

    public String getNeet_air() {
        return neet_air;
    }

    public void setNeet_air(String neet_air) {
        this.neet_air = neet_air;
    }

    public String getUniversityregno() {
        return universityregno;
    }

    public void setUniversityregno(String universityregno) {
        this.universityregno = universityregno;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCounseling_date() {
        return counseling_date;
    }

    public void setCounseling_date(String counseling_date) {
        this.counseling_date = counseling_date;
    }

    private String counseling_date;
    private String room_no;

    public String getRoom_no() {
        return room_no;
    }

    public void setRoom_no(String room_no) {
        this.room_no = room_no;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getRoom_capacity() {
        return room_capacity;
    }

    public void setRoom_capacity(String room_capacity) {
        this.room_capacity = room_capacity;
    }

    private String  room_type;
    private String room_capacity;
    private String hostel_name;

    public String getHostel_name() {
        return hostel_name;
    }

    public void setHostel_name(String hostel_name) {
        this.hostel_name = hostel_name;
    }

    public String getHostelstatus() {
        return hostelstatus;
    }

    public void setHostelstatus(String hostelstatus) {
        this.hostelstatus = hostelstatus;
    }

    public String getAdmissiondate() {
        return admissiondate;
    }

    public void setAdmissiondate(String admissiondate) {
        this.admissiondate = admissiondate;
    }

    public String getSessionname() {
        return sessionname;
    }

    public void setSessionname(String sessionname) {
        this.sessionname = sessionname;
    }

    public String getMothersmobile() {
        return mothersmobile;
    }

    public void setMothersmobile(String mothersmobile) {
        this.mothersmobile = mothersmobile;
    }

    public String getFathermobile() {
        return fathermobile;
    }

    public void setFathermobile(String fathermobile) {
        this.fathermobile = fathermobile;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    private String sessionname;
    private String mothersmobile;
    private String  fathermobile;
    private String categoryname;


    public String getAdmissionquota() {
        return admissionquota;
    }

    public void setAdmissionquota(String admissionquota) {
        this.admissionquota = admissionquota;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<Studentbean> parseHWArray(JSONArray arrayObj) {
        ArrayList<Studentbean> list = new ArrayList<Studentbean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Studentbean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Studentbean parseHWObject(JSONObject jsonObject) {
        Studentbean user = new Studentbean();
        try {
            if (jsonObject.has(ID)) {
                user.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                user.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(F_NAME) && !jsonObject.getString(F_NAME).isEmpty() && !jsonObject.getString(F_NAME).equalsIgnoreCase("null")) {
                user.setFather_name(jsonObject.getString(F_NAME));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                user.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(PHONE) && !jsonObject.getString(PHONE).isEmpty() && !jsonObject.getString(PHONE).equalsIgnoreCase("null")) {
                user.setMobile(jsonObject.getString(PHONE));
            }
            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
                user.setProfile(Constants.getImageBaseURL()+jsonObject.getString(PROFILE));
            }
            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                user.setClass_id(jsonObject.getString(CLASS_ID));
            }
            if (jsonObject.has(R_ID) && !jsonObject.getString(R_ID).isEmpty() && !jsonObject.getString(R_ID).equalsIgnoreCase("null")) {
                user.setR_id(jsonObject.getString(R_ID));
            }
            if (jsonObject.has(LAT) && !jsonObject.getString(LAT).isEmpty() && !jsonObject.getString(LAT).equalsIgnoreCase("null")) {
                user.setLat(jsonObject.getDouble(LAT));
            }
            if (jsonObject.has(EMAIL) && !jsonObject.getString(EMAIL).isEmpty() && !jsonObject.getString(EMAIL).equalsIgnoreCase("null")) {
                user.setEmail(jsonObject.getString(EMAIL));
            }
            if (jsonObject.has(PHONENO) && !jsonObject.getString(PHONENO).isEmpty() && !jsonObject.getString(PHONENO).equalsIgnoreCase("null")) {
                user.setPhoneno(jsonObject.getString(PHONENO));
            }
            if (jsonObject.has(LNG) && !jsonObject.getString(LNG).isEmpty() && !jsonObject.getString(LNG).equalsIgnoreCase("null")) {
                user.setLng(jsonObject.getDouble(LNG));
            }
            if (jsonObject.has(CLASSNAME) && !jsonObject.getString(CLASSNAME).isEmpty() && !jsonObject.getString(CLASSNAME).equalsIgnoreCase("null")) {
                user.setClass_name(jsonObject.getString(CLASSNAME));
            }


            if (jsonObject.has("bg") && !jsonObject.getString("bg").isEmpty() && !jsonObject.getString("bg").equalsIgnoreCase("null")) {
                user.setBg(jsonObject.getString("bg"));
            }
            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                user.setGender(jsonObject.getString("gender"));
            }
            if (jsonObject.has("dob") && !jsonObject.getString("dob").isEmpty() && !jsonObject.getString("dob").equalsIgnoreCase("null")) {
                user.setDob(jsonObject.getString("dob"));
            }
            if (jsonObject.has("aadhaar") && !jsonObject.getString("aadhaar").isEmpty() && !jsonObject.getString("aadhaar").equalsIgnoreCase("null")) {
                user.setAadhaar(jsonObject.getString("aadhaar"));
            }
            if (jsonObject.has("address") && !jsonObject.getString("address").isEmpty() && !jsonObject.getString("address").equalsIgnoreCase("null")) {
                user.setAddress(jsonObject.getString("address"));
            }
            if (jsonObject.has("roll_no") && !jsonObject.getString("roll_no").isEmpty() && !jsonObject.getString("roll_no").equalsIgnoreCase("null")) {
                user.setRoll_no(jsonObject.getString("roll_no"));
            }
            if (jsonObject.has("croll_no") && !jsonObject.getString("croll_no").isEmpty() && !jsonObject.getString("croll_no").equalsIgnoreCase("null")) {
                user.setCroll_no(jsonObject.getString("croll_no"));
            }

            if (jsonObject.has("username") && !jsonObject.getString("username").isEmpty() && !jsonObject.getString("username").equalsIgnoreCase("null")) {
                user.setUsername(jsonObject.getString("username"));
            }
            if (jsonObject.has("password") && !jsonObject.getString("password").isEmpty() && !jsonObject.getString("password").equalsIgnoreCase("null")) {
                user.setPassword(jsonObject.getString("password"));
            }

            if (jsonObject.has("fee_cat_id") && !jsonObject.getString("fee_cat_id").isEmpty() && !jsonObject.getString("fee_cat_id").equalsIgnoreCase("null")) {
                user.setFee_cat_id(jsonObject.getString("fee_cat_id"));
            }

            if (jsonObject.has("hostelstatus") && !jsonObject.getString("hostelstatus").isEmpty() && !jsonObject.getString("hostelstatus").equalsIgnoreCase("null")) {
                user.setHostelstatus(jsonObject.getString("hostelstatus"));
            }
            if (jsonObject.has("admissiondate") && !jsonObject.getString("admissiondate").isEmpty() && !jsonObject.getString("admissiondate").equalsIgnoreCase("null")) {
                user.setAdmissiondate(jsonObject.getString("admissiondate"));
            }

            if (jsonObject.has("sessionname") && !jsonObject.getString("sessionname").isEmpty() && !jsonObject.getString("sessionname").equalsIgnoreCase("null")) {
                user.setSessionname(jsonObject.getString("sessionname"));
            }
            if (jsonObject.has("mothersmobile") && !jsonObject.getString("mothersmobile").isEmpty() && !jsonObject.getString("mothersmobile").equalsIgnoreCase("null")) {
                user.setMothersmobile(jsonObject.getString("mothersmobile"));
            }

            if (jsonObject.has("fathermobile") && !jsonObject.getString("fathermobile").isEmpty() && !jsonObject.getString("fathermobile").equalsIgnoreCase("null")) {
                user.setFathermobile(jsonObject.getString("fathermobile"));
            }

            if (jsonObject.has("categoryname") && !jsonObject.getString("categoryname").isEmpty() && !jsonObject.getString("categoryname").equalsIgnoreCase("null")) {
                user.setCategoryname(jsonObject.getString("categoryname"));
            }


            if (jsonObject.has("room_no") && !jsonObject.getString("room_no").isEmpty() && !jsonObject.getString("room_no").equalsIgnoreCase("null")) {
                user.setRoll_no(jsonObject.getString("room_no"));
            }

            if (jsonObject.has("room_type") && !jsonObject.getString("room_type").isEmpty() && !jsonObject.getString("room_type").equalsIgnoreCase("null")) {
                user.setRoom_type(jsonObject.getString("room_type"));
            }

            if (jsonObject.has("room_capacity") && !jsonObject.getString("room_capacity").isEmpty() && !jsonObject.getString("room_capacity").equalsIgnoreCase("null")) {
                user.setRoom_capacity(jsonObject.getString("room_capacity"));
            }

            if (jsonObject.has("hostel_name") && !jsonObject.getString("hostel_name").isEmpty() && !jsonObject.getString("hostel_name").equalsIgnoreCase("null")) {
                user.setHostel_name(jsonObject.getString("hostel_name"));
            }

            if (jsonObject.has("course_name") && !jsonObject.getString("course_name").isEmpty() && !jsonObject.getString("course_name").equalsIgnoreCase("null")) {
                user.setCourse_name(jsonObject.getString("course_name"));
            }
            if (jsonObject.has("counseling_date") && !jsonObject.getString("counseling_date").isEmpty() && !jsonObject.getString("counseling_date").equalsIgnoreCase("null")) {
                user.setCounseling_date(jsonObject.getString("counseling_date"));
            }

            if (jsonObject.has("admissionquota") && !jsonObject.getString("admissionquota").isEmpty() && !jsonObject.getString("admissionquota").equalsIgnoreCase("null")) {
                user.setAdmissionquota(jsonObject.getString("admissionquota"));
            }
            if (jsonObject.has("religion") && !jsonObject.getString("religion").isEmpty() && !jsonObject.getString("religion").equalsIgnoreCase("null")) {
                user.setReligion(jsonObject.getString("religion"));
            }
            if (jsonObject.has("nationality") && !jsonObject.getString("nationality").isEmpty() && !jsonObject.getString("nationality").equalsIgnoreCase("null")) {
                user.setNationality(jsonObject.getString("nationality"));
            }
            if (jsonObject.has("universityregno") && !jsonObject.getString("universityregno").isEmpty() && !jsonObject.getString("universityregno").equalsIgnoreCase("null")) {
                user.setUniversityregno(jsonObject.getString("universityregno"));
            }




            if (jsonObject.has("pr_address") && !jsonObject.getString("pr_address").isEmpty() && !jsonObject.getString("pr_address").equalsIgnoreCase("null")) {
                user.setPr_address(jsonObject.getString("pr_address"));
            }

            if (jsonObject.has("pr_city") && !jsonObject.getString("pr_city").isEmpty() && !jsonObject.getString("pr_city").equalsIgnoreCase("null")) {
                user.setPr_city(jsonObject.getString("pr_city"));
            }

            if (jsonObject.has("pr_state") && !jsonObject.getString("pr_state").isEmpty() && !jsonObject.getString("pr_state").equalsIgnoreCase("null")) {
                user.setPr_state(jsonObject.getString("pr_state"));
            }

            if (jsonObject.has("pr_pincode") && !jsonObject.getString("pr_pincode").isEmpty() && !jsonObject.getString("pr_pincode").equalsIgnoreCase("null")) {
                user.setPr_pincode(jsonObject.getString("pr_pincode"));
            }

            if (jsonObject.has("pr_email") && !jsonObject.getString("pr_email").isEmpty() && !jsonObject.getString("pr_email").equalsIgnoreCase("null")) {
                user.setPr_email(jsonObject.getString("pr_email"));
            }
            if (jsonObject.has("pr_mobile") && !jsonObject.getString("pr_mobile").isEmpty() && !jsonObject.getString("pr_mobile").equalsIgnoreCase("null")) {
                user.setPr_mobile(jsonObject.getString("pr_mobile"));
            }

            if (jsonObject.has("pmt_address") && !jsonObject.getString("pmt_address").isEmpty() && !jsonObject.getString("pmt_address").equalsIgnoreCase("null")) {
                user.setPmt_address(jsonObject.getString("pmt_address"));
            }
            if (jsonObject.has("pmt_city") && !jsonObject.getString("pmt_city").isEmpty() && !jsonObject.getString("pmt_city").equalsIgnoreCase("null")) {
                user.setPmt_city(jsonObject.getString("pmt_city"));
            }
            if (jsonObject.has("pmt_state") && !jsonObject.getString("pmt_state").isEmpty() && !jsonObject.getString("pmt_state").equalsIgnoreCase("null")) {
                user.setPmt_state(jsonObject.getString("pmt_state"));
            }
            if (jsonObject.has("pmt_pincode") && !jsonObject.getString("pmt_pincode").isEmpty() && !jsonObject.getString("pmt_pincode").equalsIgnoreCase("null")) {
                user.setPmt_pincode(jsonObject.getString("pmt_pincode"));
            }
            if (jsonObject.has("pmt_email") && !jsonObject.getString("pmt_email").isEmpty() && !jsonObject.getString("pmt_email").equalsIgnoreCase("null")) {
                user.setPmt_email(jsonObject.getString("pmt_email"));
            }
            if (jsonObject.has("pmt_mobile") && !jsonObject.getString("pmt_mobile").isEmpty() && !jsonObject.getString("pmt_mobile").equalsIgnoreCase("null")) {
                user.setPmt_mobile(jsonObject.getString("pmt_mobile"));
            }



            if (jsonObject.has("neet_roll") && !jsonObject.getString("neet_roll").isEmpty() && !jsonObject.getString("neet_roll").equalsIgnoreCase("null")) {
                user.setNeet_roll(jsonObject.getString("neet_roll"));
            }
            if (jsonObject.has("neet_mrkobt") && !jsonObject.getString("neet_mrkobt").isEmpty() && !jsonObject.getString("neet_mrkobt").equalsIgnoreCase("null")) {
                user.setNeet_mrkobt(jsonObject.getString("neet_mrkobt"));
            }
            if (jsonObject.has("neet_totmrks") && !jsonObject.getString("neet_totmrks").isEmpty() && !jsonObject.getString("neet_totmrks").equalsIgnoreCase("null")) {
                user.setNeet_totmrks(jsonObject.getString("neet_totmrks"));
            }
            if (jsonObject.has("neet_prcntile") && !jsonObject.getString("neet_prcntile").isEmpty() && !jsonObject.getString("neet_prcntile").equalsIgnoreCase("null")) {
                user.setNeet_prcntile(jsonObject.getString("neet_prcntile"));
            }
            if (jsonObject.has("neet_statrank") && !jsonObject.getString("neet_statrank").isEmpty() && !jsonObject.getString("neet_statrank").equalsIgnoreCase("null")) {
                user.setNeet_statrank(jsonObject.getString("neet_statrank"));
            }
            if (jsonObject.has("neet_air") && !jsonObject.getString("neet_air").isEmpty() && !jsonObject.getString("neet_air").equalsIgnoreCase("null")) {
                user.setNeet_air(jsonObject.getString("neet_air"));
            }



            if (jsonObject.has("mtric_session") && !jsonObject.getString("mtric_session").isEmpty() && !jsonObject.getString("mtric_session").equalsIgnoreCase("null")) {
                user.setMtric_session(jsonObject.getString("mtric_session"));
            }
            if (jsonObject.has("mtric_school") && !jsonObject.getString("mtric_school").isEmpty() && !jsonObject.getString("mtric_school").equalsIgnoreCase("null")) {
                user.setMtric_school(jsonObject.getString("mtric_school"));
            }
            if (jsonObject.has("mtric_board") && !jsonObject.getString("mtric_board").isEmpty() && !jsonObject.getString("mtric_board").equalsIgnoreCase("null")) {
                user.setMtric_board(jsonObject.getString("mtric_board"));
            }
            if (jsonObject.has("mtric_statscol") && !jsonObject.getString("mtric_statscol").isEmpty() && !jsonObject.getString("mtric_statscol").equalsIgnoreCase("null")) {
                user.setMtric_statscol(jsonObject.getString("mtric_statscol"));
            }
            if (jsonObject.has("mtric_stboard") && !jsonObject.getString("mtric_stboard").isEmpty() && !jsonObject.getString("mtric_stboard").equalsIgnoreCase("null")) {
                user.setMtric_stboard(jsonObject.getString("mtric_stboard"));
            }


            if (jsonObject.has("elev_session") && !jsonObject.getString("elev_session").isEmpty() && !jsonObject.getString("elev_session").equalsIgnoreCase("null")) {
                user.setElev_session(jsonObject.getString("elev_session"));
            }
            if (jsonObject.has("elev_school") && !jsonObject.getString("elev_school").isEmpty() && !jsonObject.getString("elev_school").equalsIgnoreCase("null")) {
                user.setElev_school(jsonObject.getString("elev_school"));
            }
            if (jsonObject.has("elev_board") && !jsonObject.getString("elev_board").isEmpty() && !jsonObject.getString("elev_board").equalsIgnoreCase("null")) {
                user.setElev_board(jsonObject.getString("elev_board"));
            }
            if (jsonObject.has("elev_statscol") && !jsonObject.getString("elev_statscol").isEmpty() && !jsonObject.getString("elev_statscol").equalsIgnoreCase("null")) {
                user.setElev_statscol(jsonObject.getString("elev_statscol"));
            }
            if (jsonObject.has("elev_stboard") && !jsonObject.getString("elev_stboard").isEmpty() && !jsonObject.getString("elev_stboard").equalsIgnoreCase("null")) {
                user.setElev_stboard(jsonObject.getString("elev_stboard"));
            }


            if (jsonObject.has("twlv_session") && !jsonObject.getString("twlv_session").isEmpty() && !jsonObject.getString("twlv_session").equalsIgnoreCase("null")) {
                user.setTwlv_session(jsonObject.getString("twlv_session"));
            }
            if (jsonObject.has("twlv_school") && !jsonObject.getString("twlv_school").isEmpty() && !jsonObject.getString("twlv_school").equalsIgnoreCase("null")) {
                user.setTwlv_school(jsonObject.getString("twlv_school"));
            }
            if (jsonObject.has("twlv_board") && !jsonObject.getString("twlv_board").isEmpty() && !jsonObject.getString("twlv_board").equalsIgnoreCase("null")) {
                user.setTwlv_board(jsonObject.getString("twlv_board"));
            }
            if (jsonObject.has("twlv_statscol") && !jsonObject.getString("twlv_statscol").isEmpty() && !jsonObject.getString("twlv_statscol").equalsIgnoreCase("null")) {
                user.setTwlv_statscol(jsonObject.getString("twlv_statscol"));
            }
            if (jsonObject.has("twlv_stboard") && !jsonObject.getString("twlv_stboard").isEmpty() && !jsonObject.getString("twlv_stboard").equalsIgnoreCase("null")) {
                user.setTwlv_stboard(jsonObject.getString("twlv_stboard"));
            }



            if (jsonObject.has("twlv_roll") && !jsonObject.getString("twlv_roll").isEmpty() && !jsonObject.getString("twlv_roll").equalsIgnoreCase("null")) {
                user.setTwlv_roll(jsonObject.getString("twlv_roll"));
            }
            if (jsonObject.has("maxphy") && !jsonObject.getString("maxphy").isEmpty() && !jsonObject.getString("maxphy").equalsIgnoreCase("null")) {
                user.setMaxphy(jsonObject.getString("maxphy"));
            }
            if (jsonObject.has("obtphy") && !jsonObject.getString("obtphy").isEmpty() && !jsonObject.getString("obtphy").equalsIgnoreCase("null")) {
                user.setObtphy(jsonObject.getString("obtphy"));
            }
            if (jsonObject.has("maxchem") && !jsonObject.getString("maxchem").isEmpty() && !jsonObject.getString("maxchem").equalsIgnoreCase("null")) {
                user.setMaxchem(jsonObject.getString("maxchem"));
            }
            if (jsonObject.has("obtchem") && !jsonObject.getString("obtchem").isEmpty() && !jsonObject.getString("obtchem").equalsIgnoreCase("null")) {
                user.setObtchem(jsonObject.getString("obtchem"));
            }


            if (jsonObject.has("maxbio") && !jsonObject.getString("maxbio").isEmpty() && !jsonObject.getString("maxbio").equalsIgnoreCase("null")) {
                user.setMaxbio(jsonObject.getString("maxbio"));
            }
            if (jsonObject.has("obtbio") && !jsonObject.getString("obtbio").isEmpty() && !jsonObject.getString("obtbio").equalsIgnoreCase("null")) {
                user.setObtbio(jsonObject.getString("obtbio"));
            }
            if (jsonObject.has("maxeng") && !jsonObject.getString("maxeng").isEmpty() && !jsonObject.getString("maxeng").equalsIgnoreCase("null")) {
                user.setMaxeng(jsonObject.getString("maxeng"));
            }
            if (jsonObject.has("obteng") && !jsonObject.getString("obteng").isEmpty() && !jsonObject.getString("obteng").equalsIgnoreCase("null")) {
                user.setObteng(jsonObject.getString("obteng"));
            }


            if (jsonObject.has("maxpcb") && !jsonObject.getString("maxpcb").isEmpty() && !jsonObject.getString("maxpcb").equalsIgnoreCase("null")) {
                user.setMaxpcb(jsonObject.getString("maxpcb"));
            }
            if (jsonObject.has("obtpcb") && !jsonObject.getString("obtpcb").isEmpty() && !jsonObject.getString("obtpcb").equalsIgnoreCase("null")) {
                user.setObtpcb(jsonObject.getString("obtpcb"));
            }
            if (jsonObject.has("maxmarks") && !jsonObject.getString("maxmarks").isEmpty() && !jsonObject.getString("maxmarks").equalsIgnoreCase("null")) {
                user.setMaxmarks(jsonObject.getString("maxmarks"));
            }
            if (jsonObject.has("minmarks") && !jsonObject.getString("minmarks").isEmpty() && !jsonObject.getString("minmarks").equalsIgnoreCase("null")) {
                user.setMinmarks(jsonObject.getString("minmarks"));
            }


            if (jsonObject.has("bdssession") && !jsonObject.getString("bdssession").isEmpty() && !jsonObject.getString("bdssession").equalsIgnoreCase("null")) {
                user.setBdssession(jsonObject.getString("bdssession"));
            }
            if (jsonObject.has("bdsschoolname") && !jsonObject.getString("bdsschoolname").isEmpty() && !jsonObject.getString("bdsschoolname").equalsIgnoreCase("null")) {
                user.setBdsschoolname(jsonObject.getString("bdsschoolname"));
            }
            if (jsonObject.has("bdsboard_univ") && !jsonObject.getString("bdsboard_univ").isEmpty() && !jsonObject.getString("bdsboard_univ").equalsIgnoreCase("null")) {
                user.setBdsboard_univ(jsonObject.getString("bdsboard_univ"));
            }
            if (jsonObject.has("bdsmarksobt") && !jsonObject.getString("bdsmarksobt").isEmpty() && !jsonObject.getString("bdsmarksobt").equalsIgnoreCase("null")) {
                user.setBdsmarksobt(jsonObject.getString("bdsmarksobt"));
            }
            if (jsonObject.has("bdstotalmarks") && !jsonObject.getString("bdstotalmarks").isEmpty() && !jsonObject.getString("bdstotalmarks").equalsIgnoreCase("null")) {
                user.setBdstotalmarks(jsonObject.getString("bdstotalmarks"));
            }
            if (jsonObject.has("bdsstatcolg") && !jsonObject.getString("bdsstatcolg").isEmpty() && !jsonObject.getString("bdsstatcolg").equalsIgnoreCase("null")) {
                user.setBdsstatcolg(jsonObject.getString("bdsstatcolg"));
            }
            if (jsonObject.has("bdsstatuniv") && !jsonObject.getString("bdsstatuniv").isEmpty() && !jsonObject.getString("bdsstatuniv").equalsIgnoreCase("null")) {
                user.setBdsstatuniv(jsonObject.getString("bdsstatuniv"));
            }



            if (jsonObject.has("bdsfstyrmrks") && !jsonObject.getString("bdsfstyrmrks").isEmpty() && !jsonObject.getString("bdsfstyrmrks").equalsIgnoreCase("null")) {
                user.setBdsfstyrmrks(jsonObject.getString("bdsfstyrmrks"));
            }
            if (jsonObject.has("bdsfstyrtotmrks") && !jsonObject.getString("bdsfstyrtotmrks").isEmpty() && !jsonObject.getString("bdsfstyrtotmrks").equalsIgnoreCase("null")) {
                user.setBdsfstyrtotmrks(jsonObject.getString("bdsfstyrtotmrks"));
            }
            if (jsonObject.has("bdssecyrmrks") && !jsonObject.getString("bdssecyrmrks").isEmpty() && !jsonObject.getString("bdssecyrmrks").equalsIgnoreCase("null")) {
                user.setBdssecyrmrks(jsonObject.getString("bdssecyrmrks"));
            }
            if (jsonObject.has("bdssecyrtotmrks") && !jsonObject.getString("bdssecyrtotmrks").isEmpty() && !jsonObject.getString("bdssecyrtotmrks").equalsIgnoreCase("null")) {
                user.setBdssecyrtotmrks(jsonObject.getString("bdssecyrtotmrks"));
            }
            if (jsonObject.has("bdsthrdyrmrks") && !jsonObject.getString("bdsthrdyrmrks").isEmpty() && !jsonObject.getString("bdsthrdyrmrks").equalsIgnoreCase("null")) {
                user.setBdsthrdyrmrks(jsonObject.getString("bdsthrdyrmrks"));
            }
            if (jsonObject.has("bdsthrdyrtotmrks") && !jsonObject.getString("bdsthrdyrtotmrks").isEmpty() && !jsonObject.getString("bdsthrdyrtotmrks").equalsIgnoreCase("null")) {
                user.setBdsthrdyrtotmrks(jsonObject.getString("bdsthrdyrtotmrks"));
            }
            if (jsonObject.has("bdsfinlyrmrks") && !jsonObject.getString("bdsfinlyrmrks").isEmpty() && !jsonObject.getString("bdsfinlyrmrks").equalsIgnoreCase("null")) {
                user.setBdsfinlyrmrks(jsonObject.getString("bdsfinlyrmrks"));
            }
            if (jsonObject.has("bdsfinlyrtotmrks") && !jsonObject.getString("bdsfinlyrtotmrks").isEmpty() && !jsonObject.getString("bdsfinlyrtotmrks").equalsIgnoreCase("null")) {
                user.setBdsfinlyrtotmrks(jsonObject.getString("bdsfinlyrtotmrks"));
            }
            if (jsonObject.has("mother_name") && !jsonObject.getString("mother_name").isEmpty() && !jsonObject.getString("mother_name").equalsIgnoreCase("null")) {
                user.setMother_name(jsonObject.getString("mother_name"));
            }

            if (jsonObject.has("gndfathermobile") && !jsonObject.getString("gndfathermobile").isEmpty() && !jsonObject.getString("gndfathermobile").equalsIgnoreCase("null")) {
                user.setGndfathermobile(jsonObject.getString("gndfathermobile"));
            }
            if (jsonObject.has("gndmothermobile") && !jsonObject.getString("gndmothermobile").isEmpty() && !jsonObject.getString("gndmothermobile").equalsIgnoreCase("null")) {
                user.setGndmothermobile(jsonObject.getString("gndmothermobile"));
            }

            if (jsonObject.has("fatheroccu") && !jsonObject.getString("fatheroccu").isEmpty() && !jsonObject.getString("fatheroccu").equalsIgnoreCase("null")) {
                user.setFatheroccu(jsonObject.getString("fatheroccu"));
            }
            if (jsonObject.has("motheroccu") && !jsonObject.getString("motheroccu").isEmpty() && !jsonObject.getString("motheroccu").equalsIgnoreCase("null")) {
                user.setMotheroccu(jsonObject.getString("motheroccu"));
            }
            if (jsonObject.has("fatherage") && !jsonObject.getString("fatherage").isEmpty() && !jsonObject.getString("fatherage").equalsIgnoreCase("null")) {
                user.setFatherage(jsonObject.getString("fatherage"));
            }
            if (jsonObject.has("motherage") && !jsonObject.getString("motherage").isEmpty() && !jsonObject.getString("motherage").equalsIgnoreCase("null")) {
                user.setMotherage(jsonObject.getString("motherage"));
            }
            if (jsonObject.has("fathereduquali") && !jsonObject.getString("fathereduquali").isEmpty() && !jsonObject.getString("fathereduquali").equalsIgnoreCase("null")) {
                user.setFathereduquali(jsonObject.getString("fathereduquali"));
            }
            if (jsonObject.has("mothereduquali") && !jsonObject.getString("mothereduquali").isEmpty() && !jsonObject.getString("mothereduquali").equalsIgnoreCase("null")) {
                user.setMothereduquali(jsonObject.getString("mothereduquali"));
            }
            if (jsonObject.has("castetype") && !jsonObject.getString("castetype").isEmpty() && !jsonObject.getString("castetype").equalsIgnoreCase("null")) {
                user.setCastetype(jsonObject.getString("castetype"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                user.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("es_morning_vid") && !jsonObject.getString("es_morning_vid").isEmpty() && !jsonObject.getString("es_morning_vid").equalsIgnoreCase("null")) {
                user.setEs_morning_vid(jsonObject.getString("es_morning_vid"));
            }


            if (jsonObject.has("place_id") && !jsonObject.getString("place_id").isEmpty() && !jsonObject.getString("place_id").equalsIgnoreCase("null")) {
                user.setPlace_id(jsonObject.getString("place_id"));
            }
            if (jsonObject.has("route_id") && !jsonObject.getString("route_id").isEmpty() && !jsonObject.getString("route_id").equalsIgnoreCase("null")) {
                user.setRoute_id(jsonObject.getString("route_id"));
            }


            if (jsonObject.has("mobile_name") && !jsonObject.getString("mobile_name").isEmpty() && !jsonObject.getString("mobile_name").equalsIgnoreCase("null")) {
                user.setMobile_name(jsonObject.getString("mobile_name"));
            }
            if (jsonObject.has("mobile_brand") && !jsonObject.getString("mobile_brand").isEmpty() && !jsonObject.getString("mobile_brand").equalsIgnoreCase("null")) {
                user.setMobile_brand(jsonObject.getString("mobile_brand"));
            }


            if (jsonObject.has("es_evening_vid") && !jsonObject.getString("es_evening_vid").isEmpty() && !jsonObject.getString("es_evening_vid").equalsIgnoreCase("null")) {
                user.setEs_evening_vid(jsonObject.getString("es_evening_vid"));
            }
            if (jsonObject.has("village") && !jsonObject.getString("village").isEmpty() && !jsonObject.getString("village").equalsIgnoreCase("null")) {
                user.setVillage(jsonObject.getString("village"));
            }

            if (jsonObject.has("aadhar_image") && !jsonObject.getString("aadhar_image").isEmpty() && !jsonObject.getString("aadhar_image").equalsIgnoreCase("null")) {
                user.setAadhar_image(Constants.getImageBaseURL()+jsonObject.getString("aadhar_image"));
            }

            if (jsonObject.has("tenmarksheet_image") && !jsonObject.getString("tenmarksheet_image").isEmpty() && !jsonObject.getString("tenmarksheet_image").equalsIgnoreCase("null")) {
                user.setTenmarksheet_image(Constants.getImageBaseURL()+jsonObject.getString("tenmarksheet_image"));
            }

            if (jsonObject.has("twelvemarksheet_image") && !jsonObject.getString("twelvemarksheet_image").isEmpty() && !jsonObject.getString("twelvemarksheet_image").equalsIgnoreCase("null")) {
                user.setTwelvemarksheet_image(Constants.getImageBaseURL()+jsonObject.getString("twelvemarksheet_image"));
            }

            if (jsonObject.has("schol_certif_image") && !jsonObject.getString("schol_certif_image").isEmpty() && !jsonObject.getString("schol_certif_image").equalsIgnoreCase("null")) {
                user.setSchol_certif_image(Constants.getImageBaseURL()+jsonObject.getString("schol_certif_image"));
            }

            if (jsonObject.has("schol_trans_certif_image") && !jsonObject.getString("schol_trans_certif_image").isEmpty() && !jsonObject.getString("schol_trans_certif_image").equalsIgnoreCase("null")) {
                user.setSchol_trans_certif_image(Constants.getImageBaseURL()+jsonObject.getString("schol_trans_certif_image"));
            }


            if (jsonObject.has("graduation_image") && !jsonObject.getString("graduation_image").isEmpty() && !jsonObject.getString("graduation_image").equalsIgnoreCase("null")) {
                user.setGraduation_image(Constants.getImageBaseURL()+jsonObject.getString("graduation_image"));
            }

            if (jsonObject.has("aadhar_pdf") && !jsonObject.getString("aadhar_pdf").isEmpty() && !jsonObject.getString("aadhar_pdf").equalsIgnoreCase("null")) {
                user.setAadhar_pdf(Constants.getImageBaseURL()+jsonObject.getString("aadhar_pdf"));
            }

            if (jsonObject.has("tenmarksheet_pdf") && !jsonObject.getString("tenmarksheet_pdf").isEmpty() && !jsonObject.getString("tenmarksheet_pdf").equalsIgnoreCase("null")) {
                user.setTenmarksheet_pdf(Constants.getImageBaseURL()+jsonObject.getString("tenmarksheet_pdf"));
            }

            if (jsonObject.has("twelvemarksheet_pdf") && !jsonObject.getString("twelvemarksheet_pdf").isEmpty() && !jsonObject.getString("twelvemarksheet_pdf").equalsIgnoreCase("null")) {
                user.setTwelvemarksheet_pdf(Constants.getImageBaseURL()+jsonObject.getString("twelvemarksheet_pdf"));
            }

            if (jsonObject.has("schol_certif_pdf") && !jsonObject.getString("schol_certif_pdf").isEmpty() && !jsonObject.getString("schol_certif_pdf").equalsIgnoreCase("null")) {
                user.setSchol_certif_pdf(Constants.getImageBaseURL()+jsonObject.getString("schol_certif_pdf"));
            }

            if (jsonObject.has("schol_trans_certif_pdf") && !jsonObject.getString("schol_trans_certif_pdf").isEmpty() && !jsonObject.getString("schol_trans_certif_pdf").equalsIgnoreCase("null")) {
                user.setSchol_trans_certif_pdf(Constants.getImageBaseURL()+jsonObject.getString("schol_trans_certif_pdf"));
            }

            if (jsonObject.has("at_attendance_date") && !jsonObject.getString("at_attendance_date").isEmpty() && !jsonObject.getString("at_attendance_date").equalsIgnoreCase("null")) {
                user.setAt_attendance_date(jsonObject.getString("at_attendance_date"));
            }

            if (jsonObject.has("graduation_pdf") && !jsonObject.getString("graduation_pdf").isEmpty() && !jsonObject.getString("graduation_pdf").equalsIgnoreCase("null")) {
                user.setGraduation_pdf(Constants.getImageBaseURL()+jsonObject.getString("graduation_pdf"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getR_id() {
        return r_id;
    }

    public void setR_id(String r_id) {
        this.r_id = r_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }


    public String getCroll_no() {
        return croll_no;
    }

    public void setCroll_no(String croll_no) {
        this.croll_no = croll_no;
    }
}



