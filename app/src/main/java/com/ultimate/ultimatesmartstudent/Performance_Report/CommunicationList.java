package com.ultimate.ultimatesmartstudent.Performance_Report;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommunicationList extends AppCompatActivity {


    @BindView(R.id.recyclerviewlist)
    RecyclerView recyclerView;

    @BindView(R.id.parent)
    RelativeLayout parent;

    @BindView(R.id.lytHeader)
    LinearLayout lytHeader;

    RecyclerView.LayoutManager layoutManager;
    CommunicationAdapter adapter;
    ArrayList<CommunicationBean> leavelist = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;

    int loaded = 0;

    @BindView(R.id.totalRecord)
    TextView totalRecord;
    Dialog mBottomSheetDialog;
    @BindView(R.id.spinerVehicletype)
    Spinner spinnerStudent;

    @BindView(R.id.start_date)
    TextView start_date;
    @BindView(R.id.end_date)
    TextView end_date;


    String type="1",name="",date="",view_type="class",filter_id="",tdate="",ydate="",filter_start_date = "",filter_end_date = "",filter_today_date = "";
    CommonProgress commonProgress;
    @BindView(R.id.txtTitle)TextView txtTitle;
    @RequiresApi(api = Build.VERSION_CODES.N)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication_list);

        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CommunicationAdapter(this,leavelist);
        recyclerView.setAdapter(adapter);
        txtTitle.setText(getString(R.string.communication));
        fetchleavelist();
        getTodayDate();
    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //  adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        tdate = dateFrmOut.format(setdate);
        filter_end_date = dateFrmOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        //ydate= dateFormat.format(cal.getTime());
    }

    @OnClick(R.id.end)
    public void end() {
        Toast.makeText(getApplicationContext(),"You can't change end date",Toast.LENGTH_SHORT).show();

    }


    @OnClick(R.id.start)
    public void start() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(CommunicationList.this,
                R.style.MyDatePickerDialogTheme, ondate1,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)-1);
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate1 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            start_date.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            filter_start_date = dateFrmOut.format(setdate);
            fetchleavelist();
        }
    };

    private void fetchleavelist() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        //   ErpProgress.showProgressBar(this, "Please wait...");

        HashMap<String, String> params = new HashMap<>();

        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("section_id", User.getCurrentUser().getSection_id());
        params.put("std_id", User.getCurrentUser().getId());
        // params.put("today", "");
        params.put("filter_start_date", filter_start_date);
        params.put("filter_end_date", filter_end_date);
        params.put("view_type", "std");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COMMUNICATION,apicallback, this, params);

    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (leavelist != null) {
                        leavelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hw_data");
                    leavelist = CommunicationBean.parseHWArray(jsonArray);
                    if (leavelist.size() > 0) {
                        adapter.setList(leavelist);
                        totalRecord.setText("Total Entries:- "+String.valueOf(leavelist.size()));
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        lytHeader.setVisibility(View.VISIBLE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setList(leavelist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                        lytHeader.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                lytHeader.setVisibility(View.GONE);
                leavelist.clear();
                adapter.setList(leavelist);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void backCall(){
        finish();
    }

}