package com.ultimate.ultimatesmartstudent.Performance_Report;



import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CleanlinessBean {

    private static String ID="id";
    private static String COMMENT="es_remarks";
    private static String H_DATE="date";
    /**
     * id : 2
     * comment : asfgsdfgdgegdfgdfgfg
     * image : office_admin/images/homework/hw_03272018_080342.jpg
     * date : 2018-03-27
     * class : 10
     */

    private String id;
    private String comment;
    private String date;
    private String section;
    private String section_name;

    public String getEs_regid() {
        return es_regid;
    }

    public void setEs_regid(String es_regid) {
        this.es_regid = es_regid;
    }

    public String getEs_sname() {
        return es_sname;
    }

    public void setEs_sname(String es_sname) {
        this.es_sname = es_sname;
    }


    private String es_regid;
    private String es_sname;

    public String getEs_uniform() {
        return es_uniform;
    }

    public void setEs_uniform(String es_uniform) {
        this.es_uniform = es_uniform;
    }

    public String getEs_belt() {
        return es_belt;
    }

    public void setEs_belt(String es_belt) {
        this.es_belt = es_belt;
    }

    public String getEs_idcard() {
        return es_idcard;
    }

    public void setEs_idcard(String es_idcard) {
        this.es_idcard = es_idcard;
    }

    public String getEs_socks() {
        return es_socks;
    }

    public void setEs_socks(String es_socks) {
        this.es_socks = es_socks;
    }

    public String getEs_shoes() {
        return es_shoes;
    }

    public void setEs_shoes(String es_shoes) {
        this.es_shoes = es_shoes;
    }

    public String getEs_month() {
        return es_month;
    }

    public void setEs_month(String es_month) {
        this.es_month = es_month;
    }

    public String getEs_ribbon() {
        return es_ribbon;
    }

    public void setEs_ribbon(String es_ribbon) {
        this.es_ribbon = es_ribbon;
    }

    public String getEs_turban() {
        return es_turban;
    }

    public void setEs_turban(String es_turban) {
        this.es_turban = es_turban;
    }

    public String getEs_tracksuit() {
        return es_tracksuit;
    }

    public void setEs_tracksuit(String es_tracksuit) {
        this.es_tracksuit = es_tracksuit;
    }

    public String getEs_nails() {
        return es_nails;
    }

    public void setEs_nails(String es_nails) {
        this.es_nails = es_nails;
    }

    private String es_uniform;
    private String es_belt;
    private String es_idcard;
    private String es_socks;

    private String es_shoes;
    private String es_month;
    private String es_ribbon;

    private String es_turban;
    private String es_tracksuit;
    private String es_nails;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    private String profile;

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getStaff_image() {
        return staff_image;
    }

    public void setStaff_image(String staff_image) {
        this.staff_image = staff_image;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    private String staff_name;
    private String staff_image;
    private String post_name;


    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }



    /**
     * class_id : 10
     * class_name : 6TH
     */

    private String class_id;
    private String class_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public static ArrayList<CleanlinessBean> parseHWArray(JSONArray arrayObj) {
        ArrayList<CleanlinessBean> list = new ArrayList<CleanlinessBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                CleanlinessBean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static CleanlinessBean parseHWObject(JSONObject jsonObject) {
        CleanlinessBean casteObj = new CleanlinessBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(COMMENT) && !jsonObject.getString(COMMENT).isEmpty() && !jsonObject.getString(COMMENT).equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString(COMMENT));
            }
            if (jsonObject.has(H_DATE) && !jsonObject.getString(H_DATE).isEmpty() && !jsonObject.getString(H_DATE).equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString(H_DATE));
            }

            if (jsonObject.has("es_regid") && !jsonObject.getString("es_regid").isEmpty() && !jsonObject.getString("es_regid").equalsIgnoreCase("null")) {
                casteObj.setEs_regid(jsonObject.getString("es_regid"));
            }
            if (jsonObject.has("es_sname") && !jsonObject.getString("es_sname").isEmpty() && !jsonObject.getString("es_sname").equalsIgnoreCase("null")) {
                casteObj.setEs_sname(jsonObject.getString("es_sname"));
            }
            if (jsonObject.has("es_uniform") && !jsonObject.getString("es_uniform").isEmpty() && !jsonObject.getString("es_uniform").equalsIgnoreCase("null")) {
                casteObj.setEs_uniform(jsonObject.getString("es_uniform"));
            }
            if (jsonObject.has("rowcount") && !jsonObject.getString("rowcount").isEmpty() && !jsonObject.getString("rowcount").equalsIgnoreCase("null")) {
                casteObj.setRowcount(jsonObject.getString("rowcount"));
            }

            if (jsonObject.has("section") && !jsonObject.getString("section").isEmpty() && !jsonObject.getString("section").equalsIgnoreCase("null")) {
                casteObj.setSection(jsonObject.getString("section"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }

            if (jsonObject.has("profile") && !jsonObject.getString("profile").isEmpty() && !jsonObject.getString("profile").equalsIgnoreCase("null")) {
                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString("profile"));
            }
            if (jsonObject.has("es_belt") && !jsonObject.getString("es_belt").isEmpty() && !jsonObject.getString("es_belt").equalsIgnoreCase("null")) {
                casteObj.setEs_belt(jsonObject.getString("es_belt"));
            }

            if (jsonObject.has("es_idcard") && !jsonObject.getString("es_idcard").isEmpty() && !jsonObject.getString("es_idcard").equalsIgnoreCase("null")) {
                casteObj.setEs_idcard(jsonObject.getString("es_idcard"));
            }

            if (jsonObject.has("es_socks") && !jsonObject.getString("es_socks").isEmpty() && !jsonObject.getString("es_socks").equalsIgnoreCase("null")) {
                casteObj.setEs_socks(jsonObject.getString("es_socks"));
            }
            if (jsonObject.has("es_shoes") && !jsonObject.getString("es_shoes").isEmpty() && !jsonObject.getString("es_shoes").equalsIgnoreCase("null")) {
                casteObj.setEs_shoes(jsonObject.getString("es_shoes"));
            }
            if (jsonObject.has("es_month") && !jsonObject.getString("es_month").isEmpty() && !jsonObject.getString("es_month").equalsIgnoreCase("null")) {
                casteObj.setEs_month(jsonObject.getString("es_month"));
            }
            if (jsonObject.has("es_ribbon") && !jsonObject.getString("es_ribbon").isEmpty() && !jsonObject.getString("es_ribbon").equalsIgnoreCase("null")) {
                casteObj.setEs_ribbon(jsonObject.getString("es_ribbon"));
            }

            if (jsonObject.has("es_turban") && !jsonObject.getString("es_turban").isEmpty() && !jsonObject.getString("es_turban").equalsIgnoreCase("null")) {
                casteObj.setEs_turban(jsonObject.getString("es_turban"));
            }
            if (jsonObject.has("es_tracksuit") && !jsonObject.getString("es_tracksuit").isEmpty() && !jsonObject.getString("es_tracksuit").equalsIgnoreCase("null")) {
                casteObj.setEs_tracksuit(jsonObject.getString("es_tracksuit"));
            }
            if (jsonObject.has("es_nails") && !jsonObject.getString("es_nails").isEmpty() && !jsonObject.getString("es_nails").equalsIgnoreCase("null")) {
                casteObj.setEs_nails(jsonObject.getString("es_nails"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
