package com.ultimate.ultimatesmartstudent.Performance_Report;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommunicationAdapter extends RecyclerView.Adapter<CommunicationAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<CommunicationBean> dataList;
    public CommunicationAdapter(Context mContext, ArrayList<CommunicationBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.communication_class_report, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.GONE);
        } else {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.VISIBLE);

            final CommunicationBean data = dataList.get(position - 1);

            holder.txtName.setText(data.getEs_sname());
            holder.txtRoll.setText(Utils.getDateFormated_date_new(data.getEs_month()));
            holder.txtFName.setText(data.getEs_month());
//            if(data.get)
            holder.txtAbsent.setText(data.getEs_reading());
            holder.txtLeave.setText(data.getEs_writing());
            holder.txtPresent.setText(data.getEs_speaking());

            holder.txtAbsent1.setText(data.getEs_listening());
            holder.txtLeave1.setText(data.getEs_behavior());
            holder.txtPresent1.setText(data.getEs_punctuality());
        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }


    public void setList(ArrayList<CommunicationBean> dataList) {
        this.dataList = dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAbsent)
        TextView txtAbsent;
        @BindView(R.id.txtLeave)
        TextView txtLeave;
        @BindView(R.id.txtPresent)
        TextView txtPresent;

        @BindView(R.id.txtAbsent1)
        TextView txtAbsent1;
        @BindView(R.id.txtLeave1)
        TextView txtLeave1;
        @BindView(R.id.txtPresent1)
        TextView txtPresent1;

        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;
        @BindView(R.id.txtFName)
        TextView txtFName;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
