package com.ultimate.ultimatesmartstudent.Performance_Report;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CleanlinessAdapter extends RecyclerView.Adapter<CleanlinessAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<CleanlinessBean> dataList;
    public CleanlinessAdapter(Context mContext, ArrayList<CleanlinessBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.cleanliness_class_report, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.GONE);
        } else {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.VISIBLE);

            final CleanlinessBean data = dataList.get(position - 1);

            holder.txtName.setText(data.getEs_sname());
            holder.txtRoll.setText(Utils.getDateFormated_date_new(data.getEs_month()));
            holder.txtFName.setText(data.getEs_month());
//            if(data.get)
            holder.txtAbsent.setText(data.getEs_uniform());
            holder.txtLeave.setText(data.getEs_belt());
            holder.txtPresent.setText(data.getEs_idcard());

            holder.txtAbsent1.setText(data.getEs_socks());
            holder.txtLeave1.setText(data.getEs_shoes());
            holder.txtPresent1.setText(data.getEs_ribbon());

            holder.txtAbsent2.setText(data.getEs_turban());
            holder.txtLeave2.setText(data.getEs_tracksuit());
            holder.txtPresent2.setText(data.getEs_nails());
        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }


    public void setList(ArrayList<CleanlinessBean> dataList) {
        this.dataList = dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAbsent)
        TextView txtAbsent;
        @BindView(R.id.txtLeave)
        TextView txtLeave;
        @BindView(R.id.txtPresent)
        TextView txtPresent;

        @BindView(R.id.txtAbsent1)
        TextView txtAbsent1;
        @BindView(R.id.txtLeave1)
        TextView txtLeave1;
        @BindView(R.id.txtPresent1)
        TextView txtPresent1;

        @BindView(R.id.txtAbsent2)
        TextView txtAbsent2;
        @BindView(R.id.txtLeave2)
        TextView txtLeave2;
        @BindView(R.id.txtPresent2)
        TextView txtPresent2;

        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;
        @BindView(R.id.txtFName)
        TextView txtFName;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
