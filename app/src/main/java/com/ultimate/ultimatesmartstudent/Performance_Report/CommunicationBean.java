package com.ultimate.ultimatesmartstudent.Performance_Report;


import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CommunicationBean {

    private static String ID="id";
    private static String COMMENT="es_remarks";
    private static String H_DATE="date";
    /**
     * id : 2
     * comment : asfgsdfgdgegdfgdfgfg
     * image : office_admin/images/homework/hw_03272018_080342.jpg
     * date : 2018-03-27
     * class : 10
     */

    private String id;
    private String comment;
    private String date;
    private String section;
    private String section_name;

    public String getEs_regid() {
        return es_regid;
    }

    public void setEs_regid(String es_regid) {
        this.es_regid = es_regid;
    }

    public String getEs_sname() {
        return es_sname;
    }

    public void setEs_sname(String es_sname) {
        this.es_sname = es_sname;
    }


    private String es_regid;
    private String es_sname;

    private String es_reading;

    public String getEs_reading() {
        return es_reading;
    }

    public void setEs_reading(String es_reading) {
        this.es_reading = es_reading;
    }

    public String getEs_writing() {
        return es_writing;
    }

    public void setEs_writing(String es_writing) {
        this.es_writing = es_writing;
    }

    public String getEs_listening() {
        return es_listening;
    }

    public void setEs_listening(String es_listening) {
        this.es_listening = es_listening;
    }

    public String getEs_speaking() {
        return es_speaking;
    }

    public void setEs_speaking(String es_speaking) {
        this.es_speaking = es_speaking;
    }

    public String getEs_behavior() {
        return es_behavior;
    }

    public void setEs_behavior(String es_behavior) {
        this.es_behavior = es_behavior;
    }

    public String getEs_month() {
        return es_month;
    }

    public void setEs_month(String es_month) {
        this.es_month = es_month;
    }

    public String getEs_punctuality() {
        return es_punctuality;
    }

    public void setEs_punctuality(String es_punctuality) {
        this.es_punctuality = es_punctuality;
    }

    private String es_writing;
    private String es_listening;
    private String es_speaking;

    private String es_behavior;
    private String es_month;
    private String es_punctuality;



    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    private String profile;

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getStaff_image() {
        return staff_image;
    }

    public void setStaff_image(String staff_image) {
        this.staff_image = staff_image;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    private String staff_name;
    private String staff_image;
    private String post_name;


    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }



    /**
     * class_id : 10
     * class_name : 6TH
     */

    private String class_id;
    private String class_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public static ArrayList<CommunicationBean> parseHWArray(JSONArray arrayObj) {
        ArrayList<CommunicationBean> list = new ArrayList<CommunicationBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                CommunicationBean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static CommunicationBean parseHWObject(JSONObject jsonObject) {
        CommunicationBean casteObj = new CommunicationBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(COMMENT) && !jsonObject.getString(COMMENT).isEmpty() && !jsonObject.getString(COMMENT).equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString(COMMENT));
            }
            if (jsonObject.has(H_DATE) && !jsonObject.getString(H_DATE).isEmpty() && !jsonObject.getString(H_DATE).equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString(H_DATE));
            }

            if (jsonObject.has("es_regid") && !jsonObject.getString("es_regid").isEmpty() && !jsonObject.getString("es_regid").equalsIgnoreCase("null")) {
                casteObj.setEs_regid(jsonObject.getString("es_regid"));
            }
            if (jsonObject.has("es_sname") && !jsonObject.getString("es_sname").isEmpty() && !jsonObject.getString("es_sname").equalsIgnoreCase("null")) {
                casteObj.setEs_sname(jsonObject.getString("es_sname"));
            }

            if (jsonObject.has("rowcount") && !jsonObject.getString("rowcount").isEmpty() && !jsonObject.getString("rowcount").equalsIgnoreCase("null")) {
                casteObj.setRowcount(jsonObject.getString("rowcount"));
            }

            if (jsonObject.has("section") && !jsonObject.getString("section").isEmpty() && !jsonObject.getString("section").equalsIgnoreCase("null")) {
                casteObj.setSection(jsonObject.getString("section"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }

            if (jsonObject.has("profile") && !jsonObject.getString("profile").isEmpty() && !jsonObject.getString("profile").equalsIgnoreCase("null")) {
                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString("profile"));
            }
            if (jsonObject.has("es_reading") && !jsonObject.getString("es_reading").isEmpty() && !jsonObject.getString("es_reading").equalsIgnoreCase("null")) {
                casteObj.setEs_reading(jsonObject.getString("es_reading"));
            }

            if (jsonObject.has("es_writing") && !jsonObject.getString("es_writing").isEmpty() && !jsonObject.getString("es_writing").equalsIgnoreCase("null")) {
                casteObj.setEs_writing(jsonObject.getString("es_writing"));
            }

            if (jsonObject.has("es_listening") && !jsonObject.getString("es_listening").isEmpty() && !jsonObject.getString("es_listening").equalsIgnoreCase("null")) {
                casteObj.setEs_listening(jsonObject.getString("es_listening"));
            }
            if (jsonObject.has("es_speaking") && !jsonObject.getString("es_speaking").isEmpty() && !jsonObject.getString("es_speaking").equalsIgnoreCase("null")) {
                casteObj.setEs_speaking(jsonObject.getString("es_speaking"));
            }
            if (jsonObject.has("es_month") && !jsonObject.getString("es_month").isEmpty() && !jsonObject.getString("es_month").equalsIgnoreCase("null")) {
                casteObj.setEs_month(jsonObject.getString("es_month"));
            }
            if (jsonObject.has("es_behavior") && !jsonObject.getString("es_behavior").isEmpty() && !jsonObject.getString("es_behavior").equalsIgnoreCase("null")) {
                casteObj.setEs_behavior(jsonObject.getString("es_behavior"));
            }

            if (jsonObject.has("es_punctuality") && !jsonObject.getString("es_punctuality").isEmpty() && !jsonObject.getString("es_punctuality").equalsIgnoreCase("null")) {
                casteObj.setEs_punctuality(jsonObject.getString("es_punctuality"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
