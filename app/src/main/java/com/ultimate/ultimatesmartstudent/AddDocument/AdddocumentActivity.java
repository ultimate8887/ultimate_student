package com.ultimate.ultimatesmartstudent.AddDocument;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.firebase.ui.auth.IdpResponse;
import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdddocumentActivity extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Animation animation;
    CommonProgress commonProgress;
    @BindView(R.id.edit_imageaadhar)
    ImageView edit_imageaadhar;
    @BindView(R.id.imageaadhar)
    ImageView imageaadhar;

    @BindView(R.id.edit_image10th)
    ImageView edit_image10th;
    @BindView(R.id.image10th)
    ImageView image10th;

    @BindView(R.id.edit_image12th)
    ImageView edit_image12th;
    @BindView(R.id.image12th)
    ImageView image12th;

    @BindView(R.id.edit_imageschcert)
    ImageView edit_imageschcert;
    @BindView(R.id.imageschcert)
    ImageView imageschcert;

    @BindView(R.id.edit_imageschtrans)
    ImageView edit_imageschtrans;
    @BindView(R.id.imageschtrans)
    ImageView imageschtrans;

    @BindView(R.id.edit_imagegrad)
    ImageView edit_imagegrad;
    @BindView(R.id.imagegrad)
    ImageView imagegrad;


    @BindView(R.id.edit_imageschincome)
    ImageView edit_imageschincome;
    @BindView(R.id.imageschincome)
    ImageView imageschincome;

    @BindView(R.id.f_edit_imageaadhar)
    ImageView f_edit_imageaadhar;
    @BindView(R.id.f_imageaadhar)
    ImageView f_imageaadhar;


    @BindView(R.id.m_edit_imageaadhar)
    ImageView m_edit_imageaadhar;
    @BindView(R.id.m_imageaadhar)
    ImageView m_imageaadhar;


    @BindView(R.id.edit_imageschresidence)
    ImageView edit_imageschresidence;
    @BindView(R.id.imageschresidence)
    ImageView imageschresidence;

    @BindView(R.id.submit)
    TextView submit;

    private int selection;
    private Bitmap imageaadharBitmap=null, image10thBitmapProf=null, image12thBitmapProf=null, imageschcertBitmapProf=null, imageschtransBitmapProf=null,
            imagegradBitmapProf=null,imageincomeBitmapProf=null,imageresidenceBitmapProf=null,f_imageaadharBitmap=null,m_imageaadharBitmap=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adddocument);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText("Upload Document");
        checkdata();

    }

    private void checkdata() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GET_DOC, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        Log.i("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                        if (!jsonObject.getJSONObject("doc_data").getString("aadhar_image").equalsIgnoreCase("")){
                            imageaadhar.setVisibility(View.VISIBLE);
                            //Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("aadhar_image")).placeholder(R.color.divider).into(imageaadhar);
                            edit_imageaadhar.setImageResource(R.drawable.ic_baseline_edit);
                            submit.setText("Update");
                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("aadhar_image"), imageaadhar);
                        }

                        if (!jsonObject.getJSONObject("doc_data").getString("f_aadhar_image").equalsIgnoreCase("")){
                            f_imageaadhar.setVisibility(View.VISIBLE);
                            //Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("aadhar_image")).placeholder(R.color.divider).into(imageaadhar);
                            f_edit_imageaadhar.setImageResource(R.drawable.ic_baseline_edit);
                            submit.setText("Update");

                            Log.e("imagesssss",Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("f_aadhar_image"));

                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("f_aadhar_image"), f_imageaadhar);
                        }

                        if (!jsonObject.getJSONObject("doc_data").getString("m_aadhar_image").equalsIgnoreCase("")){
                            m_imageaadhar.setVisibility(View.VISIBLE);
                            //Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("aadhar_image")).placeholder(R.color.divider).into(imageaadhar);
                            m_edit_imageaadhar.setImageResource(R.drawable.ic_baseline_edit);
                            submit.setText("Update");
                            Log.e("imagesssss",Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("m_aadhar_image"));

                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("m_aadhar_image"), m_imageaadhar);
                        }

                        if (!jsonObject.getJSONObject("doc_data").getString("residence_image").equalsIgnoreCase("")){
                            imageschresidence.setVisibility(View.VISIBLE);
                            //Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("aadhar_image")).placeholder(R.color.divider).into(imageaadhar);
                            edit_imageschresidence.setImageResource(R.drawable.ic_baseline_edit);
                            submit.setText("Update");
                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("residence_image"), imageschresidence);
                        }

                        if (!jsonObject.getJSONObject("doc_data").getString("income_image").equalsIgnoreCase("")){
                            imageschincome.setVisibility(View.VISIBLE);
                            //Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("aadhar_image")).placeholder(R.color.divider).into(imageaadhar);
                            edit_imageschincome.setImageResource(R.drawable.ic_baseline_edit);
                            submit.setText("Update");
                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("income_image"), imageschincome);

                            Log.e("imagesssss",Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("income_image"));
                        }


                        if (!jsonObject.getJSONObject("doc_data").getString("tenmarksheet_image").equalsIgnoreCase("")){
                            image10th.setVisibility(View.VISIBLE);
                           // Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("tenmarksheet_image")).placeholder(R.color.divider).into(image10th);
                            edit_image10th.setImageResource(R.drawable.ic_baseline_edit);
                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("tenmarksheet_image"), image10th);
                            submit.setText("Update");
                        }

                        if (!jsonObject.getJSONObject("doc_data").getString("twelvemarksheet_image").equalsIgnoreCase("")){
                            image12th.setVisibility(View.VISIBLE);
                          //  Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("twelvemarksheet_image")).placeholder(R.color.divider).into(image12th);
                            edit_image12th.setImageResource(R.drawable.ic_baseline_edit);
                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("twelvemarksheet_image"), image12th);
                            submit.setText("Update");
                        }

                        if (!jsonObject.getJSONObject("doc_data").getString("schol_certif_image").equalsIgnoreCase("")){
                            imageschcert.setVisibility(View.VISIBLE);
                        //    Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("schol_certif_image")).placeholder(R.color.divider).into(imageschcert);
                            edit_imageschcert.setImageResource(R.drawable.ic_baseline_edit);
                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("schol_certif_image"), imageschcert);
                            submit.setText("Update");
                        }

                        if (!jsonObject.getJSONObject("doc_data").getString("schol_trans_certif_image").equalsIgnoreCase("")){
                            imageschtrans.setVisibility(View.VISIBLE);
                           // Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("schol_trans_certif_image")).placeholder(R.color.divider).into(imageschtrans);
                            edit_imageschtrans.setImageResource(R.drawable.ic_baseline_edit);
                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("schol_trans_certif_image"), imageschtrans);
                            submit.setText("Update");
                        }

                        if (!jsonObject.getJSONObject("doc_data").getString("graduation_image").equalsIgnoreCase("")){
                            imagegrad.setVisibility(View.VISIBLE);
                          //  Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("graduation_image")).placeholder(R.color.divider).into(imagegrad);
                            edit_imagegrad.setImageResource(R.drawable.ic_baseline_edit);
                            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("graduation_image"), imagegrad);
                            submit.setText("Update");
                        }


                      //  Toast.makeText(AdddocumentActivity.this, "Documents list!", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(AdddocumentActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);


    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageaadhar)
    public void edit_imageaadhar() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageaadhar.startAnimation(animation);

        selection=1;


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_image10th)
    public void edit_image10th() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_image10th.startAnimation(animation);

        selection=2;


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_image12th)
    public void edit_image12th() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_image12th.startAnimation(animation);

        selection=3;


    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageschcert)
    public void edit_imageschcert() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageschcert.startAnimation(animation);

        selection=4;


    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageschtrans)
    public void edit_imageschtrans() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageschtrans.startAnimation(animation);

        selection=5;


    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imagegrad)
    public void edit_imagegrad() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imagegrad.startAnimation(animation);

        selection=6;


    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageschresidence)
    public void edit_imageschresidenceee() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageschresidence.startAnimation(animation);

        selection=7;



    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageschincome)
    public void edit_imageschincomeee() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageschincome.startAnimation(animation);

        selection=8;


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.f_edit_imageaadhar)
    public void f_edit_imageaadharrr() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        f_edit_imageaadhar.startAnimation(animation);

        selection=9;


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.m_edit_imageaadhar)
    public void m_edit_imageaadharrr() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        m_edit_imageaadhar.startAnimation(animation);

        selection=10;


    }
//
//    @SuppressLint("MissingSuperCall")
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(AdddocumentActivity.this, data);
//                    CropImage.activity(imageUri)
//                            .start(AdddocumentActivity.this);
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    if (selection==1){
//                        imageaadharBitmap = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        imageaadhar.setVisibility(View.VISIBLE);
//                        imageaadhar.setImageBitmap(imageaadharBitmap);
//                    }
//                    if (selection==2){
//                       image10thBitmapProf = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                       image10th.setVisibility(View.VISIBLE);
//                       image10th.setImageBitmap(image10thBitmapProf);
//                    }
//                    if (selection==3){
//                        image12thBitmapProf = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        image12th.setVisibility(View.VISIBLE);
//                        image12th.setImageBitmap(image12thBitmapProf);
//                    }
//                    if (selection==4){
//                        imageschcertBitmapProf = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        imageschcert.setVisibility(View.VISIBLE);
//                        imageschcert.setImageBitmap(imageschcertBitmapProf);
//                    }
//                    if (selection==5){
//                        imageschtransBitmapProf = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        imageschtrans.setVisibility(View.VISIBLE);
//                        imageschtrans.setImageBitmap(imageschtransBitmapProf);
//                    }
//                    if (selection==6){
//                        imagegradBitmapProf = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        imagegrad.setVisibility(View.VISIBLE);
//                        imagegrad.setImageBitmap(imagegradBitmapProf);
//                    }
//                    if (selection==7){
//                        imageresidenceBitmapProf = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        imageschresidence.setVisibility(View.VISIBLE);
//                        imageschresidence.setImageBitmap(imageresidenceBitmapProf);
//                    }
//                    if (selection==8){
//                        imageincomeBitmapProf = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        imageschincome.setVisibility(View.VISIBLE);
//                        imageschincome.setImageBitmap(imageincomeBitmapProf);
//                    }
//                    if (selection==9){
//                        f_imageaadharBitmap = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        f_imageaadhar.setVisibility(View.VISIBLE);
//                        f_imageaadhar.setImageBitmap(f_imageaadharBitmap);
//                    }
//                    if (selection==10){
//                        m_imageaadharBitmap = Utils.getBitmapFromUri(AdddocumentActivity.this, resultUri, 2048);
//                        m_imageaadhar.setVisibility(View.VISIBLE);
//                        m_imageaadhar.setImageBitmap(m_imageaadharBitmap);
//                    }
//
//            }
//        }
//
//
//    }

    @OnClick(R.id.submit)
    public void submitt() {
        submit.startAnimation(animation);
        commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", User.getCurrentUser().getId());
            String msg="";

            if (imageaadharBitmap != null) {
                String encoded = Utils.encodeToBase64(imageaadharBitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("imageaadhar", encoded);
            }else{
                params.put("imageaadhar", "");
            }
            if (f_imageaadharBitmap != null) {
            String encoded = Utils.encodeToBase64(f_imageaadharBitmap, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("f_imageaadhar", encoded);
        }else{
            params.put("f_imageaadhar", "");
        }


        if (m_imageaadharBitmap != null) {
            String encoded = Utils.encodeToBase64(m_imageaadharBitmap, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("m_imageaadhar", encoded);
        }else{
            params.put("m_imageaadhar", "");
        }


            if (image10thBitmapProf != null) {
                String encoded = Utils.encodeToBase64(image10thBitmapProf, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image10th", encoded);
            }else{
                params.put("image10th", "");
            }

           if (image12thBitmapProf != null) {
            String encoded = Utils.encodeToBase64(image12thBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image12th", encoded);
           }else{
            params.put("image12th", "");
            }

        if (imageschcertBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imageschcertBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("imageschcert", encoded);
        }else{
            params.put("imageschcert", "");
        }

        if (imageschtransBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imageschtransBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("imageschtrans", encoded);
        }else{
            params.put("imageschtrans", "");
        }

        if (imagegradBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imagegradBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("imagegrad", encoded);
        }else{
            params.put("imagegrad", "");
        }

        if (imageresidenceBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imageresidenceBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("residence", encoded);
        }else{
            params.put("residence", "");
        }

        if (imageincomeBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imageincomeBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("income", encoded);
        }else{
            params.put("income", "");
        }


            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_DOC, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                    commonProgress.dismiss();
                    if (error == null) {
                             //   User.setaddCurrentUser(jsonObject.getJSONObject(Constants.USERDATA));
                                Toast.makeText(AdddocumentActivity.this, "Documents Updated Successfully", Toast.LENGTH_SHORT).show();
                         //   Log.i("USERDATA", User.getCurrentUser().getAadhar_image());
                            finish();
                    } else {
                        Toast.makeText(AdddocumentActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);
    }




    @OnClick(R.id.imgBackmsg)
    public void onback() {
        imgBackmsg.startAnimation(animation);
        finish();
    }
}