package com.ultimate.ultimatesmartstudent.GatePass.NewGatePass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartstudent.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewGatePassActivity extends AppCompatActivity {

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_gate_pass);

        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.gatepass)+" "+getString(R.string.list));
        setupTabPager();
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // txtTitle.setText("Gatepass");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });

    }

    private void setupTabPager() {

        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));

        tablayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewpager) {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                String value=String.valueOf(tab.getText());

                if (value.equalsIgnoreCase(getString(R.string.pending))){
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(NewGatePassActivity.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(NewGatePassActivity.this, R.color.light_red)
                    );
                    // Selected Tab Indicator Color
                    //   tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.leave));
                    //  tablayout.setSelectedTabIndicatorHeight(5);
                }else {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(NewGatePassActivity.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(NewGatePassActivity.this, R.color.green)
                    );
                    // Selected Tab Indicator Color
                    //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.absent));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });

        FilterTabPager adapter = new FilterTabPager(getSupportFragmentManager(),this);
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
    }
}