package com.ultimate.ultimatesmartstudent.GatePass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import java.util.ArrayList;

public class PersonAdapter extends BaseAdapter {
    private final ArrayList<GatePassBean> casteList;
    Context context;
    LayoutInflater inflter;

    public PersonAdapter(Context applicationContext, ArrayList<GatePassBean> casteList) {
        this.context = applicationContext;
        this.casteList = casteList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt_new, null);
        TextView label = (TextView) view.findViewById(R.id.txt);
        RelativeLayout lyt = (RelativeLayout) view.findViewById(R.id.lyt);
        TextView label1 = (TextView) view.findViewById(R.id.txtText);
        CircularImageView dp = (CircularImageView) view.findViewById(R.id.dp);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Person");
            lyt.setVisibility(View.GONE);
            label.setVisibility(View.VISIBLE);
        } else {
            label.setVisibility(View.GONE);
            lyt.setVisibility(View.VISIBLE);
            GatePassBean classObj = casteList.get(i - 1);
            label1.setText(classObj.getGate_person()+" \n("+classObj.getGate_relation()+")");
            String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/";
            String img=classObj.getGate_profile();
            if (img != null) {
                Picasso.get().load(path+img).placeholder(R.drawable.profile).into(dp);
            }else {
                Picasso.get().load(R.drawable.profile).into(dp);
            }

        }

        return view;
    }

}
