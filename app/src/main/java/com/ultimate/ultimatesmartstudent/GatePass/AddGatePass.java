package com.ultimate.ultimatesmartstudent.GatePass;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Leave_Mod.LeaveActivity;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddGatePass extends AppCompatActivity {


    @BindView(R.id.top_lyt)
    RelativeLayout top_lyt;
    @BindView(R.id.root)
    RelativeLayout root;

    @BindView(R.id.textNorecord)
    TextView txtNorecord;

    @BindView(R.id.spinner)
    Spinner spinnerstudnt;
    @BindView(R.id.circleimgrecepone)
    CircularImageView image;
    @BindView(R.id.edtgName)
    EditText edtgName;
    @BindView(R.id.edtRelation)
    EditText edtRelation;
    CommonProgress commonProgress;
    @BindView(R.id.edtgMobile)
    EditText edtgMobile;
    @BindView(R.id.edtTime)
    TextView edtTime;
    @BindView(R.id.edtReason)
    EditText edtReason;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    Animation animation;
    String img="",gid="";
    private ArrayList<GatePassBean> stuList;
    private PersonAdapter adapterstu;
    String am_pm="",date="";
    @BindView(R.id.speak_text)
    ImageView speak_text;
    private static final int REQUEST_CODE_SPEECH_INPUT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_add);
        ButterKnife.bind(this);

        speak_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent
                        = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                        Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak to text");

                try {
                    startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT);
                }
                catch (Exception e) {
                    Toast
                            .makeText(AddGatePass.this, " " + e.getMessage(),
                                    Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });

        commonProgress=new CommonProgress(this);
        txtTitle.setText(getString(R.string.gatepass));
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        edtgName.setEnabled(false);
        edtgMobile.setEnabled(false);
        edtRelation.setEnabled(false);
        fetchStudent();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(
                        RecognizerIntent.EXTRA_RESULTS);
                edtReason.setText(
                        Objects.requireNonNull(result).get(0));
            }
        }
    }

    private void fetchStudent() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "person");;
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STDGATEPASS_URL, apiCallbackstudnt, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback apiCallbackstudnt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    stuList = GatePassBean.parseClassArray(jsonObject.getJSONArray(Constants.GATEPASS_DATA));
                    adapterstu = new PersonAdapter(getApplicationContext(), stuList);
                    spinnerstudnt.setAdapter(adapterstu);
                    spinnerstudnt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            root.setVisibility(View.VISIBLE);
                            if (i > 0) {
                                top_lyt.setVisibility(View.VISIBLE);
                                txtNorecord.setVisibility(View.GONE);
                                gid=stuList.get(i-1).getId();
                                edtgName.setText(stuList.get(i-1).getGate_person());
                                edtgMobile.setText(stuList.get(i-1).getGate_personcont());
                                edtRelation.setText(stuList.get(i-1).getGate_relation());
                                String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/";
                                 img=stuList.get(i-1).getGate_profile();
                                if (img != null) {
                                    //Picasso.get().load(path+stulist.get(position).getGate_profile()).placeholder(R.drawable.stud).into(viewHolder.circleimg);
                                    Utils.progressImg(path+img,image,AddGatePass.this);
                                } else {
                                    Picasso.get().load(R.drawable.profile).into(image);
                                }
                            } else {
                                top_lyt.setVisibility(View.GONE);
                                txtNorecord.setVisibility(View.VISIBLE);
                                txtNorecord.setText("kindly select person");
                                edtgName.setText("");
                                edtgMobile.setText("");
                                edtRelation.setText("");
                                Picasso.get().load(R.drawable.profile).into(image);
                                img="";
                                gid="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                top_lyt.setVisibility(View.GONE);
                root.setVisibility(View.INVISIBLE);
                txtNorecord.setVisibility(View.VISIBLE);
                txtNorecord.setText("Guardian details not found, \nkindly add Guardian details first \nto add gate-pass.");
                Toast.makeText(getApplicationContext(),"Guardian details not found, kindly add Guardian details first to add gate-pass.",Toast.LENGTH_LONG).show();
            }
        }
    };

    @OnClick(R.id.edtTime)
    public void edtTimemm(){
// TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddGatePass.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int selectedMinute) {
                // edtTime.setText( selectedHour + ":" + selectedMinute );
                String status = "AM";
                if(hourOfDay > 11)
                {
                    // If the hour is greater than or equal to 12
                    // Then the current AM PM status is PM
                    status = "PM";
                }
                if (status.equalsIgnoreCase("AM")){
                    am_pm="Morning";
                }else {
                    am_pm="Evening";
                }
                // Initialize a new variable to hold 12 hour format hour value
                int hour_of_12_hour_format;
                if(hourOfDay > 11){
                    // If the hour is greater than or equal to 12
                    // Then we subtract 12 from the hour to make it 12 hour format time
                    hour_of_12_hour_format = hourOfDay - 12;
                }
                else {
                    hour_of_12_hour_format = hourOfDay;
                }
                String minute="";
                if (selectedMinute==0){
                    minute="00";
                }else if (selectedMinute==1){
                    minute="01";
                }else if (selectedMinute==2){
                    minute="02";
                }else if (selectedMinute==3){
                    minute="03";
                }else if (selectedMinute==4){
                    minute="04";
                }else if (selectedMinute==5){
                    minute="05";
                }else if (selectedMinute==6){
                    minute="06";
                }else if (selectedMinute==7){
                    minute="07";
                }else if (selectedMinute==8){
                    minute="08";
                }else if (selectedMinute==9){
                    minute="09";
                }else {
                    minute= String.valueOf(selectedMinute);
                }
                // Display the 12 hour format time in app interface
                if (hour_of_12_hour_format==0){
                    hour_of_12_hour_format=12;
                    edtTime.setText( hour_of_12_hour_format + " : " + minute + " : " + status );
                }else {
                    edtTime.setText( hour_of_12_hour_format + " : " + minute + " : " + status );
                }

            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    @OnClick(R.id.btnSignIn)
    public void btnSignInnn() {
        btnSignIn.startAnimation(animation);
        if (edtgName.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Kindly select Guardian", Toast.LENGTH_LONG).show();
        } else if (edtTime.getText().toString().equalsIgnoreCase("Set Check Out Time")) {
            Toast.makeText(getApplicationContext(), "Set Check Out Time", Toast.LENGTH_LONG).show();
        }else if (edtReason.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Kindly add Reason", Toast.LENGTH_LONG).show();
        }else {
            commonProgress.show();
            User user=User.getCurrentUser();
            HashMap<String, String> params = new HashMap<>();
            params.put("gate_class", user.getClass_id());
            params.put("gate_section", user.getSection_id());
            params.put("gate_regid", user.getId());
            params.put("gate_sname", User.getCurrentUser().getFirstname()+" "+User.getCurrentUser().getLastname());
            params.put("gate_fname", user.getFather());
            params.put("gate_mobile", user.getPhoneno());
            params.put("gate_personcont", edtgMobile.getText().toString());
            params.put("gate_profile", img);
            params.put("gate_person", edtgName.getText().toString());
            params.put("gate_relation", edtRelation.getText().toString());
            params.put("gate_timeout", edtTime.getText().toString());
            params.put("gate_datetime", am_pm);
            params.put("gate_date", date);
            params.put("gate_reason", edtReason.getText().toString());

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDSTDGATEPASS, apisendmsgCallback, this, params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                Toast.makeText(getApplicationContext(),"Added Successfully!",Toast.LENGTH_LONG).show();
                finish();
            } else {
                 Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    };



    @Override
    protected void onResume() {
        super.onResume();

    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

}