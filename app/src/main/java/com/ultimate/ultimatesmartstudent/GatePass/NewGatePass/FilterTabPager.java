package com.ultimate.ultimatesmartstudent.GatePass.NewGatePass;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ultimate.ultimatesmartstudent.R;

public class FilterTabPager extends FragmentStatePagerAdapter {

    Context context;

    public FilterTabPager(FragmentManager fm, Context nContext) {
        super(fm);
        context = nContext;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PendingGatePassFragment();
            case 1:
                return new VerifiedGatePassFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return context.getString(R.string.pending);
            case 1:
                return context.getString(R.string.verify);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
