package com.ultimate.ultimatesmartstudent.GatePass;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Examination.Activity.ReportCardNew;
import com.ultimate.ultimatesmartstudent.ForCamera.IPickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartstudent.ForCamera.PickResult;
import com.ultimate.ultimatesmartstudent.ForCamera.PickSetup;
import com.ultimate.ultimatesmartstudent.GatePass.NewGatePass.AddGateAdapter;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddGatePersonDetails extends AppCompatActivity implements AddGateAdapter.StudentPro, IPickResult {
    SharedPreferences sharedPreferences;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)TextView txtNorecord;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.add)
    FloatingActionButton add;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    CommonProgress commonProgress;
    ArrayList<GatePassBean> list =new ArrayList<>();
    private AddGateAdapter adapter;
    Animation animation;
    Dialog dialog;
    private Bitmap userBitmapProf;
    EditText edtgName;
    EditText edtRelation;
    EditText edtgMobile;
    CircularImageView image;
    ImageView edit_profile;
    Button btn;
    private int selection;
    String img="",name="",mob="",rel="",gid="",add_permission="";
    @BindView(R.id.txtSub)
    TextView txtSub;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_new);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.add_person_d));
        txtSub.setText(User.getCurrentUser().getFirstname()+""+User.getCurrentUser().getLastname()+" ("+User.getCurrentUser().getId()+")");
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        recyclerView.setLayoutManager(new LinearLayoutManager(AddGatePersonDetails.this));
        adapter = new AddGateAdapter(list, this, this);
        recyclerView.setAdapter(adapter);
        userprofile();
        fetchhwlist(0);

    }

    private void userprofile() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");
        params.put("classid", User.getCurrentUser().getClass_id());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                if (error == null) {
                    try {
                        Log.e("daaaaaaaaaaaaaaa",jsonObject.getJSONObject(Constants.USERDATA).toString());
                        add_permission = jsonObject.getJSONObject(Constants.USERDATA).getString("gatepass");
                        if (add_permission.equalsIgnoreCase("yes")){
                            add.setVisibility(View.VISIBLE);
                            if (!restorePrefDataP()){
                                setShowcaseViewP();
                            }
                        }else{
                            add.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    add.setVisibility(View.GONE);
                    // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(add,"Add Person!","Tap the Add button to add new Person details.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export_person",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export_person",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_person",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export_person",false);
    }

    @OnClick(R.id.imgBackmsg)
    public void onback() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    private void dialog_view(String img, String name, String mob, String rel,String gid, String valueee) {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_person_dialog);
        edtgName = (EditText) dialog.findViewById(R.id.edtgName);
        edtgMobile = (EditText) dialog.findViewById(R.id.edtgMobile);
        edtRelation = (EditText) dialog.findViewById(R.id.edtRelation);
        image = (CircularImageView) dialog.findViewById(R.id.circleimgrecepone);
        edit_profile = (ImageView) dialog.findViewById(R.id.edit_profile);
        btn = (Button) dialog.findViewById(R.id.btnF);
        TextView guardian_mini = (TextView) dialog.findViewById(R.id.guardian_mini);
        userBitmapProf=null;
        if (valueee.equalsIgnoreCase("update")){
            btn.setText(getString(R.string.update));
            guardian_mini.setText(getString(R.string.updae_person));
            edtgName.setText(name);
            edtgMobile.setText(mob);
            edtRelation.setText(rel);
            String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/";
            if (img != null) {
                //Picasso.get().load(path+stulist.get(position).getGate_profile()).placeholder(R.drawable.stud).into(viewHolder.circleimg);
                Utils.progressImg(path+img,image,this);
            } else {
                Picasso.get().load(R.drawable.profile).into(image);
            }
        }
        guardian_mini.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                edit_profile.startAnimation(animation);
                selection=1;
                pick_img();
//                if (CropImage.isExplicitCameraPermissionRequired(AddGatePersonDetails.this)) {
//                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
//                } else {
//                    if (!Utils.checkPermission(AddGatePersonDetails.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                        AddGatePersonDetails.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.READ_PERMISSION);
//                    } else {
//                        CropImage.startPickImageActivity(AddGatePersonDetails.this);
//                    }
//                }
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtgName.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Kindly add Guardian name", Toast.LENGTH_LONG).show();

                } else if (edtgMobile.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Kindly add Guardian mobile", Toast.LENGTH_LONG).show();

                }else if (edtRelation.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Kindly add Guardian relation", Toast.LENGTH_LONG).show();

                } else {
                    if (valueee.equalsIgnoreCase("update")){
                        addPerson(valueee,gid);
                    }else {
                        if (userBitmapProf == null) {
                            Toast.makeText(getApplicationContext(), "Kindly add Guardian image", Toast.LENGTH_LONG).show();
                        } else{
                            addPerson(valueee, gid);
                        }
                    }
                }
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
            }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        //  Log.e("imageView6", String.valueOf(r));
        if (r.getError() == null) {
            //If you want the Bitmap.
            image.setImageBitmap(r.getBitmap());
            userBitmapProf= r.getBitmap();
            image.setVisibility(View.VISIBLE);
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }


    @OnClick(R.id.add)
    public void dialog() {
        dialog_view(img,name,mob,rel,gid,"add");
    }

//    @SuppressLint("MissingSuperCall")
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(AddGatePersonDetails.this, data);
//                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                            .start(AddGatePersonDetails.this);
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    userBitmapProf = Utils.getBitmapFromUri(AddGatePersonDetails.this, resultUri, 2048);
//                    image.setImageBitmap(userBitmapProf);
//
//            }
//        }
//
//    }

    private void addPerson(String valueee,String gid) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("gate_personcont", edtgMobile.getText().toString());
        if (userBitmapProf != null) {
            String encoded = Utils.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 50);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("gate_profile", encoded);
        }
        params.put("gate_person", edtgName.getText().toString());
        params.put("gate_relation", edtRelation.getText().toString());
        params.put("check", "add");
        if (valueee.equalsIgnoreCase("update")){
            params.put("check_view", "update");
            params.put("g_id", gid);
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDSTDGATEPASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                dialog.dismiss();
                if (error == null) {
                    if (valueee.equalsIgnoreCase("update")){
                        Toast.makeText(getApplicationContext(),"Updated Successfully!",Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getApplicationContext(),"Added Successfully!",Toast.LENGTH_LONG).show();
                    }
                    fetchhwlist(5);
                } else {
                    commonProgress.dismiss();
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        }, this, params);

    }


    private void fetchhwlist(int i) {
        if (i==0) {
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "person");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STDGATEPASS_URL, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (list != null) {
                        list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray(Constants.GATEPASS_DATA);
                    list = GatePassBean.parseClassArray(jsonArray);
                    if (list.size() > 0) {
                        adapter.setHQList(list);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(list.size()));
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        adapter.setHQList(list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                adapter.setHQList(list);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    public void StudentProfile(GatePassBean std) {

    }

    @Override
    public void onUpdateCallback(GatePassBean std) {
        gid=std.getId();
        img=std.getGate_profile();
        name=std.getGate_person();
        mob=std.getGate_personcont();
        rel=std.getGate_relation();
        if (add_permission.equalsIgnoreCase("yes")){
            dialog_view(img,name,mob,rel,gid,"update");
        }else{
            Toast.makeText(getApplicationContext(),"Update Guardian details permission not enable, kindly contact with school administration to enable update Guardian details permission.!",Toast.LENGTH_LONG).show();
        }
    }

}