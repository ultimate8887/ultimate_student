package com.ultimate.ultimatesmartstudent.GatePass.NewGatePass;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.GatePass.GatePassAdapter;
import com.ultimate.ultimatesmartstudent.GatePass.GatePassBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PendingGatePassFragment extends Fragment {

    @BindView(R.id.recyclerView11)
    RecyclerView recyclerview;
    String date="";
//    @BindView(R.id.txtTitle)
////    TextView txtTitle;
////    @BindView(R.id.imgBack)
////    ImageView back;
    @BindView(R.id.cal_lyt)
    LinearLayout cal_lyt;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    Animation animation;
    ArrayList<GatePassBean> list;
    private LinearLayoutManager layoutManager;
    private GatePassAdapter adapter;
    String value="";
    @BindView(R.id.cal_text)
    TextView cal_text;
    public String from_date="";
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    public PendingGatePassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_pending_gate_pass, container, false);
        ButterKnife.bind(this,view);
        commonProgress=new CommonProgress(getActivity());
       // txtTitle.setText("Verified Gate-Pass");
    //    cal_lyt.setVisibility(View.VISIBLE);
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        // set animation on recyclerview
//        int resId = R.anim.animate_swipe_left_enter;
//        final LayoutAnimationController controller =
//                AnimationUtils.loadLayoutAnimation(this, resId);
//        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerview.setLayoutManager(layoutManager);
        adapter = new GatePassAdapter(list, getActivity());
        recyclerview.setAdapter(adapter);


        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        from_date = timeStampFormat.format(myDate);
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(myDate);
        cal_text.setText(dateString);

        fetchhwlist("");

        return view;
    }

//    @OnClick(R.id.imgBack)
//    public void backFinish() {
//        back.startAnimation(animation);
//        getActivity().finish();
//    }

    @OnClick(R.id.cal_lyt)
    public void cal_lyttttt(){

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchhwlist(from_date);
        }
    };

    private void fetchhwlist(String from_date) {
       commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("date", from_date);
        params.put("check", "pending");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STDGATEPASS_URL, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (list != null) {
                        list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray(Constants.GATEPASS_DATA);
                    list = GatePassBean.parseClassArray(jsonArray);
                    if (list.size() > 0) {
                        adapter.setGatePassList(list);
                        //setanimation on adapter...
                        recyclerview.getAdapter().notifyDataSetChanged();
                        recyclerview.scheduleLayoutAnimation();
                        //-----------end------------
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(list.size()));
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        adapter.setGatePassList(list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                adapter.setGatePassList(list);
                adapter.notifyDataSetChanged();
                Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

}