package com.ultimate.ultimatesmartstudent.GatePass;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GatePassAdapter extends RecyclerView.Adapter<GatePassAdapter.Viewholder> {
    ArrayList<GatePassBean> list;
    Context listner;

    public GatePassAdapter(ArrayList<GatePassBean> list, Context listener) {
        this.list = list;
        this.listner = listener;
    }

    @Override
    public GatePassAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gate_pass_lyt, parent, false);
        GatePassAdapter.Viewholder viewholder = new GatePassAdapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(GatePassAdapter.Viewholder holder, final int position) {
        final GatePassBean mData = list.get(position);
        holder.txtstuName.setEnabled(false);
        holder.txtReason.setEnabled(false);
        holder.txtcontprsn.setEnabled(false);
        String mclass = "";

        if (mData.getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("gp-"+mData.getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getGate_class() != null)
            mclass = mData.getGate_class();
        if (mData.getGate_mobile() != null) {
            String title = getColoredSpanned("Father's Contact No: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getGate_mobile(), "#000000");
            holder.txtPhone.setText(Html.fromHtml(title + " " + Name));
        }
        if (mData.getGate_fname() != null) {
            String title = getColoredSpanned("Father's Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getGate_fname(), "#000000");
            holder.txtFatherName.setText(Html.fromHtml(title + " " + Name));
        }

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(mData.getGate_date());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Created On: ", "#5A5C59");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Today", "#e31e25");
            holder.txtDT.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Created On: ", "#5A5C59");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Yesterday", "#1C8B3B");
            holder.txtDT.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            String title = getColoredSpanned("Created On: ", "#5A5C59");
            String l_Name = "";
//            if (mData.getGate_datetime().equalsIgnoreCase("Morning")){
//                l_Name = getColoredSpanned(Utils.getDateTimeFormated(mData.getGate_time())+"AM", "#000000");
//                //  holder.txtDT.setText(Utils.getDateTimeFormated(mData.getGate_time())+"AM");
//            }else {
                // holder.txtDT.setText(Utils.getDateTimeFormated(mData.getGate_time())+"PM");
                l_Name = getColoredSpanned(Utils.getDateTimeFormatedWithAMPM(mData.getGate_time()), "#000000");
          //  }

            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");

            holder.txtDT.setText(Html.fromHtml(title + " " + l_Name));
        }


        // holder.txtDT.setText(Utility.getDateTimeFormated(mData.getGate_time()));
        holder.txtReason.setText(mData.getGate_reason());

        if (mData.getGate_sname() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getGate_sname()+"("+mData.getGate_regid()+")", "#000000");
            holder.txtstuName.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getGate_person() != null) {
            holder.txtcontprsn.setVisibility(View.VISIBLE);
            holder.guardian_add.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getGate_person(), "#000000");
            holder.txtcontprsn.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getGate_personcont() != null) {
            holder.txtcontprsnnum.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Contact No: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getGate_personcont(), "#000000");
            holder.txtcontprsnnum.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getGate_relation() != null) {
            holder.txtrelation.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Relation: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getGate_relation(), "#000000");
            holder.txtrelation.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getGate_datetime() != null) {
            holder.txtdandtofcall.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Leave From School: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getGate_datetime()+ " at " + mData.getGate_timeout(), "#1C8B3B");
            holder.txtdandtofcall.setText(Html.fromHtml(title + " " + Name));
        }

        String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/";

        if (mData.getStaff_sign() != null) {
            Picasso.get().load(path+mData.getStaff_sign()).placeholder(R.color.white).into(holder.staff_sign);
            Log.e("chekkkkkkkkkkk",mData.getStaff_sign());
        }else {
            holder.sign_lyt.setVisibility(View.GONE);
        }

        if (mData.getAdmin_sign() != null) {
            Picasso.get().load(path+mData.getAdmin_sign()).placeholder(R.color.white).into(holder.admin_sign);
        }else {
            holder.p_title.setVisibility(View.GONE);
            holder.admin_sign.setVisibility(View.GONE);
        }

        if (mData.getStd_sign() != null) {
            Picasso.get().load(path+mData.getStd_sign()).placeholder(R.color.white).into(holder.std_sign);
        } else {
            holder.sign_lyt.setVisibility(View.GONE);
        }

        if (mData.getGate_profile() != null) {
            Picasso.get().load(path+mData.getGate_profile()).placeholder(R.color.white).into(holder.profile);
        } else {
            holder.profile.setVisibility(View.GONE);
        }

        holder.guardian_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.guardian_add.setVisibility(View.GONE);
                holder.guardian_mini.setVisibility(View.VISIBLE);
                holder.more.setVisibility(View.VISIBLE);
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.guardian_add.setVisibility(View.VISIBLE);
                holder.guardian_mini.setVisibility(View.GONE);
                holder.more.setVisibility(View.GONE);
            }
        });


        if (mData.getVerification().equalsIgnoreCase("done")) {
            holder.rejecteed.setVisibility(View.GONE);
            holder.declineimg.setVisibility(View.GONE);
            holder.aproveimage.setVisibility(View.VISIBLE);
            holder.txtReason.setTextColor(ContextCompat.getColor(listner, R.color.present));

        }else if (mData.getVerification().equalsIgnoreCase("decline")) {
            holder.rejecteed.setVisibility(View.GONE);
            holder.declineimg.setVisibility(View.VISIBLE);
            holder.aproveimage.setVisibility(View.GONE);
            holder.txtReason.setTextColor(ContextCompat.getColor(listner, R.color.new_red));
        }else  {
            holder.rejecteed.setVisibility(View.VISIBLE);
            holder.aproveimage.setVisibility(View.GONE);
            holder.declineimg.setVisibility(View.GONE);
            holder.txtReason.setTextColor(ContextCompat.getColor(listner, R.color.new_red));
        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setGatePassList(ArrayList<GatePassBean> list) {
        this.list = list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.aprvimg)
        ImageView aproveimage;
        @BindView(R.id.declineimg)
        ImageView declineimg;
        @BindView(R.id.rejetedimg)
        ImageView rejecteed;

        @BindView(R.id.txtFatherName)
        TextView txtFatherName;
        @BindView(R.id.txtPhone)
        TextView txtPhone;
        @BindView(R.id.txtDT)
        TextView txtDT;
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.p_title)
        TextView p_title;
        @BindView(R.id.txtABy)
        TextView txtABy;
        @BindView(R.id.txtReason)
        EditText txtReason;
        @BindView(R.id.txtstuName)
        EditText txtstuName;
        @BindView(R.id.txtcontprsn)
        EditText txtcontprsn;
        @BindView(R.id.txtcontprsnnum)
        TextView txtcontprsnnum;
        @BindView(R.id.txtrelation)
        TextView txtrelation;
        @BindView(R.id.txtconfirmationnum)
        TextView txtconfirmationnum;
        @BindView(R.id.txtdandtofcall)
        TextView txtdandtofcall;

        @BindView(R.id.std_sign)
        ImageView std_sign;
        @BindView(R.id.staff_sign)
        ImageView staff_sign;
        @BindView(R.id.admin_sign)
        ImageView admin_sign;

        @BindView(R.id.profile)
        CircularImageView profile;


        @BindView(R.id.sign_lyt)
        CardView sign_lyt;

        @BindView(R.id.more)
        RelativeLayout more;

        @BindView(R.id.guardian_add)
        TextView guardian_add;
        @BindView(R.id.guardian_mini)
        TextView guardian_mini;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
