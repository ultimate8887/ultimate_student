package com.ultimate.ultimatesmartstudent.GatePass.NewGatePass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.GatePass.GatePassAdapter;
import com.ultimate.ultimatesmartstudent.GatePass.GatePassBean;
import com.ultimate.ultimatesmartstudent.Leave_Mod.OptionAdapter;
import com.ultimate.ultimatesmartstudent.Leave_Mod.OptionBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GatePass extends AppCompatActivity {
    @BindView(R.id.recyclerviewlist)
    RecyclerView recyclerview;
    @BindView(R.id.parent)
    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<GatePassBean> list;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    int loaded = 0;
    String tag="all",save="All";
    SharedPreferences sharedPreferences;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.today_date)
    TextView cal_text;
    ArrayList<OptionBean> vehicleList = new ArrayList<>();
    @BindView(R.id.spinerVehicletype)
    Spinner vehicleType;
    CommonProgress commonProgress;
    GatePassAdapter adapter;
    @BindView(R.id.txtTitle)TextView txtTitle;
    public String from_date="";
    private Bitmap admin_bitmap;


    @BindView(R.id.root1)
    RelativeLayout root1;

    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gate_pass);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.gatepass)+" "+getString(R.string.list));
        commonProgress=new CommonProgress(this);
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
//        int resId = R.anim.animate_swipe_left_enter;
//        final LayoutAnimationController controller =
//                AnimationUtils.loadLayoutAnimation(this, resId);
//        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerview.setLayoutManager(layoutManager);
        adapter = new GatePassAdapter(list, this);
        recyclerview.setAdapter(adapter);
        root1.setVisibility(View.VISIBLE);
        vehicleList.add(new OptionBean("Pending"));
        vehicleList.add(new OptionBean("Approved"));
        vehicleList.add(new OptionBean("Decline"));

        OptionAdapter dataAdapter = new OptionAdapter(GatePass.this, vehicleList);
        //set the ArrayAdapter to the spinner
        vehicleType.setAdapter(dataAdapter);
        //attach the listener to the spinner
        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    tag = vehicleList.get(i - 1).getName();
                    if (tag.equalsIgnoreCase("Pending")){
                        tag="pending";
                        save="Check-In";
                    }else if (tag.equalsIgnoreCase("Decline")) {
                        tag = "decline";
                        save = "Decline";
                    }else{
                        tag="verify";
                        save="Check-Out";
                    }
                }else {
                    tag="all";
                    save="All";
                }
                cal_text.setText("Select date");
                fetchhwlist(tag,"");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @OnClick(R.id.imgBack)
    public void backFinish() {
        // back.startAnimation(animation);
        finish();
    }

    @OnClick(R.id.today)
    public void cal_lyttttt() {

        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchhwlist(tag,from_date);
        }
    };

    private void fetchhwlist(String tag, String from_date) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("date", from_date);
        params.put("check", tag);
        params.put("tag", "new");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STDGATEPASS_URL, apiCallback, this, params);
    }


    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (list != null) {
                        list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray(Constants.GATEPASS_DATA);
                    list = GatePassBean.parseClassArray(jsonArray);
                    if (list.size() > 0) {
                        adapter.setGatePassList(list);
                        //setanimation on adapter...
                        recyclerview.getAdapter().notifyDataSetChanged();
                        recyclerview.scheduleLayoutAnimation();
                        totalRecord.setText("Total Entries:- " + String.valueOf(list.size()));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setGatePassList(list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                adapter.setGatePassList(list);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };
}