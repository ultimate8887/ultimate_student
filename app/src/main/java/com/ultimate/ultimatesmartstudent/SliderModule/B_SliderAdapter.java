package com.ultimate.ultimatesmartstudent.SliderModule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.ultimate.ultimatesmartstudent.R;


import java.util.ArrayList;

public class B_SliderAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<NewsData> sliderDataArrayList;
    private LayoutInflater layoutInflater;

    public B_SliderAdapter(Context context, ArrayList<NewsData> sliderDataArrayList) {
        this.context = context;
        this.sliderDataArrayList = sliderDataArrayList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return sliderDataArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.image_slider_layout_itemsec, container, false);
        ImageView imageView = view.findViewById(R.id.myimage);

        // Load image using Glide
        Glide.with(context)
                .load(sliderDataArrayList.get(position).getImage())
                .fitCenter()
                .into(imageView);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
