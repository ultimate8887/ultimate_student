package com.ultimate.ultimatesmartstudent.SyllabusMod;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SyllabusBean {

    private static String IMAGE="image";
    private static String SUB_ID="sub_id";
    private static String S_FILE="s_file";
    private static String S_CONTENT="s_content";
    private static String S_ID="s_id";
    private static String SUB_NAME="sub_name";
    /**
     * sub_id : 32
     * sub_name : HINDI
     * s_content : vcdscfdsdsjklf dslkfj sdfdsksdj gkfdjgfdjkghdfgdfgfd g
     * s_file : office_admin/documents/sy_05152018_080521.pdf
     * s_id : 5
     */

    private String sub_id;
    private String sub_name;
    private String s_content;
    private String s_file;
    private String s_id;
    private String es_date;
    private String classname;

    private ArrayList<String> image;

    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    /**
     * msg : fdgdfg
     */

    private String image_tag;

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getEs_date() {
        return es_date;
    }

    public void setEs_date(String es_date) {
        this.es_date = es_date;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getS_content() {
        return s_content;
    }

    public void setS_content(String s_content) {
        this.s_content = s_content;
    }

    public String getS_file() {
        return s_file;
    }

    public void setS_file(String s_file) {
        this.s_file = s_file;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }
    public static ArrayList<SyllabusBean> parseSyllabusArray(JSONArray arrayObj) {
        ArrayList<SyllabusBean> list = new ArrayList<SyllabusBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                SyllabusBean p = parseSyllabusObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static SyllabusBean parseSyllabusObject(JSONObject jsonObject) {
        SyllabusBean casteObj = new SyllabusBean();
        try {
            if (jsonObject.has(S_ID)&& !jsonObject.getString(S_ID).isEmpty() && !jsonObject.getString(S_ID).equalsIgnoreCase("null")) {
                casteObj.setS_id(jsonObject.getString(S_ID));
            }
            if (jsonObject.has(S_CONTENT) && !jsonObject.getString(S_CONTENT).isEmpty() && !jsonObject.getString(S_CONTENT).equalsIgnoreCase("null")) {
                casteObj.setS_content(jsonObject.getString(S_CONTENT));
            }
            if (jsonObject.has(S_FILE) && !jsonObject.getString(S_FILE).isEmpty() && !jsonObject.getString(S_FILE).equalsIgnoreCase("null")) {
                casteObj.setS_file(Constants.getImageBaseURL()+jsonObject.getString(S_FILE));
            }
            if (jsonObject.has(SUB_NAME) && !jsonObject.getString(SUB_NAME).isEmpty() && !jsonObject.getString(SUB_NAME).equalsIgnoreCase("null")) {
                casteObj.setSub_name(jsonObject.getString(SUB_NAME));
            }

            if (jsonObject.has(IMAGE) && jsonObject.get(IMAGE) instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray(IMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }
            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                casteObj.setImage_tag(jsonObject.getString("image_tag"));
            }

            if (jsonObject.has(SUB_ID) && !jsonObject.getString(SUB_ID).isEmpty() && !jsonObject.getString(SUB_ID).equalsIgnoreCase("null")) {
                casteObj.setSub_id(jsonObject.getString(SUB_ID));
            }
            if (jsonObject.has("date") && !jsonObject.getString("date").isEmpty() && !jsonObject.getString("date").equalsIgnoreCase("null")) {
                casteObj.setEs_date(jsonObject.getString("date"));
            }
            if (jsonObject.has("classname") && !jsonObject.getString("classname").isEmpty() && !jsonObject.getString("classname").equalsIgnoreCase("null")) {
                casteObj.setClassname(jsonObject.getString("classname"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }


    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }
}
