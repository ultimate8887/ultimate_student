package com.ultimate.ultimatesmartstudent.SyllabusMod;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.Classwork.ClassworkadapterNew;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SyllabusAdapter extends RecyclerView.Adapter<SyllabusAdapter.Viewholder> {
    ArrayList<SyllabusBean> hq_list;
    SyllabusAdapter.Mycallback mAdaptercall;
    Context listner;
    Animation animation;

    public SyllabusAdapter(ArrayList<SyllabusBean> hq_list, Context listener, SyllabusAdapter.Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.hq_list = hq_list;
        this.listner = listener;
    }


    @Override
    public SyllabusAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homeada_lay_new, parent, false);
        SyllabusAdapter.Viewholder viewholder = new SyllabusAdapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(final SyllabusAdapter.Viewholder holder, final int position) {

        animation = AnimationUtils.loadAnimation(listner, R.anim.btn_blink_animation);

        String f_type="";
        if (hq_list.get(position).getImage_tag().equalsIgnoreCase("pdf")){
            f_type = "pdf file";
        }else{
            f_type = "image file";
        }
        holder.sub.setText(f_type);


        if (hq_list.get(position).getClassname() != null) {
            String title = getColoredSpanned("Class:-", "#000000");
            String Name="";

            Name = getColoredSpanned(hq_list.get(position).getClassname(), "#5A5C59");

            holder.classess.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.classess.setVisibility(View.GONE);
        }

        if (hq_list.get(position).getS_id() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("sy-"+hq_list.get(position).getS_id(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        holder.homeTopic.setText(hq_list.get(position).getS_content());

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(hq_list.get(position).getEs_date());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getEs_date()));
        }


        holder.nofile.setVisibility(View.VISIBLE);
        holder.audio_file.setVisibility(View.GONE);
        holder.empty_file.setVisibility(View.GONE);

        if (hq_list.get(position).getImage_tag().equalsIgnoreCase("images")){
            holder.pdf_file.setVisibility(View.GONE);
            holder.image_file.setVisibility(View.VISIBLE);
            Picasso.get().load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            holder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.image_file.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethodCallback(hq_list.get(position));
                    }
                }
            });
        }else{
            //   Picasso.get().load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.drawable.classwork)).into(holder.image_file);
            Picasso.get().load(R.drawable.pdf_file).into(holder.pdf_file);
            holder.pdf_file.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            if (hq_list.get(position).getS_file() != null) {
                holder.pdf_file.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.pdf_file.startAnimation(animation);
                        if(mAdaptercall!= null){
                            mAdaptercall.onpdfCallback(hq_list.get(position));
                        }
                    }
                });
            }
        }




    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface Mycallback {
        public void onMethodCallback(SyllabusBean homeworkbean);
        public void onpdfCallback(SyllabusBean homeworkbean);
        public void onUpdateCallback(SyllabusBean homeworkbean);

        public void onDelecallback(SyllabusBean homeworkbean);
    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<SyllabusBean> hq_list) {
        this.hq_list = hq_list;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView u_date,homeTopic,sub;
        public ImageView image_file, audio_file, pdf_file;
        public RelativeLayout withfile;
        @BindView(R.id.nofile)
        RelativeLayout nofile;
        @BindView(R.id.d_lyt)
        RelativeLayout d_lyt;
        @BindView(R.id.empty_file)
        ImageView empty_file;
        @BindView(R.id.classess)
        TextView classess;
        @BindView(R.id.id)
        TextView id;

        public ImageView trash, update;
        @BindView(R.id.tap_view_pdf)
        TextView tap_view_pdf;
        @BindView(R.id.tap_view_pic)
        TextView tap_view_pic;
        @BindView(R.id.tap_view_audio)
        TextView tap_view_audio;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);


        }
    }


}
