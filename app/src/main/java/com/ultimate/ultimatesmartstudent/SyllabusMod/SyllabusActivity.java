package com.ultimate.ultimatesmartstudent.SyllabusMod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.TimetableMod.ViewTimeTable;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;
import com.ultimate.ultimatesmartstudent.Webview.FileDownloader;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class SyllabusActivity extends AppCompatActivity implements SyllabusAdapter.Mycallback {

    Animation animation;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    String folder_main1 = "Syllabus";
    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;

    ArrayList<SyllabusBean> syllabusList = new ArrayList<>();

    @BindView(R.id.textNorecord)
    TextView txtNorecord;

    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    private LinearLayoutManager layoutManager;
    private SyllabusAdapter adapter;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    String check = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_new);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            check = getIntent().getExtras().getString("check");
            if(check.equalsIgnoreCase("month")){
                txtTitle.setText("Monthly Planner List");
            }else{
                txtTitle.setText("Syllabus List");
            }
        }
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
      //  txtTitle.setText(getString(R.string.syllabus)+" "+getString(R.string.list));
        txtSub.setText(Utils.setNaviHeaderClassData());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layoutManager = new LinearLayoutManager(SyllabusActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new SyllabusAdapter(syllabusList, SyllabusActivity.this, this);
        recyclerview.setAdapter(adapter);
        fetchSyllabus();
    }

    private void fetchSyllabus() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("section_id", User.getCurrentUser().getSection_id());
        params.put("check", check);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SYLLABUSGDS, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (syllabusList != null) {
                        syllabusList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("syllabus_data");
                    syllabusList = SyllabusBean.parseSyllabusArray(jsonArray);
                    if (syllabusList.size() > 0) {
                        adapter.setHQList(syllabusList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(syllabusList.size()));

                    } else {
                        adapter.setHQList(syllabusList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                txtNorecord.setVisibility(View.VISIBLE);
                syllabusList.clear();
                adapter.setHQList(syllabusList);
                adapter.notifyDataSetChanged();
            }
        }
    };
    @Override
    public void onMethodCallback(SyllabusBean data) {

        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);


        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(data.getS_content());
        ViewPagerAdapter mAdapter = new  ViewPagerAdapter(this,data.getImage(),"monthly planner");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);

        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.VISIBLE);

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                if(check.equalsIgnoreCase("month")){
                    ArrayList<String> yourArray = new ArrayList<>();
                    yourArray=data.getImage();
                    Intent intent = new Intent(SyllabusActivity.this, IMGGridView.class);
                    intent.putExtra("data", yourArray);
                    intent.putExtra("tag", "Monthly Planner");
                    intent.putExtra("title", getString(R.string.month_plan));
                    intent.putExtra("sub", "month_planner_id:- "+data.getS_id());
                    startActivity(intent);
                }else{
                    ArrayList<String> yourArray = new ArrayList<>();
                    yourArray=data.getImage();
                    Intent intent = new Intent(SyllabusActivity.this, IMGGridView.class);
                    intent.putExtra("data", yourArray);
                    intent.putExtra("tag", "syllabus");
                    intent.putExtra("title", getString(R.string.syllabus));
                    intent.putExtra("sub", "Syllabus_id:- "+data.getS_id());
                    startActivity(intent);
                }

            }
        });

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public void onpdfCallback(SyllabusBean syllabusBean) {
//        Intent intent = new Intent(SyllabusActivity.this, WebViewActivity.class);
//        intent.putExtra("MY_kEY",homeworkbean.getS_file());
//        startActivity(intent);

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        if(check.equalsIgnoreCase("month")){
            title.setText("Monthly Planner");
            Picasso.get().load(R.drawable.planner).into(image);
        }else{
            title.setText("Syllabus");
            Picasso.get().load(R.drawable.ebook).into(image);
        }
        //image.getResources().setDrawable();


        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setText(getString(R.string.view));
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(SyllabusActivity.this, ViewPdfActivity.class);
                intent.putExtra("id", syllabusBean.getS_file());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);

            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        date_wise.setText(getString(R.string.save));
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                //  UltimateProgress.showProgressBar(SyllabusActivity.this,"Please wait...");
                String mUrl=syllabusBean.getS_file();
                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("Sy_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Sy_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getApplicationContext(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();

            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            File path = new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), folder_main1);
            path.mkdir();
            File file = new File(path, folder_main1 + System.currentTimeMillis() + "saved.pdf");
            try{
                file.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, file);
            UltimateProgress.cancelProgressBar();
            return null;
        }
    }


    @Override
    public void onUpdateCallback(SyllabusBean data) {

    }


    @Override
    public void onDelecallback(SyllabusBean homeworkbean) {

    }


    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }
}
