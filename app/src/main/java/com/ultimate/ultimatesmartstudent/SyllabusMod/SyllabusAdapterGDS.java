package com.ultimate.ultimatesmartstudent.SyllabusMod;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SyllabusAdapterGDS extends RecyclerView.Adapter<SyllabusAdapterGDS.Viewholder> {
    ArrayList<SyllabusBean> sy_list;
    Mycallback mAdaptercall;
    Context mContext;
    Animation animation;

    public SyllabusAdapterGDS(ArrayList<SyllabusBean> sy_list, Context mContext, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }


    @Override
    public SyllabusAdapterGDS.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_sylbus_lyt, parent, false);
        SyllabusAdapterGDS.Viewholder viewholder = new SyllabusAdapterGDS.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(SyllabusAdapterGDS.Viewholder holder, final int position) {

        holder.txtPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdaptercall != null) {
                    animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
                    holder.txtPdf.startAnimation(animation);
                    mAdaptercall.viewPdf(sy_list.get(position));
                }
            }
        });
        if (sy_list.get(position).getS_file() != null) {
            holder.txtPdf.setVisibility(View.VISIBLE);
        } else {
            holder.txtPdf.setVisibility(View.GONE);
        }
        if (sy_list.get(position).getS_content() != null) {
            holder.txtContent.setText(Html.fromHtml(sy_list.get(position).getS_content()));
            holder.u_date.setText(Utils.dateFormat(sy_list.get(position).getEs_date()));
        }

    }

    public interface Mycallback {
        public void onMethodCallback(SyllabusBean syllabusBean);
        public void viewPdf(SyllabusBean syllabusBean);
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<SyllabusBean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPdf)
        TextView txtPdf;
        @BindView(R.id.txtContent)
        TextView txtContent;
        @BindView(R.id.u_date)
        TextView u_date;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
