package com.ultimate.ultimatesmartstudent.SplashLoginMod;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.github.siyamed.shapeimageview.CircularImageView;
//import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AccountModule.ChangeLanguageActivity;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {
    //
    @BindView(R.id.logo)
    ImageView logo;
    //    @BindView(R.id.title)
//    TextView title;
//    boolean link = true;
    private Animation animation;
    //    private long SPLASH_TIME_OUT = 3000;
//    ArrayList<Sscolbean> hwList = new ArrayList<>();
//    @BindView(R.id.logo2)ImageView logo2;
    @BindView(R.id.logo3)
    ImageView logo3;

    @BindView(R.id.play)
    LottieAnimationView play;
    Window window;
    User currentUser;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_splash_sceen);
        ButterKnife.bind(this);
        window = this.getWindow();
        window.setStatusBarColor(this.getResources().getColor(R.color.transparent));

        currentUser = User.getCurrentUser();
        //  Log.e("device_token", FirebaseInstanceId.getInstance().getToken());
        animation = AnimationUtils.loadAnimation(this, R.anim.logo_animation);
        logo.startAnimation(animation);
        setSchoolImage();
        Animation top_curve_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.top_down);
        //    logo2.startAnimation(top_curve_anim);
        animation = AnimationUtils.loadAnimation(this, R.anim.enter_from_right_animation);
        Animation center_reveal_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.center_reveal_anim);
        logo3.startAnimation(center_reveal_anim);
        // title.startAnimation(animation);

        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 2000) {
                        sleep(50);
                        waited += 50;
                    }
                    Intent intent = null;

                    if (currentUser != null) {
                        if (currentUser.getId() != null) {
//                            DebugLog.printLog("User_details ","\nStd_Name"+User.getCurrentUser().getFirstname()
//                                    + "\nStd_id_details "+User.getCurrentUser().getId()+"\nName"+User.getCurrentUser().getSchoolData().getName()
//                                    + "\nid_details "+User.getCurrentUser().getSchoolData().getFi_school_id()
//                                    + "\nid_details "+User.getCurrentUser().getSchoolData().getKey());
                            checkLanguage();

                        }else {
                            User.clearUserList();
                            User.logout();
                            intent = new Intent(SplashActivity.this,
                                    LoginActivity.class);

                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            Animatoo.animateShrink(SplashActivity.this);
                            SplashActivity.this.finish();

                        }
                    }else {
                        intent = new Intent(SplashActivity.this,
                                LoginActivity.class);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        Animatoo.animateShrink(SplashActivity.this);
                        SplashActivity.this.finish();

                    }

                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    SplashActivity.this.finish();
                }
            }

        };
        splashTread.start();

    }

    private void checkLanguage() {
//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("setting", MODE_PRIVATE);
//        String lang = sharedPreferences.getString("app_lang", "");
        Intent intent = null;
//        if (lang.equalsIgnoreCase("")) {
////            intent = new Intent(SplashActivity.this,
////                    ChangeLanguageActivity.class);
////            intent.putExtra("tag", "setup");
//            intent = new Intent(SplashActivity.this,
//                    HomeActivity.class);
//        } else {
            intent = new Intent(SplashActivity.this,
                    HomeActivity.class);
       // }
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        Animatoo.animateShrink(SplashActivity.this);
        SplashActivity.this.finish();
        // Toast.makeText(getApplicationContext(),"Language "+check,Toast.LENGTH_SHORT).show();
    }

    private void setSchoolImage() {

        if (currentUser != null && currentUser.getId()!=null && currentUser.getSchoolData() != null) {
            Picasso.get().load(currentUser.getSchoolData().getLogo()).into(logo);
        }else{
            Picasso.get().load(R.drawable.logo).into(logo);
        }

    }
}
