package com.ultimate.ultimatesmartstudent.SplashLoginMod;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.os.BuildCompat;
import androidx.fragment.app.Fragment;
import androidx.multidex.BuildConfig;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.HomeFragments.HomeFrag;
import com.ultimate.ultimatesmartstudent.HomeFragments.NotificationFrag;
import com.ultimate.ultimatesmartstudent.HomeFragments.ToolFrag;
import com.ultimate.ultimatesmartstudent.Messages.AdapterClasses.Message_Adapter;
import com.ultimate.ultimatesmartstudent.Messages.All_Activity.MessageActivity;
import com.ultimate.ultimatesmartstudent.Messages.All_Activity.Sent_MessageActivity;
import com.ultimate.ultimatesmartstudent.Messages.BeanClasses.Message_Bean;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeActivity;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeAdapter;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.UltimateERP;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.FirebaseGetDeviceToken;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import me.relex.circleindicator.CircleIndicator;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

public class HomeActivity extends AppCompatActivity implements TextToSpeech.OnInitListener, NoticeAdapter.Mycallback, Message_Adapter.Mycallback {
    SharedPreferences sharedPreferences;
    ArrayList<Fragment> fragmentList;
    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;
    private final int MY_REQUEST_CODE = 101;
    private SharedPreferences pref;
    private String deviceid;
    private int deviceIdChecked = 0;
    String mobile_name = "", mobile_id = "", online_mobile_id = "", online_mobile_name = "";
    public String date;
    TextToSpeech textToSpeech;
    Dialog n_mBottomSheetDialog;
    RecyclerView notiRV, msgRV;
    ArrayList<NoticeBean> todaynoticeList = new ArrayList<>();
    private NoticeAdapter notiAdapter;
    ArrayList<Message_Bean> messagelist = new ArrayList<>();
    private Message_Adapter message_adapter;
    TextView top_tittle,msg_tittle,top_tittle_view,msg_tittle_view, today_noNoticeData;
    RelativeLayout rt,rt_noti;
    String textholder = "", school = "";
    JSONObject jsonObject;
    Boolean valid = true;
    private static final int NOTIFICATION_PERMISSION_REQUEST_CODE = 1;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mobile_name = Build.MODEL;
        mobile_id = Build.ID;
        school=User.getCurrentUser().getSchoolData().getName();
        checkAppUpdate();
        checkLanguage();
        notification_permission();
       // Toast.makeText(this, "Value check "+User.getCurrentUser().getSchoolData().getName()+ "Value check "+User.getCurrentUser().getSchoolData().getFi_school_id(), Toast.LENGTH_SHORT).show();
        textToSpeech = new TextToSpeech(HomeActivity.this, this);
        fragmentList = new ArrayList<>();
        fragmentList.add(new HomeFrag());
        fragmentList.add(new ToolFrag());
        fragmentList.add(new NotificationFrag());


        viewPager = findViewById(R.id.viewPager);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);

        // Set up ViewPager with an adapter
        BottomViewPagerAdapter adapter1 = new BottomViewPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(adapter1);

        // Link the BottomNavigationView with the ViewPager
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.nav_tool:
                        viewPager.setCurrentItem(1);
                        return true;
                    case R.id.nav_notifications:
                        viewPager.setCurrentItem(2);
                        return true;
                }
                return false;
            }
        });

        // Add page change listener to keep the BottomNavigationView in sync with ViewPager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        bottomNavigationView.setSelectedItemId(R.id.nav_home);
                        break;
                    case 1:
                        bottomNavigationView.setSelectedItemId(R.id.nav_tool);
                        break;
                    case 2:
                        bottomNavigationView.setSelectedItemId(R.id.nav_notifications);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        //    tabLayout.setTabFourIcon(R.drawable.house);
        if (User.getCurrentUser().getSchoolData().getFi_school_id() != null && User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("NMS2024")) {
            getTodayDate();
            getDeviceid();
        } else if (User.getCurrentUser().getSchoolData().getFi_school_id() != null && User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZ")) {
            getTodayDate();
            getDeviceid();
        } else {
            // getTodayDate();
            getDeviceid();
        }
    }

    @SuppressLint("UnsafeOptInUsageError")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void notification_permission() {

        // Check for notification permission
        // Check if the device is running Android 12 or higher
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(HomeActivity.this,
                    Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
                // Log.e(TAG, "User accepted the notifications!");
                // sendNotification(HomePageActivity.this);
            } else if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {

                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle("Notification's Permission!");
                builder.setMessage("Please grant permission to receive notifications.").setCancelable(false).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                        dialog.dismiss();

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                Button positiveButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                Button negativeButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                positiveButton.setTextColor(getResources().getColor(R.color.green));
                negativeButton.setTextColor(getResources().getColor(R.color.blueee));


            } else {
                ActivityCompat.requestPermissions(HomeActivity.this,
                        new String[]{Manifest.permission.POST_NOTIFICATIONS},
                        NOTIFICATION_PERMISSION_REQUEST_CODE);
            }
        }
//        else {
//            // Handle devices running Android versions lower than 12 (Android 11 and below)
//            // You can implement a fallback mechanism here
//            Toast.makeText(this, "Notification permission ask", Toast.LENGTH_SHORT).show();
//        }

    }

    private void fetchDetails() {
        //UltimateProgress.showProgressBar(this,"Please Wait.....!");
        //  commonProgress.show();
        HashMap<String, String> header_param = new HashMap<String, String>();
        header_param.put(Constants.DEVICE_TOKEN, deviceid);
        header_param.put("dbhost", User.getCurrentUser().getSchoolData().getDbhost());
        header_param.put("dbname", User.getCurrentUser().getSchoolData().getDbname());
        header_param.put("dbpass", User.getCurrentUser().getSchoolData().getDbpass());
        header_param.put("dbuser", User.getCurrentUser().getSchoolData().getDbuser());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Details_URL, details_apiCallback, this, header_param);

    }

    ApiHandler.ApiCallback details_apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject_new, ApiHandlerError error) {
            //UltimateProgress.cancelProgressBar();
            // commonProgress.dismiss();
            if (error == null) {

                try {
                    if (User.getUserList() != null) {
                        String user_id = jsonObject_new.getJSONObject(Constants.USERDATA).getString(User.ID);
                        for (int i = 0; i < User.getUserList().size(); i++) {
                            if (user_id != null && user_id.equalsIgnoreCase(User.getUserList().get(i).getId())) {
                                valid = false;
                                break;
                            }
                        }
                        // fetchpermissioncheck();
                    }
                    jsonObject = jsonObject_new.getJSONObject(Constants.USERDATA);

//                    Log.i("dbhost", User.getCurrentUser().getSchoolData().getDbhost());
//                    Log.i("dbname", User.getCurrentUser().getSchoolData().getDbname());
//                    Log.i("dbpass", User.getCurrentUser().getSchoolData().getDbpass());
//                    Log.i("dbuser", User.getCurrentUser().getSchoolData().getDbuser());


                    if (valid) {
                        //  Log.i("dbhost", User.getCurrentUser().getSchoolData().getDbhost());
                        //  User.setCurrentUser(jsonObject);
                        //   Toast.makeText(getApplicationContext(), "User Updated!", Toast.LENGTH_LONG).show();
                        // showSnackBar("Successfully Logged In!");
                        // updateMobileInfo();
                    } else {
                        User.removeUserFromList();
                        User.setCurrentUser(jsonObject);
                        //  Toast.makeText(getApplicationContext(), "User already logged In!", Toast.LENGTH_LONG).show();
                        //  updateMobileInfo();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                // showSnackBar(error.getMessage());
            }
        }

    };


    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        // adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
        openDialog(date);


    }

    @SuppressLint("MissingInflatedId")
    private void openDialog(String date) {
        n_mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.noti_msgimage_lyt, null);
        n_mBottomSheetDialog.setContentView(sheetView);
        n_mBottomSheetDialog.setCancelable(true);
        notiRV = (RecyclerView) sheetView.findViewById(R.id.recyclerView);
        msgRV = (RecyclerView) sheetView.findViewById(R.id.recyclerView1);

        top_tittle = (TextView) sheetView.findViewById(R.id.tittle1);
        msg_tittle = (TextView) sheetView.findViewById(R.id.tittle2);

        top_tittle_view = (TextView) sheetView.findViewById(R.id.tittle1_view);
        msg_tittle_view = (TextView) sheetView.findViewById(R.id.tittle2_view);
        rt=(RelativeLayout) sheetView.findViewById(R.id.rt);
        today_noNoticeData = (TextView) sheetView.findViewById(R.id.textNorecord);
        rt_noti=(RelativeLayout) sheetView.findViewById(R.id.rt_noti);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        notiRV.setLayoutManager(layoutManager1);
        notiAdapter = new NoticeAdapter(todaynoticeList, this, this);
        notiRV.setAdapter(notiAdapter);
        todatNoticeBoardList(date);

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        msgRV.setLayoutManager(layoutManager2);
        message_adapter = new Message_Adapter(messagelist, this, this);
        msgRV.setAdapter(message_adapter);

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                n_mBottomSheetDialog.dismiss();
            }
        });
        top_tittle_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(HomeActivity.this, NoticeActivity.class);
                startActivity(intent);
                n_mBottomSheetDialog.dismiss();
            }
        });
        msg_tittle_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(HomeActivity.this, MessageActivity.class);
                intent.putExtra("Today","Today");
                startActivity(intent);
                n_mBottomSheetDialog.dismiss();
            }
        });


    }

    public void fetchmessagelist(String name,String check) {
        String id=User.getCurrentUser().getId();
        String type="1";
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE + Constants.MSGID + id + Constants.MSGTYPE + type
                + Constants.NAME + name + Constants.DATE + date, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        if (messagelist != null) {
                            messagelist.clear();
                        }
                        JSONArray jsonArray = jsonObject.getJSONArray("msg_data");
                        messagelist = Message_Bean.parseMessageArray(jsonArray);
                        if (messagelist.size() > 0) {
                            message_adapter.setmessageList(messagelist);
                            //setanimation on adapter...
                            msgRV.getAdapter().notifyDataSetChanged();
                            msgRV.scheduleLayoutAnimation();
                            msgRV.setVisibility(View.VISIBLE);
                            rt.setVisibility(View.VISIBLE);
                            String count = String.valueOf(messagelist.size());
                            if (count != null && !count.equalsIgnoreCase("0")) {
                                String title = getColoredSpanned(count, "#e31e25");
                                String Name = getColoredSpanned(" New Messages", "#000000");
                                msg_tittle.setText(Html.fromHtml(title + " " + Name));
                            } else {
                                msg_tittle.setText("Unread Messages");
                            }
                            if (check != null && !check.equalsIgnoreCase("yes")){
                                n_mBottomSheetDialog.show();
                                Window window = n_mBottomSheetDialog.getWindow();
                                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            }
                            //-----------end------------
                            //totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(messagelist.size()));
                        } else {
                            rt.setVisibility(View.GONE);
                            msgRV.setVisibility(View.GONE);
                            //totalRecord.setText(getString(R.string.t_entries)+" 0");
                            message_adapter.setmessageList(messagelist);
                            message_adapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    rt.setVisibility(View.GONE);
                    msgRV.setVisibility(View.GONE);
                    messagelist.clear();
                    message_adapter.setmessageList(messagelist);
                    message_adapter.notifyDataSetChanged();
                    //  Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

                }
            }
        }, this, params);
    }


    private void todatNoticeBoardList(String date) {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "3");
        params.put("today", date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTICE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                        todaynoticeList = NoticeBean.parseNoticetArray(noticeArray);
                        if (todaynoticeList.size() > 0) {
                            notiAdapter.setNoticeList(todaynoticeList);
                            notiAdapter.notifyDataSetChanged();
                            notiRV.setVisibility(View.VISIBLE);
                            today_noNoticeData.setVisibility(View.GONE);
                            rt_noti.setVisibility(View.VISIBLE);
                            String count = String.valueOf(todaynoticeList.size());
                            fetchmessagelist("Unread","yes");
                            if (count != null && !count.equalsIgnoreCase("0")) {
                                String title = getColoredSpanned(count, "#e31e25");
                                String Name = getColoredSpanned(" New Notice", "#000000");
                                top_tittle.setText(Html.fromHtml(title + " " + Name));
                            } else {
                                top_tittle.setText("Today Notice");
                            }
                            n_mBottomSheetDialog.show();
                            Window window = n_mBottomSheetDialog.getWindow();
                            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        } else {
                            rt_noti.setVisibility(View.GONE);
                            notiRV.setVisibility(View.GONE);
                            todaynoticeList.clear();
                            notiAdapter.setNoticeList(todaynoticeList);
                            notiAdapter.notifyDataSetChanged();
                            //    today_noNoticeData.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    fetchmessagelist("Unread","no");
                    rt_noti.setVisibility(View.GONE);
                    notiRV.setVisibility(View.GONE);
                    todaynoticeList.clear();
                    notiAdapter.setNoticeList(todaynoticeList);
                    notiAdapter.notifyDataSetChanged();
                    // today_noNoticeData.setVisibility(View.VISIBLE);

                }
            }
        }, this, params);

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void userprofile() {

        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("image", "");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

                if (error == null) {
                    try {
                        online_mobile_id = jsonObject.getJSONObject(Constants.USERDATA).getString("mobile_id");
                        online_mobile_name = jsonObject.getJSONObject(Constants.USERDATA).getString("mobile_name");
                        Log.i("USERDATA-Home1", online_mobile_id);
                        checkLogout(online_mobile_id);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void checkLogout(String online_mobile_id) {
        if (online_mobile_id != null && online_mobile_id.equalsIgnoreCase("empty")) {
            updateMobileInfo();
        } else {
            if (online_mobile_id != null && online_mobile_id.equalsIgnoreCase(mobile_id)) {
                updateMobileInfo();
            } else {
                Toast.makeText(getApplicationContext(), "Student already login with device " + "'" + online_mobile_name + "', for any query kindly contact with \nSchool Administration", Toast.LENGTH_LONG).show();
                // logoutUser();
            }
        }
    }

    private void checkLanguage() {
        sharedPreferences = getApplicationContext().getSharedPreferences("setting", MODE_PRIVATE);
        String lang = sharedPreferences.getString("app_lang", "");
        if (lang == null || lang.isEmpty()) {
            setLocale("en");
        } else {
            setLocale(lang);
        }
        // Toast.makeText(getApplicationContext(),"Language "+check,Toast.LENGTH_SHORT).show();
    }

    private void setLocale(String language) {

        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        sharedPreferences = getApplicationContext().getSharedPreferences("setting", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("app_lang", language);
        editor.apply();

    }

    private void getDeviceid() {
        pref = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        deviceid = pref.getString(Constants.DEVICE_TOKEN, "");

        if (deviceid.isEmpty()) {
            FirebaseGetDeviceToken.getDeviceToken();
            //  Toast.makeText(getApplicationContext(), FirebaseGetDeviceToken.getDeviceToken(),Toast.LENGTH_LONG).show();
        }
        if (deviceIdChecked != 0) {
            if (deviceIdChecked < 4) {
                checkDeviceIDValid();
            }
        }
    }

    public void checkDeviceIDValid() {
        if (deviceid == null && deviceid.isEmpty()) {
            if (deviceIdChecked < 3) {
                deviceIdChecked++;
                getDeviceid();
            } else {
                //  showSnackBar("Please wait.....");
                FirebaseGetDeviceToken.getDeviceToken();
                //  Toast.makeText(getApplicationContext(), FirebaseGetDeviceToken.getDeviceToken(),Toast.LENGTH_LONG).show();
            }
        } else {
            // registration_sucess();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkAppUpdate();
//
//        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZDEMO")){
//             userprofile();
//        }else{
        updateMobileInfo();
        // }
        fetchDetails();
    }

    private void logoutUser() {
        User.removeUserFromList();
        User.logout();
        if (User.getUserList() == null) {
            Intent loginpage = new Intent(getApplicationContext(), LoginActivity.class);
            loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(loginpage);
            finish();


        } else {
            User.setCurrentUserUL(User.getUserList().get(0));
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private void updateMobileInfo() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());

//        Log.i("TAG", "SERIAL: " + Build.SERIAL);
//        Log.i("TAG","MODEL: " + Build.MODEL);
//        Log.i("TAG","ID: " + Build.ID);
//        Log.i("TAG","Manufacture: " + Build.MANUFACTURER);
//        Log.i("TAG","brand: " + Build.BRAND);
//        Log.i("TAG","type: " + Build.TYPE);
//        Log.i("TAG","user: " + Build.USER);
//        Log.i("TAG","BASE: " + Build.VERSION_CODES.BASE);
//        Log.i("TAG","INCREMENTAL " + Build.VERSION.INCREMENTAL);
//        Log.i("TAG","SDK  " + Build.VERSION.SDK);
//        Log.i("TAG","BOARD: " + Build.BOARD);
//        Log.i("TAG","BRAND " + Build.BRAND);
//        Log.i("TAG","HOST " + Build.HOST);
//        Log.i("TAG","FINGERPRINT: "+Build.FINGERPRINT);
//        Log.i("TAG","Version Code: " + Build.VERSION.RELEASE);
        int code = 0;
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            code = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        params.put(Constants.DEVICE_TOKEN, deviceid);
        params.put("mobile_name", Build.MODEL);
        params.put("mobile_id", Build.ID);
        params.put("manufacture", Build.MANUFACTURER);
        params.put("brand", Build.BRAND);
        params.put("mobile_sdk", Build.VERSION.SDK);
        params.put("fingerprint", Build.FINGERPRINT);
        params.put("mob_version_code", Build.VERSION.RELEASE);
        params.put("install_app_vcode", String.valueOf(code));


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_MOB_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        String mobile_name = jsonObject.getJSONObject(Constants.USERDATA).getString("mobile_name");
                        //   Log.i("USERDATA-Home", mobile_name);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void checkAppUpdate() {
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(HomeActivity.this);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            AppUpdateType.IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }

                // Request the update.
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_REQUEST_CODE) {
            Toast.makeText(getApplicationContext(), "Downloading Start...", Toast.LENGTH_LONG);
            if (requestCode != RESULT_OK) {
                Toast.makeText(getApplicationContext(), "App Updation failed.", Toast.LENGTH_LONG);
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onMethodCallback(NoticeBean obj) {
        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
        // dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imgShow = (ImageView) dialogLog.findViewById(R.id.img);

        Picasso.get().load(obj.getImage()).into(imgShow);
        Button save = (Button) dialogLog.findViewById(R.id.submit);
        save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                // save.startAnimation(animation);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);
            }
        });

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
        Window window = dialogLog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void saveImageToGallery(Bitmap bitmap) {

        OutputStream fos;

        long time = System.currentTimeMillis();
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "noticeBoard_file_saved" + time + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            } else {

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "noticeBoard_file_saved" + time + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        } catch (Exception e) {

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onMethod_Image_callback_new(NoticeBean message_bean) {
        startActivity(new Intent(HomeActivity.this, NoticeActivity.class));
    }

    @Override
    public void onMethod_recording_callback(NoticeBean message_bean) {
        startActivity(new Intent(HomeActivity.this, NoticeActivity.class));
    }

    @Override
    public void onMethod_pdf_call_back(NoticeBean message_bean) {
        startActivity(new Intent(HomeActivity.this, NoticeActivity.class));
    }

    @Override
    public void onImgSpeechCallback(NoticeBean obj) {
        textholder = obj.getTitle() + " !  " + obj.getMsg();
        TextToSpeechFunction();
    }

    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);

    }

    @Override
    public void onDestroy() {

        textToSpeech.shutdown();

        super.onDestroy();
    }


    @Override
    public void onInit(int Text2SpeechCurrentStatus) {

        if (Text2SpeechCurrentStatus == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            TextToSpeechFunction();
        }

    }

    @Override
    public void onMethod_Image_callback(Message_Bean message_bean) {
        Intent intent= new Intent(HomeActivity.this, MessageActivity.class);
        intent.putExtra("Today","Today");
        startActivity(intent);
        if (n_mBottomSheetDialog!=null) {
            n_mBottomSheetDialog.dismiss();
        }
    }

    @Override
    public void onMethod_Image_callback_new(Message_Bean message_bean) {
        openCommonMultiImage(message_bean.getSubject(),message_bean.getMulti_image());
    }

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    String viewwwww = "";

    private void openCommonMultiImage(String subject, ArrayList<String> multiImage) {
        final Dialog warningDialog = new Dialog(HomeActivity.this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView = (TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images = (ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(subject);
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(HomeActivity.this, multiImage, "Message");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        TextView tap_count = (TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>" + "1" + "</b>" + "", "#F4212C");
        String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));

        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);
        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.VISIBLE);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=multiImage;
                Intent intent = new Intent(HomeActivity.this, IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "Message");
                intent.putExtra("title", subject);
                intent.putExtra("sub", "Message details");
                startActivity(intent);
            }
        });

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c = arg0 + 1;
                String title1 = getColoredSpanned("<b>" + String.valueOf(c) + "</b>" + "", "#F4212C");
                String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    @Override
    public void onMethod_recording_callback(Message_Bean message_bean) {
        openCommonVoice(message_bean.getRecordingsfile());
    }

    private MediaPlayer mediaPlayer;
    private Button playButton, pauseButton;
    ImageView play_img, pause_img;
    RelativeLayout play_lyt;
    private SeekBar seekBar;
    private Handler handler = new Handler();
    private TextView currentDurationTextView, totalDurationTextView;
    LottieAnimationView loti_record_play;
    private Button btnRetake, btnPick,btnSave;
    BottomSheetDialog mBottomSheetDialog_play;
    private void openCommonVoice(String recordingsfile) {

        mBottomSheetDialog_play = new BottomSheetDialog(HomeActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.activity_audio, null);
        mBottomSheetDialog_play.setContentView(sheetView);
        mBottomSheetDialog_play.setCancelable(false);

        btnRetake = sheetView.findViewById(R.id.btnRetake);
        btnPick = sheetView.findViewById(R.id.btnPick);
        btnSave = sheetView.findViewById(R.id.btnSave);
        play_lyt = sheetView.findViewById(R.id.play_lyt);
        play_img = sheetView.findViewById(R.id.play_img);
        pause_img = sheetView.findViewById(R.id.pause_img);
        playButton = sheetView.findViewById(R.id.playButton);
        pauseButton = sheetView.findViewById(R.id.pauseButton);
        seekBar = sheetView.findViewById(R.id.seekBar);
        loti_record_play = sheetView.findViewById(R.id.loti_play);
        currentDurationTextView = sheetView.findViewById(R.id.currentDurationTextView);
        totalDurationTextView = sheetView.findViewById(R.id.totalDurationTextView);


        btnSave.setVisibility(View.GONE);
        btnPick.setVisibility(View.GONE);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.seekTo(0);
                seekBar.setProgress(0);
                loti_record_play.pauseAnimation();
            }
        });
        String url=recordingsfile;
        //  Log.d("selectedImageUri",url);
        Uri selectedImageUri = Uri.parse(url);

        try {
            mediaPlayer.setDataSource(HomeActivity.this, selectedImageUri);

            mediaPlayer.prepare();
            mediaPlayer.start();
            updateSeekBar();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        play_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.pause();
                loti_record_play.pauseAnimation();
            }
        });

        pause_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.VISIBLE);
                pause_img.setVisibility(View.GONE);
                mediaPlayer.start();
                updateSeekBar();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                    updateDurationTextView(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        totalDurationTextView.setText(formatDuration(mediaPlayer.getDuration()));

        btnRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonCodeForDssmiss();
            }
        });

        // Dismiss the dialog and reset the MediaPlayer when the dialog is closed
        mBottomSheetDialog_play.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //  Toast.makeText(getApplicationContext(), "Dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog_play.show();
    }

    int starts = 0;

    private void commonCodeForDssmiss() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mBottomSheetDialog_play.dismiss();
    }

    private void updateSeekBar() {
        loti_record_play.playAnimation();
        seekBar.setMax(mediaPlayer.getDuration());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int currentPosition = mediaPlayer.getCurrentPosition();
                    seekBar.setProgress(currentPosition);
                    updateDurationTextView(currentPosition);
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    private void updateDurationTextView(int duration) {
        currentDurationTextView.setText(formatDuration(duration));
    }

    private String formatDuration(int milliseconds) {
        int seconds = (milliseconds / 1000) % 60;
        int minutes = (milliseconds / (1000 * 60)) % 60;
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }


    @Override
    public void onMethod_pdf_call_back(Message_Bean homeworkbean) {
        //   startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(homeworkbean.getPdf_file())));
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(HomeActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(homeworkbean.getSubject());
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.ebook).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        sub_wise.setText("View!");
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getBaseContext(), ViewPdfActivity.class);
                intent.putExtra("id", homeworkbean.getPdf_file());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
        date_wise.setText("Save!");
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                // UltimateProgress.showProgressBar(SubWiseHW.this,"Please wait...");
                String mUrl=homeworkbean.getPdf_file();
                // new DownloadFile().execute(mUrl);
                String path_name ="/storage/Android/data/com.ultimate.tbs_student/files/document/"+"folder_main"+"/Msg_file_saved.pdf";

                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("Msg_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Msg_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getApplicationContext(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }
}
