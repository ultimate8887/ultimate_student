package com.ultimate.ultimatesmartstudent.SplashLoginMod;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AccountModule.ChangeLanguageActivity;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.UltimateERP;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.FirebaseGetDeviceToken;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    Dialog dialogLog;
    JSONObject jsonObject;
    private int VERIFY_NUMBER = 1000;
    @BindView(R.id.edtUsename)
    EditText edtUsename;
    @BindView(R.id.edtSchoolId)
    EditText edtSchoolId;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    String login_mobile_no="", get_number = "";
    @BindView(R.id.inpSchoolId)
    TextInputLayout inpSchoolId;
    @BindView(R.id.inpUsername)
    TextInputLayout inpUsername;
    @BindView(R.id.inpPassword)
    TextInputLayout inpPassword;

    @BindView(R.id.txtResetPass)
    TextView txtResetPass;

    @BindView(R.id.txtWelcome)
    TextView txtWelcome;

    @BindView(R.id.close)
    ImageView close;

    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.tatoo)
    ImageView tatoo;
    @BindView(R.id.txtSigin)
    TextView txtSigin;
    SharedPreferences pref;
    private String deviceid;
    private int deviceIdChecked = 0;
    String id;
    private Animation animation, blink;
    Boolean valid = true;
    CommonProgress commonProgress;
    String mobile_name = "",mobile_id="",std_id="";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        checkLang();
//        edtSchoolId.setText("TSX");
//        edtSchoolId.setEnabled(false);
        mobile_name = Build.MODEL;
        mobile_id = Build.ID;
        animation = AnimationUtils.loadAnimation(this, R.anim.logo_animation);
        blink = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        logo.startAnimation(animation);
        Window window = this.getWindow();
        commonProgress = new CommonProgress(this);
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        window.setStatusBarColor(Color.TRANSPARENT);

        //   window.setStatusBarColor(this.getResources().getColor(R.color.white));
        setSchoolImage();
        Animation top_curve_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_animation);
        txtSigin.startAnimation(top_curve_anim);
        Animation center_reveal_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.center_reveal_anim);
        tatoo.startAnimation(center_reveal_anim);
        getDeviceid();
        Utils.hideKeyboard(this);
    }

    private void checkLang() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("setting", MODE_PRIVATE);
        String lang = sharedPreferences.getString("app_lang", "");

        if (lang!=null || !lang.equalsIgnoreCase("")) {
            inpSchoolId.setHint(getString(R.string.l_key));
            inpUsername.setHint(getString(R.string.l_user));
            inpPassword.setHint(getString(R.string.l_pass));
            txtWelcome.setText(getString(R.string.l_title));
            txtSigin.setText(getString(R.string.l_title1));
            btnSignIn.setText(getString(R.string.l_login));

        } else {
            inpSchoolId.setHint("School Key");
            inpUsername.setHint("Username");
            inpPassword.setHint("Password");
            txtWelcome.setHint("Welcome to Student App,");
            txtSigin.setHint("Sign In to continue");
            btnSignIn.setHint("Sign In");
        }


//        if (User.getCurrentUser()!=null){
//            if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
//                inpSchoolId.setHint("College Key");
//            }else{
//                inpSchoolId.setHint("School Key");
//            }
//        }else{
//            inpSchoolId.setHint("School Key");
//        }
    }

    private void setSchoolImage() {

        if (User.getCurrentUser() != null) {
            if (User.getCurrentUser().getSchoolData().getLogo() != null) {
                //  Toast.makeText(getApplicationContext(),"Image Hai",Toast.LENGTH_LONG).show();
                Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(logo);
                Log.e("Image", User.getCurrentUser().getSchoolData().getLogo());
            } else {
                //   Toast.makeText(getApplicationContext(),"Image Ni Hai",Toast.LENGTH_LONG).show();
                Picasso.get().load(R.drawable.logo).into(logo);
            }
        } else {
            //Toast.makeText(getApplicationContext(),"Image Hai hi nhi",Toast.LENGTH_LONG).show();
            Picasso.get().load(R.drawable.logo).into(logo);
        }
    }

    @OnClick(R.id.close)
    public void close() {
        close.startAnimation(blink);
        finish();
    }

    @OnClick(R.id.txtResetPass)
    public void txtResetPassss() {
        txtResetPass.startAnimation(blink);

    }

    @OnClick(R.id.btnSignIn)
    public void submit() {

        btnSignIn.startAnimation(blink);
        Utils.hideKeyboard(this);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Utils.isConnectionAvailable(LoginActivity.this)) {
            Utils.hideKeyboard(LoginActivity.this);
            if (checkValidation()) {
                checkDeviceIDValid();
            }
        } else
            Toast.makeText(LoginActivity.this, "Please check your network connection.", Toast.LENGTH_LONG).show();

    }

    private void registration_sucess() {
        //UltimateProgress.showProgressBar(this,"Please Wait.....!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.USER_NAME, edtUsename.getText().toString());
        params.put(Constants.PASSWORD, edtPassword.getText().toString());
        params.put(Constants.SCHOOLID, edtSchoolId.getText().toString());
        params.put(Constants.DEVICE_TOKEN, deviceid);
        Log.i("loginURL",Constants.getBaseURL() + Constants.Login_URL);
        //     if(deviceid==null){
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Login_URL, apiCallback, this, params);
//        } else{
//            UltimateProgress.cancelProgressBar();
//            Toast.makeText(LoginActivity.this, "Bye eeeeeee.", Toast.LENGTH_LONG).show();
//        }


    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject_new, ApiHandlerError error) {
            //UltimateProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {

                try {

                    if (User.getUserList() != null) {
                        String user_id = jsonObject_new.getJSONObject(Constants.USERDATA).getString(User.ID);
                        for (int i = 0; i < User.getUserList().size(); i++) {
                            if (user_id.equalsIgnoreCase(User.getUserList().get(i).getId())) {
                                valid = false;
                                break;
                            }
                        }
                        // fetchpermissioncheck();
                    }
                    String School_id ="";

                    if (User.getCurrentUser()!=null){
                        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZDEMO")) {
                            School_id = "XYZDEMO";
                        } else {
                            School_id = "";
                        }
                    }else{
                        School_id = "XYZDEMO";
                    }
                    jsonObject=jsonObject_new.getJSONObject(Constants.USERDATA);


                    if (valid) {
                        User.setCurrentUser(jsonObject);
                        showSnackBar("Successfully Logged In!");
                       // updateMobileInfo();
                    } else {
                        Toast.makeText(getApplicationContext(), "User already logged In!", Toast.LENGTH_LONG).show();
                      //  updateMobileInfo();
                    }

//                    if (School_id.equalsIgnoreCase("XYZDEMO")) {
//                        checkLanguage();
//                    } else {
                        commonCode();
                 //   }
//                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
//                    Animatoo.animateShrink(LoginActivity.this);
//                    startActivity(intent);
//                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                showSnackBar(error.getMessage());
              //  DebugLog.printLog("error", String.valueOf(error));
            }
        }

    };

    private void updateMobileInfo() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        int code = 0;
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            code = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        params.put(Constants.DEVICE_TOKEN, deviceid);
        params.put("mobile_name", Build.MODEL);
        params.put("mobile_id", Build.ID);
        params.put("manufacture", Build.MANUFACTURER);
        params.put("brand", Build.BRAND);
        params.put("mobile_sdk", Build.VERSION.SDK);
        params.put("fingerprint", Build.FINGERPRINT);
        params.put("mob_version_code", Build.VERSION.RELEASE);
        params.put("install_app_vcode", String.valueOf(code));


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_MOB_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        String mobile_name=jsonObject.getJSONObject(Constants.USERDATA).getString("mobile_name");
                        //   Log.i("USERDATA-Home", mobile_name);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    private void checkLanguage() {
        String image_url = null, logout_user = "",login_mobile_name="";
        try {
            image_url = Constants.getImageBaseURL()+jsonObject.getString("profile");
            std_id = jsonObject.getString("id");
            login_mobile_no = jsonObject.getString("phoneno");
            logout_user = jsonObject.getString("firstname");
            login_mobile_name = jsonObject.getString("mobile_id");

            Log.i("std_id",std_id);
            Log.i("login_mobile_no",login_mobile_no);
            Log.i("image_url",image_url);
            Log.i("logout_user",logout_user);
            Log.i("login_mobile_name",login_mobile_name);
            Log.i("mobile_name",mobile_name);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!login_mobile_name.equalsIgnoreCase("empty")) {

                if (login_mobile_name.equalsIgnoreCase(mobile_id)) {

                    commonCode();
                }
                else {
                    dialogLog = new Dialog(this);
                    dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogLog.setCancelable(false);
                    dialogLog.setContentView(R.layout.logout_dialog);
                    dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    TextView txt1 = (TextView) dialogLog.findViewById(R.id.txt1);
                    TextView txt2 = (TextView) dialogLog.findViewById(R.id.txt2);
                    txt1.setText(" " + getString(R.string.stud_verify));

                        String title = getColoredSpanned(""+"Welcome.."+"", "#5A5C59");
                        String title1 = getColoredSpanned("<b>"+" " + logout_user + "!  "+"</b>", "#000000");
                        String Name = getColoredSpanned(getString(R.string.stud_verify_desc), "#5A5C59");
                        String Name1 = getColoredSpanned(login_mobile_no, "#F4212C");
                        txt2.setText(Html.fromHtml(title+ " " + title1+ " " + Name+ " (" + Name1+")"));


                  //  txt2.setText( " " + getString(R.string.stud_verify_desc));

                    CircularImageView img = (CircularImageView) dialogLog.findViewById(R.id.img);


                    try {
                        if (jsonObject.getString("gender").equalsIgnoreCase("Male")) {
                            if (image_url != null) {
                                Picasso.get().load(image_url).placeholder(R.drawable.boy).into(img);
                            } else {
                                Picasso.get().load(R.drawable.boy).into(img);
                            }

                        } else {
                            if (image_url != null) {
                                Picasso.get().load(image_url).placeholder(R.drawable.girl).into(img);
                            } else {
                                Picasso.get().load(R.drawable.girl).into(img);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Button btnNo1 = (Button) dialogLog.findViewById(R.id.btnNo);
                    btnNo1.setText("Exit");
                    btnNo1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogLog.dismiss();
                        }
                    });
                    Button btnYes = (Button) dialogLog.findViewById(R.id.btnYes);
                    btnYes.setText("Verify");
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            open_layout();

                        }
                    });
                    dialogLog.show();
                    Window window = dialogLog.getWindow();
                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }


        } else {

            commonCode();
            // Toast.makeText(getApplicationContext(),"Language "+check,Toast.LENGTH_SHORT).show();
        }


    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void open_layout() {

        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.PhoneBuilder().build());

// Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setTheme(R.style.phoneTheme)
                        .setLogo(R.drawable.logo)
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false,
                                true)
                        .build(),
                VERIFY_NUMBER);

        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        //              Toast.makeText(LoginActivity.this,"SignOut",Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VERIFY_NUMBER) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                get_number = response.getPhoneNumber();
                //  Toast.makeText(LoginActivity.this,"Number "+get_number,Toast.LENGTH_SHORT).show();
                Log.e("number", get_number);
                numberCheck(get_number);
            } else {
                Toast.makeText(getApplicationContext(), "Sign in failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void numberCheck(String get_number) {
        commonProgress.show();
        if (get_number.equalsIgnoreCase("+91"+login_mobile_no)) {
            dialogLog.dismiss();
            Toast.makeText(getApplicationContext(), "Registered Mobile Number Verified Successfully!", Toast.LENGTH_SHORT).show();
            showSnackBar("Registered Mobile Number Verified Successfully!");
            commonCode();
            commonProgress.dismiss();
        } else {
                Toast.makeText(getApplicationContext(),"Registered mobile number not match, kindly verify your registered mobile number"+" (" + login_mobile_no+")", Toast.LENGTH_LONG).show();
                showSnackBar("Registered mobile number not match, kindly verify your registered mobile number"+" (" + login_mobile_no+")");
               commonProgress.dismiss();
        }
//
//        commonProgress.show();
//        HashMap<String, String> params = new HashMap<>();
//        params.put("std_id", std_id);
//        params.put("number", get_number);
//
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.VERIFY_MOBILE, new ApiHandler.ApiCallback() {
//            @Override
//            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//                UltimateProgress.cancelProgressBar();
//                commonProgress.dismiss();
//                if (error == null) {
//                    dialogLog.dismiss();
//                    Toast.makeText(getApplicationContext(), "Registered Mobile Number Verified Successfully!", Toast.LENGTH_SHORT).show();
//                    showSnackBar("Registered Mobile Number Verified Successfully!");
//                    commonCode();
//                    //    open_changePassword();
//                } else {
//                    if (error.getStatusCode() == 401) {
//                      //  Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
//                          Toast.makeText(getApplicationContext(),"Registered mobile number not match, kindly verify your registered mobile number"+" (" + login_mobile_no+")", Toast.LENGTH_LONG).show();
//                        showSnackBar("Registered mobile number not match, kindly verify your registered mobile number"+" (" + login_mobile_no+")");
//                    }
//                }
//            }
//        }, this, params);

    }

    private void commonCode() {

//        if (valid) {
//            User.setCurrentUser(jsonObject);
//            showSnackBar("Successfully Logged In!");
//            updateMobileInfo();
//        } else {
//            Toast.makeText(getApplicationContext(), "User already logged In!", Toast.LENGTH_LONG).show();
//            updateMobileInfo();
//        }

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("setting", MODE_PRIVATE);
        String lang = sharedPreferences.getString("app_lang", "");
        Intent intent = null;
        if (lang != null && !lang.isEmpty()) {
            intent = new Intent(LoginActivity.this, HomeActivity.class);
        } else {
            intent = new Intent(LoginActivity.this, ChangeLanguageActivity.class);
            intent.putExtra("tag", "setup");
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        Animatoo.animateShrink(LoginActivity.this);
        LoginActivity.this.finish();
    }

    private boolean checkValidation() {
        boolean valid = true;
        String errorMsg = null;
        if (edtSchoolId.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter school id";
        } else if (edtUsename.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter username";
        } else if (edtPassword.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter password";
        }
        if (!valid) {
            showSnackBar(errorMsg);
        }
        return valid;
    }

    private void getDeviceid() {
        pref = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        deviceid = pref.getString(Constants.DEVICE_TOKEN, "");
        if (deviceid.isEmpty()) {
            FirebaseGetDeviceToken.getDeviceToken();
            //  Toast.makeText(getApplicationContext(), FirebaseGetDeviceToken.getDeviceToken(),Toast.LENGTH_LONG).show();
        }
        if (deviceIdChecked != 0) {
            if (deviceIdChecked < 4) {
                checkDeviceIDValid();
            }
        }
    }

    public void checkDeviceIDValid() {
        if (deviceid == null && deviceid.isEmpty()) {
            if (deviceIdChecked < 3) {
                deviceIdChecked++;
                getDeviceid();
            } else {
                showSnackBar("Please Wait........!");
                FirebaseGetDeviceToken.getDeviceToken();
                //   Toast.makeText(getApplicationContext(), FirebaseGetDeviceToken.getDeviceToken(),Toast.LENGTH_LONG).show();
            }
        } else {
            // Toast.makeText(LoginActivity.this, deviceid, Toast.LENGTH_LONG).show();
            registration_sucess();
        }
    }

    public void showSnackBar(String errorMsg) {
//        UltimateProgress.cancelProgressBar();
//        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

        Snackbar.make((View) parent.getParent(), errorMsg + "", Snackbar.LENGTH_LONG).show();
    }
}
