package com.ultimate.ultimatesmartstudent.Examination.Activity;

import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Sessionexambean;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Instruction {

    private static String ID="id";
    private static String TITLE="title";


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;
    private String id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static ArrayList<Instruction> parsesessionArray(JSONArray jsonArray) {
        ArrayList<Instruction> list=new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Instruction p = parsesessionobject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    private static Instruction parsesessionobject(JSONObject jsonObject) {
        Instruction casteObj = new Instruction();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(TITLE) && !jsonObject.getString(TITLE).isEmpty() && !jsonObject.getString(TITLE).equalsIgnoreCase("null")) {
                casteObj.setTitle(jsonObject.getString(TITLE));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return casteObj;
    }
}
