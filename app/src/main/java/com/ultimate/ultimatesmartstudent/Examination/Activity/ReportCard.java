package com.ultimate.ultimatesmartstudent.Examination.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AccountModule.SchoolInfo;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Acadmicyrspinadpter;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Exam_detail_result_adapter;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Exmnamespinadapter;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.ReportCardAdapter;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Exambean;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Examdetailbean;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Sessionexambean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportCard extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ArrayList<Exambean> examList = new ArrayList<>();
    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    ArrayList<Examdetailbean> examdetaillist = new ArrayList<>();
    ReportCardAdapter examdetailadapter;
    SharedPreferences sharedPreferences;
    @BindView(R.id.export)
    FloatingActionButton imageView;
    Exmnamespinadapter exmnamespinadapter;
    Acadmicyrspinadpter acadmicyrspinadpter;
    private String examid = "";
    private String acdemicde = "";
    private String exmnm = "";
    private String mother_name = "";
    private Sessionexambean acdemic;
    Spinner spinner_session, spinner_exam;
    BottomSheetDialog mBottomSheetDialog;
    CommonProgress commonProgress;
    Animation animation;
    String logo="",s_sign="",a_sign="",school="",address="",date="";
    int totalScore = 0,totatMarks= 0,final_totatMarks= 0,gradeMarks= 0;
    @BindView(R.id.class_name)
    TextView class_name;
    @BindView(R.id.class_name1)
    TextView class_name1;
    @BindView(R.id.class_name2)
    TextView class_name2;

    @BindView(R.id.roll)
    TextView roll;

    @BindView(R.id.s_name_mother)
    TextView s_name_mother;
    @BindView(R.id.s_name)
    TextView s_name;
    @BindView(R.id.p_name)
    TextView p_name;

    @BindView(R.id.dob)
    TextView dob;

    @BindView(R.id.dob1)
    TextView dob1;
    @BindView(R.id.dob2)
    TextView dob2;

    @BindView(R.id.e_name)
    TextView e_name;

    @BindView(R.id.date)
    TextView p_date;

    @BindView(R.id.txtAffillated)
    TextView txtAffillated;

    @BindView(R.id.txtAdd)
    TextView txtAdd;

    @BindView(R.id.a_year)
    TextView a_year;
    @BindView(R.id.t_score)
    TextView t_score;
    @BindView(R.id.submit)
    TextView submit;
    @BindView(R.id.t_marks)
    TextView t_marks;

    @BindView(R.id.circleimgrecepone)
    CircularImageView circleImgLogo;

    @BindView(R.id.std_profile)
    ImageView std_profile;

    @BindView(R.id.s_signature)
    ImageView s_signature;
    @BindView(R.id.signature)
    ImageView p_signature;
    @BindView(R.id.logo)
    ImageView w_logo;
    @BindView(R.id.txtSchool)
    TextView txtTitle;
    @BindView(R.id.percentage)
    TextView percentagees;

    @BindView(R.id.e_instruction)
    TextView e_instruction;

    @BindView(R.id.term)
    TextView term;

    @BindView(R.id.parent)
    LinearLayout parent;
    @BindView(R.id.layt)
    RelativeLayout layt;
    @BindView(R.id.note)
    RelativeLayout note;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    private RelativeLayout relativeLayout;
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;
    private float scaleFactor = 1.0f;

    Window window;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_exam_wise);
        ButterKnife.bind(this);
//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.transparent));
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        recyclerView.setLayoutManager(new LinearLayoutManager(ReportCard.this));
        examdetailadapter = new ReportCardAdapter(examdetaillist, this);
        recyclerView.setAdapter(examdetailadapter);
        fetchAcademicyearlist();
        search_dialog();
        userprofile();
        getTodayDate();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });

    }


    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");
        params.put("classid", User.getCurrentUser().getClass_id());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                //  ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        Log.e("daaaaaaaaaaaaaaa",jsonObject.getJSONObject(Constants.USERDATA).toString());
                        logo = jsonObject.getJSONObject(Constants.USERDATA).getString("logo");
                        address = jsonObject.getJSONObject(Constants.USERDATA).getString("address");
//                        s_sign = jsonObject.getJSONObject(Constants.USERDATA).getString("s_signature");
                        school = jsonObject.getJSONObject(Constants.USERDATA).getString("name");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void search_dialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.exam_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(false);

        TextView title = (TextView) sheetView.findViewById(R.id.title);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        spinner_session = (Spinner) sheetView.findViewById(R.id.spinner_session);
        spinner_exam = (Spinner) sheetView.findViewById(R.id.spinner_exam);

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                finish();
                mBottomSheetDialog.dismiss();
            }
        });
        Button submitted = (Button) sheetView.findViewById(R.id.submitted);
        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);
                if (checkValid()) {
                    //  setData();
                    fetchexamResults_check();

                }
            }
        });
        mBottomSheetDialog.show();
    }

    public void fetchexamResults_check(){
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id",User.getCurrentUser().getId());
        params.put("check", "report");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Examdetailbyclassstu, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    fetchexamResults();
                } else {
                    commonProgress.dismiss();
                    Toast.makeText(getApplicationContext(),"Report Card not available, kindly contact with school administration for any query.",Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }, this, params);
    }


    public void fetchexamResults(){
        // commonProgress.show();
        String acdexm_year = acdemic.getStart_date() + acdemic.getEnd_date();
        HashMap<String, String> params = new HashMap<>();
        //params.put("grp_id",groupid);
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("section_id", User.getCurrentUser().getSection_id());
        params.put("stu_id",User.getCurrentUser().getId());
        params.put("exam_id",examid);
        params.put("acadmic_year",acdexm_year);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Examdetailbyclassstu, apiCallbackexmdetail, this, params);
    }

    ApiHandler.ApiCallback apiCallbackexmdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                    if(examdetaillist!=null){
                        examdetaillist.clear();
                    }
                    if (examdetaillist != null) {
                        JSONArray jsonArray = jsonObject.getJSONArray("examdetail_data");
                        //classnm=examdeta.getClass_name();
                        examdetaillist = Examdetailbean.parseexamDetal_Array(jsonArray);
                        examdetailadapter.setSyllabusList(examdetaillist);
                        a_sign = examdetaillist.get(0).getA_signature();
                        s_sign = examdetaillist.get(0).getS_signature();
                        mother_name = examdetaillist.get(0).getMother();
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        //  recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        //  txtNorecord.setVisibility(View.GONE);
                        for (int i = 0; i<examdetaillist.size(); i++)
                        {

                            String marks="",s_marks="",score="",grade_sub="";
                            marks=examdetaillist.get(i).getTotal_marks().trim();
                            score=examdetaillist.get(i).getGet_marks().trim();
                            s_marks=examdetaillist.get(i).getTotal_marks().trim();


                            String extract_marks = marks.replaceAll("[^0-9]", "");
                            String extract_score = score.replaceAll("[^0-9]", "");
                            String extract_s_marks = s_marks.replaceAll("[^0-9]", "");

                            //Toast.makeText(getApplicationContext(),extract_score,Toast.LENGTH_SHORT).show();
                            if (marks.equalsIgnoreCase("A")||marks.equalsIgnoreCase("A+")
                                    ||marks.equalsIgnoreCase("B")||marks.equalsIgnoreCase("B+")
                                    ||marks.equalsIgnoreCase("C")||marks.equalsIgnoreCase("C+")
                                    ||marks.equalsIgnoreCase("D")||marks.equalsIgnoreCase("D+")
                                    ||marks.equalsIgnoreCase("E")||marks.equalsIgnoreCase("E+")
                                    ||marks.equalsIgnoreCase("F")||marks.equalsIgnoreCase("F+")
                            ){
                                marks="0";
                            }else {
                                marks=examdetaillist.get(i).getTotal_marks().trim();
                            }

                            totatMarks += Integer.parseInt(marks);

                            if (score.equalsIgnoreCase("A")||score.equalsIgnoreCase("A+")
                                    ||score.equalsIgnoreCase("B")||score.equalsIgnoreCase("B+")
                                    ||score.equalsIgnoreCase("C")||score.equalsIgnoreCase("C+")
                                    ||score.equalsIgnoreCase("D")||score.equalsIgnoreCase("D+")
                                    ||score.equalsIgnoreCase("E")||score.equalsIgnoreCase("E+")
                                    ||score.equalsIgnoreCase("F")||score.equalsIgnoreCase("F+")
                                    ||score.equalsIgnoreCase("NH")||score.equalsIgnoreCase("nh")
                                    ||score.equalsIgnoreCase("NP")||score.equalsIgnoreCase("np")
                                    ||score.equalsIgnoreCase("A1")||score.equalsIgnoreCase("A2")
                                    ||score.equalsIgnoreCase("B1")||score.equalsIgnoreCase("B2")
                                    ||score.equalsIgnoreCase("C1")||score.equalsIgnoreCase("C2")
                                    ||score.equalsIgnoreCase("D1")||score.equalsIgnoreCase("D2")
                                    ||score.equalsIgnoreCase("E1")||score.equalsIgnoreCase("E2")
                                    ||score.equalsIgnoreCase("F1")||score.equalsIgnoreCase("F2")
                            )
                            {

                                if (s_marks.equalsIgnoreCase("A")||s_marks.equalsIgnoreCase("A+")
                                        ||s_marks.equalsIgnoreCase("B")||s_marks.equalsIgnoreCase("B+")
                                        ||s_marks.equalsIgnoreCase("C")||s_marks.equalsIgnoreCase("C+")
                                        ||s_marks.equalsIgnoreCase("D")||s_marks.equalsIgnoreCase("D+")
                                        ||s_marks.equalsIgnoreCase("E")||s_marks.equalsIgnoreCase("E+")
                                        ||s_marks.equalsIgnoreCase("F")||s_marks.equalsIgnoreCase("F+")
                                ){
                                    s_marks="0";
                                }else {
                                    s_marks=examdetaillist.get(i).getTotal_marks().trim();
                                }
                                grade_sub=s_marks;
                                score="0";
                            }else if(score.equalsIgnoreCase("N/A")||score.equalsIgnoreCase("NA")
                                    ||score.equalsIgnoreCase("AB")||score.equalsIgnoreCase("ab")
                                    ||score.equalsIgnoreCase("ABSENT")||score.equalsIgnoreCase("absent")
                                    ||score.equalsIgnoreCase("le")||score.equalsIgnoreCase("leave")){
                                grade_sub="0";
                                score="0";
                            }else {
                                grade_sub="0";
                                score=examdetaillist.get(i).getGet_marks().trim();
                            }

                          //  gradeMarks += Integer.parseInt(grade_sub);
                          //  totalScore += Integer.parseInt(score);
                        }



                        setData();

                    } else {
                        examdetailadapter.setSyllabusList(examdetaillist);
                        examdetailadapter.notifyDataSetChanged();
                        //  txtNorecord.setVisibility(View.VISIBLE);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),exmnm+" Examination Result is not available, \nkindly Check another Examination result",Toast.LENGTH_LONG).show();
                // txtNorecord.setVisibility(View.VISIBLE);
                examdetaillist.clear();
                examdetailadapter.setSyllabusList(examdetaillist);
                examdetailadapter.notifyDataSetChanged();
                finish();
            }
        }
    };

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(imageView,"Export Report Card!","Tap the download button to download Report Card.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export_re",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export_re",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_re",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export_re",false);
    }

    private void setData() {
        parent.setVisibility(View.VISIBLE);
        layt.setVisibility(View.VISIBLE);
        note.setVisibility(View.GONE);
        User data=User.getCurrentUser();

        imageView.setVisibility(View.VISIBLE);

        if (!restorePrefDataP()){
            setShowcaseViewP();
        }


        if (data.getFirstname() != null) {
            String title = getColoredSpanned("<b>"+"Name : "+"</b>", "#000000");
            String Name = getColoredSpanned(data.getFirstname()+" "+data.getLastname(), "#000800");
            s_name.setText(Html.fromHtml(title + " " + Name));
        }

        if (!mother_name.equalsIgnoreCase("")) {
            String title = getColoredSpanned("<b>"+"Mother's Name : "+"</b>", "#000000");
            String Name = getColoredSpanned(mother_name, "#000800");
            s_name_mother.setText(Html.fromHtml(title + " " + Name));
        }

        if (data.getFather() != null) {
            String title = getColoredSpanned("<b>"+"Father's Name : "+"</b>", "#000000");
            String Name = getColoredSpanned(data.getFather(), "#000800");
            p_name.setText(Html.fromHtml(title + " " + Name));
        }

        String start="",end="",str=data.getClass_name();


        if (str.equalsIgnoreCase("1st")){
            start="1";
            end="st";
        }else if (str.equalsIgnoreCase("2nd")){
            start="2";
            end="nd";
        }else if (str.equalsIgnoreCase("3rd")){
            start="3";
            end="rd";
        }else if (str.equalsIgnoreCase("4th")){
            start="4";
            end="th";
        }else if (str.equalsIgnoreCase("5th")){
            start="5";
            end="th";
        }else if (str.equalsIgnoreCase("6th")){
            start="6";
            end="th";
        }else if (str.equalsIgnoreCase("7th")){
            start="7";
            end="th";
        }else if (str.equalsIgnoreCase("8th")){
            start="8";
            end="th";
        }else if (str.equalsIgnoreCase("9th")){
            start="9";
            end="th";
        }else if (str.equalsIgnoreCase("10th")){
            start="10";
            end="th";
        }else {
            start=data.getClass_name();
           // start="";
            end="";
        }


        if (data.getClass_name() != null) {
            String title = getColoredSpanned("<b>"+"Class : "+"</b>", "#000000");
            String Name = getColoredSpanned(start+"", "#000800");
            class_name.setText(Html.fromHtml(title + " " + Name));
            class_name1.setText(end);
            String title1 = getColoredSpanned("", "#000000");
            String Name1 = getColoredSpanned(""+data.getSection_name(), "#000800");
            class_name2.setText(Html.fromHtml(title1 + "" + Name1));

        }

        if (data.getId() != null) {
            String title = "";
            if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS")){
                 title = getColoredSpanned("<b>"+"House No. : "+"</b>", "#000000");
            }else{
                 title = getColoredSpanned("<b>"+"Admission No. : "+"</b>", "#000000");
            }
            String Name = getColoredSpanned(data.getId(), "#000800");
            roll.setText(Html.fromHtml(title + " " + Name));
        }


        String start1="",end1="",str1=Utils.getDayOnly(data.getDob());


        if (str1.equalsIgnoreCase("01")){
            start1="1";
            end1="st";
        }else if (str1.equalsIgnoreCase("02")){
            start1="2";
            end1="nd";
        }else if (str1.equalsIgnoreCase("03")){
            start1="3";
            end1="rd";
        }else if (str1.equalsIgnoreCase("04")){
            start1="4";
            end1="th";
        }else if (str1.equalsIgnoreCase("05")){
            start1="5";
            end1="th";
        }else if (str1.equalsIgnoreCase("06")){
            start1="6";
            end1="th";
        }else if (str1.equalsIgnoreCase("07")){
            start1="7";
            end1="th";
        }else if (str1.equalsIgnoreCase("08")){
            start1="8";
            end1="th";
        }else if (str1.equalsIgnoreCase("09")){
            start1="9";
            end1="th";
        }else if (str1.equalsIgnoreCase("31")){
            start1="31";
            end1="st";
        }else {
            start1=str1;
            // start="";
            end1="th";
        }




        if (data.getDob() != null) {
            String title = getColoredSpanned("<b>"+"DOB : "+"</b>", "#000000");
            String Name = getColoredSpanned(start1, "#000800");
            dob.setText(Html.fromHtml(title + " " + Name));
            dob1.setText(end1);
            String title1 = getColoredSpanned("", "#000000");
            String Name1 = getColoredSpanned(""+Utils.getMonthYearOnly(data.getDob()), "#000800");
            dob2.setText(Html.fromHtml(title1 + "" + Name1));
        }


        //  s_name.setText(data.getFirstname()+" "+data.getLastname());
        //  p_name.setText(data.getFather());
        //  class_name.setText(data.getClass_name());

        // roll.setText(data.getId());
        if (exmnm.equalsIgnoreCase("PERIODIC TEST - 1") || exmnm.equalsIgnoreCase("PERIODIC TEST-1")){
            term.setText("Term - 1");
        }else if (exmnm.equalsIgnoreCase("PERIODIC TEST - 2" ) || exmnm.equalsIgnoreCase("PERIODIC TEST-2")){
            term.setText("Term - 2");
        }else if (exmnm.equalsIgnoreCase("PERIODIC TEST - 3" ) || exmnm.equalsIgnoreCase("PERIODIC TEST-3")){
            term.setText("Term - 3");
        }else{
            term.setText("Term - 1");
        }
        e_name.setText(exmnm+"");

        p_date.setText(Utils.dateFormat(date)+"");

        e_instruction.setText("Grading Scholastic Areas\n" +
                "Minimum 33% marks required for pass\n" +
                "Grade are awarded on 8-point grading scale as follows:");

        school=data.getSchoolData().getName();

        txtTitle.setText(school);

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS")){
            txtAffillated.setText("(Affiliated to CBSE - 1630194)");
        }else {
            txtAffillated.setText("(Affiliated to CBSE, New Delhi)");
        }
        txtAdd.setText(address);

       // final_totatMarks=totatMarks-gradeMarks;
       // t_marks.setText(String.valueOf(totalScore)+"/"+String.valueOf(final_totatMarks));
       // t_score.setText(String.valueOf(totalScore));


        if (acdemic.getStart_date() != null) {
            String title = getColoredSpanned("<b>"+"Academic Year : "+"</b>", "#000000");
            String Name = getColoredSpanned(Utils.getYearOnlyNEW(acdemic.getStart_date())+"-"+Utils.getYearOnlyNEW(acdemic.getEnd_date()), "#000800");
            a_year.setText(Html.fromHtml(title + " " + Name));
        }

        //   a_year.setText("Academic Year : "+acdemic.getStart_date()+"-"+acdemic.getEnd_date());

        if (User.getCurrentUser().getProfile()!=null) {
            Utils.setImageUri(ReportCard.this,User.getCurrentUser().getProfile(),std_profile);
           // Log.i("std_profile",User.getCurrentUser().getProfile());
            //Picasso.get().load(logo).placeholder(R.color.transparent).into(circleImgLogo);
            // w_logo.setColorFilter(ContextCompat.getColor(this, R.color.water_mark), android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            Picasso.get().load(R.color.transparent).into(std_profile);
        }


        if (!logo.equalsIgnoreCase("")) {
            //  Utils.setImageUri(ReportCard.this,logo,w_logo);
            Picasso.get().load(logo).placeholder(R.color.transparent).into(circleImgLogo);
            // w_logo.setColorFilter(ContextCompat.getColor(this, R.color.water_mark), android.graphics.PorterDuff.Mode.MULTIPLY);

        } else {
            Picasso.get().load(R.color.transparent).into(w_logo);
        }
        if (!a_sign.equalsIgnoreCase("")) {
            Utils.setImageUri(ReportCard.this,a_sign,p_signature);
            // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
        } else {
            Picasso.get().load(R.color.transparent).into(p_signature);
        }
        if (!s_sign.equalsIgnoreCase("")) {
            Utils.setImageUri(ReportCard.this,s_sign,s_signature);
            // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
        } else {
            Picasso.get().load(R.color.transparent).into(s_signature);
        }

//        double scoreeee = Double.parseDouble(t_score.getText().toString());
//        double markssss = Double.parseDouble(t_marks.getText().toString());
//
//        double percentage = (scoreeee / markssss) * 100;
//        NumberFormat nm = NumberFormat.getNumberInstance();
//        percentagees.setText(nm.format(percentage)+"%");
        // txtTitle.setText((int) percentage);


    }

    @OnClick(R.id.export)
    public void dialog() {
        submit.startAnimation(animation);
        layt.setVisibility(View.GONE);
        note.setVisibility(View.GONE);
//        View v1 = getWindow().getDecorView().getRootView();
//        v1.setDrawingCacheEnabled(true);
        //     Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
        // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        // v1.setDrawingCacheEnabled(false);

// Assuming the getBitmapFromView method returns a Bitmap object

// Get the width and height of the desired capture area
        int captureWidth = scrollView.getWidth();
        int captureHeight = scrollView.getHeight();

// Get the visible portion of the ScrollView
        int visibleWidth = scrollView.getChildAt(0).getWidth();
        int visibleHeight = scrollView.getChildAt(0).getHeight();

// Adjust the capture width and height if necessary
        if (visibleWidth < captureWidth) {
            captureWidth = visibleWidth;
        }

        if (visibleHeight < captureHeight) {
            captureHeight = visibleHeight;
        }

// Capture the bitmap with adjusted parameters
        Bitmap bitmap = getBitmapFromView(scrollView, captureHeight, captureWidth);

        saveImageToGallery(bitmap);
    }

    @OnClick(R.id.submit)
    public void submit() {
// create bitmap screen capture
        submit.startAnimation(animation);
        layt.setVisibility(View.GONE);
        note.setVisibility(View.GONE);
//        View v1 = getWindow().getDecorView().getRootView();
//        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
        // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        // v1.setDrawingCacheEnabled(false);
        saveImageToGallery(bitmap);
    }

    //create bitmap from the ScrollView
    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }


    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;
        long time= System.currentTimeMillis();
        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "report_card_img" +time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();

            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "report_card_img" +time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);


        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        layt.setVisibility(View.VISIBLE);
        // note.setVisibility(View.VISIBLE);
    }

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;

        if (acdemic == null) {
            valid = false;
            errorMsg = "Select Academic Year";
        }
        else if (examid == null) {
            valid = false;
            errorMsg = "Select Exam";

        }

        if (!valid) {
            //  Utils.showSnackBar(errorMsg, parent);
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

    private void fetchAcademicyearlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, apiCallbackacdemicyear, this, params);
    }

    ApiHandler.ApiCallback apiCallbackacdemicyear = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            commonProgress.dismiss();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("session_data");
                    sessionlist = Sessionexambean.parsesessionArray(jsonArray);
                    acadmicyrspinadpter = new Acadmicyrspinadpter(ReportCard.this, sessionlist);
                    spinner_session.setAdapter(acadmicyrspinadpter);
                    spinner_session.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                acdemic = sessionlist.get(position - 1);
                                acdemicde = Utils.dateFormat(sessionlist.get(position - 1).getStart_date()) +" to "
                                        + Utils.dateFormat(sessionlist.get(position - 1).getEnd_date());
                                fetchexamnamelist();
                            } else {
                                acdemic = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                // Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void fetchexamnamelist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            commonProgress.dismiss();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    exmnamespinadapter = new Exmnamespinadapter(ReportCard.this, examList);
                    spinner_exam.setAdapter(exmnamespinadapter);
                    spinner_exam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                examid = examList.get(position - 1).getId();
                                exmnm = examList.get(position - 1).getExamname();
                            } else {
                                examid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                //Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };
}