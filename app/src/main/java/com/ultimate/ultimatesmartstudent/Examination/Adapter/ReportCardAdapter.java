package com.ultimate.ultimatesmartstudent.Examination.Adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.AnualresultBean;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Examdetailbean;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportCardAdapter extends RecyclerView.Adapter<ReportCardAdapter.Viewholder> {
    ArrayList<Examdetailbean> sy_list;
    Context mContext;

    public ReportCardAdapter(ArrayList<Examdetailbean> sy_list, Context mContext) {
        this.sy_list = sy_list;
        this.mContext = mContext;
    }


    @Override
    public ReportCardAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_lyt_new, parent, false);
        ReportCardAdapter.Viewholder viewholder = new ReportCardAdapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(ReportCardAdapter.Viewholder holder, final int position) {
        Examdetailbean mData=sy_list.get(position);
        String grade="",marks="";
        marks=mData.getGet_marks().trim();
        grade=mData.getGrade();

        if (mData.getGet_marks() != null) {

            String inputString = mData.getGet_marks();
            boolean containsChar =containsCharacter(inputString);
            if (containsChar) {
                holder.score.setText(marks);
              //  Log.i("String me character","String me character hai.");
            } else {
                if (mData.getGrade() != null) {
                    holder.score.setText(grade);
                } else{
                    holder.score.setText("----");
                }
              //  Log.i("String me character","String me character nahi hai.");
            }
            holder.marks.setText(marks);
        } else{
            holder.marks.setText("Not Mentioned");
        }



        if (mData.getGet_marks() != null) {
            holder.subject.setText((CharSequence) mData.getSubject_name());
        } else{
            holder.subject.setText("Not Mentioned");
        }

    }

    public boolean containsCharacter(String str) {
        if (str != null && !str.isEmpty()) {
            for (int i = 0; i < str.length(); i++) {
                if (Character.isLetter(str.charAt(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<Examdetailbean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.subject)
        TextView subject;
        @BindView(R.id.marks)
        TextView marks;
        @BindView(R.id.score)
        TextView score;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
