package com.ultimate.ultimatesmartstudent.Examination.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.AnualresultBean;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnualResultAdapterGDS  extends RecyclerView.Adapter<AnualResultAdapterGDS.Viewholder> {
    ArrayList<AnualresultBean> sy_list;
    Mycallback mAdaptercall;
    Context mContext;

    public AnualResultAdapterGDS(ArrayList<AnualresultBean> sy_list, Context mContext, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }


    @Override
    public AnualResultAdapterGDS.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.syllabusgds_adpt_lyt, parent, false);
        AnualResultAdapterGDS.Viewholder viewholder = new AnualResultAdapterGDS.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(AnualResultAdapterGDS.Viewholder holder, final int position) {

        holder.txtPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdaptercall != null) {
                    mAdaptercall.viewPdf(sy_list.get(position));
                }
            }
        });
        if (sy_list.get(position).getS_file() != null) {
            holder.txtPdf.setVisibility(View.VISIBLE);
        } else {
            holder.txtPdf.setVisibility(View.GONE);
        }
        if (sy_list.get(position).getS_content() != null) {
            holder.txtContent.setText(Html.fromHtml(sy_list.get(position).getS_content()));

        }

    }

    public interface Mycallback {
        public void onMethodCallback(AnualresultBean syllabusBean);

        public void viewPdf(AnualresultBean syllabusBean);
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<AnualresultBean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPdf)
        TextView txtPdf;
        @BindView(R.id.txtContent)
        TextView txtContent;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
