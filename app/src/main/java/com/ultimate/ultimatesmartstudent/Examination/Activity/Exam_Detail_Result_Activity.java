package com.ultimate.ultimatesmartstudent.Examination.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Acadmicyrspinadpter;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Exam_detail_result_adapter;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Exmnamespinadapter;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Hall_ticket_adapter;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Instruction_Adapter;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Exambean;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Examdetailbean;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Sessionexambean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Exam_Detail_Result_Activity extends AppCompatActivity {
    Animation animation;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.spinner2)
    Spinner SubjectSpinner;
    @BindView(R.id.profile)
    LinearLayout profile;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;
    @BindView(R.id.recyclerView12)
    RecyclerView recyclerViewInst;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<Instruction> instructions = new ArrayList<>();
    private ArrayList<Exambean> examList = new ArrayList<>();
    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    ArrayList<Examdetailbean> examdetaillist = new ArrayList<>();
    Hall_ticket_adapter examdetailadapter;
    Instruction_Adapter instruction_adapter;
    Exmnamespinadapter exmnamespinadapter;
    Acadmicyrspinadpter acadmicyrspinadpter;
    private String examid = "";
    private String acdemicde = "";
    private String exmnm = "";
    private String classnm = "";
    private Sessionexambean acdemic;
    Spinner spinner_session, spinner_exam;
    BottomSheetDialog mBottomSheetDialog;
    CommonProgress commonProgress;
    @BindView(R.id.imgStud)
    CircularImageView imgStud;
    @BindView(R.id.txtStudName)
    TextView txtStudName;
    @BindView(R.id.class_name)
    TextView session;

    @BindView(R.id.instruction)
    TextView instruction;

    @BindView(R.id.instructiont)
    TextView instructiont;

    @BindView(R.id.lytHeader)
    LinearLayout lytHeader;


    @BindView(R.id.roll_no)
    TextView exam;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.stunm)
    TextView stunm;
    @BindView(R.id.classnm)
    TextView clasname;
    @BindView(R.id.examnam)
    TextView examnaname;
    @BindView(R.id.acdmicyr)
    TextView academicyr;

    @BindView(R.id.school)
    TextView school;
    @BindView(R.id.school_card)
    TextView school_card;

    String viewwwww = "";
    String img = "", name = "", mob = "", rel = "", gid = "", add_permission = "";
    SharedPreferences sharedPreferences;
    @BindView(R.id.export)
    FloatingActionButton imageView;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);
        // txtTitle.setText("Check Exam Details");

        imgProfile.setVisibility(View.GONE);
        SubjectSpinner.setVisibility(View.GONE);
        commonProgress = new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);

        viewwwww = getIntent().getStringExtra("view");
        // txtTitle.setText("Exam "+viewwwww);
        txtTitle.setText(getString(R.string.reportcard));
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerView.setLayoutManager(new LinearLayoutManager(Exam_Detail_Result_Activity.this));
        if (viewwwww.equalsIgnoreCase("Result")) {
            examdetailadapter = new Hall_ticket_adapter(examdetaillist, Exam_Detail_Result_Activity.this, 2);
        } else {
            examdetailadapter = new Hall_ticket_adapter(examdetaillist, Exam_Detail_Result_Activity.this, 1);
        }
        recyclerView.setAdapter(examdetailadapter);


        recyclerViewInst.setLayoutManager(new LinearLayoutManager(Exam_Detail_Result_Activity.this));
        instruction_adapter = new Instruction_Adapter(instructions, Exam_Detail_Result_Activity.this);
        recyclerViewInst.setAdapter(instruction_adapter);

        fetchAcademicyearlist();
        search_dialog();
    }


    private void setData() {
        school_card.setVisibility(View.VISIBLE);
        profile.setVisibility(View.VISIBLE);
        if (User.getCurrentUser().getProfile() != null) {
            Picasso.get().load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(imgStud);
        } else {
            Picasso.get().load(R.drawable.boy).into(imgStud);
        }
        if (User.getCurrentUser().getFirstname() != null && User.getCurrentUser().getClass() != null) {
            txtStudName.setText(User.getCurrentUser().getFirstname() + " (" + User.getCurrentUser().getClass_name() + ")");
        } else {
            txtStudName.setText("");
        }
        if (acdemicde != null) {
            session.setText(acdemicde);
        }
        if (exmnm != null) {
            exam.setText(exmnm);
        }

        stunm.setText(User.getCurrentUser().getFirstname() + " " + User.getCurrentUser().getLastname() + "(" + User.getCurrentUser().getId() + ")");
        clasname.setText(User.getCurrentUser().getClass_name() + "(" + User.getCurrentUser().getSection_name() + ")");
        examnaname.setText(exmnm);
        academicyr.setText(acdemicde);
        school.setText(User.getCurrentUser().getSchoolData().getName());

        String title1 = getColoredSpanned("" + "INSTRUCTION TO BE FOLLOWED:\n" + "", "#383737");
        String Name1 = getColoredSpanned("1. Assessment will be held during school hours.\n"
                + "2. There will be no preparatory holiday.\n"
                + "3. Snacks will be served on the day of assessment at 11 A.M.\n"
                + "4. School will be closed at 1:30 P.M after serving Brunch.\n"
                + "5. Summer Vacation will commence from June 4th, 2023.\n"
                + "6. Result will be shared/ Uploaded on ‘Student Management App’.", "#5A5C59");
        instructiont.setText(Html.fromHtml(title1 + "\n" + "" + " "));
//        instruction.setText("1. Assessment will be held during school hours.\n"
//                + "2. There will be no preparatory holiday.\n"
//                + "3. Snacks will be served on the day of assessment at 11 A.M.\n"
//                + "4. School will be closed at 1:30 P.M after serving Brunch.\n"
//                + "5. Summer Vacation will commence from June 4th, 2023.\n"
//                + "6. Result will be shared/ Uploaded on ‘Student Management App’.");


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    private void search_dialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.exam_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(false);

        TextView title = (TextView) sheetView.findViewById(R.id.title);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        spinner_session = (Spinner) sheetView.findViewById(R.id.spinner_session);
        spinner_exam = (Spinner) sheetView.findViewById(R.id.spinner_exam);

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                finish();
                mBottomSheetDialog.dismiss();
            }
        });
        Button submitted = (Button) sheetView.findViewById(R.id.submitted);
        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);
                if (checkValid()) {
                    setData();
                    userprofile();
                }
            }
        });
        mBottomSheetDialog.show();
    }

    private void userprofile() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");
        params.put("classid", User.getCurrentUser().getClass_id());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                mBottomSheetDialog.dismiss();
                if (error == null) {
                    try {
                        Log.e("daaaaaaaaaaaaaaa", jsonObject.getJSONObject(Constants.USERDATA).toString());
                        add_permission = jsonObject.getJSONObject(Constants.USERDATA).getString("admit_card");
                        if (add_permission.equalsIgnoreCase("yes")) {
                            imageView.setVisibility(View.VISIBLE);
                            if (!restorePrefDataP()) {
                                setShowcaseViewP();
                            }
                            if (viewwwww.equalsIgnoreCase("Result")) {
                                fetchexamResults();
                            } else {
                                fetchexamdetail();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Admit Card not found!", Toast.LENGTH_SHORT).show();
                            txtNorecord.setVisibility(View.VISIBLE);
                            imageView.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    mBottomSheetDialog.dismiss();
                    txtNorecord.setVisibility(View.VISIBLE);
                    imageView.setVisibility(View.GONE);
                    // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @OnClick(R.id.export)
    public void export() {
        imageView.setVisibility(View.GONE);
// create bitmap screen capture
        imageView.startAnimation(animation);
//        View v1 = getWindow().getDecorView().getRootView();
//        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
        // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        // v1.setDrawingCacheEnabled(false);
        saveImageToGallery(bitmap);
    }

    //create bitmap from the ScrollView
    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }


    private void saveImageToGallery(Bitmap bitmap) {

        OutputStream fos;
        long time = System.currentTimeMillis();
        String school = User.getCurrentUser().getSchoolData().getName();
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "Admit_Card" + time + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();

            } else {

                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "Admit_Card" + time + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);
            imageView.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            imageView.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(imageView, "Export Admit Card!", "Tap the download button to download Report Card.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_re", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export_re", true);
        editor.apply();
    }

    private boolean restorePrefDataP() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_re", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export_re", false);
    }


    private void fetchexamdetail() {
        commonProgress.show();
        String acdexm_year = acdemic.getStart_date() + acdemic.getEnd_date();
        HashMap<String, String> params = new HashMap<>();
        //params.put("grp_id",groupid);
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("exam_id", examid);
        params.put("acadmic_year", acdexm_year);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Examdetail, apiCallbackresult, this, params);
    }

    ApiHandler.ApiCallback apiCallbackresult = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                    if (examdetaillist != null) {
                        examdetaillist.clear();
                    }

                    JSONArray jsonArray = jsonObject.getJSONArray("examdetail_data");
                    //classnm=examdeta.getClass_name();
                    examdetaillist = Examdetailbean.parseexamDetal_Array(jsonArray);

                    if (examdetaillist.size() > 0) {
                        examdetailadapter.setexmdetailList(examdetaillist);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                      //  instructiont.setVisibility(View.VISIBLE);
                       // instruction.setVisibility(View.VISIBLE);
                        lytHeader.setVisibility(View.VISIBLE);

                        instruction.setVisibility(View.GONE);
                        fetchInstruction();

                    } else {
                        examdetailadapter.setexmdetailList(examdetaillist);
                        examdetailadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                        instructiont.setVisibility(View.GONE);
                        instruction.setVisibility(View.GONE);
                        lytHeader.setVisibility(View.GONE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                txtNorecord.setVisibility(View.VISIBLE);
                instructiont.setVisibility(View.GONE);
                instruction.setVisibility(View.GONE);
                lytHeader.setVisibility(View.GONE);
                examdetaillist.clear();
                examdetailadapter.setexmdetailList(examdetaillist);
                examdetailadapter.notifyDataSetChanged();

            }
        }
    };

    public void fetchInstruction() {
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.INSTRUCTION, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        if (instructions != null) {
                            instructions.clear();
                        }

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        //classnm=examdeta.getClass_name();
                        instructions = Instruction.parsesessionArray(jsonArray);

                    if (instructions.size() > 0) {
                        instruction_adapter.setSyllabusList(instructions);
                        //setanimation on adapter...
                        recyclerViewInst.getAdapter().notifyDataSetChanged();
                        recyclerViewInst.scheduleLayoutAnimation();
                        //-----------end------------
                        recyclerViewInst.setVisibility(View.VISIBLE);
                        instructiont.setVisibility(View.VISIBLE);

                    } else {
                        instruction_adapter.setSyllabusList(instructions);
                        instruction_adapter.notifyDataSetChanged();
                        recyclerViewInst.setVisibility(View.GONE);
                        instructiont.setVisibility(View.GONE);

                    }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    public void fetchexamResults() {
        commonProgress.show();
        String acdexm_year = acdemic.getStart_date() + acdemic.getEnd_date();
        HashMap<String, String> params = new HashMap<>();
        //params.put("grp_id",groupid);
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("stu_id", User.getCurrentUser().getId());
        params.put("exam_id", examid);
        params.put("acadmic_year", acdexm_year);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Examdetailbyclassstu, apiCallbackexmdetail, this, params);
    }

    ApiHandler.ApiCallback apiCallbackexmdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                    if (examdetaillist != null) {
                        examdetaillist.clear();
                    }

                    JSONArray jsonArray = jsonObject.getJSONArray("examdetail_data");
                    //classnm=examdeta.getClass_name();
                    examdetaillist = Examdetailbean.parseexamDetal_Array(jsonArray);

                    if (examdetaillist.size() > 0) {
                        examdetailadapter.setexmdetailList(examdetaillist);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);

                    } else {
                        examdetailadapter.setexmdetailList(examdetaillist);
                        examdetailadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                txtNorecord.setVisibility(View.VISIBLE);
                examdetaillist.clear();
                examdetailadapter.setexmdetailList(examdetaillist);
                examdetailadapter.notifyDataSetChanged();
            }
        }
    };

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;

        if (acdemic == null) {
            valid = false;
            errorMsg = "Select Academic Year";
        } else if (examid == null) {
            valid = false;
            errorMsg = "Select Exam";

        }

        if (!valid) {
            //  Utils.showSnackBar(errorMsg, parent);
            Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

    private void fetchexamnamelist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    exmnamespinadapter = new Exmnamespinadapter(Exam_Detail_Result_Activity.this, examList);
                    spinner_exam.setAdapter(exmnamespinadapter);
                    spinner_exam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                examid = examList.get(position - 1).getId();
                                exmnm = examList.get(position - 1).getExamname();
                            } else {
                                examid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void fetchAcademicyearlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, apiCallbackacdemicyear, this, params);
    }

    ApiHandler.ApiCallback apiCallbackacdemicyear = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("session_data");
                    sessionlist = Sessionexambean.parsesessionArray(jsonArray);
                    acadmicyrspinadpter = new Acadmicyrspinadpter(Exam_Detail_Result_Activity.this, sessionlist);
                    spinner_session.setAdapter(acadmicyrspinadpter);
                    spinner_session.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                acdemic = sessionlist.get(position - 1);
                                acdemicde = sessionlist.get(position - 1).getStart_date() + " to " + sessionlist.get(position - 1).getEnd_date();
                                fetchexamnamelist();
                            } else {
                                acdemic = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

}