package com.ultimate.ultimatesmartstudent.Examination.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Sessionexambean;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

public class Acadmicyrspinadpter extends BaseAdapter {
    private final ArrayList<Sessionexambean> sessionlist;
    Context scheduleActivity;
    LayoutInflater inflter;
    public Acadmicyrspinadpter(Context scheduleActivity, ArrayList<Sessionexambean> sessionlist) {
        this.sessionlist=sessionlist;
        this.scheduleActivity=scheduleActivity;
        inflter = (LayoutInflater.from(scheduleActivity));
    }


    @Override
    public int getCount() {
        return sessionlist.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Session");
        } else {
            Sessionexambean classObj = sessionlist.get(position - 1);
            label.setText(classObj.getStart_date()+"-"+ classObj.getEnd_date());
        }

        return convertView;
    }
}
