package com.ultimate.ultimatesmartstudent.Examination.BeanClasses;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Sessionexambean {
    private static String ID="id";
    private static String STARTDATE="start_date";
    private static String ENDDATE="end_date";
    /**
     * start_date : 2018-03-01
     * id : 5
     * end_date : 2019-03-31
     */

    private String start_date;
    private String id;
    private String end_date;

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public static ArrayList<Sessionexambean> parsesessionArray(JSONArray jsonArray) {
        ArrayList<Sessionexambean> list=new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Sessionexambean p = parsesessionobject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    private static Sessionexambean parsesessionobject(JSONObject jsonObject) {
        Sessionexambean casteObj = new Sessionexambean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(STARTDATE) && !jsonObject.getString(STARTDATE).isEmpty() && !jsonObject.getString(STARTDATE).equalsIgnoreCase("null")) {
                casteObj.setStart_date(jsonObject.getString(STARTDATE));
            }
            if (jsonObject.has(ENDDATE) && !jsonObject.getString(ENDDATE).isEmpty() && !jsonObject.getString(ENDDATE).equalsIgnoreCase("null")) {
                casteObj.setEnd_date(jsonObject.getString(ENDDATE));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return casteObj;
    }
}
