package com.ultimate.ultimatesmartstudent.Examination.BeanClasses;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AnualresultBean {

    private static String S_ID = "s_id";
    private static String SUB_NAME = "sub_name";
    private static String SUB_ID= "sub_id";
    private static String S_CONTENT = "s_content";
    private static String S_FILE = "s_file";
    /**
     * sub_name : English
     * sub_id : 33
     * s_content : vcdscfdsdsjklf dslkfj sdfdsksdj gkfdjgfdjkghdfgdfgfd g
     fd gd
     f
     g

     df
     g      gfh gfh gfh gfh fgh fgh fg hgf hfg hfg hfg hfg h
     * s_file : office_admin/documents/sy_05152018_080516.pdf
     * s_id : 6
     */

    private String sub_id;
    private String s_content;
    private String s_file;
    private String s_id;
    /**
     * sub_name : PUNJABI
     */

    private String sub_name;


    public static AnualresultBean parseAttendObject(JSONObject jsonObject) {
        AnualresultBean attendObj = new AnualresultBean();
        try {

            if (jsonObject.has(S_CONTENT) && !jsonObject.getString(S_CONTENT).isEmpty() && !jsonObject.getString(S_CONTENT).equalsIgnoreCase("null")) {
                attendObj.setS_content(jsonObject.getString(S_CONTENT));
            }
            if (jsonObject.has(S_FILE) && !jsonObject.getString(S_FILE).isEmpty() && !jsonObject.getString(S_FILE).equalsIgnoreCase("null")) {
                attendObj.setS_file(Constants.getImageBaseURL()+jsonObject.getString(S_FILE));
            }
            if (jsonObject.has(S_ID) && !jsonObject.getString(S_ID).isEmpty() && !jsonObject.getString(S_ID).equalsIgnoreCase("null")) {
                attendObj.setS_id(jsonObject.getString(S_ID));
            }
            if (jsonObject.has(SUB_NAME) && !jsonObject.getString(SUB_NAME).isEmpty() && !jsonObject.getString(SUB_NAME).equalsIgnoreCase("null")) {
                attendObj.setSub_name(jsonObject.getString(SUB_NAME));
            }
            if (jsonObject.has(SUB_ID) && !jsonObject.getString(SUB_ID).isEmpty() && !jsonObject.getString(SUB_ID).equalsIgnoreCase("null")) {
                attendObj.setSub_id(jsonObject.getString(SUB_ID));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return attendObj;
    }

    public static ArrayList<AnualresultBean> parseAttendArray(JSONArray jsonArray) {
        try {

            ArrayList<AnualresultBean> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                AnualresultBean obj = parseAttendObject(jsonArray.getJSONObject(i));
                arrayList.add(obj);
            }
            return arrayList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public String getS_content() {
        return s_content;
    }

    public void setS_content(String s_content) {
        this.s_content = s_content;
    }

    public String getS_file() {
        return s_file;
    }

    public void setS_file(String s_file) {
        this.s_file = s_file;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }
}
