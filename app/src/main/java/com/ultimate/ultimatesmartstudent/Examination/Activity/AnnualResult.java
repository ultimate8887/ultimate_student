package com.ultimate.ultimatesmartstudent.Examination.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.Acadmicyrspinadpter;
import com.ultimate.ultimatesmartstudent.Examination.Adapter.AnualResultAdapterGDS;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.AnualresultBean;
import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Sessionexambean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnnualResult extends AppCompatActivity implements AnualResultAdapterGDS.Mycallback {

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.spinner2)
    Spinner acadmicyrspin;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    private int loaded = 0;
    Acadmicyrspinadpter acadmicyrspinadpter;
    private Sessionexambean acdemic;
    private String acdemicde="",acdexm_year="";
    private LinearLayoutManager layoutManager;
    AnualResultAdapterGDS adapter;
    ArrayList<AnualresultBean> anualresultList = new ArrayList<>();
    Animation animation;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);
        txtTitle.setText("Annual-Result Details");
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        layoutManager = new LinearLayoutManager(AnnualResult.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AnualResultAdapterGDS(anualresultList, AnnualResult.this, this);
        recyclerView.setAdapter(adapter);
        fetchAcademicyearlist();
    }

    private void fetchAcademicyearlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, apiCallbackacdemicyear, this, params);
    }

    ApiHandler.ApiCallback apiCallbackacdemicyear = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("session_data");
                    sessionlist = Sessionexambean.parsesessionArray(jsonArray);
                    acadmicyrspinadpter = new Acadmicyrspinadpter(AnnualResult.this, sessionlist);
                    acadmicyrspin.setAdapter(acadmicyrspinadpter);
                    acadmicyrspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                acdemic = sessionlist.get(position - 1);
                                acdemicde = sessionlist.get(position - 1).getStart_date() + sessionlist.get(position - 1).getEnd_date();
                                acdexm_year = acdemic.getStart_date() + acdemic.getEnd_date();
                            } else {
                                acdemic = null;
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
//                Utils.showSnackBar(error.getMessage(), parent);

            }
        }
    };

    private void fetchexamanualresult() {
        UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id", User.getCurrentUser().getId());
        params.put("acadmic_year",acdexm_year);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SYLLABUSGDS, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (anualresultList != null) {
                        anualresultList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("syllabus_data");
                    anualresultList = AnualresultBean.parseAttendArray(jsonArray);
                    if (anualresultList.size() > 0) {
                        adapter.setSyllabusList(anualresultList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);

                    } else {
                        adapter.setSyllabusList(anualresultList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                txtNorecord.setVisibility(View.VISIBLE);
                anualresultList.clear();
                adapter.setSyllabusList(anualresultList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onMethodCallback(AnualresultBean syllabusBean) {

    }

    @Override
    public void viewPdf(AnualresultBean syllabusBean) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(syllabusBean.getS_file())));
    }
}