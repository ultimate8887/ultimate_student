package com.ultimate.ultimatesmartstudent.Examination.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Exambean;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

public class Exmnamespinadapter extends BaseAdapter {
    private final ArrayList<Exambean> examList;
    Context scheduleActivity;
    LayoutInflater inflter;

    public Exmnamespinadapter(Context scheduleActivity, ArrayList<Exambean> examList) {
        this.scheduleActivity=scheduleActivity;
        this.examList=examList;
        inflter=(LayoutInflater.from(scheduleActivity));
    }

    @Override
    public int getCount() {
        return examList.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Exam");
        } else {
            Exambean classObj = examList.get(position - 1);
            label.setText(classObj.getExamname());
        }

        return convertView;
    }
}
