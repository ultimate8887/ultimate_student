package com.ultimate.ultimatesmartstudent.Examination.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExamWebviewActivity extends AppCompatActivity {
    @BindView(R.id.webviewabout)
    WebView myWebView;
    String s_key, staff_id, class_id, quizid;
    boolean loadingFinished = true;
    boolean redirect = false;
    @BindView(R.id.imgBack)
    ImageView imgback;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    int loaded = 0;
    CommonProgress commonProgress;
    String studentid = "", sub_id = "", sub_name = "", sectionid = "", sectionname = "", classid = "", className = "";
    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;
    BottomSheetDialog mBottomSheetDialog;
    int check = 0;
    Spinner spinnersection;
    Spinner spinnerClass;
    Spinner spinnersubject;

    @BindView(R.id.spinnerstudnt)
    Spinner spinnerstudnt;
    @BindView(R.id.textNorecord)
    TextView textNorecord;

    String link ="",tag="";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_webview);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);

        if (getIntent().getExtras() != null) {
            tag = getIntent().getExtras().getString("name");
        }

        studentid = User.getCurrentUser().getId();
        s_key = User.getCurrentUser().getSchoolData().getFi_school_id();
        class_id=User.getCurrentUser().getClass_id();
        className=User.getCurrentUser().getClass_name();
        sectionid=User.getCurrentUser().getSection_id();
        sectionname=User.getCurrentUser().getSection_name();

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(ExamWebviewActivity.this);
                builder1.setMessage("Are you sure, you want to exit? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
        setReportData();
        setCommonData();

    }
    private void setCommonData() {
        myWebView.setVisibility(View.VISIBLE);
        txtTitle.setText(className + "(" + sectionname + ")");
        // txtSub.setText(sub_name);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            // myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    private void setReportData() {
        myWebView.setVisibility(View.VISIBLE);
        myWebView.requestFocus();
        myWebView.getSettings().setDefaultTextEncodingName("utf-8");
        myWebView.setVerticalScrollBarEnabled(true);
        myWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setLightTouchEnabled(true);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.setWebChromeClient(new WebChromeClient() {
                                         @Override
                                         public void onProgressChanged(WebView view, int newProgress) {
                                             if (newProgress < 100) {
                                                 commonProgress.show();
                                             }
                                             if (newProgress == 100) {
                                                 commonProgress.dismiss();
                                                 textNorecord.setVisibility(View.GONE);
                                             }
                                         }


                                     }
        );

     //   link = "https://ultimatesolutiongroup.com/onlinetest/adminResult.php?s_key=" + s_key +"";

        if (tag.equalsIgnoreCase("half")) {
        if(User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("BFPS")){

//           link="https://ultimatesolutiongroup.com/staff_panel/" +
//                   "app_view_report_card_bfps_term1.php?class_id=8&section_id=1&student_id=221500&s_key=BFPS";

            link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_bfps_term_per1.php?class_id="
                    +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;


        }else if(User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS")) {
            if (className.equalsIgnoreCase("11th") || className.equalsIgnoreCase("12th") ||
                    className.equalsIgnoreCase("11") || className.equalsIgnoreCase("12")){

                link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_termtwelves.php?class_id"
                        +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;

            } else if (className.equalsIgnoreCase("9th") || className.equalsIgnoreCase("10th") ||
                    className.equalsIgnoreCase("9") || className.equalsIgnoreCase("10")) {
                link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_termtens.php?class_id="
                        +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;

            }else{

                if (className.equalsIgnoreCase("nursery") || className.equalsIgnoreCase("lkg") ||
                        className.equalsIgnoreCase("ukg") ){
                    link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_nurs.php?class_id="
                            +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;
                }else if (className.equalsIgnoreCase("1st") || className.equalsIgnoreCase("2nd") ||
                        className.equalsIgnoreCase("3rd")){
                    link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_term_stu4.php?class_id="
                            +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;
                }else{
                    link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_term_stu1.php?class_id="
                            +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;
                }
            }
        }else{
            if (className.equalsIgnoreCase("11th") || className.equalsIgnoreCase("12th") ||
                    className.equalsIgnoreCase("11") || className.equalsIgnoreCase("12")){

                link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_termtwelve.php?class_id="
                        +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;


            } else if (className.equalsIgnoreCase("9th") || className.equalsIgnoreCase("10th") ||
                    className.equalsIgnoreCase("9") || className.equalsIgnoreCase("10")) {

                link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_termten.php?class_id="
                        +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;
            }else{

                link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_term1.php?class_id="
                        +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;
            }
        }

        } else {
            String staff_id=User.getCurrentUser().getId();
            link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_term_stu12.php?class_id="
                    +class_id+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;
        }
        Log.e("result",link);
        myWebView.loadUrl(link);
    }


}