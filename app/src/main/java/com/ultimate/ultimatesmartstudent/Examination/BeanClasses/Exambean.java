package com.ultimate.ultimatesmartstudent.Examination.BeanClasses;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Exambean {
    private static String ID="id";
    private static String EXAMNAME="examname";
    private static String ES_EXAMORDBY="es_examordby";
    /**
     * id : 1
     * examname : Unit Test
     * es_examordby : 1
     */

    private String id;
    private String examname;
    private String es_examordby;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExamname() {
        return examname;
    }

    public void setExamname(String examname) {
        this.examname = examname;
    }

    public String getEs_examordby() {
        return es_examordby;
    }

    public void setEs_examordby(String es_examordby) {
        this.es_examordby = es_examordby;
    }

    public static ArrayList<Exambean> parseexamArray(JSONArray jsonArray) {
        ArrayList<Exambean> list=new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Exambean p = parseexamObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static Exambean parseexamObject(JSONObject jsonObject) {
        Exambean casteObj = new Exambean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(EXAMNAME) && !jsonObject.getString(EXAMNAME).isEmpty() && !jsonObject.getString(EXAMNAME).equalsIgnoreCase("null")) {
                casteObj.setExamname(jsonObject.getString(EXAMNAME));
            }
            if (jsonObject.has(ES_EXAMORDBY) && !jsonObject.getString(ES_EXAMORDBY).isEmpty() && !jsonObject.getString(ES_EXAMORDBY).equalsIgnoreCase("null")) {
                casteObj.setEs_examordby(jsonObject.getString(ES_EXAMORDBY));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
