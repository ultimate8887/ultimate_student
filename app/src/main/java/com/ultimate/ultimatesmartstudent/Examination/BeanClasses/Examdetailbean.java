package com.ultimate.ultimatesmartstudent.Examination.BeanClasses;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Examdetailbean {
    private static String SUBJECT_ID = "subject_id";
    private static String EXAMDATE = "exam_date";
    private static String EXAM_DURATION = "exam_duration";
    private static String TOTAL_MARKS = "total_marks";
    private static String PASSMARKS = "pass_marks";
    private static String SUBJECT_NAME = "subject_name";
    private static String CLASS_NAME = "class_name";


    /**
     * subject_id : 0
     * exam_date : 0000-00-00 00:00:00
     * exam_duration : 57
     * total_marks : 0
     * pass_marks : 0
     * subject_name : null
     */

    private String subject_id;
    private String exam_date;
    private String exam_duration;
    private String total_marks;
    private String pass_marks;
    private String subject_name;


    private String admin_fname;

    public String getAdmin_fname() {
        return admin_fname;
    }

    public void setAdmin_fname(String admin_fname) {
        this.admin_fname = admin_fname;
    }

    public String getA_signature() {
        return a_signature;
    }

    public void setA_signature(String a_signature) {
        this.a_signature = a_signature;
    }

    public String getSt_firstname() {
        return st_firstname;
    }

    public void setSt_firstname(String st_firstname) {
        this.st_firstname = st_firstname;
    }

    public String getS_signature() {
        return s_signature;
    }

    public void setS_signature(String s_signature) {
        this.s_signature = s_signature;
    }

    private String a_signature;
    private String st_firstname;
    private String s_signature;


    public String getGet_marks() {
        return get_marks;
    }

    public void setGet_marks(String get_marks) {
        this.get_marks = get_marks;
    }

    private String get_marks;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * class_name : NURSERY1
     */

    private String grade;

    private String class_name;
    // private String class_name;

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    //    public String getClass_name() {
//        return class_name;
//    }
//
//    public void setClass_name(String class_name) {
//        this.class_name = class_name;
//    }
    private String mother;

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getExam_date() {
        return exam_date;
    }

    public void setExam_date(String exam_date) {
        this.exam_date = exam_date;
    }

    public String getExam_duration() {
        return exam_duration;
    }

    public void setExam_duration(String exam_duration) {
        this.exam_duration = exam_duration;
    }

    public String getTotal_marks() {
        return total_marks;
    }

    public void setTotal_marks(String total_marks) {
        this.total_marks = total_marks;
    }

    public String getPass_marks() {
        return pass_marks;
    }

    public void setPass_marks(String pass_marks) {
        this.pass_marks = pass_marks;
    }

    public Object getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }


    public static ArrayList<Examdetailbean> parseexamDetal_Array(JSONArray jsonArray) {
        ArrayList<Examdetailbean> list = new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Examdetailbean p = parseexamdetalObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp", e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static Examdetailbean parseexamdetalObject(JSONObject jsonObject) {
        Examdetailbean casteObj = new Examdetailbean();
        try {

            if (jsonObject.has(SUBJECT_ID) && !jsonObject.getString(SUBJECT_ID).isEmpty() && !jsonObject.getString(SUBJECT_ID).equalsIgnoreCase("null")) {
                casteObj.setSubject_id(jsonObject.getString(SUBJECT_ID));
            }
            if (jsonObject.has(EXAMDATE) && !jsonObject.getString(EXAMDATE).isEmpty() && !jsonObject.getString(EXAMDATE).equalsIgnoreCase("null")) {
                casteObj.setExam_date(jsonObject.getString(EXAMDATE));
            }

            if (jsonObject.has(EXAM_DURATION) && !jsonObject.getString(EXAM_DURATION).isEmpty() && !jsonObject.getString(EXAM_DURATION).equalsIgnoreCase("null")) {
                casteObj.setExam_duration(jsonObject.getString(EXAM_DURATION));
            }
            if (jsonObject.has(TOTAL_MARKS) && !jsonObject.getString(TOTAL_MARKS).isEmpty() && !jsonObject.getString(TOTAL_MARKS).equalsIgnoreCase("null")) {
                casteObj.setTotal_marks(jsonObject.getString(TOTAL_MARKS));
            }

            if (jsonObject.has("mother") && !jsonObject.getString("mother").isEmpty() && !jsonObject.getString("mother").equalsIgnoreCase("null")) {
                casteObj.setMother(jsonObject.getString("mother"));
            }


            if (jsonObject.has(PASSMARKS) && !jsonObject.getString(PASSMARKS).isEmpty() && !jsonObject.getString(PASSMARKS).equalsIgnoreCase("null")) {
                casteObj.setPass_marks(jsonObject.getString(PASSMARKS));
            }
            if (jsonObject.has(SUBJECT_NAME) && !jsonObject.getString(SUBJECT_NAME).isEmpty() && !jsonObject.getString(SUBJECT_NAME).equalsIgnoreCase("null")) {
                casteObj.setSubject_name(jsonObject.getString(SUBJECT_NAME));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has("get_marks") && !jsonObject.getString("get_marks").isEmpty() && !jsonObject.getString("get_marks").equalsIgnoreCase("null")) {
                casteObj.setGet_marks(jsonObject.getString("get_marks"));
            }

            if (jsonObject.has("admin_fname") && !jsonObject.getString("admin_fname").isEmpty() && !jsonObject.getString("admin_fname").equalsIgnoreCase("null")) {
                casteObj.setAdmin_fname(jsonObject.getString("admin_fname"));
            }

            if (jsonObject.has("grade") && !jsonObject.getString("grade").isEmpty() && !jsonObject.getString("grade").equalsIgnoreCase("null")) {
                casteObj.setGrade(jsonObject.getString("grade"));
            }

            if (jsonObject.has("a_signature") && !jsonObject.getString("a_signature").isEmpty() && !jsonObject.getString("a_signature").equalsIgnoreCase("null")) {
                casteObj.setA_signature(jsonObject.getString("a_signature"));
            }

            if (jsonObject.has("st_firstname") && !jsonObject.getString("st_firstname").isEmpty() && !jsonObject.getString("st_firstname").equalsIgnoreCase("null")) {
                casteObj.setSt_firstname(jsonObject.getString("st_firstname"));
            }

            if (jsonObject.has("s_signature") && !jsonObject.getString("s_signature").isEmpty() && !jsonObject.getString("s_signature").equalsIgnoreCase("null")) {
                casteObj.setS_signature(jsonObject.getString("s_signature"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }
}
