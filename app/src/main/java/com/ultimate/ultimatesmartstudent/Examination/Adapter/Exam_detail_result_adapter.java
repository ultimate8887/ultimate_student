package com.ultimate.ultimatesmartstudent.Examination.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.Examination.BeanClasses.Examdetailbean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Exam_detail_result_adapter extends RecyclerView.Adapter<Exam_detail_result_adapter.Viewholder> {
    ArrayList<Examdetailbean> examdetaillist;
    Context context;
    int type;
    public Exam_detail_result_adapter(ArrayList<Examdetailbean> examdetaillist, Context context, int type) {
        this.context=context;
        this.examdetaillist=examdetaillist;
        this.type=type;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exmdetailada_lay_new, parent, false);
        Exam_detail_result_adapter.Viewholder viewholder = new Exam_detail_result_adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        Examdetailbean mData=examdetaillist.get(position);

        holder.subname.setText((CharSequence) examdetaillist.get(position).getSubject_name());
        holder.doe.setText(Utils.getDateFormated(examdetaillist.get(position).getExam_date()));


        String title1 = getColoredSpanned("Duration: ", "#383737");
        String Name1 = getColoredSpanned(examdetaillist.get(position).getExam_duration(), "#5A5C59");
        String t1 = getColoredSpanned("Hour", "#5A5C59");
        holder.textexamduration.setText(Html.fromHtml(title1 + " " + Name1 + " " + t1));

        String title = getColoredSpanned("Total Marks: ", "#383737");
        String Name = getColoredSpanned(examdetaillist.get(position).getTotal_marks(), "#5A5C59");
        holder.exmfullmrks.setText(Html.fromHtml(title + " " + Name));
        //  holder.exmfullmrks.setText("Total Marks:" + " " + examdetaillist.get(position).getTotal_marks());
        // holder.exmpassmrksss.setText("Pass Marks:" + " "+ examdetaillist.get(position).getPass_marks());
        String title2 = getColoredSpanned("Pass Marks: ", "#383737");
        String Name2 = getColoredSpanned(examdetaillist.get(position).getPass_marks(), "#5A5C59");
        holder.exmpassmrksss.setText(Html.fromHtml(title2 + " " + Name2));
//
////        holder.textexamduration.setText("Duration:" + " " + examdetaillist.get(position).getExam_duration());
////        holder.exmfullmrks.setText("Total Marks:" + " " + examdetaillist.get(position).getTotal_marks());
////        holder.exmpassmrksss.setText("Pass Marks:" + " " + examdetaillist.get(position).getPass_marks());
//
//        if (mData.getExam_duration() != null) {
//            String title = getColoredSpanned("Duration: ", "#5A5C59");
//            String Name = getColoredSpanned(mData.getExam_duration(), "#7D7D7D");
//            holder.textexamduration.setText(Html.fromHtml(title + " " + Name));
//        }
//
//        if (mData.getTotal_marks() != null) {
//            String title = getColoredSpanned("Total Marks: ", "#5A5C59");
//            String Name = getColoredSpanned(mData.getTotal_marks(), "#7D7D7D");
//            holder.exmfullmrks.setText(Html.fromHtml(title + " " + Name));
//        }
//
//        if (mData.getPass_marks() != null) {
//            String title = getColoredSpanned("Pass Marks: ", "#5A5C59");
//            String Name = getColoredSpanned(mData.getPass_marks(), "#7D7D7D");
//            holder.exmpassmrksss.setText(Html.fromHtml(title + " " + Name));
//        }
//
//        if(type==1) {
//            holder.exmobtainmrksss.setVisibility(View.GONE);
//            holder.txt2.setVisibility(View.GONE);
//            holder.txt4.setVisibility(View.GONE);
//        } else{
//            holder.txt2.setVisibility(View.VISIBLE);
//
//           // holder.exmobtainmrksss.setText("Obtained Marks: "+examdetaillist.get(position).getGet_marks());
//
//            if (mData.getGet_marks() != null) {
//                String title = getColoredSpanned("Congratulation! ", "#e31e25");
//                String Name = getColoredSpanned("You Have Scored", "#7D7D7D");
//                holder.txt2.setText(Html.fromHtml(title + " " + Name));
//                holder.exmobtainmrksss.setVisibility(View.VISIBLE);
//                holder.exmobtainmrksss.setText(mData.getGet_marks());
//                holder.txt4.setVisibility(View.VISIBLE);
//            } else{
//                holder.exmobtainmrksss.setVisibility(View.GONE);
//                holder.txt4.setVisibility(View.GONE);
//                String title = getColoredSpanned("Ooopss! ", "#e31e25");
//                String Name = getColoredSpanned("Marks Not Uploaded!", "#7D7D7D");
//                holder.txt2.setText(Html.fromHtml(title + " " + Name));
//            }
//
//
//        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return examdetaillist.size();
    }

    public void setexmdetailList(ArrayList<Examdetailbean> examdetaillist) {
        this.examdetaillist = examdetaillist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
//        @BindView(R.id.textsubjectnm)
//        TextView subname;
//        @BindView(R.id.textdateofexm)TextView doe;
//        @BindView(R.id.textexamduration)TextView textexamduration;
//        @BindView(R.id.exmfullmrks)TextView exmfullmrks;
//        @BindView(R.id.exmpassmrksss)TextView exmpassmrksss;
//        @BindView(R.id.exmobtainmrksss)TextView exmobtainmrksss;
//        @BindView(R.id.txt3)TextView txt2;
//        @BindView(R.id.txt4)TextView txt4;
           @BindView(R.id.textsubjectnm)
            TextView subname;
        @BindView(R.id.textdateofexm)TextView doe;
        @BindView(R.id.textexamduration)TextView textexamduration;
        @BindView(R.id.exmfullmrks)TextView exmfullmrks;
        @BindView(R.id.exmpassmrksss)TextView exmpassmrksss;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
