package com.ultimate.ultimatesmartstudent.SchoolActivity;


import com.ultimate.ultimatesmartstudent.BeanModule.CommonBean;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActivityStudentBean {

    private static String ID = "id";
    private static String NAME = "name";
    private static String ANAME = "es_activity_name";
    private static String FROM_DATE = "from_date";
    private static String TO_DATE = "to_date";
    private static String VENUE = "venue";
    private static String VENUE_ADD = "venue_address";

    private static String F_NAME = "father_name";
    private static String CLASS_NAME = "class_name";
    private static String PHONE = "mobile";
    private static String PROFILE = "profile";
    private static String ATTENDANCE = "attendance";

    public String getEs_activity_name() {
        return es_activity_name;
    }

    public void setEs_activity_name(String es_activity_name) {
        this.es_activity_name = es_activity_name;
    }

    /**
     * id : 2
     * name : NURSERY
     * school_id : 0
     * group_id : 9
     * order_by : 1
     */


    private String es_activity_name;

    private String es_groupsid;
    private String es_groupname;

    private String es_activityid;
    private String staff_id;

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    private String staff_name;

    public String getEs_groupsid() {
        return es_groupsid;
    }

    public void setEs_groupsid(String es_groupsid) {
        this.es_groupsid = es_groupsid;
    }

    public String getEs_groupname() {
        return es_groupname;
    }

    public void setEs_groupname(String es_groupname) {
        this.es_groupname = es_groupname;
    }

    public String getEs_activityid() {
        return es_activityid;
    }

    public void setEs_activityid(String es_activityid) {
        this.es_activityid = es_activityid;
    }


    public String getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(String staff_id) {
        this.staff_id = staff_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    private String id;
    private String name;

    private String from_date;
    private String to_date;
    private String venue;

    private String class_name;
    private String father_name;
    private String mobile;
    private String profile;
    private String gender;
    private String section_name;


    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }



    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getVenue_address() {
        return venue_address;
    }

    public void setVenue_address(String venue_address) {
        this.venue_address = venue_address;
    }

    private String venue_address;

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    private String student_id;

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    private String attendance;

    /**
     * group_data : {"id":"1","name":"Pre-Primary(Nry-Prep2)"}
     */

    public CommonBean getGroup_data() {
        return group_data;
    }

    public void setGroup_data(CommonBean group_data) {
        this.group_data = group_data;
    }

    private CommonBean group_data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public static ArrayList<ActivityStudentBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<ActivityStudentBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                ActivityStudentBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ActivityStudentBean parseClassObject(JSONObject jsonObject) {
        ActivityStudentBean casteObj = new ActivityStudentBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(ANAME) && !jsonObject.getString(ANAME).isEmpty() && !jsonObject.getString(ANAME).equalsIgnoreCase("null")) {
                casteObj.setEs_activity_name(jsonObject.getString(ANAME));
            }
            if (jsonObject.has(FROM_DATE) && !jsonObject.getString(FROM_DATE).isEmpty() && !jsonObject.getString(FROM_DATE).equalsIgnoreCase("null")) {
                casteObj.setFrom_date(jsonObject.getString(FROM_DATE));
            }
            if (jsonObject.has(TO_DATE) && !jsonObject.getString(TO_DATE).isEmpty() && !jsonObject.getString(TO_DATE).equalsIgnoreCase("null")) {
                casteObj.setTo_date(jsonObject.getString(TO_DATE));
            }
            if (jsonObject.has(VENUE) && !jsonObject.getString(VENUE).isEmpty() && !jsonObject.getString(VENUE).equalsIgnoreCase("null")) {
                casteObj.setVenue(jsonObject.getString(VENUE));
            }
            if (jsonObject.has(VENUE_ADD) && !jsonObject.getString(VENUE_ADD).isEmpty() && !jsonObject.getString(VENUE_ADD).equalsIgnoreCase("null")) {
                casteObj.setVenue_address(jsonObject.getString(VENUE_ADD));
            }

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(F_NAME) && !jsonObject.getString(F_NAME).isEmpty() && !jsonObject.getString(F_NAME).equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString(F_NAME));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(PHONE) && !jsonObject.getString(PHONE).isEmpty() && !jsonObject.getString(PHONE).equalsIgnoreCase("null")) {
                casteObj.setMobile(jsonObject.getString(PHONE));
            }
            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString(PROFILE));
            }

            if (jsonObject.has("staff_name") && !jsonObject.getString("staff_name").isEmpty() && !jsonObject.getString("staff_name").equalsIgnoreCase("null")) {
                casteObj.setStaff_name(jsonObject.getString("staff_name"));
            }

            if (jsonObject.has("staff_id") && !jsonObject.getString("staff_id").isEmpty() && !jsonObject.getString("staff_id").equalsIgnoreCase("null")) {
                casteObj.setStaff_id(jsonObject.getString("staff_id"));
            }

            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }

            if (jsonObject.has("date") && !jsonObject.getString("date").isEmpty() && !jsonObject.getString("date").equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString("date"));
            }
            if (jsonObject.has("es_activityid") && !jsonObject.getString("es_activityid").isEmpty() && !jsonObject.getString("es_activityid").equalsIgnoreCase("null")) {
                casteObj.setEs_activityid(jsonObject.getString("es_activityid"));
            }

            if (jsonObject.has("es_groupsid") && !jsonObject.getString("es_groupsid").isEmpty() && !jsonObject.getString("es_groupsid").equalsIgnoreCase("null")) {
                casteObj.setEs_groupsid(jsonObject.getString("es_groupsid"));
            }

            if (jsonObject.has(ATTENDANCE) && !jsonObject.getString(ATTENDANCE).isEmpty() && !jsonObject.getString(ATTENDANCE).equalsIgnoreCase("null")) {
                casteObj.setAttendance(jsonObject.getString(ATTENDANCE));
            } else {
                casteObj.setAttendance("N/A");
            }

            if (jsonObject.has("es_groupname") && !jsonObject.getString("es_groupname").isEmpty() && !jsonObject.getString("es_groupname").equalsIgnoreCase("null")) {
                casteObj.setEs_groupname(jsonObject.getString("es_groupname"));
            }
            if (jsonObject.has("student_id") && !jsonObject.getString("student_id").isEmpty() && !jsonObject.getString("student_id").equalsIgnoreCase("null")) {
                casteObj.setStudent_id(jsonObject.getString("student_id"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}

