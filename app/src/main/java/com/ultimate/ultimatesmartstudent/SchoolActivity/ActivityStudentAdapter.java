package com.ultimate.ultimatesmartstudent.SchoolActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityStudentAdapter extends RecyclerView.Adapter<ActivityStudentAdapter.Viewholder> {
    ArrayList<ActivityStudentBean> list;
    Context listner;
    Mycallback mAdaptercall;
    int value;

    public ActivityStudentAdapter(ArrayList<ActivityStudentBean> list, Context listener, Mycallback mAdaptercall, int value) {
        this.list = list;
        this.value = value;
        this.listner = listener;
        this.mAdaptercall = mAdaptercall;
    }

    public interface Mycallback {
        public void onApproveCallback(ActivityStudentBean gatePassBean);

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_std_lyt_new, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(Viewholder holder, @SuppressLint("RecyclerView") final int position) {
        final ActivityStudentBean mData = list.get(position);
        holder.txtstuName.setEnabled(false);
        holder.txtReason.setEnabled(false);
        holder.txtcontprsn.setEnabled(false);
        String mclass = "";

        if (mData.getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("act-" + mData.getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getClass_name() != null)
            mclass = mData.getClass_name();
        if (mData.getMobile() != null) {
            String title = getColoredSpanned("Father's Contact No: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getMobile(), "#000000");
            holder.txtPhone.setText(Html.fromHtml(title + " " + Name));
        }
        if (mData.getFather_name() != null) {
            String title = getColoredSpanned("Father's Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getFather_name(), "#000000");
            holder.txtFatherName.setText(Html.fromHtml(title + " " + Name));
        }


        if (mData.getStudent_id() != null) {
            holder.txtdandtofcall.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Registration No: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getStudent_id(), "#1C8B3B");
            holder.txtdandtofcall.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getAttendance() != null) {
            String title = getColoredSpanned("Attendance: ", "#5A5C59");
            String Name ="";
            if (mData.getAttendance().equalsIgnoreCase("Present")){
                Name = getColoredSpanned("Present", "#1C8B3B");
            }else if (mData.getAttendance().equalsIgnoreCase("Absent")){
                Name = getColoredSpanned("Absent", "#F4212C");
            }else {
                Name = getColoredSpanned(mData.getAttendance(), "#000000");
            }
            holder.txtDT.setText(Html.fromHtml(title + " " + Name));
        } else {
            String title = getColoredSpanned("Attendance: ", "#5A5C59");
            String Name = getColoredSpanned("N/A", "#000000");
            holder.txtDT.setText(Html.fromHtml(title + " " + Name));
        }


        // holder.txtDT.setText(Utility.getDateTimeFormated(mData.getGate_time()));

        if (mData.getFrom_date()!=null){
            holder.txtReason.setText(Utils.getDateFormated(mData.getFrom_date())+" to "+Utils.getDateFormated(mData.getTo_date()));
        }else{
            holder.txtReason.setText("Not Found");
        }



        if (mData.getName() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getName(), "#000000");
            holder.txtstuName.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getEs_groupname() != null) {
            holder.txtcontprsn.setVisibility(View.VISIBLE);
            holder.guardian_add.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Category: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getEs_groupname(), "#000000");
            holder.txtcontprsn.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getEs_activity_name() != null) {
            holder.txtcontprsnnum.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Activity: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getEs_activity_name(), "#000000");
            holder.txtcontprsnnum.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getVenue() != null) {
            holder.txtrelation.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Venue: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getVenue(), "#000000");
            holder.txtrelation.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getVenue_address() != null) {
            holder.txtconfirmationnum.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Venue Address: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getVenue_address(), "#000000");
            holder.txtconfirmationnum.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getStaff_name() != null) {
            holder.txtPhone1.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Activity incharge: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getStaff_name(), "#000000");
            holder.txtPhone1.setText(Html.fromHtml(title + " " + Name));
        }




        if (mData.getProfile() != null) {
            Picasso.get().load(mData.getProfile()).placeholder(R.color.white).into(holder.std_profile);
        } else {
            holder.profile.setVisibility(View.GONE);
        }

        holder.guardian_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.guardian_add.setVisibility(View.GONE);
                holder.guardian_mini.setVisibility(View.VISIBLE);
                holder.more.setVisibility(View.VISIBLE);
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.guardian_add.setVisibility(View.VISIBLE);
                holder.guardian_mini.setVisibility(View.GONE);
                holder.more.setVisibility(View.GONE);
            }
        });


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setGatePassList(ArrayList<ActivityStudentBean> list) {
        this.list = list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtFatherName)
        TextView txtFatherName;
        @BindView(R.id.txtPhone)
        TextView txtPhone;
        @BindView(R.id.txtPhone1)
        TextView txtPhone1;
        @BindView(R.id.edtCheckOuts)
        TextView edtCheckOuts;

        @BindView(R.id.edtCheckOut)
        RelativeLayout edtCheckOut;
        @BindView(R.id.txtDT)
        TextView txtDT;
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.p_title)
        TextView p_title;
        @BindView(R.id.txtABy)
        TextView txtABy;
        @BindView(R.id.txtReason)
        EditText txtReason;
        @BindView(R.id.txtstuName)
        EditText txtstuName;
        @BindView(R.id.txtcontprsn)
        EditText txtcontprsn;
        @BindView(R.id.txtcontprsnnum)
        TextView txtcontprsnnum;
        @BindView(R.id.txtrelation)
        TextView txtrelation;
        @BindView(R.id.txtconfirmationnum)
        TextView txtconfirmationnum;
        @BindView(R.id.txtdandtofcall)
        TextView txtdandtofcall;

        @BindView(R.id.std_sign)
        ImageView std_sign;
        @BindView(R.id.staff_sign)
        ImageView staff_sign;
        @BindView(R.id.admin_sign)
        ImageView admin_sign;

        @BindView(R.id.profile)
        CircularImageView profile;

        @BindView(R.id.std_profile)
        CircularImageView std_profile;

        @BindView(R.id.sign_lyt)
        CardView sign_lyt;

        @BindView(R.id.more)
        RelativeLayout more;

        @BindView(R.id.guardian_add)
        TextView guardian_add;
        @BindView(R.id.guardian_mini)
        TextView guardian_mini;



        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
