package com.ultimate.ultimatesmartstudent.SchoolActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartstudent.BeanModule.CommonBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewStudents extends AppCompatActivity implements ActivityStudentAdapter.Mycallback {

    SharedPreferences sharedPreferences;
    @BindView(R.id.imgBack)
    ImageView back;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.txtSub)
    TextView txtSub;


    @BindView(R.id.p_recyclerview)
    RecyclerView p_recyclerview;

    Spinner spinnerGroup,spinnerActivity;
    Spinner spinnersubject;
    private String activity_id = "";
    private String activity_name = "";

    @BindView(R.id.oneDay)
    RelativeLayout p_day;

    @BindView(R.id.parent)
    RelativeLayout parent;

    @BindView(R.id.multiDay)
    RelativeLayout v_day;

    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;

    String apply="pending";
    Dialog mBottomSheetDialog;


    @BindView(R.id.cal_img)
    ImageView cal_img;
    @BindView(R.id.today_date)
    TextView today_date;
    @BindView(R.id.dialog)
    ImageView dialog;
    int check=0;
    private LinearLayoutManager layoutManager,layoutManager1;

    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    String sub_id= "", sub_name= "" ,sectionid="",sectionname="",classid = "",className = "",check_view = "";

    String from_date = "";
    private int loaded = 0;
    ActivityStudentAdapter p_mAdapter;
    ArrayList<ActivityStudentBean> p_list=new ArrayList<>();

    private ArrayList<CommonBean> groupList = new ArrayList<>();
    private String groupid = "";
    private String group_name = "";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_students);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);

        layoutManager = new LinearLayoutManager(this);
        p_recyclerview.setLayoutManager(layoutManager);
        p_mAdapter = new ActivityStudentAdapter(p_list, this,this,2);
        p_recyclerview.setAdapter(p_mAdapter);
        p_fetchhwlist();
        setCommonData();
    }


    private void setCommonData() {
        txtSub.setVisibility(View.VISIBLE);
        txtTitle.setText(getString(R.string.school_act)+" "+getString(R.string.list));
        txtSub.setText(Utils.setNaviHeaderClassData());
    }


    @OnClick(R.id.imgBack)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }



    @OnClick(R.id.oneDay)
    public void oneDay() {
        apply="pending";
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        v_day.setBackgroundColor(Color.parseColor("#00000000"));
        p_day.setBackgroundColor(Color.parseColor("#66000000"));

    }

    @OnClick(R.id.multiDay)
    public void multiDay() {
        apply="verified";
        img2.setVisibility(View.VISIBLE);
        img1.setVisibility(View.GONE);
        p_day.setBackgroundColor(Color.parseColor("#00000000"));
        v_day.setBackgroundColor(Color.parseColor("#66000000"));

    }


    private void p_fetchhwlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("es_groupsid", groupid);
        params.put("es_activityid", activity_id);


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GET_ACT_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        if (p_list != null) {
                            p_list.clear();
                        }
                        JSONArray jsonArray = jsonObject.getJSONArray(Constants.GROUP_DATA);
                        p_list = ActivityStudentBean.parseClassArray(jsonArray);
                        if (p_list.size() > 0) {
                            p_mAdapter.setGatePassList(p_list);
                            //setanimation on adapter...
                            p_recyclerview.getAdapter().notifyDataSetChanged();
                            p_recyclerview.scheduleLayoutAnimation();
                            totalRecord.setText("Total Entries:- "+String.valueOf(p_list.size()));
                            //-----------end------------
                            txtNorecord.setVisibility(View.GONE);
                        } else {
                            totalRecord.setText("Total Entries:- 0");
                            p_mAdapter.setGatePassList(p_list);
                            p_mAdapter.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText("Total Entries:- 0");
                    txtNorecord.setVisibility(View.VISIBLE);
                    p_list.clear();
                    p_mAdapter.setGatePassList(p_list);
                    p_mAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @Override
    public void onApproveCallback(ActivityStudentBean homeworkbean) {

    }
}