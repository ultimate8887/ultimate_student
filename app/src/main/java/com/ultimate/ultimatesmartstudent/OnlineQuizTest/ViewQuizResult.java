package com.ultimate.ultimatesmartstudent.OnlineQuizTest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewQuizResult extends AppCompatActivity implements OnlineQuizResultAdapter.Mycallback {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.spinner2)
    Spinner SubjectSpinner;
    @BindView(R.id.profile)
    LinearLayout profile;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    OnlineQuizResultAdapter adapter;
    Animation animation;
    AdapterOfSubjectList adaptersub;
    AdapterOfquizList adapterquiz;
    String sub_id, sub_name,quiz_name,quiz_id;
    ArrayList<OnlineQuizBean> quizlist = new ArrayList<>();
    ArrayList<OnlineQuizresultBean> quizlistresult = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    BottomSheetDialog mBottomSheetDialog;
    Spinner spinner_session, spinner_exam;
    LinearLayout l1,l2;

    @BindView(R.id.imgStud)
    CircularImageView imgStud;
    @BindView(R.id.txtStudName)
    TextView txtStudName;
    @BindView(R.id.class_name)
    TextView session;
    @BindView(R.id.roll_no)
    TextView exam;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);
        txtTitle.setText(" Online Quiz/Test");

        SubjectSpinner.setVisibility(View.GONE);

        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        recyclerView.setLayoutManager(new LinearLayoutManager(ViewQuizResult.this));
        adapter = new OnlineQuizResultAdapter(quizlistresult, ViewQuizResult.this, this);
        recyclerView.setAdapter(adapter);
        getSubjectofClass();
        search_dialog();
    }

    private void search_dialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.exam_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(false);
        TextView title = (TextView) sheetView.findViewById(R.id.title);
        title.setText("View Result");
        title.setTextColor(getResources().getColor(R.color.present));
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        Picasso.get().load(R.drawable.informative).into(image);
        spinner_session = (Spinner) sheetView.findViewById(R.id.spinner_session);
        l1 = (LinearLayout) sheetView.findViewById(R.id.l1);
        l2 = (LinearLayout) sheetView.findViewById(R.id.l2);
        l1.setBackground(getResources().getDrawable(R.drawable.present_border));
        l2.setBackground(getResources().getDrawable(R.drawable.present_border));
        spinner_exam = (Spinner) sheetView.findViewById(R.id.spinner_exam);

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                finish();
                mBottomSheetDialog.dismiss();
            }
        });
        Button submitted = (Button) sheetView.findViewById(R.id.submitted);
        submitted.setBackground(getResources().getDrawable(R.drawable.present_bg));
        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);
                if (checkValid()) {
                    fetchonlinequizresult();
                    setData();
                }

            }
        });
        mBottomSheetDialog.show();
    }

    private void setData() {
        profile.setVisibility(View.VISIBLE);
        imgProfile.setVisibility(View.GONE);
        if (User.getCurrentUser().getProfile() != null) {
            Picasso.get().load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(imgStud);
        } else {
            Picasso.get().load(R.drawable.boy).into(imgStud);
        }
        if (User.getCurrentUser().getFirstname() != null && User.getCurrentUser().getClass() != null) {
            txtStudName.setText(User.getCurrentUser().getFirstname() + " (" + User.getCurrentUser().getClass_name() + ")");
        } else {
            txtStudName.setText("");
        }
        if (sub_name != null) {
            session.setText(sub_name);
        }
        if (quiz_name != null) {
            exam.setText(quiz_name);
        }
    }

    @OnClick(R.id.imgBackmsg)
    public void onback() {
        back.startAnimation(animation);
        finish();
    }

    private void getSubjectofClass() {
        UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));
                    adaptersub = new AdapterOfSubjectList(ViewQuizResult.this, subjectList);
                    spinner_session.setAdapter(adaptersub);
                    spinner_session.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                                spinner_exam.setSelection(0);
                                quizlist.clear();
                                fetchonlinequiz();
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void fetchonlinequiz() {
        UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("sub_id", sub_id);
        params.put("student_id", User.getCurrentUser().getId());
        params.put("s_key",User.getCurrentUser().getSchoolData().getFi_school_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.getonlinequiztest, apiCallbackthree, this, params);
    }
    ApiHandler.ApiCallback apiCallbackthree = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    quizlist =   OnlineQuizBean.parseSyllabusArray(jsonObject.getJSONArray("hostelroom_data"));
                    adapterquiz = new AdapterOfquizList(ViewQuizResult.this, quizlist);
                    spinner_exam.setAdapter(adapterquiz);
                    spinner_exam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                quiz_name = quizlist.get(i - 1).getQuiz_title();
                                quiz_id = quizlist.get(i - 1).getQuiz_id();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else{
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void fetchonlinequizresult() {
        mBottomSheetDialog.dismiss();
        UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("stu_id", User.getCurrentUser().getId());
        params.put("quiz_id", quiz_id);
        params.put("s_key", User.getCurrentUser().getSchoolData().getFi_school_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.getonlinequizresult, subapiCallbackresult, this, params);

    }

    ApiHandler.ApiCallback subapiCallbackresult = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("hostelroom_data");
                    quizlistresult = OnlineQuizresultBean.parseSyllabusArray(jsonArray);
                    if (quizlistresult.size() > 0) {
                        adapter.setSyllabusList(quizlistresult);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);

                    } else {
                        adapter.setSyllabusList(quizlistresult);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                txtNorecord.setText("No Record Found! " +"\n"+"Kindly Play This Quiz Firstly!");
                txtNorecord.setVisibility(View.VISIBLE);
                quizlistresult.clear();
                adapter.setSyllabusList(quizlistresult);
                adapter.notifyDataSetChanged();
            }
        }
    };

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;

        if (sub_id == null) {
            valid = false;
            errorMsg = "Select Subject";
        }
        else if (quiz_id == null) {
            valid = false;
            errorMsg = "Select Quiz";

        }

        if (!valid) {
            //  Utils.showSnackBar(errorMsg, parent);
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

    @Override
    public void onMethodCallback(OnlineQuizresultBean onlineQuizBean) {

    }

    @Override
    public void viewresult(OnlineQuizresultBean onlineQuizBean) {

    }
}