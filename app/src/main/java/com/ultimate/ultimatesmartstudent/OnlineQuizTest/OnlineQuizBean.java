package com.ultimate.ultimatesmartstudent.OnlineQuizTest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OnlineQuizBean {
    private String quiz_id;
    private String class_id;
    private String chapter;
    private String topic;
    private String quiz_title;
    private String time;
    private String tot_qus;
    private String classname;
    private String sub_name;
    private String score;

    public String getC_date() {
        return c_date;
    }

    public void setC_date(String c_date) {
        this.c_date = c_date;
    }

    private String  c_date;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(String quiz_id) {
        this.quiz_id = quiz_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getQuiz_title() {
        return quiz_title;
    }

    public void setQuiz_title(String quiz_title) {
        this.quiz_title = quiz_title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTot_qus() {
        return tot_qus;
    }

    public void setTot_qus(String tot_qus) {
        this.tot_qus = tot_qus;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public static ArrayList<OnlineQuizBean> parseSyllabusArray(JSONArray arrayObj) {
        ArrayList<OnlineQuizBean> list = new ArrayList<OnlineQuizBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                OnlineQuizBean p = parseSyllabusObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static OnlineQuizBean parseSyllabusObject(JSONObject jsonObject) {
        OnlineQuizBean casteObj = new OnlineQuizBean();
        try {
            if (jsonObject.has("quiz_id")&& !jsonObject.getString("quiz_id").isEmpty() && !jsonObject.getString("quiz_id").equalsIgnoreCase("null")) {
                casteObj.setQuiz_id(jsonObject.getString("quiz_id"));
            }
            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }
            if (jsonObject.has("chapter") && !jsonObject.getString("chapter").isEmpty() && !jsonObject.getString("chapter").equalsIgnoreCase("null")) {
                casteObj.setChapter(jsonObject.getString("chapter"));
            }
            if (jsonObject.has("topic") && !jsonObject.getString("topic").isEmpty() && !jsonObject.getString("topic").equalsIgnoreCase("null")) {
                casteObj.setTopic(jsonObject.getString("topic"));
            }
            if (jsonObject.has("quiz_title") && !jsonObject.getString("quiz_title").isEmpty() && !jsonObject.getString("quiz_title").equalsIgnoreCase("null")) {
                casteObj.setQuiz_title(jsonObject.getString("quiz_title"));
            }
            if (jsonObject.has("time") && !jsonObject.getString("time").isEmpty() && !jsonObject.getString("time").equalsIgnoreCase("null")) {
                casteObj.setTime(jsonObject.getString("time"));
            }
            if (jsonObject.has("classname") && !jsonObject.getString("classname").isEmpty() && !jsonObject.getString("classname").equalsIgnoreCase("null")) {
                casteObj.setClassname(jsonObject.getString("classname"));
            }
            if (jsonObject.has("sub_name") && !jsonObject.getString("sub_name").isEmpty() && !jsonObject.getString("sub_name").equalsIgnoreCase("null")) {
                casteObj.setSub_name(jsonObject.getString("sub_name"));
            }
            if (jsonObject.has("tot_qus") && !jsonObject.getString("tot_qus").isEmpty() && !jsonObject.getString("tot_qus").equalsIgnoreCase("null")) {
                casteObj.setTot_qus(jsonObject.getString("tot_qus"));
            }
            if (jsonObject.has("score") && !jsonObject.getString("score").isEmpty() && !jsonObject.getString("score").equalsIgnoreCase("null")) {
                casteObj.setScore(jsonObject.getString("score"));
            }
            if (jsonObject.has("c_date") && !jsonObject.getString("c_date").isEmpty() && !jsonObject.getString("c_date").equalsIgnoreCase("null")) {
                casteObj.setC_date(jsonObject.getString("c_date"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
