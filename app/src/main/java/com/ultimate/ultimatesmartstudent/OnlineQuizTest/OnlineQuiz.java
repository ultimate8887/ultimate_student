package com.ultimate.ultimatesmartstudent.OnlineQuizTest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.E_LearningMod.WebViewOnlineLink;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OnlineQuiz extends AppCompatActivity implements OnlineQuizAdapter.Mycallback{

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.spinner2)
    Spinner SubjectSpinner;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    OnlineQuizAdapter adapter;
    Animation animation;
    AdapterOfSubjectList adaptersub;
    String sub_id, sub_name;
    ArrayList<OnlineQuizBean> ebooklist = new ArrayList<>();
    ArrayList<OnlineQuizBean> ebooklist2 = new ArrayList<>();
    BottomSheetDialog mBottomSheetDialog;
    Spinner spinner_session, spinner_exam;
    LinearLayout l1,l2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);
        txtTitle.setText("Online Quiz/Test");
        SubjectSpinner.setVisibility(View.GONE);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerView.setLayoutManager(new LinearLayoutManager(OnlineQuiz.this));
        adapter = new OnlineQuizAdapter(ebooklist, OnlineQuiz.this, this);
        recyclerView.setAdapter(adapter);
        getSubjectofClass();
        search_dialog();

    }

    private void search_dialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.exam_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(false);
        TextView title = (TextView) sheetView.findViewById(R.id.title);
        title.setText("Check Quiz");
        title.setTextColor(getResources().getColor(R.color.colorAccent));
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        Picasso.get().load(R.drawable.informative).into(image);
        spinner_session = (Spinner) sheetView.findViewById(R.id.spinner_session);
        l1 = (LinearLayout) sheetView.findViewById(R.id.l1);
        l2 = (LinearLayout) sheetView.findViewById(R.id.l2);
        l1.setBackground(getResources().getDrawable(R.drawable.holiday_border));
        spinner_exam = (Spinner) sheetView.findViewById(R.id.spinner_exam);
        l2.setVisibility(View.GONE);
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                finish();
                mBottomSheetDialog.dismiss();
            }
        });
        Button submitted = (Button) sheetView.findViewById(R.id.submitted);
        submitted.setBackground(getResources().getDrawable(R.drawable.orange_bg));
        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);
                if (checkValid()) {
                    fetchonlinequiz();
                }

            }
        });
        mBottomSheetDialog.show();
    }

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;

        if (sub_id == null) {
            valid = false;
            errorMsg = "Select Subject";
        }

        if (!valid) {
            //  Utils.showSnackBar(errorMsg, parent);
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

    @OnClick(R.id.imgBackmsg)
    public void onback() {
        back.startAnimation(animation);
        finish();
    }

    private void getSubjectofClass() {
        UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));
                    adaptersub = new AdapterOfSubjectList(OnlineQuiz.this, subjectList);
                    spinner_session.setAdapter(adaptersub);
                    spinner_session.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void fetchonlinequiz() {
        mBottomSheetDialog.dismiss();
        UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("sub_id", sub_id);
        params.put("student_id", User.getCurrentUser().getId());
        params.put("s_key",User.getCurrentUser().getSchoolData().getFi_school_id());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.getonlinequiztest, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (ebooklist != null) {
                        ebooklist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hostelroom_data");
                    ebooklist = OnlineQuizBean.parseSyllabusArray(jsonArray);
                    if (ebooklist.size() > 0) {
                        adapter.setSyllabusList(ebooklist);
                     //   adapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);

                    } else {
                        adapter.setSyllabusList(ebooklist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                txtNorecord.setVisibility(View.VISIBLE);
                ebooklist.clear();
                adapter.setSyllabusList(ebooklist);
                adapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    public void onMethodCallback(OnlineQuizBean onlineQuizBean) {
        Gson gson = new Gson();
        String ebookdata = gson.toJson(onlineQuizBean, OnlineQuizBean.class);
        Intent intent = new Intent(OnlineQuiz.this, ViewQuizTest.class);
        intent.putExtra("onlineQuiz_data", ebookdata);
        startActivity(intent);

    }

    @Override
    public void viewresult(OnlineQuizBean onlineQuizBean) {
        Toast.makeText(getApplicationContext(),ebooklist2.get(0).getScore(),Toast.LENGTH_LONG).show();
    }
}