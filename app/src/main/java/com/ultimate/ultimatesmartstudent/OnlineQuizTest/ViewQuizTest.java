package com.ultimate.ultimatesmartstudent.OnlineQuizTest;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;
import com.uzairiqbal.circulartimerview.CircularTimerListener;
import com.uzairiqbal.circulartimerview.CircularTimerView;
import com.uzairiqbal.circulartimerview.TimeFormatEnum;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewQuizTest extends AppCompatActivity {
    boolean edit = false;
    @BindView(R.id.webviewabout)
    WebView webView;
    String link,s_key,student_id,class_id,quizid;
    boolean loadingFinished = true;
    boolean redirect = false;
    Animation animation;
    @BindView(R.id.quiz_tittle)
    TextView quiz_tittle;
    @BindView(R.id.start)
    LinearLayout start;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.logo)
    LottieAnimationView logo;
    OnlineQuizBean onlineQuizBean;
    @BindView(R.id.schName)
    TextView schName;
    @BindView(R.id.imgLogo)
    CircularImageView imgLogo;
    int time,click=0;
    CircularTimerView progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_quiz_test);

        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        setSchoolData();
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("onlineQuiz_data")) {
                Gson gson = new Gson();
                onlineQuizBean = gson.fromJson(getIntent().getExtras().getString("onlineQuiz_data"), OnlineQuizBean.class);
                edit = true;
            }
        }
        time= Integer.parseInt(onlineQuizBean.getTime());
        schName.setText(onlineQuizBean.getSub_name());
        // timer code
        progressBar = findViewById(R.id.progress_circular);
        progressBar.setProgress(0);
        progressBar.setCircularTimerListener(new CircularTimerListener() {
            @Override
            public String updateDataOnTick(long remainingTimeInMs) {
                return String.valueOf((int)Math.ceil((remainingTimeInMs / 1000.f)));
            }
            @Override
            public void onTimerFinished() {
                Toast.makeText(ViewQuizTest.this, "FINISHED", Toast.LENGTH_SHORT).show();
                progressBar.setPrefix("");
                progressBar.setSuffix("");
                progressBar.setText("FINISHED THANKS!");
                finish();
            }
        }, time, TimeFormatEnum.MINUTES, 10);
        // end here .....

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                progressBar.setProgress(0);
                progressBar.startTimer();
            }
        });

        quizid=onlineQuizBean.getQuiz_id();
        quiz_tittle.setText(onlineQuizBean.getQuiz_title());
        s_key= User.getCurrentUser().getSchoolData().getFi_school_id();
        student_id=User.getCurrentUser().getId();
        class_id=User.getCurrentUser().getClass_id();
      //  link="https://ultimatesolutiongroup.com/office_admin/onlinequiz/account.php?q=1&s_key="+s_key+"&student_id="+student_id+"&class_id="+class_id+"&eid="+quizid+"";
        link="http://test.ultimatesolutiongroup.com/studentHome.php?s_key="+s_key+"&student_id="+student_id+"";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));

//        Log.e("link",link);
//        webView.setWebViewClient(new Browser_Home());
//        webView.setWebChromeClient(new ChromeClient());
//        WebSettings webSettings = webView.getSettings();
//
//        webSettings.setJavaScriptEnabled(true);
//        webSettings.setAllowFileAccess(true);
//        webSettings.setAppCacheEnabled(true);
//        loadWebSite();
//        UltimateProgress.showProgressBar(ViewQuizTest.this,"Please Wait.....!");
//
//        webView.setWebViewClient(new WebViewClient(){
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                if(!redirect){
//                    loadingFinished = true;
//                }
//                if(loadingFinished && !redirect){
//                    UltimateProgress.cancelProgressBar();
//                } else{
//                    redirect = false;
//                }
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setSchoolData() {
        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(imgLogo);
        } else {
            Picasso.get().load(R.drawable.logo).into(imgLogo);
        }
//        if (User.getCurrentUser().getSchoolData().getName() != null){
//            schName.setText(User.getCurrentUser().getSchoolData().getName());
//        }
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        webView.restoreState(savedInstanceState);

    }

    private void loadWebSite() {
        webView.loadUrl(link);
    }

    private class Browser_Home extends WebViewClient {
        Browser_Home(){}

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

    private class ChromeClient extends WebChromeClient {
        private View mCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        protected FrameLayout mFullscreenContainer;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        ChromeClient() {
        }

        public Bitmap getDefaultVideoPoster() {
            if (mCustomView == null) {
                return null;
            }
            return BitmapFactory.decodeResource(getApplicationContext().getResources(), 2130837573);
        }

        public void onHideCustomView() {
            ((FrameLayout) getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
        }

        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
            if (this.mCustomView != null) {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout) getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
            getWindow().getDecorView().setSystemUiVisibility(3846 | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
    }

}