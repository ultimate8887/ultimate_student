package com.ultimate.ultimatesmartstudent.OnlineQuizTest;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnlineQuizAdapter extends RecyclerView.Adapter<OnlineQuizAdapter.Viewholder> {
    ArrayList<OnlineQuizBean> sy_list;
    Mycallback mAdaptercall;
    Context mContext;
    Animation animation;

    public OnlineQuizAdapter(ArrayList<OnlineQuizBean> sy_list, Context mContext, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quizon_adpt_lyt, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position) {

        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
        holder.head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdaptercall != null) {
                    holder.starttest.startAnimation(animation);
                    mAdaptercall.onMethodCallback(sy_list.get(position));
                }
            }
        });

        holder.txtPdf.setText(sy_list.get(position).getQuiz_title());
        holder.u_date.setText(Utils.dateFormat(sy_list.get(position).getC_date()));
        holder.txtSub.setText(sy_list.get(position).getSub_name()+"("+sy_list.get(position).getClassname()+")");

        if (sy_list.get(position).getTime() != null) {
            String title = getColoredSpanned("Time: ", "#ff0099cc");
            String Name = getColoredSpanned(sy_list.get(position).getTime(), "#7D7D7D");
            holder.txttime.setText(Html.fromHtml(title + " " + Name));
        }

        if (sy_list.get(position).getTopic() != null) {
            String title = getColoredSpanned("Topic: ", "#ff0099cc");
            String Name = getColoredSpanned(sy_list.get(position).getTopic(), "#7D7D7D");
            holder.txttitle.setText(Html.fromHtml(title + " " + Name));
        }

        if (sy_list.get(position).getTot_qus() != null) {
            String title = getColoredSpanned("Total Question: ", "#ff0099cc");
            String Name = getColoredSpanned(sy_list.get(position).getTot_qus(), "#7D7D7D");
            holder.txtwriter.setText(Html.fromHtml(title + " " + Name));
        }

        if (sy_list.get(position).getChapter() != null) {
            String title = getColoredSpanned("Chapter: ", "#ff0099cc");
            String Name = getColoredSpanned(sy_list.get(position).getChapter(), "#7D7D7D");
            holder.txtauthor.setText(Html.fromHtml(title + " " + Name));
        }


    }

    public interface Mycallback {
        public void onMethodCallback(OnlineQuizBean onlineQuizBean);
        public void viewresult(OnlineQuizBean onlineQuizBean);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<OnlineQuizBean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPdf)
        TextView txtPdf;
        @BindView(R.id.head)
        RelativeLayout head;
        @BindView(R.id.txttitle)
        TextView txttitle;
        @BindView(R.id.txtSub)
        TextView txtSub;
        @BindView(R.id.txtwriter)
        TextView txtwriter;
        @BindView(R.id.u_date)
        TextView u_date;
        @BindView(R.id.txtauthor)
        TextView txtauthor;
        @BindView(R.id.txttime)TextView txttime;
        @BindView(R.id.starttest)TextView starttest;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
