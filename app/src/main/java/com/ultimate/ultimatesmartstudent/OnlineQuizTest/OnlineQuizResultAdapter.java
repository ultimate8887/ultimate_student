package com.ultimate.ultimatesmartstudent.OnlineQuizTest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnlineQuizResultAdapter extends RecyclerView.Adapter<OnlineQuizResultAdapter.Viewholder> {
    ArrayList<OnlineQuizresultBean> sy_list;
    OnlineQuizResultAdapter.Mycallback mAdaptercall;
    Context mContext;

    public OnlineQuizResultAdapter(ArrayList<OnlineQuizresultBean> sy_list, Context mContext, OnlineQuizResultAdapter.Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }


    @Override
    public OnlineQuizResultAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quizres_adpt_lyt, parent, false);
        OnlineQuizResultAdapter.Viewholder viewholder = new OnlineQuizResultAdapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(OnlineQuizResultAdapter.Viewholder holder, final int position) {

        holder.sahi.setText(sy_list.get(position).getCorrect());
        holder.galat.setText(sy_list.get(position).getWrong());
        holder.score.setText(sy_list.get(position).getScore());
    }

    public interface Mycallback {
        public void onMethodCallback(OnlineQuizresultBean onlineQuizBean);
        public void viewresult(OnlineQuizresultBean onlineQuizBean);
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<OnlineQuizresultBean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.sahi)
        TextView sahi;
        @BindView(R.id.galat)
        TextView galat;
        @BindView(R.id.score)
        TextView score;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
