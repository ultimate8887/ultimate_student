package com.ultimate.ultimatesmartstudent.TimetableMod.TimeTableSectionWise;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThurFragmentSection extends Fragment implements DayAdapterSecWise.UpdateTimeTable {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    ArrayList<String> subList = new ArrayList<String>();
    ArrayList<String> staffList = new ArrayList<String>();
    DayAdapterSecWise mAdapter;
    @BindView(R.id.imgView)
    ImageView imgView;

    ArrayList<String> subList2 = new ArrayList<String>();
    ArrayList<String> staffList2 = new ArrayList<String>();
    private Dialog ttDialog;
    private Spinner spinnerSubject;
    private Spinner spinnerStaff;
    public String staffid;
    private String col;
    private String class_id,section_id;
    private Spinner spinnerType;
    ArrayList<String> s_type;
    private EditText edtBatch,edtRemarks;
    private int type_value;
    private String t_col, b_col;
    private String r_col;
    CommonProgress commonProgress;
    ArrayList<String> subList23 = new ArrayList<String>();
    ArrayList<String> staffList23 = new ArrayList<String>();
    public ThurFragmentSection() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_monday, container, false);
        ButterKnife.bind(this, view);
        commonProgress=new CommonProgress(getActivity());
        s_type = new ArrayList<>();
        s_type.add("Theory");
        s_type.add("Practical");
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new DayAdapterSecWise(getActivity(), subList, staffList,subList2, staffList2,subList23, staffList23, this,this);
        recyclerView.setAdapter(mAdapter);
        fecthData();
        return view;
    }

    private void fecthData() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("section_id",  User.getCurrentUser().getSection_id());
        params.put("day", "3");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.TIMETABLESEC_URL, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    TimeTableBean timeData = TimeTableBean.parseTimeTableObject(jsonObject.getJSONObject("time_table"));
                    ArrayList<String> teach_list = (ArrayList<String>) timeData.getTeacher();
                    ArrayList<String> sub_list = (ArrayList<String>) timeData.getSubject();
                    ArrayList<String> type_list = (ArrayList<String>) timeData.getType();
                    ArrayList<String> batch_list = (ArrayList<String>) timeData.getBatch();
                    ArrayList<String> remark_list = (ArrayList<String>) timeData.getRemark();


                    ArrayList<String> teach_list2 = (ArrayList<String>) timeData.getTeacher2();
                    ArrayList<String> sub_list2 = (ArrayList<String>) timeData.getSubject2();
                    ArrayList<String> type_list2 = (ArrayList<String>) timeData.getType2();
                    ArrayList<String> batch_list2 = (ArrayList<String>) timeData.getBatch2();
                    ArrayList<String> remark_list2 = (ArrayList<String>) timeData.getRemark2();

                    ArrayList<String> teach_list23 = (ArrayList<String>) timeData.getTeacher23();
                    ArrayList<String> sub_list23 = (ArrayList<String>) timeData.getSubject23();

                    if ( sub_list != null) {
                        mAdapter.setTeacherList(teach_list);
                        mAdapter.setSubList(sub_list);
                        mAdapter.setTypeList(type_list);
                        mAdapter.setBatchList(batch_list);
                        mAdapter.setRemarkList(remark_list);

                        mAdapter.setTeacherList2(teach_list2);
                        mAdapter.setSubList2(sub_list2);
                        mAdapter.setTypeList2(type_list2);
                        mAdapter.setBatchList2(batch_list2);
                        mAdapter.setRemarkList2(remark_list2);

                        mAdapter.setTeacherList23(teach_list23);
                        mAdapter.setSubList23(sub_list23);

                        mAdapter.setData(timeData);
                        imgView.setVisibility(View.GONE);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mAdapter.setData(timeData);
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                imgView.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void updateTimeTable(int pos) {
       // openDialog(1,pos);
    }

    @Override
    public void updateTimeTable2(int pos) {
        //openDialog(2,pos);
    }



}
