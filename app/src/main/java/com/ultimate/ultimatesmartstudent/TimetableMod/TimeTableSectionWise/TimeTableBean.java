package com.ultimate.ultimatesmartstudent.TimetableMod.TimeTableSectionWise;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Monika Gupta on 3/6/2018.
 */

public class TimeTableBean {

    private static String SUBJECT = "subject";
    private static String START_TIME = "start_time";
    private static String TOTAL_PERIODS = "total_periods";
    private static String BREAK_DUR = "break_dur";
    private static String BREAK_AFTER = "break_after";
    private static String LUNCH_DUR = "lunch_dur";
    private static String LUNCH_AFTER = "lunch_after";
    private static String PERIOD_DUR = "period_dur";
    private static String TEACHER = "teacher";


    private static String CALL1 = "call1";
    private static String CALL2 = "call2";


    private static String SUBJECT2 = "subject2";
    private static String START_TIME2 = "start_time2";
    private static String TOTAL_PERIODS2 = "total_periods2";
    private static String BREAK_DUR2 = "break_dur2";
    private static String BREAK_AFTER2 = "break_after2";
    private static String LUNCH_DUR2 = "lunch_dur2";
    private static String LUNCH_AFTER2 = "lunch_after2";
    private static String PERIOD_DUR2 = "period_dur2";
    private static String TEACHER2 = "teacher2";
    private static String CALL12 = "call12";
    private static String CALL22 = "call22";

    private static String SUBJECT23 = "subject3";
    private static String TEACHER23 = "teacher3";

    /**
     * subject : [{"1":"ENGLISH","2":"HINDI","3":"MATH","4":"PUNJABI","5":"SCIENCE","6":"SOCIAL SCIENCE","7":"GK","8":"EVS"},{"1":null,"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null},{"1":null,"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null},{"1":"COMPUTER","2":"EVS","3":"GK","4":"SOCIAL SCIENCE","5":"SCIENCE","6":"MATH","7":"PUNJABI","8":null},{"1":null,"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null},{"1":null,"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null}]
     * start_time : 08:00:00
     * total_periods : 8
     * break_dur : 20
     * break_after : 2
     * lunch_dur : 20
     * lunch_after : 4
     * period_dur : 40
     * teacher : [{"1":"monika","2":"gdfgfg","3":"monika","4":"monika","5":"gdfgfg","6":"monika","7":"gdfgfg","8":"gdfgfg"},{"1":null,"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null},{"1":null,"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null},{"1":"monika","2":"monika","3":"monika","4":"monika","5":"monika","6":"monika","7":"monika","8":null},{"1":null,"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null},{"1":null,"2":null,"3":null,"4":null,"5":null,"6":null,"7":null,"8":null}]
     */

    private Date start_time;
    private String total_periods;
    private String break_dur;
    private String break_after = "0";
    private String lunch_dur;
    private String lunch_after = "0";
    private String period_dur;
    private List<String> subject;
    private List<String> teacher;

    private List<String> subject23;

    public List<String> getSubject23() {
        return subject23;
    }

    public void setSubject23(List<String> subject23) {
        this.subject23 = subject23;
    }

    public List<String> getTeacher23() {
        return teacher23;
    }

    public void setTeacher23(List<String> teacher23) {
        this.teacher23 = teacher23;
    }

    private List<String> teacher23;

    private String call1;
    private String call2;
    private List<String> type;
    private List<String> batch;
    private Object remark;

    private Date start_time2;
    private String total_periods2;
    private String break_dur2;
    private String break_after2 = "0";
    private String lunch_dur2;
    private String lunch_after2 = "0";
    private String period_dur2;
    private List<String> subject2;
    private List<String> teacher2;
    private String call12;
    private String call22;
    private List<String> type2;
    private List<String> batch2;
    private Object remark2;


    public void setCall1(String call1) {
        this.call1 = call1;
    }

    public String getCall1() {
        return call1;
    }
    public void setCall2(String call2) {
        this.call2 = call2;
    }

    public String getCall2() {
        return call2;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getStart_time2() {
        return start_time2;
    }

    public void setStart_time2(Date start_time2) {
        this.start_time2 = start_time2;
    }

    public String getTotal_periods() {
        return total_periods;
    }

    public void setTotal_periods(String total_periods) {
        this.total_periods = total_periods;
    }

    public String getBreak_dur() {
        return break_dur;
    }

    public void setBreak_dur(String break_dur) {
        this.break_dur = break_dur;
    }

    public String getBreak_dur2() {
        return break_dur2;
    }

    public void setBreak_dur2(String break_dur2) {
        this.break_dur2 = break_dur2;
    }

    public String getBreak_after() {
        return break_after;
    }

    public void setBreak_after(String break_after) {
        this.break_after = break_after;
    }

    public String getBreak_after2() {
        return break_after2;
    }

    public void setBreak_after2(String break_after2) {
        this.break_after2 = break_after2;
    }

    public String getLunch_dur() {
        return lunch_dur;
    }

    public void setLunch_dur(String lunch_dur) {
        this.lunch_dur = lunch_dur;
    }

    public String getLunch_dur2() {
        return lunch_dur2;
    }

    public void setLunch_dur2(String lunch_dur2) {
        this.lunch_dur2 = lunch_dur2;
    }

    public String getLunch_after() {
        return lunch_after;
    }

    public void setLunch_after(String lunch_after) {
        this.lunch_after = lunch_after;
    }

    public String getLunch_after2() {
        return lunch_after2;
    }

    public void setLunch_after2(String lunch_after2) {
        this.lunch_after2 = lunch_after2;
    }

    public String getPeriod_dur() {
        return period_dur;
    }

    public void setPeriod_dur(String period_dur) {
        this.period_dur = period_dur;
    }
    public String getPeriod_dur2() {
        return period_dur2;
    }

    public void setPeriod_dur2(String period_dur2) {
        this.period_dur2 = period_dur2;
    }




    public List<String> getSubject() {
        return subject;
    }

    public void setSubject(List<String> subject) {
        this.subject = subject;
    }
    public List<String> getSubject2() {
        return subject2;
    }

    public void setSubject2(List<String> subject2) {
        this.subject2 = subject2;
    }

    public List<String> getTeacher() {
        return teacher;
    }

    public void setTeacher(List<String> teacher) {
        this.teacher = teacher;
    }

    public List<String> getTeacher2() {
        return teacher2;
    }

    public void setTeacher2(List<String> teacher2) {
        this.teacher2 = teacher2;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public List<String> getType2() {
        return type2;
    }

    public void setType2(List<String> type2) {
        this.type2 = type2;
    }

    public List<String> getBatch() {
        return batch;
    }

    public void setBatch(List<String> batch) {
        this.batch = batch;
    }

    public List<String> getBatch2() {
        return batch2;
    }

    public void setBatch2(List<String> batch2) {
        this.batch2 = batch2;
    }

    public Object getRemark() {
        return remark;
    }

    public void setRemark(Object remark) {
        this.remark = remark;
    }


    public Object getRemark2() {
        return remark2;
    }

    public void setRemark2(Object remark2) {
        this.remark2 = remark2;
    }



    public static TimeTableBean parseTimeTableObject(JSONObject jsonObject) {
        TimeTableBean attendObj = new TimeTableBean();
        try {
            if (jsonObject.has(TOTAL_PERIODS) && !jsonObject.getString(TOTAL_PERIODS).isEmpty() && !jsonObject.getString(TOTAL_PERIODS).equalsIgnoreCase("null")) {
                attendObj.setTotal_periods(jsonObject.getString(TOTAL_PERIODS));
            }
            if (jsonObject.has(SUBJECT)) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject(SUBJECT);
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                  String dataName;
                  if(subObj.has(String.valueOf(j))){
                         dataName = subObj.getString(String.valueOf(j));
                    }else {
                         dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setSubject(list);
            }

            if (jsonObject.has(SUBJECT2)) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject(SUBJECT2);
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        dataName = subObj.getString(String.valueOf(j));
                    } else {
                        dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setSubject2(list);
            }

            if (jsonObject.has(SUBJECT23)) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject(SUBJECT23);
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        dataName = subObj.getString(String.valueOf(j));
                    } else {
                        dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setSubject23(list);
            }



            if (jsonObject.has("type")) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject("type");
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        if( subObj.getString(String.valueOf(j)).equalsIgnoreCase("1")){
                            dataName = "Theory";
                        }else if( subObj.getString(String.valueOf(j)).equalsIgnoreCase("2")){
                            dataName = "Practical";
                        }else{
                            dataName = "";
                        }
                    } else {
                        dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setType(list);
            }

//i have to check from mam
            if (jsonObject.has("type2")) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject("type2");
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        if( subObj.getString(String.valueOf(j)).equalsIgnoreCase("1")){
                            dataName = "Theory";
                        }else if( subObj.getString(String.valueOf(j)).equalsIgnoreCase("2")){
                            dataName = "Practical";
                        }else{
                            dataName = "";
                        }
                    } else {
                        dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setType2(list);
            }







            if (jsonObject.has("batch")) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject("batch");
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        dataName = subObj.getString(String.valueOf(j));
                    } else {
                        dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setBatch(list);
            }

            if (jsonObject.has("batch2")) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject("batch2");
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        dataName = subObj.getString(String.valueOf(j));
                    } else {
                        dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setBatch2(list);
            }


            if (jsonObject.has("remarks")) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject("remarks");
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        dataName = subObj.getString(String.valueOf(j));
                    } else {
                        dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setRemark(list);
            }

            if (jsonObject.has("remarks2")) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject("remarks2");
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        dataName = subObj.getString(String.valueOf(j));
                    } else {
                        dataName = "";
                    }
                    list.add(dataName);
                }
                attendObj.setRemark2(list);
            }


            if (jsonObject.has(START_TIME) && !jsonObject.getString(START_TIME).isEmpty() && !jsonObject.getString(START_TIME).equalsIgnoreCase("null")) {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                cal.setTime(sdf.parse(jsonObject.getString(START_TIME)));
                attendObj.setStart_time(cal.getTime());
            }
            if (jsonObject.has(START_TIME2) && !jsonObject.getString(START_TIME2).isEmpty() && !jsonObject.getString(START_TIME2).equalsIgnoreCase("null")) {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                cal.setTime(sdf.parse(jsonObject.getString(START_TIME2)));
                attendObj.setStart_time2(cal.getTime());
            }
            if (jsonObject.has(BREAK_DUR) && !jsonObject.getString(BREAK_DUR).isEmpty() && !jsonObject.getString(BREAK_DUR).equalsIgnoreCase("null")) {
                attendObj.setBreak_dur(jsonObject.getString(BREAK_DUR));
            }

            if (jsonObject.has(BREAK_DUR2) && !jsonObject.getString(BREAK_DUR2).isEmpty() && !jsonObject.getString(BREAK_DUR2).equalsIgnoreCase("null")) {
                attendObj.setBreak_dur2(jsonObject.getString(BREAK_DUR2));
            }


            if (jsonObject.has(BREAK_AFTER) && !jsonObject.getString(BREAK_AFTER).isEmpty() && !jsonObject.getString(BREAK_AFTER).equalsIgnoreCase("null")) {
                attendObj.setBreak_after(jsonObject.getString(BREAK_AFTER));
            }
            if (jsonObject.has(BREAK_AFTER2) && !jsonObject.getString(BREAK_AFTER2).isEmpty() && !jsonObject.getString(BREAK_AFTER2).equalsIgnoreCase("null")) {
                attendObj.setBreak_after2(jsonObject.getString(BREAK_AFTER2));
            }

            if (jsonObject.has(LUNCH_DUR) && !jsonObject.getString(LUNCH_DUR).isEmpty() && !jsonObject.getString(LUNCH_DUR).equalsIgnoreCase("null")) {
                attendObj.setLunch_dur(jsonObject.getString(LUNCH_DUR));
            }

            if (jsonObject.has(LUNCH_DUR2) && !jsonObject.getString(LUNCH_DUR2).isEmpty() && !jsonObject.getString(LUNCH_DUR2).equalsIgnoreCase("null")) {
                attendObj.setLunch_dur2(jsonObject.getString(LUNCH_DUR2));
            }
            if (jsonObject.has(LUNCH_AFTER) && !jsonObject.getString(LUNCH_AFTER).isEmpty() && !jsonObject.getString(LUNCH_AFTER).equalsIgnoreCase("null")) {
                attendObj.setLunch_after(jsonObject.getString(LUNCH_AFTER));
            }

            if (jsonObject.has(LUNCH_AFTER2) && !jsonObject.getString(LUNCH_AFTER2).isEmpty() && !jsonObject.getString(LUNCH_AFTER2).equalsIgnoreCase("null")) {
                attendObj.setLunch_after2(jsonObject.getString(LUNCH_AFTER2));
            }

            if (jsonObject.has(PERIOD_DUR) && !jsonObject.getString(PERIOD_DUR).isEmpty() && !jsonObject.getString(PERIOD_DUR).equalsIgnoreCase("null")) {
                attendObj.setPeriod_dur(jsonObject.getString(PERIOD_DUR));
            }

            if (jsonObject.has(PERIOD_DUR2) && !jsonObject.getString(PERIOD_DUR2).isEmpty() && !jsonObject.getString(PERIOD_DUR2).equalsIgnoreCase("null")) {
                attendObj.setPeriod_dur2(jsonObject.getString(PERIOD_DUR2));
            }

            if (jsonObject.has(CALL1) && !jsonObject.getString(CALL1).isEmpty() && !jsonObject.getString(CALL1).equalsIgnoreCase("null")) {
                attendObj.setCall1(jsonObject.getString(CALL1));
            }
            if (jsonObject.has(CALL2) && !jsonObject.getString(CALL2).isEmpty() && !jsonObject.getString(CALL2).equalsIgnoreCase("null")) {
                attendObj.setCall2(jsonObject.getString(CALL2));
            }
            if (jsonObject.has(TEACHER)) {
                ArrayList<String> List = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject(TEACHER);
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if(subObj.has(String.valueOf(j))){
                        dataName = subObj.getString(String.valueOf(j));
                    }else {
                        dataName = "";
                    }
                    List.add(dataName);
                }
                attendObj.setTeacher(List);
            }

            if (jsonObject.has(TEACHER2)) {
                ArrayList<String> List = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject(TEACHER2);
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        dataName = subObj.getString(String.valueOf(j));
                    } else {
                        dataName = "";
                    }
                    List.add(dataName);
                }
                attendObj.setTeacher2(List);
            }

            if (jsonObject.has(TEACHER23)) {
                ArrayList<String> List = new ArrayList<>();
                JSONObject subObj = jsonObject.getJSONObject(TEACHER23);
                for (int j = 1; j <= Integer.parseInt(attendObj.getTotal_periods()); j++) {
                    String dataName;
                    if (subObj.has(String.valueOf(j))) {
                        dataName = subObj.getString(String.valueOf(j));
                    } else {
                        dataName = "";
                    }
                    List.add(dataName);
                }
                attendObj.setTeacher23(List);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return attendObj;
    }

    public static ArrayList<TimeTableBean> parseTimeTableArray(JSONArray jsonArray) {
        try {

            ArrayList<TimeTableBean> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                TimeTableBean attendObj = parseTimeTableObject(jsonArray.getJSONObject(i));
                arrayList.add(attendObj);
            }
            return arrayList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
