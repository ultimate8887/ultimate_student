package com.ultimate.ultimatesmartstudent.TimetableMod.TimeTableSectionWise;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DayAdapterSecWise extends RecyclerView.Adapter<DayAdapterSecWise.MyViewHolder> {

    private UpdateTimeTable mUpdateTimeTable, mUpdateTimeTable2;
    private ArrayList<String> teacherList;
    private ArrayList<String> teacherList2;

    private ArrayList<String> teacherList23;
    private Context mContext;
    ArrayList<String> dataList;
    ArrayList<String> dataList2;

    ArrayList<String> dataList23;
    int step = 0;
    private TimeTableBean data;
    private ArrayList<String> typeList;
    private ArrayList<String> typeList2;
    private ArrayList<String> batchList;
    private ArrayList<String> batchList2;
    private ArrayList<String> remarkList;
    private ArrayList<String> remarkList2;

    DayAdapterSecWise(Context mContext, ArrayList<String> dataList, ArrayList<String> teacherList, ArrayList<String> dataList2, ArrayList<String> teacherList2
            , ArrayList<String> dataList23, ArrayList<String> teacherList23, UpdateTimeTable mUpdateTimeTable, UpdateTimeTable mUpdateTimeTable2) {
        this.mContext = mContext;
        this.mUpdateTimeTable = mUpdateTimeTable;
        this.mUpdateTimeTable2 = mUpdateTimeTable2;
        this.teacherList = teacherList;
        this.dataList = dataList;
        this.teacherList2 = teacherList2;
        this.dataList2 = dataList2;
        this.teacherList23 = teacherList23;
        this.dataList23 = dataList23;
    }

    public void setSubList23(ArrayList<String> dataList23) {
        this.dataList23 = dataList23;
    }

    public void setTeacherList23(ArrayList<String> teacherList23) {
        this.teacherList23 = teacherList23;
    }

    public void setTypeList(ArrayList<String> typeList) {
        this.typeList = typeList;
    }

    public void setTypeList2(ArrayList<String> typeList2) {
        this.typeList2 = typeList2;
    }


    public void setBatchList(ArrayList<String> batchList) {
        this.batchList = batchList;
    }

    public void setBatchList2(ArrayList<String> batchList2) {
        this.batchList2 = batchList2;
    }


    public void setRemarkList(ArrayList<String> remarkList) {
        this.remarkList = remarkList;
    }

    public void setRemarkList2(ArrayList<String> remarkList2) {
        this.remarkList2 = remarkList2;
    }

    public ArrayList<String> getRemarkList() {
        return remarkList;
    }

    public ArrayList<String> getRemarkList2() {
        return remarkList2;
    }

    public interface UpdateTimeTable {
        public void updateTimeTable(int pos);

        public void updateTimeTable2(int pos);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.time_table_lyt, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
//        holder.lytData.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mUpdateTimeTable.updateTimeTable(position);
//            }
//        });
//
//        holder.lytData1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mUpdateTimeTable2.updateTimeTable2(position);
//            }
//        });
        String period= String.valueOf(position+1);
        holder.txtSubsssss.setText("Period- "+period);

        if (data.getBreak_after().equalsIgnoreCase(String.valueOf(position + 1)) || data.getLunch_after().equalsIgnoreCase(String.valueOf(position + 1))) {
            holder.lytBreak.setVisibility(View.VISIBLE);
            if (step == 0) {
                step = 1;
                holder.txtBreak.setText(data.getCall1());
            } else {
                holder.txtBreak.setText(data.getCall2());
            }
        } else {
            holder.lytBreak.setVisibility(View.GONE);
        }
        boolean show = true;

        if(User.getCurrentUser().getSchoolData().getFi_no_subjects()!=null &&  User.getCurrentUser().getSchoolData().getFi_no_subjects().equalsIgnoreCase("two") && dataList2.size() <=0){
            if(dataList.size()<= 0 ){
                show = false;
            }else{
                show = true;
            }
        }else{
            if(dataList.size()<= 0 ){
                show = false;
            }
        }

        if (!show) {
            holder.lytData.setVisibility(View.GONE);
            holder.lytData1.setVisibility(View.GONE);
            holder.lytNoDetail.setVisibility(View.VISIBLE);
        } else {

            Date startTime = null;
            Date date = data.getStart_time();
            long t = date.getTime();
            Date endTime = null;


            if (data.getBreak_after().equalsIgnoreCase(String.valueOf(position))) {
                startTime = new Date(t + ((Integer.parseInt(data.getBreak_dur())) * 60000));
                data.setStart_time(startTime);
                endTime = new Date(startTime.getTime() + ((Integer.parseInt(data.getPeriod_dur()) * (position + 1)) * 60000));
                startTime = new Date(startTime.getTime() + ((Integer.parseInt(data.getPeriod_dur()) * position) * 60000));
            } else if (data.getLunch_after().equalsIgnoreCase(String.valueOf(position))) {
                startTime = new Date(t + ((Integer.parseInt(data.getLunch_dur())) * 60000));
                data.setStart_time(startTime);
                endTime = new Date(startTime.getTime() + ((Integer.parseInt(data.getPeriod_dur()) * (position + 1)) * 60000));
                startTime = new Date(startTime.getTime() + ((Integer.parseInt(data.getPeriod_dur()) * position) * 60000));
            } else {
                if (position == 0) {
                    startTime = data.getStart_time();
                } else {
                    startTime = new Date(t + ((Integer.parseInt(data.getPeriod_dur()) * position) * 60000));
                }
                endTime = new Date(t + ((Integer.parseInt(data.getPeriod_dur()) * (position + 1)) * 60000));

            }
            String start = (String) android.text.format.DateFormat.format("hh:mm a", startTime);
            String end = (String) android.text.format.DateFormat.format("hh:mm a", endTime);


            holder.txtTime.setText(start + " - " + end);
            holder.lytData.setVisibility(View.VISIBLE);
            holder.lytData1.setVisibility(View.VISIBLE);
            holder.lytNoDetail.setVisibility(View.GONE);


            if (dataList.get(position) != null) {
                String Name="";
                holder.txtSub.setVisibility(View.VISIBLE);
              //  holder.txtSub.setText("Subject : " + dataList.get(position));
                String title = getColoredSpanned("Subject: ", "#000000");
                if (!dataList.get(position).equalsIgnoreCase("")){
                     Name = getColoredSpanned(dataList.get(position), "#5A5C59");
                }else{
                    if (dataList2.get(position)!=null){
                        Name = getColoredSpanned(dataList2.get(position), "#5A5C59");
                    }

                }

//                if (!dataList.isEmpty() && dataList.get(position) != null) {
//                    Name = getColoredSpanned(dataList.get(position), "#5A5C59");
//                } else if (!dataList2.isEmpty() && dataList2.get(position) != null) {
//                    Name = getColoredSpanned(dataList2.get(position), "#5A5C59");
//                } else {
//                    // handle case where both ArrayLists are empty or element is null
//                }

                holder.txtSub.setText(Html.fromHtml(title + " " + Name));

                if (dataList2.get(position) != null) {
                    holder.lytData1.setVisibility(View.VISIBLE);
                    String Name1="";
                    //  holder.txtSub.setText("Subject : " + dataList.get(position));
                    String title1 = getColoredSpanned("Subject: ", "#000000");
                    Name1 = getColoredSpanned(dataList2.get(position), "#5A5C59");
                    holder.txtSub1.setText(Html.fromHtml(title1 + " " + Name1));

                    //  holder.txtSub1.setText("Subject : " + dataList2.get(position));
                } else {
                    //holder.txtSub1.setText("Subject : -");
                    holder.lytData1.setVisibility(View.GONE);
                }

                if (teacherList2 != null && teacherList2.get(position) != null) {
                    String Name2="";
                    //  holder.txtSub.setText("Subject : " + dataList.get(position));
                    String title2 = getColoredSpanned("Teacher: ", "#000000");
                    Name2 = getColoredSpanned(teacherList2.get(position), "#5A5C59");
                    holder.txtName1.setText(Html.fromHtml(title2 + " " + Name2));
                    //  holder.txtName1.setText(teacherList2.get(position));
                } else {
                    holder.txtName1.setText("----");
                    // holder.txtName1.setVisibility(View.GONE);
                }

                if (dataList23.get(position) != null) {
                    holder.lytData2.setVisibility(View.VISIBLE);
                    String Name1="";
                    //  holder.txtSub.setText("Subject : " + dataList.get(position));
                    String title1 = getColoredSpanned("Subject: ", "#000000");
                    Name1 = getColoredSpanned(dataList23.get(position), "#5A5C59");
                    holder.txtSub2.setText(Html.fromHtml(title1 + " " + Name1));

                    //  holder.txtSub1.setText("Subject : " + dataList2.get(position));
                } else {
                    //holder.txtSub1.setText("Subject : -");
                    holder.lytData2.setVisibility(View.GONE);
                }

                if (teacherList23 != null && teacherList23.get(position) != null) {
                    String Name2="";
                    //  holder.txtSub.setText("Subject : " + dataList.get(position));
                    String title2 = getColoredSpanned("Teacher: ", "#000000");
                    Name2 = getColoredSpanned(teacherList23.get(position), "#5A5C59");
                    holder.txtName2.setText(Html.fromHtml(title2 + " " + Name2));
                    //  holder.txtName1.setText(teacherList2.get(position));
                } else {
                    holder.txtName2.setText("----");
                    // holder.txtName1.setVisibility(View.GONE);
                }


            } else {
                //holder.txtSub.setText("Subject : -");
                holder.txtSub.setVisibility(View.GONE);
            }
            //if (teacherList != null && teacherList.size() > 0) {
            if (teacherList != null && teacherList.get(position) != null) {
                holder.txtName.setVisibility(View.VISIBLE);
                //holder.txtName.setText("Teacher : " + teacherList.get(position));

                String title = getColoredSpanned("Teacher: ", "#000000");
                String Name = getColoredSpanned(teacherList.get(position), "#5A5C59");


                if (!teacherList.get(position).equalsIgnoreCase("")){
                    Name = getColoredSpanned(teacherList.get(position), "#5A5C59");
                }else{
                    if (teacherList2.get(position)!=null){
                        Name = getColoredSpanned(teacherList2.get(position), "#5A5C59");
                    }

                }

                holder.txtName.setText(Html.fromHtml(title + " " + Name));
            } else {
                // holder.txtName.setText("Teacher: -");
                holder.txtName.setVisibility(View.GONE);
            }
            if (typeList != null && typeList.get(position) != null) {
                holder.txtType.setVisibility(View.VISIBLE);
                holder.txtType.setText("Type: " + typeList.get(position));
            } else {
                //holder.txtType.setText("Type: -");
                holder.txtType.setVisibility(View.GONE);
            }
            if (batchList != null && batchList.get(position) != null) {
                holder.txtBatch.setVisibility(View.VISIBLE);
                holder.txtBatch.setText("Batch: " + batchList.get(position));
            } else {
                // holder.txtBatch.setText("Batch: -");
                holder.txtBatch.setVisibility(View.GONE);
            }


            if (remarkList != null && remarkList.get(position) != null) {
                holder.txtRemark.setVisibility(View.VISIBLE);
                holder.txtRemark.setText("Remarks: " + remarkList.get(position));
            } else {
                // holder.txtRemark.setText("Remarks: -");
                holder.txtRemark.setVisibility(View.GONE);
            }

//            if (User.getCurrentUser().getSchoolData().getFi_no_subjects()!=null && User.getCurrentUser().getSchoolData().getFi_no_subjects().equalsIgnoreCase("two")) {
//                holder.lytData1.setVisibility(View.VISIBLE);
////                if (dataList2.get(position) != null) {
////                    holder.txtSub1.setText(dataList2.get(position));
////                }
//                if (dataList2.get(position) != null) {
//                    holder.txtSub1.setVisibility(View.VISIBLE);
//                    holder.txtSub1.setText("Subject : " + dataList2.get(position));
//                } else {
//                    //holder.txtSub1.setText("Subject : -");
//                    holder.txtSub1.setVisibility(View.GONE);
//                }
//                //if (teacherList != null && teacherList.size() > 0) {
//                if (teacherList2 != null && teacherList2.get(position) != null) {
//                    holder.txtName1.setVisibility(View.VISIBLE);
//                    holder.txtName1.setText(teacherList2.get(position));
//                } else {
//                    // holder.txtName1.setText("Teacher: -");
//                    holder.txtName1.setVisibility(View.GONE);
//                }
//                if (typeList2 != null && typeList2.get(position) != null) {
//                    holder.txtType1.setVisibility(View.VISIBLE);
//                    holder.txtType1.setText("Type: " + typeList2.get(position));
//                } else {
//                    // holder.txtType1.setText("Type: -");
//                    holder.txtType1.setVisibility(View.GONE);
//                }
//                if (batchList2 != null && batchList2.get(position) != null) {
//                    holder.txtBatch1.setVisibility(View.VISIBLE);
//                    holder.txtBatch1.setText("Batch: " + batchList2.get(position));
//                } else {
//                    // holder.txtBatch1.setText("Batch: -");
//                    holder.txtBatch1.setVisibility(View.GONE);
//                }
//
//
//                if (remarkList2 != null && remarkList2.get(position) != null) {
//                    holder.txtRemark1.setVisibility(View.VISIBLE);
//                    holder.txtRemark1.setText("Remarks: " + remarkList2.get(position));
//                } else {
//                    //holder.txtRemark1.setText("Remarks: -");
//                    holder.txtRemark1.setVisibility(View.GONE);
//                }
//
//
//            } else {
//                //holder.lytData1.setVisibility(View.GONE);
//            }


            int i = new Random().nextInt(254);

            GradientDrawable shape = new GradientDrawable();
            shape.setShape(GradientDrawable.OVAL);
            shape.setColor(Color.parseColor("#" + Constants.mColors[new Random().nextInt(254)]));
            holder.view.setBackground(shape);

        }

    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (data != null && data.getTotal_periods() != null) {
            size = Integer.parseInt(data.getTotal_periods());
        }
        return size;
    }

    public void setTeacherList(ArrayList<String> teacherList) {
        this.teacherList = teacherList;
    }

    public void setTeacherList2(ArrayList<String> teacherList2) {
        this.teacherList2 = teacherList2;
    }

    public void setSubList(ArrayList<String> dataList) {
        this.dataList = dataList;
    }

    public void setSubList2(ArrayList<String> dataList2) {
        this.dataList2 = dataList2;
    }

    public void setData(TimeTableBean data) {
        this.data = data;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.view)
        TextView view;
        @BindView(R.id.txtSub)
        TextView txtSub;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtBreak)
        TextView txtBreak;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.lytBreak)
        CardView lytBreak;
        @BindView(R.id.lytNoDetail)
        CardView lytNoDetail;
        @BindView(R.id.lytData)
        RelativeLayout lytData;
        @BindView(R.id.parent)
        RelativeLayout parent;
        @BindView(R.id.txtType)
        TextView txtType;
        @BindView(R.id.txtBatch)
        TextView txtBatch;
        @BindView(R.id.txtRemark)
        TextView txtRemark;


        @BindView(R.id.view1)
        TextView view1;
        @BindView(R.id.txtSub1)
        TextView txtSub1;
        @BindView(R.id.txtName1)
        TextView txtName1;

        @BindView(R.id.txtSub2)
        TextView txtSub2;
        @BindView(R.id.txtName2)
        TextView txtName2;

        //    @BindView(R.id.txtBreak1)
//    TextView txtBreak;//    @BindView(R.id.lytBreak)
//    CardView lytBreak;
//    @BindView(R.id.lytNoDetail)
//    CardView lytNoDetail;
//    @BindView(R.id.lytData)
//    RelativeLayout lytData;
        @BindView(R.id.lytData1)
        RelativeLayout lytData1;
        @BindView(R.id.lytData2)
        RelativeLayout lytData2;
        @BindView(R.id.txtType1)
        TextView txtType1;
        @BindView(R.id.txtBatch1)
        TextView txtBatch1;
        @BindView(R.id.txtRemark1)
        TextView txtRemark1;

        @BindView(R.id.txtSubsssss)
        TextView txtSubsssss;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
