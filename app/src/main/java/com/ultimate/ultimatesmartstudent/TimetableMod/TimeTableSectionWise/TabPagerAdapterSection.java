package com.ultimate.ultimatesmartstudent.TimetableMod.TimeTableSectionWise;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


public class TabPagerAdapterSection extends FragmentPagerAdapter {

    private final String class_id;
    private final String section_id;
    private final Bundle bundle;
    int tabCount;
    // tab titles
    private String[] tabTitles = new String[]{"Mon", "Tue", "Wed","Thu","Fri","Sat"};

    public TabPagerAdapterSection(FragmentManager fm, String class_id, String section_id) {
        super(fm);
        this.tabCount = 6;
        this.class_id=class_id;
        this.section_id=section_id;
        bundle = new Bundle();
        bundle.putString("class_id",class_id);
        bundle.putString("section_id",section_id);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MondayFragmentSection tab1 = new MondayFragmentSection();
                tab1.setArguments(bundle);
                return tab1;

            case 1:
                TueFragmentSection tab2 = new TueFragmentSection();
                tab2.setArguments(bundle);
                return tab2;

            case 2:
                WedFragmentSection tab3 = new WedFragmentSection();
                tab3.setArguments(bundle);
                return tab3;

            case 3:
                ThurFragmentSection tab4 = new ThurFragmentSection();
                tab4.setArguments(bundle);
                return tab4;

            case 4:
                FriFragmentSection tab5 = new FriFragmentSection();
                tab5.setArguments(bundle);
                return tab5;

            case 5:
                SatFragmentSection tab6 = new SatFragmentSection();
                tab6.setArguments(bundle);
                return tab6;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
