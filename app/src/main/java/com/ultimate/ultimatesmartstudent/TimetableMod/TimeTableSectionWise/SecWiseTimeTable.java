package com.ultimate.ultimatesmartstudent.TimetableMod.TimeTableSectionWise;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartstudent.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecWiseTimeTable extends AppCompatActivity {
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;

    Animation animation;
    private TimeTableBean timeData;
    private String class_id,section_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet_chart);
        ButterKnife.bind(this);
        //Tab layout
        txtTitle.setText("Timetable");
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
//        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        tablayout.setupWithViewPager(viewPager);
        final PagerAdapter adapter = new TabPagerAdapterSection
                (getSupportFragmentManager(), class_id,section_id);
        viewPager.setAdapter(adapter);
    }

    @OnClick(R.id.imgBackmsg)
    public void callBack() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    public void setTimeData(TimeTableBean timeData) {
        this.timeData = timeData;
    }
}