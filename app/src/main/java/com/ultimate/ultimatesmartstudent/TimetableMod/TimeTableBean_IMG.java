package com.ultimate.ultimatesmartstudent.TimetableMod;

import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TimeTableBean_IMG {


    /**
     * id : 0
     * comment :
     * pdf_file : office_admin/upload/ebooks/timetb_05092020_030502.pdf
     * date : 2020-05-09
     * class_id : 3
     * section : 2
     * section_name : Section B
     * class_name : UKG
     */

    private String id;
    private String comment;
    private String pdf_file;
    private String date;
    private String class_id;
    private String section;
    private String section_name;
    private String class_name;
    /**
     * image : office_admin/images/image.jpg
     */

    private ArrayList<String> image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }


    public static ArrayList<TimeTableBean_IMG> parseTimeTableArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<TimeTableBean_IMG>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                TimeTableBean_IMG p = parseTimeTableObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static TimeTableBean_IMG parseTimeTableObject(JSONObject jsonObject) {
        TimeTableBean_IMG casteObj = new TimeTableBean_IMG();
        try {
            if (jsonObject.has("id")) {
                casteObj.setId(jsonObject.getString("id"));
            }

            if (jsonObject.has("pdf_file") && !jsonObject.getString("pdf_file").isEmpty() && !jsonObject.getString("pdf_file").equalsIgnoreCase("null")) {
                casteObj.setPdf_file(Constants.getImageBaseURL()+jsonObject.getString("pdf_file"));
            }
            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }

            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }

            if (jsonObject.has("comment") && !jsonObject.getString("comment").isEmpty() && !jsonObject.getString("comment").equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString("comment"));
            }
            if (jsonObject.has("date") && !jsonObject.getString("date").isEmpty() && !jsonObject.getString("date").equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString("date"));
            }

            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }

            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("image") && jsonObject.get("image") instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray("image");
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return casteObj;
    }


    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }
}
