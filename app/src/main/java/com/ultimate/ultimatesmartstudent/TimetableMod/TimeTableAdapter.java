package com.ultimate.ultimatesmartstudent.TimetableMod;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.Viewholder> {
    ArrayList<TimeTableBean_IMG> time_list;
    TimtableCallbacks mAdaptercall;
    Context mContext;
    Animation animation;

    public TimeTableAdapter(ArrayList<TimeTableBean_IMG> time_list, Context mContext, TimtableCallbacks mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.time_list = time_list;
        this.mContext = mContext;
    }

    public interface TimtableCallbacks {
        public void time_viewImg(TimeTableBean_IMG timeTableBean);
        public void time_viewPdf(TimeTableBean_IMG timeTableBean);
    }


    public void setTime_list(ArrayList<TimeTableBean_IMG> time_list) {
        this.time_list = time_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView u_date,homeTopic,sub;
        public ImageView image_file, audio_file, pdf_file;
        public RelativeLayout withfile;
        @BindView(R.id.nofile)
        RelativeLayout nofile;
        @BindView(R.id.d_lyt)
        RelativeLayout d_lyt;
        @BindView(R.id.empty_file)
        ImageView empty_file;
        @BindView(R.id.id)
        TextView id;
        public ImageView trash, update;
        @BindView(R.id.tap_view_pdf)
        TextView tap_view_pdf;
        @BindView(R.id.tap_view_pic)
        TextView tap_view_pic;
        @BindView(R.id.tap_view_audio)
        TextView tap_view_audio;


        public Viewholder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);
        }
    }


    @NonNull
    @Override
    public TimeTableAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeada_lay, parent, false);
        Viewholder viewholder=new TimeTableAdapter.Viewholder(view);
        return viewholder;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onBindViewHolder(@NonNull TimeTableAdapter.Viewholder holder, int position) {




        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
        if (time_list.get(position).getClass_name() != null){
            holder.sub.setText(time_list.get(position).getClass_name()+"("+time_list.get(position).getSection_name()+")");
        }else{
            holder.sub.setText("Not Mentioned");
        }


        if (time_list.get(position).getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("tb-"+time_list.get(position).getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

//        holder.homeTopic.setText(hq_list.get(position).getComment());

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(time_list.get(position).getDate());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.u_date.setText(Utils.getDateFormated(time_list.get(position).getDate()));
        }

        //   holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getDate()));

        if (time_list.get(position).getPdf_file() != null){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.VISIBLE);
            holder.empty_file.setVisibility(View.GONE);
            holder.pdf_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tap_view_pdf.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.time_viewPdf(time_list.get(position));
                    }
                }
            });
        }else {
            holder.empty_file.setVisibility(View.GONE);
            holder.image_file.setVisibility(View.VISIBLE);
            Picasso.get().load(time_list.get(position).getImage().get(0)).placeholder(mContext.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            holder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mAdaptercall!= null){
                        mAdaptercall.time_viewImg(time_list.get(position));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return time_list.size();
    }


}
