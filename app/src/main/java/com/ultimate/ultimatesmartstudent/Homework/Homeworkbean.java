package com.ultimate.ultimatesmartstudent.Homework;

import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Homeworkbean {

    private static String ID="id";
    private static String IMAGE="image";
    private static String COMMENT="comment";
    private static String H_DATE="date";
    /**
     * id : 2
     * comment : asfgsdfgdgegdfgdfgfg
     * image : office_admin/images/homework/hw_03272018_080342.jpg
     * date : 2018-03-27
     * class : 10
     */


    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    private String image_tag;

    public ArrayList<String> getMulti_image() {
        return multi_image;
    }

    public void setMulti_image(ArrayList<String> multi_image) {
        this.multi_image = multi_image;
    }

    private ArrayList<String> multi_image;

    private String id;
    private String comment;
    private String image;
    private String date;
    private String section;
    private String section_name;
    private String pdf_file;
    private String recordingsfile;
    private String subject_name;

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getRecordingsfile() {
        return recordingsfile;
    }

    public void setRecordingsfile(String recordingsfile) {
        this.recordingsfile = recordingsfile;
    }

    /**
     * class_id : 10
     * class_name : 6TH
     */

    private String class_id;
    private String class_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public static ArrayList<Homeworkbean> parseHWArray(JSONArray arrayObj) {
        ArrayList<Homeworkbean> list = new ArrayList<Homeworkbean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Homeworkbean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Homeworkbean parseHWObject(JSONObject jsonObject) {
        Homeworkbean casteObj = new Homeworkbean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString(IMAGE));
            }
            if (jsonObject.has(COMMENT) && !jsonObject.getString(COMMENT).isEmpty() && !jsonObject.getString(COMMENT).equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString(COMMENT));
            }
            if (jsonObject.has(H_DATE) && !jsonObject.getString(H_DATE).isEmpty() && !jsonObject.getString(H_DATE).equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString(H_DATE));
            }
            if (jsonObject.has("section") && !jsonObject.getString("section").isEmpty() && !jsonObject.getString("section").equalsIgnoreCase("null")) {
                casteObj.setSection(jsonObject.getString("section"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }

            if (jsonObject.has("pdf_file") && !jsonObject.getString("pdf_file").isEmpty() && !jsonObject.getString("pdf_file").equalsIgnoreCase("null")) {
                casteObj.setPdf_file(Constants.getImageBaseURL()+jsonObject.getString("pdf_file"));
            }
            if (jsonObject.has("recordingsfile") && !jsonObject.getString("recordingsfile").isEmpty() && !jsonObject.getString("recordingsfile").equalsIgnoreCase("null")) {
                casteObj.setRecordingsfile(Constants.getImageBaseURL()+jsonObject.getString("recordingsfile"));
            }
            if (jsonObject.has("subject_name") && !jsonObject.getString("subject_name").isEmpty() && !jsonObject.getString("subject_name").equalsIgnoreCase("null")) {
                casteObj.setSubject_name(jsonObject.getString("subject_name"));
            }

            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                casteObj.setImage_tag(jsonObject.getString("image_tag"));
            }
            if (jsonObject.has("multi_image") && jsonObject.get("multi_image") instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray("multi_image");
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setMulti_image(img_arrs);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
