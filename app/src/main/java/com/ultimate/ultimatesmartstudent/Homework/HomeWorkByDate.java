package com.ultimate.ultimatesmartstudent.Homework;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Classwork.ViewPagerAdapter;
import com.ultimate.ultimatesmartstudent.E_book.Ebook;
import com.ultimate.ultimatesmartstudent.Gallery.IMGGridView;
import com.ultimate.ultimatesmartstudent.NoticeMod.NoticeActivity;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.ErpProgress;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Webview.FileDownloader;
import com.ultimate.ultimatesmartstudent.Webview.ViewPdfActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class HomeWorkByDate extends AppCompatActivity implements Homeworkadapter.Mycallback,TextToSpeech.OnInitListener {
    String image_url = "", school = "";
    String textholder="";
    TextToSpeech textToSpeech;
    private Homeworkadapter adapter;
    ArrayList<Homeworkbean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    RelativeLayout parent;
    ImageView imgback;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id, sub_name;
    String folder_main = "HomeWork";
    Animation animation;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.cal_lyt)
    LinearLayout cal_lyt;
    @BindView(R.id.cal_img)
    ImageView cal_img;
    @BindView(R.id.cal_text)
    TextView cal_text;
    public String from_date = "";
    private int loaded = 0;
    CommonProgress commonProgress;
    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gdstest);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.today)+" "+getString(R.string.homework));
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        imgback = (ImageView) findViewById(R.id.imgBackmsg);
        commonProgress = new CommonProgress(this);
        cal_lyt.setVisibility(View.VISIBLE);
        textToSpeech = new TextToSpeech(HomeWorkByDate.this, this);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        layoutManager = new LinearLayoutManager(HomeWorkByDate.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new Homeworkadapter(hwList, HomeWorkByDate.this, this);
        recyclerview.setAdapter(adapter);

        school = User.getCurrentUser().getSchoolData().getName();
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        from_date = timeStampFormat.format(myDate);
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(myDate);
        cal_text.setText(dateString);
        fetchhwlist(from_date);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgback.startAnimation(animation);
                finish();
            }
        });
    }

    @OnClick(R.id.cal_lyt)
    public void cal_lyttttt() {
        cal_img.startAnimation(animation);

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        fetchhwlist(from_date);
//    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchhwlist(from_date);
        }
    };

    private void fetchhwlist(String from_date) {
//        if (loaded == 0) {
//            UltimateProgress.showProgressBar(this, "Please Wait.....!");
//        }
//        loaded++;
        commonProgress.show();
        // UltimateProgress.showProgressBar(this, "Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("from_date", from_date);
        params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("section_id", User.getCurrentUser().getSection_id());

        Log.e("from_date", from_date);
        Log.e("class_id", User.getCurrentUser().getClass_id());
        if (User.getCurrentUser().getSection_id() != null) {
            Log.e("section_id", User.getCurrentUser().getSection_id());
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.gdshomeworkbydatreurl, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            UltimateProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {

                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hw_data");
                    hwList = Homeworkbean.parseHWArray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        //setanimation on adapter...
                        recyclerview.getAdapter().notifyDataSetChanged();
                        recyclerview.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText(getString(R.string.t_entries) + " " + String.valueOf(hwList.size()));

                    } else {
                        totalRecord.setText(getString(R.string.t_entries) + " 0");
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText(getString(R.string.t_entries) + " 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };



    @Override
    public void onMethod_Image_callback(Homeworkbean data) {
        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(data.getComment());
        ViewPagerAdapter mAdapter = new  ViewPagerAdapter(this,data.getMulti_image(),"homework");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

//
//        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
//        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
//        mBottomSheetDialog.setContentView(sheetView);
//        final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);
//        mBottomSheetDialog.setCancelable(true);
//
//        Animation animation;
//        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
//        ImageView imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);
//        TextView title = (TextView) sheetView.findViewById(R.id.title);
//        TextView path = (TextView) sheetView.findViewById(R.id.path);
//        path.setText("Internal Storage/Pictures/"+school+"/hw_img_saved.jpg");
//        TextView imgDownload = (TextView) sheetView.findViewById(R.id.imgDownload);
//        title.setText(obj.getComment());
//        Picasso.get().load(obj.getImage()).placeholder(getResources().getDrawable(R.drawable.logo)).into(imgShow);
//        imgDownload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                imgDownload.startAnimation(animation);
//             //   UltimateProgress.showProgressBar(HomeWorkByDate.this, "downloading...");
//               // Picasso.with(HomeWorkByDate.this).load(obj.getImage()).into(imgTraget);
//                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
//                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
//                Bitmap bitmap = bitmapDrawable.getBitmap();
//                saveImageToGallery(bitmap);
//            }
//        });
//        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
//        btnNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnNo.startAnimation(animation);
//                mBottomSheetDialog.dismiss();
//            }
//        });
//
//        mBottomSheetDialog.show();

        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getMulti_image().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        ImageView imageView=(ImageView) warningDialog.findViewById(R.id.img);

        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.VISIBLE);

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=data.getMulti_image();
                Intent intent = new Intent(HomeWorkByDate.this, IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "homework");
                intent.putExtra("title", getString(R.string.homework));
                intent.putExtra("sub", "Homework_id:- "+data.getId());
                startActivity(intent);
            }
        });

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getMulti_image().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onMethod_recording_callback(Homeworkbean homeworkbean) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(homeworkbean.getRecordingsfile())));
    }

    @Override
    public void onMethod_pdf_call_back(Homeworkbean homeworkbean) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(HomeWorkByDate.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.homework_choice_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(HomeWorkByDate.this, R.anim.btn_blink_animation);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        ImageView image= (ImageView) sheetView.findViewById(R.id.image);
        // Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        title.setText(homeworkbean.getSubject_name()+"("+homeworkbean.getComment()+")");
        //image.getResources().setDrawable();
        Picasso.get().load(R.drawable.ebook).into(image);

        Button sub_wise= (Button) sheetView.findViewById(R.id.sub_wise);
        sub_wise.setBackground(getResources().getDrawable(R.drawable.absent_bg));
        sub_wise.setText("View!");
        sub_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub_wise.startAnimation(animation);
                Intent intent = new Intent(getBaseContext(), ViewPdfActivity.class);
                intent.putExtra("id", homeworkbean.getPdf_file());
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
        Button date_wise= (Button) sheetView.findViewById(R.id.date_wise);
        date_wise.setBackground(getResources().getDrawable(R.drawable.present_bg));
        date_wise.setText("Save!");
        date_wise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_wise.startAnimation(animation);
                // UltimateProgress.showProgressBar(SubWiseHW.this,"Please wait...");
                String mUrl=homeworkbean.getPdf_file();
                // new DownloadFile().execute(mUrl);
                String path_name ="/storage/Android/data/com.ultimate.ultimatesmartstudent/files/document/"+folder_main+"/HW_file_saved.pdf";

                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(mUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        .setAllowedOverRoaming(false)
                        .setTitle("Hw_pdf.pdf")
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Hw_pdf_"+System.currentTimeMillis()+".pdf");

                downloadManager.enqueue(request);
                Toast.makeText(getApplicationContext(), "Pdf Downloaded Successfully", Toast.LENGTH_LONG).show();
                mBottomSheetDialog.dismiss();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();

    }

    @Override
    public void onImgSpeechCallback(Homeworkbean obj) {
        textholder = obj.getComment();
        TextToSpeechFunction();
    }

    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);

    }

    @Override
    public void onDestroy() {

        textToSpeech.shutdown();

        super.onDestroy();
    }


    @Override
    public void onInit(int Text2SpeechCurrentStatus) {

        if (Text2SpeechCurrentStatus == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            TextToSpeechFunction();
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.Q)
    private Uri savePDFFile(HomeWorkByDate homeWorkByDate, InputStream in, String mimeType, String displayName, String subFolder) {
        String relativeLocation = Environment.DIRECTORY_DOCUMENTS;

        if (!TextUtils.isEmpty(subFolder)) {
            relativeLocation += File.separator + subFolder;
        }

        final ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, displayName);
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, relativeLocation);
        contentValues.put(MediaStore.Video.Media.TITLE, "SomeName");
        contentValues.put(MediaStore.Video.Media.DATE_ADDED, System.currentTimeMillis() / 1000);
        contentValues.put(MediaStore.Video.Media.DATE_TAKEN, System.currentTimeMillis());
        final ContentResolver resolver = getContentResolver();
        OutputStream stream = null;
        Uri uri = null;

        try {
            final Uri contentUri = MediaStore.Files.getContentUri("external");
            uri = resolver.insert(contentUri, contentValues);
            ParcelFileDescriptor pfd;
            try {
                assert uri != null;
                pfd = getContentResolver().openFileDescriptor(uri, "w");
                assert pfd != null;
                FileOutputStream out = new FileOutputStream(pfd.getFileDescriptor());

                byte[] buf = new byte[4 * 1024];
                int len;
                while ((len = in.read(buf)) > 0) {

                    out.write(buf, 0, len);
                }
                out.close();
                in.close();
                pfd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            contentValues.clear();
            contentValues.put(MediaStore.Video.Media.IS_PENDING, 0);
            getContentResolver().update(uri, contentValues, null, null);
            stream = resolver.openOutputStream(uri);
            if (stream == null) {
                throw new IOException("Failed to get output stream.");
            }else {
                Toast.makeText(this, "Pdf Saved", Toast.LENGTH_SHORT).show();
            }
            return uri;
        } catch (IOException e) {
            // Don't leave an orphan entry in the MediaStore
            resolver.delete(uri, null, null);
            try {
                throw e;
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return uri;
    }




    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf


            String relativeLocation = Environment.DIRECTORY_DOCUMENTS;
//
//            if (!TextUtils.isEmpty(subFolder)) {
//                relativeLocation += File.separator + subFolder;
//            }

            File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), folder_main);

            //   File path = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), folder_main);


            path.mkdir();

            File file = new File(path, folder_main + System.currentTimeMillis() + "saved.pdf");
            try{
                file.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, file);
            UltimateProgress.cancelProgressBar();
            return null;
        }
    }
}
