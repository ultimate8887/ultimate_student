package com.ultimate.ultimatesmartstudent.E_LearningMod;

import com.ultimate.ultimatesmartstudent.Leave_Mod.Leavelistbean;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Live_Class_Bean {


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    private String id;
    private String link;
    private String class_id;

    public static ArrayList<Live_Class_Bean> parseLive_Class_BeanArray(JSONArray jsonArray) {
        ArrayList<Live_Class_Bean> list = new ArrayList<Live_Class_Bean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Live_Class_Bean p = parseLive_Class_BeanObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static Live_Class_Bean parseLive_Class_BeanObject(JSONObject jsonObject) {
        Live_Class_Bean msgObj = new Live_Class_Bean();
        try {
            if (jsonObject.has("id")) {
                msgObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("link") && !jsonObject.getString("link").isEmpty() && !jsonObject.getString("link").equalsIgnoreCase("null")) {
                msgObj.setLink(jsonObject.getString("link"));
            }
            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                msgObj.setClass_id(jsonObject.getString("class_id"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;
    }


}
