package com.ultimate.ultimatesmartstudent.E_LearningMod;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeBaseActivity;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YoutubePlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener
        ,YouTubePlayer.PlaybackEventListener,YouTubePlayer.PlayerStateChangeListener {

    YouTubePlayerView youTubePlayerView;
//    Button start;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    OnlineclsBean hostlroombean;
    String video_id="";
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("video_data")) {
                Gson gson = new Gson();
                hostlroombean = gson.fromJson(getIntent().getExtras().getString("video_data"), OnlineclsBean.class);
                // txtTitle.setText(hostlroombean.getSub_name()+"("+hostlroombean.getClassname()+")");
            }
        }
         txtTitle.setText(hostlroombean.getEs_title());
         video_id=hostlroombean.getS_file();
         youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        // start = (Button) findViewById(R.id.start);
         youTubePlayerView.initialize(Constants.YOUTUBE_API,this);
      //  youTubePlayerView.setEnabled(false);


//         onInitializedListener=new YouTubePlayer.OnInitializedListener() {
//             @Override
//             public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
//
//             }
//
//             @Override
//             public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//
//             }
//         };
//
//         start.setOnClickListener(new View.OnClickListener() {
//             @Override
//             public void onClick(View v) {
//
//             }
//         });

    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }



    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

        if(!b) {

            youTubePlayer.loadVideo(video_id);
         //   youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
            youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                @Override
                public void onLoading() {

                }

                @Override
                public void onLoaded(String s) {

                }

                @Override
                public void onAdStarted() {

                }

                @Override
                public void onVideoStarted() {

                }

                @Override
                public void onVideoEnded() {
                    youTubePlayer.seekToMillis(0);
                    youTubePlayer.play();
                    youTubePlayer.pause();
                }

                @Override
                public void onError(YouTubePlayer.ErrorReason errorReason) {

                }
            });

        }
    }




    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onStopped() {

    }

    @Override
    public void onBuffering(boolean b) {

    }

    @Override
    public void onSeekTo(int i) {

    }

    @Override
    public void onLoading() {

    }

    @Override
    public void onLoaded(String s) {

    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onVideoStarted() {

    }

    @Override
    public void onVideoEnded() {

    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {

    }
}