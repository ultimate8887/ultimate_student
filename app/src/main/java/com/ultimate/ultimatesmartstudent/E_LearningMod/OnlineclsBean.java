package com.ultimate.ultimatesmartstudent.E_LearningMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OnlineclsBean {


    /**
     * id : 5
     * es_title : English class video
     * es_date : 2020-04-24
     * es_class : 9
     * classname : 6th
     * es_subid : 93
     * sub_name : ENGLISH
     * s_file : https://youtu.be/OGls12oQpH0
     */

    private String id;
    private String es_title;
    private String es_date;
    private String es_class;
    private String classname;
    private String es_subid;
    private String sub_name;
    private String s_file;

    public String getV_type() {
        return v_type;
    }

    public void setV_type(String v_type) {
        this.v_type = v_type;
    }

    private String v_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEs_title() {
        return es_title;
    }

    public void setEs_title(String es_title) {
        this.es_title = es_title;
    }

    public String getEs_date() {
        return es_date;
    }

    public void setEs_date(String es_date) {
        this.es_date = es_date;
    }

    public String getEs_class() {
        return es_class;
    }

    public void setEs_class(String es_class) {
        this.es_class = es_class;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getEs_subid() {
        return es_subid;
    }

    public void setEs_subid(String es_subid) {
        this.es_subid = es_subid;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getS_file() {
        return s_file;
    }

    public void setS_file(String s_file) {
        this.s_file = s_file;
    }

    public static ArrayList<OnlineclsBean> parseSyllabusArray(JSONArray arrayObj) {
        ArrayList<OnlineclsBean> list = new ArrayList<OnlineclsBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                OnlineclsBean p = parseSyllabusObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static OnlineclsBean parseSyllabusObject(JSONObject jsonObject) {
        OnlineclsBean casteObj = new OnlineclsBean();
        try {
            if (jsonObject.has("id")&& !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("es_subid") && !jsonObject.getString("es_subid").isEmpty() && !jsonObject.getString("es_subid").equalsIgnoreCase("null")) {
                casteObj.setEs_subid(jsonObject.getString("es_subid"));
            }
            if (jsonObject.has("s_file") && !jsonObject.getString("s_file").isEmpty() && !jsonObject.getString("s_file").equalsIgnoreCase("null")) {
                casteObj.setS_file(jsonObject.getString("s_file"));
            }

            if (jsonObject.has("es_title") && !jsonObject.getString("es_title").isEmpty() && !jsonObject.getString("es_title").equalsIgnoreCase("null")) {
                casteObj.setEs_title(jsonObject.getString("es_title"));
            }
            if (jsonObject.has("es_class") && !jsonObject.getString("es_class").isEmpty() && !jsonObject.getString("es_class").equalsIgnoreCase("null")) {
                casteObj.setEs_class(jsonObject.getString("es_class"));
            }
            if (jsonObject.has("classname") && !jsonObject.getString("classname").isEmpty() && !jsonObject.getString("classname").equalsIgnoreCase("null")) {
                casteObj.setClassname(jsonObject.getString("classname"));
            }
            if (jsonObject.has("sub_name") && !jsonObject.getString("sub_name").isEmpty() && !jsonObject.getString("sub_name").equalsIgnoreCase("null")) {
                casteObj.setSub_name(jsonObject.getString("sub_name"));
            }

            if (jsonObject.has("v_type") && !jsonObject.getString("v_type").isEmpty() && !jsonObject.getString("v_type").equalsIgnoreCase("null")) {
                casteObj.setV_type(jsonObject.getString("v_type"));
            }
            if (jsonObject.has("es_date") && !jsonObject.getString("es_date").isEmpty() && !jsonObject.getString("es_date").equalsIgnoreCase("null")) {
                casteObj.setEs_date(jsonObject.getString("es_date"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
