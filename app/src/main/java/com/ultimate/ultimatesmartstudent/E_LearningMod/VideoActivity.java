package com.ultimate.ultimatesmartstudent.E_LearningMod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.google.gson.Gson;

import com.ultimate.ultimatesmartstudent.AdapterClasses.AdapterOfSubjectList;
import com.ultimate.ultimatesmartstudent.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoActivity extends AppCompatActivity implements OnlineclasAdapter.Mycallback {


    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;


    @BindView(R.id.textView8)
    TextView textsubtitle;
    @BindView(R.id.spinnerGroups)
    Spinner SubjectSpinner;
    @BindView(R.id.totalRecord)
    TextView totalRecord;


    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    String  sub_id, sub_name;
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;

    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<OnlineclsBean> ebooklist = new ArrayList<>();
    OnlineclasAdapter adapter;
    Animation animation;
    CommonProgress commonProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.video));
        textsubtitle.setText(getString(R.string.f_subject));
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerView.setLayoutManager(new LinearLayoutManager(VideoActivity.this));
        adapter = new OnlineclasAdapter(ebooklist, VideoActivity.this, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //if (class_id != null && !class_id.isEmpty())
        getSubjectofClass();
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }


    private void getSubjectofClass() {
      //  UltimateProgress.showProgressBar(this,"Please Wait.....!");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", User.getCurrentUser().getClass_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            UltimateProgress.cancelProgressBar();
            if (error == null) {
                try {
                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));
                    AdapterOfSubjectList adaptersub = new AdapterOfSubjectList(VideoActivity.this, subjectList);
                    SubjectSpinner.setAdapter(adaptersub);
                    SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }
                            fetchSubWise(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void fetchSubWise(int i) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        if (i == 0){
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("v_type","video");
        } else {
            params.put("class_id", User.getCurrentUser().getClass_id());
            params.put("sub_id",sub_id);
            params.put("v_type","video");
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.VIEW_ONLINELINK, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        if (ebooklist != null) {
                            ebooklist.clear();
                        }
                        //  Toast.makeText(getApplicationContext(),"Success!",Toast.LENGTH_SHORT).show();
                        JSONArray jsonArray = jsonObject.getJSONArray("syllabus_data");
                        ebooklist = OnlineclsBean.parseSyllabusArray(jsonArray);
                        if (ebooklist.size() > 0) {
                            adapter.setSyllabusList(ebooklist);
                            //setanimation on adapter...
                            recyclerView.getAdapter().notifyDataSetChanged();
                            recyclerView.scheduleLayoutAnimation();
                            //-----------end------------
                            txtNorecord.setVisibility(View.GONE);
                            totalRecord.setText("Total Entries:- "+String.valueOf(ebooklist.size()));
                        } else {
                            totalRecord.setText("Total Entries:- 0");
                            adapter.setSyllabusList(ebooklist);
                            adapter.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText("Total Entries:- 0");
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                    txtNorecord.setVisibility(View.VISIBLE);
                    ebooklist.clear();
                    adapter.setSyllabusList(ebooklist);
                    adapter.notifyDataSetChanged();
                }
            }
        }, this, params);

    }



    @Override
    public void onMethodCallback(OnlineclsBean syllabusBean) {

    }

    @Override
    public void viewPdf(OnlineclsBean syllabusBean) {
        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://elms.xyzultimatesolution.com/index.php/bbb-room/php-programming/")));
        Gson gson = new Gson();
        String ebookdata = gson.toJson(syllabusBean, OnlineclsBean.class);
        Intent intent = new Intent(VideoActivity.this, PlayVideoActivity.class);
        intent.putExtra("video_data", ebookdata);
        startActivity(intent);

    }


}