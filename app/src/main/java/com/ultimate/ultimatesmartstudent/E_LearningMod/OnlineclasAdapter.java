package com.ultimate.ultimatesmartstudent.E_LearningMod;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnlineclasAdapter extends RecyclerView.Adapter<OnlineclasAdapter.Viewholder> {
    ArrayList<OnlineclsBean> sy_list;
    Mycallback mAdaptercall;
    Context mContext;
    Animation animation;
    public OnlineclasAdapter(ArrayList<OnlineclsBean> sy_list, Context mContext, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.onlincls_adpt_lyt, parent, false);
        OnlineclasAdapter.Viewholder viewholder = new OnlineclasAdapter.Viewholder(view);
        return viewholder;

    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onBindViewHolder(OnlineclasAdapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {


        holder.txttitle.setText(sy_list.get(position).getEs_title());
        // holder.txtwriter.setText(Utils.getDateFormated(sy_list.get(position).getEs_date()));
        if (sy_list.get(position).getV_type() != null) {
            String title = getColoredSpanned("Type:-", "#000000");
            String Name="";
            if (sy_list.get(position).getV_type().equalsIgnoreCase("video")) {
                Name = getColoredSpanned(" Video file" , "#1C8B3B");
                holder.video_img.setVisibility(View.VISIBLE);
                holder.you_img.setVisibility(View.GONE);
                holder.play.setAnimation(R.raw.video);
            }else{
                Name = getColoredSpanned(" Youtube link", "#F55B53");
                holder.you_img.setVisibility(View.VISIBLE);
                holder.video_img.setVisibility(View.GONE);
                holder.play.setAnimation(R.raw.youtube);
            }

            holder.txtSub.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.txtSub.setVisibility(View.GONE);
        }

        if (sy_list.get(position).getClassname() != null) {
            String title = getColoredSpanned(sy_list.get(position).getSub_name(), "#000000");
            String Name="";

            Name = getColoredSpanned("-"+sy_list.get(position).getClassname(), "#5A5C59");

            holder.txtwriter.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.txtwriter.setVisibility(View.GONE);
        }

        if (sy_list.get(position).getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("el-"+sy_list.get(position).getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(sy_list.get(position).getEs_date());
        if (check.equalsIgnoreCase(dateString)){

            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

        }else {
            holder.u_date.setText(Utils.getDateFormated(sy_list.get(position).getEs_date()));
        }

        holder.txtPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
                holder.play.startAnimation(animation);
                if (mAdaptercall != null) {
                    mAdaptercall.viewPdf(sy_list.get(position));
                }
            }
        });
        if (sy_list.get(position).getS_file() != null) {
            holder.txtPdf.setVisibility(View.VISIBLE);
        } else {
            holder.txtPdf.setVisibility(View.GONE);
        }


    }

    public interface Mycallback {
        public void onMethodCallback(OnlineclsBean syllabusBean);

        public void viewPdf(OnlineclsBean syllabusBean);
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<OnlineclsBean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.head)
        RelativeLayout txtPdf;
        @BindView(R.id.classess)
        TextView txttitle;
        @BindView(R.id.u_date)
        TextView u_date;
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.subject)
        TextView txtSub;
        @BindView(R.id.homeTopic)
        TextView txtwriter;
        @BindView(R.id.imgStud)
        ImageView video_img;
        @BindView(R.id.imgStud1)
        ImageView you_img;
        @BindView(R.id.play)
        LottieAnimationView play;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
