package com.ultimate.ultimatesmartstudent.Webview;

import androidx.appcompat.app.AppCompatActivity;


import android.annotation.SuppressLint;
import android.content.Context;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;


import android.os.Handler;
import android.util.Log;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ViewPdfActivity extends AppCompatActivity {

    private WebView pdfView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Animation animation;
    CommonProgress commonProgress;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pdf);
        ButterKnife.bind(this);

        commonProgress = new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        pdfView = findViewById(R.id.webView);

        // Retrieve the PDF URL from the intent
        if (getIntent().getExtras() != null) {
            String pdfUrl = getIntent().getStringExtra("id");
            String title = getIntent().getStringExtra("title");
            txtTitle.setText(title);
            loadPdfWithRetry(pdfView, pdfUrl);
        } else {
            // Handle the case where no URL is provided
            finish(); // Close the activity if no URL is provided
        }
    }

    @OnClick(R.id.imgBackmsg)
    public void onback() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    // This function checks network availability
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // This function attempts to load the PDF with a retry mechanism
    private void loadPdfWithRetry(final WebView webView, final String url) {
        if (webView != null) {
            if (isNetworkAvailable()) {
                commonProgress.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setupWebViewWithUrl(webView, url);
                    }
                }, 1000); // Delay of 1 second to ensure WebView is ready
            } else {
                Toast.makeText(this, "No internet connection. Please try again later.", Toast.LENGTH_LONG).show();
            }
        }
    }

    // This function configures the WebView to display the PDF.
    private void setupWebViewWithUrl(WebView webView, String url) {
        if (webView != null) {
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    commonProgress.cancel(); // Hide loader
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    super.onReceivedError(view, errorCode, description, failingUrl);
                    commonProgress.cancel();
                    // Toast.makeText(ViewPdfActivity.this, "Failed to load PDF. Please try again.", Toast.LENGTH_SHORT).show();
                    // Retry mechanism: Reload the page if there's an error
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadPdfWithRetry(webView, url);
                        }
                    }, 2000); // Retry after 2 seconds
                }
            });

            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true); // Enable JavaScript
            webSettings.setSupportZoom(true); // Enable zoom controls
            webSettings.setBuiltInZoomControls(true); // Show built-in zoom controls
            webSettings.setDisplayZoomControls(false); // Hide the default zoom controls UI (optional)
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setUseWideViewPort(true);

            // Load the PDF URL
            webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + url);
        }
    }
}

