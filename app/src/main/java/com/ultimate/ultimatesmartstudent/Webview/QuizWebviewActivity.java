package com.ultimate.ultimatesmartstudent.Webview;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizWebviewActivity extends AppCompatActivity {
    String s_key, student_id, class_id;
    WebView myWebView;
    CommonProgress commonProgress;
    @BindView(R.id.imgBackmsg)
    ImageView imgback;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_webview);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        s_key = User.getCurrentUser().getSchoolData().getFi_school_id();
        student_id = User.getCurrentUser().getId();
        class_id = User.getCurrentUser().getClass_id();
        txtTitle.setText("Online Quiz");
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(QuizWebviewActivity.this);
                builder1.setMessage("Are you sure, you want to exit? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
        myWebView = (WebView) findViewById(R.id.webview);
        //  myWebView = new WebView(this);
        // setContentView(myWebView);
        myWebView.requestFocus();
        myWebView.getSettings().setDefaultTextEncodingName("utf-8");
        myWebView.setVerticalScrollBarEnabled(true);
        myWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                   return true;
            }
        });
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setLightTouchEnabled(true);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.setWebChromeClient(new WebChromeClient() {
                                         @Override
                                         public void onProgressChanged(WebView view, int newProgress) {
                                             if (newProgress < 100) {
                                                 commonProgress.show();
                                             }
                                             if (newProgress == 100) {
                                                 commonProgress.dismiss();
                                             }
                                         }
                                     }
        );


        String link = "https://ultimatesolutiongroup.com/onlinetest/studentHome.php?s_key=" + s_key + "&student_id=" + student_id + "&class_id=" + class_id + "";
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));


        myWebView.loadUrl(link);
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
           // myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }



}