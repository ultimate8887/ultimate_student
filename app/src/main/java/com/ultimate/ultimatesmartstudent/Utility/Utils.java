package com.ultimate.ultimatesmartstudent.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.FeeModule.FeeDetailsActivity;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.TimetableMod.ViewTimeTable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.NO_WRAP);
    }

    public static String encodeFileToBase64Binary(String Path)
            throws IOException {

        File file = new File(Path);
        byte[] bytes = loadFile(file);
        String encodedString = Base64.encodeToString(bytes, 1);
        return encodedString;
    }
    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }


    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }

    public static Bitmap getBitmapFromUri(Context context, Uri selectedImage, int requiredSize) {

        Bitmap bitmap = null;
        Bitmap rotatedBitmap = null;
        if (Build.VERSION.SDK_INT < 19) {
            String selectedImagePath = getRealPathFromURI(context, selectedImage);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(selectedImagePath, options);
            bitmap = BitmapFactory.decodeFile(selectedImagePath, decodeFile(options, requiredSize));
            try {
                ExifInterface exif = new ExifInterface(selectedImagePath);
                String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
                int rotationAngle = 0;
                switch (orientation) {
                    case ExifInterface.ORIENTATION_NORMAL:
                        rotationAngle = 0;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotationAngle = 90;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotationAngle = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotationAngle = 270;
                        break;
                    case ExifInterface.ORIENTATION_UNDEFINED:
                        rotationAngle = 0;
                        break;
                    default:
                        rotationAngle = 90;
                }

                Matrix matrix = new Matrix();
                matrix.setRotate(rotationAngle, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, options.outWidth, options.outHeight, matrix, true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            ParcelFileDescriptor parcelFileDescriptor;
            try {
                parcelFileDescriptor = context.getContentResolver().openFileDescriptor(selectedImage, "r");
                FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
                bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, decodeFile(options, requiredSize));
                parcelFileDescriptor.close();
                try {
                    ExifInterface exif = new ExifInterface(getRealPathFromURI(context, selectedImage));
                    String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                    int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
                    int rotationAngle = 0;
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_NORMAL:
                            rotationAngle = 0;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotationAngle = 90;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotationAngle = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotationAngle = 270;
                            break;
                        case ExifInterface.ORIENTATION_UNDEFINED:
                            rotationAngle = 0;
                            break;
                        default:
                            rotationAngle = 90;
                    }
                    Matrix matrix = new Matrix();
                    matrix.setRotate(rotationAngle, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
                    rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, options.outWidth, options.outHeight, matrix, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (rotatedBitmap == null) {
            return bitmap;
        } else {
            return rotatedBitmap;
        }
    }


    private static String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            result = cursor.getString(0);
            cursor.close();
        }
        return result;
    }

    private static BitmapFactory.Options decodeFile(BitmapFactory.Options options, int requiredSize) {
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = options.outWidth, height_tmp = options.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < requiredSize && height_tmp < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize

        options.inJustDecodeBounds = false;
        options.inSampleSize = scale;

        return options;
    }
    public static String getTime(String datestring) {
        String text_date = "";
        try {
            Date date = new SimpleDateFormat("HH:mm:ss").parse(datestring);
            text_date = new SimpleDateFormat("hh:mm a").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return text_date;
    }

//    public static String setNaviHeaderData(String datestring) {
//        String text_date = "";
//        try {
//            Date date = new SimpleDateFormat("HH:mm:ss").parse(datestring);
//            text_date = new SimpleDateFormat("hh:mm a").format(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return text_date;
//    }


//    public static void showSnackBar(String errorMsg, View parent) {
////        Snackbar snack = Snackbar.make(parent, errorMsg, Snackbar.LENGTH_LONG);
////        View view = snack.getView();
////        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
////        tv.setTextColor(Color.WHITE);
//        snack.show();
//    }

    public static String getYearOnlyNEW(String datestring) {
        String text_date = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datestring);
            text_date = new SimpleDateFormat("yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return text_date;
    }

    public static String getDateFormated_date(String datestring) {
        String text_date = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datestring);
            text_date = new SimpleDateFormat("yyyy-MM-dd").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return text_date;
    }


    public static String getDateFormated_date_new(String datestring) {
        String text_date = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datestring);
            text_date = new SimpleDateFormat("dd, MMM").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return text_date;
    }


    public static String getDateOnly(String datestring) {
        String text_date = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datestring);
            text_date = new SimpleDateFormat("EEEE").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return text_date;
    }


    public static String getDateFormated(String datestring) {
        String text_date = "";
        try {
            if (datestring != null && !datestring.isEmpty() && !datestring.equals("0000-00-00")) {
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datestring);
                text_date = new SimpleDateFormat("dd MMM, yyyy").format(date);
            }else{
                text_date="Not found";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return text_date;
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isConnectionAvailable(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    public static String dateFormat(String d_date) {
        String date = null;
        try {
            Date dte = new SimpleDateFormat("yyyy-MM-dd").parse(d_date);
            date = new SimpleDateFormat("dd MMM, yyyy").format(dte);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static String stringWithFirstcap(String myString) {
        String upperString;
        if (myString != null) {
            myString.toLowerCase();
            upperString = myString.substring(0, 1).toUpperCase() + myString.substring(1);
        } else {
            upperString = "";
        }
        return upperString;
    }

    public static int getMaxDateByMonth(int i) {
        switch (i) {
            case 1:
                return 31;
            case 2:
                return 28;
            case 3:
                return 31;
            case 4:
                return 30;
            case 5:
                return 31;
            case 6:
                return 30;
            case 7:
                return 31;
            case 8:
                return 31;
            case 9:
                return 30;
            case 10:
                return 31;
            case 11:
                return 30;
            case 12:
                return 31;
        }
        return 31;
    }
    public static int getMaxDateByMonthleap(int i) {
        switch (i) {
            case 1:
                return 31;
            case 2:
                return 29;
            case 3:
                return 31;
            case 4:
                return 30;
            case 5:
                return 31;
            case 6:
                return 30;
            case 7:
                return 31;
            case 8:
                return 31;
            case 9:
                return 30;
            case 10:
                return 31;
            case 11:
                return 30;
            case 12:
                return 31;
        }
        return 31;
    }

    public static void setNaviHeaderData(TextView stu_name, TextView class_namessss, CircularImageView navi_profile,String image_url, FragmentActivity activity) {



//        if (User.getCurrentUser().getProfile() != null) {
//            Picasso.get().load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(navi_profile);
//        } else {
//            Picasso.get().load(R.drawable.boy).into(navi_profile);
//        }
        String gender="";
        if(User.getCurrentUser().getGender()!=null){
            gender=User.getCurrentUser().getGender();
        }else{
            gender="male";
        }
        if (gender.equalsIgnoreCase("Male")) {
            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.boy).into(navi_profile);
            } else {
                Picasso.get().load(R.drawable.boy).into(navi_profile);
            }
        }else{
            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.girl).into(navi_profile);

              //  Log.i("USERDATA",User.getCurrentUser().getProfile());
                Log.i("USERDATA-1",image_url);

            } else {
                Picasso.get().load(R.drawable.girl).into(navi_profile);
            }
        }



        if (User.getCurrentUser().getFirstname() != null && User.getCurrentUser().getLastname() != null) {
            stu_name.setText(User.getCurrentUser().getFirstname() + "" + User.getCurrentUser().getLastname() + " (" + User.getCurrentUser().getId() + ")" );

        } else if (User.getCurrentUser().getFirstname() != null) {
            stu_name.setText(User.getCurrentUser().getFirstname());
        } else {
            stu_name.setText("");
        }

        if (User.getCurrentUser().getSection_name() != null) {
            //   classname.setText(User.getCurrentUser().getId() + "," + User.getCurrentUser().getClass_name());
            class_namessss.setText(User.getCurrentUser().getClass_name() + " ("+ User.getCurrentUser().getSection_name() +")");
        } else {
            class_namessss.setText(User.getCurrentUser().getClass_name());
        }
    }

    public static void setNaviHeaderDataHouse(TextView stu_name, TextView class_namessss, CircularImageView navi_profile,String image_url,
                                              FragmentActivity activity,String house) {

        String gender="";
        if(User.getCurrentUser().getGender()!=null){
            gender=User.getCurrentUser().getGender();
        }else{
            gender="male";
        }
        if (gender.equalsIgnoreCase("Male")) {
            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.boy).into(navi_profile);
            } else {
                Picasso.get().load(R.drawable.boy).into(navi_profile);
            }
        }else{
            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/student_photos/")) {
                Picasso.get().load(image_url).placeholder(R.drawable.girl).into(navi_profile);

                //  Log.i("USERDATA",User.getCurrentUser().getProfile());
                Log.i("USERDATA-1",image_url);

            } else {
                Picasso.get().load(R.drawable.girl).into(navi_profile);
            }
        }



        if (User.getCurrentUser().getFirstname() != null && User.getCurrentUser().getLastname() != null) {
            stu_name.setText(User.getCurrentUser().getFirstname() + "" + User.getCurrentUser().getLastname() + " (" + User.getCurrentUser().getId() + ")" );

        } else if (User.getCurrentUser().getFirstname() != null) {
            stu_name.setText(User.getCurrentUser().getFirstname());
        } else {
            stu_name.setText("");
        }

        if (!house.equalsIgnoreCase("")) {
            //   classname.setText(User.getCurrentUser().getId() + "," + User.getCurrentUser().getClass_name());
            class_namessss.setText(house);
        } else {
            class_namessss.setText(User.getCurrentUser().getClass_name() + " ("+ User.getCurrentUser().getSection_name() +")");
        }
    }

    public static void setImageUri(Context mContext, String url, ImageView imageView) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(mContext);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        RequestOptions request = new RequestOptions();
        request.placeholder(circularProgressDrawable);
        request.error(R.drawable.logo);

        if (url != null) {

            Glide.with(mContext).
                    load(url)
                    .apply(request)
                    .into(imageView);
        }
    }

    public static void progressImg(String url, ImageView m_image, Context mContext) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(mContext);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        RequestOptions request = new RequestOptions();
        request.placeholder(circularProgressDrawable);
        request.error(R.drawable.logo);

        if (url !=null) {
            // Picasso.get().load(image_url).placeholder(R.drawable.boy).into(navi_profile);
            Glide.with(mContext).
                    load(url)
                    .apply(request)
                    .into(m_image);
        }

    }

    public static String getDayOnly(String datestring) {
        String dayOfMonth = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datestring);
            dayOfMonth = new SimpleDateFormat("dd").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfMonth;
    }
    public static String getMonthYearOnly(String datestring) {
        String text_date = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datestring);
            text_date = new SimpleDateFormat("MMM, yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return text_date;
    }


    public static String getDateTimeFormatedWithAMPM(String d_date) {
        String date = null;
        try {
            Date dte = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(d_date);
            date = new SimpleDateFormat("dd MMM, yyyy hh:mm a").format(dte);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getTimeHr(String time) {
        String timenew = "";
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(time);
            timenew = _12HourSDF.format(_24HourDt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timenew;
    }

    public static String getDateTimeFormated(String d_date) {
        String date = null;
        try {
            Date dte = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(d_date);
            date = new SimpleDateFormat("dd MMM, yyyy hh:mm ").format(dte);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String setNaviHeaderClassData() {
        String value=User.getCurrentUser().getClass_name()+"("+User.getCurrentUser().getSection_name()+")";
        return value;
    }

}
