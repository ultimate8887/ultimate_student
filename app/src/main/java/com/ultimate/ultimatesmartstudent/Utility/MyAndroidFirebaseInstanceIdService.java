package com.ultimate.ultimatesmartstudent.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.UltimateERP;

import org.json.JSONObject;

import java.util.HashMap;

public class MyAndroidFirebaseInstanceIdService {


    private static void saveOnServer(String deviceToken) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("device_token", deviceToken);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEVICE_TOKEN_UPDATE, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

            }
        }, UltimateERP.getInstance(), params);

    }

    public static void saveInSharedPref(String deviceToken) {
        SharedPreferences preferences = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(Constants.DEVICE_TOKEN, deviceToken).commit();
    }

}
