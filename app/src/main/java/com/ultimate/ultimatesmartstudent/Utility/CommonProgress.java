package com.ultimate.ultimatesmartstudent.Utility;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.ultimate.ultimatesmartstudent.R;

public class CommonProgress extends Dialog {
    private static ProgressDialog progressDialog;

    public CommonProgress(@NonNull Context context) {
        super(context);

        WindowManager.LayoutParams params= getWindow().getAttributes();
        params.gravity= Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(params);
        setCancelable(false);
        setTitle(null);
        setOnCancelListener(null);
        View view= LayoutInflater.from(context).inflate(R.layout.progress_dialog, null);
        setContentView(view);

    }
}
