package com.ultimate.ultimatesmartstudent.Utility;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.SplashLoginMod.HomeActivity;
import com.ultimate.ultimatesmartstudent.UltimateERP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {


    @Override
    public void onNewToken(@NonNull String deviceToken) {
        super.onNewToken(deviceToken);
        Log.e("device_token",deviceToken);
        saveInSharedPref(deviceToken);
        if (User.getCurrentUser() != null)
            saveOnServer(deviceToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("Notification Body 1", "notify");
        Log.d("Notification Body 2" ,"From: " + remoteMessage.getFrom());
        if (remoteMessage.getData() != null) {
            Map<String, String> data = remoteMessage.getData();
            JSONObject object = new JSONObject(data);
            Log.e("Notification Body 3", String.valueOf(data));
            try {
                Log.e("Notification Body 4: ", User.getCurrentUser().getId().equals(object.getString("user_id")) + "");

                if (User.getCurrentUser() != null && User.getCurrentUser().getId().equals(object.getString("user_id"))) {
                    createNotification(object.getString("body"), object.getString("title"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (remoteMessage.getNotification() != null) {
            Log.e("Notification Body 5", "Message Notification Body: " + remoteMessage.getNotification().getBody());
            if (User.getCurrentUser() != null) {


                createNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
            }
        }
    }
    private void createNotification(String msg, String title) {
        Intent intent = null;
        PendingIntent resultIntent = null;
        int m = 0;
        m = new Random().nextInt();
        intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent = PendingIntent.getActivity(this, m, intent,
                PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
        Uri alarmSound = Uri.parse("android.resource://"
                + getApplication().getPackageName() + "/" + R.raw.good);
        String channelId = "XYZParent";
        CharSequence channelName = "XYZParent";


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(msg)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(resultIntent)
                .setSound(alarmSound)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setAutoCancel(true);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            //int importance = NotificationManager.IMPORTANCE_MAX;
            NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                    .build();
            channel.setSound(alarmSound, audioAttributes);
            channel.setDescription(msg);
            notificationManager.createNotificationChannel(channel);

        }
        notificationManager.notify(m, mBuilder.build());
        Log.e("Notification Body 6","fdsfds"+msg);
    }

    private static void saveOnServer(String deviceToken) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("device_token", deviceToken);
        Log.e("deviceToken",deviceToken);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEVICE_TOKEN_UPDATE, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

            }
        }, UltimateERP.getInstance(), params);

    }

    public static void saveInSharedPref(String deviceToken) {
        SharedPreferences preferences = UltimateERP.getInstance().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
        preferences.edit().putString(Constants.DEVICE_TOKEN, deviceToken).commit();
    }
}
