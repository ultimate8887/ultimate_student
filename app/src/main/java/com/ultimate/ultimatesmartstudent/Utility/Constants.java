package com.ultimate.ultimatesmartstudent.Utility;

import androidx.multidex.BuildConfig;

import com.ultimate.ultimatesmartstudent.BeanModule.User;


public class Constants {
    public static final String VERIFY_MOBILE ="verify_phone.php";
    public static final String NEWTIMETABLE ="newtimetable.php";
    public static final String REPLYASSIGNMENT_URL ="replyassignment.php";
    public static final String VIEWTHOUGHT ="viewthought.php";
    public static final String UPDATE_MOB_INFO ="update_mobile_info.php";
    public static final String COMMON_ATTEND_URL ="morn_eveng_transport_attendance.php";
    public static final String LIVECLSS ="live_class.php";
    public static final String UPDATE_IMG ="update_image.php";
    public static final String NEWS_URL ="news.php";
    public static final String NEW_MSG ="msg_noti.php";
    public static final String NEW_MSG_NOTI ="noti_toggle.php";
    public static final String STDGATEPASS_URL ="student_gatepass.php";
    public static final String IMGTOPDFCONV =  "Image to PDF";
    public static final String TAGGG ="?tag=";

    public static final String ACTIVITY = "School Activity";
    public static final String EDIT_CATEGORY_URL = "updategroup.php";
    public static final String ADD_CATEGORY_URL = "add_activity_groups.php";
    public static final String CATEGORY_LIST_URL = "get_activity_groups.php";
    public static final String DEL_ACT_URL = "deletegroup.php";
    public static final String GET_ACT_URL = "get_activity_students.php";



    public static final String HEALTH = "get_health.php";
    public static final String POST = "get_post.php";
    public static final String INSTRUCTION = "instruction.php";

    public static String health = "Health";
    public static String performance = "Performance";
    public static String post = "Social Post";

    public static final String CHECK_MONTHLY = "check_monthly_fees.php";
    public static final String CHECK_FEES_CLASS ="fetch_class_fees.php";
    public static final String EXTRADETAILS ="fetchextradetails.php";
    public static final String ADDFEES ="pay_fees.php";
    public static final String ADDSTDGATEPASS ="add_studentgatepass.php";
    public static final String FEE_PAID_STUDENT_NEW ="feepaidlist_new.php";
    public static final String ADDFEESN ="pay_fees_new.php";
    public static final String CLEANLINESS = "get_cleanliness.php";
    public static final String COMMUNICATION = "get_communication.php";
    public static String ASSIGNMENT_URL = "assignment.php";
    public static String ASSIGNMENT_DATA = "assign_data";
    public static final String UPDATE_SCHOOL_INFO ="update_schoolinfo.php";
    public static String collegeattendance = "   Attendance";
    public static String gensisattendance = "Student Attendance";
    public static final String USERDATA = "user_data";
    public static final String ADD_DOC = "add_document.php";
    public static final String GET_DOC = "documentlist.php";
    public static String DOC_URL = "office_admin/images/";
    public static String PREF_NAME = "ERPApp_Pref";
    public static String API_BASE_EXT = "studentapi/";
    public static String API_BASE_EXT_I = "includes/studentapi/";
//    public static String API_BASE_EXT_I = "includes/studentapi_testing/";
    public static String DEVICE_TOKEN_UPDATE="device_token.php";
    public static String stureport="Student Report";
    public static String YOUTUBE_API="AIzaSyB7xE9QnsvgPLktwVIq_UqhwJzwc32_BEg";
    //    public static String Dev_Base_Url = "http://androidtest.smartedgedigitalschool.com/includes/";
//    public static String Prod_Base_Url = "http://ict.smartedgedigitalschool.com/old/includes/";
//    public static String Dev_Base_Url = "http://androidtest.smartedgedigitalschool.com/";
//    public static String Prod_Base_Url = "http://ict.smartedgedigitalschool.com/";
//    public static String Dev_Base_Url = "http://ultimatesolutiongroup.com/";
//    public static String Prod_Base_Url = "http://ultimatesolutiongroup.com/";
    public static String VIDEO_PATH = "office_admin/upload/ebooks/";
    public static String Dev_Base_Url = "https://ultimatesolutiongroup.com/";
    public static String Prod_Base_Url = "https://ultimatesolutiongroup.com/";
    public static String attendance ="Attendance";
    public static String Fee_manage = "Fee";
    public static final int READ_PERMISSION =1;
    public static String ColgFee_manage = "Fee";
    //public static String CHSFee_manage  ="    Fee";
    public static String GDSsclFee_manage = "School Fee";
    public static String timetable = "Timetable";
    public static String exam = "Examination";
    public static String Login_URL = "login.php";
    public static String PAY_FEE_URL = "payfee";
    public static final String DEVICE_TOKEN = "device_token";
    public static String transport = "Transport";
    public static String USER_NAME = "user_name";
    public static String GATEPASS_URL="gatepass.php?";
    public static String GATEPASS_DATA="gatepass_data";
    public static String gatepass="Gatepass";
    public static String collegegatepass=" Gate-pass";
    public static String SESSIONDATA = "session_data";
    public static String FEE_PAID_BY_STUDENT="feepaidByStudent.php";
    public static String FEE_PAID_STUDENT="feepaidlist.php";
    public static String STUDENT_URL = "student.php/";
    public static String FEE_TO_PAY_BY_STUDENT_URL = "feepartstudent.php/";
    public static String LAST_NAME = "lastname";
    public static String FIRST_NAME = "firstname";
    public static String PASSWORD = "password";
    public static String homework = "Homework";
    public static String gdshomework = "Home work";
    public static String syllabus = "Syllabus";
    public static String syllabusgds = "SyllabusMod.";
    public static final String UPDATE_TOKEN = "updated_device_id";
    public static String event = "Event";
    public static final String NOTIFICATION_STATUS = "noti_status";
    public static String lib_manage = "Library management";
    public static String notice = "Notice-board";
    public static String test = "Test";
  //  public static String testgds = "Test Mod.";
    public static String ATTENDANCE_URL = "attendance.php";
    public static String ID = "id";
    public static String SYLLABUS_URL = "syllabus.php";
    public static String TIMETABLE_URL = "timetable.php";
    public static String NOTICE_URL = "noticeboard.php";
    public static String NOTIFICATION_URL = "notification.php";
    public static String notification = "Notification";
    public static String SubjectList = "subjectlist.php";
    public static String AddQuery = "sendquerry.php";
    public static String TestList = "test.php";
    public static String classwork="Classwork";
    public static String gdsclasswork="Class work";
    public static String CLASSWORK_URL="classwork.php";
    public static String addclasswork="addcw";
    public static String CLASSLIST_URL = "classlist/";
    public static String GROUP_DATA = "group_data";
    public static String CLASSDATA = "class_data";
    public static String Homework_url = "homework.php";
    public static String SCHOOLID="school_id";
    public static String querry="Query";
    public static String querry_URL="querry.php";
    public static String ResponseQuery="replyquerry.php";
    public static String Responsemsg="queryres.php";
    public static String Examdetail="examdetail.php";
    public static String SESSION_URL = "sessionlist.php/";
    public static String addexam="addexam.php";
    public static String PAY_ONLINE="payfeeform.php/";
    public static String BUS_LOCATION="buslocation.php?";
    public static String LOCATION="Update Location";
    public static String STUDENT_LOC="studentloc";
    public static String messages = "Message";
    public static String MESSAGE = "message.php";
    public static String MSGID = "?id=";
    public static String MSGTYPE = "&type=";
    public static String NAME = "&name=";
    public static String DATE = "&today=";
    public static String applyleave = "Apply Leave";
    public static String APPLYLEAVE = "applyleave.php";
    public static String LEAVELIST = "leavelist.php";
    public static String LEAVELISTID = "?id=";
    public static String ADMINLIST="adminlist.php ";
    public static String deparmrnt="department.php";
    public static String Post="post.php";
    public static String dpartment_id="?d_id=";
    public static String STAFF_DETAIL="staff_detail.php";
    public static String sendnotice="Notice";
    public static String   NOTICEURL="notice.php";
    public static String dietchart="Diet Chart";
    public static String DAILYTIPSURL="dailytips.php";
    public static String DIETCHARTURL="dietchart.php";
    public static String TestResult="testresult.php";
    public static String HOLIDAY_URL="holidays.php";
    public static String PASS_RESET_URL="passwordreset.php";
    public static String GALLERY_URL="gallery.php";
    public static String gallery="Gallery";
    public static String DATE_SHEET_URL="datesheet.php";
    public static String ds="Datesheet";
    public static String foldername="SmartEdge";
    public static String galfoldername="SmartEdgeGallery";
    public static String EVENTLIST_URL="eventlist.php";
    public static String  EVNTLISTLASTDATEPARTICIPATE_URL="eventparticipatelastdate.php";
    public static String  ADDEVNTPARTICIPATE_URL="addeventparticipate.php";
    public static String event_id = "?event_id=";
    public static String studid = "&stud_id=";
    public static String GETACHIVMNT_URL="getAchievementlist.php";
    public static String ebook="E-book";
    public static String elibrary="Library";
    public static String newsmagzine="News & Magazines";
    public static String GETNEWSMAGZINE_url="getnewsmagzine.php";
    public static String hostel="Hostel";
    public static String GETALLOTEDROOMLIST_URL="gethostlroomallotment.php";
    public static String GETHOSTLPAYFEELIST_URL="gethostelpayfeelist.php";
    public static String COLLEGEATTENDANCE_URL="college_student_attendance.php";
    public static String COLGACADEMICYAER_URL="colgacademicyear.php";
    public static String FEE_PAID_BY_CollegeSTUDENT = "feepaidBycollegeStudent.php";
    public static String GETCOLLEGEGATEPASSLIST_URL = "getcollegegatepasslist.php";
    public static String GETHOSTELGATEPASSLIST="gethostelgetpasslist.php";
    public static String EXTENSION_LIST_URL="colgfeeextensionlist.php";
    public static String Examdetailbyclassstu="examdetailbyclassstu.php";
    public static String COLLEGESubjectList="colgsubjectlist.php";
    public static String GETCALEVENTbydate="getcalevntbydate.php";
    public static String GETCALEVENT="getcalevnt.php";
    public static String calendars="Calendar";
    public static String GET_ST_COMMUNI_URL = "getStuCommuni.php";
    public static String GET_ST_BEHAV_URL = "getStuBehavior.php";
    public static String GET_ST_CLEAN_URL = "getStuclean.php";
    public static String GET_ST_PUNC_URL = "getStupunc.php";
    public static String GET_VIEW_REPORT = "getviewreport.php";
    public static String GET_STU_LISTEN = "getStulisting.php";
    public static String GETLESSONPLANLIST="getlessonplanlist.php";
    public static String lessonplan="Lesson Plan";
    public static String fetchMornVehicle="getMorningVecleDetail.php";
    public static String fetchEvngVehicle="getEveningVecleDetail.php";
    public static String newVehicleLocation="newVehicleLocation.php";
    public static String sports= "Sports";
    public static String childcare= "Child Care";
    public static String summercamp= "Summer Camp";
    public static String trainingmgt= "Training Mgt.";
    public static String getChildCareInfo="getChildCareInfo.php";
    public static String getSummerCamp="getSummerCamp.php";
    public static String getregSummerCamp="getregSummerCamp.php";
    public static String SPORTLIST_URL="sportlist.php";
    public static String GETSPORTPARTCPT_URL="getSportParticipate.php";
    public static String gettraining="gettraining.php";
    public static String regTraining="regTraining.php";
    public static String GETSPORTACHIVMNT_URL="getSportAchievementlist.php";
    public static String FEE_PAID_BY_STUDENTGDS = "feepaidByStudentGDS.php";
    public static String GDSHomework_url = "homeworkGDS.php";
    public static String GDSCLASSWORK_URL="classworkGDS.php";
    public static String GDSGALLERYLIST_URL="gdsgallerylist.php";
    public static String TestResultgds="testresultgds.php";
    public static String TestListgds = "testgds.php";
    public static String SYLLABUSGDS = "syllabusgds.php";
    public static String Synopsis = "Synopsis.";
    public static String SYNOPSIS = "getsynopsis.php";
    public static String GETAUTHORISEPRSNGP="getauthoriseprsngp.php";
    public static String TIMETABLESEC_URL = "timetable.php";
    public static String timetablesecwise = "Time-table.";
    public static String GETLIBRARY="getLibrary.php";
    public static String schlchkinout= "Check In-Out";
    public static String studentcheckInOut= "studentcheckInOut";
    public static String ocr= "OCR";
    public static String GIDSRSubjectList="gidsrsubjectlist.php";
    public static String gethomeworksubwise = "getHWsubwise.php";
    public static String ANNUALRESULT = "annualresultgds.php";
    public static String get_EBOOK = "getebook.php";
    public static String onlineclass= "E-Learning";
    public static String liveclass= "Online Classes";
    public static String VIEW_ONLINELINK = "viewonlinelink.php";
    public static String leave="Leave";
    public static String gdshomeworkbydatreurl="gdshomeworkbydate.php";
    public static String assignment="Assignment";
    public static String onlinequiztest= "Online Exam";
    public static String getonlinequiztest= "getonlinequiz.php" ;
    public static String getonlinequizresult= "getonlinequizresult.php" ;
    public static String video_gallery="Video Gallery";
    public static String fee="Fee Paid List";
    public static String add_doc="Add Document";
    public static String gatestudentlistclass="gate_attendence.php";
    public static String birthdaylist = "birthdaylist.php";
    public static String ScheduleClassListgds ="getscheduleclass.php";
    public static String Details_URL="getStudentDetails.php";

    public static String getBaseURL() {
        if(User.getCurrentUser()!= null && User.getCurrentUser().getSchoolData().getBaseUrl() !=null){
            return User.getCurrentUser().getSchoolData().getBaseUrl()+ API_BASE_EXT_I;
            //return  User.getCurrentUser().getSchoolData().getBaseUrl()+ API_BASE_EXT_I;
        }else {
            if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
                return Prod_Base_Url + API_BASE_EXT_I;
            } else {
                return Dev_Base_Url + API_BASE_EXT_I;
            }
        }
    }


    public static String getImageBaseURL() {
        if (User.getCurrentUser() != null && User.getCurrentUser().getSchoolData().getBaseUrl() != null) {
            if(User.getCurrentUser().getSchoolData().getBaseUrl().contains("includes")){
                String[] separated = User.getCurrentUser().getSchoolData().getBaseUrl().split("includes");
                return separated[0];
            }else{
                return User.getCurrentUser().getSchoolData().getBaseUrl();
            }
        } else {
            if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
                return Prod_Base_Url;
            } else {
                return Dev_Base_Url;
            }

        }

    }

    public static String[] mColors = {
            "FFEBEE", "FFCDD2", "EF9A9A", "E57373", "EF5350", "F44336", "E53935",        //reds
            "D32F2F", "C62828", "B71C1C", "FF8A80", "FF5252", "FF1744", "D50000",
            "FCE4EC", "F8BBD0", "F48FB1", "F06292", "EC407A", "E91E63", "D81B60",        //pinks
            "C2185B", "AD1457", "880E4F", "FF80AB", "FF4081", "F50057", "C51162",
            "F3E5F5", "E1BEE7", "CE93D8", "BA68C8", "AB47BC", "9C27B0", "8E24AA",        //purples
            "7B1FA2", "6A1B9A", "4A148C", "EA80FC", "E040FB", "D500F9", "AA00FF",
            "EDE7F6", "D1C4E9", "B39DDB", "9575CD", "7E57C2", "673AB7", "5E35B1",        //deep purples
            "512DA8", "4527A0", "311B92", "B388FF", "7C4DFF", "651FFF", "6200EA",
            "E8EAF6", "C5CAE9", "9FA8DA", "7986CB", "5C6BC0", "3F51B5", "3949AB",        //indigo
            "303F9F", "283593", "1A237E", "8C9EFF", "536DFE", "3D5AFE", "304FFE",
            "E3F2FD", "BBDEFB", "90CAF9", "64B5F6", "42A5F5", "2196F3", "1E88E5",        //blue
            "1976D2", "1565C0", "0D47A1", "82B1FF", "448AFF", "2979FF", "2962FF",
            "E1F5FE", "B3E5FC", "81D4fA", "4fC3F7", "29B6FC", "03A9F4", "039BE5",        //light blue
            "0288D1", "0277BD", "01579B", "80D8FF", "40C4FF", "00B0FF", "0091EA",
            "E0F7FA", "B2EBF2", "80DEEA", "4DD0E1", "26C6DA", "00BCD4", "00ACC1",        //cyan
            "0097A7", "00838F", "006064", "84FFFF", "18FFFF", "00E5FF", "00B8D4",
            "E0F2F1", "B2DFDB", "80CBC4", "4DB6AC", "26A69A", "009688", "00897B",        //teal
            "00796B", "00695C", "004D40", "A7FFEB", "64FFDA", "1DE9B6", "00BFA5",
            "E8F5E9", "C8E6C9", "A5D6A7", "81C784", "66BB6A", "4CAF50", "43A047",        //green
            "388E3C", "2E7D32", "1B5E20", "B9F6CA", "69F0AE", "00E676", "00C853",
            "F1F8E9", "DCEDC8", "C5E1A5", "AED581", "9CCC65", "8BC34A", "7CB342",        //light green
            "689F38", "558B2F", "33691E", "CCFF90", "B2FF59", "76FF03", "64DD17",
            "F9FBE7", "F0F4C3", "E6EE9C", "DCE775", "D4E157", "CDDC39", "C0CA33",        //lime
            "A4B42B", "9E9D24", "827717", "F4FF81", "EEFF41", "C6FF00", "AEEA00",
            "FFFDE7", "FFF9C4", "FFF590", "FFF176", "FFEE58", "FFEB3B", "FDD835",        //yellow
            "FBC02D", "F9A825", "F57F17", "FFFF82", "FFFF00", "FFEA00", "FFD600",
            "FFF8E1", "FFECB3", "FFE082", "FFD54F", "FFCA28", "FFC107", "FFB300",        //amber
            "FFA000", "FF8F00", "FF6F00", "FFE57F", "FFD740", "FFC400", "FFAB00",
            "FFF3E0", "FFE0B2", "FFCC80", "FFB74D", "FFA726", "FF9800", "FB8C00",        //orange
            "F57C00", "EF6C00", "E65100", "FFD180", "FFAB40", "FF9100", "FF6D00",
            "FBE9A7", "FFCCBC", "FFAB91", "FF8A65", "FF7043", "FF5722", "F4511E",        //deep orange
            "E64A19", "D84315", "BF360C", "FF9E80", "FF6E40", "FF3D00", "DD2600",
            "EFEBE9", "D7CCC8", "BCAAA4", "A1887F", "8D6E63", "795548", "6D4C41",        //brown
            "5D4037", "4E342E", "3E2723",
            "FAFAFA", "F5F5F5", "EEEEEE", "E0E0E0", "BDBDBD", "9E9E9E", "757575",        //grey
            "616161", "424242", "212121",
            "ECEFF1", "CFD8DC", "B0BBC5", "90A4AE", "78909C", "607D8B", "546E7A",        //blue grey
            "455A64", "37474F", "263238"
    };


}
