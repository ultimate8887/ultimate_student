package com.ultimate.ultimatesmartstudent.Utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ApiHandler {

    public static final int ERROR_PARSING = 1001;
    public static final int ERROR_VOLLEY = 1002;
    public static final int OBJECT_NOT_FOUND = 401;
    public static final int API_REQUIRED_FIELDS = 400;
    public static final int API_WRONG_METHOD = 405;
    public static final int UNAUTHORIZED_ACCESS = 402;
    public static final int SUCCESS = 200;

    public static interface ApiCallback {
        void onDataFetched(JSONObject jsonObject, ApiHandlerError error);
    }

    public static void apiHit(int requestMethod, String url, final ApiCallback apiCallback, final Context context, final Map<String, String> params) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            StringRequest stringRequest = new StringRequest(requestMethod, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                DebugLog.printLog("api_response", response);
                                JSONObject jsonObject = new JSONObject(response);
                               // String status_code = jsonObject.getString("status_code");
                                String status_code = getStatusCode(jsonObject);

                                if (apiCallback != null) {
                                    if (status_code.equalsIgnoreCase(String.valueOf(SUCCESS))) {
                                        apiCallback.onDataFetched(jsonObject, null);
                                        DebugLog.printLog("api_response_success", response);
                                    } else {
                                        ApiHandlerError apiHandlerError = new ApiHandlerError(
                                                Integer.parseInt(status_code),
                                                Utils.stringWithFirstcap(jsonObject.getString("error_msg"))
                                        );
                                        apiCallback.onDataFetched(null, apiHandlerError);
                                        DebugLog.printLog("api_response_error", apiHandlerError.toString());
                                    }
                                }
                            } catch (JSONException e) {
                                handleJsonException(e, response,apiCallback,context);
                            } catch (NumberFormatException e) {
                                handleNumberFormatException(e, response,apiCallback,context);
                            }

//                            try {
//                                DebugLog.printLog("api_response",response);
//                                JSONObject jsonObject = new JSONObject(response);
//                                String status_code = jsonObject.getString("status_code");
//                                if (apiCallback != null) {
//                                    if (status_code.equalsIgnoreCase(String.valueOf(SUCCESS))) {
//                                        if (apiCallback != null)
//                                            apiCallback.onDataFetched(jsonObject, null);
//                                        DebugLog.printLog("api_response_success",response);
//                                    } else {
//                                        ApiHandlerError apiHandlerError = new ApiHandlerError(Integer.valueOf(status_code), Utils.stringWithFirstcap(jsonObject.getString("error_msg")));
//                                        apiCallback.onDataFetched(null, apiHandlerError);
//                                        DebugLog.printLog("api_response_error1", String.valueOf(apiHandlerError));
//                                    }
//                                }
//                            } catch (JSONException e) {
//                                ApiHandlerError apiHandlerError = new ApiHandlerError(ApiHandler.ERROR_PARSING,context.getString(R.string.error_parse));
//                                apiCallback.onDataFetched(null, apiHandlerError);
//                                e.printStackTrace();
//                                DebugLog.printLog("api_response_error2", String.valueOf(apiHandlerError));
//                             //   DebugLog.printLog("error",response);
//                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            if (apiCallback != null) {
                                int errorCode = ApiHandler.ERROR_VOLLEY;
                                if (error.networkResponse != null && error.networkResponse.statusCode == 500) {
                                    errorCode = 500;
                                }
                                ApiHandlerError apiHandlerError = new ApiHandlerError(errorCode, getError(context, error));
                                apiCallback.onDataFetched(null, apiHandlerError);
                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    if (params != null) {
                        if (User.getCurrentUser()!=null){
                            params.put("user_id",User.getCurrentUser().getId());
                            params.put("school_key",User.getCurrentUser().getSchoolData().getFi_school_id());
                        }
                        return params;
                    } else {
                        return new HashMap<>();
                    }

                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    if (User.getCurrentUser() != null && User.getCurrentUser().getSchoolData().getDbhost()!= null) {
                        HashMap<String, String> header_param = new HashMap<>();
                        header_param.put("dbhost", User.getCurrentUser().getSchoolData().getDbhost());
                        header_param.put("dbname", User.getCurrentUser().getSchoolData().getDbname());
                        header_param.put("dbpass", User.getCurrentUser().getSchoolData().getDbpass());
                        header_param.put("dbuser", User.getCurrentUser().getSchoolData().getDbuser());
                        return header_param;
                    }
                    return new HashMap<>();
                }
            };
            if (context != null) {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 6, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                Volley.newRequestQueue(context).add(stringRequest);
            }





        } else {
            if (apiCallback != null) {
                ApiHandlerError error = new ApiHandlerError(ERROR_VOLLEY, "Please check internet connectivity.");
                apiCallback.onDataFetched(null, error);
            }
        }
    }
    private static String getStatusCode(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("status_code")) {
            try {
                return String.valueOf(jsonObject.getInt("status_code"));
            } catch (JSONException e) {
                return jsonObject.getString("status_code");
            }
        } else {
            throw new JSONException("status_code not found in response");
        }
    }

    private static void handleJsonException(JSONException e, String response, ApiCallback apiCallback, Context context) {
        DebugLog.printLog("json_exception", "JSONException occurred: " + e.getMessage() + ", Response: " + response);
        ApiHandlerError apiHandlerError = new ApiHandlerError(
                ApiHandler.ERROR_PARSING,
                context.getString(R.string.error_parse)
        );
        if (apiCallback != null) {
            apiCallback.onDataFetched(null, apiHandlerError);
        }
        DebugLog.printLog("api_response_error_json", apiHandlerError.toString());
        e.printStackTrace();
    }

    private static void handleNumberFormatException(NumberFormatException e, String response, ApiCallback apiCallback, Context context) {
        DebugLog.printLog("number_format_exception", "NumberFormatException occurred: " + e.getMessage() + ", Response: " + response);
        ApiHandlerError apiHandlerError = new ApiHandlerError(
                ApiHandler.ERROR_PARSING,
                context.getString(R.string.error_parse)
        );
        if (apiCallback != null) {
            apiCallback.onDataFetched(null, apiHandlerError);
        }
        DebugLog.printLog("api_response_error_number_format", apiHandlerError.toString());
        e.printStackTrace();
    }

    private static String getError(Context context, VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            return context.getString(R.string.error_network_timeout);
        } else if (error instanceof AuthFailureError) {
            return context.getString(R.string.authfailure);
        } else if (error instanceof ServerError) {
            return context.getString(R.string.error_server);
        } else if (error instanceof NetworkError) {
            return context.getString(R.string.error_network);
        } else if (error instanceof ParseError) {
            return context.getString(R.string.error_parse);
        }
        return "";
    }

    public interface AdapterCallback {
    }
}
