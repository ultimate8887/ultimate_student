package com.ultimate.ultimatesmartstudent.Utility;

public class ApiHandlerError {
    int statusCode;
    String message;

    ApiHandlerError(){}

    public ApiHandlerError(int code, String message){
        statusCode = code;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
