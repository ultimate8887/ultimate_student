package com.ultimate.ultimatesmartstudent.Gallery;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoGalleryAdapter extends RecyclerView.Adapter<VideoGalleryAdapter.Viewholder> {
    ArrayList<Eventlist_bean> sy_list;
    Mycallback mAdaptercall;
    Context mContext;
    Animation animation;
    public VideoGalleryAdapter(ArrayList<Eventlist_bean> sy_list, Context mContext, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.onlincls_adpt_lyt, parent, false);
        VideoGalleryAdapter.Viewholder viewholder = new VideoGalleryAdapter.Viewholder(view);
        return viewholder;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public void onBindViewHolder(Viewholder holder, final int position) {

        if (sy_list.get(position).getEvent_id() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("gl-"+sy_list.get(position).getEvent_id(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(sy_list.get(position).getFromdate());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.u_date.setText(Utils.getDateFormated(sy_list.get(position).getFromdate()));
        }


        holder.txtPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdaptercall != null) {
                    mAdaptercall.viewPdf(sy_list.get(position));
                }
            }
        });


        if (sy_list.get(position).getEvent_name()!=null) {
            holder.txttitle.setText(sy_list.get(position).getEvent_name());
        }else {
            holder.txttitle.setVisibility(View.GONE);

        }
        holder.txtSub.setText(sy_list.get(position).getTitle());

    }

    public interface Mycallback {
        public void onMethodCallback(Eventlist_bean syllabusBean);

        public void viewPdf(Eventlist_bean syllabusBean);
    }
    public void setSyllabusList(ArrayList<Eventlist_bean> sy_list) {
        this.sy_list = sy_list;
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.head)
        RelativeLayout txtPdf;
        @BindView(R.id.classess)
        TextView txttitle;
        @BindView(R.id.u_date)
        TextView u_date;
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.subject)
        TextView txtSub;
        @BindView(R.id.homeTopic)
        TextView txtwriter;
        @BindView(R.id.imgStud)
        ImageView video_img;
        @BindView(R.id.imgStud1)
        ImageView you_img;
        @BindView(R.id.play)
        LottieAnimationView play;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            txtwriter.setVisibility(View.GONE);
        }
    }
}
