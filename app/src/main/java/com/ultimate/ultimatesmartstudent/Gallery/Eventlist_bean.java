package com.ultimate.ultimatesmartstudent.Gallery;

import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Eventlist_bean {
    private static String ID="event_id";
    private static String IMAGE="image";
    private static String TITLE="title";
    private static String DESCRIPTION="description";
    private static String FROMDATE="fromdate";
    private static String TODATE="todate";
    private static String MSG="msg";
    private static String LATDATE="lastdate";
    private static String VIDEO="video";

    private static String ETITLE="event_name";

    /**
     * event_id : 9
     * title : Drawing Event
     * description :  Drawing event is starting on 15th January.
     * image : ["office_admin/images/evnt_01082019_0701560.jpg ","office_admin/images/evnt_01082019_0701561.jpg"]
     * fromdate : 2019-01-15
     * todate : 2019-01-19
     * lastdate : 2019-01-14
     */
    private String msg;

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    private String event_name;

    /**
     * event_id : 9
     * title : Drawing Event
     * description :  Drawing event is starting on 15th January.
     * image : ["office_admin/images/evnt_01082019_0701560.jpg ","office_admin/images/evnt_01082019_0701561.jpg"]
     * fromdate : 2019-01-15
     * todate : 2019-01-19
     * lastdate : 2019-01-14
     */

    private String event_id;
    private String title;
    private String description;
    private String fromdate;
    private String todate;
    private String lastdate;

    private String es_coordinator;

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    private String video;

    public String getEs_coordinator() {
        return es_coordinator;
    }

    public void setEs_coordinator(String es_coordinator) {
        this.es_coordinator = es_coordinator;
    }
    private List<String> image;

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getLastdate() {
        return lastdate;
    }

    public void setLastdate(String lastdate) {
        this.lastdate = lastdate;
    }

    public ArrayList<String> getImage() {
        return (ArrayList<String>) image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static ArrayList<Eventlist_bean> parseEVENTArray(JSONArray arrayObj) {
        ArrayList<Eventlist_bean> list = new ArrayList<Eventlist_bean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Eventlist_bean p = parseEVENTObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Eventlist_bean parseEVENTObject(JSONObject jsonObject) {
        Eventlist_bean casteObj = new Eventlist_bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setEvent_id(jsonObject.getString(ID));
            }
            if (jsonObject.has(IMAGE) && jsonObject.get(IMAGE) instanceof JSONArray) {
                JSONArray img_arr = jsonObject.getJSONArray(IMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }
            if (jsonObject.has(TITLE) && !jsonObject.getString(TITLE).isEmpty() && !jsonObject.getString(TITLE).equalsIgnoreCase("null")) {
                casteObj.setTitle(jsonObject.getString(TITLE));
            }
            if (jsonObject.has(VIDEO) && !jsonObject.getString(VIDEO).isEmpty() && !jsonObject.getString(VIDEO).equalsIgnoreCase("null")) {
                casteObj.setVideo(jsonObject.getString(VIDEO));
            }

            if (jsonObject.has(DESCRIPTION) && !jsonObject.getString(DESCRIPTION).isEmpty() && !jsonObject.getString(DESCRIPTION).equalsIgnoreCase("null")) {
                casteObj.setDescription(jsonObject.getString(DESCRIPTION));
            }
            if (jsonObject.has(FROMDATE) && !jsonObject.getString(FROMDATE).isEmpty() && !jsonObject.getString(FROMDATE).equalsIgnoreCase("null")) {
                casteObj.setFromdate(jsonObject.getString(FROMDATE));
            }
            if (jsonObject.has(TODATE) && !jsonObject.getString(TODATE).isEmpty() && !jsonObject.getString(TODATE).equalsIgnoreCase("null")) {
                casteObj.setTodate(jsonObject.getString(TODATE));
            } if (jsonObject.has(MSG) && !jsonObject.getString(MSG).isEmpty() && !jsonObject.getString(MSG).equalsIgnoreCase("null")) {
                casteObj.setMsg(jsonObject.getString(MSG));
            }
            if (jsonObject.has(LATDATE) && !jsonObject.getString(LATDATE).isEmpty() && !jsonObject.getString(LATDATE).equalsIgnoreCase("null")) {
                casteObj.setLastdate(jsonObject.getString(LATDATE));
            }
            if (jsonObject.has("es_coordinator") && !jsonObject.getString("es_coordinator").isEmpty() && !jsonObject.getString("es_coordinator").equalsIgnoreCase("null")) {
                casteObj.setEs_coordinator(jsonObject.getString("es_coordinator"));
            }

            if (jsonObject.has(ETITLE) && !jsonObject.getString(ETITLE).isEmpty() && !jsonObject.getString(ETITLE).equalsIgnoreCase("null")) {
                casteObj.setEvent_name(jsonObject.getString(ETITLE));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
