package com.ultimate.ultimatesmartstudent.Gallery;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import me.relex.circleindicator.CircleIndicator;

public class GDSGallerylist_adapter extends RecyclerView.Adapter<GDSGallerylist_adapter.Viewholder>{
    private final Context context;
    ArrayList<Eventlist_bean> eventList;
    private final Mycallback mAdaptercall;

    public GDSGallerylist_adapter(ArrayList<Eventlist_bean> eventList, Context context, Mycallback mAdaptercall) {
        this.context=context;
        this.eventList=eventList;
        this.mAdaptercall = mAdaptercall;

    }

    @NonNull
    @Override
    public GDSGallerylist_adapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_adapt_lay, parent, false);
        GDSGallerylist_adapter.Viewholder viewholder = new GDSGallerylist_adapter.Viewholder(view);
        return viewholder;
    }


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onBindViewHolder(@NonNull final GDSGallerylist_adapter.Viewholder holder, final int position) {

        if (eventList.get(position).getEvent_name()!=null) {
            holder.textTittle.setText(eventList.get(position).getEvent_name());
        }else {
            holder.textTittle.setText(eventList.get(position).getTitle());
        }


        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(eventList.get(position).getFromdate());

        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Uploaded in today", "#ffffff");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.fromdate.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Uploaded in yesterday", "#ffffff");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.fromdate.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.fromdate.setText(Utils.getDateFormated(eventList.get(position).getFromdate()));
        }

        //  holder.fromdate.setText(""+ Utility.getDateFormated(eventList.get(position).getFromdate()));

        if (eventList.get(position).getImage().get(0)!=null) {
            //   holder.count.setText(String.valueOf(eventList.get(position).getImage().size()));
            Picasso.get().load(eventList.get(position).getImage().get(0)).placeholder(context.getResources().getDrawable(R.color.transparent)).into(holder.product_img);
        }else {
            Picasso.get().load(R.drawable.photo_gallery).into(holder.product_img);

        }

        holder.layt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdaptercall.DisplayImage(eventList.get(position));
            }
        });

    }

    public interface Mycallback {
        void DisplayImage(final Eventlist_bean obj);
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public void setEventList(ArrayList<Eventlist_bean> eventList) {
        this.eventList = eventList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView textTittle,fromdate;
        public ImageView product_img;
        public RelativeLayout layt;

        public Viewholder(View itemView) {
            super(itemView);
            layt= itemView.findViewById(R.id.layt);
            textTittle= itemView.findViewById(R.id.name);
            product_img=itemView.findViewById(R.id.product_img);
            fromdate= itemView.findViewById(R.id.date);
        }
    }
}
