package com.ultimate.ultimatesmartstudent.Gallery;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IMGGridView extends AppCompatActivity implements IMG_Adapter.IMGCallback {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.export)
    FloatingActionButton imageView;
    @BindView(R.id.parent)
    RelativeLayout parent;
    private LinearLayoutManager layoutManager;
    Eventlist_bean data;
    IMG_Adapter offerBannerAdapter;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    ArrayList<String> hq_list= new ArrayList<>();
    int size=0;
    @BindView(R.id.txtSub)
    TextView txtSub;
    String tag="",title="",sub="";
    CommonProgress commonProgress;
    SharedPreferences sharedPreferences;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_common_neww);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(this);
        imageView.setVisibility(View.VISIBLE);
        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("data")) {
//            Gson gson = new Gson();
//            data = gson.fromJson(intent_value.getString("data"), Eventlist_bean.class);
            hq_list=(ArrayList<String>)getIntent().getSerializableExtra("data");
            tag = getIntent().getExtras().getString("tag");
            title = getIntent().getExtras().getString("title");
            sub = getIntent().getExtras().getString("sub");
            txtSub.setVisibility(View.VISIBLE);
            txtTitle.setText(title);
            txtSub.setText(sub);
            totalRecord.setText("Total Images:- "+String.valueOf(hq_list.size()));
            recyclerView.setLayoutManager(new GridLayoutManager(this,2));
            offerBannerAdapter = new IMG_Adapter(hq_list,this,this);
            recyclerView.setAdapter(offerBannerAdapter);
            if (!restorePrefDataP()){
                setShowcaseViewP();
            }
        } else {
            return;
        }
    }

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(imageView,"Download all images!","Tap the download button to download all images.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();

            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export_gl",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export_gl",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_gl",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export_gl",false);
    }

    @OnClick(R.id.imgBackmsg)
    public void onback() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @OnClick(R.id.export)
    public void dialog() {
        size=hq_list.size();
        for (int i = 0; i < hq_list.size(); i++) {
            Picasso.get().load(hq_list.get(i)).into(imageView);
            BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
            Bitmap bitmap = bitmapDrawable.getBitmap();
            commonProgress.show();
            saveImageToGallery(bitmap,i+1);
        }
    }

    private void saveImageToGallery(Bitmap bitmap,int v){
        long time= System.currentTimeMillis();
        OutputStream fos;
        String school= User.getCurrentUser().getSchoolData().getName();
        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, tag+"_img_saved"+time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));

                if (v==size) {
                    Picasso.get().load(R.drawable.photo).into(imageView);
                    Toast.makeText(getApplicationContext(), "All images are successfully \ndownloaded in gallery", Toast.LENGTH_SHORT).show();
                    commonProgress.dismiss();
                }
            }
            else{
                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir,  tag+"_img_saved"+time+ ".jpg");
                fos = new FileOutputStream(image);

                if (v==size) {
                    Picasso.get().load(R.drawable.photo).into(imageView);
                    Toast.makeText(getApplicationContext(), "All images are successfully \ndownloaded in gallery", Toast.LENGTH_SHORT).show();
                    commonProgress.dismiss();
                }

                //resetOpTimes();
            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){
            Picasso.get().load(R.drawable.photo).into(imageView);
            commonProgress.dismiss();
            Toast.makeText(getApplicationContext(), "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void displayImage(String obj) {
        Dialog dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(true);
        dialogLog.setContentView(R.layout.enter_mobile_dialog);
       // dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imgShow = (ImageView) dialogLog.findViewById(R.id.img);

        Picasso.get().load(obj).into(imgShow);
        Button save = (Button) dialogLog.findViewById(R.id.submit);
        save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                save.startAnimation(animation);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);
            }
        });

        RelativeLayout btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // btnNo.startAnimation(animation4);
                dialogLog.dismiss();
            }
        });
        dialogLog.show();
        Window window = dialogLog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void saveImageToGallery(Bitmap bitmap) {
        String school= User.getCurrentUser().getSchoolData().getName();
        OutputStream fos;
        long time= System.currentTimeMillis();
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME,  tag+"_img_saved"+time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            } else {

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir,  tag+"_img_saved"+time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        } catch (Exception e) {

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}