package com.ultimate.ultimatesmartstudent.Gallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IMG_Adapter extends RecyclerView.Adapter<IMG_Adapter.Viewholder>{
    ArrayList<String> hq_list;
    Animation animation;
    Context listner;
    IMGCallback imgCallback;

    public IMG_Adapter(ArrayList<String> hq_list, Context listner, IMGCallback imgCallback){
        this.hq_list=hq_list;
        this.listner=listner;
        this.imgCallback=imgCallback;

    }

    public interface IMGCallback {
        void displayImage(String obj);
    }

    @Override
    public IMG_Adapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.pager_item_new, parent, false);
        IMG_Adapter.Viewholder viewholder = new IMG_Adapter.Viewholder(view);
        return viewholder;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onBindViewHolder(final IMG_Adapter.Viewholder holder, final int position) {
        // Utils.setImageUri(listner,hq_list.get(position),holder.imageView);
        Picasso.get().load(hq_list.get(position)).into(holder.imageView);
        holder.layt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgCallback.displayImage(hq_list.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(hq_list!= null){
            size = hq_list.size();
        }
//        if(msg!= null){
//            size= size+1;
//        }
        return size;
    }


    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_img)
        ImageView imageView;
        @BindView(R.id.layt)
        RelativeLayout layt;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
