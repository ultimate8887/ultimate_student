package com.ultimate.ultimatesmartstudent.Gallery;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Objects;

public class ViewPagerAdapters extends PagerAdapter {

    private final Context context;
    private final ArrayList<String> image;
    String folder_main = "Gallery";
    String msg,school="";
    public ViewPagerAdapters(Context context, ArrayList<String> image, String msg) {
        this.context=context;
        this.image=image;
        this.msg=msg;
    }

    @Override
    public int getCount() {
        int size = 0;
        if(image!= null){
            size = image.size();
        }
        if(msg!= null){
            size= size+1;
        }
        return size;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.pager_itemsec, container, false);
        school= User.getCurrentUser().getSchoolData().getName();
        ImageView imageView = itemView.findViewById(R.id.img_pager_item);
        TextView txtText= (TextView)itemView.findViewById(R.id.clsswrktexttt);

        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
              //  UltimateProgress.showProgressBar(context, "Waiting For Downloading......!");
               // Picasso.get().load(image.get(position)).into(imgTraget);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);
                return false;
            }
        });

        if(msg!= null){
            if(position==0){
                txtText.setVisibility(View.VISIBLE);
                txtText.setText(msg);
                txtText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog(msg);
                    }
                });
            }else{
                Picasso.get().load(image.get(position-1)).into(imageView);
            }
        }else{
            Picasso.get().load(image.get(position)).into(imageView);
        }

        container.addView(itemView);

        return itemView;
    }


    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = context.getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "Gallery_img_saved" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(context, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "Gallery_img_saved" + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(context, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(context, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }


    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    private void openDialog(String msg) {
        final Dialog logoutDialog = new Dialog(context);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setCancelable(true);
        logoutDialog.setContentView(R.layout.msg_lyt);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);
        txt2.setText(msg);
        logoutDialog.show();
    }

}
