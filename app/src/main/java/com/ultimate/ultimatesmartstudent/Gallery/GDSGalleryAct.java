package com.ultimate.ultimatesmartstudent.Gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.Leave_Mod.Leavelist_Activity;
import com.ultimate.ultimatesmartstudent.Leave_Mod.OptionAdapter;
import com.ultimate.ultimatesmartstudent.Leave_Mod.OptionBean;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GDSGalleryAct extends AppCompatActivity implements GDSGallerylist_adapter.Mycallback {
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)TextView txtNorecord;
    private GDSGallerylist_adapter adapter;
    ArrayList<Eventlist_bean> eventList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    Animation animation;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @BindView(R.id.textView8)
    TextView textView8;
    ArrayList<OptionBean> vehicleList = new ArrayList<>();
    @BindView(R.id.spinnerGroups)
    Spinner vehicleType;
    String tag="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_common);
        ButterKnife.bind(this);
        recyclerView.setNestedScrollingEnabled(false);
        textView8.setText(getString(R.string.f_leave));
        txtTitle.setText(getString(R.string.image)+" "+getString(R.string.list));
        txtSub.setText(Utils.setNaviHeaderClassData());
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        adapter = new GDSGallerylist_adapter(eventList, GDSGalleryAct.this,this);
        recyclerView.setAdapter(adapter);


        //vehicleList.add(new OptionBean("All"));
        vehicleList.add(new OptionBean("Gender"));
        vehicleList.add(new OptionBean("Class"));

        OptionAdapter dataAdapter = new OptionAdapter(GDSGalleryAct.this, vehicleList);
        vehicleType.setAdapter(dataAdapter);
        //attach the listener to the spinner
        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    tag = vehicleList.get(i - 1).getName();
                }else {
                    tag="";
                }
                fetchEventlist(tag);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    private void fetchEventlist(String tag) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("tag",tag);
        params.put("class",User.getCurrentUser().getClass_id());
        params.put("sec",User.getCurrentUser().getSection_id());
        params.put("gender", User.getCurrentUser().getGender());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSGALLERYLIST_URL, apiCallback, this, params);

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();

            if (error == null) {
                try {
                    if (eventList != null) {
                        eventList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("event_data");
                    eventList = Eventlist_bean.parseEVENTArray(jsonArray);
                    if (eventList.size() > 0) {
                        adapter.setEventList(eventList);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setEventList(eventList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                eventList.clear();
                adapter.setEventList(eventList);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };
    @OnClick(R.id.imgBackmsg)
    public void onback() {
        imgBackmsg.startAnimation(animation);
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
//        fetchEventlist();
    }

    @Override
    public void DisplayImage(Eventlist_bean data) {
        ArrayList<String> yourArray = new ArrayList<>();
        yourArray=data.getImage();
        Intent intent = new Intent(GDSGalleryAct.this, IMGGridView.class);
        intent.putExtra("data", yourArray);
        intent.putExtra("tag", "gallery");
        intent.putExtra("title", data.getTitle());
        intent.putExtra("sub", data.getEvent_name());
        startActivity(intent);
    }
}