package com.ultimate.ultimatesmartstudent.Gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoGalleryActivity extends AppCompatActivity implements VideoGalleryAdapter.Mycallback {

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    String  sub_id, sub_name;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    CommonProgress commonProgress;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<Eventlist_bean> ebooklist = new ArrayList<>();
    VideoGalleryAdapter adapter;
    Animation animation;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_new);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.video)+" "+getString(R.string.list));
        txtSub.setText(Utils.setNaviHeaderClassData());
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerView.setLayoutManager(new LinearLayoutManager(VideoGalleryActivity.this));
        adapter = new VideoGalleryAdapter(ebooklist, VideoGalleryActivity.this, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //if (class_id != null && !class_id.isEmpty())
        fetchSubWise();
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }


    private void fetchSubWise() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("v_type","video");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSGALLERYLIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        if (ebooklist != null) {
                            ebooklist.clear();
                        }
                        //  Toast.makeText(getApplicationContext(),"Success!",Toast.LENGTH_SHORT).show();
                        JSONArray jsonArray = jsonObject.getJSONArray("event_data");
                        ebooklist = Eventlist_bean.parseEVENTArray(jsonArray);
                        if (ebooklist.size() > 0) {
                            adapter.setSyllabusList(ebooklist);
                            //setanimation on adapter...
                            recyclerView.getAdapter().notifyDataSetChanged();
                            recyclerView.scheduleLayoutAnimation();
                            //-----------end------------
                            txtNorecord.setVisibility(View.GONE);
                            totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(ebooklist.size()));
                        } else {
                            adapter.setSyllabusList(ebooklist);
                            adapter.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText(getString(R.string.t_entries)+" 0");
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                    txtNorecord.setVisibility(View.VISIBLE);
                    ebooklist.clear();
                    adapter.setSyllabusList(ebooklist);
                    adapter.notifyDataSetChanged();
                }
            }
        }, this, params);

    }


    @Override
    public void onMethodCallback(Eventlist_bean syllabusBean) {

    }

    @Override
    public void viewPdf(Eventlist_bean syllabusBean) {
        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://elms.xyzultimatesolution.com/index.php/bbb-room/php-programming/")));
        Gson gson = new Gson();
        String ebookdata = gson.toJson(syllabusBean, Eventlist_bean.class);
        Intent intent = new Intent(VideoGalleryActivity.this, GalleryVideoPlay.class);
        intent.putExtra("video_data", ebookdata);
        startActivity(intent);
    }
}