package com.ultimate.ultimatesmartstudent.Gallery;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GalleryVideoPlay extends AppCompatActivity {

    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Animation animation;
    private VideoView videoView;
    Eventlist_bean hostlroombean;
    String video_id = "";
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    CommonProgress commonProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_video_play);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        videoView = findViewById(R.id.videoView);
        commonProgress = new CommonProgress(this);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("video_data")) {
                Gson gson = new Gson();
                hostlroombean = gson.fromJson(getIntent().getExtras().getString("video_data"), Eventlist_bean.class);
                txtTitle.setText(hostlroombean.getTitle());
                video_id = Constants.getImageBaseURL() + hostlroombean.getVideo();
                //Log.e("video", video_id);
                commonProgress.show();
                loadVideo();
            }
        }


    }

    @OnClick(R.id.imgBackmsg)
    public void onback() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    private void loadVideo() {

        Uri videoUri = Uri.parse(video_id);
        videoView.setVideoURI(videoUri);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                commonProgress.cancel();
                videoView.start();
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                commonProgress.cancel();
                // Handle video playback completion if needed
            }
        });
    }

}