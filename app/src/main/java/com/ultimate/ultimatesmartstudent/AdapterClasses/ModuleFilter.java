package com.ultimate.ultimatesmartstudent.AdapterClasses;

import android.widget.Filter;

import com.ultimate.ultimatesmartstudent.HomeFragments.DashBoardAdapter;
import com.ultimate.ultimatesmartstudent.HomeFragments.DashboardBean;

import java.util.ArrayList;

public class ModuleFilter extends Filter {

    DashBoardAdapter adapter;
    ArrayList<DashboardBean> filterList;

    public ModuleFilter(ArrayList<DashboardBean> filterList, DashBoardAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;
    }

   @Override
    protected Filter.FilterResults performFiltering(CharSequence constraint) {
       Filter.FilterResults results=new Filter.FilterResults();
       //CHECK CONSTRAINT VALIDITY
       if(constraint != null && constraint.length() > 0)
       {
           //CHANGE TO UPPER
           constraint=constraint.toString().toUpperCase();
           //STORE OUR FILTERED PLAYERS
           ArrayList<DashboardBean> filteredPlayers=new ArrayList<>();
           for (int i=0;i<filterList.size();i++)
           {
               //CHECK
               if(filterList.get(i).getTitle().toUpperCase().contains(constraint))
               {
                   //ADD PLAYER TO FILTERED PLAYERS
                   filteredPlayers.add(filterList.get(i));
               }
           }
           results.count=filteredPlayers.size();
           results.values=filteredPlayers;
       }else
       {
           results.count=filterList.size();
           results.values=filterList;
       }
       return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
        adapter.dataList= (ArrayList<DashboardBean>) results.values;
        //REFRESH
        adapter.notifyDataSetChanged();
    }
}
