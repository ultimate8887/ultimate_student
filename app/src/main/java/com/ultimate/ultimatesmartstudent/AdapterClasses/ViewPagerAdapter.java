package com.ultimate.ultimatesmartstudent.AdapterClasses;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.toolbox.ImageLoader;
import com.ultimate.ultimatesmartstudent.BeanModule.BannerBean;
import com.ultimate.ultimatesmartstudent.R;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<BannerBean> sliderImg;
    private ImageLoader imageLoader;


    public ViewPagerAdapter(List<BannerBean> sliderImg,Context context) {
        this.sliderImg = sliderImg;
        this.context = context;
    }

    @Override
    public int getCount() {
        return sliderImg.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout, null);

        BannerBean utils = sliderImg.get(position);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        TextView text = (TextView) view.findViewById(R.id.texts);

        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        Log.e("imageLoader", String.valueOf(imageLoader));
        Log.e("TittleLoader", utils.getTitle());

//        text.setText(utils.getTitle());
      //  Toast.makeText(context, "Tittle "+ utils.getTitle(), Toast.LENGTH_SHORT).show();

        if(utils.getBaner_image()!=null) {
            imageLoader.get(utils.getBaner_image(), ImageLoader.getImageListener(imageView, R.color.white, android.R.drawable.ic_dialog_alert));
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(position == 0){
                    Toast.makeText(context, "Slide 1 Clicked", Toast.LENGTH_SHORT).show();
                } else if(position == 1){
                    Toast.makeText(context, "Slide 2 Clicked", Toast.LENGTH_SHORT).show();
                } else if(position == 2){
                    Toast.makeText(context, "Slide 3 Clicked", Toast.LENGTH_SHORT).show();
                } else if(position == 3){
                    Toast.makeText(context, "Slide 4 Clicked", Toast.LENGTH_SHORT).show();
                }  else {
                    Toast.makeText(context, "Slide 5 Clicked", Toast.LENGTH_SHORT).show();
                }

            }
        });

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }

}
