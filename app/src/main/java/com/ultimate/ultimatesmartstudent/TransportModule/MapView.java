package com.ultimate.ultimatesmartstudent.TransportModule;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapView extends AppCompatActivity implements OnMapReadyCallback {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    CommonProgress commonProgress;
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Marker myMarker = null;
    double lat, lng;
    Handler handler = new Handler();
    Geocoder geo;
    List<Address> addresses = null;
    StringBuilder  str = null;
    LatLng latLng;
    private VehicleDetailMornBean data;
    @BindView(R.id.errortext)TextView errortext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);
        ButterKnife.bind(this);
        txtTitle.setText("Transport");
        geo = new Geocoder(getApplicationContext(), Locale.getDefault());
        commonProgress=new CommonProgress(this);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_location);
        mapFragment.getMapAsync(this);//remember getMap() is deprecated!
        Log.e("morningVid", User.getCurrentUser().getMorningVId());
        Log.e("eveningVid",User.getCurrentUser().getEveningVId());


        if (getIntent().getExtras().containsKey("vehicledt")) {
            Gson gson = new Gson();
            data = gson.fromJson(getIntent().getExtras().getString("vehicledt"), VehicleDetailMornBean.class);
        } else {
            return;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runLocation, 1500);
    }


    public Runnable runLocation = new Runnable(){

        @Override
        public void run() {

            fetchDriverLocation();

            MapView.this.handler.postDelayed(MapView.this.runLocation, 1500);
        }
    };
    @OnClick(R.id.imgBackmsg)
    public void backCall(){
        finish();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

//        try{
//            boolean isSuccess=googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,R.raw.my_map_style));
//
//            if(!isSuccess){
//                Log.e("ERROR","MAP STYLE NOT LOADED");
//            }
//        }catch (Resources.NotFoundException ex){
//            ex.printStackTrace();
//        }

        map = googleMap;
        map.setMaxZoomPreference(18);
        map.setMinZoomPreference(14);
        fetchDriverLocation();
    }

    private void fetchDriverLocation() {
//        String url = Constants.getBaseURL() + Constants.BUS_LOCATION + "r_id=" + User.getCurrentUser().getR_id();
//        Log.e("url: ", url);
//        Log.e("whatr_id",User.getCurrentUser().getR_id());
//        ApiHandler.apiHit(Request.Method.GET, url, apiCallBack, this, null);


        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.newVehicleLocation+"?v_id="+data.getV_id(), apiCallBack, this, null);
    }


    ApiHandler.ApiCallback apiCallBack = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    if (jsonObject.has("loc_data")) {
                        JSONObject obj = jsonObject.getJSONObject("loc_data");
                        if (obj.has("lat")) {
                            lat = obj.getDouble("lat");
                        }
                        if (obj.has("lng")) {
                            lng = obj.getDouble("lng");
                        }
                        if (obj.has("logged_on")) {
                            String dt = obj.getString("logged_on");
                            try {
                                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                Date date = (Date) formatter.parse(dt);
                                if (Math.abs(date.getTime() - System.currentTimeMillis()) > 120000) {
                                    //server timestamp is within 5 minutes of current system time
                                } else {
                                    //server is not within 5 minutes of current system time
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        setLocationOnMap(lat, lng);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                errortext.setVisibility(View.VISIBLE);
                errortext.setText(error.getMessage());
                //  Toast.makeText(MapView.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    };

    private void setLocationOnMap(double latitude, double longitude) {

        latLng = new LatLng(latitude,longitude );
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(latitude, longitude), 16));
        String ABCD;

        try {
            addresses = geo.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.isEmpty()) {


        } else {
            if (addresses.size() > 0) {
                Address returnAddress = addresses.get(0);
                String localityString = returnAddress.getLocality();

                str = new StringBuilder();

                str.append(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getAddressLine(1) + " ");
                Log.e("my loctinstring",str.toString() );

            }
        }
        if(str.toString()!=null){
            ABCD=str.toString();
        }else{
            ABCD="fetching location!";
        }

        MarkerOptions markerOpt = new MarkerOptions()
                .title(ABCD)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_stop))
                .position(new LatLng(latitude, longitude));
        if (myMarker == null) {
            myMarker = map.addMarker(markerOpt);
            myMarker.showInfoWindow();
        } else {
            myMarker.setPosition(new LatLng(latitude, longitude));
        }
//        map.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
//        map.animateCamera(CameraUpdateFactory.newLatLngZoom(   new LatLng(latitude, longitude), 16));
        animateMarker(latLng, false);
    }
    public void animateMarker(final LatLng toPosition,final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(latLng);
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 5000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                myMarker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarke) {
                        myMarker.setVisible(false);
                    } else {
                        myMarker.setVisible(true);
                    }
                }
            }
        });
    }


}