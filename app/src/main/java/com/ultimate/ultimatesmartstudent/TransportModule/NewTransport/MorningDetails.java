package com.ultimate.ultimatesmartstudent.TransportModule.NewTransport;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.TransportModule.Common_Attend_Adapter;
import com.ultimate.ultimatesmartstudent.TransportModule.Common_Attend_Bean;
import com.ultimate.ultimatesmartstudent.TransportModule.TranTodayAttActivity;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MorningDetails extends AppCompatActivity {
    String from_date = "",driver_name = "",vehicle_name = "",route="",mobile="";
    String viewwwww = "";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    Animation animation;
    ArrayList<Common_Attend_Bean> list;
    ArrayList<Common_Attend_Bean> list1;
    private LinearLayoutManager layoutManager;
    private Common_Attend_Adapter_New adapter;
    CommonProgress commonProgress;
    @BindView(R.id.today_date)
    TextView today_date;
    SharedPreferences sharedPreferences;
    @BindView(R.id.dialog)
    ImageView dialog1;

    Dialog dialog;
    EditText edtgName;
    EditText edtRelation,edtRelation1;
    EditText edtgMobile;
    CircularImageView image;
    ImageView edit_profile;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_morning_details);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        viewwwww = getIntent().getStringExtra("view");
        if (viewwwww.equalsIgnoreCase("morning_date")){
            txtTitle.setText(getString(R.string.m_att)+" "+getString(R.string.list));
        }else if (viewwwww.equalsIgnoreCase("evening_date")){
            txtTitle.setText(getString(R.string.e_att)+" "+getString(R.string.list));
        }
        else if (viewwwww.equalsIgnoreCase("morning")){
            txtTitle.setText(getString(R.string.m_att));
        }else{
            txtTitle.setText(getString(R.string.e_att));
        }

        if (!restorePrefData()){
            setShowcaseView();
        }

        list = new ArrayList<>();
        list1 = new ArrayList<>();
        layoutManager = new LinearLayoutManager(MorningDetails.this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerview.setLayoutManager(layoutManager);
        adapter = new Common_Attend_Adapter_New(list, MorningDetails.this);
        recyclerview.setAdapter(adapter);

    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog1,"Driver/Van Details!","Tap the Driver/Van button to view Driver/Van Details.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_search_driver",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_search_driver",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_search_driver",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_search_driver",false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        // params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("stu_id", User.getCurrentUser().getId());
        if (viewwwww.equalsIgnoreCase("morning")){
            params.put("vehicle_id", User.getCurrentUser().getMorningVId());
            params.put("view_type", "morning_date");
        }else{
            params.put("vehicle_id", User.getCurrentUser().getEveningVId());
            params.put("view_type", "evening_date");
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COMMON_ATTEND_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                try {
                    if (list1 != null) {
                        list1.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("attend_data");
                    list1 = Common_Attend_Bean.parseMorn_Attend_BeanArray(jsonArray);
                    driver_name=list1.get(0).getDrivername();
                    route=list1.get(0).getRoutename();
                    mobile=list1.get(0).getDriver_mobile();
                    vehicle_name=list1.get(0).getVehiclename();
                   // Toast.makeText(getApplicationContext(), "driver_name: "+driver_name, Toast.LENGTH_SHORT).show();
                    dialog_view();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, this, params);

    }

    private void dialog_view() {

        dialog = new Dialog(MorningDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);

        dialog.setContentView(R.layout.add_person_dialog_new);
        edtgName = (EditText) dialog.findViewById(R.id.edtgName);
        edtgMobile = (EditText) dialog.findViewById(R.id.edtgMobile);
        edtRelation = (EditText) dialog.findViewById(R.id.edtRelation);
        edtRelation1 = (EditText) dialog.findViewById(R.id.edtRelation1);
        image = (CircularImageView) dialog.findViewById(R.id.circleimgrecepone);
        edit_profile = (ImageView) dialog.findViewById(R.id.edit_profile);

        edtgName.setEnabled(false);
        edtgMobile.setEnabled(false);
        edtRelation.setEnabled(false);
        edtRelation1.setEnabled(false);

        btn = (Button) dialog.findViewById(R.id.btnF);
        TextView guardian_mini = (TextView) dialog.findViewById(R.id.guardian_mini);
        if (driver_name!=null){
            edtgName.setText(driver_name);
        }else{
            edtgName.setText("Not Mentioned");
        }
        if (mobile!=null){
            edtgMobile.setText(mobile);
        }else{
            edtgMobile.setText("Not Mentioned");
        }
        if (route!=null){
            edtRelation.setText(route);
        }else{
            edtRelation.setText("Not Mentioned");
        }
        if (vehicle_name!=null){
            edtRelation1.setText(vehicle_name);
        }else{
            edtRelation1.setText("Not Mentioned");
        }

        guardian_mini.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        edit_profile.setVisibility(View.GONE);
        btn.setVisibility(View.GONE);

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @OnClick(R.id.root)
    public void cal_lyttttt(){

        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            today_date.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
        //    if (apply.equalsIgnoreCase("Pending")){
              fetchGatePass();
//            }else {
//                p_fetchhwlist(from_date,"");
//            }

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        fetchGatePass();
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    private void fetchGatePass() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        // params.put("class_id", User.getCurrentUser().getClass_id());

        params.put("stu_id", User.getCurrentUser().getId());
        if (viewwwww.equalsIgnoreCase("morning")){
            params.put("vehicle_id", User.getCurrentUser().getMorningVId());
        }else{
            params.put("vehicle_id", User.getCurrentUser().getEveningVId());
        }
        params.put("view_type", viewwwww);
        params.put("mdate", from_date);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COMMON_ATTEND_URL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (list != null) {
                        list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("attend_data");
                    list = Common_Attend_Bean.parseMorn_Attend_BeanArray(jsonArray);

                    if (list.size() > 0) {
                        adapter.setList(list);
                        //setanimation on adapter...
                        recyclerview.getAdapter().notifyDataSetChanged();
                        recyclerview.scheduleLayoutAnimation();
                        totalRecord.setText("Total Entries:- "+String.valueOf(list.size()));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setList(list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                adapter.setList(list);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };
}