package com.ultimate.ultimatesmartstudent.TransportModule;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleDetailMornAdap extends RecyclerView.Adapter<VehicleDetailMornAdap.Viewholder> {

    ArrayList<VehicleDetailMornBean> hq_list;
    private MethodCallBack callBackMethod;
    Context listner;
    Animation animation;
    public VehicleDetailMornAdap(ArrayList<VehicleDetailMornBean> hq_list, Context listner,MethodCallBack callBackMethod) {
        this.hq_list = hq_list;
        this.listner = listner;
        this.callBackMethod = callBackMethod;
    }



    @Override
    public VehicleDetailMornAdap.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lessonplanada_lay, parent, false);
        VehicleDetailMornAdap.Viewholder viewholder = new VehicleDetailMornAdap.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final VehicleDetailMornAdap.Viewholder holder, final int position) {
        animation = AnimationUtils.loadAnimation(listner, R.anim.btn_blink_animation);

        if (hq_list.get(position).getV_no() != null) {
            holder.v_number.setText(hq_list.get(position).getV_no());
        }else {
            holder.v_number.setText("Not Mentioned!");
        }
        if (hq_list.get(position).getV_no() != null) {
            holder.v_name.setText(hq_list.get(position).getTr_transport_name());
        }else {

            holder.v_name.setText("Not Mentioned!");
        }
        if (hq_list.get(position).getV_no() != null) {
            holder.name.setText(hq_list.get(position).getD_name());
        }else {
            holder.name.setText("Not Mentioned!");
        }
        if (hq_list.get(position).getV_no() != null) {
            holder.phone.setText(hq_list.get(position).getD_mobile());
        }else {
            holder.phone.setText("Not Mentioned!");
        }

        holder.view_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.view_map.startAnimation(animation);
                if (callBackMethod != null) {
                    callBackMethod.ViewLocation(hq_list.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setroomList(ArrayList<VehicleDetailMornBean> hq_list) {
        this.hq_list = hq_list;
    }

    public interface MethodCallBack {

        void ViewLocation(VehicleDetailMornBean vehicleDetailMornBean);
    }
    public class Viewholder extends RecyclerView.ViewHolder {
        public AppCompatTextView name,phone,v_number,v_name;

        @BindView(R.id.view_map)
        Button view_map;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            name = itemView.findViewById(R.id.name);
            phone = itemView.findViewById(R.id.phone);
            v_name = itemView.findViewById(R.id.v_name);

            v_number = itemView.findViewById(R.id.v_number);




        }
    }

}
