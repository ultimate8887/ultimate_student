package com.ultimate.ultimatesmartstudent.TransportModule.NewTransport;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.textfield.TextInputLayout;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.TransportModule.Common_Attend_Adapter;
import com.ultimate.ultimatesmartstudent.TransportModule.Common_Attend_Bean;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Common_Attend_Adapter_New extends RecyclerView.Adapter<Common_Attend_Adapter_New.Viewholder> {
    ArrayList<Common_Attend_Bean> list;
    Context listner;

    public Common_Attend_Adapter_New(ArrayList<Common_Attend_Bean> list, Context listener) {
        this.list = list;
        this.listner = listener;
    }

    @Override
    public Common_Attend_Adapter_New.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gate_pass_lyt_new, parent, false);
        Common_Attend_Adapter_New.Viewholder viewholder = new Common_Attend_Adapter_New.Viewholder(view);
        return viewholder;

    }

    public void setList(ArrayList<Common_Attend_Bean> list) {
        this.list = list;
    }

    @Override
    public void onBindViewHolder(Common_Attend_Adapter_New.Viewholder holder, final int position) {
        final Common_Attend_Bean data = list.get(position);
        holder.txtstuName.setEnabled(false);
        holder.txtstuName.setEnabled(false);
        holder.txtReason.setEnabled(false);

        holder.txtstuName.setEnabled(false);
        holder.txtReason.setEnabled(false);
        holder.txtcontprsn.setEnabled(false);
        String mclass = "";

        if (data.getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("gp-" + data.getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

//        if (mData.getGate_class() != null)
//            mclass = mData.getGate_class();
        if (User.getCurrentUser().getSection_name() != null) {
            String title = getColoredSpanned("Class: ", "#5A5C59");
            String Name = getColoredSpanned(User.getCurrentUser().getClass_name() + "-" + User.getCurrentUser().getSection_name(), "#000000");
            holder.txtPhone.setText(Html.fromHtml(title + " " + Name));
        }


        if (data.getStudent_name() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(data.getStudent_name() + "(" + data.getStudent_id() + ")", "#000000");
            holder.txtstuName.setText(Html.fromHtml(title + " " + Name));
        }


        String attend = "N/A", attend_time = "   -";
        if (data.getAttendance().equalsIgnoreCase("a")) {
            attend = "Absent";
            holder.txtDT.setTextColor(ContextCompat.getColor(listner, R.color.absent));
        } else if (data.getAttendance().equalsIgnoreCase("p")) {
            attend = "Present";
            holder.txtDT.setTextColor(ContextCompat.getColor(listner, R.color.present));
        } else if (data.getAttendance().equalsIgnoreCase("l")) {
            attend = "Leave";
            holder.txtDT.setTextColor(ContextCompat.getColor(listner, R.color.leave));
        } else {
            holder.txtDT.setTextColor(ContextCompat.getColor(listner, R.color.light_grey));
        }
        holder.txtDT.setText(attend);


        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1 = dateFormat.format(cal.getTime());

        String check = Utils.getDateFormated(data.getAt_attendance_date());
        if (check.equalsIgnoreCase(dateString)) {
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            String title1 = getColoredSpanned("at", "#5A5C59");
            String title2 = getColoredSpanned(Utils.getTimeHr(data.getCheckintime()), "#e31e25");
            String l_Name = getColoredSpanned("Attendance time: ", "#5A5C59");
            holder.txtdandtofcall.setText(Html.fromHtml(l_Name + " " + title+ " " + title1+ " " + title2));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        } else if (check.equalsIgnoreCase(dateString1)) {

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            String title1 = getColoredSpanned("at", "#5A5C59");
            String title2 = getColoredSpanned(Utils.getTimeHr(data.getCheckintime()), "#1C8B3B");

            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Attendance time: ", "#5A5C59");
            holder.txtdandtofcall.setText(Html.fromHtml(l_Name + " " + title+ " " + title1+ " " + title2));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        } else {
            //   if (mData.getGate_datetime().equalsIgnoreCase("Morning")){
         //   holder.txtdandtofcall.setText(Utils.getDateTimeFormated(data.getAt_attendance_date()) + "AM");
            String title = getColoredSpanned(Utils.getDateTimeFormatedWithAMPM(data.getCheckintime()), "#000000");
            String l_Name = getColoredSpanned("Attendance time: ", "#5A5C59");
            holder.txtdandtofcall.setText(Html.fromHtml(l_Name + " " + title+ " "));
//            }else {
//                holder.txtDT.setText(Utils.getDateTimeFormated(mData.getAt_attendance_date())+"PM");
//            }
        }


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtFatherName)
        TextView txtFatherName;
        @BindView(R.id.txtPhone)
        TextView txtPhone;
        @BindView(R.id.txtDT)
        TextView txtDT;
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.p_title)
        TextView p_title;
        @BindView(R.id.txtABy)
        TextView txtABy;
        @BindView(R.id.txtReason)
        EditText txtReason;
        @BindView(R.id.txtstuName)
        EditText txtstuName;
        @BindView(R.id.txtcontprsn)
        EditText txtcontprsn;
        @BindView(R.id.txtcontprsnnum)
        TextView txtcontprsnnum;
        @BindView(R.id.txtrelation)
        TextView txtrelation;
        @BindView(R.id.txtconfirmationnum)
        TextView txtconfirmationnum;
        @BindView(R.id.txtdandtofcall)
        TextView txtdandtofcall;

        @BindView(R.id.inpUsername)
        TextInputLayout inpUsername;

        @BindView(R.id.std_sign)
        ImageView std_sign;
        @BindView(R.id.staff_sign)
        ImageView staff_sign;
        @BindView(R.id.admin_sign)
        ImageView admin_sign;

        @BindView(R.id.profile)
        CircularImageView profile;


        @BindView(R.id.sign_lyt)
        CardView sign_lyt;

        @BindView(R.id.ggg)
        RelativeLayout ggg;

        @BindView(R.id.guardian_add)
        TextView guardian_add;
        @BindView(R.id.guardian_mini)
        TextView guardian_mini;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ggg.setVisibility(View.GONE);
            inpUsername.setVisibility(View.GONE);
            txtFatherName.setVisibility(View.GONE);
        }
    }
}
