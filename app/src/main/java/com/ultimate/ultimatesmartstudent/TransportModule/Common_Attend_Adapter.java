package com.ultimate.ultimatesmartstudent.TransportModule;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartstudent.AttendMod.Util;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Common_Attend_Adapter extends RecyclerView.Adapter<Common_Attend_Adapter.Viewholder> {
    ArrayList<Common_Attend_Bean> list;
    Context listner;

    public Common_Attend_Adapter(ArrayList<Common_Attend_Bean> list, Context listener) {
        this.list = list;
        this.listner = listener;
    }

    @Override
    public Common_Attend_Adapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.common_att_lyt, parent, false);
        Common_Attend_Adapter.Viewholder viewholder = new Common_Attend_Adapter.Viewholder(view);
        return viewholder;

    }

    public void setList(ArrayList<Common_Attend_Bean> list) {
        this.list = list;
    }

    @Override
    public void onBindViewHolder(Common_Attend_Adapter.Viewholder holder, final int position) {
        final Common_Attend_Bean mData = list.get(position);
        holder.txtstuName.setEnabled(false);
        holder.txtstuName.setEnabled(false);
        holder.txtReason.setEnabled(false);
        holder.txtReason1.setEnabled(false);

        if (mData.getAttendance() != null) {
            String title = getColoredSpanned("Attendance: ", "#5A5C59");
            String Name = getColoredSpanned("Present", "#1C8B3B");
            holder.txtPhone.setText(Html.fromHtml(title + " " + Name));
        }
        if (mData.getStudent_name() != null) {
            String title = getColoredSpanned("Student Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getStudent_name(), "#7D7D7D");
            holder.txtFatherName.setText(Html.fromHtml(title + " " + Name));
        }

        if(mData.getCheckintime() != null){
            holder.txtReason.setText(mData.getCheckintime());
        }
        else{
            holder.txtReason.setText("Not Mentioned");
        }

        if(mData.getCheckouttime() != null){
        holder.txtReason1.setText(mData.getCheckouttime());
        }
        else{
            holder.txtReason1.setText("Not Mentioned!");
        }

        if (mData.getDrivername() != null) {
            holder.txtcontprsnnum.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Driver Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getDrivername(), "#7D7D7D");
            holder.txtcontprsnnum.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getAttendMarkByUser() != null) {
            holder.txtrelation.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Attendance Mark By: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getAttendMarkByUser(), "#7D7D7D");
            holder.txtrelation.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getVehiclename() != null) {
            holder.txtvanName.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Van Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getVehiclename(), "#7D7D7D");
            holder.txtvanName.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getRoutename() != null) {
            holder.txtproute.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Route Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getRoutename(), "#7D7D7D");
            holder.txtproute.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getPickupname() != null) {
            holder.txtpickup.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Pick-Up Point: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getPickupname(), "#7D7D7D");
            holder.txtpickup.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getAt_attendance_date() != null) {
            holder.txtDT.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned("<b>" +Utils.dateFormat(mData.getAt_attendance_date())+"</b>" , "#F4D00C");
            holder.txtDT.setText(Html.fromHtml(title + " " + Name));
        }


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtFatherName)
        TextView txtFatherName;
        @BindView(R.id.txtPhone)
        TextView txtPhone;
        @BindView(R.id.txtDT)
        TextView txtDT;
        @BindView(R.id.txtABy)
        TextView txtABy;
        @BindView(R.id.txtReason)
        EditText txtReason;
        @BindView(R.id.txtReason1)
        EditText txtReason1;
        @BindView(R.id.txtstuName)
        EditText txtstuName;
        @BindView(R.id.txtcontprsn)
        EditText txtcontprsn;
        @BindView(R.id.txtcontprsnnum)
        TextView txtcontprsnnum;
        @BindView(R.id.txtrelation)
        TextView txtrelation;
        @BindView(R.id.txtconfirmationnum)
        TextView txtconfirmationnum;
        @BindView(R.id.txtdandtofcall)
        TextView txtdandtofcall;

        @BindView(R.id.txtvanName)
        TextView txtvanName;
        @BindView(R.id.txtproute)
        TextView txtproute;
        @BindView(R.id.txtpickup)
        TextView txtpickup;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
