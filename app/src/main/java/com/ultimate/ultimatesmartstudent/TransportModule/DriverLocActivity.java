package com.ultimate.ultimatesmartstudent.TransportModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;
import com.ultimate.ultimatesmartstudent.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverLocActivity extends AppCompatActivity implements VehicleDetailMornAdap.MethodCallBack,VehicleDetailEvngAdap.MethodCallBack {
    CommonProgress commonProgress;
    String viewwwww = "";
    @BindView(R.id.parentss)
    RelativeLayout parent;
    @BindView(R.id.recycleMorn)
    RecyclerView recycleMorn;
    @BindView(R.id.recycleEvng)
    RecyclerView recycleEvng;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManager2;
    ArrayList<VehicleDetailMornBean> arrayList = new ArrayList<>();
    ArrayList<VehicleDetailMornBean> arrayList2 = new ArrayList<>();
    private VehicleDetailMornAdap adap;
    private VehicleDetailEvngAdap evngadap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_loc);
        ButterKnife.bind(this);
        viewwwww = getIntent().getStringExtra("view");
        commonProgress=new CommonProgress(this);
      if (viewwwww.equalsIgnoreCase("morning")){
            txtTitle.setText(getString(R.string.track_morn));
            recycleMorn.setVisibility(View.VISIBLE);
            recycleEvng.setVisibility(View.GONE);
        }else{
            txtTitle.setText(getString(R.string.track_evng));
          recycleMorn.setVisibility(View.GONE);
          recycleEvng.setVisibility(View.VISIBLE);
        }
        layoutManager = new LinearLayoutManager(DriverLocActivity.this);
        recycleMorn.setLayoutManager(layoutManager);
        adap = new VehicleDetailMornAdap(arrayList, DriverLocActivity.this, this);
        recycleMorn.setAdapter(adap);

        layoutManager2 = new LinearLayoutManager(DriverLocActivity.this);
        recycleEvng.setLayoutManager(layoutManager2);
        evngadap = new VehicleDetailEvngAdap(arrayList2, DriverLocActivity.this, this);
        recycleEvng.setAdapter(evngadap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetechMornVehclList();
        fetechEvngVehclList();
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        finish();
    }

    private void fetechEvngVehclList() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("evng_id", User.getCurrentUser().getEveningVId());
        params.put("boardid", User.getCurrentUser().getR_id());

        Log.e("evng_id", User.getCurrentUser().getEveningVId());
        Log.e("boardid", User.getCurrentUser().getR_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.fetchEvngVehicle, apiCallBack2, this, params);

    }

    ApiHandler.ApiCallback apiCallBack2 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (arrayList2 != null) {
                        arrayList2.clear();
                        // ArrayList<VehicleDetailMornBean> arrayList1 = VehicleDetailMornBean.parseDrivrToVicleArray(jsonObject.getJSONArray("alotdv_data"));
                        JSONArray jsonArray = jsonObject.getJSONArray("acieve_data");
                        arrayList2 = VehicleDetailMornBean.parseHOSTLRoomArray(jsonArray);
                        if (arrayList2.size() > 0) {
                            evngadap.setroomList(arrayList2);
                            evngadap.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.GONE);
                        } else {
                            evngadap.setroomList(arrayList2);
                            evngadap.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // Utils.showSnackBar(error.getMessage(), parent);
                txtNorecord.setVisibility(View.VISIBLE);
                arrayList2.clear();
                evngadap.setroomList(arrayList2);
                evngadap.notifyDataSetChanged();
            }
        }
    };

    private void fetechMornVehclList() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mornV_id", User.getCurrentUser().getMorningVId());
        params.put("boardid", User.getCurrentUser().getR_id());

        Log.e("mornV_id", User.getCurrentUser().getMorningVId());
        Log.e("boardid", User.getCurrentUser().getR_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.fetchMornVehicle, apiCallBack, this, params);
        //ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.fetchMornVehicle, apiCallBack, this, null);
    }


    ApiHandler.ApiCallback apiCallBack = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (arrayList != null) {
                        arrayList.clear();

                        // ArrayList<VehicleDetailMornBean> arrayList1 = VehicleDetailMornBean.parseDrivrToVicleArray(jsonObject.getJSONArray("alotdv_data"));
                        JSONArray jsonArray = jsonObject.getJSONArray("acieve_data");
                        arrayList = VehicleDetailMornBean.parseHOSTLRoomArray(jsonArray);
                        if (arrayList.size() > 0) {
                            adap.setroomList(arrayList);
                            adap.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.GONE);
                        } else {
                            adap.setroomList(arrayList);
                            adap.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // Utils.showSnackBar(error.getMessage(), parent);
                txtNorecord.setVisibility(View.VISIBLE);
                arrayList.clear();
                adap.setroomList(arrayList);
                adap.notifyDataSetChanged();
            }
        }
    };


    @Override
    public void ViewLocation(VehicleDetailMornBean vehicleDetailMornBean) {

        Gson gson = new Gson();
        String dv = gson.toJson(vehicleDetailMornBean, VehicleDetailMornBean.class);
        Intent i = new Intent(DriverLocActivity.this, MapView.class);
        i.putExtra("vehicledt", dv);
        startActivity(i);
        Animatoo.animateShrink(DriverLocActivity.this);
    }

    @Override
    public void ViewLocationEvng(VehicleDetailMornBean vehicleDetailMornBean) {

        Gson gson = new Gson();
        String dv = gson.toJson(vehicleDetailMornBean, VehicleDetailMornBean.class);
        Intent i = new Intent(DriverLocActivity.this, MapView.class);
        i.putExtra("vehicledt", dv);
        startActivity(i);
        Animatoo.animateShrink(DriverLocActivity.this);
    }
}