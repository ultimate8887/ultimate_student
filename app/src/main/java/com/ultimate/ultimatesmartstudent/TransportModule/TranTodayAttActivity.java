package com.ultimate.ultimatesmartstudent.TransportModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartstudent.BeanModule.User;
import com.ultimate.ultimatesmartstudent.R;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandler;
import com.ultimate.ultimatesmartstudent.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartstudent.Utility.CommonProgress;
import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.UltimateProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TranTodayAttActivity extends AppCompatActivity {
    String viewwwww = "";
    @BindView(R.id.recyclerView11)
    RecyclerView recyclerview;
    @BindView(R.id.spinner2)
    Spinner SubjectSpinner;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    Animation animation;
    ArrayList<Common_Attend_Bean> list;
    private LinearLayoutManager layoutManager;
    private Common_Attend_Adapter adapter;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tran_today_att);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        viewwwww = getIntent().getStringExtra("view");
         if (viewwwww.equalsIgnoreCase("morning_date")){
             txtTitle.setText(getString(R.string.m_att)+" "+getString(R.string.list));
         }else if (viewwwww.equalsIgnoreCase("evening_date")){
             txtTitle.setText(getString(R.string.e_att)+" "+getString(R.string.list));
         }
        else if (viewwwww.equalsIgnoreCase("morning")){
            txtTitle.setText(getString(R.string.m_att));
        }else{
            txtTitle.setText(getString(R.string.e_att));
        }

        SubjectSpinner.setVisibility(View.GONE);
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(TranTodayAttActivity.this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerview.setLayoutManager(layoutManager);
        adapter = new Common_Attend_Adapter(list, TranTodayAttActivity.this);
        recyclerview.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchGatePass();
    }

    @OnClick(R.id.imgBackmsg)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    private void fetchGatePass() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
       // params.put("class_id", User.getCurrentUser().getClass_id());
        params.put("vehicle_id", User.getCurrentUser().getMorningVId());
        params.put("stu_id", User.getCurrentUser().getId());
        params.put("view_type", viewwwww);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COMMON_ATTEND_URL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (list != null) {
                        list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("attend_data");
                    list = Common_Attend_Bean.parseMorn_Attend_BeanArray(jsonArray);

                    if (list.size() > 0) {
                        adapter.setList(list);
                        //setanimation on adapter...
                        recyclerview.getAdapter().notifyDataSetChanged();
                        recyclerview.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        adapter.setList(list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                adapter.setList(list);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };
}