package com.ultimate.ultimatesmartstudent.TransportModule;

import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Common_Attend_Bean {


    private String id;
    private String student_id;
    private String student_name;
    private String attendance;
    private String checkintime;
    private String checkouttime;
    private String drivername;
    private String driverid;
    private String AttendMarkByUser;
    private String AttendMarkByUserId;
    private String status;

    public String getAt_attendance_date() {
        return at_attendance_date;
    }

    public void setAt_attendance_date(String at_attendance_date) {
        this.at_attendance_date = at_attendance_date;
    }

    private String at_attendance_date;

    public String getRoutename() {
        return routename;
    }

    public void setRoutename(String routename) {
        this.routename = routename;
    }

    public String getPickupname() {
        return pickupname;
    }

    public void setPickupname(String pickupname) {
        this.pickupname = pickupname;
    }

    public String getVehiclename() {
        return vehiclename;
    }

    public void setVehiclename(String vehiclename) {
        this.vehiclename = vehiclename;
    }


    public String getDriver_mobile() {
        return driver_mobile;
    }

    public void setDriver_mobile(String driver_mobile) {
        this.driver_mobile = driver_mobile;
    }

    private String driver_mobile;
    private String routename;
    private String pickupname;
    private String vehiclename;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getCheckintime() {
        return checkintime;
    }

    public void setCheckintime(String checkintime) {
        this.checkintime = checkintime;
    }

    public String getCheckouttime() {
        return checkouttime;
    }

    public void setCheckouttime(String checkouttime) {
        this.checkouttime = checkouttime;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getDriverid() {
        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }

    public String getAttendMarkByUser() {
        return AttendMarkByUser;
    }

    public void setAttendMarkByUser(String attendMarkByUser) {
        AttendMarkByUser = attendMarkByUser;
    }

    public String getAttendMarkByUserId() {
        return AttendMarkByUserId;
    }

    public void setAttendMarkByUserId(String attendMarkByUserId) {
        AttendMarkByUserId = attendMarkByUserId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static ArrayList<Common_Attend_Bean> parseMorn_Attend_BeanArray(JSONArray arrayObj) {
        ArrayList<Common_Attend_Bean> list = new ArrayList<Common_Attend_Bean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Common_Attend_Bean p = parseMorn_Attend_BeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Common_Attend_Bean parseMorn_Attend_BeanObject(JSONObject jsonObject) {
        Common_Attend_Bean casteObj = new Common_Attend_Bean();
        try {

            if (jsonObject.has("id") && !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("student_id") && !jsonObject.getString("student_id").isEmpty() && !jsonObject.getString("student_id").equalsIgnoreCase("null")) {
                casteObj.setStudent_id(jsonObject.getString("student_id"));
            }
            if (jsonObject.has("student_name") && !jsonObject.getString("student_name").isEmpty() && !jsonObject.getString("student_name").equalsIgnoreCase("null")) {
                casteObj.setStudent_name(jsonObject.getString("student_name"));
            }
            if (jsonObject.has("attendance") && !jsonObject.getString("attendance").isEmpty() && !jsonObject.getString("attendance").equalsIgnoreCase("null")) {
                casteObj.setAttendance(jsonObject.getString("attendance"));
            }
            if (jsonObject.has("checkintime") && !jsonObject.getString("checkintime").isEmpty() && !jsonObject.getString("checkintime").equalsIgnoreCase("null")) {
                casteObj.setCheckintime(jsonObject.getString("checkintime"));
            }
            if (jsonObject.has("driver_mobile") && !jsonObject.getString("driver_mobile").isEmpty() && !jsonObject.getString("driver_mobile").equalsIgnoreCase("null")) {
                casteObj.setDriver_mobile(jsonObject.getString("driver_mobile"));
            }

            if (jsonObject.has("checkouttime") && !jsonObject.getString("checkouttime").isEmpty() && !jsonObject.getString("checkouttime").equalsIgnoreCase("null")) {
                casteObj.setCheckouttime(jsonObject.getString("checkouttime"));
            }

            if (jsonObject.has("drivername") && !jsonObject.getString("drivername").isEmpty() && !jsonObject.getString("drivername").equalsIgnoreCase("null")) {
                casteObj.setDrivername(jsonObject.getString("drivername"));
            }
            if (jsonObject.has("driverid") && !jsonObject.getString("driverid").isEmpty() && !jsonObject.getString("driverid").equalsIgnoreCase("null")) {
                casteObj.setDriverid(jsonObject.getString("driverid"));
            }
            if (jsonObject.has("AttendMarkByUser") && !jsonObject.getString("AttendMarkByUser").isEmpty() && !jsonObject.getString("AttendMarkByUser").equalsIgnoreCase("null")) {
                casteObj.setAttendMarkByUser(jsonObject.getString("AttendMarkByUser"));
            }
            if (jsonObject.has("AttendMarkByUserId") && !jsonObject.getString("AttendMarkByUserId").isEmpty() && !jsonObject.getString("AttendMarkByUserId").equalsIgnoreCase("null")) {
                casteObj.setAttendMarkByUserId(jsonObject.getString("AttendMarkByUserId"));
            }

            if (jsonObject.has("status") && !jsonObject.getString("status").isEmpty() && !jsonObject.getString("status").equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString("status"));
            }

            if (jsonObject.has("routename") && !jsonObject.getString("routename").isEmpty() && !jsonObject.getString("routename").equalsIgnoreCase("null")) {
                casteObj.setRoutename(jsonObject.getString("routename"));
            }
            if (jsonObject.has("pickupname") && !jsonObject.getString("pickupname").isEmpty() && !jsonObject.getString("pickupname").equalsIgnoreCase("null")) {
                casteObj.setPickupname(jsonObject.getString("pickupname"));
            }

            if (jsonObject.has("vehiclename") && !jsonObject.getString("vehiclename").isEmpty() && !jsonObject.getString("vehiclename").equalsIgnoreCase("null")) {
                casteObj.setVehiclename(jsonObject.getString("vehiclename"));
            }
            if (jsonObject.has("at_attendance_date") && !jsonObject.getString("at_attendance_date").isEmpty() && !jsonObject.getString("at_attendance_date").equalsIgnoreCase("null")) {
                casteObj.setAt_attendance_date(jsonObject.getString("at_attendance_date"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
