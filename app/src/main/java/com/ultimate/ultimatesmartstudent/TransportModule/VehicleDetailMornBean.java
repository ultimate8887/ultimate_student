package com.ultimate.ultimatesmartstudent.TransportModule;

import com.ultimate.ultimatesmartstudent.Utility.Constants;
import com.ultimate.ultimatesmartstudent.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VehicleDetailMornBean {

        private String b_id;
        private String v_id;
        private String v_no;
        private String d_id;
        private String d_name;
        private String d_mobile;

    public String getTr_transport_name() {
        return tr_transport_name;
    }

    public void setTr_transport_name(String tr_transport_name) {
        this.tr_transport_name = tr_transport_name;
    }

    private String tr_transport_name;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    private String profile;

        public String getB_id() {
        return b_id;
    }

        public void setB_id(String b_id) {
        this.b_id = b_id;
    }

        public String getV_id() {
        return v_id;
    }

        public void setV_id(String v_id) {
        this.v_id = v_id;
    }

        public String getV_no() {
        return v_no;
    }

        public void setV_no(String v_no) {
        this.v_no = v_no;
    }

        public String getD_id() {
        return d_id;
    }

        public void setD_id(String d_id) {
        this.d_id = d_id;
    }

        public String getD_name() {
        return d_name;
    }

        public void setD_name(String d_name) {
        this.d_name = d_name;
    }

        public String getD_mobile() {
        return d_mobile;
    }

        public void setD_mobile(String d_mobile) {
        this.d_mobile = d_mobile;
    }

        public static ArrayList<VehicleDetailMornBean> parseHOSTLRoomArray(JSONArray arrayObj) {
        ArrayList<VehicleDetailMornBean> list = new ArrayList<VehicleDetailMornBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                VehicleDetailMornBean p = parseHOSTLSRoomObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

        public static VehicleDetailMornBean parseHOSTLSRoomObject(JSONObject jsonObject) {
        VehicleDetailMornBean casteObj = new VehicleDetailMornBean();
        try {

            if (jsonObject.has("b_id") && !jsonObject.getString("b_id").isEmpty() && !jsonObject.getString("b_id").equalsIgnoreCase("null")) {
                casteObj.setB_id(jsonObject.getString("b_id"));
            }
            if (jsonObject.has("v_id") && !jsonObject.getString("v_id").isEmpty() && !jsonObject.getString("v_id").equalsIgnoreCase("null")) {
                casteObj.setV_id(jsonObject.getString("v_id"));
            }
            if (jsonObject.has("v_no") && !jsonObject.getString("v_no").isEmpty() && !jsonObject.getString("v_no").equalsIgnoreCase("null")) {
                casteObj.setV_no(jsonObject.getString("v_no"));
            }
            if (jsonObject.has("d_id") && !jsonObject.getString("d_id").isEmpty() && !jsonObject.getString("d_id").equalsIgnoreCase("null")) {
                casteObj.setD_id(jsonObject.getString("d_id"));
            }
            if (jsonObject.has("d_name") && !jsonObject.getString("d_name").isEmpty() && !jsonObject.getString("d_name").equalsIgnoreCase("null")) {
                casteObj.setD_name(jsonObject.getString("d_name"));
            }
            if (jsonObject.has("d_mobile") && !jsonObject.getString("d_mobile").isEmpty() && !jsonObject.getString("d_mobile").equalsIgnoreCase("null")) {
                casteObj.setD_mobile(jsonObject.getString("d_mobile"));
            }

            if (jsonObject.has("tr_transport_name") && !jsonObject.getString("tr_transport_name").isEmpty() && !jsonObject.getString("tr_transport_name").equalsIgnoreCase("null")) {
                casteObj.setTr_transport_name(jsonObject.getString("tr_transport_name"));
            }
            if (jsonObject.has("profile") && !jsonObject.getString("profile").isEmpty() && !jsonObject.getString("profile").equalsIgnoreCase("null")) {
                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString("profile"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    }
